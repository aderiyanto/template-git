<?php
	#includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/mainmod-extend.php");
	include("../modz/connic.php");
	include("../modz/getall.php");
	
	$page = 'home';

	#meta
	$pagedata['description'] = '';
	$pagedata['keyword'] = '';

	#minify html
	ob_start('minify');
?>
<!DOCTYPE html>
<html lang="<?php print $active_lang;?>">
<head>
<?php include("com/com-meta.php"); ?>
<title>Home | <?php print SITE_TITLE; ?></title>
<?php
$cssfiles = array(
	'dropdown/dropdown.css',
	'dropdown/themes/_template/default.css',
	'nivostyle.css',
	'facebox.css',
	'style.css'
);?>
<link rel="stylesheet" type="text/css" href="<?php print filecache('render', $cssfiles, 'css');?>"/>
<?php print customscript();?>
<?php
$jsfiles = array(
	'jquery.js',
	'jquery.validate.js',
	'jquery.form.js',
	'validate.js',
	'jquery.dropdown.js',
	'jquery.nivo.slider.pack.js',
	'function.web.js'
);?>
<script type="text/javascript" src="<?php print filecache('render', $jsfiles, 'js');?>"></script>
<?php include ("com/com-extend-head.php");?>
</head>
<body>
<div id="result"></div>
<div id="outermost">
	<div id="outer">
		<div id="topnav">
		</div>

		<div id="logo">
		</div>

		<div id="nav">
		</div>

		<b>MULTILINGUAL</b><br/>
		<?php
		if(count($lc_lang)>1){
			?>
			<div class="lang">
				<?php
				foreach($lc_lang as $key=>$value){
					?>
					<a href="/set/lang/<?php print $value;?>/"><img src="/assets/interface/flag-<?php print $value;?>.png"<?php print ($lang==$value ? ' class="lang-selected"':'');?>/></a>
					<?php
				}
				?>
			</div>
			<?php
			}
		?>
		<?php print $lang.': '._('HELLO'); #example how to use multilingual feature ?>
		
		<br/><br/>
		<b>FORMATING DATE</b><br/>
		<?php
		$format = 'l, d F Y H:i:s';
		print formatdate($format, $now);
		?>		
	</div>
</div>
<?php include ("com/com-extend-body.php");?>
</body>
</html>
<?php ob_end_flush();?>