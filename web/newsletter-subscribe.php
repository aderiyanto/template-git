<?php
	#includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/mainmod-extend.php");
	include("../modz/connic.php");
	include("../modz/getall.php");
	
	$page = 'newsletter';

	if($form=="addnewsletter"){
		#validate form guardian
		$fguard = guardian('validate');

		if(is_array($fguard)){
			#user pass, same key that already used before. we have to generate a new key and resubmit user form
			?>
			<script type="text/javascript">
				formguard('#newsletter', '<?php print "{$fguard['key']}_{$fguard['value']}_{$fguard['_gkey']}";?>');
				$('#newsletter').submit();
			</script>
			<?php
			exit;
		}

		if(empty($newsletteremail)){
			$error=errorlist(70);
		}else{
			//make sure email is of correct format
			if (!filter_var($newsletteremail, FILTER_VALIDATE_EMAIL)){
				$error=errorlist(12);
			}

			if(!$error){
 				$account=getval('subEmail,subActive,subIsMember','newsletter_subscriber','subEmail',$newsletteremail);
				
				if($account['subIsMember'] !='y'){
					$account['subIsMember'] = 'n';
				}
				
				if(!empty($account['subEmail']) and $account['subActive']=='y'){
					$already_subscribe = true;
				}
				
				//if exist,but its not active, try update or display error msg
				if(!isset($already_subscribe)){
					if(!empty($account['subEmail'])){
						if($account['subActive']=='n'){
							$sqln="UPDATE newsletter_subscriber SET subActive='y' WHERE subEmail='$newsletteremail'";
							$queryn=query($sqln);
						}
					}else{
						$verifycode=codegen(40);
						$sqln="INSERT INTO newsletter_subscriber VALUES(null,'$newsletteremail','$verifycode','".$account['subIsMember']."','y')";
						$queryn=query($sqln);
					}
					 
					if($account['subActive'] != 'y'){ 
						#Check it,if it is not exist, add to *Non-customer* subscriber
						if($account['subIsMember'] == 'y'){    
							#but, if it is exist in that list, changes its status to subscribe
							$sendyparams['list'] = getconfig('SENDY_NEWSLETTER_CUSTOMER_ID'); //CUSTOMER list id
						}else{
							$sendyparams['list'] = getconfig('SENDY_NEWSLETTER_NONCUSTOMER_ID'); //NONCUSTOMER list id
						}
						
						#Subscribe this email to sendy newsletter *Non-customer* list.
						#If the email already exist in sendy, its will be force to change the status unsubscribe to subscribe.
						$sendyparams['name'] = null; //name is optional
						$sendyparams['email'] = $newsletteremail;
						$sendyparams['boolean'] = true; //to allow the email send with plaintext mode 
						$sendyto = sendyto('subscribe', $sendyparams);
					}
				}
			}
		}

		//check whether query was successful
		if(!$query){
			$error=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}
		else{
			#clear form guardian session
			guardian('clear');
		
			if(!$js){
				header("location:/newsletter/registrasi/sukses/");
			}
			else{
			?>
				<script type="text/javascript">
					window.location = "/newsletter/registrasi/sukses/";
				</script>
				<?php
			}
		}
		exit;
	}
	
	
	if($status!='sukses'){
		header('location:/home/');
		exit;
	}
	
	#do not crawl this page
	$pagedata['googlebot'] = 'noindex, follow';
	$pagedata['robots'] = 'noindex, follow';
	
	#minify html
	ob_start('minify');
?>
<!DOCTYPE html>
<html lang="<?php print $active_lang;?>">
<head>
<?php include('com/com-meta.php');?>
<title><?php print _('newsletter_pagetitle');?> | <?php print SITE_NAME; ?></title>
<?php
$cssfiles = array(
	'megamenu.css',
	'normalize.css',
	'style.css',
	'style.color.css',
);?>
<link rel="stylesheet" type="text/css" href="/minify/?c=css&amp;f=<?php print urlencode(implode('|', $cssfiles));?>"/>
<?php print customscript();?>
<?php
$jsfiles = array(
	'jquery.js',
	'jquery.validate.js',
	'jquery.form.js',
	'jquery.caroufredsel.js',
	'validate.js',
	'megamenu_plugins.js',
	'megamenu.js',
	'function.web.js'
);?>
<script type="text/javascript" src="/minify/?c=js&f=<?php print urlencode(implode('|', $jsfiles));?>"></script>
<?php include ("com/com-extend-head.php");?>
</head>
<body>
<div id="result"></div>
<div id="wrapper">
	<?php include('com/com-header.php');?>
	<?php include('com/com-nav.php');?>
	<section>
		<div id="left"><?php include('com/com-sidebar.php');?></div>

		<div id="content">
			<div class="content-property">
				<div class="content-title"><h1><?php print _('newsletter_pagetitle');?></h1></div>
				<div class="linebar"></div>
			</div>
			<?php
			$content = getval('contentDesc','content',"contentId='13'"); 
			print output($content);
			?>
		</div>
	</section>
	<!-- end of section -->
	<?php include('com/com-footer.php');?>
</div>
<?php include ("com/com-extend-body.php");?>
<?php ganalytics();?>
</body>
</html>
<?php ob_end_flush();?>