<?php
    #includes all files necessary to support operations
    include("../modz/config-main.php");
    include("../modz/config.php");
    include("../modz/license.php");
    include("../modz/errormsg.php");
    include("../modz/mainmod.php");
    include("../modz/mainmod-extend.php");
    include("../modz/connic.php");
    include("../modz/getall.php");

    $page = 19;

	$latall="0.5278164";
	$longall="101.4423915";

    if($form=="submit"){
        #validate form guardian
        $fguard = guardian('validate');

        if(is_string($fguard)){
            #user pass, same key that already used before. we have to generate a new key and resubmit user form
            ?>
            <script type="text/javascript">
                formguard('#contact', '<?php print $fguard;?>');
                $('#contact').submit();
            </script>
            <?php
        }

        if (empty($name) or empty($email) or empty($hp) or empty($message)){
            $error=errorlist(2);
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $error.=errorlist(12);
        }


        if(!$error){
            $emailto=explode(",",SITE_EMAIL);
            foreach($emailto as $key => $emailvalue){
                $pesan="";
                $sentsubject="[Contact] Pesan dari " . $name;

                $pesan.=$message;
                $pesan.="<br /><br />regards,<br />";
                $pesan.=$name;
                $pesan.="<br />";
                $pesan.="Alamat ".$hp;
                $pesan.="<br />";
                $pesan.=$email;

                #send mail
                $option = array();
                $option['from'] = $email;
                $option['fromname'] = $name;
                $option['replyto'] = '';
                $option['cc'] = '';
                $option['bcc'] = '';
                $option['to'] = $emailvalue;
                $option['subject'] = $sentsubject;
                $option['message'] = $pesan;
                $option['messagetype'] = 'text';
                $sendmail = sendMailComplete($option);
            }
        }

        if ($error){
            print "<p>";
            print "<ul>";
            print nl2br($error);
            print "</ul>";
            print "</p>";
        }else{
            #clear form guardian session
            guardian('clear');

            if(!$js){
                header("location:/hubungi-kami/n/");
            }else{
                print "ok";
            }
        }
        exit;
    }

    #meta
    $sql = "SELECT * FROM content WHERE contentId='20'";
    $query = query($sql);
    $dataz = fetch($query);
    $dataz = locale($dataz,"content","contentId='".$dataz['contentId']."'");
    $dataz = output($dataz);
    $pagedata['description'] = output(stripquote($dataz['contentDescSeo']));
    $pagedata['keyword'] = output(stripquote($dataz['contentKeywordSeo']));

    #form guard
    $fguard = guardian('init', 'contactmessage');

    #minify html
    ob_start('');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include("com/com-meta.php"); ?>
<title><?php print _('web_contact')?> - <?php print SITE_NAME; ?> | <?php print SITE_TITLE; ?></title>
<link href="https://fonts.googleapis.com/css?family=Arimo:400,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Overpass:400,800,900" rel="stylesheet"> 
<?php
$cssfiles = array(
    'style-web.css',
    'media-screen.css',
    'bootstrap.css',
    'font-awesome.css',
    'owl.carousel.css',
    'owl.theme.css',
    'animasi.css'
);
?>
<link rel="stylesheet" type="text/css" href="<?php print filecache('render', $cssfiles, 'css');?>"/>
<?php
$jsfiles = array(
    'jquery.js',
    'bootstrap.min.js',
    'owl.carousel.js',
    'function.web.js',
    'jquery.validate.js',
    'jquery.form.js',
    'validateweb.js'
);?>
<script type="text/javascript" src="<?php print filecache('render', $jsfiles, 'js');?>"></script>
<script type="text/javascript">
$(document).ready(function(){
    validate('#result', '#contactmessage', '/hubungi-kami/n/', '<?php print $fguard;?>');
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
<?php
    include ("com/com-extend-head.php");
?>
</head>
<body>
    <div id="result"></div>
    <?php include ("com/com-menu.php"); ?>
    <section class="background5">
        <div class="container">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="breadcrumb-title text-center">
                        <h2>Hubungi Kami</h2>
                    </div>
                    <ol class="breadcrumb text-center">
                        <li><a href="/home">Home</a></li>
                        <li class="active">Hubungi Kami</li>
                    </ol>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </section>
        
            <p id="map" style="width:100%; height:328px;"></p>
        
    <section style="margin-top: 250px;">
        <div class="container">
            <div class="row">
                <?php
                if($commentmod=='n'){
                ?> <div class="col-md-12" style="margin-top:15px;">
                        <div class="alert alert-warning">
                            <div class="contactdesc" align="center">
                            <strong><?php print _('web_message_sent')?></strong>
                            </div>
                        </div>
                    </div>
                <?php
                }
                ?>
                <div class="col-sm-6 col-md-6" style="margin-bottom: 35px;">
                    <div class="breadcrumb-title">
                        <h4 class="box_header"><i class="fa fa-map-marker"></i> <?php print _('web_address')?></h4>
                    </div>
                    <?php
                     print $dataz['contentDesc'];
                     print "<br/>";
                     print getval("contentDesc","content","contentId='8'");
                     ?>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="breadcrumb-title">
                        <h4 class="box_header"><?php print _('web_contact_form')?></h4>
                    </div>
                     <form style="margin-top:10px;"  name="contactmessage" id="contactmessage" method="post" role="form" action="/web/contact.php?form=submit">              
                        <input type="text" name="ishuman" style="display:none;"/>
                        <div class="form-group">
                            <label for="name"><?php print _('web_name')?></label>
                                <input type="text" class="form-control" id="name" name="name" required="required">
                        </div>
                        <div class="form-group">
                            <label for="email"><?php print _('web_email')?></label>
                            <input type="email" class="form-control" id="email" name="email" required="required">
                        </div>
                        <div class="form-group">
                            <label for="phone"><?php print _('web_address_contact')?></label>
                            <input type="text" class="form-control" id="hp" name="hp" required="required">
                        </div>
                        <div class="form-group">
                            <label for="name"><?php print _('web_message')?></label>
                            <textarea name="message" id="message" class="form-control" required="required"></textarea>
                        </div>
                        <div class="form-group">
                              <button type="submit" class="btn btn-primary"><?php print _('web_send')?></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
<?php include ("com/com-footer.php");?>
<?php include ("com/com-extend-body.php");?>

<script>
    function initMap() {
       var myLatLng = {lat: <?php print $latall; ?>, lng: <?php print $longall; ?>};
            var directionsService = new google.maps.DirectionsService;
            var directionsDisplay = new google.maps.DirectionsRenderer;
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 16,
                center: myLatLng
            }); 
            var mercLatLng = {lat: <?php print $latall; ?>, lng: <?php print $longall; ?>};
            var marker = new google.maps.Marker({
                position: mercLatLng,
                map: map,
                title: "Lokasi",
            }); 
            if( (navigator.platform.indexOf("iPhone") != -1) 
                || (navigator.platform.indexOf("iPod") != -1)
                || (navigator.platform.indexOf("iPad") != -1))
                 var urlLoc = '<a class="btn btn-xs btn-warning" target="_blank" href="maps://maps.google.com/maps?daddr=<?php print $latall; ?>,<?php print $longall; ?>&amp;ll="><i class="fa fa-location-arrow" aria-hidden="true"></i>&nbsp;<?php print _('Get Directions'); ?></a>';
            else
                 var urlLoc = '<a class="btn btn-xs btn-warning" target="_blank" href="https://maps.google.com/maps?daddr=<?php print $latall; ?>,<?php print $longall; ?>&amp;ll="><i class="fa fa-location-arrow" aria-hidden="true"></i>&nbsp;<?php print _('Get Directions'); ?></a>';
             
            var ContetWin = '<div class="panel panel-body"><h5>tempat lokasi</h5>'+urlLoc+'</div>';
            var infowindow = new google.maps.InfoWindow({
              content: ContetWin
            }); 
            marker.addListener('click', function() {
              infowindow.open(map, marker);
            });
    }
    function calculateAndDisplayRoute(directionsService, directionsDisplay,position,destination) {
        directionsService.route({
          origin: position,
          destination: destination,
          travelMode: 'DRIVING'
        }, function(response, status) {
          if (status === 'OK') {
            directionsDisplay.setDirections(response);
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
    }
    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
          'Error: The Geolocation service failed.' :
          'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAiqlCyAjrLA3YAswfDvLGWL5tFL7S4L04&callback=initMap">
</script>
</body>
</html>
<?php ob_end_flush();?>