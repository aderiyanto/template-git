<div data-role="panel" data-position-fixed="true" data-theme="a" id="nav-panel">
	<ul data-role="listview" data-theme="a" data-divider-theme="a" data-icon="false" class="panel-menu">
		<li><a data-ajax="false" href="/produk/"><span class="panelmenu panelmenu-product-all"></span><?php print _('nav_allproduct');?></a></li>
		<li><a data-ajax="false" href="/produk/terbaru/"><span class="panelmenu panelmenu-product-new"></span><?php print _('nav_newestproduct');?></a></li>
		<li><a data-ajax="false" href="/produk/terlaris/"><span class="panelmenu panelmenu-product-demand"></span><?php print _('nav_demandproduct');?></a></li>
		<!--<li><div class="panelmenu panelmenu-product-favorite"></div><a data-ajax="false" href="/produk/terpopuler/"><?php print _('nav_popularproduct');?></a></li>-->
		<li><a href="/bantuan/"><span class="panelmenu panelmenu-help"></span><?php print _('nav_help');?></a></li>
		<?php 
		if(lcauth(37)){ 
			?><li><a data-ajax="false" href="/blog/"><span class="panelmenu panelmenu-blog"></span><?php print _('nav_blog');?></a></li><?php
		}
		
		if(lcauth(36)){ 
			?><li><a data-ajax="false" href="/galeri-foto/"><span class="panelmenu panelmenu-imagegallery"></span><?php print _('nav_imagegallery');?></a></li><?php
		}
		
		if(lcauth(39)){ 
			?><li><a data-ajax="false" href="/video/"><span class="panelmenu panelmenu-video"></span><?php print _('nav_video');?></a></li><?php
		}

		if(lcauth(14)){ 
			?><li><a href="/testimonial/"><span class="panelmenu panelmenu-testi"></span><?php print _('nav_testi');?></a></li><?php
		}?>
		
		<li><a href="/about-us/"><span class="panelmenu panelmenu-about"></span><?php print _('nav_aboutus');?></a></li>
		<li><a data-ajax="false" href="/contact/"><span class="panelmenu panelmenu-contact"></span><?php print _('nav_contact');?></a></li>
		<?php
		if(lcauth('addpage')){
			$sql = "SELECT * FROM content WHERE contentStatus='added' AND contentDisplay='y' ORDER BY contentTitle ASC";
			$query = query($sql);
			if(rows($query)){
				?><li><a data-ajax="false" href="/page/"><span class="panelmenu panelmenu-morepage"></span><?php print _('nav_more');?></a></li><?php
			}
		}
		?>
	</ul>
</div>
<div data-role="panel" data-position-fixed="true" data-theme="a" data-position="right" id="action-panel"></div>