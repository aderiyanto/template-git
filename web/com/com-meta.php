<meta charset="UTF-8" />
<meta name="description" content="<?php print $pagedata['description'] ? $pagedata['description']:SITE_DESCRIPTION; ?>" />
<meta name="keywords" content="<?php print $pagedata['keyword'] ? $pagedata['keyword']:SITE_KEYWORD; ?>" />
<meta name="copyright" content="2013. <?php print SITE_NAME; ?>. All rights reserved." />
<meta name="robots" content="<?php print $pagedata['robots'] ? $pagedata['robots']:'index,follow';?>" />
<meta name="googlebot" content="<?php print $pagedata['googlebot'] ? $pagedata['googlebot']:'index,follow';?>" />
<meta name="rating" content="general" />
<link rel="shortcut icon" href="/assets/interface/favicon.ico" />
<!--[if lt IE 9]><script type="text/javascript" src="/javascript/html5.js"></script><![endif]-->