<?php
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");	
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall.php");
	
	#change language
	$ref = $_SERVER['HTTP_REFERER'];
	if(in_array(cleanup($_GET['lang']), $lc_lang)){
		setcookie("lang",cleanup($_GET['lang']),0,"/");
	}

	if($ref){
		header('location:'.$ref);
	}
	else{
		header('location:/home/');
	}
	exit;
?>