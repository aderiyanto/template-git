<?php
	ob_start();
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/mainmod-extend.php");
	include("../modz/connic.php");
	include("../modz/getall.php");
	include("../modz/dompdf/dompdf_config.inc.php");
	
	$urlquery = geturlquery();
	$urlquery = $urlquery['query'];

	$fsource = strdecode(urldecode($urlquery['fsource']));
	$fname = strdecode(urldecode($urlquery['fname']));
	$fstream = ($urlquery['fstream']==1 ? FALSE:TRUE);
	
	if(substr($fsource, 0, 7) != 'http://'){
		if(!file_exists($fsource)){
			print 'invalid data source';
			exit;
		}
	}
	
	$dompdf = new DOMPDF();
	$dompdf->load_html_file($fsource);
	$dompdf->render();
	$dompdf->stream($fname,array('Attachment'=>$fstream));
?>