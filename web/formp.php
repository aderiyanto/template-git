<?php
//includes all files necessary to support operations
include("../modz/config-main.php");
include("../modz/config.php");
include("../modz/license.php");
include("../modz/errormsg.php");
include("../modz/mainmod.php");
include("../modz/connic.php");
include("../modz/getall.php");
if($form == 'submit'){
	#validate form guardian
	$fguard = guardian('validate');

	if(is_string($fguard)){
		#user pass, same key that already used before. we have to generate a new key and resubmit user form
		?>
		<script type="text/javascript">
			formguard('#add', '<?php print $fguard;?>');
			$('#add').submit();
		</script>
		<?php
		exit;
	}

	#get visitor ip address, it's valid ip or not
	$ipAddress = visitor_ip();

	#check that news id is valid
	$exist = countdata("newsinfo", "newsId='$id' AND newsAllowComment='y'");
	if (!$exist){
		#this is bad user, set temorary or just exit here
		exit;
	}

	#validate user input
	if (empty($name) or empty($email) or empty($message)){
		$error = errorlist(2);
	} elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)){
		$error = errorlist(16);
	}

	#replace any <br> tag into \r\n
	$message = preg_replace("#<br[[:space:]]*/?"."[[:space:]]*>#", "\n", trim(nl2br($message)));
	
	$split = array_filter(explode("\n",$message));
	if(count($split) > 3){
		$message = null;
		foreach($split as $key=>$value){
			$message .= trim($value);
			if($key <= 2){
				$message .= "\n";
			} else {
				$message .= ' ';
			}
		}
	}

	#shorten multiple whitespace squence
	$message = preg_replace("/(\s)+/s", "\\1", $message);

	#check comment length must greaher than 10 character
	if(strlen(trim($message)) < 10){
		$error = errorlist(36);
	}

	#check comment length, it's should less than 500 character
	if(strlen($message) > 500){
		$error = errorlist(41);
	}

	#check for double posting
	$sql = "SELECT COUNT(cId) numOfComment FROM newsinfo_comment WHERE newsId='$id' AND cEmail='$email' AND cName='$name' AND cContent='$message'";
	$query = query($sql);
	$data = fetch($query);
	
	if($data['numOfComment'] > 1){
		#this request mark as same, lets do something
		$error = errorlist(37);
	}

	#check last posting time interval
	$sql = "SELECT MAX(cDateAdded) as lastReq FROM newsinfo_comment WHERE newsId='$id' AND cIpAddress='$ipAddress' AND cEmail='$email'";
	$query = query($sql);
	$data = fetch($query);

	if($data['lastReq']){
		$reqInterval = $now - $data['lastReq'];
		if($reqInterval < 60){
			#request less than minimum required interval
			$error = errorlist(38);
		}
	}

	if (!$error){
		#detect comment moderation
		$commod = COMMENT_MOD_BLOG;
		if ($commod == 'off'){
			$is_publish = 'y';
		} else {
			$is_publish = 'n';
		}
		
		#mark as spam
		$is_spam = 'n';
		if(is_spam($message)){
			$is_spam = 'y';
			$is_publish = 'n';
		}
		
		#check webbot status
		$wbStatus = getval("wbStatus", "webbot", "wbIpAddress", $ipAddress);
		if($wbStatus == 'deny'){
			$is_publish = 'n';
		}
		
		$sql = "INSERT INTO newsinfo_comment VALUES(null, '$id', '$email', '$name', '$message', '$now', '$is_publish', '$is_spam', '$ipAddress', 0, 'guest')";
		$query = query($sql);
	}

	#check whether query was successful
	if (!$query){
		$error = errorlist(3);
	}

	if ($error){
		print '<ul>'.nl2br($error).'</ul>';
	} else {
		#clear form guardian session
		guardian('clear');

		#set output
		print "ok";
	}
}
?>