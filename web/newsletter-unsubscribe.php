<?php
	#includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/mainmod-extend.php");
	include("../modz/connic.php");
	include("../modz/getall.php");

	$page = 'newsletter';
	
	//do verification here
	$sql = "SELECT COUNT(subId) AS is_exist, subEmail, subIsMember FROM newsletter_subscriber WHERE subId='$id' AND subCode='$code'";
	$query = query($sql);
	$data = fetch($query);
	
	if($data['is_exist'] > 0){
		$status=14;
		$sql="UPDATE newsletter_subscriber SET subActive='n' WHERE subId='" . $id . "'";
		$query=query($sql);
		
		if($query){
			$sql="DELETE FROM newsletter_tmp WHERE subId='" . $id . "'";
			$query=query($sql);
		}else{
			$status=15;
		}
		
		$sendyparams['email']= $data['subEmail'];
		$sendyparams['list'] = $data['subIsMember'] == 'n' ? getconfig('SENDY_NEWSLETTER_NONCUSTOMER_ID') : getconfig('SENDY_NEWSLETTER_NONCUSTOMER_ID');  
		$sendyparams['boolean'] = true; //to allow the email send with plaintext mode 
		$sendyto = sendyto('unsubscribe',$sendyparams);
	}
	else{
		$status=15;
	}
	 
	if($mobile){
	 	$permalinkcontent=getval('contentPermalink','content','contentId',$status);
		$mobileuri='http://'. getconfig('MOBILE_URL') .'/page/'.$permalinkcontent.'/';
		header("location:$mobileuri");
		exit;
	}
	#do not crawl this page
	$pagedata['googlebot'] = 'noindex, follow';
	$pagedata['robots'] = 'noindex, follow';
	
	#minify html
	ob_start('minify');
?>
<!DOCTYPE html>
<html lang="<?php print $active_lang;?>">
<head>
<?php include('com/com-meta.php');?>
<title><?php print _('newsletter_pagetitle');?> | <?php print SITE_NAME; ?></title>
<?php
$cssfiles = array(
	'megamenu.css',
	'normalize.css',
	'style.css',
	'style.color.css'
);?>
<link rel="stylesheet" type="text/css" href="/minify/?c=css&amp;f=<?php print urlencode(implode('|', $cssfiles));?>"/>
<?php print customscript();?>
<?php
$jsfiles = array(
	'jquery.js',
	'jquery.validate.js',
	'jquery.form.js',
	'jquery.caroufredsel.js',
	'validate.js',
	'megamenu_plugins.js',
	'megamenu.js',
	'function.web.js'
);?>
<script type="text/javascript" src="/minify/?c=js&f=<?php print urlencode(implode('|', $jsfiles));?>"></script>
</head>
<body>
<div id="result"></div>
<div id="wrapper">
	<?php include('com/com-header.php');?>
	<?php include('com/com-nav.php');?>
	<section>
		<div id="left"><?php include('com/com-sidebar.php');?></div>

		<div id="content">
			<div class="content-property">
				<div class="content-title"><h1><?php print _('newsletter_pagetitle');?></h1></div>
				<div class="linebar"></div>
			</div>
			<?php 
				$content = getval('contentDesc','content','contentId',$status); 
				print output($content);
			?>
		</div>
	</section>
	<!-- end of section -->
	<?php include('com/com-footer.php');?>
</div>
<?php include ("com/com-extend-body.php");?>
<?php ganalytics();?>
</body>
</html>
<?php ob_end_flush();?>