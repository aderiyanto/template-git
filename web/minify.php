<?php
	#give page index to exclude this request tracking by getall.php
	$page = 'minify';

	#includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall.php");
	
	if($c == 'css'){
		#specify output header
		header("Content-type: text/css");
		
		#how long this file will be cached in client web browser
		$cache_duration = 604800; #a week
		
		#specify cache filename, end filename with real suffix for further investigation purpose
		$cache_name = md5(urlencode(strtolower($_GET['f']))).'.css';
		
		#check if cache already exists
		if(filecache('check', $cache_name)){
			#read file cache and direcly display output to client
			filecache('read', $cache_name, null, $cache_duration);
		}
		else{
			#start minify css code
			$content = '';
			$cssfiles = explode('|', $f);
			foreach($cssfiles as $key=>$value){
				$ext = end(explode('.', $value));
				$fpath = '../style/'.cleanup($value);
				if(!file_exists($fpath) or $ext != 'css'){
					continue;
				}

				$content .= trim(file_get_contents($fpath));
			}
			
			#replace any avaliable image source width static url if any
			if(getconfig('ASSETS_URL') != '' and getconfig('ASSETS_URL') != '/assets' and getconfig('ASSETS_URL') != 'http://'.getconfig('SITE_URL').'/assets'){
				$content = preg_replace('/src\=\"(\/assets|\.\.\/assets)/','src="'.getconfig('ASSETS_URL'), $content);
			}

			#create new cache file
			filecache('write', $cache_name, minify_css($content) , $cache_duration);
		}
	} else {
		#specify output header
		header("Content-type: application/javascript");
		
		#how long this file will be cached in client web browser
		$cache_duration = 604800; #a week
		
		#specify cache filename, end filename with real suffix for further investigation purpose
		$cache_name = md5(urlencode(strtolower($_GET['f']))).'.js';
		
		#check if cache already exists
		if(filecache('check', $cache_name)){
			#read file cache and direcly display output to client
			filecache('read', $cache_name);
		}
		else{
			#start minify js code
			$content = '';
			$jsfiles = explode('|', $f);
			foreach($jsfiles as $key=>$value){
				$ext = end(explode('.', $value));
				$fpath = '../javascript/'.cleanup($value);
				if(!file_exists($fpath) or $ext != 'js'){
					continue;
				}

				$content .= trim(file_get_contents($fpath));
				if(substr($content, -1, 1) != ';'){
					$content .= ';';
				}
			}
			
			#replace any avaliable image source width static url if any
			if(getconfig('ASSETS_URL') != '' and getconfig('ASSETS_URL') != '/assets' and getconfig('ASSETS_URL') != 'http://'.getconfig('SITE_URL').'/assets'){
				$content = preg_replace('/src\=\"(\/assets|\.\.\/assets)/','src="'.getconfig('ASSETS_URL'), $content);

				#facebox
				$content = preg_replace('/\/assets\/facebox/',getconfig('ASSETS_URL').'/facebox', $content);
			}

			#create new cache file
			filecache('write', $cache_name, minify_js($content) , $cache_duration);
		}
	}
?>