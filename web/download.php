<?php
	//includes all files necessary to support operations
	include("../modz/config-main.php");	
	include("../modz/config.php");	
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/mainmod-extend.php");
	include("../modz/connic.php");
	include("../modz/getall.php");
 
	/*
		This file used by file manager to allow user downloading file and will increase the download counter for 
		every hit.
	*/
	
	if(!empty($uri)){	
	 	$sql="SELECT * FROM file_manager WHERE fileId='".$uri."'";
		$query=query($sql);
		$data=fetch($query);
		
		//Increase counter
	 	$sql="UPDATE file_manager SET fileCounter=fileCounter+1 WHERE fileId='$uri'"; 
		$query=query($sql);
		
		$fullpath="../assets/fileman/".$data['fileDir']."/".$data['fileName'];
		$type="application/octet-stream";
		header("Cache-Control: public, must-revalidate");
		header("Pragma: public");
		header("Content-Type: " . $type);
		header("Content-Length: " .(string)(filesize($fullpath)));
		header('Content-Disposition: attachment; filename="'.$data['fileName'].'"');
		header("Content-Transfer-Encoding: binary\n");								
		
		readfile($fullpath);
	}else{
		//If user access file directly or file unavailable
		header("location:/");
	}
?>