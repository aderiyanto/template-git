<?php
session_start();
//session_destroy();

//includes all files necessary to support operations
include("../modz/config-main.php");
include("../modz/config.php");
include("../modz/license.php");
include("../modz/errormsg.php");
include("../modz/mainmod.php");
include("../modz/mainmod-extend.php");
include("../modz/connic.php");
include("../modz/getall.php");

#form guard
$fguard = guardian('init', 'newscomment');
?>
<html>
<head>
<title>Form komentar</title>
<?php
$cssfiles = array(
	'style.css'
);?>
<link rel="stylesheet" type="text/css" href="/minify/?c=css&amp;f=<?php print urlencode(implode('|', $cssfiles));?>"/>
<?php
$jsfiles = array(
	'jquery.js',
	'function.web.js',
    'jquery.validate.js',
    'jquery.form.js',
    'validateweb.js'
);
?>
<script type="text/javascript" src="/minify/?c=js&f=<?php print urlencode(implode('|', $jsfiles));?>"></script>
<script>
$(document).ready(function(){
	validate('#result', '#add', 'form.php?a=b', '<?php print $fguard;?>');
});
</script>
<?php include ("com/com-extend-head.php");?>
</head>
<body>
<noscript class="noscript"><?php print $msg[35];?></noscript>
<div id="result"></div>
<form name="add" id="add" class="myForm" method="post" action="formp.php?form=submit">
	<input type="hidden" name="id" value="1" />
	<table cellpadding="5">
		<tr>
			<td width="170" valign="top"><h4><?php print _('news_detail_comment_name');?></h4></td>
			<td width="1" valign="top">:</td>
			<td ><input type="text" name="name" id="name" class="required" size="30" maxlength="255" /></td>
		</tr>
		<tr>
			<td valign="top"><h4><?php print _('news_detail_comment_email');?></h4></td>
			<td valign="top">:</td>
			<td><input type="text" name="email" id="email" class="required email" size="30" maxlength="255" /></td>
		</tr>
		<tr>
			<td valign="top"><h4><?php print _('news_detail_comment_msg');?></h4></td>
			<td valign="top">:</td>
			<td ><textarea name="message" class="required" id="message" rows="4" cols="60"></textarea></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td >&nbsp;</td>
			<td><input type="submit" id="submit" name="Submit" value="<?php print _('news_detail_comment_sendbtn');?>" class="btn btn-primary" />&nbsp;<input type="reset" value="<?php print _('news_detail_comment_resetbtn');?>"  class="btn" /></td>
		</tr>
	</table>
</form>
<?php include ("com/com-extend-body.php");?>
</body>
</html>
<?php ob_end_flush();?>