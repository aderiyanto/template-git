<?php
	
	#includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall.php");

	$page = 'pagecontent';
	
	$sql = "SELECT * FROM content WHERE contentPermalink='$permalink'";
	$query = query($sql);
	$dataz = fetch($query);
	$dataz = locale($dataz,"content","contentId='".$dataz['contentId']."'");
	$dataz = output($dataz);
	$pagedata['description'] = output(stripquote($dataz['contentDescSeo']));
	$pagedata['keyword'] = output(stripquote($dataz['contentKeywordSeo']));
	
	#minify html
	ob_start('minify');
?>
<!DOCTYPE html>
<html lang="<?php print $active_lang;?>">
<head>
<?php include('com/com-meta.php');?>
<title><?php print $dataz['contentTitle'];?> | <?php print SITE_NAME; ?></title>
<?php
$cssfiles = array(
	'style.css',
	'style.color.css'
);?>
<link rel="stylesheet" type="text/css" href="/minify/?c=css&amp;f=<?php print urlencode(implode('|', $cssfiles));?>"/>
<?php print customscript();?>
<?php
$jsfiles = array(
	'jquery.js',
	'jquery.validate.js',
	'jquery.form.js',
	'validate.js',
	'function.web.js'
);?>
<script type="text/javascript" src="/minify/?c=js&f=<?php print urlencode(implode('|', $jsfiles));?>"></script>
<?php include ("com/com-extend-head.php");?>
</head>
<body>
<div id="result"></div>
<div id="wrapper">
	<?php include('com/com-header.php');?>
	<?php include('com/com-nav.php');?>
	<section>
		<div id="left"><?php include('com/com-sidebar.php');?></div>

		<div id="content">
			<div class="content-property">
				<div class="content-title"><h1><?php print $dataz['contentTitle'];?></h1></div>
				<div class="linebar"></div>
			</div>
			<?php print $dataz['contentDesc'];?>
		</div>
	</section>
	<!-- end of section -->
	<?php include('com/com-footer.php');?>
</div>
<?php include ("com/com-extend-body.php");?>
<?php ganalytics();?>
</body>
</html>
<?php ob_end_flush();?>