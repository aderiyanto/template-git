<?php
/*
* This file was created on 21/09/2015
* Include the extend function here
*/

// Export Import Excel Library Function
function exExcelLibs (){
	return require_once( '../modz/extoexcel/PHPExcel.php' );
	return require_once( '../modz/extoexcel/PHPExcel/Writer/Excel2007.php' );
}
function imExcelLibs (){
	return require_once( '../modz/extoexcel/PHPExcel/IOFactory.php' );
}

function dateformat($tstamp,$desc=NULL){
	if($desc==NULL){
		$multilang=getconfig('MULTILANGUAGE_CMS');
		if($_COOKIE['lang_admin'] AND in_array($_COOKIE['lang_admin'],$multilang)){
			$lang = $_COOKIE['lang_admin'];
		}
		else{
			$lang = $multilang[0];
		}
		
		if($lang=="id_ID"){
			$date=date('d',$tstamp)." ".convertmonthtolang(date('m',$tstamp))." ".date('Y',$tstamp);
		}elseif($lang=="en_US"){
			$date=convertmonthtolang(date('m',$tstamp))." ".date('d',$tstamp).", ".date('Y',$tstamp);
		}
	}elseif($desc=="day"){
		$multilang=getconfig('MULTILANGUAGE_CMS');
		if($_COOKIE['lang_admin'] AND in_array($_COOKIE['lang_admin'],$multilang)){
			$lang = $_COOKIE['lang_admin'];
		}
		else{
			$lang = $multilang[0];
		}
		
		if($lang=="id_ID"){
			$date=convertdaytolang(date('l',$tstamp)).", ".date('d',$tstamp)." ".convertmonthtolang(date('m',$tstamp))." ".date('Y',$tstamp);
		}elseif($lang=="en_US"){
			$date=convertdaytolang(date('l',$tstamp)).", ".convertmonthtolang(date('m',$tstamp))." ".date('d',$tstamp).", ".date('Y',$tstamp);
		}
	}elseif($desc=="full"){
		$multilang=getconfig('MULTILANGUAGE_CMS');
		if($_COOKIE['lang_admin'] AND in_array($_COOKIE['lang_admin'],$multilang)){
			$lang = $_COOKIE['lang_admin'];
		}
		else{
			$lang = $multilang[0];
		}
		
		if($lang=="id_ID"){
			$date=convertdaytolang(date('l',$tstamp)).", ".date('d',$tstamp)." ".convertmonthtolang(date('m',$tstamp))." ".date('Y',$tstamp)." ".date('H:i',$tstamp);
		}elseif($lang=="en_US"){
			$date=convertdaytolang(date('l',$tstamp)).", ".convertmonthtolang(date('m',$tstamp))." ".date('d',$tstamp).", ".date('Y',$tstamp)." ".date('g:i A',$tstamp);
		}
	}elseif($desc=="time"){
		$multilang=getconfig('MULTILANGUAGE_CMS');
		if($_COOKIE['lang_admin'] AND in_array($_COOKIE['lang_admin'],$multilang)){
			$lang = $_COOKIE['lang_admin'];
		}
		else{
			$lang = $multilang[0];
		}
		
		if($lang=="id_ID"){
			$date=date('d',$tstamp)." ".convertmonthtolang(date('m',$tstamp))." ".date('Y',$tstamp)." ".date("H:i",$tstamp);
		}elseif($lang=="en_US"){
			$date=convertmonthtolang(date('m',$tstamp))." ".date('d',$tstamp).", ".date('Y',$tstamp)." ".date('g:i A',$tstamp);
		}
	}
	
	return $date;
}

//function mobile notification
function sendpushnotification($tokens, $notification, $custom = array(),$to) {
		 if($to=="customer"){
		    define('FIREBASE_SERVER_KEY', getconfig('FIREBASE_SERVER_KEY'));
		 }else{
		    define('FIREBASE_SERVER_KEY', getconfig('FIREBASE_SERVER_KEY_SALES'));
		 }
		 $url = 'https://fcm.googleapis.com/fcm/send';

		 $fields = array
		 (
		 'registration_ids' => $tokens,
		 'data' => $custom,
		 'priority' => 'high'
		 );
		 $headers = array(
		 'Authorization: key=' . FIREBASE_SERVER_KEY,
		 'Content-Type: application/json'
		 );
		 
		 $ch = curl_init();
		 curl_setopt($ch, CURLOPT_URL, $url);
		 curl_setopt($ch, CURLOPT_POST, true);
		 curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		 curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
		 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		 curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		 $result = curl_exec($ch);           
		 if ($result === FALSE) {
		 die('Curl failed: ' . curl_error($ch));
		 }
		 curl_close($ch);
		 return $result;
	
}
function endek($encrypt_decrypt,$string){
    	$password = getconfig('PASSWORD_ENCRYPT');
    	$method = 'aes-128-cfb';
    	$iv = substr(hash('sha1', $password), 0, 16);
    	$output='';
    if($encrypt_decrypt=='encrypt'){
        $output = openssl_encrypt($string, $method, $password, 0, $iv);
        $output = base64_encode($output);
   } else if($encrypt_decrypt=='decrypt'){
        $output = base64_decode($string);
        $output = openssl_decrypt($output, $method, $password, 0, $iv);
   }
   return $output;
}
//---------------------------------------------------fungsi cek point--------------------------------
function checkpointGen($table, $columnId, $id) {
	$sqlcek=query("SELECT checkPoint FROM $table");
	$db=getconfig('DB_NAME');
	if (!$sqlcek){
		$hasil=FALSE;
	}else{
	$sql=query("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '$db' and TABLE_NAME = '$table' and COLUMN_NAME<>'checkPoint'");
	$no=0;
	while($data=fetch($sql)){
		$query=query("select ".$data['COLUMN_NAME']." from $table where $columnId='$id'");
		 while($dataisi=fetch($query)){
			$datax[$no][$data['COLUMN_NAME']]= $dataisi[$data['COLUMN_NAME']];
		}
		$no++;
	}
		$text_json=json_encode($datax);
		$cp = endek('encrypt',$text_json);
		$update=query("update $table set checkPoint='$cp' where $columnId='$id'");
		$hasil=TRUE;
	}
	return $hasil;
}
function checkpoint($table, $columnId, $id) {
	$sqlcek=query("SELECT checkPoint FROM $table");
	$db=getconfig('DB_NAME');
	if (!$sqlcek){
		$hasil=FALSE;
	}else{
	$sql=query("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '$db' and TABLE_NAME = '$table' and COLUMN_NAME<>'checkPoint'");
	$no=0;
	while($data=fetch($sql)){
		$query=query("select ".$data['COLUMN_NAME']." from $table where $columnId='$id'");
		 while($dataisi=fetch($query)){
			$datax[$no][$data['COLUMN_NAME']]= $dataisi[$data['COLUMN_NAME']];
		}
		$no++;
	}
		$sqlcp="select checkPoint from $table where $columnId='$id'";
	   	$querycp=query($sqlcp);
	   	$datacp=fetch($querycp);

		$text_json=json_encode($datax);
		$cp = endek('encrypt',$text_json);

		if ($cp==$datacp['checkPoint']){
            	//$sql="UPDATE storesetting SET setValue='on' WHERE setId=6";
				//$query=query($sql);
		}else {
				$sql="UPDATE storesetting SET setValue='off' WHERE setId=6";
				$query=query($sql);

				$sql="SELECT setVar, setValue FROM storesetting";
				$query=query($sql);
				$written.="\n";
				while ($data=fetch($query)){
					$data=output($data);
					$written.="define(\"" . $data['setVar'] . "\",\"" . $data['setValue'] . "\");\n";
				}

				$writtens.="<?php \nerror_reporting(0);\n";
				$writtens.="date_default_timezone_set(\"Asia/Jakarta\"); \n";
				$writtens.='$now=time();';
				$writtens.="\n";
				$writtens.=stripslashes($written) . "?>";

				//open config.php
				$file="../modz/config.php";
				chmod($file, 0777);
				$fp=fopen($file,"w");
				fwrite($fp,$writtens);
				fclose($fp);
				chmod($file, 0644);

				//----------------------------------KIRIM EMAIL KE DEVELOPER --------------------------
				$option = array();
				$option['from'] = SITE_EMAIL;
				$option['mailopt'] = 'queue';
				$option['fromname'] = SITE_NAME;
				$option['replyto'] = SITE_EMAIL;
				$option['cc'] = '';
				$option['bcc'] = '';
				$option['to'] = SITE_DEVELOPER_EMAIL;
				$option['subject'] = "DATA ERROR";
				$option['message'] = '<img src="http://'.getconfig(SITE_URL).NEWSLETTER_HEADER.'" title="Logo" alt="logo" />"<br />Pada tabel <b> $table </b> terdapat data yang invalid di  $columnId : <b> $id </b>';
				$option['messagetype'] = 'text';
				$sendmail = sendMailComplete($option);
               }
		$hasil=TRUE;
	}
	return $cp;
}

/*function format_event_date($stardate,$enddate){
	$starttime=date('d-m-Y-H:i',$stardate);
	list($days,$months,$years,$times)=explode('-',$starttime);
	$endtime=date('d-m-Y-H:i',$enddate);
	list($daye,$monthe,$yeare,$timee)=explode('-',$endtime);
	$showdate="";
	if($starttime==$endtime){
		$monthms=convertmonthtoidshort($months);
		$showdate=$days." ".$monthms." ".$years." ".$times;
	}elseif($months==$monthe and $years==$yeare and $times==$timee){
		$monthms=convertmonthtoidshort($months);
		$showdate=$days." - ".$daye." ".$monthms." ".$years." ".$times;
	}elseif($months==$monthe and $years==$yeare){
		$monthms=convertmonthtoidshort($months);
		$showdate=$days." - ".$daye."  ".$monthms." ".$years." ".$times."-".$timee;
	}elseif($years==$yeare and $times==$timee){
		$monthms=convertmonthtoidshort($months);
		$showdate=$days." ".$monthms." - ".$daye." ".$monthms." ".$years." ".$times;
	}elseif ($times=$timee){
		$monthms=convertmonthtoidshort($months);
		$monthme=convertmonthtoidshort($monthe);
		$showdate=$days." ".$monthms." - ".$daye." ".$monthme." ".$yeare." ".$timee;
	}else{
		$monthms=convertmonthtoidshort($months);
		$monthme=convertmonthtoidshort($monthe);
		$showdate=$days." ".$monthms." - ".$daye." ".$monthme." ".$yeare." ".$times."-".$timee;
	}
	return $showdate;
}*/
function format_event_date($stardate,$enddate){
	$starttime=date('d-m-Y',$stardate);
	list($days,$months,$years,$times)=explode('-',$starttime);
	$endtime=date('d-m-Y',$enddate);
	list($daye,$monthe,$yeare,$timee)=explode('-',$endtime);
	$showdate="";
	if($starttime==$endtime){
		$monthms=convertmonthtoidshort($months);
		$showdate=$days." ".$monthms." ".$years." ";
	}elseif($months==$monthe and $years==$yeare){
		$monthms=convertmonthtoidshort($months);
		$showdate=$days." - ".$daye." ".$monthms." ".$years;
	}elseif($years==$yeare){
		$monthms=convertmonthtoidshort($months);
		$showdate=$days." ".$monthms." - ".$daye." ".$monthms." ".$years;
	}else{
		$monthms=convertmonthtoidshort($months);
		$monthme=convertmonthtoidshort($monthe);
		$showdate=$days." ".$monthms." - ".$daye." ".$monthme." ".$yeare;
	}
	return $showdate;
}

function format_event_date_time($startdate,$enddate){
	$starttime=date('d m Y H:i',$startdate);
	list($days,$months,$years,$times)=explode(' ',$starttime);
	$endtime=date('d m Y H:i',$enddate);
	list($daye,$monthe,$yeare,$timee)=explode(' ',$endtime);
	$showdate="";
	//$showdate=$starttime." - ".$endtime;
	if($starttime==$endtime){
		//$monthms=convertmonthtoid($months);
		$showdate=$times;
	}elseif($days==$daye and $months==$monthe and $years==$yeare){
		$showdate=$times." - ".$timee;
	}else{
		$showdate=$times." - ".$timee;
	}

	return $showdate;
}

function datediff($tgl1, $tgl2){
   $diff_secs = abs($tgl1 - $tgl2);
    $base_year = min(date("Y", $tgl1), date("Y", $tgl2));
    $diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);

    $fullago= array( "years" => date("Y", $diff) - $base_year, "months_total" => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1, "months" => date("n", $diff) - 1, "days_total" => floor($diff_secs / (3600 * 24)), "days" => date("j", $diff) - 1, "hours_total" => floor($diff_secs / 3600), "hours" => date("G", $diff), "minutes_total" => floor($diff_secs / 60), "minutes" => (int) date("i", $diff), "seconds_total" => $diff_secs, "seconds" => (int) date("s", $diff) );
    if($fullago['years']){
      $result=$fullago['years']." "._("year")." ";
    }
    if($fullago['months']){
      $result.=$fullago['months']." "._("month")." ";
    }
    if($fullago['days']){
      $result.=$fullago['days']." "._("day")." ";
    }
    if($fullago['hours']){
      $result.=$fullago['hours']." "._("hour")." ";
    }
    if($fullago['minutes']){
      $result.=$fullago['minutes']." "._("minute")." ";
    }
    if($fullago['seconds']){
      $result.=$fullago['seconds']." "._("second")." ";
    }
    return $result;
}
function send_sms($to,$text,$opt,$smsid=null){
	if (empty($opt)){
		$opt=getconfig('SMS_OPTION');
	}
	global $now;
	#example $to="0812123456789,0852123456789,0819123456789";
	#$text     ="Everything";
	#$smsid     =value id; if sms has been queue before!
	$return=true;

	if(empty($to)){
		$return=false;
	}
    $pecah              = explode(",",$to);
    $jumlah             = count($pecah);
    $from               = getconfig('SMS_API_FROM');//Sender ID or SMS Masking Name, if leave blank, it will use default from telco
    $username           = getconfig('SMS_API_USERNAME'); //your smsviro username
    $password           = getconfig('SMS_API_PASSWORD'); //your smsviro password
    $postUrl            = "http://107.20.199.106/restapi/sms/1/text/advanced"; # DO NOT CHANGE THIS
    
    // getnotify sms
    $notifyUrl          = getconfig('SMS_API_NOTIF_URL'); //notifyurl
    $notifyContentType  = "application/json";
    $callbackData       = getconfig('SMS_API_NOTIF_URL'); 
	
	
	$day = date("d",$now);
	$month = date("m",$now);
	$year = date("Y",$now);
	$dateen = date("Y-m-d",$now);
	$timenow = date("H:i:s",$now);

	if ($return==true){
	    for($i=0; $i<$jumlah; $i++){
	        if(substr($pecah[$i],0,2) == "62" || substr($pecah[$i],0,3) == "+62"){
	            $pecah = $pecah;
	        }elseif(substr($pecah[$i],0,1) == "0"){
	            $pecah[$i][0] = "X";
	            $pecah = str_replace("X", "62", $pecah);
	        }else{
	            // Invalid mobile number format
	            $return=false;
	        }
			if ($return==true){
				if($opt=="queue"){
					$is_exist = countdata("sms","smsTo='{$pecah[$i]}' AND smsText='{$text}' and smsDate='$dateen'"); 
					if($is_exist<1){
						$nextsmsid = nextid("smsId","sms");
						$sql = "INSERT INTO sms VALUES ('{$nextsmsid}','','{$pecah[$i]}','{$text}','{$day}','{$month}','{$year}','{$dateen}','{$timenow}','{$now}','','new')";
						$query = query($sql);
						if (!$query){
							$return=false;
						}
					}
				}else{
					if(!$smsid){
						$is_exist = countdata("sms","smsTo='{$pecah[$i]}' AND smsText='{$text}' and smsDate='$dateen'"); 
						if($is_exist<1){
							$nextsmsid = nextid("smsId","sms");
							$sql = "INSERT INTO sms VALUES ('{$nextsmsid}','','{$pecah[$i]}','{$text}','{$day}','{$month}','{$year}','{$dateen}','{$timenow}','{$now}','','sent')";
							$query = query($sql);
							if (!$query){
								$return=false;
							}
						}
					}
					if(getconfig('SMS_DEVELOPER_MODE')==false){
						$destination = array("to" => $pecah[$i]);
						$message     = array("from" => $from,
											 "destinations" => $destination,
											 "text" => $text,
											 "intermediateReport" => true,
											 "notifyUrl" => $notifyUrl,
											 "smsCount" => 2,
											 "notifyContentType" => $notifyContentType,
											 "callbackData" => $callbackData);
						
						$postData           = array("messages" => array($message));
						$postDataJson       = json_encode($postData);
						$ch                 = curl_init();
						$header             = array("Content-Type:application/json", "Accept:application/json");
						
						curl_setopt($ch, CURLOPT_URL, $postUrl);
						curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
						curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
						curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
						curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
						curl_setopt($ch, CURLOPT_MAXREDIRS, 2);
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataJson);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						$response = curl_exec($ch);
						$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						$responseBody = json_decode($response);
						curl_close($ch);
						
						if($responseBody){
							$messages = $responseBody->messages;
							foreach ($messages as $val) {
								$messageid = $val->messageId;
							}
							//set id from where
							$smsrequiredId = $nextsmsid;
							if($smsid){
								$smsrequiredId = $smsid;
							}
							$sql = "UPDATE sms SET messageId='$messageid', smsStatus='sent' WHERE smsId='$smsrequiredId'";
							$query = query($sql);
							if (!$query){
								$return=false;
							}
						}
					}
				}
			}
    	} 
	}  
	return $return;
}
function send_mail_pic($compid,$message,$subject,$opt,$adminemail){
	unset($email);
	$email=getval("compEmail","company","compId='$compid'");
	$sql=query("SELECT * FROM company_pic where compId='$compid' and picStatus='y'");
	while($data=fetch($sql)){
		$emailpic[]=$data['picEmail'];
	}
	$emailpic=implode(",", $emailpic);
	if($adminemail){
		$emailcc = $emailpic.",".$adminemail;
	}else{
		$emailcc=$emailpic;
	}

	$emailfrom= explode(",",SITE_EMAIL);
	
	$options = array(
        'mailopt' => $opt,
        'from' => $emailfrom[0],
		'fromname' => SITE_NAME,
        'to' => $email,
        'toname' => $name,
        'subject' => $subject,
        'replyto' =>'',
        'cc' => '',
        'bcc' => $emailcc,
        'message' => $message,
        'messagetype' => 'html',
        'filename' => ''
    );
    $mail = sendMailComplete($options);
    if ($mail){
    	return true;
    }else{
    	return false;
    }
}
function send_sms_pic($compid,$text,$opt){
	$sql=query("SELECT * FROM company_pic where compId='$compid' and picStatus='y'");
	while($data=fetch($sql)){
		$hp=$data['picHP'];
        $smssend=Send_SMS($hp,$text,$opt);
	}
    if ($smssend){
    	return true;
    }else{
    	return false;
    }
}


function terbilang($satuan){ 
		$huruf = array ("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh","Sebelas"); 
		if ($satuan < 12) 
			return " ".$huruf[$satuan]; 
		elseif ($satuan < 20) 
			return terbilang($satuan - 10)." Belas "; 
		elseif ($satuan < 100) 
			return terbilang($satuan / 10)." Puluh ".terbilang($satuan % 10); 
		elseif ($satuan < 200) 
			return "Seratus".terbilang($satuan - 100); 
		elseif ($satuan < 1000) 
			return terbilang($satuan / 100)." Ratus ".terbilang($satuan % 100);
		elseif ($satuan < 2000) 
			return "Seribu".terbilang($satuan - 1000); 
		elseif ($satuan < 1000000) 
			return terbilang($satuan / 1000)." Ribu ".terbilang($satuan % 1000); 
		elseif ($satuan < 1000000000) 
			return terbilang($satuan / 1000000)." Juta ".terbilang($satuan % 1000000); 
		elseif ($satuan >= 1000000000) 
            echo "Angka yang Anda masukkan terlalu besar"; 
    }
function GetBalance($username,$password){
/*    $username           = "ongdedy"; //your smsviro username
    $password           = "Pass8476!123"; //your smsviro password*/
    $postUrl            = "https://api.infobip.com/account/1/balance";
    $ch                 = curl_init();
    $header             = array("Content-Type:application/json", "Accept:application/json");

    curl_setopt($ch, CURLOPT_URL, $postUrl);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 2);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    
    $response = curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $responseBody = json_decode($response, true);
    curl_close($ch);
    
    $data = array(
        'currency' => $responseBody['currency'],
        'balance' => $responseBody['balance']
    );
    
    return $data;
}
function convertidtomonthEng($month){
	//this function will convert day of week to Indonesian language

    if (is_numeric($month)){
        switch ($month){
            case 1: $month="January"; break;
            case 2: $month="February"; break;
            case 3: $month="March"; break;
            case 4: $month="April"; break;
            case 5: $month="May"; break;
            case 6: $month="June"; break;
            case 7: $month="July"; break;
            case 8: $month="August"; break;
            case 9: $month="September"; break;
            case 10: $month="October"; break;
            case 11: $month="November"; break;
            case 12: $month="December"; break;
        }
    }
    return $month;
}
function displayhp($hp){
    $hp=trim($hp);
    $str=array(" ", "-" ,"(" ,")" ,"," ,"_",".");
    $hp=str_replace($str,"",$hp);
    $firstchar=substr($hp,0,1);
    if($firstchar=="0"){
        $hp='+62'.substr($hp,1);
    }elseif($firstchar=="6"){
        $hp='+'.$hp;
    }
    return $hp;
}
function splitname ($name){
	$expname=explode(" ",$name);
	$num=count($expname);
	if ($num==1){
		$arrname=array($expname[0],"","");
	}elseif ($num==2){
		$arrname=array($expname[0],"",$expname[1]);
	}elseif ($num==3){
		$arrname=array($expname[0],$expname[1],$expname[2]);
	}elseif ($num>3){
		for ($i=2;$i<=$num;$i++){
			$lastnm.=$expname[$i] . " ";
		}

		$arrname=array($expname[0],$expname[1],$lastnm);
	}

	return $arrname;
}
#SET PROTOCOL
$http  = "http".($_SERVER['HTTPS'] ? "s" : null)."://";
function  get_file_name_backup($scandir,$ext){

  foreach($scandir as $file_key => $file_name){

   $file_tester = preg_match('/\.'.$ext.'$/i', $file_name);

   if($file_tester == 1){
    return $file_name;
   }
  }

  #return false if no file matches
  return false;
}
function xml_fetchall_data_backup($path){
	#return data array( path => date) if exist and false if not exist
	$data = array();
	$xmldoc = new DOMDocument();
	$xmldoc->load($path);
	$elements = $xmldoc->getElementsByTagName("file");

	$xpath = new DOMXPath($xmldoc);

	foreach($elements as $elements){
		$file_path = $elements->getAttribute('file_path');
		$date_modified = $elements->textContent;
		$data[$file_path] = $date_modified;
	}

	return $data;

	return false;
}
$file_includelib = include("../modz/qr-code/qrlib.php");

function generateqrcodetofile($url,$filename="",$location=""){
    chdir($_SERVER['DOCUMENT_ROOT'].'/admin');
	if(empty($location)){
		$nowdir=date('dmY');
		$targetdir="../assets/qr-code-member/" . $nowdir . "/";

		//test whether targetdir exists
		if(!@opendir($targetdir) ){
			mkdir($targetdir);
		}
	}elseif($location == "event"){
		$nowdir=date('dmY');
		$targetdir="../assets/qr-event/" . $nowdir . "/";

		//test whether targetdir exists
		if(!@opendir($targetdir) ){
			mkdir($targetdir);
		}
	}elseif($location == 'merchant'){
		$nowdir=date('dmY');
		$targetdir="../assets/qr-merchant/" . $nowdir . "/";

		//test whether targetdir exists
		if(!@opendir($targetdir) ){
			mkdir($targetdir);
		}
	}

	$codeContents = $url;

	if(empty($filename)){
		$fileName=$url;
	}else{
		$fileName=$filename;
	}

	$fileName = $fileName.".png";

	if(empty($location)){
		$pngAbsoluteFilePath = "../assets/qr-code-member/".$nowdir."/".$fileName;
	}elseif($location == "event"){
		$pngAbsoluteFilePath = "../assets/qr-event/".$nowdir."/".$fileName;
	}elseif($location == "merchant"){
		$pngAbsoluteFilePath = "../assets/qr-merchant/".$nowdir."/".$fileName;
	}

	QRcode::png($codeContents, $pngAbsoluteFilePath,"L",90,2);
}
function doCurlNew($url) {
    $ch = curl_init();
	// Disable SSL verification
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	// Will return the response, if false it print the response
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// Set the url
	curl_setopt($ch, CURLOPT_URL,$url);
	// Execute
	$result=curl_exec($ch);
	// Closing
	curl_close($ch);
    return $result;
}
function Send_Notif($to,$notifdata,$notifid=null){
	/*
	$to : berisi all/ playerid jika lebih dari pakai ,
	$notifdata : dalam bentuk array
	*/
	global $now;
	global $http;

	if(empty($to)){
		return false;
	}

	$appid=getconfig('ONESIGNAL_API_KEY');

	if($to=="all"){
		$queryto=query("SELECT notifPlayerId FROM member_notification_data WHERE notifPlayerId!='' ORDER BY notifRegister DESC");
		while ($datato=fetch($queryto)) {
			$playerid[]=$datato['notifPlayerId'];
		}
	}elseif($to=="mobile"){
		$queryto=query("SELECT notifPlayerId FROM member_notification_data WHERE notifDeviceType='1' ORDER BY notifRegister DESC");
		while ($datato=fetch($queryto)) {
			$playerid[]=$datato['notifPlayerId'];
		}
		// $playerid[]="14e5b139-94e9-406e-84e6-9714e9fb7b74";
	}elseif($to=="dekstop"){
		$queryto=query("SELECT notifPlayerId FROM member_notification_data WHERE notifDeviceType!='1' ORDER BY notifRegister DESC");
		while ($datato=fetch($queryto)) {
			$playerid[]=$datato['notifPlayerId'];
		}
		// $playerid[]="1c26da56-60e0-4af9-ba4c-1dc989364344";

	}else{
		$playerid=explode(",", $to);
	}

	if(!$notifid){
		$day = date("d",$now);
		$month = date("m",$now);
		$year = date("Y",$now);
		$dateen = date("Y-m-d",$now);
		$timenow = date("H:i:s",$now);
	}

	if(!$notifid){
		$notifid = nextid("notificationId","notification");
		$sql = "INSERT INTO notification(notificationId,notificationTo,notificationTitle,notificationContent,notificationTTL,notificationURL,notificationSmallDir,notificationSmallImage,notificationBigDir,notificationBigImage,notificationDay,notificationMonth,notificationYear,notificationdDate,notificationTime,notificationFullTime,notificationStatus) VALUES ('$notifid','$to','{$notifdata['title']}','{$notifdata['content']}','{$notifdata['ttl']}','{$notifdata['url']}','{$notifdata['sdir']}','{$notifdata['spic']}','{$notifdata['bdir']}','{$notifdata['bpic']}','$day','$month','$year','$dateen','$timenow','$now','new')";
		$query = query($sql);

	}
	$headings = array(
		"en" => $notifdata['title']
		);
	$content = array(
		"en" => $notifdata['content']
		);
	if($playerid and $headings['en'] and $content['en']){
		$fields=array();
  		$fields['app_id']=$appid;
  		$fields['include_player_ids']= $playerid;
  		$fields['contents']=$content;
  		$fields['headings']=$headings;

  		if($notifdata['ttl']){
  			$fields['ttl']=$notifdata['ttl'];
  		}
  		if($notifdata['url']){
  			$fields['url']=$notifdata['url'];
  		}
		if($notifdata['sdir'] and $notifdata['spic']){
			$fields['chrome_web_icon']=$http.getconfig('SITE_URL')."/assets/notif/".$notifdata['sdir']."/".$notifdata['spic'];
			// $fields['small_icon']=$http.getconfig('SITE_URL')."/assets/notif/".$notifdata['sdir']."/".$notifdata['spic'];
			$fields['large_icon']=$http.getconfig('SITE_URL')."/assets/notif/".$notifdata['sdir']."/".$notifdata['spic'];
		}
		if($notifdata['bdir'] and $notifdata['bpic']){
			$bimage=$http.getconfig('SITE_URL')."/assets/notif/".$notifdata['bdir']."/".$notifdata['bpic'];
			$fields['big_picture']=$bimage;
			$fields['chrome_web_image']=$bimage;
		}
		if($notifdata['date'] and $notifdata['time']){
			$fields['send_after']=$notifdata['date']." ".$notifdata['time']." UTC+0700";
		}
		$fields = json_encode($fields);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
												   'Authorization: Basic '.getconfig('ONESIGNAL_REST_API_KEY')));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);

		$theresponse=json_encode($response);
		$response=json_decode($response, true);
		if($response){
			if($response['id'] and $response['recipients']){
				$sql = "UPDATE notification SET responseId='{$response['id']}', notificationStatus='sent', notificationSent='$now' WHERE notificationId='$notifid'";
				$query = query($sql);
			}
			$historyid = nextid("historyid","notification_history");
			$sql = "INSERT INTO notification_history(historyId,responseId,historyResponse) VALUES($historyid,'{$response['id']}','$theresponse')";
			$query = query($sql);
		}
		if($response['errors']['invalid_player_ids']){
			foreach ($response['errors']['invalid_player_ids'] as $key => $value) {
				$sql = "DELETE FROM member_notification_data WHERE notifPlayerId='$value'";
				$query = query($sql);
			}
		}
		$responseBody=$response;
	}else{
		$responseBody="Playerid / Judul / Content Notifikasi no data";
	}
	return $responseBody;
}
function strposa($haystack, $needles=array(), $offset=0) {
	$chr = array();
	foreach($needles as $needle) {
		$res = strpos($haystack, $needle, $offset);
		if ($res !== false){
			return true;
			break;
		}
	}
	return false;
}

function checkbox_parent($arraybox){
    $c = array();
    foreach($arraybox as $y){
        $yd = explode(".",$y);
        foreach($yd as $k){
            $c[] = $k;
        }
    }
    $c = array_unique($c);
    return $c;
}
function image360($content,$for){
	if($for=='content'){
		$img360= explode("{",$content);
        foreach($img360 as $key => $value ){
            $img=strpos($value,"}");
            if($img){
            $img360_all[]=substr($value, 0, $img);
            }
        }

        $no=1;
        foreach ($img360_all as $key => $value) {
            $data360='
                <div class="text-center"><a class="btn btn-success fancybox-frame" style="color:#fff; padding:3px;"  href="/web/image360.php?link='.$value.'">Lihat Foto 360<sup>0</sup></a></div>
            ';

            $value2="{".$value."}";
            $content=str_ireplace("{$value2}",$data360,$content);
        $no++;
        }
    return $content;
	}elseif($for=='script'){
		$img360= explode("{",$content);
        foreach($img360 as $key => $value ){
            $img=strpos($value,"}");
            if($img){
            $img360_all[]=substr($value, 0, $img);
            }
        }
		$cno=1;
        $script360='
            <script>
                window.onload = function() {
        ';
	    foreach ($img360_all as $key => $value) {
	        $script360.='
	                    var PSV = new PhotoSphereViewer({
	                        panorama: "'.$value.'",
	                        container: "your-pano'.$cno.'",
	                        /* autoplay */
	                        time_anim: false,
	                        navbar: true,
	                        size: {
	                            width: "100%",
	                            height: "500"
	                        },

	                        navbar_style: {
	                          backgroundColor: "rgba(58, 67, 77, 0.7)"
	                        }
	                    });
	        ';
	    $cno++; }
	    $script360.='
	           };
	            </script>
	        ';
	    return $script360;
	}else{
		$cno=1;
        $script360='
            <script>
                window.onload = function() {
        ';
        $script360.='
                    var PSV = new PhotoSphereViewer({
                        panorama: "'.$content.'",
                        container: "your-pano'.$cno.'",
                        /* autoplay */
                        time_anim: false,
                        navbar: true,
                        size: {
                            width: "100%",
                            height: "500"
                        },

                        navbar_style: {
                          backgroundColor: "rgba(58, 67, 77, 0.7)"
                        }
                    });
        ';
	    $script360.='
	           };
	            </script>
	        ';
	    return $script360;
	}
}
function checkemail($email){
	$noemail = array('gmail.con','gmail.co.id','yahoo.con','gmai.com','gmail.co','yahoi.com','gmal.com','gmal.co','gmal.co.id','gmai.co.id','yaho.com','yaho.con','yaho.co.id','yahoi.co','yahoi.con','yahooo.co','yahooo.com','yahooo.co.id','gmail.coid','gmai.coid','gmal.coid','gmai.co');
	$arrayemail = explode("@",$email);
	if(in_array($arrayemail[1],$noemail)){
		return false;
	}else{
		return true;
	}
}
function validate_format($type, $string){
		if($type == 'indo_phone_num'){
			$check_indo_phonenum = preg_match("/^(\+628|628)|^(08)([\d]{8,20})$/", $string, $phonenum_match);

			if($check_indo_phonenum){
				return true;
			}
		}

		return false;
}
function createsitemapimage($sitemapconfig){
	//this function will creata sitemap file.
	global $now;
	global $webprotocol;
	$sitemaptag='<?xml version="1.0" encoding="UTF-8"?>
				<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
				xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';

	//check the sitemap type, navigation is website nav or articlelike for article record.
	if($sitemapconfig['sitemaptype']=='navigation'){
		$sitemaptag.="<url>
					  <loc>".$webprotocol.getconfig('SITE_URL')."/</loc>
					  <lastmod>".date('Y-m-d',$now)."</lastmod>
					  <changefreq>".$sitemapconfig['changefreq']."</changefreq>
					  <priority>1.00</priority>
					</url>";
	}
	$sitemaptag.=$sitemapconfig['url'];
	$sitemaptag.="</urlset>";

	$file=realpath(dirname(__file__).'/..').'/sitemap/'.$sitemapconfig['filename'];
	@chmod($file, 0777);
	$fp=@fopen($file,"w");
	@fwrite($fp,$sitemaptag);
	@fclose($fp);
	@chmod($file, 0644);
}
function generateqrcode($url){
    $codeContents = $url;
    #QRcode::png($codeContents, $pngAbsoluteFilePath,"L",7,2);
    QRcode::png($codeContents,false,"L",7,2);
    #QRcode::png($url);
}
?>