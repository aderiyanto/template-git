<?php
	#define license variable
	$lc = unserialize(strdecode($license_0195));

	//protect admin page to only allow access from trusted IP
	$whitelist=WHITELIST_IP;
	if($whitelist){
		$whitelist=explode(",",$whitelist);
		if(count($whitelist)!=0){
			if(!in_array(getenv('REMOTE_ADDR'),$whitelist)){
				header("location:../docs/off.php");
				exit;
			}
		}
	}

	#clean all input
	$_GET    = cleanup($_GET,"admin");
	$_POST   = cleanup($_POST,"admin");
	$_COOKIE = cleanup($_COOKIE,"admin");
	
	#get all data from post
	foreach ($_POST as $key=>$value){
		$$key=$value;
	}
	#get all data from get
	foreach ($_GET as $key=>$value){
		$$key=$value;
	}

	//get all cookies value that we have set
	$cook_username = $_COOKIE['cook_username'];
	$cook_id	   = $_COOKIE['cook_id'];
	$cook_cp	   = $_COOKIE['cook_cp'];
	$cook_gid	   = $_COOKIE['cook_gid'];
	$cook_lastlog  = $_COOKIE['cook_lastlog'];

	//make sure this user is allowed to access this menu
	//this script also keep the permission for specific page
	if(isset($_SERVER['HTTP_USER_AGENT'])){
		if(!empty($cook_gid) and is_numeric($page)){
			$sql="SELECT * FROM admin_group_menu WHERE menuId='$page' and groupId='$cook_gid'";
			$query=query($sql);
			$uac_allowed=rows($query);
			if ($uac_allowed==0){
				header("location:index.php");
				exit;
			}else{
				$data = fetch($query);
				$uac_add    = ($data['gmAdd']=="y")?"y":"";
				$uac_edit   = ($data['gmEdit']=="y")?"y":"";
				$uac_delete = ($data['gmDelete']=="y")?"y":"";
			}

			if(!in_array($page, $lc['features']) and ($cook_gid!=1 and ($page!=10 or $page!=11))){
				header("location:index.php");
				exit;
			}
		}
	}

	#SYSTEMLOG
	//get logging setting for admin
	if (ADMIN_LOG=="on"){
		$url_log = cleanup("http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);
		$ip_log = visitor_ip();

		//check if current ip visite the url before in last 24 hours
		$minrange=$now-(60*60*24);
		$sql="SELECT * FROM systemlog WHERE logIp='$ip_log' AND logUrl='$url_log' AND (logTime BETWEEN $minrange AND $now)";
		$query=query($sql);
		$data=fetch($query);

		if(rows($query)==0){
			//save to database if never visit before
			$sql="INSERT INTO systemlog VALUES ($now,'$ip_log','$url_log',1,1,'$cook_username')";
			$query=query($sql);
		}else{
		//update counter if this ip already visit the url befor
			$sql="UPDATE systemlog SET logCounter=logCounter+1 WHERE logIp='$ip_log' AND logUrl='$url_log'";
			$query=query($sql);
		}
	}


	###########################################################################
	#MULTILINGUAL FEATURE
	$multilang_cms = getconfig('MULTILANGUAGE_CMS');
	
	#define default language
	$lg['default'] = $multilang_cms[0];
	
	if(empty($_COOKIE['lang_admin'])){
		$_COOKIE['lang_admin'] = $lg['default'];
	}

	#set gettext variable
	$lg['active'] = (($_COOKIE['lang_admin'] AND in_array($_COOKIE['lang_admin'], $multilang_cms)) ? $_COOKIE['lang_admin']:$lg['default']);
	$lg['path'] = "../lang";
	$lg['domain'] = "messages_admin";

	#set gettext invironment
	setlocale(LC_ALL, $lg['active']);
	setlocale(LC_TIME, $lg['active']);
	putenv("LANG=".$lg['active']);

	#additional setting to debug while develope progress, it's use to avoid apache cache
	if(getconfig('SHOW_DEBUG')==TRUE){
		#set .mo file path and get last update
		$lg['filename'] = $lg['path'].'/'.$lg['active'].'/LC_MESSAGES/'.$lg['domain'].'.mo';
		$lg['mtime'] = @filemtime($lg['filename']);

		if (!@file_exists($lg['filename_new'])) {
			$dirpath = scandir(dirname($lg['filename']));
			foreach ($dirpath as $filepath) {
				if(in_array($filepath, array('.','..', $lg['domain'].'.po', $lg['domain'].'.mo'))) continue;
				if(pathinfo(dirname($lg['filename']). DIRECTORY_SEPARATOR .$filepath, PATHINFO_EXTENSION)=='mo'){
					//if file name not using "admin.mo" prefix just continue
					if(!strpos($filepath, "admin.mo")) continue;
					unlink(dirname($lg['filename']). DIRECTORY_SEPARATOR .$filepath);
				}
			}

			#controlling .mo file to avoid apache cache
			$lg['filename_new'] = $lg['path'].'/'.$lg['active'].'/LC_MESSAGES/'.$lg['domain'].'_'.$lg['mtime'].'_admin'.'.mo';
			@copy($lg['filename'], $lg['filename_new']);

			#compute the new domain name
			$lg['domain'] = $lg['domain'] .'_'.$lg['mtime'].'_admin';
		}
	}

	#binding current language
	bindtextdomain($lg['domain'],$lg['path']);

	#activate current language
	textdomain($lg['domain']);

	if($_COOKIE['lang_admin'] AND in_array($_COOKIE['lang_admin'], $multilang_cms)){
		$lang_admin = $_COOKIE['lang_admin'];
	}
	else{
		$lang_admin = $lg['default'];
	}

	#define lang_admin acronim
	$active_lang = substr($lang_admin,0,2);

	#remove variable
	unset($lg);
	
	#safe license variable
	$lc_uri = $lc['url'];
	$lc_uri = $lc_uri[0];
	$lc_social = $lc['social'];
	$lc_features = $lc['features'];

	$lc_expt = $lc['expire'];
	if($lc_expt!=0){
		$lc_exp  = convertdaytoid(date("l",$lc_expt)) . ", " . date("d ",$lc_expt) . convertmonthtoid(date("F",$lc_expt)) . date(" Y",$lc_expt);
	}

	$lc_lang = $lc['language'];
	$lc_lang_default = $lc['language_default'];

	#dstroy license variable
	unset($lc); 
?>