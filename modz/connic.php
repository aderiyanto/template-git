<?php
	$dsn = "mysql:dbname=".getconfig('DB_NAME').";charset=utf8;host=".getconfig('DB_HOST');
	try{
		$dbh = new PDO($dsn, getconfig('DB_USER'), getconfig('DB_PASSWORD'));
	}catch(PDOException $e){
		$file=$_SERVER['SCRIPT_FILENAME'];
		warnwebmaster($file,$e->getMessage());
		header("location:/docs/down.php"); 
		exit;
	}
	
	#set as utf8
	query("SET NAMES utf8");
	query("SET CHARSET utf8");
	query("SET character_set_client=utf8");
	query("SET character_set_connection=utf8");
	query("SET character_set_results=utf8");
?>