<?php
/*
* This file was created on 21/09/2015
* Include the extend function here
*/

// Export Import Excel Library Function
function exExcelLibs (){
	return require_once( '../modz/extoexcel/PHPExcel.php' );
	return require_once( '../modz/extoexcel/PHPExcel/Writer/Excel2007.php' );
}
function imExcelLibs (){
	return require_once( '../modz/extoexcel/PHPExcel/IOFactory.php' );
}

function dateformat($tstamp,$desc=NULL){
	if($desc==NULL){
		$multilang=getconfig('MULTILANGUAGE_CMS');
		if($_COOKIE['lang_admin'] AND in_array($_COOKIE['lang_admin'],$multilang)){
			$lang = $_COOKIE['lang_admin'];
		}
		else{
			$lang = $multilang[0];
		}
		
		if($lang=="id_ID"){
			$date=date('d',$tstamp)." ".convertmonthtolang(date('m',$tstamp))." ".date('Y',$tstamp);
		}elseif($lang=="en_US"){
			$date=convertmonthtolang(date('m',$tstamp))." ".date('d',$tstamp).", ".date('Y',$tstamp);
		}
	}elseif($desc=="day"){
		$multilang=getconfig('MULTILANGUAGE_CMS');
		if($_COOKIE['lang_admin'] AND in_array($_COOKIE['lang_admin'],$multilang)){
			$lang = $_COOKIE['lang_admin'];
		}
		else{
			$lang = $multilang[0];
		}
		
		if($lang=="id_ID"){
			$date=convertdaytolang(date('l',$tstamp)).", ".date('d',$tstamp)." ".convertmonthtolang(date('m',$tstamp))." ".date('Y',$tstamp);
		}elseif($lang=="en_US"){
			$date=convertdaytolang(date('l',$tstamp)).", ".convertmonthtolang(date('m',$tstamp))." ".date('d',$tstamp).", ".date('Y',$tstamp);
		}
	}elseif($desc=="full"){
		$multilang=getconfig('MULTILANGUAGE_CMS');
		if($_COOKIE['lang_admin'] AND in_array($_COOKIE['lang_admin'],$multilang)){
			$lang = $_COOKIE['lang_admin'];
		}
		else{
			$lang = $multilang[0];
		}
		
		if($lang=="id_ID"){
			$date=convertdaytolang(date('l',$tstamp)).", ".date('d',$tstamp)." ".convertmonthtolang(date('m',$tstamp))." ".date('Y',$tstamp)." ".date('H:i',$tstamp);
		}elseif($lang=="en_US"){
			$date=convertdaytolang(date('l',$tstamp)).", ".convertmonthtolang(date('m',$tstamp))." ".date('d',$tstamp).", ".date('Y',$tstamp)." ".date('g:i A',$tstamp);
		}
	}elseif($desc=="time"){
		$multilang=getconfig('MULTILANGUAGE_CMS');
		if($_COOKIE['lang_admin'] AND in_array($_COOKIE['lang_admin'],$multilang)){
			$lang = $_COOKIE['lang_admin'];
		}
		else{
			$lang = $multilang[0];
		}
		
		if($lang=="id_ID"){
			$date=date('d',$tstamp)." ".convertmonthtolang(date('m',$tstamp))." ".date('Y',$tstamp)." ".date("H:i",$tstamp);
		}elseif($lang=="en_US"){
			$date=convertmonthtolang(date('m',$tstamp))." ".date('d',$tstamp).", ".date('Y',$tstamp)." ".date('g:i A',$tstamp);
		}
	}
	
	return $date;
}

//function mobile notification
function sendpushnotification($tokens, $notification, $custom = array(),$to) {
		 if($to=="customer"){
		    define('FIREBASE_SERVER_KEY', getconfig('FIREBASE_SERVER_KEY'));
		 }else{
		    define('FIREBASE_SERVER_KEY', getconfig('FIREBASE_SERVER_KEY_SALES'));
		 }
		 $url = 'https://fcm.googleapis.com/fcm/send';

		 $fields = array
		 (
		 'registration_ids' => $tokens,
		 'data' => $custom,
		 'priority' => 'high'
		 );
		 $headers = array(
		 'Authorization: key=' . FIREBASE_SERVER_KEY,
		 'Content-Type: application/json'
		 );
		 
		 $ch = curl_init();
		 curl_setopt($ch, CURLOPT_URL, $url);
		 curl_setopt($ch, CURLOPT_POST, true);
		 curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		 curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
		 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		 curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		 $result = curl_exec($ch);           
		 if ($result === FALSE) {
		 die('Curl failed: ' . curl_error($ch));
		 }
		 curl_close($ch);
		 return $result;
	
}
function endek($encrypt_decrypt,$string){
    	$password = getconfig('PASSWORD_ENCRYPT');
    	$method = 'aes-128-cfb';
    	$iv = substr(hash('sha1', $password), 0, 16);
    	$output='';
    if($encrypt_decrypt=='encrypt'){
        $output = openssl_encrypt($string, $method, $password, 0, $iv);
        $output = base64_encode($output);
   } else if($encrypt_decrypt=='decrypt'){
        $output = base64_decode($string);
        $output = openssl_decrypt($output, $method, $password, 0, $iv);
   }
   return $output;
}
//---------------------------------------------------fungsi cek point--------------------------------
function checkpointGen($table, $columnId, $id) {
	$sqlcek=query("SELECT checkPoint FROM $table");
	$db=getconfig('DB_NAME');
	if (!$sqlcek){
		$hasil=FALSE;
	}else{
	$sql=query("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '$db' and TABLE_NAME = '$table' and COLUMN_NAME<>'checkPoint'");
	$no=0;
	while($data=fetch($sql)){
		$query=query("select ".$data['COLUMN_NAME']." from $table where $columnId='$id'");
		 while($dataisi=fetch($query)){
			$datax[$no][$data['COLUMN_NAME']]= $dataisi[$data['COLUMN_NAME']];
		}
		$no++;
	}
		$text_json=json_encode($datax);
		$cp = endek('encrypt',$text_json);
		$update=query("update $table set checkPoint='$cp' where $columnId='$id'");
		$hasil=TRUE;
	}
	return $hasil;
}
function checkpoint($table, $columnId, $id) {
	$sqlcek=query("SELECT checkPoint FROM $table");
	$db=getconfig('DB_NAME');
	if (!$sqlcek){
		$hasil=FALSE;
	}else{
	$sql=query("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '$db' and TABLE_NAME = '$table' and COLUMN_NAME<>'checkPoint'");
	$no=0;
	while($data=fetch($sql)){
		$query=query("select ".$data['COLUMN_NAME']." from $table where $columnId='$id'");
		 while($dataisi=fetch($query)){
			$datax[$no][$data['COLUMN_NAME']]= $dataisi[$data['COLUMN_NAME']];
		}
		$no++;
	}
		$sqlcp="select checkPoint from $table where $columnId='$id'";
	   	$querycp=query($sqlcp);
	   	$datacp=fetch($querycp);

		$text_json=json_encode($datax);
		$cp = endek('encrypt',$text_json);

		if ($cp==$datacp['checkPoint']){
            	//$sql="UPDATE storesetting SET setValue='on' WHERE setId=6";
				//$query=query($sql);
		}else {
				$sql="UPDATE storesetting SET setValue='off' WHERE setId=6";
				$query=query($sql);

				$sql="SELECT setVar, setValue FROM storesetting";
				$query=query($sql);
				$written.="\n";
				while ($data=fetch($query)){
					$data=output($data);
					$written.="define(\"" . $data['setVar'] . "\",\"" . $data['setValue'] . "\");\n";
				}

				$writtens.="<?php \nerror_reporting(0);\n";
				$writtens.="date_default_timezone_set(\"Asia/Jakarta\"); \n";
				$writtens.='$now=time();';
				$writtens.="\n";
				$writtens.=stripslashes($written) . "?>";

				//open config.php
				$file="../modz/config.php";
				chmod($file, 0777);
				$fp=fopen($file,"w");
				fwrite($fp,$writtens);
				fclose($fp);
				chmod($file, 0644);

				//----------------------------------KIRIM EMAIL KE DEVELOPER --------------------------
				$option = array();
				$option['from'] = SITE_EMAIL;
				$option['mailopt'] = 'queue';
				$option['fromname'] = SITE_NAME;
				$option['replyto'] = SITE_EMAIL;
				$option['cc'] = '';
				$option['bcc'] = '';
				$option['to'] = SITE_DEVELOPER_EMAIL;
				$option['subject'] = "DATA ERROR";
				$option['message'] = '<img src="http://'.getconfig(SITE_URL).NEWSLETTER_HEADER.'" title="Logo" alt="logo" />"<br />Pada tabel <b> $table </b> terdapat data yang invalid di  $columnId : <b> $id </b>';
				$option['messagetype'] = 'text';
				$sendmail = sendMailComplete($option);
               }
		$hasil=TRUE;
	}
	return $cp;
}

function format_event_date($stardate,$enddate){
	$starttime=date('d-m-Y',$stardate);
	list($days,$months,$years)=explode('-',$starttime);
	$endtime=date('d-m-Y',$enddate);
	list($daye,$monthe,$yeare)=explode('-',$endtime);
	$showdate="";
	if($starttime==$endtime){
		$monthms=convertmonthtoid($months);
		$showdate=$days." ".$monthms." ".$years;
	}elseif($months==$monthe and $years==$yeare){
		$monthms=convertmonthtoid($months);
		$showdate=$days."-".$daye." ".$monthms." ".$years;
	}elseif($years==$yeare){
		$monthms=convertmonthtoid($months);
		$monthme=convertmonthtoid($monthe);
		$showdate=$days." ".$monthms." - ".$daye." ".$monthme." ".$yeare;
	}else{
		$monthms=convertmonthtoid($months);
		$monthme=convertmonthtoid($monthe);
		$showdate=$days." ".$monthms." ".$years." - ".$daye." ".$monthme." ".$yeare;
	}
	return $showdate;
}

function format_event_date_time($startdate,$enddate){
	$starttime=date('d m Y H:i',$startdate);
	list($days,$months,$years,$times)=explode(' ',$starttime);
	$endtime=date('d m Y H:i',$enddate);
	list($daye,$monthe,$yeare,$timee)=explode(' ',$endtime);
	$showdate="";
	//$showdate=$starttime." - ".$endtime;
	if($starttime==$endtime){
		//$monthms=convertmonthtoid($months);
		$showdate=$times;
	}elseif($days==$daye and $months==$monthe and $years==$yeare){
		$showdate=$times." - ".$timee;
	}else{
		$showdate=$times." - ".$timee;
	}

	return $showdate;
}
function send_sms($to,$text,$opt,$smsid=null){
	if (empty($opt)){
		$opt=getconfig('SMS_OPTION');
	}
	global $now;
	#example $to="0812123456789,0852123456789,0819123456789";
	#$text     ="Everything";
	#$smsid     =value id; if sms has been queue before!
	$return=true;

	if(empty($to)){
		$return=false;
	}
    $pecah              = explode(",",$to);
    $jumlah             = count($pecah);
    $from               = getconfig('SMS_API_FROM');//Sender ID or SMS Masking Name, if leave blank, it will use default from telco
    $username           = getconfig('SMS_API_USERNAME'); //your smsviro username
    $password           = getconfig('SMS_API_PASSWORD'); //your smsviro password
    $postUrl            = "http://107.20.199.106/restapi/sms/1/text/advanced"; # DO NOT CHANGE THIS
    
    // getnotify sms
    $notifyUrl          = getconfig('SMS_API_NOTIF_URL'); //notifyurl
    $notifyContentType  = "application/json";
    $callbackData       = getconfig('SMS_API_NOTIF_URL'); 
	
	
	$day = date("d",$now);
	$month = date("m",$now);
	$year = date("Y",$now);
	$dateen = date("Y-m-d",$now);
	$timenow = date("H:i:s",$now);

	if ($return==true){
	    for($i=0; $i<$jumlah; $i++){
	        if(substr($pecah[$i],0,2) == "62" || substr($pecah[$i],0,3) == "+62"){
	            $pecah = $pecah;
	        }elseif(substr($pecah[$i],0,1) == "0"){
	            $pecah[$i][0] = "X";
	            $pecah = str_replace("X", "62", $pecah);
	        }else{
	            // Invalid mobile number format
	            $return=false;
	        }
			if ($return==true){
				if($opt=="queue"){
					$is_exist = countdata("sms","smsTo='{$pecah[$i]}' AND smsText='{$text}' and smsDate='$dateen'"); 
					if($is_exist<1){
						$nextsmsid = nextid("smsId","sms");
						$sql = "INSERT INTO sms VALUES ('{$nextsmsid}','','{$pecah[$i]}','{$text}','{$day}','{$month}','{$year}','{$dateen}','{$timenow}','{$now}','','new')";
						$query = query($sql);
						if (!$query){
							$return=false;
						}
					}
				}else{
					if(!$smsid){
						$is_exist = countdata("sms","smsTo='{$pecah[$i]}' AND smsText='{$text}' and smsDate='$dateen'"); 
						if($is_exist<1){
							$nextsmsid = nextid("smsId","sms");
							$sql = "INSERT INTO sms VALUES ('{$nextsmsid}','','{$pecah[$i]}','{$text}','{$day}','{$month}','{$year}','{$dateen}','{$timenow}','{$now}','','sent')";
							$query = query($sql);
							if (!$query){
								$return=false;
							}
						}
					}
					if(getconfig('SMS_DEVELOPER_MODE')==false){
						$destination = array("to" => $pecah[$i]);
						$message     = array("from" => $from,
											 "destinations" => $destination,
											 "text" => $text,
											 "intermediateReport" => true,
											 "notifyUrl" => $notifyUrl,
											 "smsCount" => 2,
											 "notifyContentType" => $notifyContentType,
											 "callbackData" => $callbackData);
						
						$postData           = array("messages" => array($message));
						$postDataJson       = json_encode($postData);
						$ch                 = curl_init();
						$header             = array("Content-Type:application/json", "Accept:application/json");
						
						curl_setopt($ch, CURLOPT_URL, $postUrl);
						curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
						curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
						curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
						curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
						curl_setopt($ch, CURLOPT_MAXREDIRS, 2);
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataJson);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						$response = curl_exec($ch);
						$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						$responseBody = json_decode($response);
						curl_close($ch);
						
						if($responseBody){
							$messages = $responseBody->messages;
							foreach ($messages as $val) {
								$messageid = $val->messageId;
							}
							//set id from where
							$smsrequiredId = $nextsmsid;
							if($smsid){
								$smsrequiredId = $smsid;
							}
							$sql = "UPDATE sms SET messageId='$messageid', smsStatus='sent' WHERE smsId='$smsrequiredId'";
							$query = query($sql);
							if (!$query){
								$return=false;
							}
						}
					}
				}
			}
    	} 
	}  
	return $return;
}
function delete_directory($dirname,$create) {
     if (is_dir($dirname))
           $dir_handle = opendir($dirname);
	 if (!$dir_handle)
	      return false;
	 while($file = readdir($dir_handle)) {
	       if ($file != "." && $file != "..") {
	            if (!is_dir($dirname."/".$file))
	                 unlink($dirname."/".$file);
	            else
	                 delete_directory($dirname.'/'.$file);
	       }
	 }
	 closedir($dir_handle);
	 if ($create!='y'){
	 	rmdir($dirname);
	 }
	 return true;
}
  function GetBalance($username,$password){
/*    $username           = "ongdedy"; //your smsviro username
    $password           = "Pass8476!123"; //your smsviro password*/
    $postUrl            = "https://api.infobip.com/account/1/balance";
    $ch                 = curl_init();
    $header             = array("Content-Type:application/json", "Accept:application/json");

    curl_setopt($ch, CURLOPT_URL, $postUrl);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 2);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    
    $response = curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $responseBody = json_decode($response, true);
    curl_close($ch);
    
    $data = array(
        'currency' => $responseBody['currency'],
        'balance' => $responseBody['balance']
    );
    
    return $data;
}
?>