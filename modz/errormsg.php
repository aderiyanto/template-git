<?php
	##################################################################
	# Description:
	# This file contains all the error messages
	# They are stored in array and will be displayed in error.php file
	##################################################################
	
	$msg[1]  = _("error_1");
	$msg[2]  = _("error_2");
	$msg[3]  = _("error_3");
	$msg[4]  = _("error_4");
	$msg[5]  = _("error_5");
	$msg[6]  = _("error_6");
	$msg[7]  = _("error_7");
	$msg[8]  = _("error_8");
	$msg[9]  = _("error_9");
	$msg[10] = _("error_10");
	$msg[11] = _("error_11");
	$msg[12] = _("error_12");
	$msg[13] = _("error_13");
	$msg[14] = _("error_14");
	$msg[15] = _("error_15");	
	$msg[16] = _("error_16");
	$msg[17] = _("error_17");
	$msg[18] = _("error_18");
	$msg[19] = _("error_19");
	$msg[20] = _("error_20");
	$msg[21] = _("error_21");
	$msg[22] = _("error_22");
	$msg[23] = _("error_23");
	$msg[24] = _("error_24");	
	$msg[25] = _("error_25");
	$msg[26] = _("error_26");
	$msg[27] = _("error_27");
	$msg[28] = _("error_28");
	$msg[29] = _("error_29");
	$msg[30] = _("error_30");
	$msg[31] = _("error_31");
	$msg[32] = _("error_32");
	$msg[33] = _("error_33");
	$msg[34] = _("error_34");
	$msg[35] = _("error_32");
	$msg[36] = _("error_36");
	$msg[37] = _("error_37");
	$msg[38] = _("error_38");
	$msg[39] = _("error_39");
	$msg[40] = _("error_40");
	$msg[41] = _("error_41");
	$msg[42] = _("error_42");
	$msg[43] = _("error_43");
	$msg[44] = _("error_44");
	$msg[45] = _("error_45");
	$msg[46] = _("error_46");
	$msg[47] = _("error_47");
	$msg[48] = _("error_48");
	$msg[66] = _("error_66");
?>