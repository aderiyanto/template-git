<?php
	######################################################################
	# Description:
	# This file will hold all the main modules necessary for the system
	# Included in all files
	##################################################################

	# ROUTINE FOR LICENSE CHECK MULTI URL
	$lc = unserialize(strdecode($license_0195));
	if (count($lc['url'])==0){
		header("location:/docs/license.php");
		exit;
	}

	if(isset($_SERVER['HTTP_USER_AGENT'])){
		$host = str_replace("www.","",getenv('HTTP_HOST'));
		if(!empty($host) and !in_array($host, $lc['url'])){
			header("location:/docs/license.php");
			exit;
		}
	}

	if ((int)$lc['expire']!==0){
		if((int)$lc['expire']<$now){
			header("location:/docs/license.php");
			exit;
		}
	}
	unset($lc);
	# END OF LICENSE CHECK

	$blackIP=BLACKLIST_IP;
	if ($blackIP){
		//This line will redirect blacklist ip to somewhere
		$blocklist=explode(",",$blackIP);
		if (in_array(getenv('REMOTE_ADDR'),$blocklist)){
			header("location:../docs/off.php");
			exit;
		}
	}

	function cleanup($input,$level="user"){
		//This function will sanitize any input
		global $_POST, $_GET, $_FILES, $now;

		if (is_array($input)){
			foreach ($input as $key=>$value){
				$input[$key]=cleanup($value,$level);
			}
		}
		else{
			$input=str_replace("<p>&nbsp;</p>","",$input);
			$input=str_replace("src=\"../","src=\"/",$input);
			$input=str_replace("href=\"../","href=\"/",$input);

			if($level=="admin"){
				$input=strip_tags($input, '<a><address><pre><b><i><em><span><img><sub><sup><p><hr><h1><h2><h3><h4><h5><h6><font><thead><link><caption><blockquote><br><li><table><td><th><tbody><tr><strong><ul><ol><tfoot><thead><center><blink><strike><iframe><div><style><object><param>');
			}elseif($level=="sendmail"){
				$input=$input;
			}else{
				$input=strip_tags($input, '<a><address><pre><b><i><span><p><h1><h2><h3><h4><h5><h6><font><caption><br><li><strong><ul><ol><center>');
			}

			if (!get_magic_quotes_gpc()){
				$input=addslashes($input);
			}
			$input=trim($input);
		}

		#update database last activity for automatic backup plan
		if ((is_array($_POST) and count(array_filter($_POST)) != 0) and $_GET['js'] == 'on'){
			$data = getval("actData", "activitylog", "actName", "CRON_BACKUP_DATABASE");
			if ($data){
				$data = unserialize($data);
				$data['last_backup'] = $data['last_backup'] ? $data['last_backup']:0;
				$data['last_modify'] = $now;
			}

			$sql = "UPDATE activitylog SET actLastActivity='$now', actData='".serialize($data)."' WHERE actName='CRON_BACKUP_DATABASE'";
			$query = query($sql);
		}

		#update assets last activity for automatic backup plan
		if ((is_array($_FILES) and count(array_filter($_FILES)) != 0) and $_GET['js'] == 'on'){
			$data = getval("actData", "activitylog", "actName", "CRON_BACKUP_ASSET");
			if ($data){
				$data = unserialize($data);
				$data['last_backup'] = $data['last_backup'] ? $data['last_backup']:0;
				$data['last_modify'] = $now;
			}

			$sql = "UPDATE activitylog SET actLastActivity='$now', actData='".serialize($data)."' WHERE actName='CRON_BACKUP_ASSET'";
			$query = query($sql);
		}

		return $input;
	}

		function output($input){
		global $_glossDataTarget, $_glossDataReplacement;

		#function to prepare all string before we output to browser
		if (is_array($input)){
			foreach ($input as $key=>$value){
				$input[$key]=output($value);
			}
		} else {
			$input=stripslashes($input);

			#####################################################
			# Routine for words censor
			#####################################################
			if(!$_glossDataTarget){
				$sql_w="SELECT * FROM glossary ORDER BY glossaryTarget";
				$query_w=query($sql_w);

				while($data = fetch($query_w)){
					$target[]=$data['glossaryTarget'];
					$replacement[]=$data['glossaryReplacement'];
				}

				$_glossDataTarget = $target;
				$_glossDataReplacement = $replacement;
			} else {
				$target = $_glossDataTarget;
				$replacement = $_glossDataReplacement;
			}

			if(count($target) > 0) {
				foreach($target as $value){
					$orig[]="/\b" . $value . "\b/si";
				}

				if (count($orig)){
					$input=preg_replace($orig,$replacement,$input);
				}
			}
			#####################################################
		}

		return $input;
	}

	function formoutput($input){
	//function to prepare all string before we output to form (text field, text area, list/menu)
		if (is_array($input)){
			foreach ($input as $key=>$value){
				$input[$key]=formoutput($value);
			}
		}else{
			$input=stripslashes($input);
			$input=htmlentities($input);
		}

		return $input;
	}

	function query($sql){
		global $dbh;
		$is_insert = false; // to check whether the query is INSERT
		$last_insert_id = null; //Only return value if the field in the table is auto increment
		$exec_regex = preg_match("/^INSERT/i", trim($sql), $match);
		$select = preg_match("/^(SELECT|SHOW)/i", trim($sql), $matchSelect);

		if(in_array($matchSelect[0], $matchSelect) === false){

			$query=$dbh->exec($sql);

			if($query===FALSE){
				$error=$dbh->errorInfo();
				if(getconfig('SHOW_DEBUG')==TRUE){
					print "<b>Query:</b> $sql<br /><b>Error Message:</b> <i>$error[2]</i>";
					exit;
				}else{
					return FALSE;
				}
			}else{
				$is_insert = strtolower($match[0]) == 'insert' ? true : false;

				if($is_insert){
					$last_insert_id = $dbh->lastInsertId();
				}

				querylog($sql); // log the query

				if($is_insert){
					if($last_insert_id < 1){
						return TRUE;
					}else{
						return $last_insert_id;
					}
				}
				return TRUE;
			}
		} else {
			$query=$dbh->query($sql);
 			if(!is_object($query)){
				$error=$dbh->errorInfo();
				if(getconfig('SHOW_DEBUG')==TRUE){
					print "<b>Query:</b> $sql<br /><b>Error Message:</b> <i>$error[2]</i>";
				}
				else{
					return FALSE;
				}
			}
		}
		return $query;
	}

	function querylog($sql){
		global $dbh;
		global $now;

		#backupTable backupLastUpdate backupLastupload
		###### Log: for insert and update ########
		#if the query too long, cut for 200 chars and convert them to array
		$sql_clean_space=str_replace(array("  ",'`'),array(" ",''),$sql); //change double space to single space
		if(strlen($sql_clean_space) > 200){
			$sql_clean_space = substr($sql_clean_space, 0, 200); // get only 200 char
		}

		$sql_array=explode(" ", $sql_clean_space); //convert to array to get the command name, insert or update
		$now_date=$now;

		#save log
		if(strtolower($sql_array[0])=='insert' or strtolower($sql_array[0])=='delete'){
			#check for insert or update data on the row
			$sqlx = "SELECT * FROM backup_log WHERE backupTable='".$sql_array[2]."'";
			$query=$dbh->query($sqlx);
			if(is_object($query)){
				$check_row=$query->rowCount($query);

				if($check_row){
					$sql_log="UPDATE backup_log SET backupLastUpdate='$now' WHERE backupTable='". $sql_array[2] . "'";
				}else{
					$sql_log="INSERT INTO backup_log VALUES('".$sql_array[2]."', '$now_date','$now_date')";
				}

				$query_log=$dbh->exec($sql_log);
			}
		}elseif(strtolower($sql_array[0])=='update'){
			$sqlx = "SELECT * FROM backup_log WHERE backupTable='".$sql_array[1]."'";
			$query=$dbh->query($sqlx);
			if(is_object($query)){
				$check_row=$query->rowCount($query);

				if($check_row){
					$sql_log="UPDATE backup_log SET backupLastUpdate='$now' WHERE backupTable='". $sql_array[1] . "'";
				}else{
					$sql_log="INSERT INTO backup_log VALUES('".$sql_array[1]."', '$now_date','$now_date')";
				}

				$query_log=$dbh->exec($sql_log);
			}
		}
	}

	function fetch($query,$type='array'){
		if(!is_object($query)){
			global $dbh;
			$error=$dbh->errorInfo();
			if(getconfig('SHOW_DEBUG')==TRUE){
				print "<b>Query:</b> $sql<br /><b>Error Message:</b> <i>$error[2]</i>";
			}
			else{
				return FALSE;
			}
		}
		else{
			if($type=='array'){
				return $data=$query->fetch(PDO::FETCH_ASSOC);
			}else{
				return $data=$query->fetch(PDO::FETCH_NUM);
			}
		}
	}

	function rows($query){
		if(!is_object($query)){
			global $dbh;
			exit;
		}else{
			return $rows=$query->rowCount($query);
		}
	}

	function nextid($idfield,$tablename){
		//this function will try to get the next ID to be inserted to particular table
		$sqlspec="SELECT MAX($idfield) AS latestId FROM $tablename";
		$queryspec=query($sqlspec);
		$dataspec=fetch($queryspec);
		$latestId=$dataspec['latestId'];
		$nowId=1;
		if (!empty($latestId)){
			$nowId=$latestId+1;
		}
		return $nowId;
	}

	function errorlist($e) {
		//This function will return error list only.
		global $msg;

		if(is_numeric($e)){
			$e = _($msg[$e]);
		}

		return "<li>".$e."</li>";
	}

	function paging($options){
		//This function will do the necessary tasks to create the page i
		//some variables should be passed to this file and it will return the query results + the page iing to the calling page
		//Style 1 without number, style 2 with number in pagination
		foreach($options as $key=>$value){
			$$key=$value;
		}

		$urlquery = geturlquery();
		if($urlquery['query']){
			foreach($urlquery['query'] as $key=>$value){
				if($key!='pg'){
					$vars[]=$key.'='.$value;
				}
			}
			$var=@implode('&amp;',$vars);
		}

		if($pg=="" OR !is_numeric($pg)){
			$pg=1;
		}

		if($numPerPage=="" OR !is_numeric($numPerPage)){
			$numPerPage=20;
		}

		$offset=($pg-1)*$numPerPage;
		$kel=$total/$numPerPage;
		if($kel==floor($total/$numPerPage)){
			$page=$kel;
		}else{
			$page=floor($total/$numPerPage)+1;
		}
		$prev=$pg-1;
		$next=$pg+1;

		if($page>1){
			//Style 1 start
			if($style==1){
				$numlink="<table width=\"100%\" cellpadding=\"5\"><tr><td valign=\"middle\" width=\"45%\" align=\"right\">";
				if ($pg!=1){
					if($pg!=2){
						if($addquery){
							if($i==1){
								$filenamed=$filename;
							}
							else{
								$filenamed=$filename.'?'.($var ? $var.'&amp;':'').'pg='.$prev;
							}
						}else{
							$filenamed=str_replace('{pg}',$prev,$qualifier).(($customquery AND $var) ? '?'.$var:'');
						}
					}else{
						if($addquery){
							$filenamed=$filename.($var ? '?'.$var:'');
						}else{
							$filenamed=str_replace('{pg}',$prev,$filename).(($customquery AND $var) ? '?'.$var:'');
						}
					}
					//$numlink.="<a href='".$filenamed."'><img src=\"/assets/images/ico-prev.jpg\" alt=\"Previous\" width=\"40\" height=\"20\" align=\"middle\" border=\"0\" /></a> ";
					$numlink.="<a href='".$filenamed."' class=\"btn btn-xs btn-success\"><i class=\"fa fa-chevron-left\"></i> Prev</a> ";
				}else{
					//$numlink.="<img src=\"/assets/images/ico-prev-dim.jpg\" alt=\"Previous\" width=\"40\" height=\"20\" align=\"middle\" /> ";
					$numlink.="<button class=\"btn btn-xs btn-success disabled\"><i class=\"fa fa-chevron-left\"></i> Prev</button> ";
				}

				if ($pg!=$page){
					if($addquery){
						$filenamed=$filename.'?'.($var ? $var.'&amp;':'').'pg='.$next;
					}
					else{
						$filenamed=str_replace('{pg}',$next,$qualifier).(($customquery AND $var) ? '?'.$var:'');
					}

					//$numlink.="<a href=\"" . $filenamed ."\"><img src=\"/assets/images/ico-next.jpg\" alt=\"Next\" width=\"40\" height=\"20\" align=\"middle\" border=\"0\" /></a> ";
					$numlink.="<a href=\"" . $filenamed ."\" class=\"btn btn-xs btn-success\">Next <i class=\"fa fa-chevron-right\"></i></a> ";
				}
				else{
					//$numlink.="<img src=\"/assets/images/ico-next-dim.jpg\" alt=\"Next\" width=\"40\" height=\"20\" align=\"middle\" /> ";
					$numlink.="<button class=\"btn btn-xs btn-success disabled\">Next <i class=\"fa fa-chevron-right\"></i></button> ";
				}
				$numlink.="</td><td valign=\"middle\" align=\"left\">";

				$numlink.=" <strong>Halaman: </strong><select class=\"form-control input-sm select-pagination\" name=\"nextpage\" onChange=\"MM_jumpMenu('parent',this,1)\">";
				for ($i=1;$i<=$page;$i++){
					if($i>1){
						if($addquery){
							$filenamed=$filename.'?'.($var ? $var.'&amp;':'').'pg='.$i;
						}
						else{
							$filenamed=str_replace('{pg}',$i,$qualifier).(($customquery AND $var) ? '?'.$var:'');
						}
					}
					else{
						if($addquery){
							$filenamed=$filename.($var ? '?'.$var:'');
						}
						else{
							$filenamed=$filename;
						}
					}
					$numlink.="<option value=\"" . $filenamed."\"";
					if ($i==$pg){
						$numlink.=" selected=\"selected\"";
					}
					$numlink.=">" .  $i . "</option>";
				}
				$numlink.="</select> <strong> dari $page</strong>";
				$numlink.="</td></tr></table>";
			}
			//Style 1 end
			else{
			//Style 2 start
				$numlink.="<table width=\"100%\" cellpadding=\"5\"><tr><td>";
				$numlink.='<div class="paging">';
				if ($pg!=1){
					if($pg!=2){
						if($addquery){
							$filenamed=$filename.'?'.$var.'&amp;pg='.$prev;
						}else{
							$filenamed=str_replace('{pg}',$prev,$qualifier).(($customquery AND $var) ? '?'.$var:'');
						}
					}else{
						if($addquery){
							$filenamed=$filename.'?'.$var;
						}else{
							$filenamed=str_replace('{pg}',$prev,$filename).(($customquery AND $var) ? '?'.$var:'');
						}
					}
					$numlink.="<a class='page' href=\"" . $filenamed ."\">Prev</a>";
				}

				//Set range number to show
				//To make calculation not to be fail, if $page is 6, the range must be set to be 5.
				if($page<9){
					if($numPerPage==9){
						$range=20;
					}else{
						$range=20;
					}
			 	}else{
					//Default range is 7
					$range=7;
			 	}

				$range_min = ($range % 2 == 0) ? ($range / 2) - 1 : ($range - 1) / 2;
				$range_max = ($range % 2 == 0) ? $range_min + 1 : $range_min;
				$page_min = $pg - $range_min;
				$page_max = $pg + $range_max;

				$page_min = ($page_min < 1) ? 1 : $page_min;
				$page_max = ($page_max < ($page_min + $range - 1)) ? $page_min + $range - 1 : $page_max;
				if ($page_max > $page) {
						$page_min = ($page_min > 1) ? $page - $range + 1 : 1;
						$page_max = $page;
				}

				$page_min = ($page_min < 1) ? 1 : $page_min;

				if ( ($pg > ($range - $range_max)) && ($page > $range)){
					if($addquery){
						$filenamed=$filename.'?'.$var.'&amp;pg=1';
					}else{
						$filenamed=str_replace('{pg}',1,$qualifier).(($customquery AND $var) ? '?'.$var:'');
					}
					 $numlink .='<a title="First" href="'.$filenamed.'">1</a>';
				}

				if ( ($pg > ($range - $range_min+1)) && ($page > $range) ) {
					 $numlink .='<span>...</span>';
				}

				for ($i = $page_min;$i <= $page_max;$i++) {
					if ($i == $pg){
						$numlink .='<span>' . $i . '</span>';
					}else{
						if($addquery){
							$filenamed=$filename.'?'.$var.'&amp;pg='.$i;
						}else{
							if($i==1){
								$filenamed=$filename.(($customquery AND $var) ? '?'.$var:'');
							}
							else{
								$filenamed=str_replace('{pg}',$i,$qualifier).(($customquery AND $var) ? '?'.$var:'');
							}
						}
						$numlink.='<a href="'.$filenamed.'">'.$i.'</a>';
					}
				}

				if (($pg < ($page-$range_min-1)) && ($pg < ($page-$range_min))){
					$numlink  .='<span>...</span>';
				}

				if(($pg < ($page-$range_min))){
					if($addquery){
						$filenamed=$filename.'?'.$var.'&amp;pg='.$page;
						$numlink .='<a href="'.$filenamed.'">'.$page.'</a>';
					}else{
						$filenamed=str_replace('{pg}',$page,$qualifier).(($customquery AND $var) ? '?'.$var:'');
						$numlink .='<a href="'.$filenamed.'">'.$page.'</a>';
					}
				}

				if ($pg!=$page){
					if($addquery){
						$filenamed=$filename.'?'.$var.'&amp;pg='.$next;
					}else{
						$filenamed=str_replace('{pg}',$next,$qualifier).(($customquery AND $var) ? '?'.$var:'');
					}

					$numlink.="<a class='page' href=\"" . $filenamed ."\">Next</a>";
				}else{
					$numlink.=" ";
				}
				$numlink.='</div>';
				$numlink.="</td></tr></table>";
			}
			//Styles 2 end
		}
		//end of link generation
		print $numlink;
	}

	function convertdaytoid($dayoweek){
	//this function will convert day of week to Indonesian language
		switch ($dayoweek){
			case "Monday": $dayoweek="Senin"; break;
			case "Tuesday": $dayoweek="Selasa"; break;
			case "Wednesday": $dayoweek="Rabu"; break;
			case "Thursday": $dayoweek="Kamis"; break;
			case "Friday": $dayoweek="Jum'at"; break;
			case "Saturday": $dayoweek="Sabtu"; break;
			case "Sunday": $dayoweek="Minggu"; break;
		}
		return $dayoweek;
	}
	function convertdaytolang($dayoweek){
	//this function will convert day of week to Indonesian language
		switch ($dayoweek){
			case "Monday": $dayoweek= _("day_mounday"); break;
			case "Tuesday": $dayoweek= _("day_tuesday"); break;
			case "Wednesday": $dayoweek= _("day_wednesday"); break;
			case "Thursday": $dayoweek= _("day_thursday"); break;
			case "Friday": $dayoweek= _("day_friday"); break;
			case "Saturday": $dayoweek= _("day_saturday"); break;
			case "Sunday": $dayoweek= _("day_sunday"); break;
		}
		return $dayoweek;
	}

	function convertmonthtoid($month){
		//this function will convert day of week to Indonesian language
		if (is_numeric($month)){
			switch ($month){
				case 1: $month="Januari"; break;
				case 2: $month="Februari"; break;
				case 3: $month="Maret"; break;
				case 4: $month="April"; break;
				case 5: $month="Mei"; break;
				case 6: $month="Juni"; break;
				case 7: $month="Juli"; break;
				case 8: $month="Agustus"; break;
				case 9: $month="September"; break;
				case 10: $month="Oktober"; break;
				case 11: $month="November"; break;
				case 12: $month="Desember"; break;
			}
		}else{
			switch ($month){
				case "January": $month="Januari"; break;
				case "February": $month="Februari"; break;
				case "March": $month="Maret"; break;
				case "April": $month="April"; break;
				case "May": $month="Mei"; break;
				case "June": $month="Juni"; break;
				case "July": $month="Juli"; break;
				case "August": $month="Agustus"; break;
				case "September": $month="September"; break;
				case "October": $month="Oktober"; break;
				case "November": $month="Nopember"; break;
				case "December": $month="Desember"; break;
			}
		}
		return $month;
	}

	function convertmonthtolang($month){
		//this function will convert day of week to Indonesian language
		if (is_numeric($month)){
			switch ($month){
				case 1: $month=_("day_january"); break;
				case 2: $month=_("day_february"); break;
				case 3: $month=_("day_march"); break;
				case 4: $month= _("day_april"); break;
				case 5: $month=_("day_may"); break;
				case 6: $month=_("day_june"); break;
				case 7: $month=_("day_july"); break;
				case 8: $month=_("day_august"); break;
				case 9: $month=_("day_september"); break;
				case 10: $month=_("day_october"); break;
				case 11: $month= _("day_november"); break;
				case 12: $month=_("day_december"); break;
			}
		}else{
			switch ($month){
				case "January": $month= _("day_january"); break;
				case "February": $month= _("day_february"); break;
				case "March": $month= _("day_march"); break;
				case "April": $month= _("day_april"); break;
				case "May": $month= _("day_may"); break;
				case "June": $month= _("day_june"); break;
				case "July": $month= _("day_july"); break;
				case "August": $month= _("day_august"); break;
				case "September": $month= _("day_september"); break;
				case "October": $month= _("day_october"); break;
				case "November": $month= _("day_november"); break;
				case "December": $month= _("day_december"); break;
			}
		}
		return $month;
	}


	function convertidtomonth($month){
		//this function will convert month indonesia to numeric month
		switch ($month){
			case "januari": $month=1; break;
			case "februari": $month=2; break;
			case "maret": $month=3; break;
			case "april": $month=4; break;
			case "mei": $month=5; break;
			case "juni": $month=6; break;
			case "juli": $month=7; break;
			case "agustus": $month=8; break;
			case "september": $month=9; break;
			case "oktober": $month=10; break;
			case "nopember": $month=11; break;
			case "desember": $month=12; break;
		}

		return $month;
	}

	function uploadit($fname,$tmpfile,$subdir,$fileid,$sw,$sh,$mw,$mh,$lw=null,$lh=null,$specialprefix=null){
		//this function will upload and at the same time resize the image and return file
		//upload the file to 'images/correct directory/ddmmyyyy'
		//Param $fname is the real name of uploaded file
		//Param $fileid is the record id
		//Param $tmpfile is file temp in php temp dir
		//Param $specialprefix is the prefix name that will added to file name.

		//Populate file name which will be created
		$tmp=getext($fname,$fileid,$specialprefix);
		$lfname=$tmp['l'].'.'.$tmp['ext'];
		$mfname=$tmp['m'].'.'.$tmp['ext'];
		$sfname=$tmp['s'].'.'.$tmp['ext'];

		//Original filename
		$fname=$lfname;

		//Loop to create each file
		for($i=1; $i<=3;$i++){
			//resize the image
			if($i==1){
				$max_width=$lw;
				$max_height=$lh;
				$fname=$fname;
			}

			if($i==2){
				$max_width=$mw;
				$max_height=$mh;
				$fname=$mfname;
			  	chdir("../../");
			}

			if($i==3){
				$max_width=$sw;
				$max_height=$sh;
				$fname=$sfname;
				chdir("../../");
			}

			$heightWidth=getimagesize($tmpfile);
			$width=$heightWidth[0];
			$height=$heightWidth[1];
			$file=$tmpfile;

			list($firstdir,$nowdir)=explode("/",$subdir);
			if (!$nowdir){
				$nowdir=date('dmY');
				$targetdir="../assets/" . $firstdir . '/' . $nowdir . '/';
			}else{
				$targetdir="../assets/" . $subdir . '/';
			}

			//test whether targetdir exists
			if(!@opendir($targetdir) ){
				mkdir($targetdir);
			}

			chdir($targetdir);

			#################################################################
			# SourceCode originally written by phpBuilder.com forum member
			# Little modification done by Dedy (dedy at webby emedia dot net)
			#################################################################
			$x_ratio = $max_width / $width;
			$y_ratio = $max_height / $height;

			if (($width <= $max_width) && ($height <= $max_height)) {
				$tn_width = $width;
				$tn_height = $height;
			}else if (($x_ratio * $height) < $max_height) {
				$tn_height = ceil($x_ratio * $height);
				$tn_width = $max_width;
			}else {
				$tn_width = ceil($y_ratio * $width);
				$tn_height = $max_height;
			}
			##################################################################
			$imagetype=$heightWidth[2];
			//Image type ?
			if($imagetype==1){
				$src = imagecreatefromgif($file);
			}elseif($imagetype==2){
				$src = imagecreatefromjpeg($file);
			}elseif($imagetype==3){
				$src = imagecreatefrompng($file);
			}

			$dst = imagecreatetruecolor($tn_width,$tn_height);

			//Create transparance for gif and png
			if(($imagetype== 1) OR ($imagetype==3)){
				imagealphablending($dst, false);
				imagesavealpha($dst,true);
				$transparent = imagecolorallocatealpha($dst, 255, 255, 255, 127);
				imagefilledrectangle($dst, 0, 0, $tn_width, $tn_height, $transparent);
			}

			imagecopyresampled($dst, $src, 0, 0, 0, 0,$tn_width,$tn_height,$width,$height);

			//Create image accord to type
			if($imagetype==1){
				imagegif($dst, $fname);
			}elseif($imagetype==2){
				imageinterlace($dst, true); #enable high compress
				imagejpeg($dst, $fname, IMAGE_QUALITY);
			}elseif($imagetype==3){
				imagepng($dst, $fname);
			}

			imagedestroy($src);
			imagedestroy($dst);
			#################################################################
		}

		return array("dir"=>$nowdir,"filename"=>$lfname);
	}

	function createtimestamp($day,$month,$year){
		//this function, as the name implies, will return a timestamp from a given day,month and year data
		$timestamp=mktime(0,0,0,$month,$day,$year);
		return $timestamp;
	}

	function checkduplicate($table,$field,$value){
		//this function will check whether data is already existed in the database
		$sql="SELECT COUNT(*) AS exist FROM " . $table . " WHERE " . $field . "='" . $value . "'";
		$query=query($sql);
		$data=fetch($query);
		$exist=$data['exist'];
		return $exist;
	}

	function checkDuplicateRecord($table,$whereClause){
		//this function will check whether data is already existed in the database
		$sql="SELECT COUNT(*) AS exist FROM `$table` WHERE $whereClause";
		$query=query($sql);
		$data=fetch($query);
		$exist=$data['exist'];
		return $exist;
	}

	function genthumb($file,$sizetype){
		//this function will return the thumbnail file name for a given picture file name
		//param $sizetype there are m,s
		list($name,$ext)=explode(".",$file);
		$tname=$name."_". $sizetype."." . $ext;
		return $tname;
	}

	function warnwebmaster($file,$error){
		//warn the webmaster of some critical errors
		//send WARNING email to the web developer
		$webmasterEmail=SITE_DEVELOPER_EMAIL;
		$subject="[SQL ERROR]" . SITE_NAME;

		$message="Error in file: ". $file . "\n";
		$message.="Specific Error Message: " . $error . "\n\n";
		$message.="Please review the information above and if any serious problems happen, check the server or the file!\n";
		$message.="Thanks.";
		$headers="FROM: " . SITE_NAME . " Error System <no-reply@" . getconfig("SITE_SHORT_URL") . ">";
		sendmail($webmasterEmail,$subject,$message,$headers);
		return true;
	}

	function codegen($length){
	//as the name implies, this function will randomly generate some code of user-specified length
	//the code will contain the combination of uppercase, lowercase and numbers
	//it is also served as random password generator. (^_^);
		$source="abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789";
		for ($i=0;$i<$length;$i++){
			$sourceLen=strlen($source);
			$randNo=rand(1,$sourceLen-1);
			$code.=substr($source,$randNo,1);
		}
		return $code;
	}

	function nextsort($table,$section,$sectionval,$sortfield){
		$sql="SELECT MAX($sortfield) AS lastnum FROM $table WHERE $section='$sectionval'";
		$query=query($sql);
		$data=fetch($query);
		$lastnum=$data['lastnum']+1;
		return $lastnum;
	}

	function permalink($word,$id=0){
		// This funtion will return permalink with maximum <=100 char
		//Allow only a-z A-Z 0-9 or - and _
		$max_return_word=100;
		$word=str_replace(' ','-',strtolower(trim(preg_replace("/[^a-zA-Z0-9 _]/", '', $word))));

		//Only create one if the "-" more than one
		if(preg_match('/-{2,}/',$word)){
			$word=preg_replace("/-{2,}/","-",$word);
		}

		//If the $word >= 100 char, cut the $word at the very last "-".
		$countword=strlen($word);
		if($countword>=$max_return_word){
			$word=substr($word,0,$max_return_word);

			$laststrip=strrpos($word,"-");
			if($countword>=$max_return_word){
				$word=substr($word,0,-($max_return_word-$laststrip));
			}
		}
		if ($id>0){
			$word=$id."-".$word;
		}
		return $word;
	}

	function getext($filename,$fileid,$specialprefix=null){
		//This function will return array that contain the name of file and sign it as each size
		$fext=$filename;

		if($specialprefix){
			$site_name=permalink(SITE_NAME,0).'_'.$specialprefix;
		}else{
			$site_name=permalink(SITE_NAME,0);
		}

		$fext=strtolower(end(explode('.',$filename)));
		$filename=str_replace(".","",$filename);
		$filename=strtolower(codegen(5)).'_'.$fileid;

		$original = strtolower($site_name."_".$filename);
		$medium   = strtolower($site_name."_".$filename.'_m');
		$small    = strtolower($site_name."_".$filename.'_s');
		$result   = array("l"=>$original,"m"=>$medium,"s"=>$small,"ext"=>$fext);

		return $result;
	}

	function stripquote($word){
		$result=preg_replace("/[\"|\']/","",$word);
		return $result;
	}

	function getval($fieldToDisplay,$table,$fieldReference,$reference=null){
		#this function allow us get data by one line of query
		# Modified: 12-9-2013
		#if not pass $reference, means the $fieldReference containt 'WHERE clause'

		#return error variable if fail
		global $error;
		$fieldToDisplay = str_replace(" ", "", $fieldToDisplay);

		if(is_null($reference)){
			$clause = $fieldReference;
		}else{
			$clause = $fieldReference."='".$reference."'";
		}

		$sql='SELECT '.$fieldToDisplay.' FROM `'.$table.'` WHERE '.$clause;
		$query=query($sql);

		if(!$query){
			if(!$error){
				$error = errorlist(3);
			}else{
				$error .= errorlist(3);
			}

			return $error;
		}

		$data=fetch($query);
		$fields=explode(',',$fieldToDisplay);
		if(count($fields)!=1){
			foreach($fields as $key){
				$result[$key]=$data[$key];
			}
		}
		else{
			$result=$data[$fieldToDisplay];
		}
		return $result;
	}

	function formatdate($dateformat, $tstamp, $lang = null){
		#set date by selected language
		if($lang == null){
			$lang = $_COOKIE['lang'];
		}

		$date = date($dateformat, $tstamp);

		if(strpos($dateformat, 'l') !== false or strpos($dateformat, 'D') !== false){
			$tday = date('l', $tstamp);
			$day = ($lang == 'en_US' ? $tday:convertdaytoid($tday));
			$date = str_ireplace($tday, $day, $date);

			#three letter format
			if(strpos($dateformat, 'D') !== false){
				$tday = substr(date('D', $tstamp), 0, 3);
				$shortday = substr($day, 0, 3);
				$date = str_ireplace($tday, $shortday, $date);
			}
		}

		if(strpos($dateformat, 'F') !== false or strpos($dateformat, 'M') !== false){
			$tmonth = date('F', $tstamp);
			$month = ($lang == 'en_US' ? $tmonth:convertmonthtoid($tmonth));
			$date = str_ireplace($tmonth, $month, $date);

			#three letter format
			if(strpos($dateformat, 'M') !== false){
				$tmonth = substr(date('M', $tstamp), 0, 3);
				$shortmonth = substr($month, 0, 3);
				$date = str_ireplace($tmonth, $shortmonth, $date);
			}
		}

		return $date;
	}

	function getOptionList($table,$id,$alias,$whereClause=FALSE,$orderBy=FAlSE,$selectedItem=FALSE){
		$sql="SELECT * FROM $table".($whereClause ? " WHERE $whereClause":'').($orderBy ? " ORDER BY $orderBy":'');
		$query=query($sql);
		while($data=fetch($query)){
			$sel=($data[$id]==$selectedItem ? "selected=\"selected\"" : "");
			print "<option value=\"" .$data[$id] ."\" $sel>" .ucwords($data[$alias]) ."</option>";
		}
		return true;
	}

	//addition by adien.globalnet@gmail.com
	function format_events_data($intdate1, $intdate2){
		// convert int ke tahun
        $difyear1=date("Y",$intdate1);
		$difyear2=date("Y",$intdate2);
        // convert int ke bulan
        $difmonth1=date("F",$intdate1);
		$difmonth2=date("F",$intdate2);
        // convert int ke hari
        $difday1=date("d",$intdate1);
		$difday2=date("d",$intdate2);

		if($difyear1==$difyear2){
			if($difmonth1==$difmonth2){
				if($difday1==$difday2){
					$fdate=$difday1." ".$difmonth1." ".$difyear1;
				}else{
					$fdate=$difday1."-".$difday2." ".$difmonth1." ".$difyear1;
				}
			}else{
				$fdate=$difday1." ".$difmonth1."-".$difday2." ".$difmonth2." ".$difyear1;
			}
		}else{
			$fdate=$difday1." ".$difmonth1." ".$difyear1."-".$difday2." ".$difmonth2." ".$difyear2;
		}

		//print hours, minutes
		$ftime=date("H:i",$intdate1)."-".date("H:i",$intdate2);
		return print $fdate." | ".$ftime." wib";
	}

	function countdata($table,$whereClause=null){
		//this function will check whether data is already existed in the database or can be used to count record
		$sql="SELECT COUNT(*) AS exist FROM $table".($whereClause ? " WHERE $whereClause":"");
		$query=query($sql);
		$data=fetch($query);
		return $data['exist'];
	}

	function getAds($page,$channelId,$limit,$indeks='0'){
		$limit=($limit ? "LIMIT $limit" : "");
		$now=mktime(23,59,59,date('n,d,Y'));

		//Page set here
		if($page=="home"){
			$where="and a.home='y'";
		}elseif($page=="other"){
			$where="and a.other='y'";
		}

		//Wheter the page exist?
		if($where){
			//Check if the data is retrieved by a single index that has been specified
			if($indeks>0){
				$sql="SELECT a.*,b.* FROM adscontent a,adschannel b WHERE a.adschannel='$channelId' and a.adsDisplay='y' and a.adschannel=b.channelId and (adsExpired='0' or adsExpired>=$now) $where ORDER BY a.adsOrder ASC $limit OFFSET $indeks";
			}else{
				//If channelId is 1, Ads will be order by asc from adsId. Only the lowest Id will be showed.
				$sql="SELECT a.*,b.* FROM adscontent a,adschannel b WHERE a.adschannel='$channelId' and a.adsDisplay='y' and a.adschannel=b.channelId and (adsExpired='0' or adsExpired>=$now) $where ORDER BY a.adsOrder ASC $limit";
			}

			$query=query($sql);
			while($data=fetch($query)){
				//HTML Style here
				$fixheight=$data['channelLength'];
				$fixwidth=$data['channelWidth'];

				if($data['adsType']=="image"){
					$img="<img class=\"advertisement\" title=\"" .$data['adsTitle'] ."\" width=\"".$fixwidth."\"  height=\"".$fixheight."\" src=\"/assets/ads/" .$data['adsDir'] ."/" .$data['adsSource'] ."\" alt=\"" .$data['adsTitle'] ."\" />";
					if($data['adsURL']){
						print "<a href=\"".$data['adsURL']."\" target=\"_blank\">$img</a>";
					}else{
						print "$img";
					}
				}else{
					//get the size
					$size=getimagesize("../assets/ads/" .$data['adsDir'] ."/" .$data['adsSource']);

					print "<object classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0\" ".$size[3]." id=\"mymoviename\">";
					print "<param name=\"movie\" value=\"../assets/ads/" .$data['adsDir'] ."/" .$data['adsSource'] ."\"/>";
					print "<param name=\"quality\" value=\"high\" />";
					print "<param name=\"bgcolor\" value=\"#ffffff\" />";
					print "<embed src=\"/assets/ads/" .$data['adsDir'] ."/" .$data['adsSource'] ."\" quality=\"high\" bgcolor=\"#ffffff\" ".$size[3]." name=\"mymoviename\" align=\"center\" type=\"application/x-shockwave-flash\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" class=\"advertisement\">";
					print "</embed>";
					print "</object> ";
				}
			}
		}
	}

	function is_mobile(){
		global $license_0195;
		$lc = unserialize(strdecode($license_0195));

		if(SITE_MOBILE=='on' AND $lc['mobile'] == 'on'){
			$is_mobile = mobile_device_detect();
			if(is_array($is_mobile)){
				#special additional control for
				if(preg_match('/ipad|tablet|playbook|xoom|kindle|sch-i800/i',$_SERVER['HTTP_USER_AGENT'])){
					$mobile = false;
				}
				elseif(preg_match('/android/i',$_SERVER['HTTP_USER_AGENT'])){
					if(preg_match('/mobile/i',$_SERVER['HTTP_USER_AGENT'])){
						$mobile = $is_mobile[0];
						$mobiletype = $is_mobile[1];
					}
					else{
						$mobile = false;
					}
				}
				else{
					$mobile = $is_mobile;

				}
			}
			else{
				$mobile = $is_mobile;
			}

			if(!$_COOKIE['cross_device']){
				if($mobile){
					if($_SERVER['HTTP_HOST']==strtolower(getconfig('SITE_URL')) or $_SERVER['HTTP_HOST']==strtolower(getconfig('SITE_SHORT_URL'))){
						$uri=str_replace("www.","",strtolower(getconfig('SITE_URL')));
						$uri = "http". ($_SERVER['HTTPS'] ? "s" : null) ."://" .strtolower(getconfig('MOBILE_URL')) .$_SERVER['REQUEST_URI'];
						header("location:$uri");
						exit;
					}
				}
				else{
					if($_SERVER['HTTP_HOST']==getconfig('MOBILE_URL')){
						$uri = "http". ($_SERVER['HTTPS'] ? "s" : null) ."://" .strtolower(getconfig('SITE_URL')) . $_SERVER['REQUEST_URI'];
						header("location:$uri");
						exit;
					}
				}
			}
		}
		else{
			if($_SERVER['HTTP_HOST']!=strtolower(getconfig('SITE_URL'))){
				header('location:http://'.strtolower(getconfig('SITE_URL')));
				exit;
			}
		}
	}

	function clickable($url){
		//This source code originally written by user on php.net
		//This function will return the text with a format that can display a link
		$url  = str_replace("\\r","\r",$url);
		$url  = str_replace("\\n","\n<br />",$url);
		$url  = str_replace("\\n\\r","\n\r",$url);

		$in=array(
		'`((?:https?|ftp)://\S+[[:alnum:]]/?)`si',
		'`((?<!//)(www\.\S+[[:alnum:]]/?))`si'
		);
		$out=array(
		'<a href="$1" rel="nofollow">$1</a> ',
		'<a href="http://$1" rel=\"nofollow\">$1</a>'
		);
		return preg_replace($in,$out,$url);
	}


	function strtoimage($msg, $font = "verdana.ttf"){
		//This function will convert string to image
		header("Content-type: image/jpg");
		$width = 0;
		$height = 0;
		$bounds = array();
		$image = "";

		//File for font locate at /modz dir
		$font = '../assets/font/'.$font;

		//determine bounding box.
		$bounds = ImageTTFBBox(9, 0, $font, $msg);
		$width = abs($bounds[4]-$bounds[6]);
		$height = abs($bounds[7]-$bounds[1]);

		$image = imagecreate($width+(0*2)+1,$height+(0*2)+1);
		$background = ImageColorAllocate($image,255,255,255);
		$foreground = ImageColorAllocate($image,21,21,21);

		//render the image
		ImageTTFText($image, 9, 0, 0, 10, $foreground, $font, $msg);

		//output PNG object.
		imagejpeg($image);
		imagedestroy($image);
	}

	function sendmail($emailto, $emailsubject, $emailmsg='', $emailtype = 'html', $emailhead='', $emailattach=''){
		//This function will queuing email before send it.
		//Will not re-queue data to table if the email and content is exist
		//You can count the successful query by the return value;
		global $now;

		//Checking whether same data exist in mail_queue and email_blacklist
		$isemailexist = countdata("email_queue","emailMsg='$emailmsg' AND emailTo='$emailto'");
		$blackemail   = countdata("email_blacklist","blackEmail='$emailto'");

	 	if($isemailexist<1 and $blackemail<1){
			#upload file
			if (!empty ($emailattach)){
				$nowdir = date('dmY');

				$targetFile = end(explode('/', $emailattach));
				$targetDir = realpath(dirname(__file__).'/..').'/assets/emailqueue/'.$nowdir;
				@mkdir($targetDir);

				#make it unique
				if(file_exists($targetDir.'/'.$targetFile)){
					list($origName, $origExt) = explode('.', $targetFile);
					$ext = strtolower(end(explode('.', $targetFile)));

					$targetFile = $origName.'_'.strtolower(codegen(4)).'.'.$ext;
				}

				@copy($emailattach, $targetDir.'/'.$targetFile);
			}

			$nextid = nextid("emailId","email_queue");
			$sql = "INSERT INTO email_queue VALUES ('$nextid','$emailto','$emailsubject','$emailmsg','$emailtype','$emailhead','$now','0','n','$nowdir','$targetFile', '$ext')";
			$query = query($sql);
		}

		if($query){
			return true;
		}else{
			return false;
		}
	}

	function readmore($sentences,$limit,$readmorelink=""){
		//This function will check and reduce long user comment
		//and also return readmore link if it's a long senteces accord to the it's word limit
		$cutoff=substr($sentences, 0, $limit);
		$addtoarray=explode(' ', $cutoff);
		$lenaftercut=(count($addtoarray)-1);//Will cut off 1 word, so does not print truncated word
		$finalword=implode(' ', array_slice($addtoarray, 0, $lenaftercut));

		if(strlen($sentences)>$limit){
			return $finalword.$readmorelink;
		}else{
			return $sentences;
		}
	}

	function sendWithAttach($to,$subject,$message,$file){
	 //This function will send email with attachment
	  if (strtoupper(substr(PHP_OS,0,3)=='WIN')) {
			$eol="\r\n";
		} elseif (strtoupper(substr(PHP_OS,0,3)=='MAC')) {
			$eol="\r";
		} else {
			$eol="\n";
		}

		//change this to path where generated pdf exist
		$fullpathfile="generatedpdf/".$file;

		$handle=fopen($fullpathfile, 'rb');
		$f_contents=fread($handle, filesize($fullpathfile));
		$f_contents=chunk_split(base64_encode($f_contents));
		$f_type=filetype($fullpathfile);
		fclose($handle);

		//Common Headers
		$headers  = 'Webmaster <'.EMAIL_ADDRESS.'>'.$eol;
		$mime_boundary=md5(time());


		// Attachment
		if (!empty($file)) {
			$msg  = "";
			$msg .= "--".$mime_boundary.$eol;
			$msg .= "Content-Type: ".$f_type."; name=\"".$file."\"".$eol;
			$msg .= "Content-Transfer-Encoding: base64".$eol;
			$msg .= "Content-Disposition: attachment; filename=\"".$file."\"".$eol.$eol;
			$msg .= $f_contents.$eol.$eol;
		}
		// Text Version
		if(!empty($message)){
			$msg .= "--".$mime_boundary.$eol;
			$msg .= "Content-Type: text/plain; charset=iso-8859-1".$eol;
			$msg .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
			$msg .=$message;
		}
		//check again
		if(sendfastemail($to,$subject,$msg,$headers)){
			return true;
		}else{
			return false;
		}
	}

	function watermark($imagesrc){
		//This void function will re-create and replace the picture with marked picture when it is uploaded
		//Support for gif,jpg and png image file type
		$watermarksrc='../assets/images/'.WATERMARK_IMAGE;
		$pos=WATERMARK_POSITION;
	  	$savefile=$imagesrc;
		$padding=5;

		//Initialize required images
		$watermark = imagecreatefrompng($watermarksrc);

		//Image file type ?
		$imagetype=getimagesize($imagesrc);
		if($imagetype[2]==1){
			$image = imagecreatefromgif($imagesrc);
		}elseif($imagetype[2]==2){
			$image = imagecreatefromjpeg($imagesrc);
		}elseif($imagetype[2]==3){
			$image = imagecreatefrompng($imagesrc);
		}

		//Let the png image to be transparent
	 	imageAlphaBlending($watermark, false);
		imageSaveAlpha($watermark, true);
		$watermark_width = imagesx($watermark);
		$watermark_height = imagesy($watermark);
		$image_width= imagesx($image);
		$image_height=imagesy($image);

		//The watermark will appear at main image
		//Here is the position control for watermark
		//it is will be use the watermark posision in website settings as reference
		if($pos=="1"){
			//Kiri atas
			//Width
			$dest_x = $padding;
			//Height
			$dest_y = $padding;
		}elseif($pos=="2"){
			//Tengah atas
			$dest_x = $image_width/2 - $watermark_width/2;
			$dest_y = $padding;
		}elseif($pos=="3"){
			//Kanan atas
			$dest_x =  $image_width-($watermark_width+$padding);
			$dest_y =  $padding;
		}elseif($pos=="4"){
			//Tengah
			$dest_x = $image_width/2 - $watermark_width/2;
			$dest_y = $image_height/2 - $watermark_height /2;
		}elseif($pos=="5"){
			//Kiri bawah
			$dest_x = $padding;
			$dest_y =  $image_height-($watermark_height-$padding);
		}elseif($pos=="6"){
			//Tengah bawah
			$dest_x = $image_width/2 - $watermark_width/2;
			$dest_y = $image_height-($watermark_height-$padding);
		}elseif($pos=="7"){
			//Kanan bawah
			$dest_x = $image_width-($watermark_width+$padding);
			$dest_y = $image_height-($watermark_height-$padding);
		}

		//Create transparance for gif only
		if(($imagetype[2]==1)){
			$getrealcolor = imagecreatetruecolor($image_width,$image_height);
			imagecopy($getrealcolor, $image, 0, 0, 0, 0,$image_width, $image_height);
			$image = $getrealcolor;
		}

		//Create and replace image with new watermarked image
		imagecopy($image, $watermark, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height);

		//Create image accord to type
		if($imagetype[2]==3){
			imagepng($image,$savefile);
		}else{
			imagejpeg($image,$savefile);
		}
	 	imagedestroy($image);
		imagedestroy($watermark);
	}

	function articlerss(){
		//This function will create new file at root dir named feed.xml
		$string='<?xml version="1.0" encoding="ISO-8859-1" ?>'."\n";
		$string.='<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">'."\n";
		$string.='<channel>'."\n";
		$string.='<atom:link href="http://'.getconfig('SITE_URL').'/feed.xml" rel="self" type="application/rss+xml" />'."\n";
		$string.='<title>'.SITE_NAME.' - Articles</title>'."\n";
		$string.='<link>http://'.getconfig('SITE_URL').'</link>'."\n";
		$string.='<description>RSS '.SITE_NAME.' - '.SITE_TITLE.'</description>'."\n";
		$string.='<image>'."\n";
		$string.='<url>http://'.getconfig('SITE_URL').'/assets/interface/mascot.png</url>'."\n";
		$string.='<title>'.SITE_NAME.'</title>'."\n";
		$string.='<link>http://'.getconfig('SITE_URL').'</link>'."\n";
		$string.='</image>'."\n";
		$string.='<language>en-us</language>'."\n";
		$string.='<copyright>2012 Copyright. '.SITE_NAME.'. All rights reserved.</copyright>'."\n";

		$sql="SELECT * FROM newsinfo ORDER BY newsId DESC LIMIT 50";
		$query=query($sql);
		while ($data = fetch($query)){
			$category= getval("catPermalink", "newsinfo_cat", "catId", $data['catId']);
			$URL='http://'.getconfig('SITE_URL').'/article/'.$category.'/'.$data['newsPermalink'].'/';
			$data=output($data);

			$string.='<item>'."\n";
			$string.='<title><![CDATA['.strip_tags($data[newsTitle]).']]></title>'."\n";
			$string.='<link>'.$URL.'</link>'."\n";
			$string.='<description><![CDATA[<img align="left" width="50"  hspace="5" src="http://'.getconfig('SITE_URL').'/assets/news/'.$data['newsDir'].'/'.$data['newsPicThumbs'].'"/>'.$data[newsHeadline].']]></description>'."\n";
			$string.='<pubDate>'.date("D, d M Y H:i:s O",$data[newsAddedOn]).' </pubDate>'."\n";
			$string.='<guid>'.$URL.'</guid>'."\n";
			$string.='</item>'."\n";
			}

			$string.='</channel>'."\n";
			$string.='</rss>'."\n";

		$file="../feed.xml";
		@chmod($file, 0777);
		$fp=fopen($file,"w");
		fwrite($fp,$string);
		fclose($fp);
		@chmod($file, 0644);
	}

	function sendstrviaCURL($url,$data,$return='httpresponse',$parse=true){
		if($parse==true){
			$urlquery=parse_url($url);
			if($urlquery['query']){
				$urlquerys=explode('&',$urlquery['query']);
				foreach($urlquerys as $values){
					list($key,$value)=explode('=',$values);
					$data[$key]=$value;
				}
				$url='http://'.$urlquery['host'].$urlquery['path'];
			}
		}
		$data=json_encode($data);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,TRUE);
		curl_setopt($ch, CURLOPT_HTTPHEADER,array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data))
		);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$result=curl_exec($ch);
		if($return=='httpresponse'){
			$result=curl_getinfo($ch, CURLINFO_HTTP_CODE);
		}
		curl_close($ch);
		return $result;
	}

	function sendfastemail($to, $subject,$body,$bodyhtml,$header=''){
		//Default email header
		if(empty($header)){
			$from     = SITE_EMAIL;
			$fromname = SITE_NAME;
		}else{
			//Header is set, so explode it as its separator, exp : judy#<judy@example.com>
			list($fromname,$from)=explode("#",$header);
		}

		//plaintext or HTML?
		//Usually it's will be used to send mail with another email address, such as guest book.
		if($from!=SITE_EMAIL){
			$headers="From: $fromname <$from>";
			$body=$body;
		}else{
			//Only internal system/admin able to send email with HTML header format
			$headers= "From: $fromname <$from>\r\n";
			$headers.="MIME-Version: 1.0\r\n";
			$headers.="Content-Type: text/html; charset=ISO-8859-1\r\n";
			$body=$bodyhtml;
		}

		$result=mail($to,$subject,$body,$headers);
		if ($result){
			return true;
		}else{
			return false;
		}
	}

	function getYoutubeId($url){
		parse_str(parse_url($url, PHP_URL_QUERY), $variables);
		return $variables['v'];
	}

	function strencode($str, $salt=null){
		$encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, ($salt ? $salt:getconfig('CMS_SALT_STRENCODE')), $str, MCRYPT_MODE_ECB);
		$encrypted = base64_encode($encrypted);
		return urlencode(trim($encrypted));
	}

	function strdecode($str, $salt=null){
		if(preg_match('/%/', $str)){
			$str = urldecode(trim($str));
		}
		$str = base64_decode($str);
		$decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, ($salt ? $salt:getconfig('CMS_SALT_STRENCODE')), $str, MCRYPT_MODE_ECB);
		return trim($decrypted);
	}

	function geturlquery(){
		$url = $_SERVER['REQUEST_URI'];
		$url = parse_url($url);
		$path = $url['path'];
		if($url['query']){
			$url = explode('&',$url['query']);
			foreach($url as $keys){
				list($key,$value)=explode('=',$keys);
				$query[$key] = $value;
			}
		}
		return array('path'=>$path, 'query'=>$query);
	}

	function pageviewnow($pagetype=null,$pagetyleid=0){
		//This function will count the visitor for each opened page.
		global $now;
		if(!isset($_COOKIE['pageviewnow'])){
			setcookie("pageviewnow",true,time()+(5*60));
			$remoteip=visitor_ip();
			$pageid=nextid("pageId","pageviewnow");
			$requesturi="http://".cleanup($_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);
			$sql="INSERT INTO pageviewnow VALUES ($pageid,'$requesturi','$remoteip',$now,'$pagetype',$pagetyleid)";
			$query=query($sql);

			setcookie("pageidnow",$pageid,0,"/");
		}

		if ($_COOKIE['pageviewnow']){
			$sql="UPDATE pageviewnow SET pageLastVisit=$now WHERE pageId='" . $_COOKIE['pageidnow']."'";
			$query=query($sql);
		}

		//delete all 'pageviewnow' data if more than 5 mins ago
		$minsago=$now-(1*60);
		$sql="DELETE FROM pageviewnow WHERE pageLastVisit<'" . $minsago."'";
		$query=query($sql);
	}

	function ganalytics(){
		if($_SERVER['HTTP_HOST'] == strtolower(getconfig('SITE_URL'))){
			$ga_url = getconfig('SITE_URL');
		}
		else{
			$ga_url = getconfig('MOBILE_URL');
		}

		//This function will print the Google Analytics js script
		print "
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', '".getconfig('GA_PROPERTY_ID')."', 'auto');
		  ga('send', 'pageview');
		</script>
		";
	}

	function get404($ref='desktop'){
		if($ref=='desktop'){
			header('location:/err/404/');
		}
		else{
			print '<script type="text/javascript">window.location="/err/404/";</script>';
		}
		exit;
	}

	function locale($data, $table, $whereClause){
		#this function is used when using multilingual feature
		global $license_0195;
		$lc = unserialize(strdecode($license_0195));
		if($_COOKIE['lang']!=$lc['language_default'] AND in_array($_COOKIE['lang'], $lc['language'])){
			$sql = "SELECT * FROM {$table}_lang WHERE $whereClause AND lang='".$_COOKIE['lang']."'";
			$query = query($sql);
			if(rows($query)){
				$result = fetch($query);
				$data = array_merge($data,$result);
			}
		}
		return $data;
	}

	function localename($lang){
		$code = array(
			'id_ID'=>'Indonesia',
			'en_US'=>'Inggris'
		);
		return $code[$lang];
	}

	function cli($cmd, $multithread = TRUE) {
		if(substr($cmd,0,3)=='php'){
			if(strtolower(substr(php_uname(), 0, 7)) == 'windows'){
				if(getconfig('PHP_CLI_FILENAME') == ''){
					$cmd = ($_SERVER['PHPRC'] ? $_SERVER['PHPRC'].DIRECTORY_SEPARATOR:'') .$cmd;
				}
			}
			else{
				$charlen = strlen($cmd);
				$cmd = substr($cmd, 4, $charlen);
				$cmd = getconfig('PHP_CLI_FILENAME') ." $cmd";
			}
		}

		if($multithread == TRUE){
			if(strtolower(substr(php_uname(), 0, 7)) == 'windows'){
				pclose(popen('start /B '. $cmd, 'r'));
			}
			else {
				exec($cmd . ' > /dev/null 2>&1 &');
			}
		}
		else{
			exec($cmd,$output,$err);
			return array($output,$err);
		}
	}

	function createsitemap($sitemapconfig){
		//this function will creata sitemap file.
		global $now;
		global $webprotocol;
		$sitemaptag='<?xml version="1.0" encoding="UTF-8"?>
					<urlset
					  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
					  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
					  xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
						  http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';

		//check the sitemap type, navigation is website nav or articlelike for article record.
		if($sitemapconfig['sitemaptype']=='navigation'){
			$sitemaptag.="<url>
						  <loc>".$webprotocol.getconfig('SITE_URL')."/</loc>
						  <lastmod>".date('Y-m-d',$now)."</lastmod>
						  <changefreq>".$sitemapconfig['changefreq']."</changefreq>
						  <priority>1.00</priority>
						</url>";
		}
		$sitemaptag.=$sitemapconfig['url'];
		$sitemaptag.="</urlset>";

		$file=realpath(dirname(__file__).'/..').'/sitemap/'.$sitemapconfig['filename'];
		@chmod($file, 0777);
		$fp=@fopen($file,"w");
		@fwrite($fp,$sitemaptag);
		@fclose($fp);
		@chmod($file, 0644);
	}

	function minify($buffer){
		$regex = array(
			"`^([\t\s]+)`ism"=>'',
			"`^\/\*(.+?)\*\/`ism"=>"",
			"`([\n\A;]+)\/\*(.+?)\*\/`ism"=>"$1",
			"`([\n\A;\s]+)//(.+?)[\n\r]`ism"=>"$1", #sould be $i\n
			"`(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+`ism"=>"", #should be \n
			'#/\*.*?\*/#si' => '', #css + js comment
			'/\>[^\S ]+/s' => '>', #strip whitespace after tags, except space
			'/[^\S ]+\</s' => '<', #strip whitespace before tags, except space
			'/(\s)+/s' => '\\1', #shorten multiple whitespace squence
		);

		#url resource rule
		if(getenv('HTTP_HOST') == getconfig('MOBILE_URL')){
			#mobile
			if(getconfig('ASSETS_URL') == '' or getconfig('ASSETS_URL') == '/assets'){
				$url_resource = 'http://'.getconfig('SITE_URL').'/assets';
			}
			else{
				$url_resource = getconfig('ASSETS_URL');
			}

			$buffer = preg_replace('/href\=\"(\/assets|\.\.\/assets)/','href="'.$url_resource, $buffer);
			$buffer = preg_replace('/src\=\"(\/assets|\.\.\/assets)/','src="'.$url_resource, $buffer);
		}
		else{
			#desktop
			if(getconfig('ASSETS_URL') != '' and getconfig('ASSETS_URL') != '/assets' and getconfig('ASSETS_URL') != 'http://'.getconfig('SITE_URL').'/assets'){
				$url_resource = getconfig('ASSETS_URL');
				$buffer = preg_replace('/src\=\"(\/assets|\.\.\/assets|http\:\/\/'.getconfig('SITE_URL').'\/assets)/','src="'.$url_resource, $buffer);
			}
		}

		#minify if minify enabled
		if(getconfig('MOD_MINIFY') == TRUE){
			$result = preg_replace(array_keys($regex), $regex, $buffer);
		}
		else{
			$result = $buffer;
		}

		return $result;
	}

	function minify_js($buffer){
		/*
		#unrevelead regex rule
		$regex = array(
			"/(\/\*([\s\S]*?)\*\/)|(\/\/(.*)$)/gm" => '', #strip js comment
		);
		preg_replace(array_keys($regex), $regex, $buffer);*/

		#url resource rule
		if(getenv('HTTP_HOST') == getconfig('MOBILE_URL')){
			#mobile
			if(getconfig('ASSETS_URL') == '' or getconfig('ASSETS_URL') == '/assets'){
				$url_resource = 'http://'.getconfig('SITE_URL').'/assets';
			}
			else{
				$url_resource = getconfig('ASSETS_URL');
			}

			$buffer = preg_replace('/src\=\"(\/assets|\.\.\/assets)/','src="'.$url_resource, $buffer);
		}
		else{
			#desktop
			if(getconfig('ASSETS_URL') != '' and getconfig('ASSETS_URL') != '/assets' and getconfig('ASSETS_URL') != 'http://'.getconfig('SITE_URL').'/assets'){
				$url_resource = getconfig('ASSETS_URL');
				$buffer = preg_replace('/src\=\"(\/assets|\.\.\/assets|http\:\/\/'.getconfig('SITE_URL').'\/assets)/','src="'.$url_resource, $buffer);
			}
		}

		return $buffer;
	}

	function minify_css($buffer){
		$regex = array(
			"#/\*.*?\*/#si" => '', #css comment
			"`^([\t\s]+)`ism"=>'', #tabs and space
			'/\s*([{}|:;,])\s+/si' => '$1', #css whitespace
			"/ \{/" => '{', #whitespace before bracket
			"/ \:/" => ':', #whitespace before ...?
			"`(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+`ism"=>"", #should be \n
			'/(\s)+/s' => '\\1', #shorten multiple whitespace squence
		);

		#url resource rule
		if(getenv('HTTP_HOST') == getconfig('MOBILE_URL')){
			#mobile
			if(getconfig('ASSETS_URL') == '' or getconfig('ASSETS_URL') == '/assets'){
				$url_resource = 'http://'.getconfig('SITE_URL').'/assets';
			}
			else{
				$url_resource = getconfig('ASSETS_URL');
			}

			$buffer = preg_replace('/url\((\"|\')?(\/assets|\.\.\/assets)/', 'url($1'.$url_resource, $buffer);
		}
		else{
			#desktop
			if(getconfig('ASSETS_URL') != '' and getconfig('ASSETS_URL') != '/assets'){
				$url_resource = getconfig('ASSETS_URL');
				$buffer = preg_replace('/url\((\"|\')?(\/assets|\.\.\/assets)/', 'url($1'.$url_resource, $buffer);
			}
		}

		#minify if minify enabled
		if(getconfig('MOD_MINIFY') == TRUE){
			$result = preg_replace(array_keys($regex), $regex, $buffer);
		}
		else{
			$result = $buffer;
		}

		return $result;
	}

	function lcauth($c){
		/*
			this function is used to handling license (organize all website content by given license)
			create on: 21 agust 2013
			last modified: 21 agust 2013
		*/

		global $license_0195;
		global $now;

		$lc = unserialize(strdecode($license_0195));
		if(is_numeric($c)){
			if(in_array($c, $lc['features'])){
				$result = true;
			} else {
				$result = false;
			}
		}
		else{
			if(is_array($lc[$c])){
				$result = (count($lc[$c])>0 ? true:false);
			}
			else{
				if($c=='sms'){
					if($lc['sms'] == 'on'){
						$result = (($lc['smsexpire']!=0 and $lc['smsexpire']<$now) ? false:true);
					}
					else{
						$result = false;
					}
				}
				if($c=='mobile'){
					$result = ($lc['mobile'] == 'on' ? true:false);
				}
				elseif($lc[$c]=='on'){
					$result = true;
				}
				elseif($lc[$c]=='off'){
					$result = false;
				}
				elseif(is_numeric($lc[$c])){
					$result = ($lc[$c]>0 ? true:false);
				}
			}
		}

		return $result;
	}

	function filecache($method, $cache_name, $cache_data = null, $cache_duration = null){
		/*
			this function is used to handling cache
			create on: 21 agust 2013
			last modified: 06 March 2014
		*/

		$realPath = (defined('BASEDIR') ? basedir():'..');

		if($method == 'check'){
			#check ccache file in chache directory
			$lastmod = @filemtime($realPath.'/cache/'.$cache_name);
			$lastmod_cfg = @filemtime($realPath.'/modz/config.php');

			if($lastmod AND getconfig('MOD_CACHE') === true AND $lastmod_cfg <= $lastmod){
				$result = TRUE;
			}
			else{
				$result = FALSE;
			}
		}
		elseif($method == 'write'){
			if(getconfig('MOD_CACHE') == FALSE){
				#display output as and return 200 http code to client web browser
				print $cache_data;

				$result = TRUE;
			}
			else{
				#make content cacheable
				$file = fopen($realPath.'/cache/'.$cache_name, 'w+');
				if($file){
					#write cache
					fwrite($file, $cache_data);
					fclose($file);

					#get the last-modified-date of this very file
					$lastModified = @filemtime($realPath.'/cache/'.$cache_name);

					#get a unique hash of this file (etag)
					$etagFile = md5_file($realPath.'/cache/'.$cache_name);

					#set last-modified header
					header("Last-Modified: ".gmdate("D, d M Y H:i:s", $lastModified)." GMT");

					#set etag-header
					header("Etag: $etagFile");

					#make sure caching is turned on
					$ts = gmdate("D, d M Y H:i:s", time() + $cache_duration) . " GMT";
					header("Expires: $ts");
					header("Pragma: cache");
					header("Cache-Control: max-age=".$cache_duration.", public");

					#display output as and return 200 http code to client web browser
					print $cache_data;

					$result = TRUE;
				}
				else{
					$result = FALSE;
				}
			}
		} elseif($method == 'render'){
			if($cache_data == 'css' or $cache_data == 'js'){
				#specify cache filename
				$cache_fileName = md5(urlencode(strtolower(implode('|', $cache_name)))).'.'.$cache_data;

				if(@file_exists($realPath.'/cache/'.$cache_fileName)){
					if(getconfig('CACHE_URL') != '/cache' and getconfig('CACHE_URL') != 'http://'.getconfig('SITE_URL').'/cache'){
						$cache_url = getconfig('CACHE_URL');
					} else {
						$cache_url = 'http://'.getconfig('SITE_URL').'/cache';
					}

					$result = $cache_url.'/'.$cache_fileName;
				} else {
					$result = '/minify/?c='.$cache_data.'&amp;f='.urlencode(implode('|', $cache_name));
				}
			}
		} else {
			#get the last-modified-date of this very file
			$lastModified = @filemtime($realPath.'/cache/'.$cache_name);

			#get a unique hash of this file (etag)
			$etagFile = md5_file($realPath.'/cache/'.$cache_name);

			#get the HTTP_IF_MODIFIED_SINCE header if set
			$ifModifiedSince = (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false);

			#get the HTTP_IF_NONE_MATCH header if set (etag: unique file hash)
			$etagHeader = (isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false);

			#check if page has changed. If not, send 304 and exit
			if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $lastModified || $etagHeader == $etagFile){
				#set etag-header
				//header("Etag: $etagHeader");

				#make sure caching is turned on
				$ts = gmdate("D, d M Y H:i:s", $lastModified + $cache_duration) . " GMT";
				header("Expires: $ts");
				header("Cache-Control: max-age=".$cache_duration.", public");

				header("HTTP/1.1 304 Not Modified");
				exit;
			}
			else{
				#display output as and return 200 http code to client web browser
				print file_get_contents($realPath.'/cache/'.$cache_name);
				$result = FALSE;
			}
		}
		return $result;
	}

	function customscript(){
		#load style from user config
		if(SITE_IMAGE){
			@list($bgpos, $bgrepeat) = @explode(',', BACKGROUND_SETTING);

			$css = 'body {background:'.SITE_COLOR .' url('.(substr(SITE_IMAGE, 0, 7) == 'http://' ? SITE_IMAGE:getconfig('ASSETS_URL').str_replace('/assets/', '/', SITE_IMAGE)).') '.$bgpos.' '.$bgrepeat.' !important;}';
		}
		elseif(SITE_COLOR){
			$css = 'body {background-color:'.SITE_COLOR.' !important;}';
		}

		$result = ($css ? '<style type="text/css">'.$css.'</style>':'');
		return $result;
	}

	function newsletter_sendmail($newsletterid,$subscriberid,$tmpsign,$emailto,$emailsubject,$emailmsg,$emailmsghtml,$emailhead=''){
		#This function will queuing Newsletter-email before send it.
		#Will not re-queue data to table if the email and content is exist
		#You can count the successful query by the return value;
		global $now;

		#Checking whether same data exist in mail_queue and email_blacklist
		$sqle = "SELECT COUNT(*) AS exist FROM newsletter_email_queue JOIN newsletter_tmp ";
		$sqle .= "ON newsletter_email_queue.tmpSign=CONCAT( newsletter_tmp.subId, '.', newsletter_tmp.nId ) ";
		$sqle .= "WHERE newsletter_tmp.nId='".$newsletterid."' AND newsletter_tmp.subId='".$subscriberid."'";
		$querye = query($sqle);
		$datae = fetch($querye);

		$isemailexist = $datae['exist'];

		$blackemail = countdata("email_blacklist","blackEmail='$emailto'");

 		#Clean it up, please refer to cleanup() to see the allowed html code
		$emailsubject = cleanup($emailsubject,"admin");
		$emailmsg = cleanup($emailmsg,"admin");
		$emailmsghtml = cleanup($emailmsghtml,"admin");
		if(!empty($emailhead)){
			$emailhead = cleanup($emailhead,"admin");
		}

	 	if($isemailexist == 0 and $blackemail == 0){
			$nextid = nextid("emailId","newsletter_email_queue");
			$sql = "INSERT INTO newsletter_email_queue VALUES ('$nextid','$emailto','$emailsubject','$emailmsg','$emailmsghtml','$emailhead','$now','n','$tmpsign')";
			$query = query($sql);
		}

		if($query){
			return true;
		}else{
			return false;
		}
	}

	function guardian($cmd, $formId = null){
		/*
			this function is used to handling form, safe it without captha
			create on: 07 january 2014
			last modified: 3 november 2015
		*/

		#set global variable
		global $ishuman, $js, $_gkey, $now;

		#required php session, start it if needed
		if (session_id() == null){
			session_start();
		}

		if ($cmd == 'init'){
			#generate master key for current session, make every active session unique
			if(! isset($_SESSION['guardian']['master_key'])){
				$_SESSION['guardian']['master_key'] = codegen(16);
			}

			#set token
			$token_key = codegen(rand(4, 8));
			$token_value = codegen(rand(5, 10));

			if(is_numeric(substr($token_key, 0, 1))){
				$token_key = 'x'.$token_key;
			}

			#create request signature
			$token_guard = array(
				$token_key,
				$token_value,
				$formId,
				$_SESSION['guardian']['master_key']
			);

			#set result
			$result = $token_key.'$'.$token_value.'$'.str_replace('%', '_', strencode(implode('|', $token_guard), $_SESSION['guardian']['master_key']));
		} elseif($cmd == 'validate'){
			#get signature info
			list($token_key, $token_value, $formId, $master_key) = explode('|', strdecode(str_replace('_', '%', $_gkey), $_SESSION['guardian']['master_key']));

			#validate ishuman
			if(!empty($ishuman) or !isset($ishuman)){
				#this is bad user, redirect or just exit here
				blackhole('ishumanIsFilled', $formId);
				exit;
			}

			#validate additional carried variable special for ajax request
			if ($js != 'on'){
				#this is bad user, redirect or just exit here
				blackhole('noJsIsOn', $formId);
				exit;
			} else {
				#AJAX Check
				if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) or strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
					blackhole('jsIsOnButNotAjax', $formId);
					exit;
				}
			}

			if($master_key != $_SESSION['guardian']['master_key']){
				#this is bad user, redirect or just exit here
				blackhole('invalidSignature', $formId);
				exit;
			}

			#check, is the key already used before?
			if(@in_array($token_key.$token_value, $_SESSION['guardian']['history'])){
				#this is bad user, redirect or just exit here
				blackhole('invalidGuardian', $formId);
				exit;
			}

			#validate token
			if($_SESSION['guardian']['data'][$token_key] != $_POST[$token_key]) {
				#this is bad user, redirect or just exit here
				blackhole('invalidGuardian', $formId);
				exit;
			}

			$result = true;
		} elseif ($cmd == 'clear') {
			#get signature info
			list($token_key, $token_value, $formId, $master_key) = explode('|', strdecode(str_replace('_', '%', $_gkey), $_SESSION['guardian']['master_key']));

			#clear guardian token
			unset($_SESSION['guardian']['data'][$token_key]);
			if(count($_SESSION['guardian']['data']) == 0 ) {
				unset($_SESSION['guardian']['data']);
			}

			#set new history variable
			if(! isset($_SESSION['guardian']['history'])){
				$_SESSION['guardian']['history'] = array();
			}

			#prepend data to array history to avoid spammer force using http replay attact
			array_unshift($_SESSION['guardian']['history'], $token_key.$token_value);

			#limit array size
			array_splice($_SESSION['guardian']['history'], 25);

			$result = true;
		} else {
			die('<b>'.$cmd.'</b> is invalid command');
		}

		return $result;
	}

	function is_spam ($string, $type_of_exp = '', $debug_mode = false){
		/*
			var @string = the comments
			var @type_of_exp = 'url,shortulr,bbpin,phonenum'
				- url 		: exp http|https|ftp://example.com or www.example.com
 				- bbpin 	: 8 hexa digit
				- phonenum 	: 8 or more number
				- email		: goerge@example.com
			var @debug_mode = true for returning the matches expression

			return true and false if not a spam
		*/

		$going_to_check = $type_of_exp == '' ? array('url', 'bbpin', 'phonenum','email') : array_merge(explode(",",$type_of_exp));
		$spam = array('url' => false, 'bbpin' => false, 'phonenum' => false, 'email' => false); // containt true if @string is spam
		$debug_result = array('url' => false, 'bbpin' => false, 'phonenum' => false, 'email' => false);

		#1.Check for url
		if(in_array("url", $going_to_check)){
 			$check_url = preg_match('/((((http|https|ftp|ftps)\:\/\/)|www\.)[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,4}(\/\S*)?)/', $string, $url_match);
			if($check_url) {
				$spam['url'] = true;

				$debug_result['url'] = $url_match[0];
			}
		}

		$clean_string = preg_replace("/[^\w ]/ui", ' ', $string); // remove non-alphanumeric char

		#2.Check for BB Pin, 8 hexa char
		if(in_array("bbpin", $going_to_check)){
			$get_hexa = preg_match("/\b[a-fA-F0-9]{8}\b/", $clean_string, $bbpin_match);
			#print_r($bbpin_match); // debug the preg_match

			if($get_hexa){
				$spam['bbpin'] = true;
				$debug_result['bbpin'] = $bbpin_match[0];
			}
		}

		#3.Check for phone number
		if(in_array("phonenum", $going_to_check)){
			$check_phonenum = preg_match("/\b[0-9]{8,}\b/", $clean_string, $phonenum_match);
			if($check_phonenum){
				$spam['phonenum'] = true;
				$debug_result['phonenum'] = $phonenum_match[0];
			}
		}

		#4.Check for email
		if(in_array("email", $going_to_check)){
			$check_email = preg_match("/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})/i", $string, $email_match);
			if($check_email){
				$spam['email'] = true;
				$debug_result['email'] = $email_match[0];
			}
		}

		if($debug_mode){
			return  array_filter($debug_result);
		}

		if (in_array(true, $spam)) {
			return true;
		}

		return false;
	}

	function visitor_ip(){
		$client  = @$_SERVER['HTTP_CLIENT_IP'];
		$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $forward = explode(",",$forward);
        $forward = $forward[0];
        
		$remote  = $_SERVER['REMOTE_ADDR'];

		if (filter_var($client, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) or filter_var($client, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)){
			$ip = $client;
		} elseif (filter_var($forward, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) or filter_var($forward, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)){
			$ip = $forward;
		} elseif (filter_var($remote, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) or filter_var($remote, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)){
			$ip = $remote;
		} else {
			#this is bad user, sredirect or just exit here
			if(isset($_SERVER['HTTP_USER_AGENT'])){
				exit;
			}
		}

		return cleanup($ip);
	}

	function blackhole($reason = null, $formId = null){
		global $dbh, $now;

		#these variable is temporary disabled and will be availabe for next features
		define('BOT_LOG_SIZE', 3);
		define('BOT_LOG_RULE_ALLOW', false);
		define('BOT_LOG_RULE_DENY', true);
		define('BOT_LOG_RULE_UNVERIFIED', true);

		#cleanup server input variable, make it safe.
		$serverData = cleanup($_SERVER);

		#collect neccesary data from server input variable
		$botData = array(
			'HTTP_REFERER' => $serverData['HTTP_REFERER'],
			'HTTP_ORIGIN' => $serverData['HTTP_ORIGIN'],
			'HTTP_USER_AGENT' => $serverData['HTTP_USER_AGENT'],
			'HTTP_REFERER' => $serverData['HTTP_REFERER'],
			'HTTP_COOKIE' => $serverData['HTTP_COOKIE'],
			'REQUEST_METHOD' => $serverData['REQUEST_METHOD'],
			'REQUEST_URI' => $serverData['REQUEST_URI'],
			'SCRIPT_NAME' => $serverData['SCRIPT_NAME']
		);

		#check crawler status by ip
		$sql = "SELECT wbId,wbIpAddress,wbStatus,wbLogRule FROM webbot WHERE wbIpAddress='".visitor_ip()."'";
		$query = query($sql);
		$crawler = fetch($query);

		if ($crawler['wbIpAddress']){
			$log = false;
			#logging for good bot
			if((($crawler['wbStatus'] == 'allow' and BOT_LOG_RULE_ALLOW == true) or $crawler['wbLogRule'] == 'always') and $crawler['wbLogRule'] != 'never'){
				$log = true;
			}

			#logging for bad bot
			if((($crawler['wbStatus'] == 'deny' and BOT_LOG_RULE_DENY == true) or $crawler['wbLogRule'] == 'always') and $crawler['wbLogRule'] != 'never'){
				$log = true;
			}

			#logging for unverified bot
			if((($crawler['wbStatus'] == 'unverified' and BOT_LOG_RULE_UNVERIFIED == true) or $crawler['wbLogRule'] == 'always') and $crawler['wbLogRule'] != 'never'){
				$log = true;
			}

			#update statistic
			$sql = "UPDATE webbot SET wbCount=wbCount+1, wbLastLog='$now' WHERE wbId='{$crawler['wbId']}'";
			$query = query($sql);

			if($log == true){
				#add crawler data
				if($reason != 'justVisit'){
					$sql = "INSERT INTO webbot_log VALUES('".serialize($botData)."', '$reason', '".cleanup($formId)."', '$now', '{$crawler['wbId']}')";
					$query = query($sql);
				}

				#keep log data in fixed size
				$sql = "SELECT COUNT(wbId) as nData FROM webbot_log WHERE wbId='{$crawler['wbId']}'";
				$query = query($sql);
				$data = fetch($query);
				$botLogSize = $data['nData'];

				if($botLogSize > BOT_LOG_SIZE){
					$delPlan = (int) abs(BOT_LOG_SIZE - $botLogSize);
					$sql = "DELETE FROM webbot_log WHERE wbId='{$crawler['wbId']}' ORDER BY wblogAddedOn ASC LIMIT $delPlan";
					$query = query($sql);
				}
			}
		} else {
			$ipAddr = visitor_ip();

			#generate ip signature
			$ipAddrSignature = base_convert(str_replace('.', '', $ipAddr), 10, 32).substr(md5($ipAddr), 5, 3).substr(md5($ipAddr), 10, 2);
			#get IP information
			$ipinfo = geoip_info($ipAddr);

			#insert new
			$sql = "INSERT IGNORE INTO webbot VALUES(NULL, '$ipAddr', '$ipAddrSignature', '$now', $now, $now, 1, 'script', 'none', 'default', 'deny', '$ipinfo')";
			$query = query($sql);

			if($query){
				#add crawler data
				if(is_object($dbh)){
					$botId = $query;
				} else {
					$botId = mysql_insert_id($query);
				}

				$sql = "INSERT INTO webbot_log VALUES('".serialize($botData)."', '$reason', '".cleanup($formId)."', '$now', '$botId')";
				$query = query($sql);
			}
		}
	}

	function geoip_info($ip){
		$query = "http://www.telize.com/geoip/" . $ip;

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,0);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($curl, CURLOPT_URL, $query);
		$result = curl_exec($curl);
		curl_close($curl);
		return $result;
	}
	include("phpmailer/PHPMailerAutoload.php");
	function sendMailComplete($options){
		#example
		#email options[OPTIONAL]:
		#$options['mailopt']="standard"; #option: standard or queue or sendgrid

		#email from[OPTIONAL]:
		#$options['from']="";

		#email from name[OPTIONAL]:
		#$options['fromname']="";

		#email reply[OPTIONAL]:
		#$options['replyto']="";

		#email cc[OPTIONAL]:
		#$options['cc']="";

		#email BCC[OPTIONAL]:
		#$options['bcc']="";

		#email to:
		#$options['to']="";

		#email subject:
		#$options['subject']="";

		#email messagetype:
		#$options['messagetype']="html"; #option: html or text

		#email message:
		#$options['message']="<html><body><div style='background-color:#ccc; color:red;font-size:18pt'>Header</div><br/> Content content content</body></html>";

		#email attachment:
		#$options['filename']="../assets/fileman/05122013/template_dxju6_1.zip";
		
		

		foreach($options as $opti=>$value){
			$$opti = $value;
		}

		if(empty($mailopt)){
			$mailopt = getconfig('EMAIL_OPTION');
		} else {
			$mailopt = $mailopt;
		}

		if(empty($fromname)){
			if (empty($from)){
				$from = SITE_EMAIL;
				$fromname = SITE_NAME;
			} else {
				list($fromname, $xc) = explode("@", $from);
			}
		}

		#ATTACHMENT FILE
		if (!empty($filename)){
			if(!file_exists($filename)){
				$filename = '';
			} else {
				$name_of_file =  end(explode("/", $filename));
				list($name, $ext) = explode(".", $name_of_file);
				$ftype = "";
				if ($ext == "doc") {
					$ftype = "application/msword";
				} elseif ($ext == "jpg") {
					$ftype = "image/jpeg";
				} elseif ($ext == "gif") {
					$ftype = "image/gif";
				} elseif ($ext == "png") {
					$ftype = "image/png";
				} elseif ($ext == "zip") {
					$ftype = "application/zip";
				} elseif ($ext == "pdf") {
					$ftype = "application/pdf";
				} else {
					$ftype = "application/octet-stream";
				}

				$files = $filename;
				$file_size = filesize($files);
				$file = fopen($files, "rb");
				$data = fread($file, $file_size);
				fclose($file);

				#split the file into chunks for attaching
				$content = chunk_split(base64_encode($data));
			}
		}

		$uid = md5(uniqid(time()));
		#OPTION=STANDARD
		if ($mailopt == "standard"){
			$mail = new PHPMailer;
			$mail->addCustomHeader('IsTransactional', 'true');
			$mail->setFrom($from, $fromname);
			$mail->addAddress($to, $toname);
			$mail->addReplyTo($replyto, "Reply");
			$mail->addCC($cc);
			$mail->addBCC($bcc);
			if($messagetype=='html'){
				$mail->isHTML(true); 
			}else{
				$mail->isHTML(false); 
			}
			if (!empty($filename)){
				$mail->addAttachment($filename,$name,'base64',  $ftype ,   'attachment');
			}
			$mail->CharSet="UTF-8";
			$mail->Subject = $subject;
			$mail->Body    = $message;

			if(!$mail->send()) {
				$result = FALSE;
			} else {
				$result = TRUE;
			}
		} elseif ($mailopt=='queue') {
			#OPTION=QUEUE

			#header
			$header = $fromname."#".$from;
			if ($replyto) $header .= "#".$replyto;
			if ($cc) $header .= "#".$cc;
			if ($bcc) $header .= "#".$bcc;
			if ($toname) $header .= "#".$toname;

			#send mail
			$result = sendmail($to, $subject, $message, $messagetype, $header, $filename);
		} else {
			#SENDGRID

			#append <br> if the body format is text
			if($messagetype == 'text'){
				$message=nl2br($message);
			}

			$params = array(
				'api_user' => getconfig('SENDGRID_USER'),
				'api_key' => getconfig('SENDGRID_PASSWORD'),
				'to' => $to,
				'subject' => $subject,
				'html' => $message,
				'text' => $message,
				'from' => $from,
				'fromname' => $fromname,
			 );

			if (!empty($filename)){
				$params['files['.$filename.']'] = '@'.realpath($files);
			}

			$request = 'http://sendgrid.com/api/mail.send.json';

			#Generate curl request
			$session = curl_init($request);

			#Tell curl to use HTTP POST
			curl_setopt ($session, CURLOPT_POST, true);

			#Tell curl that this is the body of the POST
			curl_setopt ($session, CURLOPT_POSTFIELDS, $params);

			#Tell curl not to return headers, but do return the response
			curl_setopt($session, CURLOPT_HEADER, false);
			curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

			#obtain response
			$response = curl_exec($session);
			curl_close($session);

			$response = json_decode($response, true);

			if($response['message'] == 'success'){
				$result = true;
			} else {
				$result = false;
			}
		}

		if($result == false and $options['mailopt'] != 'queue'){
			#queue it
			$options['mailopt'] = 'queue';
			$options = cleanup($options,"sendmail");
			sendMailComplete($options);
		}

		return $result;
	}

	/*UNIVERSAL INBOX
	========================================================================
	*/

	//check label email ex: yahoo,google,etc
	function checklabelemail($email){
		if (preg_match("/^([._a-z0-9-]+[._a-z0-9-]*)@(yahoo|ymail|rocketmail).(com|co.id)$/", $email)) {
			$label="yahoo";
		}
		elseif (preg_match("/^([._a-z0-9-]+[._a-z0-9-]*)@(gmail).(com|co.id)$/", $email)) {
			$label="gmail";
		}
		elseif (preg_match("/^([._a-z0-9-]+[._a-z0-9-]*)@(outlook|hotmail|windowslive).(com|co.id)$/", $email)) {
			$label="hotmail";
		}
		elseif (preg_match("/^([._a-z0-9-]+[._a-z0-9-]*)@(aol).(com|co.id)$/", $email)) {
			$label="aol";
		}
		else {
			$label="domain";
		}

		return $label;
	}

	//for body email
	function get_mime_type(&$structure) {
	   $primary_mime_type = array("TEXT", "MULTIPART","MESSAGE", "APPLICATION", "AUDIO","IMAGE", "VIDEO", "OTHER");
	   if($structure->subtype) {
			return $primary_mime_type[(int) $structure->type] . '/' .$structure->subtype;
	   }
		return "TEXT/PLAIN";
	}

   function get_part($stream, $msg_number, $mime_type, $structure = false,$part_number    = false) {

		if(!$structure) {
			$structure = imap_fetchstructure($stream, $msg_number);
		}
		if($structure) {
			if($mime_type == get_mime_type($structure)) {
				if(!$part_number) {
					$part_number = "1";
				}
				$text = imap_fetchbody($stream, $msg_number, $part_number);
				if($structure->encoding == 3) {
					return imap_base64($text);
				} else if($structure->encoding == 4) {
					return imap_qprint($text);
				} else {
				return $text;
			}
		}

			if($structure->type == 1) /* multipart */ {
			while(list($index, $sub_structure) = each($structure->parts)) {
				if($part_number) {
					$prefix = $part_number . '.';
				}
				$data = get_part($stream, $msg_number, $mime_type, $sub_structure,$prefix .    ($index + 1));
				if($data) {
					return $data;
				}
			} // END OF WHILE
			} // END OF MULTIPART
		} // END OF STRUTURE
		return false;
   } // END OF FUNCTION

	function transformHTML($str) {
		if ((strpos($str,"<HTML") < 0) || (strpos($str,"<html")    < 0)) {
			$makeHeader = "<html><head><meta http-equiv=\"Content-Type\"    content=\"text/html; charset=iso-8859-1\"></head>\n";
			if ((strpos($str,"<BODY") < 0) || (strpos($str,"<body")    < 0)) {
				$makeBody = "\n<body>\n";
				$str = $makeHeader . $makeBody . $str ."\n</body></html>";
			} else {
				$str = $makeHeader . $str ."\n</html>";
			}
		} else {
			$str = "<meta http-equiv=\"Content-Type\" content=\"text/html;    charset=iso-8859-1\">\n". $str;
		}
		return $str;
	}

	//for  Gets all attachments
	/**
	* Gets all attachments
	* Including inline images or such
	* @author: Axel de Vignon
	* @param $content: the email structure
	* @param $part: not to be set, used for recursivity
	* @return array(type, encoding, part, filename)
	*
	*/
	function get_attachments($content, $part = null, $skip_parts = false) {
		static $results;

		// First round, emptying results
		if (is_null($part)) {
			$results = array();
		}
		else {
			// Removing first dot (.)
			if (substr($part, 0, 1) == '.') {
				$part = substr($part, 1);
			}
		}

		// Saving the current part
		$actualpart = $part;
		// Split on the "."
		$split = explode('.', $actualpart);

		// Skipping parts
		if (is_array($skip_parts)) {
			foreach ($skip_parts as $p) {
				// Removing a row off the array
				array_splice($split, $p, 1);
			}
			// Rebuilding part string
			$actualpart = implode('.', $split);
		}

		// Each time we get the RFC822 subtype, we skip
		// this part.
		if (strtolower($content->subtype) == 'rfc822') {
			// Never used before, initializing
			if (!is_array($skip_parts)) {
				$skip_parts = array();
			}
			// Adding this part into the skip list
			array_push($skip_parts, count($split));
		}

		// Checking ifdparameters
		if (isset($content->ifdparameters) && $content->ifdparameters == 1 && isset($content->dparameters) && is_array($content->dparameters)) {
			foreach ($content->dparameters as $object) {
				if (isset($object->attribute) && preg_match('~filename~i', $object->attribute)) {
					$results[] = array(
					'type'          => (isset($content->subtype)) ? $content->subtype : '',
					'encoding'      => $content->encoding,
					'part'          => empty($actualpart) ? 1 : $actualpart,
					'filename'      => $object->value
					);
				}
			}
		}

		// Checking ifparameters
		else if (isset($content->ifparameters) && $content->ifparameters == 1 && isset($content->parameters) && is_array($content->parameters)) {
			foreach ($content->parameters as $object) {
				if (isset($object->attribute) && preg_match('~name~i', $object->attribute)) {
					$results[] = array(
					'type'          => (isset($content->subtype)) ? $content->subtype : '',
					'encoding'      => $content->encoding,
					'part'          => empty($actualpart) ? 1 : $actualpart,
					'filename'      => $object->value
					);
				}
			}
		}

		// Recursivity
		if (isset($content->parts) && count($content->parts) > 0) {
			// Other parts into content
			foreach ($content->parts as $key => $parts) {
				get_attachments($parts, ($part.'.'.($key + 1)), $skip_parts);
			}
		}
		return $results;
	}

	function sendimap($to,$from,$replyto,$cc,$bcc,$subtitle,$message,$msgId,$attachment="",$reference="",$inrep="") {
		$fileman=$attachment;
		if (!empty($fileman)){
			$files=$attachment;
			$file = fopen($files, "rb");
			$data = fread($file,  filesize( $files ) );
			fclose($file);

			// split the file into chunks for attaching
			$content = chunk_split(base64_encode($data));
			$uid = md5(uniqid(time()));
			list($name,$ext)=explode(".",$fileman);
			$ftype = "";
			if ($ext == "doc") $ftype = "application/msword";
			if ($ext == "jpg") $ftype = "image/jpeg";
			if ($ext == "gif") $ftype = "image/gif";
			if ($ext == "png") $ftype = "image/png";
			if ($ext == "zip") $ftype = "application/zip";
			if ($ext == "pdf") $ftype = "application/pdf";
			if ($ftype=="") $ftype = "application/octet-stream";
		}
		$to = $to;
		$subject = $subtitle;
		$body = $message;

		if ($replyto){ $rep=$replyto; }
		else{ $rep=$from; }

		$headers = "From: ".$from."\r\n".
				   "Reply-To: ".$rep."\r\n".
				   "Message-ID: ".$msgId."\r\n";
		if ($reference){ $headers.= "References: ".$reference."\r\n"; }
		if ($inrep){ $headers.= "In-Reply-To: ".$inrep."\r\n"; }
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: MULTIPART/MIXED; boundary=\"".$uid."\"\r\n\r\n";
		$headers .= "This is a multi-part message in MIME format.\r\n";
		$headers .= "--".$uid."\r\n";
		$headers .= "Content-type:TEXT/HTML; charset=iso-8859-1\r\n";
		$headers .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
		$headers .= $body."\r\n\r\n";
		$headers .= "--".$uid."\r\n";
		if (!empty($fileman)){
				$headers .= "Content-Type: ".$ftype."; name=\"".basename($fileman)."\"\r\n";
				$headers .= "Content-Transfer-Encoding: base64\r\n";
				$headers .= "Content-Disposition: attachment; filename=\"".basename($fileman)."\"\r\n\r\n";
				$headers .= $content."\r\n\r\n";
				$headers .= "--".$uid."--";
			}
			$cc = $cc;
			$bcc = $bcc;
			$return_path = $to;
			//send the email using IMAP
			return imap_mail($to, $subject, $body, $headers, $cc, $bcc, $return_path);

	}

	/*
		This function will updating log for cron files
		@param $options : array {
					cron_name : String
					cron_data : String
				}
		@return : boolean
	*/
	function cron_activity_log($options) {
		global $now;

		if(empty($options['cron_name'])){
			return false;
		}

		$sql = "UPDATE activitylog SET actLastActivity='$now' WHERE actName='".$options['cron_name']."'";
		$query = query($sql);

		if($query){
			return true;
		}

		return false;
	}

	/*
		this function is used to scan dir recursively
		@param dirs : array()
		@param ext : String (file extention ex:php)
		@return : array()
	*/
	function scandir_r($dirs, $ext = '') {
		//only for /admin
		$exception_dir = "xml_data";

		foreach($dirs as $key => $dir){
			$dir = rtrim($dir, '\\/');
			$scanned_dir = null;
			$scanned_dir = scandir($dir);

 			foreach ($scanned_dir as $f) {
			  if ($f !== '.' and $f !== '..' and $f !== $exception_dir) {
					if (is_dir("$dir/$f")) {
						$result = array_merge($result, scandir_r(array("$dir/$f"), $ext));
					} else {
						if($ext){
							if(strtolower(end(explode('.', $f))) != $ext){
								continue;
							}
						}

						$result[] = $dir.'/'.$f;
					}
			  }
			}
		}

		return $result;
	}

	//this function used to upload product image
	function uploadbox($opt){
		foreach($opt as $key=>$value){
			$$key=$value;
		}

		list($firstdir,$nowdir)=explode("/",$subdir);
		if (!$nowdir){
			$nowdir=date('dmY');
			$targetdir="../assets/" . $firstdir . '/' . $nowdir . '/';
		}
		else{
			$targetdir="../assets/" . $subdir . '/';
		}

		#test whether targetdir exists
		if(!@opendir($targetdir) ){
			@mkdir($targetdir);
		}

		$tmp=getext($fname,$fileid,$suffix);
		$tmp['xl']=$tmp['l'].'_xl'; # <- additional suffix
		$tmp['xxl']=$tmp['l'].'_xxl'; # <- additional suffix

		$lfname=$tmp['l'].'.'.$tmp['ext'];
		$tmpfname='tmp_'.$lfname;
		chdir($targetdir);
		$fcopy=copy($opt['tmpfile'],$tmpfname);

		$image_info=getimagesize($tmpfname);
		$image_width=$image_info[0];
		$image_height=$image_info[1];
		$image_type=$image_info[2];

		if($image_type==1){
			$src=imagecreatefromgif($tmpfname);
		}
		elseif($image_type==2){
			$src=imagecreatefromjpeg($tmpfname);
		}
		elseif($image_type==3){
			$src=imagecreatefrompng($tmpfname);
		}

		foreach($img_w as $key=>$value){
			$fname=$tmp[$key].'.'.$tmp['ext'];

			$dst_x = $img_w[$key]/2 - $image_width/2;
			$dst_y = $img_h[$key]/2 - $image_height/2;

			$x_ratio = $img_w[$key] / $image_width;
			$y_ratio = $img_h[$key] / $image_height;

			if (($image_width <= $img_w[$key]) && ($image_height <= $img_h[$key])) {
				$tn_width = $image_width;
				$tn_height = $image_height;
				$trans_x=$dst_x;
				$trans_y=$dst_y;
			}
			else if (($x_ratio * $image_height) < $img_h[$key]) {
				$tn_height = ceil($x_ratio * $image_height);
				$tn_width = $img_w[$key];
				$trans_x=0;
				$trans_y=$img_h[$key]/2 - $tn_height/2;
			}
			else {
				$tn_width = ceil($y_ratio * $image_width);
				$tn_height = $img_h[$key];
				$trans_x=$img_w[$key]/2 - $tn_width/2;;
				$trans_y=0;
			}

			$dst=imagecreatetruecolor($img_w[$key] , $img_h[$key]);
			$color=imagecolorallocate($dst, 255, 255, 255);
			imagefill($dst, 0, 0, $color);
			imagecopyresampled($dst, $src, $trans_x, $trans_y, 0, 0 ,$tn_width, $tn_height, $image_width, $image_height);

			//Image type ?
			if($image_type==1){
				imagegif($dst, $fname);
				$srcx=imagecreatefromgif($tmp[$key].'.'.$tmp['ext']);
			}
			elseif($image_type==2){
				imageinterlace($dst, true); #enable high compress
				imagejpeg($dst, $fname, IMAGE_QUALITY);
				$srcx=imagecreatefromjpeg($tmp[$key].'.'.$tmp['ext']);
			}
			elseif($image_type==3){
				imagepng($dst, $fname);
				$srcx=imagecreatefrompng($tmp[$key].'.'.$tmp['ext']);
			}

			imagedestroy($dst);
			imagedestroy($srcx);
		}

		imagedestroy($src);
		unlink($tmpfname);

		return array("dir"=>$nowdir,"filename"=>$lfname);
	}
?>