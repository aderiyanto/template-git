<?php
define('WEB_TO_SMS','../admin/xml_data/sms.xml');
define('WEB_TO_CONTACT','../admin/xml_data/contact.xml');
define("WEB_XML_EXPIRED",48); // in hour
#define("WEB_SMS_TOKEN","9d642539d81dae27436ae9c8703a4d5774a51ce5");

function XMLcreate($xmlFile){
	if(!file_exists($xmlFile)){
		$dom=new domDocument("1.0");
		$root=$dom->createElement("smsviro","");
		$dom->appendChild($root);
		$dom->formatOutput=TRUE;
		$saved=$dom->save($xmlFile);
		chmod($xmlFile,0755);
		unset($dom);		
		$result=($saved ? TRUE:FALSE);
	}
	else{
		$result=TRUE;
	}
	return $result;
}

function XMLNextId($xmlFile,$identifier,$identifierId){
	$dom=new domDocument();
	if($dom->load($xmlFile)){
		$tagIdentifier=$dom->getElementsByTagName($identifier);
		$nData=($tagIdentifier->length)-1;
		if($nData>=0){
			$maxID=$tagIdentifier->item($nData)->getAttribute($identifierId);
			$result=$maxID+1;
			
			//revalidate
			if($result==1 OR $result<$maxID){
				$tagIdentifier=$dom->getElementsByTagName($identifier);
				$nData=($tagIdentifier->length)-1;
				$maxID=$tagIdentifier->item($nData)->getAttribute($identifierId);
				$result=$maxID+1;

				if($result==1){
					$result=FALSE;
				}
			}
		}
		else{
			//revalidate
			$nData=($tagIdentifier->length);
			if($nData>0){
				$result=FALSE;
			}
			else{
				$result=1;
			}			
		}
	}
	else{
		$result=FALSE;
	}
	unset($dom);
	return $result;
}

function XMLInsertNewSMS($recipient,$date,$message,$xmlFile=null){
	if($recipient AND $date AND $message){
		if(!$xmlFile){
			$xmlFile=WEB_TO_SMS;
		}
		
		if(XMLcreate($xmlFile)){
			$nextID=XMLNextId($xmlFile,'sms','smsID');
			if($nextID){
				$date=(empty($date) ? time() : $date);
				$dom=new domDocument("1.0");
				$dom->preserveWhiteSpace=FALSE;
				if($dom->load($xmlFile)){
					$root=$dom->getElementsByTagName("smsviro")->item(0);
					$smsviro=$dom->createElement("sms");
					
					$node_recipient=$dom->createElement("recipient");	
					$node_message=$dom->createElement("message");
					
					$date=$dom->createElement("date",$date);
						
					$smsviro->setAttribute("smsID",$nextID);	
					$root->appendChild($smsviro);
					
					$node_recipient = $smsviro->appendChild($node_recipient);
					$node_recipient->appendChild($dom->createCDATASection($recipient)); 
					
					$node_message = $smsviro->appendChild($node_message);
					$node_message->appendChild($dom->createCDATASection($message)); 
					
					$smsviro->appendChild($date);
						
					$dom->formatOutput=TRUE;
					$saved=$dom->save($xmlFile);
					unset($dom);
					
					$result=($saved ? TRUE:FALSE);
				}
				else{
					$result=FALSE;
				}
			}
			else{
				$result=FALSE;
			}
		}
		else{
			$result=FALSE;
		}
	}
	else{
		$result=FALSE;
	}
	return $result;
}

function XMLInsertNewContact($options,$date=0,$xmlFile=null){
	/* Contact variable
	$options['conFullName']='contact name';
	$options['conHP']='phone number';
	$options['conGroup']='group name';
	$options['conEmail']='contact email';
	$options['conCity']='contact city';
	$options['conDesc']='contact additional info';
	*/
	
	if($options['conFullName'] AND $options['conHP']){
		if(!$xmlFile){
			$xmlFile=WEB_TO_CONTACT;
		}
		
		if(XMLcreate($xmlFile)){
			$nextID=XMLNextId($xmlFile,'data','ID');
			if($nextID){
				$dataencode=htmlentities(json_encode($options));
				$date=(empty($date) ? time() : $date);
				$dom=new domDocument("1.0");
				$dom->preserveWhiteSpace = FALSE;
				if($dom->load($xmlFile)){
					$root=$dom->getElementsByTagName("smsviro")->item(0);
					$smsviro=$dom->createElement("data");
					$dataencode=$dom->createElement("dataencode",$dataencode);	
					$date=$dom->createElement("date",$date);
						
					$smsviro->setAttribute("ID",$nextID);	
					$root->appendChild($smsviro);
					$smsviro->appendChild($dataencode);
					$smsviro->appendChild($date);
						
					$dom->formatOutput=TRUE;
					$saved=$dom->save($xmlFile);
					unset($dom);
					
					$result=($saved ? TRUE:FALSE);
				}
				else{
					$result=FALSE;
				}
			}
			else{
				$result=FALSE;
			}
		}
	}
	else{
		$result=FALSE;
	}
	return $result;
}

function XMLremoveOldNode($xmlType,$hourCount=0,$xmlFile=null){
	if(!$xmlFile){
		$xmlFile=($xmlType=='sms' ? WEB_TO_SMS:WEB_TO_CONTACT);
	}
	
	$hourCount=($hourCount ? $hourCount:WEB_XML_EXPIRED);
	$timeRange=$hourCount*3600;
	$timePass=time()-$timeRange;
	
	$dom=new domDocument();
	if($dom->load($xmlFile)){
		$xPath=new DOMXPath($dom);
		$query=($xmlType=='sms' ? "/smsviro/sms[(date<='$timePass')]":"/smsviro/data[(date<='$timePass')]");
		$nSMS=$xPath->query($query);
		$n=$nSMS->length;
		$tagId=($xmlType=='sms' ? 'smsID':'ID');
		
		for($i=0;$i<$n;$i++){
			$dataID=$nSMS->item($i)->getAttribute($tagId);
			$query=($xmlType=='sms' ? "/smsviro/sms[@smsID='$dataID']":"/smsviro/data[@ID='$dataID']");
			$context=$xPath->query($query)->item(0);
			$context=$context->parentNode->removeChild($context);
		}
		
		$dom->formatOutput=TRUE;
		$saved=$dom->save($xmlFile);
		unset($dom);
		
		$result=($saved ? TRUE:FALSE);
	}
	else{
		$result=FALSE;
	}
	return $result;
}

function XMLreset($xmlType,$xmlFile=null){
	if(!$xmlFile){
		$xmlFile=($xmlType=='sms' ? WEB_TO_SMS:WEB_TO_CONTACT);
	}
	
	$dom=new domDocument();
	if($dom->load($xmlFile)){
		$root=$dom->getElementsByTagName("smsviro")->item(0);
		$dom->removeChild($root);
		$root=$dom->createElement("smsviro","");
		$dom->appendChild($root);
		$dom->formatOutput=TRUE;
		$saved=$dom->save($xmlFile);
		unset($dom);
		
		$result=($saved ? TRUE:FALSE);
	}
	else{
		$result=FALSE;
	}
	return $result;
}

function XMLSelectDate($xmlType,$xmlFile=null,$tagName=null){
	if(!$xmlFile){
		$xmlFile=($xmlType=='sms' ? WEB_TO_SMS:WEB_TO_CONTACT);
	}
	
	if(!$tagName){
		$tagName='smsviro';
	}
	
	$dom=new domDocument();
	if($dom->load($xmlFile)){
		$root=$dom->getElementsByTagName($tagName);
		foreach($root as $smsviro){
			$date[]=$smsviro->getElementsByTagName("date")->item(0)->nodeValue;
		}
		unset($dom);
		$result=$date;
	}
	else{
		$result=FALSE;
	}
	return $result;
}

function XMLremoveSingleNode($xmlType,$id,$xmlFile=null){
	if(!$xmlFile){
		$xmlFile=($xmlType=='sms' ? WEB_TO_SMS:WEB_TO_CONTACT);
	}
	
	$dom=new domDocument();
	if($dom->load($xmlFile)){
		$xPath=new DOMXPath($dom);
		$query=($xmlType=='sms' ? "/smsviro/sms[@smsID='$id']":"/smsviro/data[@ID='$id']");
		$data=$xPath->query($query)->item(0);
		$data->parentNode->removeChild($data); // remove node from $doc
		$dom->formatOutput=true;
		$saved=$dom->save($xmlFile);
		unset($dom);
		
		$result=($saved ? TRUE:FALSE);
	}
	else{
		$result=FALSE;
	}
	return $result;
}

function XMLremoveMultipleNode($xmlType,$dateMin,$dateMax,$xmlFile=null){
	if(!$xmlFile){
		$xmlFile=($xmlType=='sms' ? WEB_TO_SMS:WEB_TO_CONTACT);
	}
	
	$dom=new domDocument();
	if($dom->load($xmlFile)){
		$xPath=new DOMXPath($dom);
		$query=($xmlType=='sms' ? "/smsviro/sms[(date>='$dateMin')and(date<='$dateMax')]":"/smsviro/data[(date>='$dateMin')and(date<='$dateMax')]");
		$nSMS=$xPath->query($query);
		$n=$nSMS->length;
		$tagId=($xmlType=='sms' ? 'smsID':'ID');
		
		for($i=0;$i<$n;$i++){	
			$dataID=$nSMS->item($i)->getAttribute($tagId);
			$query=($xmlType=='sms' ? "/smsviro/sms[@smsID='$dataID']":"/smsviro/data[@ID='$dataID']");
			$context=$xPath->query($query)->item(0);
			$context=$context->parentNode->removeChild($context);
		}
		
		$dom->formatOutput=true;
		$saved=$dom->save($xmlFile);
		unset($dom);
		
		$result=($saved ? TRUE:FALSE);
	}
	else{
		$result=FALSE;
	}
	return $result;
}
?>
