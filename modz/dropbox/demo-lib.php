<?php

/**
 * Some helper functions for the samples
 */

function demo_init() {
	error_reporting( E_ALL );
	enable_implicit_flush();
	echo "<pre>";
}

function store_token($token, $name){
		$sql = "INSERT INTO dropbox_token VALUES ('".$name."','".serialize($token)."')";
		$query = query($sql);
	}

function load_token($name){
	$sql="SELECT value FROM dropbox_token WHERE name='".$name."'";
	$query = query($sql);
	$data = fetch($query);

	if(empty($data['value'])){
		return null;
	}

	return @unserialize($data['value']);
}

function demo_token_delete( $name ) {
	@unlink( "tokens/$name.token" );
}


function enable_implicit_flush() {
	if ( function_exists( 'apache_setenv' ) ) {
		@apache_setenv( 'no-gzip', 1 );
	}
	@ini_set( 'zlib.output_compression', 0 );
	@ini_set( 'implicit_flush', 1 );
	for ( $i = 0; $i < ob_get_level(); $i ++ ) {
		ob_end_flush();
	}
	ob_implicit_flush( 1 );
	echo "<!-- " . str_repeat( ' ', 2000 ) . " -->";
}