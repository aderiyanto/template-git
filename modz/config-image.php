<?php
#img.php
#############################
define("IMG_MEDIUM_WIDTH",600);
define("IMG_MEDIUM_HEIGHT",600);
define("IMG_SMALL_WIDTH",100);
define("IMG_SMALL_HEIGHT",100);

#changeban.php
#############################
define("BANNER_LARGE_WIDTH",540);
define("BANNER_LARGE_HEIGHT",300); 
define("BANNER_MEDIUM_WIDTH",300);
define("BANNER_MEDIUM_HEIGHT",1000);
define("BANNER_SMALL_WIDTH",100);
define("BANNER_SMALL_HEIGHT",1000);

#newsinfo.php
#############################
#newsinfo image
define("NEWSINFO_LARGE_WIDTH",750);
define("NEWSINFO_LARGE_HEIGHT",100000);
define("NEWSINFO_MEDIUM_WIDTH",300);
define("NEWSINFO_MEDIUM_HEIGHT",300);
define("NEWSINFO_SMALL_WIDTH",100);
define("NEWSINFO_SMALL_HEIGHT",100);

#newinfo slide
define("NEWSINFO_SLIDE_LARGE_WIDTH",750);
define("NEWSINFO_SLIDE_LARGE_HEIGHT",300);
define("NEWSINFO_SLIDE_MEDIUM_WIDTH",300);
define("NEWSINFO_SLIDE_MEDIUM_HEIGHT",300);
define("NEWSINFO_SLIDE_SMALL_WIDTH",100);
define("NEWSINFO_SLIDE_SMALL_HEIGHT",100);

#gallery_album.php
#############################
define("GALLERY_ALBUM_LARGE_WIDTH",750);
define("GALLERY_ALBUM_LARGE_HEIGHT",600);
define("GALLERY_ALBUM_MEDIUM_WIDTH",300);
define("GALLERY_ALBUM_MEDIUM_HEIGHT",300);
define("GALLERY_ALBUM_SMALL_WIDTH",100);
define("GALLERY_ALBUM_SMALL_HEIGHT",100);

#gallery.php
#############################
define("GALLERY_LARGE_WIDTH",600);
define("GALLERY_LARGE_HEIGHT",600);
define("GALLERY_MEDIUM_WIDTH",205);
define("GALLERY_MEDIUM_HEIGHT",205);
define("GALLERY_SMALL_WIDTH",100);
define("GALLERY_SMALL_HEIGHT",100);

#admin_developer.php & admin.php
#############################
define("PROFILE_LARGE_WIDTH",450);
define("PROFILE_LARGE_HEIGHT",450);
define("PROFILE_MEDIUM_WIDTH",128);
define("PROFILE_MEDIUM_HEIGHT",128);
define("PROFILE_SMALL_WIDTH",80);
define("PROFILE_SMALL_HEIGHT",80);
?>