<?php
	#define save license variable
	$lc = unserialize(strdecode($license_0195));

	#clean all input
	$_GET     = cleanup($_GET);
	$_POST    = cleanup($_POST);
	$_COOKIE  = cleanup($_COOKIE);

	#get all data from post
	foreach ($_POST as $key=>$value){
		$$key=$value;
	}
	#get all data from get
	foreach ($_GET as $key=>$value){
		$$key=$value;
	}

	#exclude some action to be tracked from css/js request triggered of minify.php file
	if($page != 'minify'){
		#check bot
		$checkVisitor = getval("wbId", "webbot", "wbIpAddress", visitor_ip());
		if(is_numeric($checkVisitor)){
			blackhole('justVisit');
		}

		###########################################################################
		# STATISTICAL CODE
		###########################################################################

		//This cookie is needed to prevent counter from incrementing itself
		//everytime someone re-access the page on the same window session
		if (!isset($_COOKIE['justVisit'])){
			setcookie("justVisit",true,0);
		}

		if (!$_COOKIE['justVisit']){
			$sql="UPDATE counter SET conValue=conValue+1 WHERE  conId=1";
			$query=query($sql);
		}

		$sql="UPDATE counter SET conValue=conValue+1 WHERE conId=2";
		$query=query($sql);

		###########################################################################
		# ONLINE NOW CODE
		###########################################################################
         /*
		if (!isset($_COOKIE['stillonlinenow'])){
			setcookie("stillonlinenow",true,time()+(15*60),"/");

			//insert data
			$remoteip = cleanup(visitor_ip());
			$sql = "INSERT INTO onlinenow VALUES (NULL,'$remoteip','g',$now)";
			$query = query($sql);

			setcookie("onlinenowid",$olid,0,"/");
		}

		if ($_COOKIE['stillonlinenow']){
			$sql="UPDATE onlinenow SET olLastVisit=$now WHERE olId='".$_COOKIE['onlinenowid']."'";
			$query=query($sql);
		}
         */
         $remoteip=visitor_ip();
         $cekip=getval("olIP","onlinenow","olIP='$remoteip'");
		if ($cekip){
            $sql="UPDATE onlinenow SET olLastVisit=$now WHERE olId='".$_COOKIE['onlinenowid']."'";
			$query=query($sql);
		}else{
            $sql="INSERT INTO onlinenow VALUES (NULL,'$remoteip','g','$now')";
			$query=query($sql);
		}

		//delete all 'onlinenow' data if more than 15 mins ago
		$minsago=$now-(15*60);
		$sql="DELETE FROM onlinenow WHERE olLastVisit<'$minsago'";
		$query=query($sql);
		###########################################################################

		#SYSTEMLOG
		//get logging setting /web
		$url = cleanup('http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);
		if(SITE_LOG == 'on'){
			$ip = visitor_ip();

			//check if current ip visite the url before in last 24 hours
			$minrange=$now-(60*60*24);
			$sql="SELECT * FROM systemlog WHERE logIp='$ip' AND logUrl='$url' AND logTime BETWEEN $minrange AND $now";
			$query=query($sql);
			$numvisit=rows($query);
			$data=fetch($query);
			$logcounter=$data['logCounter'];

			if($numvisit==0){
				//save to database if never visit befor
				$sql="INSERT INTO systemlog VALUES ($now,'$ip','$url',2,1,'')";
			}else{
				//update counter if this ip already visit the url befor
				$sql="UPDATE systemlog SET logCounter=$logcounter+1 WHERE logIp='$ip' AND logUrl='$url'";
			}
			$query=query($sql);
		}

		## make surewebsite is 'on' ##
		if (SITE_STATUS=="off"){
			header("location:/docs/off.php");
			exit();
		}
	}

	###########################################################################
	#MULTILINGUAL FEATURE
	if(count($lc['language'])!=0){
		#define default language
		$lg['default'] =$lc['language_default'];
		if(empty($_COOKIE['lang'])){
			$_COOKIE['lang'] = $lg['default'];
		}

		#set gettext variable
		$lg['active'] = (($_COOKIE['lang'] AND in_array($_COOKIE['lang'], $lc['language'])) ? $_COOKIE['lang']:$lg['default']);
		$lg['path'] = "../lang";
		$lg['domain'] = "messages";

		#set gettext invironment
		setlocale(LC_ALL, $lg['active']);
		setlocale(LC_TIME, $lg['active']);
		putenv("LANG=".$lg['active']);

		#additional setting to debug while develope progress, it's use to avoid apache cache
		if(getconfig('SHOW_DEBUG') == TRUE){
			#set .mo file path and get last update
			$lg['filename'] = $lg['path'].'/'.$lg['active'].'/LC_MESSAGES/'.$lg['domain'].'.mo';
			$lg['mtime'] = filemtime($lg['filename']);

			if (!file_exists($lg['filename_new'])) {
				$dir = scandir(dirname($lg['filename']));
				foreach ($dir as $file) {
					if(in_array($file, array('.','..', $lg['domain'].'.po', $lg['domain'].'.mo'))) continue;
					if(pathinfo(dirname($lg['filename']).DIRECTORY_SEPARATOR .$file, PATHINFO_EXTENSION)=='mo'){
						//if file name using "admin.mo" prefix just continue
						if(strpos($file, "admin.mo")) continue;
						unlink(dirname($lg['filename']).DIRECTORY_SEPARATOR .$file);
					}
				}

				#controlling .mo file to avoid apache cache
				$lg['filename_new'] = $lg['path'].'/'.$lg['active'].'/LC_MESSAGES/'.$lg['domain'].'_'.$lg['mtime'].'.mo';
				@copy($lg['filename'], $lg['filename_new']);

				#compute the new domain name
				$lg['domain'] = $lg['domain'] .'_'.$lg['mtime'];
			}
		}

		#binding current language
		bindtextdomain($lg['domain'],$lg['path']);

		#activate current language
		textdomain($lg['domain']);

		if($_COOKIE['lang'] AND in_array($_COOKIE['lang'], $lc['language'])){
			$lang = $_COOKIE['lang'];
		}
		else{
			$lang = $lc['language_default'];
		}

		#define lang acronim
		$active_lang = substr($lang,0,2);

		#remove variable
		unset($lg);
	}

	#safe license variable
	$lc_lang = $lc['language'];
	$lc_lang_default = $lc['language_default'];
	unset($lc);
?>