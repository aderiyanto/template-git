<?php 
error_reporting(0);
date_default_timezone_set("Asia/Jakarta"); 
$now=time();

define("SITE_NAME","Template");
define("SITE_TITLE","Basic Template");
define("SITE_KEYWORD","basic, cms, template");
define("SITE_DESCRIPTION","ini adalah bagian dari cms template dasar");
define("SITE_STATUS","on");
define("ONLINE_BACKUP","on");
define("SITE_EMAIL","info@bisnislab.com");
define("SITE_DEVELOPER_EMAIL","info@bisnislab.com");
define("MAIL_SIGNATURE","regards,

Template");
define("SITE_IMAGE","");
define("SITE_COLOR","#93c47d");
define("WATERMARK_IMAGE","watermarkimage.png");
define("WATERMARK_POSITION","4");
define("BLACKLIST_IP","");
define("WHITELIST_IP","");
define("SITE_LOG","off");
define("ADMIN_LOG","off");
define("IMAGE_QUALITY","80");
define("COMMENT_MOD_BLOG","off");
define("BACKGROUND_SETTING","center top,no-repeat");
define("WEBSITE_LOGO_DESKTOP","/assets/imgbank/16102017/rw-property_k3mfu_6.png");
define("WEBSITE_LOGO_MOBILE","/assets/imgbank/16102017/rw-property_k3mfu_6.png");
?>