function initialiseInstance(editor) {  
	//This script taken from www.matthewkenny.com 
	var container = $('#' + editor.editorId);  
	$(editor.formElement).find("input[type=submit]").click(  
		function(event) {
			tinyMCE.triggerSave();
			$("#" + editor.id).valid();
				container.val(editor.getContent());  
		}  
	);  
}  

tinymce.init({ 
	selector:'textarea.tinymce-simple',
	//width : 700,
	plugins : 'media advlist link searchreplace lists fullscreen emoticons contextmenu colorpicker anchor autolink textcolor table code autoresize link image lists charmap print preview wordcount',
	menubar: false,
	force_p_newlines : true,
	toolbar: ['| bold italic underline striketrough | alignleft aligncenter alignright alignjustify | styleselect fontselect | fontsizeselect |  bullist numlist | outdent indent blockquote | link unlink emoticons'
	],
	contextmenu: 'link image inserttable | cell row column deletetable',
	resize: true,
	force_br_newlines : true,
	force_p_newlines : false,
	forced_root_block : '',
	disk_cache : false,
	debug : false,
	document_base_url : 'http://192.168.1.25',
	relative_urls : false, 
	remove_script_host : false,
	init_instance_callback : 'initialiseInstance',
	setup: function (editor) {
        editor.on('change', function () {
            editor.save();
        });
    }
});