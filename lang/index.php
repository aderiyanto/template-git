<?php

  	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/mainmod-extend.php");
	include("../modz/connic.php");
	include("../modz/getall.php");

    $page=1;
    setcookie("url_paket","",0,"/");

    $querymaps = query("SELECT packageLocationCoordinate,packageName,packageLocation,packageId,packagePermalink FROM package WHERE packageDisplay='y'");
    while($dquerymaps = fetch($querymaps)){
        $coordinate = json_decode($dquerymaps['packageLocationCoordinate']);
        foreach($coordinate as $v){
            list($latall[],$longall[]) = explode(",",$v);
            $packagenameall[]=$dquerymaps['packageName'];
            $packagelocall[]=cleanup($dquerymaps['packageLocation'],"admin"); 
            $packageidall[]=$dquerymaps['packageId'];
            $packagepermaall[]=$dquerymaps['packagePermalink']; 
        }
    }
    $latall = array_filter($latall);
    $longall = array_filter($longall);


    #minify html
    ob_start('');
?>
<!DOCTYPE html>
<html lang="<?php print $active_lang;?>">
<head>
    <?php include("com/com-meta.php"); ?>
    <title><?php print SITE_NAME; ?> | <?php print SITE_TITLE; ?></title>
    <?php
        $cssfiles = array(
            'style-web.css',
            'bootstrap.css',
            'animasi.css',
            'font-awesome.css',
            'ionicons.css',
            'media-screen.css',
            'owl.carousel.css',
            'owl.theme.css',
            'bootstrap-datetimejs.css',
            'bootstrap-toggle.min.css',
            'sweetalert.css'

        );?>
        <link rel="stylesheet" type="text/css" href="<?php print filecache('render', $cssfiles, 'css');?>"/>
        <link href="https://fonts.googleapis.com/css?family=Ropa+Sans" rel="stylesheet"> 
        <?php print customscript();?>
        <?php
        $jsfiles = array(
            'jquery.min.js',
            'bootstrap.min.js',
            'owl.carousel.js',
            'jquery.validate.js',
            'jquery.form.js',
            'validate.js',
            'sweetalert-dev.js'
        );
    ?>
    <script type="text/javascript" src="<?php print filecache('render', $jsfiles, 'js');?>"></script>
    <?php include('com/com-extend-head.php'); ?>
    <script type="text/javascript">
        function countdowndatetime(id,startdate,status,timestamp,statuspaket){
            var timestampnow=<?php print $now;?>;
            var times = " 23:59:59";
            var newstartdate = startdate.concat(times);
            var countDownDate = new Date(newstartdate).getTime();

            var datestart = new Date(startdate).getTime();
            // Update the count down every 1 second
            var x = setInterval(function() {

            // Get todays date and time
            var now = new Date().getTime();

            // Find the distance between now an the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            if(days > 0){
              days=days +  " <?php print _('web_days')?>";
            }else{
              days='';
            }

            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            if(hours > 0){
              hours=hours +  " <?php print _('web_hour')?>";
            }else{
              hours='';
            }

            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            if(minutes > 0){
              minutes=minutes +  " <?php print _('web_minute')?>";
            }else{
              minutes='';
            }
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            // If the count down is finished, write some text
            if(status=="taked" || status=="start"){
                        if (distance < 0) {
                            if(status=="taked" && datestart < 1){
                          document.getElementById("demo"+id).innerHTML = "";
                            }else if(status=="taked" && countDownDate <= now){
                          document.getElementById("demo"+id).innerHTML = "<div class='late-taked'><div class='start-project box-countdown'><?php print _('web_long_time_start')?></div>";
                            }else{
                          document.getElementById("demo"+id).innerHTML = "<div class='late-taked'><div class='start-project box-countdown'><?php print _('web_long_time')?></div>";
                            }
                        }else{

                            if(status=="taked"){
                                  document.getElementById("demo"+id).innerHTML = "<div class='countdown-start'><p><?php print _('web_countdownstart')?></p><div class='start-project'>" + days +" "+ hours + " "
                                  + minutes + " " + seconds + " <?php print _('web_second')?> </div></div>";
                            }else{
                                   document.getElementById("demo"+id).innerHTML = "<div class='countdown'><div class='start-project'>" + days +" "+ hours + " "
                                  + minutes + " " + seconds + " <?php print _('web_second')?> </div></div>";
                            }
                        }
            }else{
                if(status=="done"){
                     document.getElementById("demo"+id).innerHTML = "<div class='countdown'><div class='start-project'>"+statuspaket+"</div></div>";
                }else{
                     document.getElementById("demo"+id).innerHTML = "<div class='countdown-start'><div class='start-project box-countdown'>"+statuspaket+"</div></div>";
                }
            }
          }, 1000);
        }
    </script>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">
    <div id="result"></div>
		<?php
			include('com/com-menu.php');
			include('com/com-banner.php');
        ?>

    <section id="home1">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-lg-2">&nbsp;</div>
                <div class="col-md-8 col-lg-8 text-center">
                    <h4>
						<?php
                          $pgcontent = getval("contentDesc","content","contentId='6'");
                          print output($pgcontent);
                    ?>                        
                    </h4>
                </div>
                <div class="col-md-2 col-lg-2">&nbsp;</div>
            </div>
        </div>
	</section>

	<div class="container-fluid social">
		<div class="row">
			<div class="col-sm-6 col-md-6 social-left">
				<div class="row">
					<div class="col-md-3 col-lg-5"></div>
					<div class="col-md-8 col-lg-6">
                        <i class="icon ion-ios-people" style="margin-bottom:15px;"></i>
						<?php
                          $pgcontent = getval("contentDesc","content","contentId='7'");
                          print output($pgcontent);
                        if (empty($is_login) and empty($is_login_person)){
                        ?>

						<div class="box-content" style="margin-top:10px">
							<a href="/masyarakat/daftar" class="btn btn-danger">
				        		<?php print _('home_signup_now') ?> <i class="fa fa-angle-right"></i>
				        	</a>
						</div>
                        <?php } ?>
					</div>
					<div class="col-md-1 col-lg-1"></div>
				</div>
			</div>
			<div class="col-sm-6 col-md-6 social-right">
				<div class="row">
					<div class="col-md-1 col-lg-1"></div>
					<div class="col-md-8 col-lg-6">
                        <i class="fa fa-building-o fa-5x" style="margin-bottom:3px;padding:0px 0px 14px;"></i>
						<?php
                        $pgcontent = getval("contentDesc","content","contentId='8'");
                        print output($pgcontent);
                        if (empty($is_login) and empty($is_login_person)){
                        ?>
						<div class="box-content">
							<a href="/perusahaan/daftar" class="btn btn-danger">
				        		<?php print _('home_signup_now') ?> <i class="fa fa-angle-right"></i>
				        	</a>
						</div>
                        <?php } ?>
					</div>
					<div class="col-md-3 col-lg-5"></div>
				</div>
			</div>
		</div>
	</div>


	<section id="content" class="box-content">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
                    <?php
                        $pgcontent = getval("contentDesc,contentLabel","content","contentId='9'");
                        $pgcontent=output($pgcontent);
                    ?>
					<div class="main-title">
						<h2><?php print $pgcontent['contentLabel']; ?></h2>
					</div>
                    <?php
                        print  $pgcontent['contentDesc'];
                    ?>
				</div>
			</div>
            <div class="row">
                <div class="col-md-12 col-sm-12 text-center" style="margin-top:-5px; margin-bottom:15px;">
                    <a href="/paket-pembangunan/baru">
                        <button class="btn btn-md btn-primary">
                            <?php print _("web_new"); ?>
                        </button>
                    </a>
                    <a href="/paket-pembangunan/proses">
                        <button class="btn btn-md btn-info">
                            <?php print _("web_start"); ?>
                        </button>
                    </a>
                    <a href="/paket-pembangunan/selesai">
                        <button class="btn btn-md btn-success">
                            <?php print _("web_pack_done"); ?>
                        </button>
                    </a>
                    <a href="/paket-pembangunan/tidak-selesai">
                        <button class="btn btn-md btn-warning">
                            <?php print _("web_not"); ?>
                        </button>
                    </a>
                </div>
            </div>
			<div class="row">
                <?php
                $querypackage = query("SELECT * FROM package WHERE packageDisplay = 'y' and packageDelete='0' ORDER by packageAddedTimestamp DESC LIMIT 18");
                while($datapackage = fetch($querypackage)){
                    $datapackage=output($datapackage);
                    $queryimagepackage = query("SELECT imageDir, imageFile FROM package_image WHERE packageid={$datapackage['packageId']} AND imagePrimary='y'");
                    $dataimagepackage = fetch($queryimagepackage);
                    $dataimagepackage=output($dataimagepackage);
                ?>
                <div class="col-sm-6 col-md-4">
                    <div class="panel panel-default">
                        <div class="ribbon">
                            <img src="/assets/interface/badge.png">
                        </div>
                        <div class="panel-heading">
                            <a href="/paket/<?php print $datapackage['packageId'] ?>/<?php print $datapackage['packagePermalink'] ?>">
                                <?php if ($dataimagepackage['imageDir']){?>
                                    <img src="/assets/package/<?php print $dataimagepackage['imageDir'] ?>/<?php print genthumb($dataimagepackage['imageFile'],'m') ?>" srcset="/assets/package/<?php print $dataimagepackage['imageDir'] ?>/<?php print genthumb($dataimagepackage['imageFile'],'m') ?> 400w, /assets/package/<?php print $dataimagepackage['imageDir'] ?>/<?php print genthumb($dataimagepackage['imageFile'],'m') ?> 1366w, /assets/package/<?php print $dataimagepackage['imageDir'] ?>/<?php print genthumb($dataimagepackage['imageFile'],'m') ?> 1920w">
                                <?php }else{?>
                                    <img src="/assets/interface/default.png">
                                <?php } ?>
                            </a>
                            <div id="badge-ribbon" class="tes1">
                                <span>
                                    <p><?php print number_format($datapackage['packagePoin'],0,",","."); ?> Poin</p>
                                </span>
                            </div>
                        </div>
                        <div class="panel-body">
                            <?php
                            if($datapackage['compId'] != 0 and $datapackage['packageStatus']=="taked" or $datapackage['packageStatus']=="start" or $datapackage['packageStatus']=="done" or $datapackage['packageStatus']=="not"){
                            ?>
                            <div class="company-detail">
                                <?php print _('web_taked_by') ?> :
                                <?php
                                $companyName = getval("compName,compPermalink,compPoin","company","compId={$datapackage['compId']}");
                                ?>
                                <a href="/perusahaan/detail/<?php print $datapackage['compId'] ?>/<?php print  $companyName['compPermalink']?>"><h3>
                                <?php print output($companyName['compName']);?></h3>
                                </a>
                                <?php 
                                $sqlstar="SELECT ratingStar FROM rating_point WHERE {$companyName['compPoin']} >= ratingPointMin or {$companyName['compPoin']}>=ratingPointMax ORDER BY ratingStar DESC Limit 1";
                                $querystar=query($sqlstar);
                                $datastar=fetch($querystar);
                                ?>
                                <div class="rating">
                                    <?php
                                        for ($g=1; $g <=$datastar['ratingStar'] ; $g++) { 
                                            print '<span class="fa fa-star checked"></span>';
                                        }
                                        for ($h=$g; $h <=5 ; $h++) { 
                                            print '<span class="fa fa-star"></span>';
                                        }
                                    ?>
                                </div>
                                <div class="stamp">
                                    <?php 
                                    if($datapackage['packageStatus']=="taked"){ ?>
                                        <img src="/assets/interface/status.png">
                                    <?php }
                                    if($datapackage['packageStatus']=="start"){ ?>
                                        <img src="/assets/interface/status_proses.png">
                                    <?php }
                                    if($datapackage['packageStatus']=="done"){ ?>
                                        <img src="/assets/interface/status_selesai.png">
                                    <?php }
                                    if($datapackage['packageStatus']=="not"){ ?>
                                        <img src="/assets/interface/status_tidak_selesai.png">
                                    <?php } ?>
                                    
                                </div>
                            </div>
                            <?php } ?>
                            <div class="info">
                                <div class="cause-info">
                                    <p><a href="/paket/<?php print $datapackage['packageId'] ?>/<?php print $datapackage['packagePermalink'] ?>"><?php print $datapackage['packageCode']; ?></a></p>
                                    <div class="info-bidang">
                                        <i class="fa fa-tags"></i> <span>
                                            <?php $catName = getval("catName","package_category","catId={$datapackage['catId']}");
                                            print $catName; ?></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="desc-info jobs">
                                            <a href="/paket/<?php print $datapackage['packageId'] ?>/<?php print $datapackage['packagePermalink'] ?>">
                                                <h4><?php print ucwords($datapackage['packageName']); ?></h4>
                                            </a>
                                            <?php if ($datapackage['packageNominal'] > 0) { ?>
                                                <span class="text-success" style="text-transform: capitalize;"><?php print _('web_value_pack').": "; print "Rp".number_format($datapackage['packageNominal'],0,",",".").",-"; ?> 
                                                </span>
                                            <?php } ?>
                                            <?php if (($datapackage['packageNominalEstimated'] > 0) and ($datapackage['packageNominal'] == 0) ) { ?>
                                                <span class="text-success" style="text-transform: capitalize;"><?php print _('web_pack_value').": "; print "Rp".number_format($datapackage['packageNominalEstimated'],0,",",".").",-"; ?> </span>
                                            <?php } ?><br/>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-footer">
                            <?php
                            $idpackage=$datapackage['packageId']; 
                            if($datapackage['packageStatus'] == "new"){ ?>
                                <a href="/paket/<?php print $datapackage['packageId'] ?>/<?php print $datapackage['packagePermalink'] ?>">
                                    <?php print _('web_click_info') ?>
                                </a>
                            <?php }elseif($datapackage['packageStatus'] == "taked"){
                                if (($datapackage['packageStartTimestamp'] > $now ) and ($datapackage['packageEndTimestamp'] > 0)) {

                                    $tglmulai = date('Y-m-d', $datapackage['packageStartTimestamp']);
                                    $tglmulai = new DateTime($tglmulai);
                                    $tglnow = new DateTime();

                                    $selisih = $tglnow->diff($tglmulai)->format("%a");
                                    $selisih = $selisih+1;
                                    print _('web_work_do')." ".$selisih." "._('web_days_more');

                                }else{
                                    print _('web_prepare_process');
                                }
                             
                            } elseif ($datapackage['packageStatus'] == "start") {
                                if ($now < ($datapackage['packageEndTimestamp'])) {
                                    $tglselesai = date('Y-m-d', $datapackage['packageEndTimestamp']);
                                    $tglselesai = new DateTime($tglselesai);
                                    $tglnow = new DateTime();

                                    $selisih = $tglnow->diff($tglselesai)->format("%a");
                                    $selisih = $selisih+1;
                                    print _('web_work_done_in')." ".$selisih." "._('web_days_more'); 
                                }elseif ($now > ($datapackage['packageEndTimestamp'])) {
                                    print _('web_long_time_end'); 
                                }else {
                                    print _('web_long_time');
                                }
                                
                            }elseif ($datapackage['packageStatus'] == "done") {
                                print _('web_work_done');
                            }elseif ($datapackage['packageStatus'] == "not") {
                                print _('web_work_not_done');
                            }

                            ?>
                        </div>
                        
                    </div>
                </div>
                <?php
                }
                ?>
			</div>
			<div class="row">
				<div class="col-md-12 text-center mt-25">
					<a href="/paket-pembangunan" class="btn btn-danger">
		        		<?php print _('home_loadmore') ?> <i class="fa fa-angle-right"></i>
		        	</a>
				</div>
			</div>
		</div>
	</section>
    <div id="content" class="box-content">
    	<div id="map-project">
            <div class="main-title text-center">
                <h2><?php print _('title_map')?></h2>
            </div>
            <br/>
    		<div class="container-fluid">
    			<div class="row">
                    <p id="map" style="width:100%; height:550px;"></p>
    			</div>
    		</div>
    	</div>
    </div>
	<section id="bidang">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
                    <?php
                        $pgcontent = getval("contentDesc,contentLabel","content","contentId='10'");
                        $pgcontent=output($pgcontent);
                    ?>
					<div class="main-title">
						<h2><?php print $pgcontent['contentLabel'] ?></h2>
					</div>
                    <?php
                        print $pgcontent['contentDesc'];
                    ?>
				</div>
			</div>
			<div class="row">
                <?php
                $querypackagecat = query("SELECT * FROM package_category order by catSort");
                while($datapackagecat = fetch($querypackagecat)){
                    $datapackagecat=output($datapackagecat);
                ?>
				<div class="col-xs-6 col-sm-3 col-md-2 bidang">
					<div class="panel panel-default">
                        <a href="/bidang/<?php print $datapackagecat['catId'] ?>/<?php print $datapackagecat['catPermalink'] ?>">
                            <div class="panel-body">
                                <div class="icon">
                                    <img src="/assets/packagecat/<?php print $datapackagecat['catDirPic'] ?>/<?php print genthumb($datapackagecat['catFilePic'],'s') ?>" alt="">
                                </div>
                                <div class="icon-text"><?php print $datapackagecat['catName']?></div>
                            </div>
                        </a>
                    </div>
				</div>

                <?php
                }
                ?>
			</div>
		</div>
	</section>

    <?php $querynews = query("SELECT * FROM newsinfo order by newsAddedOn   DESC LIMIT 3"); 
    $totalnews = rows($querynews);
    if ($totalnews > 0) {
    ?>
	<section id="news">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
                    <?php
                        $pgcontent = getval("contentDesc,contentLabel","content","contentId='11'");
                        $pgcontent= output($pgcontent);
                    ?>
					<div class="main-title">
						<h2><?php print $pgcontent['contentLabel'] ?></h2>
					</div>
                    <?php
                        print $pgcontent['contentDesc'];
                    ?>
				</div>
			</div>
			<div class="row news-content">
                <?php
                
                while($datanews = fetch($querynews)){
                    $datanews=output($datanews);
                ?>
				<div class="col-sm-4 col-md-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<a href="/berita/<?php print $datanews['newsId']; ?>/<?php print $datanews['newsPermalink'] ?>">
								<img src="/assets/news/<?php print $datanews['newsDir'] ?>/<?php print genthumb($datanews['newsPic'],'m') ?>" srcset="/assets/news/<?php print $datanews['newsDir'] ?>/<?php print genthumb($datanews['newsPic'],'m') ?> 400w, /assets/news/<?php print $datanews['newsDir'] ?>/<?php print genthumb($datanews['newsPic'],'m') ?> 1366w, /assets/news/<?php print $datanews['newsDir'] ?>/<?php print genthumb($datanews['newsPic'],'m') ?> 1920w" class="img-responsive">
							</a>
						</div>
						<div class="panel-body">
							<div class="desc-info">
								<i class="fa fa-clock-o"></i> <span class="text-danger"><?php print formatdate("d F Y H:i",$datanews['newsAddedOn']); ?> WIB</span>
							</div>
							<div class="cause-info">
								<a href="/berita/<?php print $datanews['newsId']; ?>/<?php print $datanews['newsPermalink'] ?>">
									<h4><?php print $datanews['newsTitle']; ?></h4>
								</a>
							</div>
							<p><?php print $datanews['newsHeadline'] ?></p>
						</div>
					</div>
				</div>
                <?php
                }
                ?>
			</div>
			<div class="row">
				<div class="col-md-12 text-center mt-25 box-content">
					<a href="/berita" class="btn btn-danger">
		        		<?php print _('home_loadmore') ?> <i class="fa fa-angle-right"></i>
		        	</a>
				</div>
			</div>
		</div>
	</section>
    <?php } ?>

    <?php $querytopcomp = query("SELECT * FROM company WHERE compStatus = 'y' AND compDeleted='0' AND compPoin != 0 ORDER by compPoin DESC"); 
    $totaltopcomp = rows($querytopcomp);
    if ($totaltopcomp > 0){
    ?>
	<section id="volunteer">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
                    <?php
                        $pgcontent = getval("contentDesc,contentLabel","content","contentId='12'");
                        $pgcontent= output($pgcontent);
                    ?>
					<div class="main-title">
						<h2><?php print $pgcontent['contentLabel'] ?></h2>
					</div>
                    <?php
                        print $pgcontent['contentDesc'];
                    ?>
				</div>
			</div>
			<div class="row">
                <?php
                
                while($datatopcomp = fetch($querytopcomp)){
                    $datatopcomp=output($datatopcomp);
                   if ($datatopcomp['compDirPic']){
                ?>
				<div class="col-xs-6 col-sm-4 col-md-2 ">
					<div class="box-volunteer">
                        <a href="/perusahaan/detail/<?php print $datatopcomp['compId'] ?>/<?php print  $datatopcomp['compPermalink']?>">
						<img src="/assets/logo/<?php print $datatopcomp['compDirPic'] ?>/<?php print genthumb($datatopcomp['compFilePic'],'m') ?>">
                        </a>
					</div>
				</div>
                <?php
                    }
                }
                ?>
			</div>
		</div>
	</section>
    <?php } ?>

	<?php
		include('com/com-footer.php');
		include('com/com-extend-body.php');
	?>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAiqlCyAjrLA3YAswfDvLGWL5tFL7S4L04&callback=initMap"></script>
    <script>
	<?php
		if(!empty($latall)){
			?>
			function initMap() {
				<?php
				if(count($latall) == 1){
					?>
					var myLatLng = {lat: <?php print $latall[0]; ?>, lng: <?php print $longall[0]; ?>};

					var directionsService = new google.maps.DirectionsService;
					var directionsDisplay = new google.maps.DirectionsRenderer;
					var map = new google.maps.Map(document.getElementById('map'), {
						zoom: 16,
						center: myLatLng,
                        styles: [
                              {
                                "elementType": "geometry",
                                "stylers": [
                                  {
                                    "color": "#f5f5f5"
                                  }
                                ]
                              },
                              {
                                "elementType": "labels.icon",
                                "stylers": [
                                  {
                                    "visibility": "off"
                                  }
                                ]
                              },
                              {
                                "elementType": "labels.text.fill",
                                "stylers": [
                                  {
                                    "color": "#616161"
                                  }
                                ]
                              },
                              {
                                "elementType": "labels.text.stroke",
                                "stylers": [
                                  {
                                    "color": "#f5f5f5"
                                  }
                                ]
                              },
                              {
                                "featureType": "administrative.land_parcel",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                  {
                                    "color": "#bdbdbd"
                                  }
                                ]
                              },
                              {
                                "featureType": "poi",
                                "elementType": "geometry",
                                "stylers": [
                                  {
                                    "color": "#eeeeee"
                                  }
                                ]
                              },
                              {
                                "featureType": "poi",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                  {
                                    "color": "#757575"
                                  }
                                ]
                              },
                              {
                                "featureType": "poi.park",
                                "elementType": "geometry",
                                "stylers": [
                                  {
                                    "color": "#e5e5e5"
                                  }
                                ]
                              },
                              {
                                "featureType": "poi.park",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                  {
                                    "color": "#9e9e9e"
                                  }
                                ]
                              },
                              {
                                "featureType": "road",
                                "elementType": "geometry",
                                "stylers": [
                                  {
                                    "color": "#ffffff"
                                  }
                                ]
                              },
                              {
                                "featureType": "road.arterial",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                  {
                                    "color": "#757575"
                                  }
                                ]
                              },
                              {
                                "featureType": "road.highway",
                                "elementType": "geometry",
                                "stylers": [
                                  {
                                    "color": "#dadada"
                                  }
                                ]
                              },
                              {
                                "featureType": "road.highway",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                  {
                                    "color": "#616161"
                                  }
                                ]
                              },
                              {
                                "featureType": "road.local",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                  {
                                    "color": "#9e9e9e"
                                  }
                                ]
                              },
                              {
                                "featureType": "transit.line",
                                "elementType": "geometry",
                                "stylers": [
                                  {
                                    "color": "#e5e5e5"
                                  }
                                ]
                              },
                              {
                                "featureType": "transit.station",
                                "elementType": "geometry",
                                "stylers": [
                                  {
                                    "color": "#eeeeee"
                                  }
                                ]
                              },
                              {
                                "featureType": "water",
                                "elementType": "geometry",
                                "stylers": [
                                  {
                                    "color": "#c9c9c9"
                                  }
                                ]
                              },
                              {
                                "featureType": "water",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                  {
                                    "color": "#9e9e9e"
                                  }
                                ]
                              }
                            ]
					});
					var mercLatLng = {lat: <?php print $latall[0]; ?>, lng: <?php print $longall[0]; ?>};
					var marker = new google.maps.Marker({
						position: mercLatLng,
						map: map,
						title: "<?php print output($data['merchantName']); ?>",
					});
					if( (navigator.platform.indexOf("iPhone") != -1)
						|| (navigator.platform.indexOf("iPod") != -1)
						|| (navigator.platform.indexOf("iPad") != -1))
						 var urlLoc = '<a class="label label-success" href="maps://maps.google.com/maps?daddr=<?php print $latall[0]; ?>,<?php print $longall[0]; ?>&amp;ll=" target="_blank"><i class="fa fa-location-arrow" aria-hidden="true"></i>&nbsp;<?php print _('brosispku_get_direction'); ?></a>';
					else
						 var urlLoc = '<a class="label label-success" href="https://maps.google.com/maps?daddr=<?php print $latall[0]; ?>,<?php print $longall[0]; ?>&amp;ll=" target="_blank"><i class="fa fa-location-arrow" aria-hidden="true"></i>&nbsp;<?php print _('brosispku_get_direction'); ?></a>';

					var ContetWin = '<div class="panel panel-body"><h5><?php print output($data['merchantName']); ?></h5>'+urlLoc+'</div>';
					var infowindow = new google.maps.InfoWindow({
					  content: ContetWin
					});
					marker.addListener('click', function() {
					  infowindow.open(map, marker);
					});
					<?php
				}else{
					?>
					var directionsService = new google.maps.DirectionsService;
					var directionsDisplay = new google.maps.DirectionsRenderer;
					var cwc2011_venue_data = [
						<?php
						foreach($latall as $key => $value){
						?>
						{
							latlng: new google.maps.LatLng(<?php print $value; ?>, <?php print $longall[$key]; ?>)
						},
						<?php
						}
						?>
					];
					var urki;
					var cwc2011_venue_data_win = [
					<?php
						foreach($latall as $key => $value){
							?>
							"<div class=\"panel panel-body\"><b><?php print _("map_package") ?></b> <a href=\"/paket/<?php print $packageidall[$key] ?>/<?php print $packagepermaall[$key] ?>\" target=\"_blank\"><?php print $packagenameall[$key] ?></a><br/><b><?php print _("map_location") ?></b> <?php print $packagelocall[$key] ?><br/><a class=\"label label-success\" href=\"https://maps.google.com/maps?daddr=<?php print $value; ?>,<?php print $longall[$key]; ?>&amp;ll=\" target=\"_blank\"><i class=\"fa fa-location-arrow\" aria-hidden=\"true\"></i>&nbsp;<?php print _('get_direction'); ?></a></div>",
							<?php
						}
					?> ];
					var map = new google.maps.Map(document.getElementById("map"), {
						center: new google.maps.LatLng(0, 0),
						zoom: 0,
						mapTypeId: google.maps.MapTypeId.ROADMAP,
                        styles: [
                              {
                                "elementType": "geometry",
                                "stylers": [
                                  {
                                    "color": "#f5f5f5"
                                  }
                                ]
                              },
                              {
                                "elementType": "labels.icon",
                                "stylers": [
                                  {
                                    "visibility": "off"
                                  }
                                ]
                              },
                              {
                                "elementType": "labels.text.fill",
                                "stylers": [
                                  {
                                    "color": "#616161"
                                  }
                                ]
                              },
                              {
                                "elementType": "labels.text.stroke",
                                "stylers": [
                                  {
                                    "color": "#f5f5f5"
                                  }
                                ]
                              },
                              {
                                "featureType": "administrative.land_parcel",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                  {
                                    "color": "#bdbdbd"
                                  }
                                ]
                              },
                              {
                                "featureType": "poi",
                                "elementType": "geometry",
                                "stylers": [
                                  {
                                    "color": "#eeeeee"
                                  }
                                ]
                              },
                              {
                                "featureType": "poi",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                  {
                                    "color": "#757575"
                                  }
                                ]
                              },
                              {
                                "featureType": "poi.park",
                                "elementType": "geometry",
                                "stylers": [
                                  {
                                    "color": "#e5e5e5"
                                  }
                                ]
                              },
                              {
                                "featureType": "poi.park",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                  {
                                    "color": "#9e9e9e"
                                  }
                                ]
                              },
                              {
                                "featureType": "road",
                                "elementType": "geometry",
                                "stylers": [
                                  {
                                    "color": "#ffffff"
                                  }
                                ]
                              },
                              {
                                "featureType": "road.arterial",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                  {
                                    "color": "#757575"
                                  }
                                ]
                              },
                              {
                                "featureType": "road.highway",
                                "elementType": "geometry",
                                "stylers": [
                                  {
                                    "color": "#dadada"
                                  }
                                ]
                              },
                              {
                                "featureType": "road.highway",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                  {
                                    "color": "#616161"
                                  }
                                ]
                              },
                              {
                                "featureType": "road.local",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                  {
                                    "color": "#9e9e9e"
                                  }
                                ]
                              },
                              {
                                "featureType": "transit.line",
                                "elementType": "geometry",
                                "stylers": [
                                  {
                                    "color": "#e5e5e5"
                                  }
                                ]
                              },
                              {
                                "featureType": "transit.station",
                                "elementType": "geometry",
                                "stylers": [
                                  {
                                    "color": "#eeeeee"
                                  }
                                ]
                              },
                              {
                                "featureType": "water",
                                "elementType": "geometry",
                                "stylers": [
                                  {
                                    "color": "#c9c9c9"
                                  }
                                ]
                              },
                              {
                                "featureType": "water",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                  {
                                    "color": "#9e9e9e"
                                  }
                                ]
                              }
                            ]
					});
					markers = Array();
					infoWindows = Array();
					for (var i = 0; i < cwc2011_venue_data.length; i++) {

						var marker  = new google.maps.Marker({
							position: cwc2011_venue_data[i].latlng,
							map: map,
							title: cwc2011_venue_data[i].name,
							infoWindowIndex : i
						});

						var infoWindow = new google.maps.InfoWindow({
						  content: cwc2011_venue_data_win[i]
						});
						google.maps.event.addListener(marker, 'click',
							function(event)
							{
								infoWindows[this.infoWindowIndex].open(map, this);
							}
						);

						infoWindows.push(infoWindow);
						markers.push(marker);
					}
					var latlngbounds = new google.maps.LatLngBounds();
					for (var i = 0; i < cwc2011_venue_data.length; i++) {
						latlngbounds.extend(cwc2011_venue_data[i].latlng);
					}
					map.fitBounds(latlngbounds);

					<?php
				}
				?>

			}
			<?php
		}
	?>



	function calculateAndDisplayRoute(directionsService, directionsDisplay,position,destination) {
		directionsService.route({
		  origin: position,
		  destination: destination,
		  travelMode: 'DRIVING'
		}, function(response, status) {
		  if (status === 'OK') {
			directionsDisplay.setDirections(response);
		  } else {
			window.alert('Directions request failed due to ' + status);
		  }
		});
	}
	function handleLocationError(browserHasGeolocation, infoWindow, pos) {
		infoWindow.setPosition(pos);
		infoWindow.setContent(browserHasGeolocation ?
		  'Error: The Geolocation service failed.' :
		  'Error: Your browser doesn\'t support geolocation.');
		infoWindow.open(map);
	}
</script>
<script type="text/javascript" src="/javascript/top.js"></script>
    <?php ganalytics(); ?>
</body>
</html>
<?php ob_end_flush();?>
