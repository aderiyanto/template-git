<?php
include("../modz/config-main.php");
include("../modz/config.php");
include("../modz/license.php");
include("../modz/errormsg.php");
include("../modz/mainmod.php");
include("../modz/mainmod-extend.php");
include("../modz/connic.php");
include("../modz/getall-admin.php");

$sql = "SELECT * FROM sms_schedule WHERE schNextSend < $now";
$query = query($sql);
while($data = fetch($query)){
	
	#generate next send
	$baseTimeStamp = strtotime(date('Y-m-d', $now));
	list($h, $m) = explode(':', $data['schTime']);
	$sendOn = ((int) $h * 3600) + ((int) $m * 60);

	if($data['schType'] == 'multiple'){
		$schNextSent = strtotime("+{$data['schRules']} days", $baseTimeStamp);
	} elseif($data['schType'] == 'day'){
		$s_day = explode(',', $data['schRules']);

		foreach($s_day as $key => $value){
			$nextDay[] = strtotime('next '.$value, $baseTimeStamp);
		}

		$schNextSent = min($nextDay);
	} elseif($data['schType'] == 'month'){
		$s_date = explode(',', $data['schRules']);
		
		$result = false;
		foreach($s_date as $key => $value){
			if($value > date('j', $baseTimeStamp)){
				$nextDate = $value;
				$result = true;
				break;
			}
		}
		
		if($result == true){
			$schNextSent = strtotime("+".($nextDate - date('j', $baseTimeStamp))." days", $baseTimeStamp);
		} else {
			$nextDate = min($s_date);
			$schNextSent =  strtotime("$nextDate ".date('F Y', strtotime('+1 month', $baseTimeStamp)));
		}
	} elseif($data['schType'] == 'year'){
		$year = (int) date('Y', $now) + 1;
		$schNextSent = strtotime("{$data['schRules']} $year");
	}
	
	#add hour
	$schNextSent += $sendOn;
	
	if($data['schStatus'] == 'active'){
		#send sems
        $to = $data['schTo'];
        $text = cleanup($data['schText']);

        $send=send_sms($to,$text,'standard');
        if ($send){
	        $sqlx = "UPDATE sms_schedule SET schLastSent='$now', schNextSend='$schNextSent' WHERE schId='{$data['schId']}'";
			$queryx = query($sqlx);
		}
	} else {
		#update schedule info
		$sqlx = "UPDATE sms_schedule SET schNextSend='$now', schStatus='inactive' WHERE schId='{$data['schId']}'";
		$queryx = query($sqlx);
	}
}

	//update activity statistic
	$cron_option['cron_name'] = 'CRON_SEND_SMS_SCHEDULE'; 
	cron_activity_log($cron_option);
?>