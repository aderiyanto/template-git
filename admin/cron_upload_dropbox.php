<?php
	#this file only allow to execute by using cron or cli command, but it still accessible using specific http get parameter
	/*if(isset($_SERVER['HTTP_USER_AGENT']) and $_GET['t'] != date('mY')){
		die('Forbidden Access');
	} */

	//includes all files necessary to support operations
	$basepath = realpath(dirname(__file__).'/..');
	include($basepath."/modz/config-main.php");
	include($basepath."/modz/config.php");
	include($basepath."/modz/license.php");
	include($basepath."/modz/errormsg.php");
	include($basepath."/modz/mainmod.php");
	include($basepath."/modz/mainmod-extend.php");
	include($basepath."/modz/connic.php");
	include($basepath."/modz/getall-admin.php");

	if(ONLINE_BACKUP == 'off'){
		exit;
	}

require_once ($basepath.'/modz/dropbox/demo-lib.php');
demo_init(); // this just enables nicer output

// if there are many files in your Dropbox it can take some time, so disable the max. execution time
set_time_limit( 0 );

require_once ($basepath.'/modz/dropbox/DropboxClient.php');

/** you have to create an app at @see https://www.dropbox.com/developers/apps and enter details below: */
/** @noinspection SpellCheckingInspection */
$dropbox = new DropboxClient( array(
	'app_key'         => getconfig('DROPBOX_APP_KEY'),
	'app_secret'      => getconfig('DROPBOX_APP_SECRET'),
	'app_full_access' => false,
) );


/**
 * Dropbox will redirect the user here
 * @var string $return_url
 */
$return_url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'] . "?auth_redirect=1";

// first, try to load existing access token
$bisnislab_token = load_token( "bisnislab" );

if ( $bisnislab_token ) {
	$dropbox->SetBearerToken( $bisnislab_token );
	//echo "loaded bisnislab token: " . json_encode( $bisnislab_token, JSON_PRETTY_PRINT ) . "\n";
} elseif ( ! empty( $_GET['auth_redirect'] ) ) // are we coming from dropbox's auth page?
{
	// get & store bisnislab token
	$bisnislab_token = $dropbox->GetbisnislabToken( null, $return_url );
	demo_store_token( $bisnislab_token, "bisnislab" );
} elseif ( ! $dropbox->IsAuthorized() ) {
	// redirect user to Dropbox auth page
	$auth_url = $dropbox->BuildAuthorizeUrl( $return_url );
	die( "Authentication required. <a href='$auth_url'>Continue.</a>" );
}
//----------------------------------------------------------------------------check file in folder--------------------------------
	#get root dir
	$dir = substr(dirname(__FILE__), 0, -6);

	#coz this script is a horor, we need to verify the 'backup' path
	if(!is_dir($dir.'/assets/backup')){
		print "Direktori ".$dir.'/assets/backup'. ' tidak ditemukan. Proses upload dihentikan!';
		exit;
	}

 	$scandir = scandir($dir.'/assets/backup');

	#start unset after this . and this ..
	unset($scandir[0],$scandir[1]);

	#get scanned file name
	$fname = get_file_name($scandir,'zip');

	#UPLOAD TO DROPBOX
	$filepath = $dir."/assets/backup/{$fname}";
	if(is_file($filepath)){
		$meta = $dropbox->UploadFile($filepath,'backup_db/'.getconfig('SITE_SHORT_URL').'/'.$fname);
		#print_r($meta);//activate for debug mode

		#get file name without .zip extension
		/*$filepath = substr($filepath, 0, -4);
		unlink($filepath.'.wb');
		unlink($filepath.'.zip');
		unlink($filepath.'.sql');
		unlink($filepath.'.log');*/
	}

	#DELETE CORRUPT FILES
	#the backup procesess maybe can cause the system lack of memory, thus, we must clear the 'backup' directory even
	#if the file not found because the memory lack
	$backup_path = $dir.'/assets/backup';

	#make sure the destination directory is exist
	if(is_dir($backup_path)){
		foreach ($scandir as $file_key => $file_name){
			$file_path = $backup_path . '/'.$scandir[$file_key];

			#make it sure its a file, dangerous if them a directory!
			if(is_file($file_path)){
				unlink($file_path);
				#print $file_path . ' was deleted! \r\n'; // activate for debug mode
			}
		}
	}

	#Do not touch, please :D
	#================================================================================
	#input array of scandir() and extension if file (eg. zip,txt,etc)
	#return string: name of matches file in scanned directory
	function  get_file_name($scandir,$ext){

		foreach($scandir as $file_key => $file_name){

			$file_tester = preg_match('/\.'.$ext.'$/i', $file_name);

			if($file_tester == 1){
				return $file_name;
			}
		}

		#return false if no file matches
		return false;
	}
//---------------------------------------------------------------------------end upload file to dropbox----------------------------
	


/*
	#you have to create an app at https://www.dropbox.com/developers/apps and enter details below:
	$dropbox = new DropboxClient(array(
		'app_key' => getconfig('DROPBOX_APP_KEY'),
		'app_secret' => getconfig('DROPBOX_APP_SECRET'),
		'app_full_access' => 'dropbox',
	),'en');

	#check auth firts
	handle_dropbox_auth($dropbox);

	#get root dir
	$dir = substr(dirname(__FILE__), 0, -6);

	#coz this script is a horor, we need to verify the 'backup' path
	if(!is_dir($dir.'/assets/backup')){
		print "Direktori ".$dir.'/assets/backup'. ' tidak ditemukan. Proses upload dihentikan!';
		exit;
	}

 	$scandir = scandir($dir.'/assets/backup');

	#start unset after this . and this ..
	unset($scandir[0],$scandir[1]);

	#get scanned file name
	$fname = get_file_name($scandir,'zip');

	#UPLOAD TO DROPBOX
	$filepath = $dir."/assets/backup/{$fname}";
	if(is_file($filepath)){
		$meta = $dropbox->UploadFile($filepath,'backup_db/'.getconfig('SITE_SHORT_URL').'/'.$fname);
		#print_r($meta);//activate for debug mode

		#get file name without .zip extension
		$filepath = substr($filepath, 0, -4);
		unlink($filepath.'.wb');
		unlink($filepath.'.zip');
		unlink($filepath.'.sql');
		unlink($filepath.'.log');
	}

	#DELETE CORRUPT FILES
	#the backup procesess maybe can cause the system lack of memory, thus, we must clear the 'backup' directory even
	#if the file not found because the memory lack
	$backup_path = $dir.'/assets/backup';

	#make sure the destination directory is exist
	if(is_dir($backup_path)){
		foreach ($scandir as $file_key => $file_name){
			$file_path = $backup_path . '/'.$scandir[$file_key];

			#make it sure its a file, dangerous if them a directory!
			if(is_file($file_path)){
				unlink($file_path);
				#print $file_path . ' was deleted! \r\n'; // activate for debug mode
			}
		}
	}

	#Do not touch, please :D
	#================================================================================
	#input array of scandir() and extension if file (eg. zip,txt,etc)
	#return string: name of matches file in scanned directory
	function  get_file_name($scandir,$ext){

		foreach($scandir as $file_key => $file_name){

			$file_tester = preg_match('/\.'.$ext.'$/i', $file_name);

			if($file_tester == 1){
				return $file_name;
			}
		}

		#return false if no file matches
		return false;
	}

	function store_token($token, $name){
		$sql = "INSERT INTO dropbox_token VALUES ('".$name."','".serialize($token)."')";
		$query = query($sql);
	}

	function load_token($name){
		$sql="SELECT value FROM dropbox_token WHERE name='".$name."'";
		$query = query($sql);
		$data = fetch($query);

		if(empty($data['value'])){
			return null;
		}

		return @unserialize($data['value']);
	}

	function delete_token($name){
		$sql="DELETE FROM dropbox_token WHERE name='".$name."'";
		$query = query($sql);
	}

	function handle_dropbox_auth($dropbox){
		#try load existing access token from db
		$access_token = load_token('access');

		if(!empty($access_token)){

			$dropbox->SetAccessToken($access_token);

		}elseif(!empty($_GET['auth_callback'])){
			#are we coming from dropbox's auth page?
			#then load our previosly created request token
			$request_token = load_token($_GET['oauth_token']);
			if(empty($request_token)) die('Request token not found!');

			#get & store access token, the request token is not needed anymore
			$access_token = $dropbox->GetAccessToken($request_token);
			store_token($access_token, "access");
			delete_token($_GET['oauth_token']);
		}

		#checks if access token is required
		if(!$dropbox->IsAuthorized()){
			#redirect user to dropbox auth page
			$return_url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME']."?auth_callback=1";
			$auth_url = $dropbox->BuildAuthorizeUrl($return_url);
			$request_token = $dropbox->GetRequestToken();
			store_token($request_token, $request_token['t']);
			header("location: $auth_url");
		}
	}*/
	#================================================================================

	#update activity statistic
	$cron_option['cron_name'] = 'CRON_UPLOAD_DATABASE';
	cron_activity_log($cron_option);
?>