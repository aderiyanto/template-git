<?php
	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");	
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS</title>
<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript">
	function copyToClipboard(element) {
	  var $temp = $("<input>");
	  $("body").append($temp);
	  $temp.val($(element).text()).select();
	  document.execCommand("copy");
	  $temp.remove();
	  $('[data-toggle="tooltip1"]').tooltip({ title: "Copied!"});
	  $('[data-toggle="tooltip1"]').tooltip('show');
	}
	function copy(selector){
	  var $temp = $("<span>");
	  $("body").append($temp);
	  $temp.attr("contenteditable", true)
	       .html($(selector).html()).select()
	       .on("focus", function() { document.execCommand('selectAll',false,null) })
	       .focus();
	  $temp.removeAttr("style");
	  document.execCommand("copy");
	  $temp.remove();
	  $('[data-toggle="tooltip"]').tooltip({ title: "Copied!"});
	  $('[data-toggle="tooltip"]').tooltip('show');
	}
</script>
</head>

<body class="flyctn" style="background:#F9F9F9;">

<div class="container body">
	<div class="main_container">

		<div class="row">
			<div class="col-md-12">
			<?php
			if($action=="data"){
					$sql = "SELECT * FROM imgbank WHERE imgId='". $id ."'";
					$query = query($sql);
					$data = fetch($query);
					$picfile="../assets/imgbank/" . $data['imgDir'] . '/' . $data['imgFile'];
					$size=getimagesize($picfile);
					$widthpic=$size[0];
					$heightpic=$size[1];
					$imgsz = get_headers("https://www.brosispku.com/assets/imgbank/" . $data['imgDir'] . '/' . $data['imgFile'], 1);
					$filesize=round($imgsz["Content-Length"]/1024,2);
					if($filesize<=1023){
						$filesize=$filesize;
						$denomination=" KB";
					}else{
						$filesize=round($filesize/1024,2);
						$denomination=" MB";
					}
					?>
					
					<div style="padding-top:10px">
						<div class="text-center">
						<img src="/assets/imgbank/<?php print $data['imgDir']; ?>/<?php print $data['imgFile']; ?>" width="400px" height="auto" alt="noname"/><br/><br/>
						<p id="url-i" style="display:none;">/assets/imgbank/<?php print $data["imgDir"]; ?>/<?php print $data['imgFile']; ?></p>
						<span id="demo" style="display:none;"><img src="/assets/imgbank/<?php print $data['imgDir']; ?>/<?php print $data['imgFile']; ?>" alt="noname"/></span>
						<p class="small-info">
							<b><?php print _("img_view_pic"); ?></b>: <?php print $data['imgFile']; ?> | 
							<b><?php print _("img_view_imagesize"); ?></b>: <?php print $widthpic . " x " . $heightpic . " px"; ?> |
							<b><?php print _("file_manager_view_filesize"); ?></b>: <?php print $filesize.$denomination; ?>
						</p>
						<p class="small-desc">
							Klick <button class="btn btn-primary" data-toggle="tooltip" class="btn btn-primary" onclick="copy('#demo')">Copy Gambar</button> kemudian paste (tekan CTRL+V) pada kolom isi.
						</p>
						<p><button id="copyme" data-toggle="tooltip1" class="btn btn-default" onclick="copyToClipboard('#url-i')">/assets/imgbank/<?php print $data['imgDir']; ?>/<?php print $data['imgFile']; ?></button></p>
						</div>
					</div>
					<?php
			}
			?>


			</div>
		</div>
	</div>
</div>
</body>
</html>