<?php
	$page=23;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

	#check language license
	if($lang and !in_array($lang, $lc_lang) and ($action=='adden' or $action=='editen')){
		header('location:video.php');
		exit;
	}

	if ($form=="submit"){
		if ($action=="addvid" and $uac_add){
			if (empty($name) or empty ($link) or empty($category)){
				$error=errorlist(2);
			}

			//get next videgory id
			$vidid = nextid("vidId","video");

			//get permalink
			$permalink=permalink($name,$vidid);

			//insert data
			if (!$error){
				//get the next sorting value
				$sortid= nextsort("video","catId","$category","vidSort");

				//insert to table news_vid
				$sql="INSERT INTO video VALUES ($vidid, $category,'$name', '$permalink', '$desc', '$link', 0, $sortid)";
				$query=query($sql);
			}
		}elseif ($action=="editvid" and $uac_edit){
			if (empty($name) or empty($link)){
				$error=errorlist(2);
			}

			//get permalink
			$permalink=permalink($name,$vidid);
			if (!$error){
				//update data
				$sql="UPDATE video SET vidName='$name', catId=$category, vidPermalink='$permalink', vidLink='$link', vidDesc='$desc' WHERE vidId=" . $vidid;
				$query=query($sql);
			}
		}elseif ($action=="delvid" and $uac_delete){
			$sql="DELETE FROM video WHERE vidId='$vidid'";
			$query=query($sql);

			if($query){
				$sql="DELETE FROM video_lang WHERE vidId='$vidid'";
				$query=query($sql);
			}
		}elseif ($action=="updatesort" and $uac_edit){
			foreach ($s as $id=>$sortnum){
				$sql="UPDATE video SET vidSort=$sortnum WHERE vidId='" . $id . "'";
				$query=query($sql);
			}
		}elseif($action=="adden" and $uac_add){
			if (empty($name) or empty($link)){
				$error=errorlist(2);
			}else{
				$exists = countdata('video',"vidId='$vidid'");
				if($exists){
					$sql="INSERT INTO video_lang VALUES ($vidid, '$name', '$desc', '$link','$lang')";
					$query=query($sql);
				}
			}
		}elseif($action=="editen" and $uac_edit){
			if (empty($name) or empty($link)){
				$error=errorlist(2);
			}else{
				$sql="UPDATE video_lang SET vidName='$name',vidLink='$link', vidDesc='$desc' WHERE vidId='$vidid' AND lang='$lang'";
				$query=query($sql);
			}
		}

		//check whether query was successful
		if (!$query){
			$error=errorlist(3);
		}

		if ($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:video.php?cid=$cid");
			}else{
				print "ok";
			}
		}
		exit;
	}

	if ($cid){
		$sql="SELECT * FROM video_cat WHERE catId='$cid'";
		$query=query($sql);
		$data=fetch($query);
		$catname=$data['catName'];
		$cpid=$data['catParentId'];

		$title=$catname;

		if ($cpid>0){
			$sql="SELECT * FROM video_cat WHERE catId='$cpid'";
			$query=query($sql);
			$data=fetch($query);
			$catparentname=$data['catName'];

			$title=$catparentname . " - " . $title;
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _('video_pagetitle'); ?> <?php print ($title)?" - " . $title:""; ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">


<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce_simple.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('#result').hide();
	validate('#result','#add','video.php');
	validate('#result','#edit','video.php');
	$('.fancybox').fancybox({
		'arrows': false
	});
	$('.fancybox-frame').fancybox({
		'type': 'iframe'
	});
});

</script>
</head>

<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('video_pagetitle'); ?></h3>
			                <p><?php print _('video_pagedesc'); ?></p>
			            </div>
		            </div>
	        	</div>
        	</div>

        	<?php if ($uac_add){ ?>
        	<div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                	<div class="pull-right">
                		<a href="video.php?action=addvid" class="btn btn-primary"><i class="fa fa-plus"></i> <?php print _('cms_addbuttonvideo'); ?></a>
                	</div>
                </div>
        	</div>
        	<?php } ?>

        	<!-- THE CONTENT OF PAGE HERE -->
        	<div class="row">
	        	<div class="col-md-12">
	        		<?php if ($action==""){ ?>
	        		<form action="video.php?action=updatesort&amp;form=submit&amp;cid=<?php print $cid; ?>" method="post" enctype="multipart/form-data" name="sort" id="sort">
	        			<div class="block-break">
	        				<?php if($uac_edit){ ?>
                            <button type="submit" name="update" class="btn btn-info"><i class="fa fa-refresh"></i> <?php print _('video_updatelist'); ?></button>
	        				<?php } ?>
		        			<?php if ($catname){ ?>
					        <strong><a href="videocat.php"><?php print _('video_category'); ?></a>:</strong> <?php print ($catparentname)?"<a href=\"video.php?cid=$cpid\">" . $catparentname . "</a> - ":""; ?> <a href="video.php?cid=<?php print $cid; ?>"><?php print $catname; ?></a>
					        <?php } ?>
				        </div>

	        			<div class="table-responsive">
	        				<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
	        					<thead>
	        						<tr>
										<th width="70" class="text-center">No</th>
										<th width="150" class="text-center"><?php print _('video_category'); ?></th>
										<th><?php print _('video_titlevideo'); ?></th>
										<?php if(count($lc_lang)>1){ ?><th style="width:40px;" class="text-center"><i rel="tooltip" title="<?php echo _('multi_language_view_language'); ?>" class="fa fa-language"></i></th><?php }?>
										<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
										<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
									</tr>
	        					</thead>
	        					<tbody>
	        					<?php
								  	if(empty($pg)){
								  		$pg=1;
								  	}

									$numPerPage=20;
									$offset=($pg-1)*$numPerPage;
									$num=1;
									$num=$num+(($pg-1)*$numPerPage);
									if ($cid){
										$sql="SELECT * FROM video v, video_cat c WHERE v.catId=c.catId and v.catId='$cid' ORDER BY v.catId, v.vidSort ASC";
									}else{
										$sql="SELECT * FROM video v, video_cat c WHERE v.catId=c.catId ORDER BY v.catId, v.vidSort ASC";
									}

									$query=query($sql);
									$numofdata=rows($query);
									$sql.=' LIMIT '.$offset.','.$numPerPage;
									$query=query($sql);
									if($numofdata<1){
										?>
										<tr>
											<td colspan="6" class="asterik"><div class="text-center"><br /><?php print _('video_videonotavailable'); ?><br /><br /></div></td>
										</tr>
									<?php
									}else{
										while ($data=fetch($query)){
											?>
											<tr>
												<td class="text-center">
												<?php if($uac_edit){ ?>
													<input type="text" size="3" name="s[<?php print $data['vidId']; ?>]" value="<?php print $data['vidSort']; ?>" class="form-control text-center" />
												<?php }else{ print $num;} ?>
												</td>
												<td class="text-center"><?php print $data['catName']; ?></td>
												<td>
													<a href="#mov<?php print $data['vidId']; ?>" class="fancybox"><strong><?php print $data['vidName']; ?></strong></a>
													<div id="mov<?php print $data['vidId']; ?>" style="display: none;">
														<iframe src="http://www.youtube.com/embed/<?php print getYoutubeId($data['vidLink']) ; ?>" width="560" height="315"></iframe>
						                                <?php print empty($data['vidDesc']) ? "":"<hr/>".$data['vidDesc']; ?>
													</div>
												</td>
												<?php
												if(count($lc_lang)>1){
													?>
													<td class="text-center">
														<?php
														foreach($lc_lang as $key=>$value){
															$exist=countdata("video_lang","vidId='".$data['vidId']."' and lang='$value'");
															if($value==$lc_lang_default) continue;
															if($exist and $uac_edit){
																?><a href="video.php?action=editen&amp;lang=<?php print $value;?>&vidid=<?php print $data['vidId']; ?>" rel="tooltip" title="<?php print _('video_editvers'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-edit.png" alt="<?php print $value;?>"  border="0" /></a><?php
															}
															elseif($uac_add){
																?><a href="video.php?action=adden&amp;lang=<?php print $value;?>&vidid=<?php print $data['vidId']; ?>" rel="tooltip" title="<?php print _('video_addvers'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-add.png" alt="<?php print $value;?>" border="0" /></a><?php
															}
														}?>
													</td>
													<?php
												} ?>
												<td class="text-center"><?php if ($uac_edit){ ?><a href="video.php?action=editvid&amp;vidid=<?php print $data['vidId']; ?>"><i title="<?php print _('cms_edit'); ?>: <?php print stripquote($data['vidName']); ?>" rel="tooltip" class="fa fa-edit"></i></a><?php } ?></td>
												<td class="text-center"><?php if ($uac_delete){ ?><a class="delete" href="video.php?form=submit&action=delvid&vidid=<?php print $data['vidId']; ?>" title="<?php print stripquote($data['vidName']); ?>"><i rel="tooltip" title="<?php print _('cms_delete'); ?>: <?php print stripquote($data['vidName']); ?>" class="fa fa-trash"></i></a><?php } ?></td>
											  </tr>
											<?php
											$num++;
										}
									}
									?>
	        					</tbody>
	        				</table>
	        			</div>
	        			<?php 
	        			$options['total']=$numofdata;
						$options['filename']='video.php';
						$options['qualifier']='video.php';
						$options['pg']=$pg;
						$options['numPerPage']=$numPerPage;
						$options['style']=1;
						$options['addquery']="cid=$cid";
						paging($options);
						?>
					</form>
					<?php } elseif($action=="addvid" and $uac_add){ ?>
						<a href="video.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
						<h4><?php print _('cms_mainaddbuttonvideo'); ?></h4>
						<hr/>
						<form action="video.php?action=addvid&amp;form=submit" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">
							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="category"><?php print _('video_category'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-2 col-sm-6 col-xs-12">
		                        	<select name="category" id="category" class="form-control" required>
						            	<?php
											$sqlx="SELECT * FROM video_cat WHERE catParentId=0 ORDER BY catSort";
											$queryx=query($sqlx);
											while ($datax=fetch($queryx)){
												$datax=formoutput($datax);
										?>
						            	<option value="<?php print $datax['catId']; ?>"><?php print $datax['catName']; ?></option>
						            	<?php
												$sqlxx="SELECT * FROM video_cat WHERE catParentId='" . $datax['catId'] . "' ORDER BY catSort";
												$queryxx=query($sqlxx);
												while ($dataxx=fetch($queryxx)){
													$dataxx=formoutput($dataxx);
										?>
			                            <option value="<?php print $dataxx['catId']; ?>">&nbsp; &nbsp; - <?php print $dataxx['catName']; ?></option>
			                            <?php
												}
											}
										?>
						            </select>
		                        </div>
		                	</div>
		                	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _('video_title'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="name" type="text" id="name" size="50" required class="form-control videoname" />
		                        </div>
		                	</div>
		                	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="link"><?php print _('video_linkyoutube'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="link" type="text" id="link" size="50" required class="form-control" />
		                        </div>
		                	</div>
		                	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="desc"><?php print _('video_description'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="desc" cols="80" rows="5" id="desc" class="tinymce-simple"></textarea>
		                        </div>
		                	</div>

		                	<hr/>

		                	<div class="form-group">
		                        <div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
		                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='video.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                        </div>
	                    	</div>

	                    	<br/><br/><br/>
						</form>
					<?php } elseif ($action=="editvid" and $uac_edit){
							$sql="SELECT * FROM video WHERE vidId=" . $vidid;
							$query=query($sql);
							$data=fetch($query);
							$data=formoutput($data);
				  	?>
				  		<a href="video.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
						<h4><?php print _('video_editvideo'); ?></h4>
						<hr/>

						<form action="video.php?action=editvid&amp;form=submit&amp;vidid=<?php print $vidid; ?>" method="post" enctype="multipart/form-data" name="edit" id="edit" class="form-horizontal form-label-left">
							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="category"><?php print _('video_category'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-2 col-sm-6 col-xs-12">
		                        	<select name="category" id="category" class="form-control" required>
						            	<?php
											$sqlx="SELECT * FROM video_cat WHERE catParentId=0 ORDER BY catSort";
											$queryx=query($sqlx);
											while ($datax=fetch($queryx)){
												$datax=formoutput($datax);
										?>
						            	<option value="<?php print $datax['catId']; ?>"<?php print ($datax['catId']==$data['catId'])?" selected=\"selected\"":""; ?>><?php print $datax['catName']; ?></option>
						            	<?php
												$sqlxx="SELECT * FROM video_cat WHERE catParentId='" . $datax['catId'] . "' ORDER BY catSort";
												$queryxx=query($sqlxx);
												while ($dataxx=fetch($queryxx)){
													$dataxx=formoutput($dataxx);
										?>
			                            <option value="<?php print $dataxx['catId']; ?>"<?php print ($dataxx['catId']==$data['catId'])?" selected=\"selected\"":""; ?>>&nbsp; &nbsp; - <?php print $dataxx['catName']; ?></option>
			                            <?php
												}
											}
										?>
						            </select>
		                        </div>
		                	</div>

		                	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _('video_title'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="name" type="text" id="name" size="50" value="<?php print $data['vidName']; ?>" class="form-control" required />
		                        </div>
		                	</div>

		                	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="link"><?php print _('video_linkyoutube'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="link" type="text" id="link" size="50" value="<?php print $data['vidLink']; ?>" class="form-control" required />
		                        </div>
		                	</div>

		                	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="desc"><?php print _('video_description'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="desc" cols="80" rows="5" id="desc" class="tinymce-simple"><?php print $data['vidDesc']; ?></textarea>
		                        </div>
		                	</div>

		                	<hr/>

		                	<div class="form-group">
		                        <div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_updatebutton'); ?>"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
		                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='video.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                        </div>
	                    	</div>

	                    	<br/><br/><br/>
						</form>
					<?php }elseif ($action=="adden" and $uac_add){
						$sql="SELECT vidName, vidLink FROM video WHERE vidId='$vidid'";
						$query=query($sql);
						$data=fetch($query);
						$data=formoutput($data);
						$videoname=$data['vidName'];
						$videolink=$data['vidLink'];
					?>
						<a href="video.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
						<h4><?php print _('video_addvideovers'); ?> <?php print localename($lang);?> &raquo; <?php print $videoname; ?></h4>
						<hr/>

						<form action="video.php?action=adden&amp;form=submit&amp;lang=<?php print $lang;?>&amp;vidid=<?php print $vidid; ?>" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">
							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _('video_title'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="name" type="text" id="name" size="50" required class="form-control videoname" />
		                        </div>
		                	</div>
		                	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="link"><?php print _('video_linkyoutube'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="link" type="text" id="link" value="<?php print $videolink;?>" size="50" required class="form-control" />
		                        </div>
		                	</div>
		                	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="desc"><?php print _('video_description'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="desc" cols="80" rows="5" id="desc" class="tinymce-simple"></textarea>
		                        </div>
		                	</div>

		                	<hr/>

		                	<div class="form-group">
		                        <div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
		                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='video.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                        </div>
	                    	</div>

	                    	<br/><br/><br/>
						</form>
					<?php }elseif ($action=="editen" and $uac_edit){
						$sql="SELECT vidName FROM video WHERE vidId='$vidid'";
						$query=query($sql);
						$data=fetch($query);
						$data=formoutput($data);
						$videoname=$data['vidName'];

						$sql="SELECT * FROM video_lang WHERE vidId='$vidid' AND lang='$lang'";
						$query=query($sql);
						$data=fetch($query);
						$data=formoutput($data);
					?>
						<a href="video.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
						<h4><?php print _('video_editvideovers'); ?> <?php print localename($lang);?> &raquo; <?php print $videoname; ?></h4>
						<hr/>
						<form action="video.php?action=editen&amp;form=submit&amp;lang=<?php print $lang;?>&amp;vidid=<?php print $vidid; ?>" method="post" enctype="multipart/form-data" name="edit" id="edit" class="form-horizontal form-label-left">
							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _('video_title'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="name" type="text" id="name" size="50" value="<?php print $data['vidName']; ?>" required class="form-control" />
		                        </div>
		                	</div>

		                	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="link"><?php print _('video_linkyoutube'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="link" type="text" id="link" size="50" value="<?php print $data['vidLink'];?>" required class="form-control" />
		                        </div>
		                	</div>

		                	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="desc"><?php print _('video_description'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="desc" cols="80" rows="5" id="desc" class="tinymce-simple"><?php print $data['vidDesc']; ?></textarea>
		                        </div>
		                	</div>

		                	<hr/>

		                	<div class="form-group">
		                        <div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_updatebutton'); ?>"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
		                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='video.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                        </div>
	                    	</div>
						</form>
					<?php } ?>
	        	</div>
	       	</div>
	       	<!-- END THE CONTENT OF PAGE HERE -->


		</div>
		<!-- END THE CONTENT OF PAGE HERE -->

		<?php include("com/com-footer.php"); ?>

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>
