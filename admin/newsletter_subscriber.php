<?php
	$page=31;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

	if($form=="submit"){
		#do this, if the user action is edit or changes status
		if(($action == "edit" or $action == "changestatus") and $uac_edit){
			$sql = "SELECT COUNT(subId) AS is_exist, subEmail, subIsMember, subActive
					FROM newsletter_subscriber
					WHERE subId='$id'";
			$query = query($sql);
			$data = fetch($query);

			#get subscriber name from member table if it exist
			//$subscriber_name = getval('mName','member','mEmail',$dummy_email);

			#show error if the user try to change  the radio option values when *edit action*
			if($action == "edit" and !in_array($status,array('n','y'))){
				$error = errorlist(83);
			}

			if(!$error){
				if(getconfig('SENDY_NEWSLETTER_NONCUSTOMER_ID')){
					#is the email changes? if yes, unsubscribe old email from sendy
					if($action == "edit" and $dummy_email != $email){
						#unsubscribe old email $status
						$sendyparams['email']= $data['subEmail'];
						$sendyparams['list'] = $data['subIsMember'] == 'n' ? getconfig('SENDY_NEWSLETTER_NONCUSTOMER_ID') : getconfig('SENDY_NEWSLETTER_CUSTOMER_ID');
						$sendyparams['boolean'] = true; //to allow the email send with plaintext mode
						$sendyto = sendyto('unsubscribe',$sendyparams);
						unset($sendyparams);
					}
				 
					if($status == 'y'){
						#Subscribe this email to sendy newsletter *Non-customer* list.
						#If the email already exist in sendy, its will be force to change the status unsubscribe to subscribe.
						$sendyparams['name'] = !empty($subscriber_name) ? $subscriber_name : null; //name is optional
						$sendyparams['email'] = $action == 'edit' ? $email : $data['subEmail'];
						$sendyparams['list'] = $data['subIsMember'] == 'n' ? getconfig('SENDY_NEWSLETTER_NONCUSTOMER_ID') : getconfig('SENDY_NEWSLETTER_CUSTOMER_ID');
						$sendyparams['boolean'] = true; //to allow the email send with plaintext mode
						$sendyto = sendyto('subscribe', $sendyparams);
					}elseif($status == 'n'){
						#unsubscribe old email
						$sendyparams['email']= $action == 'edit' ? $email : $data['subEmail'];
						$sendyparams['list'] = $data['subIsMember'] == 'n' ? getconfig('SENDY_NEWSLETTER_NONCUSTOMER_ID') : getconfig('SENDY_NEWSLETTER_CUSTOMER_ID');
						$sendyparams['boolean'] = true; //to allow the email send with plaintext mode
						$sendyto = sendyto('unsubscribe',$sendyparams);
					}
				}
			}	
		}

		if($action == "edit" and $uac_edit){
			#show error if the user try to change  the radio option values
			if(!in_array($status,array('n','y'))){
				$error = errorlist(83);
			}

			if(!$error){
				$sql="UPDATE newsletter_subscriber SET subEmail='$email' ,subActive='$status' WHERE subId='". (int)$id ."'";
				$query=query($sql);
			}
		}elseif($action=="del" and $uac_delete){
			$sql = "SELECT subEmail, subIsMember, subActive
					FROM newsletter_subscriber
					WHERE subId='$id'";
			$query = query($sql);
			$data = fetch($query);
			
			if(getconfig('SENDY_NEWSLETTER_NONCUSTOMER_ID')){
				$sendyparams['email']= $data['subEmail'];
				$sendyparams['list'] = $data['subIsMember'] == 'n' ? getconfig('SENDY_NEWSLETTER_NONCUSTOMER_ID') : getconfig('SENDY_NEWSLETTER_CUSTOMER_ID');
				$sendyparams['boolean'] = true; //to allow the email send with plaintext mode
				$sendyto = sendyto('unsubscribe',$sendyparams);
			}
			
			$sql="DELETE FROM newsletter_subscriber WHERE subId='". (int)$id ."'";
			$query=query($sql);
		}elseif($action=="changestatus" and $uac_edit){
			$sql="UPDATE newsletter_subscriber SET subActive='$status' WHERE subId='". (int)$id ."'";
			$query=query($sql);
		}

		//check whether query was successful
		if(!$query){
			$error=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:newsletter_subscriber.php");
			}else{
				print "ok";
			}
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?>- CMS - <?php print _('pagecontent_pagetitle'); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>

<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>
<!-- jQuery Tags Input -->
<script type="text/javascript" src="/libs/jquery.tagsinput/src/jquery.tagsinput.js"></script>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('#result').hide();
	$('#keywordseo').tagsInput({
		width: 'auto'
	});
	validate('#result','#edit','newsletter_subscriber.php');
});
</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
    <div class="main_container">
        
        <div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->
        
        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h3><?php print _("newsletter_subscriber_pagetitle"); ?></h3>
                    <p><?php print _("newsletter_subscriber_pagedesc"); ?></p>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                            <?php
                            $sql="SELECT * FROM newsletter_subscriber";
                            $query=query($sql);
                            $rows=rows($query);
                                if($rows>0){
                            ?> 
                            <a href="newsletter_subscriber_to_excel.php" class="btn btn-warning"><i class="fa fa-download"></i> Export to Excel</a>
                            <?php
                                }
                            ?>
                        </div>
                        
                        <div class="col-md-6 col-sm-6 col-xs-12" style="margin-bottom: 10px;display: inline-block;">
                            <div class="pull-right">
                                <form name="search" method="get" class="form-inline">
                                    <div class="form-group">
                                        <select  name="status" class="form-control input-sm">
                                            <option value=""><?php print _("newsletter_view_allstatus"); ?></option>
                                            <option value="y" <?php print $status=="y" ? 'selected="selected"': ''; ?>>Active</option>
                                            <option value="n" <?php print $status=="n" ? 'selected="selected"': ''; ?>>Non Active</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input name="searchtext" type="text" id="searchtext" size="40" value="<?php print $searchtext;?>"   class="form-control input-sm" />
                                    </div>

                                    <button type="submit" name="Submit"  class="btn btn-sm btn-dark"><?php print _("newsletter_subscriber_search"); ?></button>
								</form>
				            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_content"> 
                                 <?php
                                if (empty($action)){
                                ?>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                                      <thead>
                                        <tr>
                                            <th class="text-center" style="width: 3%;"><?php print _("cms_no"); ?></th>
                                            <th class="text-center"><?php print _("newsletter_view_email"); ?></th>
                                            <th class="text-center" style="width: 40px;"><i class="fa fa-cog"></i></th>
                                            <th class="text-center" style="width: 40px;"><i class="fa fa-cog"></i></th>
                                            <th class="text-center" style="width: 40px;"><i class="fa fa-cog"></i></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                          <?php
                                                if (empty($pg)){
                                                    $pg=1;
                                                }

                                                if($searchtext){
                                                    $sqlsearch=" AND subEmail LIKE '%$searchtext%' ";
                                                }

                                                //$ismember ? $sqlsearch.="AND subIsMember='".$ismember."'" : "";
                                                $status ? $sqlsearch.="AND subActive='".$status."'" : "";


                                                $sql="SELECT * FROM newsletter_subscriber WHERE subId IS NOT NULL 	 $sqlsearch ORDER BY  subId DESC";
                                                $query=query($sql);
                                                $numofdata=rows($query);

                                                $num=1;
                                                $numPerPage=20;
                                                $offset=($pg-1)*$numPerPage;
                                                $num=$num+(($pg-1)*$numPerPage);

                                                $sql.=' LIMIT '.$offset.','.$numPerPage;
                                                $query=query($sql);
                                                if ($numofdata<1){
                                            ?>
                                          <tr>
                                              <td colspan="7" class="asterik" align="center"><br /><?php print _("newsletter_subscriber_notavailable"); ?><br /><br /></td>
                                          </tr>
                                          <?php
                                                }else{
                                                    while($data=fetch($query)){
                                          ?>
                                          <tr>
                                              <td class="text-center"><?php print $num; ?></td>
                                              <td><?php print $data['subEmail']; ?></td>
                                              <td class="text-center">
                                                  <?php if ($uac_edit){ 
                                                        if($data['subActive']=='y'){
                                                            print "<a href=\"javascript:if (window.confirm('"._('newsletter_subscriber_suretononactive')." ". $data['subEmail']."?')){ window.location='newsletter_subscriber.php?form=submit&amp;action=changestatus&status=n&amp;id=".$data['subId']."'; };\" title=\""._('newsletter_view_disable').": ". $data['subEmail']."\" data-toggle='tooltip' data-placement='top'><i class='fa fa-check'></i></a>";
                                                        }else{
                                                            print "<a href=\"javascript:if (window.confirm('"._('newsletter_subscriber_suretoactive')." ". $data['subEmail']."?')){ window.location='newsletter_subscriber.php?form=submit&amp;action=changestatus&status=y&amp;id=".$data['subId']."'; };\" title=\""._('newsletter_view_enable').": ". $data['subEmail']."\" data-toggle='tooltip' data-placement='top'><i class='fa fa-minus-circle' title='' style='color:#cc0000;'></i></a>";
                                                        }
                                                    } ?>
                                                 
                                              </td>
                                              <td class="text-center">
                                                  <?php if ($uac_edit){ ?><a href="newsletter_subscriber.php?action=edit&amp;id=<?php print $data['subId']; ?>" data-toggle="tooltip" data-placement="top" ><i class="fa fa-edit"></i></a><?php } ?>
                                              </td>
                                              <td class="text-center">
                                                  <?php if ($uac_delete){ ?><a class="delete" href="newsletter_subscriber.php?form=submit&amp;action=del&amp;id=<?php print $data['subId']; ?>" title="<?php print stripquote($data['subEmail']); ?>" ><i class="fa fa-trash" title="Hapus: <?php print stripquote($data['subEmail']); ?>" data-toggle="tooltip" data-placement="top"></i></a><?php } ?>
                                              </td>
                                          </tr>
                                          <?php
                                                        $num++;
                                                    }
                                                }
                                        ?>
                                      </tbody>
                                    </table>
                                    <?php
                                        $options['total']=$numofdata;
                                        $options['filename']='newsletter_subscriber.php';
                                        $options['qualifier']='newsletter_subscriber.php';
                                        $options['pg']=$pg;
                                        $options['numPerPage']=$numPerPage;
                                        $options['style']=1;
                                        $options['addquery']=TRUE;
                                        paging($options);
                                    ?>
                              </div>
                                <?php
                                }elseif($action=="edit" and $uac_edit){
                                    $sql="SELECT * FROM newsletter_subscriber WHERE subId='$id'";
                                    $query=query($sql);
                                    $data=fetch($query);
                                ?>
                                <a href="newsletter_subscriber.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
                                <h4><?php print _('newsletter_subscriber_edittitle'); ?></h4>
                                <hr/>
                                <form action="newsletter_subscriber.php?form=submit&action=edit&id=<?php print $id; ?>" method="post" name="edit" id="edit" class="form-horizontal form-label-left">
                                    <input type="hidden" name="dummy_email" value="<?php print $data['subEmail'];?>" >
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="email"><?php print _('newsletter_view_email'); ?><span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input name="email" required class="form-control col-md-7 col-xs-12"  type="text" value="<?php print $data['subEmail'];?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="last-name"><?php print _('newsletter_view_status'); ?>
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class="radio-inline"><input type="radio" class="checkradio" name="status" value="y" <?php print $data['subActive']=="y" ? 'checked="checked"':'';?>><?php print _('newsletter_view_active'); ?></label>
                                        <label class="radio-inline"><input type="radio" class="checkradio" name="status" value="n" <?php print $data['subActive']=="n"  ? 'checked="checked"':'';?>><?php print _('newsletter_view_nonactive'); ?></label>
                                      </div>
                                    </div>
                                    <hr/>
                                    <div class="form-group">
                                        <div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <button name="Submit" type="submit" class="btn btn-primary btn-sm" value="Submit"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
                                            <button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='newsletter_subscriber.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
                                            <button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
                                        </div>
                                    </div>
                                </form>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
        	</div>
        </div>
        
        <?php include("com/com-footer.php"); ?>
    </div>
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>