<?php
	$page=12;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config-image.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

	#check language license
	if($lang and !in_array($lang, $lc_lang) and ($action=='adden' or $action=='editen')){
		header('location:changeban.php');
		exit;
	}

	if($form=="submit"){
		//set the restrictions
		$allowedType=array("image/pjpeg","image/jpeg","image/png","image/gif");
		$realExt = array('jpeg','jpg','png','gif');

		$pic=$_FILES['pic'];

		if($action=="add" and $uac_add){
			if(empty($pic['name']) or empty($name)){
				$error=errorlist(2);
			}

			if(empty($url)){
				$url="javascript:;";
			}

			if($showdate){
				list($showD,$showM,$showY)=explode(" ",$showdate);
				$showMs		=convertidtomonth(strtolower(convertmonthtoid($showM)));
				$tshowdate	=mktime(0,0,0,$showMs,$showD,$showY);
			}

			if($hidedate){
				list($hideD,$hideM,$hideY)=explode(" ",$hidedate);
				$hideMs		=convertidtomonth(strtolower(convertmonthtoid($hideM)));
				$thidedate	=mktime(0,0,0,$hideMs,$hideD,$hideY);
			}

			if($tshowdate > $thidedate){
				$error=errorlist(35);
			}

			//make sure it is allowed
			if(in_array($pic['type'], $allowedType)){
				$fileExt = array_keys($allowedType, $pic['type']);
				$fileExt = $realExt[$fileExt[0]];

				if(is_uploaded_file($pic['tmp_name'])){
					//make sure it is really really image
					$img=getimagesize($pic['tmp_name']);
					if(!in_array($pic['type'],$allowedType) or !(is_numeric($img[0]) or is_numeric($img[1]) or $img[0]>0 or $img[1]>0)){
						//make sure it is allowed
						$error.=errorlist(13);
					}
				}
				else{
					$error.=errorlist(27);
				}
			}
			else{
				$error.=errorlist(13);
			}

			//get filename
			$fname = str_replace('.','',$pic['name']).'.'.$fileExt;

			//insert data
			if(!$error){
				$mbanid=nextid("mbanId","mbanner");

				$img=uploadit($fname,$pic['tmp_name'],"mbanner",$mbanid,BANNER_SMALL_WIDTH,BANNER_SMALL_HEIGHT,BANNER_MEDIUM_WIDTH,BANNER_MEDIUM_HEIGHT,BANNER_LARGE_WIDTH,BANNER_LARGE_HEIGHT);
				$dir=$img['dir'];
				$imgfilename=$img['filename'];

				//Is it will use watermark ?
				if($watermark=="y"){
					chdir('../../');
					$imagesrc='../assets/mbanner/' . $dir . '/'.$imgfilename;
					watermark($imagesrc);
				}

				$sql="INSERT INTO mbanner VALUES ($mbanid,'$name','$tshowdate','$thidedate','$mdesc','$imgfilename','$dir','$url',0,$mbanid)";
				$query=query($sql);
			}
		}elseif($action=="edit" and $uac_edit){
			if (empty($url)){
				$url="javascript:;";
			}

			if(empty($name)){
				$error=errorlist(2);
			}

			if($showdate){
				list($showD,$showM,$showY)=explode(" ",$showdate);
				$showMs		=convertidtomonth(strtolower(convertmonthtoid($showM)));
				$tshowdate	=mktime(0,0,0,$showMs,$showD,$showY);
			}

			if($hidedate){
				list($hideD,$hideM,$hideY)=explode(" ",$hidedate);
				$hideMs		=convertidtomonth(strtolower(convertmonthtoid($hideM)));
				$thidedate	=mktime(0,0,0,$hideMs,$hideD,$hideY);
			}

			if($tshowdate > $thidedate){
				$error=errorlist(35);
			}
			if($pic){
				$fileExt = array_keys($allowedType, $pic['type']);
				$fileExt = $realExt[$fileExt[0]];

				if(is_uploaded_file($pic['tmp_name'])){
					//make sure it is really really image
					$img=getimagesize($pic['tmp_name']);
					if(!in_array($pic['type'],$allowedType) or !(is_numeric($img[0]) or is_numeric($img[1]) or $img[0]>0 or $img[1]>0)){
						//make sure it is allowed
						$error.=errorlist(13);
					}
				}
				else{
					$error.=errorlist(27);
				}

				//get filename
				$fname = str_replace('.','',$pic['name']).'.'.$fileExt;
			}

			if(!$error){
				$sql="SELECT mbanFile,mbanDir FROM mbanner WHERE mbanId='".$id."'";
				$query=query($sql);
				$data=fetch($query);
				$xfile=$data['mbanFile'];
				$xdir=$data['mbanDir'];

				$uploadnew = false;
				if(!empty($pic['name'])){
					//delete current file
					unlink("../assets/mbanner/".$xdir."/".$xfile);
					unlink("../assets/mbanner/".$xdir."/".genthumb($xfile,"m"));
					unlink("../assets/mbanner/".$xdir."/".genthumb($xfile,"s"));

					$img=uploadit($fname,$pic['tmp_name'],"mbanner",$mbanid,BANNER_SMALL_WIDTH,BANNER_SMALL_HEIGHT,BANNER_MEDIUM_WIDTH,BANNER_MEDIUM_HEIGHT,BANNER_LARGE_WIDTH,BANNER_LARGE_HEIGHT);
					$dir=$img['dir'];
					$imgfilename=$img['filename'];	

					$xfile=$imgfilename;
					$xdir=$dir;				

					$sqlban=",mbanDir='$dir',mbanFile='$imgfilename'";

					$uploadnew = true;
				}

				//Is it will use watermark ?
				if($watermark=="y"){
					if($uploadnew){ chdir('../../'); }
				 	$imagesrc='../assets/mbanner/' . $xdir . '/'.$xfile;
					watermark($imagesrc);
				}

				//update data
				$sql="UPDATE mbanner SET mbanName='$name',mbanURL='$url',mbanDesc='$mdesc', mbanShowTime='$tshowdate', mbanHideTime='$thidedate'  $sqlban WHERE mbanId='". $id ."'";
				$query=query($sql);
			}
		}elseif($action=="del" and $uac_delete){
			$sql="SELECT mbanFile, mbanDir FROM mbanner WHERE mbanId='" . $id . "'";
			$query=query($sql);
			$data=fetch($query);
			$xfile=$data['mbanFile'];
			$xdir=$data['mbanDir'];

			unlink("../assets/mbanner/".$xdir."/".$xfile);
			unlink("../assets/mbanner/".$xdir."/".genthumb($xfile,"m"));
			unlink("../assets/mbanner/".$xdir."/".genthumb($xfile,"s"));
			rmdir("../assets/mbanner/" . $xdir);

			$sql="DELETE FROM mbanner WHERE mbanId='" . $id . "'";
			$query=query($sql);

			#remove english version
			$sql="DELETE FROM mbanner_lang WHERE mbanId='" . $id . "'";
			$query=query($sql);
		}elseif($action=="updatesort" and $uac_edit){
			foreach ($s as $id=>$sortnum){
				$sql="UPDATE mbanner SET mbanSort='$sortnum' WHERE mbanId=$id";
				$query=query($sql);
			}
		}elseif($action=="adden" and $uac_add){
			//can change: mbanName mbanURL, mbanDesc
			if(empty($name)){
				$error=errorlist(2);
			}else{
				$sql="INSERT INTO mbanner_lang VALUES ($id,'$name','$mdesc','$lang')";
				$query=query($sql);
			}
		}elseif($action=="editen" and $uac_edit){
			if(empty($name)){
				$error=errorlist(2);
			}else{
				$sql="UPDATE mbanner_lang SET mbanName='$name',mbanDesc='$mdesc' WHERE mbanId='$id' AND lang='$lang'";
				$query=query($sql);
			}
		}

		//check whether query was successful
		if(!$query){
			$error .=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}
		else{
			if(!$js){
				header("location:changeban.php");
			}else{
				print "ok";
			}
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _('changeban_pagetitle'); ?></title>
<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- bootstrap-datepicker -->
<link rel="stylesheet" type="text/css" href="/libs/bootstrap-datepicker/css/datepicker3.css" />
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce_simple.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('.fancybox').fancybox({
		'arrows': false
	});

	$('#result').hide();
	validate('#result','#add','changeban.php');
	validate('#result','#edit','changeban.php');
	
	
	$(".datepickerdate").datepicker({
		startDate: '<?php echo date('d') ?>/<?php echo date('m') ?>/<?php echo date('Y') ?>',
		format: 'dd MM yyyy',
		todayHighlight: true,
		toggleActive: true,
		autoclose: true
	});
});
</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('changeban_title'); ?></h3>
			                <p><?php printf (_('changeban_pagedesc'),SITE_NAME); ?></p>
			            </div>
		            </div>
	        	</div>
        	</div>

        	<?php if ($uac_add){ ?>
        	<div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 text-right" style="margin-bottom: 10px;">
                	<a href="changeban.php?action=add" class="btn btn-primary"><?php print _('cms_buttonaddbanner'); ?></a>
                </div>
        	</div>
        	<?php } ?>
        	<div class="row">
	        	<div class="col-md-12">

	                <div class="x_content">
	                	<?php if (empty($action)){ ?>
	                	<form action="changeban.php?form=submit&amp;action=updatesort" method="post" enctype="multipart/form-data" name="sort" id="sort">
	                		<?php if($uac_edit){ ?>
	                		<div class="block-break">
	                			<button type="submit" name="update" value="<?php print _('mainbanner_updatelist'); ?>" class="btn btn-sm btn-info"><i class="fa fa-refresh"></i> <?php print _('mainbanner_updatelist'); ?></button>
	                		</div>
	                		<?php } ?>

	                		<div class="table-responsive">
			        			<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
			        				<thead>
										<tr class="tablehead">
											<th class="text-center" width="75"><?php print _('cms_no'); ?></th>
											<th><?php print _('changeban_banename'); ?></th>
											<th width="400"><?php print _('changeban_url'); ?></th>
											<?php if(count($lc_lang)>1){ ?><th style="width:40px;" class="text-center"><i rel="tooltip" title="<?php echo _('multi_language_view_language'); ?>" class="fa fa-language"></i></th><?php }?>
											<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
											<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
										</tr>
									</thead>
									<tbody>
										<?php
											$sql="SELECT * FROM mbanner ORDER BY mbanSort";
											$query=query($sql);
											$numofrec=rows($query);

											if ($numofrec==0){
										?>
										<tr>
											<td colspan="6"><div class="text-center"><br /><?php print _('changeban_notavailablebane'); ?><br /><br /></div></td>
										</tr>
										<?php } else { 
											$num=1;
											while($data=fetch($query)){
												$data=output($data);
												if ( $data['mbanURL']=="javascript:;"){
													$data['mbanURL']='-';
												} ?>
										<tr>
											<td class="text-center"><?php if($uac_edit){ ?><input type="text" size="3" name="s[<?php print $data['mbanId']; ?>]" value="<?php print $data['mbanSort']; ?>" class="text-center form-control" /> <?php }else{ print $num;} ?></td>
											<td><a class="fancybox" href="<?php print "../assets/mbanner/" . $data['mbanDir'] . "/" . $data['mbanFile']; ?>" rel="facebox"><strong><?php print output($data['mbanName']); ?></strong></a></td>
											<td><?php print ($data['mbanURL']!='-')? '<a href="'.$data['mbanURL'].'" style="font-weight:normal" target="_blank">'.$data['mbanURL'].'</a>':"-"; ?></td>
											<?php
											if(count($lc_lang)>1){
												?>
												<td class="text-center">
													<?php
													foreach($lc_lang as $key=>$value){
														$exist=countdata("mbanner_lang","mbanId='".$data['mbanId']."' and lang='$value'");
														if($value==$lc_lang_default) continue;
														if($exist and $uac_edit){
															?><a href="changeban.php?action=editen&amp;lang=<?php print $value;?>&id=<?php print $data['mbanId']; ?>" rel="tooltip" title="<?php print _('changeban_editvers'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-edit.png" alt="<?php print $value;?>"  border="0" /></a><?php
														}
														elseif($uac_add){
															?><a href="changeban.php?action=adden&amp;lang=<?php print $value;?>&id=<?php print $data['mbanId']; ?>" rel="tooltip" title="<?php print _('changeban_addvers'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-add.png" alt="<?php print $value;?>" border="0" /></a><?php
														}
													}?>
												</td>
												<?php
											}?>
											<td class="text-center">
												<?php if ($uac_edit){ ?><a href="changeban.php?action=edit&amp;id=<?php print $data['mbanId']; ?>" rel="tooltip" title="<?php print _('changeban_edit'); ?>: <?php print stripquote($data['mbanName']); ?>"><i class="fa fa-edit"></i> </a><?php } ?>
											</td>
											<td class="text-center">
												<?php if ($uac_delete){ ?><a href="changeban.php?action=del&amp;form=submit&amp;id=<?php print $data['mbanId']; ?>" title="<?php print stripquote($data['mbanName']); ?>" class="delete"><i title="<?php print _('cms_delete'); ?>: <?php print stripquote($data['mbanName']); ?>" rel="tooltip" class="fa fa-trash-o"></i></a><?php } ?>
											</td>
										</tr>
										<?php
													$num++;
												}
											}
										?>
									</tbody>
			        			</table>
		        			</div>
		        		</form>
		        		<?php } elseif ($action=="add" and $uac_add){ ?>
		        		<a href="changeban.php" class="btn btn-warning btn-sm" style="margin-bottom: 10px;"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

		        		<h4><?php print _('changeban_addmainbanner') ?></h4>
						<hr/>
						<form action="changeban.php?action=add&amp;form=submit" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">

							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="pic"><?php print _('changeban_fileban'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="pic" type="file" id="pic" size="40" required>
		                        	<span class="help-block"><?php print _('changeban_fileban_info'); ?> <?php print BANNER_LARGE_WIDTH . "x" . BANNER_LARGE_HEIGHT; ?> pixel</span>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="name"><?php print _('changeban_banename'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="name" type="text" id="name" size="50" required class="form-control col-md-7 col-xs-12">
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="showdate"><?php print _('changeban_starttimedisplay'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="showdate" id="showdate" type="text" size="30" class="datepickerdate form-control has-feedback-left dob showdate">
		                        	<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
		                        	<span class="help-block"><?php print _('changeban_startdatedisplay_info'); ?></span>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="hidedate"><?php print _('changeban_enddatedisplay'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="hidedate" id="hidedate" type="text" size="30" class="datepickerdate form-control has-feedback-left dob hidedate">
		                        	<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
		                        	<span class="help-block"><?php print _('changeban_enddatedisplay_info'); ?></span>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="news"><?php print _('changeban_infobane'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="mdesc" id="news" class="tinymce-simple"></textarea>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="url"><?php print _('changeban_url'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="url" type="text" id="url" size="40" class="form-control" />
		                        	<span class="help-block"><?php print _('changeban_url_info'); ?><?php print getconfig('SITE_URL'); ?></span>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="watermark">Watermark
		                        </label>
		                        <div class="checkbox col-md-6 col-sm-6 col-xs-12">
	                        		<label>
	                        			<input type="checkbox" name="watermark" id="watermark" value="y" id="watermark"> Ya
	                        		</label>
		                        </div>
	                    	</div>

	                    	<hr/>

	                    	<div class="form-group">
		                        <div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
		                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='changeban.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                        </div>
	                    	</div>

						</form>

		        		<?php } elseif ($action=="edit" and $uac_edit){ 
		        			$sql="SELECT * FROM mbanner WHERE mbanId='$id'";
							$query=query($sql);
							$data=fetch($query);
							$data=formoutput($data);

							if($data['mbanURL']=="javascript:;"){
								$data['mbanURL']='';
							}
							if($data['mbanShowTime']){
								$starttime	=date('d F Y',$data['mbanShowTime']);
							}
							if($data['mbanHideTime']){
								$endtime	=date('d F Y',$data['mbanHideTime']);
							}
		        		?>
		        		<a href="changeban.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

		        		<h4><?php print _('changeban_editmainbane') ?></h4>
						<hr/>

						<form action="changeban.php?action=edit&amp;form=submit&amp;id=<?php print $id; ?>" method="post" enctype="multipart/form-data" name="edit" id="edit" class="form-horizontal form-label-left">
							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="pic"><?php print _('changeban_changefileban'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<img src="../assets/mbanner/<?php print $data['mbanDir']; ?>/<?php print $data['mbanFile']; ?>" /><br/><br/>
		                        	<input name="pic" type="file" id="pic" size="40">
		                        	<span class="help-block"><?php print _('changeban_fileban_info'); ?> <?php print BANNER_LARGE_WIDTH . "x" . BANNER_LARGE_HEIGHT; ?> pixel</span>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="name"><?php print _('changeban_banename'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="name" type="text" value="<?php print $data['mbanName']; ?>" id="name" size="50" required class="form-control col-md-7 col-xs-12">
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="showdate"><?php print _('changeban_starttimedisplay'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="showdate" id="showdate" type="text" size="30" class="datepickerdate form-control has-feedback-left dob showdate" value="<?php print $starttime ?>">
		                        	<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
		                        	<span class="help-block"><?php print _('changeban_startdatedisplay_info'); ?></span>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="hidedate"><?php print _('changeban_enddatedisplay'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="hidedate" id="hidedate" type="text" size="30" class="datepickerdate form-control has-feedback-left dob hidedate" value="<?php print $endtime ?>">
		                        	<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
		                        	<span class="help-block"><?php print _('changeban_enddatedisplay_info'); ?></span>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="news"><?php print _('changeban_infobane'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="mdesc" id="news" class="tinymce-simple"><?php print $data['mbanDesc']; ?></textarea>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="url"><?php print _('changeban_url'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="url" type="text" id="url" size="40" class="form-control" value="<?php print $data['mbanURL']; ?>" />
		                        	<span class="help-block"><?php print _('changeban_url_info'); ?><?php print getconfig('SITE_URL'); ?></span>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="watermark">Watermark
		                        </label>
		                        <div class="checkbox col-md-6 col-sm-6 col-xs-12">
	                        		<label>
	                        			<input type="checkbox" name="watermark" id="watermark" value="y" id="watermark"> Ya
	                        		</label>
		                        </div>
	                    	</div>

	                    	<hr/>

	                    	<div class="form-group">
		                        <div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_updatebutton'); ?>"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
		                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='changeban.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                        </div>
	                    	</div>

						</form>
						<?php }elseif ($action=="adden" and $uac_add){
							$sql="SELECT * FROM mbanner WHERE mbanId='$id'";
							$query=query($sql);
							$data=fetch($query);
							$data=formoutput($data);
							$bannername=$data['mbanName'];

							if($data['mbanURL']=="javascript:;"){
								$data['mbanURL']='';
							}
						?>
						<a href="changeban.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

		        		<h4><?php print _('changeban_addmainbanevers'); ?><?php print localename($lang);?>  &raquo;  <?php print $bannername; ?></h4>
						<hr/>
						<form action="changeban.php?action=adden&amp;form=submit&amp;lang=<?php print $lang;?>&amp;id=<?php print $id; ?>" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">

							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="name"><?php print _('changeban_banename'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="name" type="text" id="name" size="50" required class="form-control col-md-7 col-xs-12">
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="news"><?php print _('changeban_infobane'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="mdesc" id="news" class="tinymce-simple"></textarea>
		                        </div>
	                    	</div>

	                    	<hr/>

	                    	<div class="form-group">
		                        <div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i><?php print _('cms_addbutton'); ?></button>
		                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='changeban.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                        </div>
	                    	</div>

						</form>
						<?php }elseif ($action=="editen" and $uac_edit){
							$sql="SELECT * FROM mbanner WHERE mbanId='$id'";
							$query=query($sql);
							$data=fetch($query);
							$bannername=$data['mbanName'];
							$dir = $data['mbanDir'];
							$pic = $data['mbanFile'];

							$sql="SELECT * FROM mbanner_lang WHERE mbanId='$id' and lang='$lang'";
							$query=query($sql);
							$data=fetch($query);
							$data=formoutput($data);

							if($data['mbanURL']=="javascript:;"){
								$data['mbanURL']='';
							}
						?>
						<a href="changeban.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

		        		<h4><?php print _('changeban_editmainbanevers'); ?><?php print localename($lang);?>  &raquo; <?php print $bannername; ?></h4>
						<hr/>

						<form action="changeban.php?action=editen&amp;form=submit&amp;lang=<?php print $lang;?>&amp;id=<?php print $id; ?>" method="post" enctype="multipart/form-data" name="edit" id="edit" class="form-horizontal form-label-left">

							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="name"><?php print _('changeban_banename'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="name" type="text" id="name" size="50" required class="form-control col-md-7 col-xs-12" value="<?php print $data['mbanName']; ?>">
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="news"><?php print _('changeban_infobane'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="mdesc" id="news" class="tinymce-simple"><?php print $data['mbanDesc']; ?></textarea>
		                        </div>
	                    	</div>

	                    	<hr/>

	                    	<div class="form-group">
		                        <div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_updatebutton'); ?>"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
		                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='changeban.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                        </div>
	                    	</div>

						</form>
	                	<?php } ?>
	                </div>
	                <!-- /.x_content -->
		        </div>
		    </div>

        </div>
        <!-- END THE CONTENT OF PAGE HERE -->

        <?php include("com/com-footer.php"); ?>

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>
