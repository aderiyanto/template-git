<?php
	$page=35;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _('webbot_pagetitle'); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($){
	$('.fancybox').fancybox({
		'arrows': false
	});
	$('.fancybox-frame').fancybox({
		'type': 'iframe'
	});
	
	$('#result').hide();
});
</script>
</head>
<body>


<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('webbot_pagetitle'); ?></h3>
			                <p><?php print _('webbot_pagedesc'); ?></p>
			            </div>
		            </div>
	        	</div>
        	</div>

        	<div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                	<div class="pull-right">
                	<a href="javascript: if (window.confirm('<?php print _('sitelog_windowsconfirm'); ?>?')){ window.location='sitelog.php?form=submit&action=clearlog';};" title="clear log" class="btn btn-primary"><?php print _('sitelog_emptysitelog'); ?></a>
                	</div>
                </div>
        	</div>

        	<div class="row">
	        	<div class="col-md-12">
	        		<?php if ($action==""){ ?>
	        			<div class="row">
	        				<div class="col-md-9 pull-right">
	        					<div class="pull-right">
	        						<form name="search" method="get" class="form-inline">
	        							<div class="form-group">
	        								<select name="status" class="form-control input-sm">
												<option value=""><?php print _('webbot_status'); ?></option>
												<option value="unverified"<?php print $status == 'unverified' ? ' selected="selected"':'';?>><?php print _('webbot_unverified'); ?></option>
												<option value="allow"<?php print $status == 'allow' ? ' selected="selected"':'';?>><?php print _('webbot_allowed'); ?></option>
												<option value="deny"<?php print $status == 'deny' ? ' selected="selected"':'';?>><?php print _('webbot_blocked'); ?></option>
											</select>
	        							</div>
	        							<div class="form-group">
	        								<input name="keyword" type="text" class="form-control input-sm" id="keyword" size="40" value="<?php print $keyword;?>" />
	        							</div>
	        							<button type="submit" name="Submit" value="<?php print _('webbot_find'); ?>" class="btn btn-sm btn-dark"><?php print _('webbot_find'); ?></button>
	        						</form>
	        					</div>
	        				</div>
	        			</div>
	        			<div class="table-responsive" style="margin-top:12px;">
	        				<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
                				<thead>
                					<tr>
                						<th width="20" class="text-center"><?php print _('webbot_numb'); ?></th>
										<th width="120"><?php print _('webbot_ipaddress'); ?></th>
							            <th class="text-center"><?php print _('webbot_isp'); ?></th>
										<th class="text-center"><?php print _('webbot_country'); ?></th>
										<th width="160" class="text-center"><?php print _('webbot_lastvisit'); ?></th>
										<th class="text-center"><?php print _('webbot_hits'); ?></th>
										<!--<td>Perambah</td>-->
										<th width="100" class="text-center"><?php print _('webbot_status'); ?></th>
                					</tr>
                				</thead>
                				<tbody>
                					<?php
									if (empty($pg)){
										$pg=1;
									}
									
									$numPerPage=20;
									$offset=($pg-1)*$numPerPage;
									$num=1+(($pg-1)*$numPerPage);
									
									$whereClause = '';
									if($status){
										$whereClause[] = "wbStatus='$status'";
									}
									
									if($keyword){
										$whereClause[] = "wbIpAddress LIKE '%$keyword%' OR wbDesc LIKE '%$keyword%'";
									}
									
									if($whereClause){
										$whereClause = "WHERE ".implode(' AND ', $whereClause);
									}
									
									$sql="SELECT * FROM webbot $whereClause ORDER BY wbLastLog DESC";
									$query=query($sql);
									$numofdata=rows($query);

									$sql.=' LIMIT '.$offset.','.$numPerPage;
									$query=query($sql);

									if ($numofdata<1){
										?>
										<tr>
											<td colspan="7" class="asterik"><div class="text-center"><br /><?php print _('webbot_datawebbotnotavailable'); ?><br /><br /></div></td>
										</tr>
										<?php
									} else{
										while ($data=fetch($query)){
											if($data['wbStatus'] == 'unverified'){
												$data['wbStatus'] = '<span class="label label-warning">'. _('webbot_unverified').' </span>';
											} elseif($data['wbStatus'] == 'allow'){
												$data['wbStatus'] = '<span class="label label-success">'. _('webbot_diizinkan').' </span>';
											} else {
												$data['wbStatus'] = '<span class="label label-danger">'. _('webbot_dicekal').' </span>';
											}
											
											if ($data['wbDesc']){
												$wbdesc     = json_decode($data['wbDesc'],true);
												$ip_isp     = $wbdesc['isp'];
												$ip_country = $wbdesc['country'];
											}else{
												$ip_isp="";
												$ip_country="";
											}
											
											?>
											<tr style="vertical-align: top">
												<td align="center"><?php print $num;?></td>
												<td><a href="webbot_detail.php?id=<?php print $data['wbId']; ?>" class="fancybox-frame"><?php print $data['wbIpAddress'];?></td>
							                    <td align="center"><?php print $ip_isp ? $ip_isp:'-';?></td>
												<td align="center"><?php print $ip_country ? $ip_country:'-';?></td>
												<td align="center"><?php print $data['wbLastLog'] ? date('d/m/Y H:i:s', $data['wbLastLog']):'-';?></td>
												<td align="center"><?php print number_format($data['wbCount'], 0, ',', '.');?></td>
												<!--<td align="center"><?php print $data['wbAddedMecanism'];?></td>-->
												<td align="center"><?php print $data['wbStatus'];?></td>
											</tr>
											<?php
											$num++;
										}
									}
									?>
                				</tbody>
                			</table>
                		</div>
                		<?php 
                		$options['total'] = $numofdata;
						$options['filename'] = 'webbot.php';
						$options['qualifier'] = 'webbot.php';
						$options['pg'] = $pg;
						$options['numPerPage'] = $numPerPage;
						$options['style'] = 1;
						$options['addquery'] = TRUE;
						paging($options);
						?>
						<br/><br/><br/>
	        		<?php } ?>
	        	</div>
	        </div>
	        

		</div>
        <!-- END THE CONTENT OF PAGE HERE -->


		<?php include("com/com-footer.php"); ?>

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>