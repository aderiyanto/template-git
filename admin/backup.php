<?php
	$page=7;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");
	include("../modz/mainmod-extend.php");

	if($form=='submit'){
		if($action=='dobackup'){
			$fname = getconfig('DB_NAME').'-'.date("d",$now).date("F",$now).date("Y-Hi",$now).'-'.codegen(3);

			$cmd = "php -q \"".$_SERVER['DOCUMENT_ROOT']."/admin/backup_do.php\" \"{$_SERVER['DOCUMENT_ROOT']}\" \"".($_SERVER['MYSQL_HOME'] ? $_SERVER['MYSQL_HOME']:'unknown')."\" \"$fname\"";
			cli($cmd);
			print $fname;
		}
		elseif($action=='checkbackup'){
			if(file_exists("../assets/backup/{$fname}.wb")){
				$back_var = _("backup_success");
				$output = array(
					'msgid' => 'ok',
					'msg' => $back_var. '<script>window.location.href = "backup.php?form=submit&action=download&fname='.$fname.'";</script>'
				);
			}
			else{
				$output = array(
					'msgid' => 'wait',
					'msg' => '<span class="fa fa-spinner fa-spin"></span> '._("backup_prepare")
				);
			}
			print json_encode($output);
		}
		elseif($action=='download'){
			if($fname){
				/*
				header("Content-Disposition: attachment; filename={$fname}.zip");
				header("Content-Length: " . filesize("../assets/backup/{$fname}.zip"));
				header("Content-Type: application/octet-stream");
				$read = readfile("../assets/backup/{$fname}.zip");
				*/

				header("Content-Type: application/zip");
        		header("Content-Transfer-Encoding: Binary");
				header("Content-Disposition: attachment; filename={$fname}.zip");
				header("Content-Length: " . filesize("../assets/backup/{$fname}.zip"));
				$read = readfile("../assets/backup/{$fname}.zip");
				
				if($read and $fname){
					$status = true;
				} else {
					$status = false;
				}
			} else {
				$status = false;
			}

			$nextid=nextid("backactId","backup_activity");

			if($status){
				$stat = 'success';
			} else {
				$stat = 'failed';
			}

			$count = countdata("backup_activity","backactFileName='{$fname}'");
			if($count==0){
				$sql = "INSERT INTO backup_activity VALUES('{$nextid}', '{$now}', '{$stat}','{$fname}')";
				$query = query($sql);
			}
		}
		elseif($action=='download_only'){
			if($fname){
				if(file_exists("../assets/backup/{$fname}.zip")){
					header("Content-Type: application/zip");
	        		header("Content-Transfer-Encoding: Binary");
					header("Content-Disposition: attachment; filename={$fname}.zip");
					header("Content-Length: " . filesize("../assets/backup/{$fname}.zip"));
					$read = readfile("../assets/backup/{$fname}.zip");
				}
			}
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _('backup_pagetitle'); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce_simple.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>


<script type="text/javascript"> 
jQuery(document).ready(function($){
	$('.fancybox').fancybox({
		'arrows': false
	});
	$('.btnbackup').click(function(e){
		e.preventDefault();
		$(this).addClass('disabled');
		
		$.get("backup.php?form=submit&action=dobackup",function(data){
			checkbackup(data);
		});
	});
});

function checkbackup(fname){
	$.get("backup.php?form=submit&action=checkbackup&fname="+fname,function(data){
		data = JSON.parse(data);
		$('#checker').html('<div class="alert alert-info"><p><span class="fa fa-info-circle"></span> '+data.msg+'</p></div>');
		if(data.msgid=='ok'){
			reloadwithajax();
		} else {
			setTimeout(checkbackup(fname),2000);
		}
	});
}

function reloadwithajax(){
	$.ajax({
		url: "backup_load.php",
		beforeSend: function () {
			$('#datahistoryupdate').html('<br/><br/><br/><center><p><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></p></center>');
		},
		success: function(data) {
			$('#datahistoryupdate').html(data).slideDown('fast');
		}
	});
}
</script>
<style type="text/css">
.btnbackupDisabled{
	opacity:0.4;
	filter:alpha(opacity=40);
}

#checker{
	font-size:14px;
}
</style>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('backup_pagetitle'); ?></h3>
			                <p><?php print _('backup_pagedesc'); ?> <?php print SITE_NAME; ?></p>
			            </div>
		            </div>
	        	</div>
        	</div>

        	<!-- THE CONTENT OF PAGE HERE -->
        	<div class="row">
	        	<div class="col-md-12">
	        		<div class="backupbox">
						<?php
						if(function_exists('exec')){
							?>
							<a class="btn btn-lg btn-primary btnbackup" href="javascript:void();"><i class="fa fa-save"></i> Backup Data Now</a><br/>
							<?php
						}
						else{
							?><div class="alert alert-warning"><p><span class="fa fa-warning"></span> <?php print _("backup_descinfo"); ?></div><?php
						} ?>
					</div>
					<div id="checker" style="margin-top:10px;"></div>
					<hr/>
					<h4><?php print _("backup_backuphistory"); ?></h4>
					<div id="datahistoryupdate">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
								<thead>
	                				<tr>
	                					<th width="35" class="text-center"><?php echo _('cms_no'); ?></th>
	                					<th width="180" class="text-center"><?php echo _('backup_activitydate'); ?></th>
	                					<th><?php echo _('backup_filebackup'); ?></th>
	                					<th width="130" class="text-center"></th>
	                				</tr>
	                			</thead>
	                			<tbody>
	                				<?php
	                				if(empty($pg)){
										$pg=1;
									}

									$numPerPage=20;
									$offset=($pg-1)*$numPerPage;
									$num=1;
									$num=$num+(($pg-1)*$numPerPage);

	                				$sql="SELECT * FROM backup_activity ORDER BY backactId DESC";
									$query=query($sql);
									$numofdata=rows($query);

									if ($numofdata<1){
									?>
									<tr>
										<td colspan="9"><div class="text-center"><br />
										<?php print _('backup_datablognotavailable'); ?><br />
										<br /></td>
									</tr>								
									<?php 
									} else {
										$sql.=' LIMIT '.$offset.','.$numPerPage;
										$query=query($sql);
										while ($data=fetch($query)){
	                				?>
		                				<tr>
		                					<td class="text-center"><?php echo $num; ?></td>
		                					<td class="text-center"><?php print dateformat($data['backactDate'],'time'); ?></td>
		                					<td>
		                						<a href="backup.php?form=submit&action=download_only&fname=<?php echo $data['backactFileName']; ?>"><?php echo $data['backactFileName'].".zip"; ?></a>
		                					</td>
		                					<td class="text-center">
		                					<?php 
		                						if($data['backactStatus']=='success'){
		                							echo '<a href="backup.php?form=submit&action=download_only&fname='.$data['backactFileName'].'" class="btn btn-primary btn-sm"><i class="fa fa-download"></i> Download</a>';
		                						} elseif($data['backactStatus']=='failed'){
		                							echo '<span class="label label-danger">Backup Failed</span>';
		                						}
		                					?>
		                					</td>
		                				</tr>
	                				<?php $num++; }
	                				}
	                				?>
	                			</tbody>
							</table>
						</div>
						<?php
							$options['total']=$numofdata;
							$options['filename']='backup.php';
							$options['qualifier']='backup.php';
							$options['pg']=$pg;
							$options['numPerPage']=$numPerPage;
							$options['style']=1;
							$options['addquery']=TRUE;
							paging($options);
						?>
					</div>
	        	</div>
	        </div>


		</div>
		<!-- END THE CONTENT OF PAGE HERE -->

		<?php include("com/com-footer.php"); ?>

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>