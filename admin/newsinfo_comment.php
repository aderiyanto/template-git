<?php
	$page=34;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/mainmod-extend.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

	if($form == "submit"){
		if($action=="del" and $uac_delete){
			$sql="DELETE FROM newsinfo_comment WHERE cId='". (int)$id ."'";
			$query=query($sql);
		}elseif($action == 'delspam' and $uac_delete){
			$sql = "DELETE FROM newsinfo_comment WHERE cSpam='y'";
			$query=query($sql);
		}elseif($action=="show" and $uac_edit){
			$sql="UPDATE newsinfo_comment SET cStatus='y' WHERE cId='". (int)$id ."'";
			$query=query($sql);
		}elseif($action=="hide" and $uac_edit){
			$sql="UPDATE newsinfo_comment SET cStatus='n' WHERE cId='". (int)$id ."'";
			$query=query($sql);
		}

		//check whether query was successful
		if(!$query){
			$error=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				if($searchtext){
					$uri="newsinfo_comment.php?searchtext=$searchtext&pg=$pg";
				}else{
					$uri="newsinfo_comment.php?pg=$pg";
				}
				header("location:".$uri);
			}else{
				print "ok";
			}
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _('newsinfocomment_pagetitle'); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce_simple.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('.fancybox').fancybox({
		'arrows': false
	});
	$('.fancybox-frame').fancybox({
		'type': 'iframe'
	});
	$('#result').hide();
	validate('#result','#edit','newsinfo_comment.php');

});
</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('newsinfocomment_title'); ?></h3>
			                <p><?php print _('newsinfocomment_pagedesc'); ?></p>
			            </div>
		            </div>
	        	</div>
        	</div>

        	<!-- THE CONTENT OF PAGE HERE -->
        	<div class="row">
	        	<div class="col-md-12">
	        		<?php if ($action==""){
						if($newsid){
							$sql = "SELECT newsTitle FROM newsinfo WHERE newsId='$newsid'";
							$query = query($sql);
							$data = fetch($query);
							?>
							<div class="block-break">
								<p><?php print _('newsinfocomment_allcommentfromnews'); ?>: <b><?php print $data['newsTitle'];?></b>
									<a href="newsinfo_comment.php"><i rel="tooltip" title="<?php print _('newsinfocomment_displayallcomment'); ?>" class="fa fa-times-circle"></i></a>
								</p>
							</div>
							<?php
						} 

						$numOfSpam = countdata("newsinfo_comment", "cSpam='y'");
						if($numOfSpam){
							?><div class="block-break">
								<a style="padding: 3px; background-color: #FC0; border: 1px solid #C00" href="javascript:if (window.confirm('<?php print _('newsinfocomment_areyousure'); ?>?')){ window.location='newsinfo_comment.php?form=submit&amp;action=delspam';};"><?php print _('newsinfocomment_deletecommentspam'); ?> (<?php print number_format($numOfSpam, 0, ',', '.');?>)</a>
							</div><?php
						}
					?>
						<div class="row">
							<div class="col-md-12">
								<div class="pull-right">
									<form name="search" method="get" class="form-inline">
										<?php if($newsid){ ?>
										<input type="hidden" name="newsid" value="<?php print $newsid;?>" />
										<?php } ?>
										<div class="form-group">
											<p class="form-control-static"><?php print _('newsinfocomment_display'); ?>: </p>
										</div>
										<div class="form-group">
											<select name="fspam" class="form-control input-sm">
												<option value=""><?php print _('newsinfocomment_allcomment'); ?></option>
												<option value="y"<?php print ($fspam == 'y' ? ' selected="selected"':'');?>><?php print _('newsinfocomment_spam1'); ?></option>
												<option value="n"<?php print ($fspam == 'n' ? ' selected="selected"':'');?>><?php print _('newsinfocomment_spam2'); ?></option>
											</select>
										</div>
										<div class="form-group">
											<p class="form-control-static">Status: </p>
										</div>
										<div class="form-group">
											<select name="fstatus" class="form-control input-sm">
												<option value=""><?php print _('newsinfocomment_allstat'); ?></option>
												<option value="y"<?php print ($fstatus == 'y' ? ' selected="selected"':'');?>><?php print _('newsinfocomment_after'); ?></option>
												<option value="n"<?php print ($fstatus == 'n' ? ' selected="selected"':'');?>><?php print _('newsinfocomment_before'); ?></option>
											</select>
										</div>
										<div class="form-group">
											<input type="text" class="form-control input-sm" name="searchtext" size="40" value="<?php print $searchtext;?>" />
										</div>
										<button type="submit" name="Submit" value="<?php print _('newsinfocomment_discover'); ?>" class="btn btn-sm btn-dark"><?php print _('newsinfocomment_discover'); ?></button>
									</form>
								</div>
							</div>
						</div>
						<div class="table-responsive" style="margin-top:12px;">
							<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
								<thead>
									<tr>
										<th width="35">No</th>
										<th width="350">Post</th>
										<th><?php print _('newsinfocomment_comment'); ?></th>
										<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
										<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
										<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
									</tr>
								</thead>
								<tbody>
								<?php
								if (empty($pg)){
									$pg=1;
								}

								if($searchtext){
									//cahnge space between word to %
									$searchtext=str_replace(" ", "%", trim($searchtext));
									$whereClause[] = "(a.cName like '%$searchtext%' or a.cEmail like '%$searchtext%' or a.cContent like '%$searchtext%')";
								}

								if($newsid){
									$whereClause[] = "a.newsId='$newsid'";
								}

								if($fspam){
									$whereClause[] = "a.cSpam='".($fspam == 'y' ? 'y':'n')."'";
								}

								if($fstatus){
									$whereClause[] = "a.cStatus='".($fstatus == 'y' ? 'y':'n')."'";
								}

								if(is_array($whereClause)){
									$whereClause=implode(' and ',$whereClause);
								}

								if($whereClause){
									$whereClause = " AND $whereClause";
								}

								$sql="SELECT a.*,c.newsPermalink,c.newsTitle,d.catPermalink FROM newsinfo_comment a,newsinfo c,newsinfo_cat d
								WHERE c.catId=d.catId  AND a.newsId=c.newsId $whereClause ORDER BY a.cId DESC";

								$query=query($sql);
								$numofdata=rows($query);

								$num=1;
								$numPerPage=20;
								$offset=($pg-1)*$numPerPage;
								$num=$num+(($pg-1)*$numPerPage);

								$sql.=' LIMIT '.$offset.','.$numPerPage;
								$query=query($sql);
									if ($numofdata<1){
								?>
									<tr>
										<td colspan="6"><div class="text-center"><br /><?php print _('newsinfocomment_commentnotavailable'); ?><br /><br /></div></td>
									</tr>
								<?php
									}else{
										while ($data=fetch($query)){
											$data=output($data); 
											$memail=$data['cEmail'];
									?>
									<tr>
										<td class="text-center"><?php print $num; ?></td>
										<td>
											<a href="newsinfo_comment.php?newsid=<?php print $data['newsId'];?>" rel="tooltip" title="<?php print _('newsinfocomment_displaycomment'); ?>"><?php print $data['newsTitle'];?></a>
											<a href="http://<?php print getconfig('SITE_URL').'/blog/'.$data['catPermalink'].'/'.$data['newsPermalink'];?>/" target="_blank"><i class="fa fa-external-link"></i></a>
										</td>
										<td style="position:relative;">
											<strong><?php print _('newsinfocomment_shipper'); ?>: </strong>
											<?php
											#$numofComment = countdata("newsinfo_comment", "cEmail='$memail'") - 1;
											#$numofComment = number_format($numofComment, 0, ',', '.');

											print '<a style="font-weight:normal;" href="newsinfo_comment.php?searchtext='.stripquote($memail).'&Search=Temukan" rel="tooltip" title="Tampilkan semua komentar terkait ' .stripquote(ucfirst($data['cName'])).'">'.ucfirst($data['cName']).' ('.$data['cEmail'].')</a>';?>
											<div style="margin-top:2px;"><small><?php print dateformat($data['cDateAdded'],"full"); ?></small></div>
											<div style="margin-top:8px;">
											 <em>
											<?php
												$readmoreword="<a class='fancybox-frame' href='newsinfo_comment_readmore.php?action=readmore&amp;id=$data[cId]'> read more ...</a>";
												print readmore($data['cContent'],200,$readmoreword);
											?>
											</em>
											</div>

											<div style="position:absolute;top:4px;right:10px" data-toggle="tooltip" data-placement="left" title="<?php print $data['cSpam'] == 'y' ? 'Ditandai sebagai spam':'Bersih dari spam'; ?>"><i style="color:#<?php print $data['cSpam'] == 'y' ? 'cc0000':'00cc00'; ?>;" class="fa fa-envelope-o fa-lg"></i></div>

										</td>
										<td class="text-center" valign="top"><a title="detail" rel="tooltip" class="fancybox-frame" href="newsinfo_comment_readmore.php?action=readmore&id=<?php print $data['cId'];?>"><i class="fa fa-list-alt"></i></a></td>
										<td class="text-center" valign="top">
											<?php
											if ($data['cStatus']=="y"){
												?><a href="newsinfo_comment.php?form=submit&amp;action=hide&amp;id=<?php print $data['cId'];?>&pg=<?php print $pg;?>" rel="tooltip" title="<?php print _('newsinfocomment_hiddencomment'); ?>"><i style="color:#00cc00;" class="fa fa-check-square"></i></a><?php
											}else{
												?><a href="newsinfo_comment.php?form=submit&amp;action=show&amp;id=<?php print $data['cId'];?>&pg=<?php print $pg;?>" rel="tooltip" title="<?php print _('newsinfocomment_showcomments'); ?>"><i style="color:#cc0000;" class="fa fa-times-circle"></i></a><?php
											}?>
										</td>
										<td class="text-center" valign="top">
										<?php if ($uac_delete){ ?><a class="delete" href="newsinfo_comment.php?form=submit&amp;action=del&amp;id=<?php print $data['cId']; ?>" title="No: <?php print $num; ?>"><i class="fa fa-trash" title="<?php print _('newsinfocomment_deletecomment'); ?>" rel="tooltip"></i></a><?php } ?></td>
									</tr>
									<?php
											$num++;
										}
									}
								?>
								</tbody>
							</table>
						</div>
						<?php
						$options['total']=$numofdata;
						$options['filename']='newsinfo_comment.php';
						$options['qualifier']='newsinfo_comment.php';
						$options['pg']=$pg;
						$options['numPerPage']=$numPerPage;
						$options['style']=1;
						$options['addquery']=TRUE;
						paging($options);
						?>
					<?php } ?>
				</div>
	        </div>
	        <!-- END THE CONTENT OF PAGE HERE -->
				
		</div>
		<!-- END THE CONTENT OF PAGE HERE -->

		<?php include("com/com-footer.php"); ?>

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>