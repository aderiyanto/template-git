<?php
	$page=19;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config-image.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

	#check language license
	if($lang and !in_array($lang, $lc_lang) and ($action=='adden' or $action=='editen')){
		header('location:gallery.php');
		exit;
	}

	if($form=="submit"){
		//Is id exist ?
		if($action=="edit" or $action=="del"){
			$exist=countdata("gallery_photo","phoId='". (int)$phoid ."'");
			if($exist<1){
				$emptyid=errorlist(29);
			}
		}

		//set the restrictions
		$allowedType=array("image/pjpeg","image/jpeg","image/png","image/gif");
		$realExt = array('jpeg','jpg','png','gif');

		if($action=="add" and $uac_add){
			//get filename, filetype
			$pic=$_FILES['phofilename'];

			if(empty($pic['name']) or empty($phocaption)){
				$error=errorlist(2);
			}else{
				//make sure it is allowed
				if(in_array($pic['type'], $allowedType)){
					$fileExt = array_keys($allowedType, $pic['type']);
					$fileExt = $realExt[$fileExt[0]];

					if(is_uploaded_file($pic['tmp_name'])){
						//make sure it is really really image
						$img=getimagesize($pic['tmp_name']);
						if(!in_array($pic['type'],$allowedType) or !(is_numeric($img[0]) or is_numeric($img[1]) or $img[0]>0 or $img[1]>0)){
							//make sure it is allowed
							$error.=errorlist(13);
						}
					}else{
						$error.=errorlist(27);
					}
				} else{
					$error.=errorlist(13);
				}

				//get filename
				$fname = str_replace('.','',$pic['name']).'.'.$fileExt;

				if(!$error){
					//Next id
					$nextid=nextid("phoId","gallery_photo");

					$img=uploadit($fname,$pic['tmp_name'],"gallery",$nextid,GALLERY_SMALL_WIDTH,GALLERY_SMALL_HEIGHT,GALLERY_MEDIUM_WIDTH,GALLERY_MEDIUM_HEIGHT,GALLERY_LARGE_WIDTH,GALLERY_LARGE_HEIGHT);
					$dir=$img['dir'];
					$imgfilename=$img['filename'];

					//Set sort for different album
					$sqlspec="SELECT MAX(phoSort) AS latestId FROM gallery_photo WHERE albId='". (int)$albid ."'";
					$queryspec=query($sqlspec);
					$dataspec=fetch($queryspec);
					$latestId=$dataspec['latestId'];
					$sort=1;
					if (!empty($latestId)){
						$sort=$latestId+1;
					}

					//Is it will use watermark ?
					if($watermark=="y"){
						chdir('../../');
						$imagesrc='../assets/gallery/' . $dir . '/'.$imgfilename;
						watermark($imagesrc);
					}

					$sql="INSERT INTO gallery_photo VALUES ('$nextid','". (int)$albid ."','$phocaption','$dir','$imgfilename','0','$now','$phosource','$sort')";
					$query=query($sql);

					//Update Konten terakhir in album every modificate data in the album
					$sql="UPDATE gallery_album SET albLastUpdateDate='$now' WHERE albId='". (int)$albid ."'";
					$query=query($sql);
				}
			}
		}elseif($action=="edit" and $uac_edit){
			//Stop if lost the data id
			if($emptyid){
				$error=$emptyid;
			}else{
				if(empty($phocaption)){
					$error=errorlist(2);
				}else{
					//get filename, filetype
					$pic=$_FILES['phofilename'];
					if(!empty($pic['name'])){
						//make sure it is allowed
						if(in_array($pic['type'], $allowedType)){
							$fileExt = array_keys($allowedType, $pic['type']);
							$fileExt = $realExt[$fileExt[0]];

							if(is_uploaded_file($pic['tmp_name'])){
								//make sure it is really really image
								$img=getimagesize($pic['tmp_name']);
								if(!in_array($pic['type'],$allowedType) or !(is_numeric($img[0]) or is_numeric($img[1]) or $img[0]>0 or $img[1]>0)){
									//make sure it is allowed
									$error.=errorlist(13);
								}
							}else{
								$error.=errorlist(27);
							}
						} else{
							$error.=errorlist(13);
						}

						//get filename
						$fname = str_replace('.','',$pic['name']).'.'.$fileExt;
					}

					if($errorfile){
						$error.=$errorfile;
					}

					if(!$error){
						//Upload file if exist
						if(!empty($pic['name'])){
							$sql="SELECT * FROM gallery_photo WHERE albId='". (int)$albid ."' and phoId='". (int)$phoid ."'";
							$query=query($sql);
							$data=fetch($query);

							$img=uploadit($fname,$pic['tmp_name'],"gallery",$phoid,GALLERY_SMALL_WIDTH,GALLERY_SMALL_HEIGHT,GALLERY_MEDIUM_WIDTH,GALLERY_MEDIUM_HEIGHT,GALLERY_LARGE_WIDTH,GALLERY_LARGE_HEIGHT);
							$dir=$img['dir'];
							$imgfilename=$img['filename'];

							$sqlfile=",phoDir='$dir',phoFileName='$imgfilename'";

							chdir("../../");

							//Delete photo
							@unlink("../assets/gallery/".$data['phoDir'].'/'.$data['phoFileName']);
							@unlink("../assets/gallery/".$data['phoDir'].'/'.genthumb($data['phoFileName'],"m"));
							@unlink("../assets/gallery/".$data['phoDir'].'/'.genthumb($data['phoFileName'],"s"));
							@rmdir ("../assets/gallery/".$data['phoDir']);

							//Is it will use watermark ?
							if($watermark=="y"){
								$imagesrc='../assets/gallery/' . $dir . '/'.$imgfilename;
								watermark($imagesrc);
							}
						}

						//update data
						$sql="UPDATE gallery_photo SET phoCaption='$phocaption',phoSource='$phosource' $sqlfile WHERE albId='". (int)$albid ."' and phoId='". (int)$phoid ."'";
						$query=query($sql);

						//Update Konten terakhir in album every modificate data in the album
						$sql="UPDATE gallery_album SET albLastUpdateDate='$now' WHERE albId='". (int)$albid ."'";
						$query=query($sql);
					}
				}
			}
		}elseif ($action=="del" and $uac_delete){
			//Stop if lost the data id
			if($emptyid){
				$error=$emptyid;
			}else{
				$sql="SELECT * FROM gallery_photo WHERE albId='". (int)$albid ."' and phoId='". (int)$phoid ."'";
				$query=query($sql);
				$data=fetch($query);

				//Delete photo
				@unlink("../assets/gallery/".$data['phoDir'].'/'.$data['phoFileName']);
				@unlink("../assets/gallery/".$data['phoDir'].'/'.genthumb($data['phoFileName'],"m"));
				@unlink("../assets/gallery/".$data['phoDir'].'/'.genthumb($data['phoFileName'],"s"));
				@rmdir("../assets/gallery/".$data['phoDir']);

				//Delete all data in table photo and all files related with the albId
				$sql="DELETE FROM gallery_photo WHERE albId='". (int)$albid ."' and phoId='". (int)$phoid ."'";
				$query=query($sql);

				#remove multilingual features
				$sql="DELETE FROM gallery_photo_lang WHERE phoId='". (int)$phoid ."'";
				$query=query($sql);

				//Update Konten terakhir in album every modificate data in the album
				$sql="UPDATE gallery_album SET albLastUpdateDate='$now' WHERE albId='". (int)$albid ."'";
				$query=query($sql);
			}
		}elseif($action=="update"){
			foreach ($s as $id=>$sortnum){
			  $sql="UPDATE gallery_photo SET phoSort='$sortnum' WHERE phoId='$id' AND albId='". (int)$albid ."'";
				$query=query($sql);
			}
		}
		elseif($action=='adden' and $uac_add){
			if(empty($phocaption)){
				$error=errorlist(2);
			}
			else{
				$sql="INSERT INTO gallery_photo_lang VALUES ('$phoid', '$phocaption','$lang')";
				$query = query($sql);
			}
		}
		elseif($action=='editen' and $uac_edit){
			if(empty($phocaption)){
				$error=errorlist(2);
			}
			else{
				$sql="UPDATE gallery_photo_lang SET phoCaption='$phocaption' WHERE phoId='$phoid' AND lang='$lang'";
				$query = query($sql);
			}
		}

		//check whether query was successful
		if (!$query){
			$error=errorlist(3);
		}

		if ($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				$url="gallery.php?albid=$albid";
				header("location:$url");
			}else{
				print "ok";
			}
		}
		exit;
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _('gallery_pagetitle'); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('#result').hide();
	$('.fancybox').fancybox({
		'arrows': false
	});

	validate('#result','#add','gallery.php?albid=<?php print $albid; ?>');
	validate('#result','#edit','gallery.php?albid=<?php print $albid; ?>');
});
</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('gallery_title'); ?></h3>
			                <p><?php print nl2br(_('galleryalbum_pagedesc')); ?></p>
			            </div>
		            </div>
	        	</div>
        	</div>

        	<?php if ($uac_add){ ?>
        	<div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                	<div class="row">
	                	<div class="col-md-6">
		                	<div class="text-left">
		                		<a href="gallery_album.php" class="btn btn-warning"><i class="fa fa-angle-double-left"></i> <?php print _('gallery_backtoalbum'); ?></a>
		                	</div>
	                	</div>
	                	<div class="col-md-6">
		                	<div class="text-right">
		                		<a href="gallery.php?action=add&amp;albid=<?php print $albid; ?>" class="btn btn-primary"><i class="fa fa-plus"></i> <?php print _('gallery_addphoto'); ?></a>
		                	</div>
	                	</div>
	                </div>
                </div>
        	</div>
        	<?php } ?>

        	<div class="row">
	        	<div class="col-md-12">
                	<?php if ($action==""){ ?>
            		<form action="gallery.php?form=submit&amp;action=update&amp;albid=<?php print $albid; ?>" method="post" name="sort">
            			<?php if($uac_edit){ ?>
            			<div class="block-break">
            				<button type="submit" name="update" value="<?php print _('gallery_updatelist'); ?>" class="btn btn-sm btn-info"><i class="fa fa-refresh"></i> <?php print _('gallery_updatelist'); ?></button>
            			</div>
            			<?php } ?>
            			<div class="table-responsive">
                			<table class="table table-striped table-bordered table-hover nowrap">
                				<thead>
                					<tr class="tablehead">
										<th width="35"><?php print _('gallery_list'); ?></th>
										<th width="125">Thumbnail</th>
										<th><?php print _('gallery_description'); ?></th>
										<th width="120"><?php print _('gallery_source'); ?></th>
										<?php if(count($lc_lang)>1){ ?><th style="width:40px;" class="text-center"><i rel="tooltip" title="<?php echo _('multi_language_view_language'); ?>" class="fa fa-language"></i></th><?php }?>
										<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
										<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
									</tr>
                				</thead>
                				<tbody>
                					<?php
									if(empty($pg)){
										$pg=1;
									}

					 				$numPerPage=20;
									$offset=($pg-1)*$numPerPage;
									$num=1;
									$num=$num+(($pg-1)*$numPerPage);
									$sql="SELECT * FROM gallery_photo WHERE albId='". $albid ."' ORDER BY phoSort ASC";
									$query=query($sql);
									$numofdata=rows($query);
									$sql.=' LIMIT '.$offset.','.$numPerPage;
									$query=query($sql);
									if($numofdata<1){
									?>
									<tr>
										<td colspan="7"><div class="text-center"><br /><?php print _('gallery_photonotavailable'); ?><br /><br /></div></td>
									</tr>
									<?php
									} else {
										while ($data=fetch($query)){
									?>
									<tr>
										<td class="text-center" valign="top"><?php if($uac_edit){ ?><input type="text" size="3" name="s[<?php print $data['phoId']; ?>]" value="<?php print $data['phoSort']; ?>" class="text-center form-control" /><?php }else{ print $num;} ?></td>
										<td class="text-center" valign="top"><a class="fancybox" href="/assets/gallery/<?php print $data['phoDir']; ?>/<?php print $data['phoFileName']; ?>" rel="facebox"><img src="../assets/gallery/<?php print $data['phoDir']; ?>/<?php print genthumb($data['phoFileName'],"s"); ?>" alt="<?php print $data['phoFileName']; ?>"></a></td>
										<td valign="top"><?php print  $data['phoCaption']; ?></td>
										<td class="text-center" valign="top"><?php print  ($data['phoSource']=="" ? "-":"$data[phoSource]"); ?></td>
										<?php
										if(count($lc_lang)>1){
											?>
											<td class="text-center" valign="top">
												<?php
												foreach($lc_lang as $key=>$value){
													$exist=countdata("gallery_photo_lang","phoId='".$data['phoId']."' and lang='$value'");
													if($value==$lc_lang_default) continue;
													if($exist and $uac_edit){
														?><a href="gallery.php?action=editen&amp;lang=<?php print $value;?>&albid=<?php print $data['albId']; ?>&amp;phoid=<?php print $data['phoId']; ?>" rel="tooltip" title="<?php print _('gallery_editvers'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-edit.png" alt="<?php print $value;?>"  border="0" /></a><?php
													}
													elseif($uac_add){
														?><a href="gallery.php?action=adden&amp;lang=<?php print $value;?>&albid=<?php print $data['albId']; ?>&amp;phoid=<?php print $data['phoId']; ?>" rel="tooltip" title="<?php print _('gallery_addvers'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-add.png" alt="<?php print $value;?>" border="0" /></a><?php
													}
												}?>
											</td>
											<?php
										}?>
										<td class="text-center" valign="top">
											<?php if ($uac_edit){ ?><a href="gallery.php?action=edit&amp;albid=<?php print $data['albId']; ?>&amp;phoid=<?php print $data['phoId']; ?>" title="<?php print _('cms_edit'); ?>: foto" rel="tooltip"><i class="fa fa-edit"></i></a></a><?php } ?>
										</td>
										<td class="text-center" valign="top">
											<?php if ($uac_delete){ ?><a class="delete" href="gallery.php?form=submit&amp;action=del&amp;albid=<?php print $data['albId']; ?>&amp;phoid=<?php print $data['phoId']; ?>" title="photo"><i rel="tooltip" title="photo" class="fa fa-trash-o"></i></a><?php } ?>
										</td>
									</tr>
									<?php
											$num++;
										}
									}
								?>
                				</tbody>
                			</table>
                		</div>
                		<?php 
                		$options['total']=$numofdata;
						$options['filename']='gallery.php';
						$options['qualifier']='gallery.php';
						$options['pg']=$pg;
						$options['numPerPage']=$numPerPage;
						$options['style']=1;
						$options['addquery']=TRUE;
						paging($options);
                		?>
            		</form>
            		<?php } elseif($action=="add" and $uac_add){ ?>
            		<a href="gallery.php?albid=<?php print $albid; ?>" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('gallery_mainaddphotos') ?></h4>
					<hr/>

					<form action="gallery.php?form=submit&amp;action=add&amp;albid=<?php print $albid; ?>" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">

            			<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="phofilename">
	                        	<?php print _('gallery_file'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="phofilename" type="file" id="phofilename" size="40" required>
	                        	<span class="help-block"><?php print _('gallery_file_info'); ?></span>
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="phocaption">
	                        	<?php print _('gallery_description'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<textarea name="phocaption" type="text" id="phocaption" class="form-control" required maxlength="255"></textarea>
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="phocaption">
	                        	<?php print _('gallery_source'); ?>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="phosource" type="text" id="phosource" size="40" class="form-control">
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="watermark">Watermark 
	                        </label>
	                        <div class="checkbox col-md-6 col-sm-6 col-xs-12">
                        		<label>
                        			<input name="watermark" id="watermark" value="y" type="checkbox"> Ya
                        		</label>
	                        </div>
                    	</div>

                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='gallery.php?&amp;albid=<?php print $albid; ?>'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>

                    </form>
                    <?php
					} elseif($action=="edit" and $uac_edit){
						$sql="SELECT * FROM gallery_photo WHERE phoId='". $phoid ."'";
						$query=query($sql);
						$data=formoutput($data=fetch($query));
					?>
					<a href="gallery.php?albid=<?php print $albid; ?>" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('gallery_editphotos') ?></h4>
					<hr/>

					<form action="gallery.php?form=submit&amp;action=edit&amp;albid=<?php print $albid; ?>&amp;phoid=<?php print $phoid; ?>" method="post" enctype="multipart/form-data" name="edit" id="edit" class="form-horizontal form-label-left">
						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="albname"><?php print _('gallery_file'); ?>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<img src="/assets/gallery/<?php print $data['phoDir']; ?>/<?php print genthumb($data['phoFileName'],"m"); ?>" alt="<?php print stripquote($data['phoFileName'],"m"); ?>" title="<?php print stripquote($data['phoFileName']); ?>">
								<br /><br />
								<input name="phofilename" type="file" id="phofilename" size="40" />
								<span class="help-block"><?php print _('gallery_file_info'); ?></span>
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="phocaption"><?php print _('gallery_description'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<textarea name="phocaption" type="text" id="phocaption" cols="60" rows="4" required class="form-control" maxlength="255"><?php print $data['phoCaption']; ?></textarea>
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="phosource"><?php print _('gallery_source'); ?>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="phosource" type="text" id="phosource" size="40" value="<?php print $data['phoSource']; ?>" class="form-control">
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="watermark">Watermark 
	                        </label>
	                        <div class="checkbox col-md-6 col-sm-6 col-xs-12">
                        		<label>
                        			<input name="watermark" id="watermark" value="y" type="checkbox"> Ya
                        		</label>
	                        </div>
                    	</div>

                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_updatebutton'); ?>"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='gallery.php?&amp;albid=<?php print $albid; ?>'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>

					</form>
					<?php }elseif($action=="adden" and $uac_add){
						$img=getval('phoDir,phoFileName','gallery_photo','phoId',$phoid);
					?>
					<a href="gallery.php?albid=<?php print $albid; ?>" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('gallery_addphotovers'); ?> <?php print localename($lang); ?></h4>
					<hr/>							

					<form action="gallery.php?form=submit&amp;action=adden&amp;lang=<?php print $lang;?>&amp;albid=<?php print $albid;?>&amp;phoid=<?php print $phoid; ?>" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">

						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="phocaption">
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<img src="/assets/gallery/<?php print $img['phoDir'];?>/<?php print genthumb($img['phoFileName'],'m');?>"/>
	                        </div>
                    	</div>

						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="phocaption"><?php print _('gallery_description'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<textarea name="phocaption" type="text" id="phocaption" cols="60" rows="4" required class="form-control col-md-7 col-xs-12" maxlength="255"></textarea>
	                        </div>
                    	</div>

                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='gallery.php?&amp;albid=<?php print $albid; ?>'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i>  <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>

					</form>
					<?php
					} elseif($action=="editen" and $uac_edit){
						$img=getval('phoDir,phoFileName','gallery_photo','phoId',$phoid);

						$sql="SELECT * FROM gallery_photo_lang WHERE phoId='$phoid' AND lang='$lang'";
						$query=query($sql);
						$data=formoutput($data=fetch($query));
					?>
					<a href="gallery.php?albid=<?php print $albid; ?>" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('gallery_editphotovers'); ?> <?php print localename($lang); ?></h4>
					<hr/>
					<form action="gallery.php?form=submit&amp;action=editen&amp;lang=<?php print $lang;?>&amp;albid=<?php print $albid; ?>&amp;phoid=<?php print $phoid; ?>" method="post" enctype="multipart/form-data" name="edit" id="edit" class="form-horizontal form-label-left">

						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="phocaption">
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<img src="/assets/gallery/<?php print $img['phoDir'];?>/<?php print genthumb($img['phoFileName'],'m');?>"/>
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="phocaption"><?php print _('gallery_description'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<textarea name="phocaption" type="text" id="phocaption" cols="60" rows="4" required class="form-control col-md-7 col-xs-12" maxlength="255"><?php print $data['phoCaption']; ?></textarea>
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_updatebutton'); ?>"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='gallery.php?&amp;albid=<?php print $albid; ?>'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>

					</form>
                	<?php } ?>
		        </div>

		    </div>
		    <!-- END .row -->

		</div>
        <!-- END THE CONTENT OF PAGE HERE -->


		<!-- footer content -->
        <?php include("com/com-footer.php"); ?>
        <!-- /footer content -->

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>
