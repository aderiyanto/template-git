<?php
	$page=41;
	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/mainmod.php");	
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("../modz/errormsg.php");
	include("authuser.php");
	include("../modz/mainmod-extend.php");
	
	if ($form=="submit"){
        if ($action=="delete"){
			foreach ($checkedid as $key => $value) {
				$sql = "DELETE FROM sms  WHERE smsId='$value'";
				$query = query($sql);
			}
      
		}
        if(!$query){
			$error=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:sms_log.php");
			}else{
				print "ok";
			}
		}
		exit;
    }

	
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print _('sms_page_title'); ?> - <?php print _('merchant_area'); ?> - <?php print SITE_NAME; ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="/libs/bootstrap-datepicker/css/datepicker3.css" />
<link rel="stylesheet" href="/libs/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" type="text/css" href="/style/footable.bootstrap.css" />
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">


<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>

<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="/libs/moment/min/moment-with-locales.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="/javascript/footable.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($){
	
	/* check all id function */
	$(".checkedid_all").click(function(){
		if ( (this).checked == true ){

		   $('.checkbox-area').prop('checked', true);

		} else {

		   $('.checkbox-area').prop('checked', false);

		}
	});
	
	/*delete bulk function */
	$(document).on('click', '.delete_bulk', function(event) {
		con=$(this);
		if(con.attr('title')){
			var orititle=con.attr('title');
		}else{
			var orititle=con.attr('data-original-title');
		}
		event.preventDefault();

		if(confirm( orititle.replace('Delete: ', '') + '?')){
			form=$('#bulk_action');
			$.ajax({
				type: 'POST',
				url: form.attr('action')+'&js=on',
				data: form.serialize(),
				beforeSend: function(data){
					form.attr('disabled','disabled');
				},
				success: function(data) {
					if(data){
						data = data.trim();
						if (data=='ok') {
							 window.location = 'sms_log.php<?php print ($pg) ? $pg.'/' : ''; ?>';
						}
						else if(data.slice(0,7)=='<script' || data.slice(-1,8)=='/script>'){
							$(result).html(data);
						}else{
							$('#result').show();
							$('#result').html(data);
							$(form).click(function(form) {
								$(result).fadeOut(500);
							});
						}
					}
					con.removeAttr('disabled');
				}
			});
		}
	});
});

</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">
			<div class="row">
				<div class="col-md-12">
					<h3><?php print _('sms_page_title'); ?></h3>
                    <p><?php print _('sms_page_title_desc'); ?></p>
					<hr>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
                    <?php 
					if ($action==""){
                    ?>
                   <div class="row">
	                    <div class="col-md-7 pull-right">
	                        <div class="pull-right">
	                            <form name="search" method="get" class="form-inline">
	                                <div class="form-group">
	                                    <input name="searchtext" type="text" class="form-control input-sm" id="keyword" size="40" value="<?php print $searchtext;?>" />
	                                </div>
	                                <button type="submit" name="Submit" value="<?php print _('webbot_find'); ?>" class="btn btn-sm btn-dark"><?php print _('search'); ?></button>
	                            </form>
	                        </div>
	                    </div>
	                </div>
	                <br/>
                    <div class="row">
						<form action="sms_log.php?action=delete&amp;form=submit" method="post" enctype="multipart/form-data" name="bulk_action" id="bulk_action">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
										<thead>
											<tr>
												<th style="width:20px;" class="text-center"><input name="checkedid_all" class="checkedid_all" type="checkbox" value="y" /></th>
												<th style="width:20px;" class="text-center"><?php print _('cms_no'); ?></th>
												<th style="width:120px;" class="text-center"><?php print _('sms_to'); ?></th>
												<th style="width:450px;" data-breakpoints="xs" class="text-center"><?php print _('sms_text'); ?></th>
												<th style="width:230px;" data-breakpoints="xs" class="text-center"><?php print _('sms_datetime'); ?></th>
												<th style="width:100px;" class="text-center" class="text-center"><?php print _('sms_status'); ?></th>
												<th style="width:20px;" data-breakpoints="xs" class="text-center"></th>
												
											</tr>
										</thead>
										<tbody data-empty="bcdcbd">
											<?php
											if(empty($pg)){
												$pg=1;
											}

											$numPerPage=50;
											$offset=($pg-1)*$numPerPage;
											$num=1;
											$num=$num+(($pg-1)*$numPerPage);
							
											if($searchtext){
												$whereClause[]="(smsText like '%$searchtext%' OR smsTo LIKE '%$searchtext%')";
											}

											if(is_array($whereClause)){
												$whereClause=implode('  and ',$whereClause);
											}
											if($searchtext){
												$whereClause="AND $whereClause";
											}    
										
											$sql="SELECT * FROM sms WHERE smsId!=0 $whereClause ORDER BY smsId DESC";
											$query=query($sql);
											$numofdata=rows($query);
											$sql.=' LIMIT '.$offset.','.$numPerPage;
											$query=query($sql);
											if($numofdata>0){ 
												$arrayhistory = array();
												while($data = fetch($query)){
													$arrayhistory[] = $data;
													$sql2 = "SELECT * FROM sms_history WHERE historyMessageId='$data[messageId]' ORDER BY historyId DESC LIMIT 1";
													$query2 = query($sql2);
													$data2 = fetch($query2);
													$historyresponse = $data2['historyResponse'];
													$decodejson = json_decode($historyresponse, true);
													$statusgroupid = $decodejson['results'][0][status][groupId];
													$statusgroupname= $decodejson['results'][0][status][groupName];
											?>
											<tr>
												<td >
													<input name="checkedid[]" class="checkbox-area" type="checkbox" value="<?php print $data['smsId']; ?>" />
												</td>
												<td class="text-center"><?php print $num; ?></td>
												<td><?php print output($data['smsTo']); ?></td>
											   
												<td><?php print output($data['smsText']); ?></td>
												<td  class="text-center">
													<?php print output(dateformat($data['smsTimetime'],"")); ?>
													<?php print output(date("H:i:s",$data['smsTimetime'])); ?> WIB
												</td>
												<td class="text-center">
													<?php
													if($data['smsStatus']=="new"){
													?>
													 <span class="label label-warning">Pending</span>
													<?php
													}else{
														if(empty($data['messageId'])){
														?>
															<span class="label label-info">On Process</span>
														<?php
														}else{
															if($statusgroupid==1){
																print "<span class='label label-warning'>$statusgroupname</span>";
															}else if($statusgroupid==2){
																print "<span class='label label-danger'>$statusgroupname</span>";
															}else if($statusgroupid==3){
																print "<span class='label label-success'>$statusgroupname</span>";
															}
														}
													}
													?>
												</td>
												<td class="text-center">
													<?php
													if(!empty($data['messageId'])){
													?>
													<a id="modal-history" href="#modal-history<?php print $data['smsId']; ?>"  data-toggle="modal"><i class="fa fa-list"></i></a>
													<?php
													}else{
												    ?>
                                                    <i class="fa fa-list"></i>
                                                    <?php
													}
													?>
												</td>
												
											</tr>
											<?php
												$num++;
												}
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-md-12" style="margin-bottom:15px;">
								<div class="row">
									<?php
                                    if($numofdata>0){ 
                                    ?>
									<div class="col-md-10 col-sm-9 col-xs-8">
										<button title="<?php print _('delete_question'); ?>" class="btn btn-danger delete_bulk"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp;<?php print _('sms_btndel_logs'); ?></button>
									</div>
                                    <?php
                                    }
                                    ?>
								</div>
							</div>
						</form>
                        <?php
                        $options['total']=$numofdata;
                        $options['filename']='sms_log.php';
                        $options['qualifier']='sms_log.php';
                        $options['pg']=$pg;
                        $options['numPerPage']=$numPerPage;
                        $options['style']=1;
                        $options['addquery']=true;
                        paging($options);
                        ?>
                    </div>
                    <?php
                    foreach($arrayhistory as $index=>$val){
                    ?>
                    <div class="modal fade" id="modal-history<?php print $val['smsId']; ?>" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">

                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        ×
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">
                                        History SMS
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <th style="width:20px;">No</th>
                                                <th style="width:10px;">Message ID</th>
                                                <th style="width:130px;">Sent AT</th>
                                                <th style="width:130px;">Done AT</th>
                                                <th style="width:10px;" class="text-center">SMS Count</th>
                                                <th style="width:120px;">Price</th>
                                                <th style="width:50px;">Status</th>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $sqlhistory = "SELECT * FROM sms_history WHERE historyMessageId='$val[messageId]'";
                                                $queryhistory = query($sqlhistory);
                                                $rowshistory = rows($queryhistory);
                                                if($rowshistory > 0){
                                                    $numb = 1;
                                                    while($datahistory = fetch($queryhistory)){
                                                        $history_response = $datahistory['historyResponse'];
                                                        $decode_json = json_decode($history_response, true);
                                                        //print "<pre>";
                                                        //print_r($decode_json);
                                                ?>
                                                <tr>
                                                    <td><?php print $numb; ?></td>
                                                    <td><?php print $decode_json['results'][0][messageId]; ?></td>
                                                    <td><?php print gmdate("Y-m-d H:i:s",strtotime($decode_json['results'][0][sentAt])); ?></td>
                                                    <td><?php print gmdate("Y-m-d H:i:s",strtotime($decode_json['results'][0][doneAt])); ?></td>
                                                    <td class="text-center"><?php print $decode_json['results'][0][smsCount]; ?></td>
                                                    <td><?php print "Per Message: ".$decode_json['results'][0][price][pricePerMessage]."<br>Currency: ".$decode_json['results'][0][price][currency]; ?></td>
                                                    <td><?php print "Group Name<br>"."<strong>".$decode_json['results'][0][status][groupName]."</strong><br>Name<br><strong>".$decode_json['results'][0][status][name]."</strong>"; ?></td>
                                                </tr>
                                                <?php
                                                        
                                                        $numb++;
                                                    }
                                                }else{
                                                ?>
                                                <tr>
                                                    <td colspan="9" class="text-center">Belum Ada History</td>
                                                </tr>
                                                <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="modal-footer">

                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                        Close
                                    </button>
                                </div>
                            </div>

                        </div>

                    </div>
                    <?php
                    }    
                    ?>
                    <?php
                    }
                    ?>
				</div>
			</div>
        </div>
        <!-- /page content -->

        <?php include("com/com-footer.php"); ?>
	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>