<?php
	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");
	
	#change language
	$ref = $_SERVER['HTTP_REFERER'];
	
	$multilang_cms = getconfig('MULTILANGUAGE_CMS');
	if(in_array(cleanup($_GET['lang']), $multilang_cms)){
		setcookie("lang_admin",cleanup($_GET['lang']),0,"/");
	}

	if($ref){
		header('location:'.$ref);
	}
	else{
		header('location:/admin/');
	}
	exit;
?>