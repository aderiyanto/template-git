<?php
	#this file only allow to execute by using cron or cli command
	if(! defined('STDIN') ){
		die('Forbidden Access');
	}
	
	//includes all files necessary to support operations
	$basepath = realpath(dirname(__file__).'/..');
	include($basepath."/modz/config-main.php");
	include($basepath."/modz/config.php");
	include($basepath."/modz/license.php");
	include($basepath."/modz/errormsg.php");
	include($basepath."/modz/mainmod.php");
	include($basepath."/modz/connic.php");
	include($basepath."/modz/getall-admin.php");

	#SELECT NEWSLETTER 
	$sqln = "SELECT * FROM newsletter_attach JOIN newsletter ON newsletter_attach.nId=newsletter.nId";
	$queryn = query($sqln);
	while($datan = fetch($queryn)){
		$data_att[] = $datan['nId'];
	}
	
	#This file will access data from database and send it as email according to cron schedule
	$sql = "SELECT * FROM newsletter_email_queue JOIN newsletter_tmp JOIN newsletter ON newsletter_email_queue.tmpSign=CONCAT(newsletter_tmp.subId, '.', newsletter_tmp.nId ) AND newsletter_tmp.nId=newsletter.nId WHERE newsletter_email_queue.emailStatus='n' AND newsletter.nSending='y' ORDER BY newsletter_email_queue.emailId ASC LIMIT ".getconfig('EMAIL_QUOTA')."";
	$query = query($sql);
	$numofdata = rows($query);
	
	if ($numofdata>0){  
		while($data = fetch($query)){
			## PROSES HERE
			$subid = getval("subId","newsletter_subscriber","subEmail='".$data['emailTo']."'");
			$sql_is = "SELECT newsletter.nId FROM newsletter_tmp JOIN newsletter ON newsletter_tmp.nId=newsletter.nId WHERE newsletter_tmp.subId='".$subid."'";
			$query_s = query($sql_is);
			$data_s = fetch($query_s);
			$thenid = $data_s['nId'];
			if(in_array($thenid,$data_att)){
				$files = getval("attachDir,attachFile","newsletter_attach","nId='".$thenid."'");
				$thedir = $files['attachDir'];
				$path = $basepath.'/assets/newsletter/'.$thedir.'/';
				$thefile = $files['attachFile'];
			}else{
				$path = '';
				$thefile = '';
			}
			$toname = explode("@",$data['emailTo']);
			$toname = $toname[0];
			
			$options = array(
				'mailopt' => '',
				'from' => SITE_EMAIL,
				'fromname' => SITE_NAME,
				'to' => $data['emailTo'],
				'toname' => $toname,
				'subject' => $data['emailSubject'],
				'replyto' => '',
				'cc' => '',
				'bcc' => '',
				'message' => $data['emailMsg'],
				'messagetype' => 'html',
				'filename' => $path.$thefile
			);
			$mail = sendMailComplete($options);

			#update email statiscic
			if($mail){
				$sqlx = "UPDATE newsletter_email_queue SET emailStatus='y' WHERE emailId='".$data['emailId']."'";
				$queryx = query($sqlx);
			}
		}
	}
	
	#update newsletter status
	$sql = "SELECT * FROM newsletter WHERE nSending='y'";
	$query = query($sql);
	if(rows($query)){
		while($data = fetch($query)){
			$sqlx = "SELECT COUNT(*) as nData FROM newsletter_email_queue WHERE emailStatus='n' AND substring_index(tmpSign, '.' , -1) = '{$data['nId']}'";
			$queryx = query($sqlx);
			$datax = fetch($queryx);
			
			if($datax['nData'] == 0){
				#update newsletter status
				$sqlx = "UPDATE newsletter SET  nSending='' WHERE nId='{$data['nId']}'";
				$queryx = query($sqlx);
			}
		}
	}
	
	#update activity statistic
	$cron_option['cron_name'] = 'CRON_NEWSLETTER'; 
	cron_activity_log($cron_option);
?>