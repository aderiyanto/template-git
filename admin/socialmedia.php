<?php
	$page=28;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");	 
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

	@ksort($generaldata);

	if ($form=="submit" and $action=="edit" and $uac_edit){
		#if any form is submitted
		foreach($social as $key=>$value){
			$sql = "UPDATE socialmedia SET socialValue='$value' WHERE socialId='$key'";
			$query = query($sql);
		}
		
		#check whether query was successful
		if(!$js){
			header("location:socialmedia.php");
			exit;
		}
		else{
			print "ok";
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _('socialmedia_contact_pagetittle'); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>

<script type="text/javascript">
jQuery(document).ready(function($) {
	$('.fancybox').fancybox({
		'arrows': false
	});
    $('#result').hide();
	validate('#result','#edit','socialmedia.php');
});
</script>
</head><body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('socialmedia_contact_pagetittle'); ?></h3>
			                <p><?php print _('socialmedia_contact_pagedesc'); ?></p>
			            </div>
		            </div>
	        	</div>
        	</div>

        	<!-- THE CONTENT OF PAGE HERE -->
        	<div class="row">
	        	<div class="col-md-12">
	        		<hr/>
	        		<form id="edit" name="edit" method="post" action="socialmedia.php?form=submit&amp;action=edit" class="form-horizontal form-label-left" class="form-horizontal form-label-left">
	        			
	        			<?php
						$sql="SELECT * FROM socialmedia ORDER BY socialRemark ASC, socialId ASC";
						$query=query($sql);
						
						$num=1;
						$icon=array("fa fa-phone",
									"fa fa-envelope",
									"fa fa-facebook",
									"fa fa-twitter",
									"fa fa-google-plus",
									"fa fa-youtube-play",
									"glyphicon glyphicon-comment",
									"fa fa-weixin",
									"fa fa-whatsapp",
									"fa fa-comment",
									"fa fa-instagram");
						while ($data=fetch($query)){
							//color definition
							$badge_style = "border:1px solid #ccc;background:#fff; color:#444;";
							if($data['socialType']=='facebook'):		$badge_style = "border:none;background:#3B5998;";
							elseif($data['socialType']=='twitter'):		$badge_style = "border:none;background:#55ACEE;";
							elseif($data['socialType']=='bbm'):			$badge_style = "border:none;background:#333333;";
							elseif($data['socialType']=='wechat'):		$badge_style = "border:none;background:#5FC325;";
							elseif($data['socialType']=='whatsapp'):	$badge_style = "border:none;background:#2AB200;";
							elseif($data['socialType']=='line'):		$badge_style = "border:none;background:#4ECD00;";
							elseif($data['socialType']=='gplus'):		$badge_style = "border:none;background:#DC4E40;";
							elseif($data['socialType']=='youtube'):		$badge_style = "border:none;background:#C8302A;";
							elseif($data['socialType']=='instagram'):	$badge_style = "border:none;background:#000000;";
							endif;
							?>

							<div class="form-group">
							    <label class="control-label col-md-1 col-sm-2 col-xs-12" for="<?php print $data['socialId']; ?>">
							    	<span class="label label-default" style="<?php echo $badge_style; ?>"><i class="<?php print $icon[$data['socialId']-1]; ?>"></i> <?php print $data['socialName']; ?></span>
							    </label>
							    <div class="col-md-5 col-sm-6 col-xs-12">
							    	<?php if($uac_edit){ ?>
								    <input type="text" class="form-control" name="social[<?php print $data['socialId'];?>]" value="<?php print stripquote($data['socialValue']);?>" id="<?php print $data['socialId']; ?>" />
								    <?php
									if($data['socialRemark']){
										print '<span class="help-block">'.$data['socialRemark'].'</span>';
									}
									//only for phone
									if($data['socialType'] == 'phone'){
										print '<span class="help-block">'._('socialmedia_view_phoneremark').'</span>';
									} }else{
										print "<div class='control-label'>: ".stripquote($data['socialValue'])."</div>";
									}
									?>
								</div>
							</div>						

							<?php
							$num++;
						}
						?>
						<hr/>
						<?php if($uac_edit){ ?>
						<div class="form-group">
							<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_updatebutton'); ?>"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
						</div>
						<?php } ?>
						<br/><br/><br/>
	        		</form>
	        	</div>
	        </div>

		</div>
		<!-- END THE CONTENT OF PAGE HERE -->

		<?php include("com/com-footer.php"); ?>

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>