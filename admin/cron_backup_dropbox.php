<?php
	#this file only allow to execute by using cron or cli command, but it still accessible using specific http get parameter
	if(isset($_SERVER['HTTP_USER_AGENT']) and $_GET['t'] != date('mY')){
		die('Forbidden Access');
	}
	
	//includes all files necessary to support operations
	$basepath = realpath(dirname(__file__).'/..');
	include($basepath."/modz/config-main.php");
	include($basepath."/modz/config.php");
	include($basepath."/modz/license.php");
	include($basepath."/modz/errormsg.php");
	include($basepath."/modz/mainmod.php");
	include($basepath."/modz/connic.php");
	include($basepath."/modz/getall-admin.php"); 
	
	if(ONLINE_BACKUP == 'off'){
		exit();
	}
	
	#detect database change based activitylog table
	$data = getval("actData", "activitylog", "actName", "CRON_BACKUP_DATABASE");
	if($data){
		$data = unserialize($data);
		if($data['last_backup'] > $data['last_modify']){
			exit;
		}
	}
	
	#create file temp and upload
	$fname = getconfig('DB_NAME').'-'.date("d",$now).date("F",$now).date("Y-Hi",$now).'-'.codegen(3);
	$dir = substr(dirname(__FILE__), 0, -6);
	$cmd = "php -q \"".$dir."/admin/cron_backup_dropbox_do.php\" \"".$dir."\" \"".($_SERVER['MYSQL_HOME'] ? $_SERVER['MYSQL_HOME']:'unknown')."\" \"$fname\"";
	cli($cmd);
?>