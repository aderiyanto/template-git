<?php
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");

	#validate form guardian
	$fguard = guardian('validate');

	if(is_string($fguard)){
		#user pass, same key that already used before. we have to generate a new key and resubmit user form
		?>
		<script type="text/javascript">
			formguard('#login', '<?php print $fguard;?>');
			$('#login').submit();
		</script>
		<?php
		exit;
	}
	
	#get visitor ip address, it's valid ip or not
	$ipAddress = visitor_ip();
	
	if(empty($username) or empty($password)){
		$error = errorlist(2);
	}
	else{
		$sql="SELECT * FROM admin WHERE adminUsername='$username' and adminActive='y'";
		$query=query($sql);
		$numOfReg=rows($query);

		// check whether user exist or not
		if ($numOfReg<1) {
			$error=errorlist(4);
		}

		//check whether query was successful
		$data         = fetch($query);
		$realpassword = $data['adminPassword'];
		$mid          = $data['adminId'];
		$lastlogin    = $data['adminLastLogin'];
		$level        = $data['groupId'];
		$password     = sha1($password.getconfig('CMS_SALT_STRING'));

		// check whether typed password equal with stored password
		if(!($password===$realpassword)) {
			$error = errorlist(4);
		}

		if(!$error) {
			$cookie1=$username;
			$cookie2=$mid;
			$cookie4=$level;

			//create a checkpoint to verify that all cookies value (see the line above) are valid
			//This to prevent hacker/cracker to directly change the value of cookies file
			$cookie3="[[[[CM||S" . $cookie1 . "Au0m4t3&F44STENProc" . $cookie2 . "6..009>>>" . $cookie4 . "..]]]]]]" . SITE_NAME;
			$cookie3=md5($cookie3);

			setcookie("cook_username",$cookie1,0,"/");
			setcookie("cook_id",$cookie2,0,"/");
			setcookie("cook_gid",$cookie4,0,"/");
			setcookie("cook_cp",$cookie3,0,"/");
			setcookie("cook_lastlog",$lastlogin,0,"/");

			// update admin last login to table
			$sql="UPDATE admin SET adminLastLogin=$now WHERE adminUsername='$username'";
			$query=query($sql);
		}
		
		if(!$query){
			$error=errorlist(3);
		}
	}

	//check whether query was successful
	if($error){
		print "<p>";
		print "<ul>";
		print nl2br($error);
		print "</ul>";
		print "</p>";
	}else{
		#clear form guardian session
		guardian('clear');
		
		if(!$js){
			header("location:index.php");
		}else{
			print "ok";
		}
	}
?>
