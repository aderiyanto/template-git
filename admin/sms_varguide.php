<?php
	#This file used to guiding user to using the variable, every email template have different amount of variable
	#If variable forced by the user, the variable will not changed to be the wish replacement, it will show the variable name
	$message=str_ireplace("{MOBILE}",$mobile,$message);
	$message=str_ireplace("{FULLNAME}",$fullname,$message);
	$message=str_ireplace("{EMAIL}",$email,$message);
	$message=str_ireplace("{PERSONNAME}",$personName,$message);
	$message=str_ireplace("{COMPANYNAME}",$company,$message);
	$message=str_ireplace("{SYSTEMNAME}",SITE_NAME,$message);
	$message=str_ireplace("{PACKAGENAME}",$itemname,$message);
	$message=str_ireplace("{PICNAME}",$picname,$message);
	$message=str_ireplace("{DATE}",$date,$message);
	$message=str_ireplace("{POIN}",$totalpoin,$message);
	$message=str_ireplace("{PACKAGESTARTDATE}",$starttime,$message);
	$message=str_ireplace("{POINFIRST}",$poinpackage,$message);
	$message=str_ireplace("{PROGRESS}",$progress,$message);
	$message=str_ireplace("{PACKAGEDATA}",$packagedata,$message);
    
	/* Output $message before printed */
	$message = output($message);
?>