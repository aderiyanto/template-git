<?php
	$page=26;
	
	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");
	include("../modz/google/gapi.class.php");
?>	
	<div id="querytable">
		<?php
			if($action=='showquery'){
				if($id=='country'){
					$dimension='country';
					$dimensiontitle='Negara';
				}elseif($id=='city'){
					$dimension='city';
					$dimensiontitle='Kota';
				}elseif($id=='browser'){
					$dimension='browser';
					$dimensiontitle='Web Browser';
				}elseif($id=='os'){
					$dimension='operatingSystem';
					$dimensiontitle='Sistem Operasi';
				}/* elseif($id=='mos'){
					$dimension='mobileDeviceModel';
					$dimensiontitle='Mobile: Sistem Operasi';
				} */
			}else{
				$dimension='city';
				$dimensiontitle='Kota';
			}   
		 
			list($day,$month,$year)=explode("/", $startdate);		
			$startdate=$year.'-'.$month.'-'.$day;
			
			list($day,$month,$year)=explode("/", $enddate);	
			$enddate=$year.'-'.$month.'-'.$day;
		 
			$ga = new gapi(getconfig('GA_EMAIL'),getconfig('GA_PASSWORD')); 
			$ga->requestReportData(getconfig('GA_PROFILE_ID'),array($dimension),array('visits','pageviews'),'-visits','',$startdate,$enddate);
			$allvisitor=$ga->getVisits();
		?>
 		<table width="100%" border="1" cellspacing="0" cellpadding="5">
			<tr class="tablehead">
				<td width="30">No</td>
				<td><?php print $dimensiontitle;?></td>
				<td width="100">Kunjungan</td>
				<td width="100">% Kunjungan</td>
			</tr>
		<?php 
			if($allvisitor<1){
				print '<tr><td  colspan="4" align="center" class="asterik">Data belum ada</td></tr>';
			}else{
			$no=1;
			foreach($ga->getResults() as $result) {?>
			<tr onmouseover="this.className='hilite'" onmouseout="this.className=''">
				<td  align="center"><?php print $no; ?></td>
				<td><strong><?php print $result;  ?></strong></td>
				<td align="center"><?php print $result->getVisits(); ?></td>
				<td align="center"><?php print abs(round(($result->getVisits()/$allvisitor)*100,2)); ?>%</td>
			</tr>
		<?php 
				$no++;
			}
			}
		?>
        </table>
	</div>