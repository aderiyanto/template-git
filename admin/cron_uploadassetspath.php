<?php
	#this file only allow to execute by using cron or cli command, but it still accessible using specific http get parameter
	/*if(isset($_SERVER['HTTP_USER_AGENT']) and $_GET['t'] != date('mY')){
		die('Forbidden Access');
	}*/

	//includes all files necessary to support operations
	$basepath = realpath(dirname(__file__).'/..');
	include($basepath."/modz/config-main.php");
	include($basepath."/modz/config.php");
	include($basepath."/modz/license.php");
	include($basepath."/modz/errormsg.php");
	include($basepath."/modz/mainmod.php");
	include($basepath."/modz/mainmod-extend.php");
	include($basepath."/modz/connic.php");
	include($basepath."/modz/getall-admin.php");
	require_once ($basepath.'/modz/dropbox/demo-lib.php');
	include($basepath."/modz/dropbox/DropboxClient.php");

	if(ONLINE_BACKUP == 'off'){
		exit;
	}

require_once ($basepath.'/modz/dropbox/demo-lib.php');
demo_init(); // this just enables nicer output

// if there are many files in your Dropbox it can take some time, so disable the max. execution time
set_time_limit( 0 );

require_once ($basepath.'/modz/dropbox/DropboxClient.php');

/** you have to create an app at @see https://www.dropbox.com/developers/apps and enter details below: */
/** @noinspection SpellCheckingInspection */
$dropbox = new DropboxClient( array(
	'app_key'         => getconfig('DROPBOX_APP_KEY'),
	'app_secret'      => getconfig('DROPBOX_APP_SECRET'),
	'app_full_access' => false,
) );


/**
 * Dropbox will redirect the user here
 * @var string $return_url
 */
$return_url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'] . "?auth_redirect=1";

// first, try to load existing access token
$bisnislab_token = load_token( "bisnislab" );

if ( $bisnislab_token ) {
	$dropbox->SetBearerToken( $bisnislab_token );
	//echo "loaded bisnislab token: " . json_encode( $bisnislab_token, JSON_PRETTY_PRINT ) . "\n";
} elseif ( ! empty( $_GET['auth_redirect'] ) ) // are we coming from dropbox's auth page?
{
	// get & store bisnislab token
	$bisnislab_token = $dropbox->GetbisnislabToken( null, $return_url );
	demo_store_token( $bisnislab_token, "bisnislab" );
} elseif ( ! $dropbox->IsAuthorized() ) {
	// redirect user to Dropbox auth page
	$auth_url = $dropbox->BuildAuthorizeUrl( $return_url );
	die( "Authentication required. <a href='$auth_url'>Continue.</a>" );
}

	#get root dir
	$dir = substr(dirname(__FILE__), 0, -6);
	$xmlfile_to_upload  = $dir.DIRECTORY_SEPARATOR."xml-backup".DIRECTORY_SEPARATOR."uploaded-file.xml";

     $scandir = xml_fetchall_data($xmlfile_to_upload);


	#get scanned file name
	#UPLOAD TO DROPBOX
	foreach($scandir as $file => $date){
		$filepath = $file;
		$filepath_in_dropbox = str_replace($dir,'',$file); //get relative part of path
		if(is_file($filepath)){
			$meta = $dropbox->UploadFile($filepath,'backup_assets'.DIRECTORY_SEPARATOR.getconfig('SITE_SHORT_URL').DIRECTORY_SEPARATOR.date("d",$now).date("F",$now).date("Y-Hi",$now).$filepath_in_dropbox);
			//print_r($meta);//activate for debug mode
		}
	}

	#Do not touch, please :D
	#================================================================================
	#input array of scandir() and extension if file (eg. zip,txt,etc)
	#return string: name of matches file in scanned directory
	function xml_fetchall_data($path){
		#return data array( path => date) if exist and false if not exist
 		$data = array();
		$xmldoc = new DOMDocument();
		$xmldoc->load($path);
		$elements = $xmldoc->getElementsByTagName("file");

		$xpath = new DOMXPath($xmldoc);

		foreach($elements as $elements){
			$file_path = $elements->getAttribute('file_path');
			$date_modified = $elements->textContent;
			$data[$file_path] = $date_modified;
		}

		return $data;

		return false;
 	}

	function find_all_files($dir){
		$root = scandir($dir);
		foreach($root as $value){
			if($value === '.' || $value === '..'){
				continue;
			}

			if(is_file("$dir".DIRECTORY_SEPARATOR."$value")) {
				$result[]="$dir".DIRECTORY_SEPARATOR."$value";
				continue;
			}

			foreach(find_all_files("$dir".DIRECTORY_SEPARATOR."$value") as $value){
				$result[]=$value;
			}
		}
		return $result;
	}

	function  get_file_name($scandir,$ext){

		foreach($scandir as $file_key => $file_name){

			$file_tester = preg_match('/\.'.$ext.'$/i', $file_name);

			if($file_tester == 1){
				return $file_name;
			}
		}

		#return false if no file matches
		return false;
	}
	
	#================================================================================

	#update activity statistic
	$cron_option['cron_name'] = 'CRON_UPLOAD_FILES';
	cron_activity_log($cron_option);
?>