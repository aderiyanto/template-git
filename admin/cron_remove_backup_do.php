<?php 
	#this file only allow to execute by using cron or cli command
	if(!defined('STDIN') ){
		die('Forbidden Access');
	}

	//includes all files necessary to support operations
	$basepath = realpath(dirname(__file__).'/..');
	include($basepath."/modz/config-main.php");
	include($basepath."/modz/config.php");
	include($basepath."/modz/license.php");
	include($basepath."/modz/errormsg.php");
	include($basepath."/modz/mainmod.php");
	include($basepath."/modz/connic.php");
	include($basepath."/modz/getall-admin.php");

	#This file will access data from database and send it as email according to cron schedule
	$sql = "SELECT * FROM backup_activity WHERE backactStatus='success'";
	$query = query($sql);
	$numofdata = rows($query);

	if ($numofdata>0){
		while($data = fetch($query)){
			if(file_exists("../assets/backup/{$data['backactFileName']}.zip")){
				unlink("../assets/backup/{$data['backactFileName']}.wb");
				unlink("../assets/backup/{$data['backactFileName']}.zip");
				unlink("../assets/backup/{$data['backactFileName']}.sql");
			}
			$sql2 = "DELETE FROM backup_activity WHERE backactId='{$data['backactId']}'";
			$query2 = query($sql2);
		}
	}
 
	//update activity statistic
	$cron_option['cron_name'] = 'CRON_REMOVE_BACKUP_FILE'; 
	cron_activity_log($cron_option);
?>