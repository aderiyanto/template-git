<?php
	##################################################################
	# Description:
	# This file will check whether a user has logged in
	# It is included in all protected files
	##################################################################
	
	//make sure all are NOT empty
	if (empty($cook_username) || empty($cook_id) || empty($cook_cp) || empty($cook_gid) || empty($cook_lastlog)){
		//ask user to login
		header("location:logout.php");
		exit();
	}
	
	//when all cookies are NOT empty
	//make sure the content of each cookie is valid
	//we can check against the checkpoint
	$checkcookie="[[[[CM||S" . $cook_username . "Au0m4t3&F44STENProc" . $cook_id . "6..009>>>" . $cook_gid . "..]]]]]]" . SITE_NAME;
	$checkcookie=md5($checkcookie);
		
	//if both value are NOT the same, send user to logout.php page
	if (!($checkcookie===$cook_cp)){
		header("location:logout.php");
		exit();
	}

	if (!defined("WEBBY_EXEC")) { define('WEBBY_EXEC', true); }
?>