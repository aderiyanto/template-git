
<?php
    $page=40;
    //includes all files necessary to support operations
    include("../modz/config-main.php");
    include("../modz/config.php");
    include("../modz/license.php");
    include("../modz/mainmod.php");
    include("../modz/connic.php");
    include("../modz/getall-admin.php");
    include("../modz/errormsg.php");
    include("authuser.php");
    include("../modz/mainmod-extend.php");

    if ($form=="submit"){

        if ($action=="add"){
            if(empty($message) or empty($senderid)){
                $error=errorlist(2);
            }

            if($getbalance < 275 or empty($getbalance)){
                $error=errorlist(66);
            }

            if(empty($gr) and empty($cn)){
                $error=errorlist(2);
            }

            if($option!="now"){
                if(empty($datesend) or empty($time)){
                    $error=errorlist(2);
                }
            }

            if(!empty($scheduletype)){
                #sms schedule section
                if($scheduletype == 'e_date' and empty($s_mul)){
                     $error=errorlist(2);
                } elseif($scheduletype == 'e_day' and count(array_filter($s_day)) == 0){
                     $error=errorlist(2);
                } elseif($scheduletype == 'e_month' and count(array_filter($s_date)) == 0){
                     $error=errorlist(2);
                } elseif($scheduletype == 'e_year' and (empty($cdate) or empty($cmon))){
                     $error=errorlist(2);
                }


            }




            if(!$error){
                //change message var
                $msg = $message;
                if(empty($scheduletype)){
                    if($option=="now"){
                        $day    = date('d', $now);
                        $month  = date('m', $now);
                        $year   = date('Y', $now);
                        $date   = date('Y-m-d', $now);
                        $time   = date('H:i:s', $now);
                        $timestamp = $now;
                    }else{
                        $listdate = explode('-', $datesend);
                        $day    = $listdate[0];
                        $month  = $listdate[1];
                        $year   = $listdate[2];
                        $date   = $year."-".$month."-".$day;
                        $time   = $time;
                        $timestamp = strtotime($date." $time");
                    }

                }else{
                    $baseTimeStamp = ($startdate) ? strtotime($startdate) : strtotime(date('Y-m-d'));
                    list($h, $m) = explode(':', $schintime);
                    $sendOn = ((int) $h * 3600) + ((int) $m * 60);
                    $startSendOn = strtotime($startdatesend." $starttime");
                    $endSendOn = strtotime($enddatesend." $endtime");
                    if($scheduletype == 'e_date'){
                        $schType = 'multiple';
                        $schRules = $s_mul;
                        $schNextSent = strtotime("+$s_mul days", $baseTimeStamp);
                    }else if($scheduletype == 'e_day'){
                        $schType = 'day';
                        ksort($s_day);
                        $schRules = implode(',', array_filter($s_day));

                        if(in_array(date('l', $baseTimeStamp), $s_day) and ($baseTimeStamp + $sendOn) > $now){
                            $schNextSent = $baseTimeStamp;
                        } else {
                            foreach($s_day as $key => $value){
                                $nextDay[] = strtotime('next '.$value, $baseTimeStamp);
                            }

                            $schNextSent = min($nextDay);
                        }
                    }else if($scheduletype == 'e_month'){
                        $schType = 'month';
                        ksort($s_date);
                        $schRules = implode(',', array_filter($s_date));

                        if(in_array(date('j', $baseTimeStamp), $s_date) and ($baseTimeStamp + $sendOn) > $now){
                            $schNextSent = $baseTimeStamp;
                        } else {
                            $result = false;
                            foreach($s_date as $key => $value){
                                if($value > date('j', $baseTimeStamp)){
                                    $nextDate = $value;
                                    $result = true;
                                    break;
                                }
                            }

                            if($result == true){
                                $schNextSent = strtotime("+".($nextDate - date('j', $baseTimeStamp))." days", $baseTimeStamp);
                            } else {
                                $nextDate = min($s_date);
                                $schNextSent =  strtotime("$nextDate ".date('F Y', strtotime('+1 month', $baseTimeStamp)));
                            }
                        }
                    }else if($scheduletype == 'e_year'){
                        $schType = 'year';
                        $schRules = $cdate.' '.$cmon;
                        if(strtotime("$cdate $cmon") < $baseTimeStamp){
                            $year = date('Y', $baseTimeStamp) + 1;
                        } else {
                            $year = date('Y', $baseTimeStamp);
                        }

                        $schNextSent = strtotime("$cdate $cmon $year");
                    }
                    $schNextSent += $sendOn;

                }

                if(count($cn) > 0){
                    $valuecn = array();
                    $value_sch = array();
                    foreach($cn as $c){
                        $contact = getval('conFullName,conHP,conEmail','contacts_sms','conId='.$c);
                        $mobile = $contact['conHP'];
                        $fullname = $contact['conFullName'];
                        $email = $contact['conEmail'];
                        include "../admin/sms_varguide.php";

                        $message = cleanup($message);
                        $hp[]=$mobile;


                       $exist2 = checkDuplicateRecord('sms_schedule', "schTo='$mobile' and schText='$message'");
                       if($exist2 == 0){
                            $value_sch[] = "('$senderid','$mobile','$message','$now','$schType','$schRules','$schintime','$startSendOn','$endSendOn','0','$schNextSent','active')";
                        }

                    }
                }
                if(count($gr) > 0){
                    $valuegr = array();
                    $value_sch_gr = array();
                    foreach($gr as $g){
                        $sqlg = "SELECT * FROM groups_contacts_sms WHERE groupId='$g'";
                        $queryg = query($sqlg);
                        while($datag = fetch($queryg)){
                            $contact = getval('conFullName,conHP,conEmail','contacts_sms','conId='.$datag['conId']);
                            $mobile = $contact['conHP'];
                            $fullname = $contact['conFullName'];
                            $email = $contact['conEmail'];
                            include "../admin/sms_varguide.php";
                            $message = cleanup($message);
                            $hp[]=$mobile;

                            $exist2 = checkDuplicateRecord('sms_schedule', "schTo='$mobile' and schText='$message'");

                            if($exist2 == 0){
                                $value_sch_gr[] = "('$senderid','$mobile','$message','$now','$schType','$schRules','$schintime','$startSendOn','$endSendOn','0','$schNextSent','active')";
                            }

                        }
                    }
                }
                if(empty($scheduletype)){
                  if($option == "now"){
                    foreach ($hp as $k => $v) {
                      $send = send_sms($v,$message,"queue");
                    }
                 }else{
                   foreach ($hp as $k => $v) {
                     $existsms = checkDuplicateRecord('sms', "smsTo='$v' and smsText='$message' and smsDate='$date'");
                     if ($existsms==0){
                     $query=query("INSERT INTO sms(smsTo, smsText, smsDay, smsMonth, smsYear, smsDate, smsTime,smsTimetime,smsTimeSent,smsStatus) VALUES ('$v', '$message', '$day', '$month', '$year', '$date', '$time','$now','$timestamp','new')");
                     }
                   }
                 }
                 print "<script>window.location='sms_log.php'</script>";
                }else{
                    $implode_sch = implode(',', $value_sch);
                    $implode_sch_gr = implode(',', $value_sch_gr);

                    if(!empty($implode_sch)){
                        $sql = "INSERT INTO sms_schedule(smsSenderId,schTo,schText,schAddedOn,schType,schRules,schTime,schStartDateOn,schEndDateOn,schLastSent,schNextSend,schStatus) VALUES $implode_sch";
                        $query = query($sql);
                    }

                    if(!empty($implode_sch_gr)){
                        $sql = "INSERT INTO sms_schedule (smsSenderId,schTo,schText,schAddedOn,schType,schRules,schTime,schStartDateOn,schEndDateOn,schLastSent,schNextSend,schStatus) VALUES $implode_sch_gr";
                        $query = query($sql);
                    }

                    print "<script>window.location='sms_schedule.php'</script>";
                }

            }

        }
        if(!$query){
            $error=errorlist(3);
        }

        if($error){
            print "<p>";
            print "<ul>";
            print nl2br($error);
            print "</ul>";
            print "</p>";
        }else{
            if(!$js){
                header("location:sms_log.php");
            }else{
                print "ok";
            }
        }
        exit;
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print _('sms_advanced_page_title'); ?> - <?php print _('area'); ?> - <?php print SITE_NAME; ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="/libs/bootstrap-datepicker/css/datepicker3.css" />
<link rel="stylesheet" href="/libs/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" type="text/css" href="/style/bootstrap-clockpicker.min.css" />
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">
<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>

<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="/libs/moment/min/moment-with-locales.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="/javascript/bootstrap-clockpicker.min.js"></script>

<script type="text/javascript">
jQuery(document).ready(function($){
    $('#result').hide();
    validate('#result','#add','');

    /*$(".datetimepicker").datetimepicker({
        format: 'DD-MM-YYYY',
        locale: 'id'
    });*/

    $(".datepickerdate").datepicker({
        startDate: '<?php echo date('d') ?>-<?php echo date('M') ?>-<?php echo date('Y') ?>',
        format: 'dd-mm-yyyy',
        todayHighlight: true,
        toggleActive: true,
        autoclose: true
    });



    $('.clockpicker').clockpicker()
        .find('input').change(function(){
            console.log(this.value);
    });

    $('input[type=radio][name=option]').change(function() {
        if (this.value == 'tanggal') {
            $('#showdate').show();
        }else if(this.value == 'now'){
             $('#showdate').hide();
        }

    });

    $('#scheduletype').change(function(){
            x=$(this).val();

            if(x=="e_date"){
                $('#e_date').show();
                $('#star_date').show();
                $('#s_now').hide();
                $('#e_year').hide();
            }else if(x=="e_day"){
                $('#e_day').show();
                $('#e_date').hide();
                $('#star_date').show();
                $('#e_year').hide();
                $('#s_now').hide();
                $('#e_month').hide();
            }else if(x=="e_month"){
                $('#e_day').hide();
                $('#e_date').hide();
                $('#e_month').show();
                $('#e_year').hide();
                $('#star_date').show();
                $('#s_now').hide();
            }else if(x=="e_year"){
                $('#e_day').hide();
                $('#e_date').hide();
                $('#e_month').hide();
                $('#e_year').show();
                $('#star_date').show();
                $('#s_now').hide();
            }else if(x==""){
                $('#e_date').hide();
                $('#e_day').hide();
                $('#e_year').hide();
                $('#star_date').hide();
                $('#e_month').hide();
                $('#s_now').show();
            }
        });
});
function countChar(val) {
    var len = val.value.length;
    var jumsms = Math.ceil(len/160);
    $('#charNum').text(len + ' Karakter');
    $('#countSMS').text(jumsms + ' SMS');

}
function myFunction() {
    // Declare variables
    var input, filter, ul, li, a, i;
    input = document.getElementById('myInput');
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    li = ul.getElementsByTagName('li');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("label")[0];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

function SearchContact() {
    // Declare variables
    var input, filter, ul, li, a, i;
    input = document.getElementById('InputContact');
    filter = input.value.toUpperCase();
    ul = document.getElementById("UlContact");
    li = ul.getElementsByTagName('li');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("label")[0];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}
</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="container body">
    <div class="main_container">

        <div class="col-md-3 left_col menu_fixed">
            <div class="left_col scroll-view">
                <!-- Header Menu -->
                <?php require("com/com-header-menu.php"); ?>
                <!-- /Header Menu -->
                <br />
                <!-- Main Menu -->
                <?php include("com/com-menu.php"); ?>
                <!-- /Main Menu -->
            </div>
            <!-- END .left_col .scroll-view -->
        </div>
        <!-- END .col-md-3 .left_col .menu_fixed -->

        <!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">
            <?php
            include "com/com-getbalance.php";
            ?>
            <div class="row">
                <div class="col-md-12">
                    <h3><?php print _('sms_advanced_page_title'); ?></h3>
                    <?php print "Balance: ".$balance['currency']." ".number_format($balance['balance']); ?>
                    <hr>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?php
                    if ($action==""){
                    ?>
                    <form action="sms_advanced.php?form=submit&amp;action=add" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">
                        <input type="hidden" name="getbalance" id="getbalance" value="<?php print $balance['balance']; ?>">
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
                                <?php print _('sms_from'); ?><span class="required">*</span>
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <select class="form-control" name="senderid" required>
                                    <option value="<?php print getconfig('SMS_API_FROM'); ?>"><?php print getconfig('SMS_API_FROM'); ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
                                Group
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search Group Name.." class="form-control" style="margin-bottom:5px;">
                                <div style="height: 200px; overflow: auto; width: 100%; border: 1px solid #c4c4c4; padding: 5px; padding-left: 10px">
                                    <ul id="myUL" style="list-style-type: none;padding: 0;margin: 0;">
                                      <?php
                                        $sqlx="SELECT * FROM groups_sms  ORDER BY groupName ASC";
                                        $queryx=query($sqlx);
                                        while ($datax=fetch($queryx)){
                                            $datax=output($datax);
                                            $sqlc = "SELECT COUNT(*) AS total FROM groups_contacts_sms WHERE groupId='$datax[groupId]'";
                                            $queryc = query($sqlc);
                                            $datac = fetch($queryc);
                                        ?>
                                        <li>
                                          <label class="checkbox-inline">
                                               <input type="checkbox" name="gr[]" value="<?php print $datax['groupId']; ?>" id="group_<?php print $datax['groupId']; ?>" /> <?php print $datax['groupName']; ?>
                                          </label>
                                         <div style="display:inline;margin-right:5px;" class="pull-right"><label class="checkbox-inline"><?php print number_format($datac['total'],0,',',','); ?></label></div>
                                        </li>
                                       <?php
                                        }
                                        ?>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
                                Contact
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <input type="text" id="InputContact" onkeyup="SearchContact()" placeholder="Search Name.." class="form-control" style="margin-bottom:5px;">
                                <div style="height: 200px; overflow: auto; width: 100%; border: 1px solid #c4c4c4; padding: 5px; padding-left: 10px">
                                    <ul id="UlContact" style="list-style-type: none;padding: 0;margin: 0;">
                                      <?php
                                        $sqlx="SELECT A.* FROM contacts_sms A ORDER BY conFullName ASC";
                                        $queryx=query($sqlx);
                                        while ($datax=fetch($queryx)){
                                            $datax=output($datax);
                                        ?>
                                        <li>
                                          <label class="checkbox-inline">
                                               <input type="checkbox" name="cn[]" value="<?php print $datax['conId']; ?>" id="group_<?php print $datax['conId']; ?>" /> <?php print ucwords($datax['conFullName']); ?>
                                          </label>
                                          <div style="display:inline;margin-right:5px;" class="pull-right"><label class="checkbox-inline"><?php print $datax['conHP']; ?></label></div>
                                        </li>
                                       <?php
                                        }
                                        ?>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="mCatPicLogo">
                                <?php print _('sms_use_template'); ?>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="list-template-box">
                                    <ul class="list-template">
                                        <?php
                                        $sqltemp ="SELECT * FROM template_sms ORDER BY tempName ASC";
                                        $querytemp = query($sqltemp);
                                        while($datatemp =fetch($querytemp)){
                                            ?>
                                            <li style="list-style:none;margin-left:-30px"><i class="fa fa-angle-right"></i>&nbsp;<a href="javascript:;" onclick="document.add.textd.value='<?php print stripquote(ereg_replace("\r\n"," ",$datatemp['tempTemplate'])); ?>'; document.add.textd.focus();"><?php print $datatemp['tempName']; ?></a></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="mCatPicLogo">
                                <?php print _('sms_text'); ?> <span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <textarea class="form-control" cols="10" rows="10" name="message" id="textd" onkeyup="countChar(this)" onfocus="countChar(this)" required></textarea>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 col-sm-6 col-lg-6">
                                        <div id="charNum" class="help-block" style="font-weight: bold;">0 Karakter</div>
                                    </div>
                                    <div class="col-md-6 col-xs-6 col-sm-6 col-lg-6">
                                        <div id="countSMS" class="help-block text-right" style="font-weight: bold;">0 SMS</div>
                                    </div>
                                </div>
                                <a href="#modal-smsvarguide" role="button" class="btn btn-success" data-toggle="modal"><i class="fa fa-list"></i> SMS Variable Guide</a>
                            </div>
                        </div>
                        <br>
                        <div class="form-group" id="s_now">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
                                <?php print _('sms_datetime'); ?>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                               <div class="radio">
                                     <label><input type="radio" name="option" value="now" id="optionfilter" checked>Send Now</label>
                               </div>
                                <div class="radio">
                                       <label><input type="radio" name="option" value="tanggal" id="optionfilter">Date</label>
                               </div><br>
                               <div id="showdate" style="display:none;">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group input-group">
                                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                                <input name="datesend"  type="text" class="datepickerdate form-control" aria-describedby="basic-addon1" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-group clockpicker pull-right" data-placement="top" data-align="top" data-autoclose="true">
                                                <input type="text" name="time" class="form-control" readonly>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                           </div>
                                        </div>
                                    </div>

                               </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
                                <?php print _('sms_schedule'); ?>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <select class="form-control" name="scheduletype"  id="scheduletype" style="width:250px;">
                                     <option value="">- <?php print _('sms_schedule_type'); ?> -</option>
                                     <option value="e_date"><?php print _('sms_schedule_selected_date'); ?></option>
                                     <option value="e_day"><?php print _('sms_schedule_every_day'); ?></option>
                                     <option value="e_month"><?php print _('sms_schedule_every_month'); ?></option>
                                     <option value="e_year"><?php print _('sms_schedule_every_year'); ?></option>
                                </select><br>

                                <div class="form-group"  id="e_date" style="display:none;">
                                    <div class="row">
                                        <div class="col-md-2"><label>Setiap</label></div>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" name="s_mul">
                                        </div>
                                        <div class="col-md-6"> <label style="display:inline;">Hari</label></div>
                                    </div>
                                </div>
                                <div class="form-group"  id="e_day" style="display:none;">
                                    <label>Select Day</label><br>
                                    <label class="checkbox-inline"><input type="checkbox" name="s_day[0]" value="Sunday" />Sunday</label>
                                    <label class="checkbox-inline"><input type="checkbox" name="s_day[1]" value="Monday" />Monday</label>
                                    <label class="checkbox-inline"><input type="checkbox" name="s_day[2]" value="Tuesday" />Tuesday</label>
                                    <label class="checkbox-inline"><input type="checkbox" name="s_day[3]" value="Wednesday" />Wednesday</label><br>
                                    <label class="checkbox-inline"><input type="checkbox" name="s_day[4]" value="Thursday" />Thursday</label>
                                    <label class="checkbox-inline"><input type="checkbox" name="s_day[5]" value="Friday" />Friday</label>
                                    <label class="checkbox-inline"><input type="checkbox" name="s_day[6]" value="Saturday" />Saturday</label>
                                </div>
                                <div class="form-group"  id="e_month" style="display:none;">
                                    <label>Select Date</label><br>
                                     <?php
                                     for ($i=1; $i<=28; $i++) {
                                        if ($i<10) { $i="0$i"; }
                                        ?>
                                    <label class="checkbox-inline">

                                    <input type="checkbox" name="s_date[<?php print (int) $i;?>]" value="<?php echo (int) $i; ?>" style="margin-right:-2px"/> <?php echo"$i"; ?></label>
                                     <?php
                                    } ?>
                                </div>
                                <div class="row" id="e_year" style="display:none;">
                                    <div class="col-md-6">
                                        <div class="form-group" >
                                            <label>Select Date</label>
                                            <select name="cdate" class="form-control">
                                               <option value="">Date</option>
                                               <?php for ($i=1; $i<=28; $i++) { ?>
                                               <option value="<?php print $i; ?>"><?php print $i; ?></option>
                                             <?php } ?>
                                             </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" >
                                            <label>Month</label>
                                            <select name="cmon" class="form-control">
                                                 <option value="">Month</option>
                                                 <?php for ($i=1; $i<=12; $i++) { ?>
                                                 <option value="<?php convertidtomonthEng($i); ?>"><?php print convertidtomonthEng($i); ?></option>
                                                 <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div id="star_date" style="display:none;">
                                    <div class="form-group" style="width:150px;">
                                        <label>Time</label>
                                        <select name="schintime" id="schintime" class="form-control">
                                            <?php for ($i=0;$i<=23;$i++){
                                                $h=($i<10 ?"0":"");
                                                $h.=$i;
                                                ?>
                                            <option value="<?php print ($i<10?"0":""); ?><?php print $i; ?>:00"<?php if($sendingHour==$h and $sendingMinute=='00'){ print ' selected="selected"';}?>><?php print ($i<10)?"0":""; ?><?php print $i; ?>:00</option>
                                            <?php
                                                if($sendingHour==$h and $sendingMinute!='00'){
                                                    print  '<option value="'.$sendingTime.'" selected="selected">'.$sendingTime.'</option>';
                                                }
                                            } ?>
                                          </select>
                                    </div>
                                    <div class="form-group" >
                                        <div class="row">
                                            <div class="col-md-6">
                                               <label>Start Date</label>
                                                <div class="form-group input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                                    <input name="startdatesend"  type="text" class="datepickerdate form-control" aria-describedby="basic-addon1" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Time</label>
                                                <select name="starttime" id="starttime" class="form-control">
                                                       <?php for ($i=0;$i<=23;$i++){
                                                        $h=($i<10 ?"0":"");
                                                        $h.=$i;
                                                        ?>
                                                       <option value="<?php print ($i<10?"0":""); ?><?php print $i; ?>:00"<?php if($sendingHour==$h and $sendingMinute=='00'){ print ' selected="selected"';}?>><?php print ($i<10)?"0":""; ?><?php print $i; ?>:00</option>
                                                       <?php
                                                        if($sendingHour==$h and $sendingMinute!='00'){
                                                            print  '<option value="'.$sendingTime.'" selected="selected">'.$sendingTime.'</option>';
                                                        }
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" >
                                    <div class="row">
                                        <div class="col-md-6">
                                           <label>End Date</label>
                                            <div class="form-group input-group">
                                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                                <input name="enddatesend"  type="text" class="datepickerdate form-control" aria-describedby="basic-addon1" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Time</label>
                                            <select name="endtime" id="endtime" class="form-control">
                                                   <?php for ($i=0;$i<=23;$i++){
                                                    $h=($i<10 ?"0":"");
                                                    $h.=$i;
                                                    ?>
                                                   <option value="<?php print ($i<10?"0":""); ?><?php print $i; ?>:00"<?php if($sendingHour==$h and $sendingMinute=='00'){ print ' selected="selected"';}?>><?php print ($i<10)?"0":""; ?><?php print $i; ?>:00</option>
                                                   <?php
                                                    if($sendingHour==$h and $sendingMinute!='00'){
                                                        print  '<option value="'.$sendingTime.'" selected="selected">'.$sendingTime.'</option>';
                                                    }
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <hr/>

                        <div class="form-group">
                            <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <button name="Submit" type="submit" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> <?php print _('sms_add'); ?></button>
                                <button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
                            </div>
                        </div>
                    </form>

                    <div class="modal fade" id="modal-smsvarguide" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        ×
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">
                                        SMS Variable Guide
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table>
                                                <tr>
                                                    <td style="width:150px;"><b>{MOBILE}</b></td>
                                                    <td style="width:250px;">Replaced with contact's phone number</td>
                                                </tr>
                                                <tr>
                                                    <td style="width:150px;"><b>{FULLNAME}</b></td>
                                                    <td style="width:250px;">Replaced with name</td>
                                                </tr>
                                                <tr>
                                                    <td style="width:150px;"><b>{EMAIL}</b></td>
                                                    <td style="width:250px;">Replaced with email</td>
                                                </tr>
                                            </table>
                                            <br>
                                            <strong>Example:</strong><br>
                                            Dear Sir/Madam {FULLNAME}. We have received your message and we will reply as soon as possible to {MOBILE}.<br><br>
                                            <strong>Recipient will receive SMS as:</strong><br>
                                            Dear Sir/Madam John Doe. We have received your message and we will reply as soon as possible to +6281902188999.<br><br>
                                            <strong style="color:red;">WARNING:</strong><br>
                                            <span style="color:red;">If any contact do NOT have the data, it will appear as BLANK</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                        Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <?php include("com/com-footer.php"); ?>
    </div>
    <!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>

</body>
</html>
