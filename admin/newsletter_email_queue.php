<?php
	$page=33;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/mainmod-extend.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

	if($form=="submit"){
		if($action=="edit" and $uac_edit){
			//Check whether id exist
			if($emptyid){
				$error=$emptyid;
			}else{
				if(empty($emailsubject) or empty($emailmsg)){
					$error=errorlist(2);
				}else{
					// check whether email exist
					$isexist=countdata("newsletter_email_queue","emailMsg='$emailmsg' AND emailTo='$emailto' AND  emailId!='". (int)$id ."'");
					if($isexist>0){
						$error=errorlist(39);
					}else{
						$sql="UPDATE newsletter_email_queue SET emailSubject='$emailsubject',emailMsg='$emailmsg',emailMsgHtml='$emailmsg',emailHead='$emailhead' WHERE emailId='". (int)$id ."'";
						$query=query($sql);
					}
				}
			}
		}elseif($action=="del" and $uac_delete){
			//if del all
			if($id=="all"){
				$sql="DELETE FROM newsletter_email_queue";
				$query=query($sql);
			}else{
				//Check whether id exist
				if($emptyid){
					$error=$emptyid;
				}else{
					$sql="DELETE FROM newsletter_email_queue WHERE emailId='". (int)$id ."'";
					$query=query($sql);
				}
			}
		}elseif($action=="queuehide" and $uac_edit){
			$sql="UPDATE newsletter_email_queue SET emailStatus='s' WHERE emailId='". (int)$id ."'";
			$query=query($sql);
		}elseif($action=="queueshow" and $uac_edit){
			$sql="UPDATE newsletter_email_queue SET emailStatus='n' WHERE emailId='". (int)$id ."'";
			$query=query($sql);
		}

		//check whether query was successful
		if(!$query){
			$error=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:newsletter_email_queue.php");
			}else{
				print "ok";
			}
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?>- CMS - <?php print _('pagecontent_pagetitle'); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>

<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>
<!-- jQuery Tags Input -->
<script type="text/javascript" src="/libs/jquery.tagsinput/src/jquery.tagsinput.js"></script>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('.fancybox-frame').fancybox({
		'type'			: 'iframe'
	});
	$('#result').hide();
	$('#keywordseo').tagsInput({
		width: 'auto'
	});
    validate('#result','#edit','newsletter_email_queue.php');
});
</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
    <div class="main_container">
        
        <div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->
        
        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h3><?php print _('newsletter_queue_pagetitle'); ?></h3>
                    <p><?php print _('newsletter_queue_pagedesc'); ?></p>
                    
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12" style="margin-bottom: 10px;"></div>
                        <div class="col-md-6 col-sm-6 col-xs-12 text-right" style="margin-bottom: 10px;display: inline-block;">
                            <?php
                            $countqueue=countdata("newsletter_email_queue","emailId");
                            $sent=countdata("newsletter_email_queue","emailStatus='y'");
                            $pending=countdata("newsletter_email_queue","emailStatus='n'");

                            if ($uac_delete and ($countqueue>0)){ ?>
                            <a href="javascript:if (window.confirm('<?php print _("newsletter_queue_askdelete"); ?>')){ window.location='newsletter_email_queue.php?form=submit&amp;action=del&amp;id=all'; };" class="btn btn-primary"><i class="fa fa-close"></i> <?php print _("newsletter_queue_deleteall"); ?></a>
                            <?php
                                }
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_content">
                                <?php
                                if (empty($action)){?>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                                      <p>
                                      <?php
                                          print "<strong>"._('newsletter_view_emailtotal')."</strong> " . number_format($countqueue,0,",",".") . " | ";
                                          print "<strong>"._('newsletter_view_emailsend')."</strong> " . number_format($sent,0,",",".") . " | ";
                                          print "<strong>"._('newsletter_view_emailpending')."</strong> " . number_format($pending,0,",",".");
                                        ?>  
                                      </p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="margin-bottom: 10px;display: inline-block;">
                                        <div class="pull-right">
                                            <form  class="form-inline"  action="newsletter_email_queue.php" method="get">
                                                <div class="form-group">
                                                    <select name="filter" class="form-control input-sm">
                                                        <option value=""><?php print _("newsletter_queue_alltitle"); ?></option>
                                                        <?php
                                                        $sqlf	="SELECT * FROM newsletter_email_queue GROUP BY emailSubject";
                                                        $queryf	=query($sqlf);
                                                        while($dataf=fetch($queryf)){
                                                              $dataf=output($dataf);?>
                                                        <option value="<?php print $dataf['emailSubject'] ?>" <?php print $dataf['emailSubject']==$filter ? 'selected="selected"': ''; ?>><?php print $dataf['emailSubject'] ?></option>
                                                        <?php
                                                        }?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <input name="search"  value="<?php print $search ?>" size="40"  class="form-control input-sm" />
                                                </div>
                                                <button type="submit" name="cari"  class="btn btn-sm btn-dark"><?php print _("newsletter_queue_search"); ?></button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                      <thead>
                                        <tr>
                                          <th class="text-center" style="width: 3%;"><?php print _("cms_no"); ?></th>
                                          <th class="text-center"><?php print _("newsletter_view_email"); ?></th>
                                          <th class="text-center"><?php print _("newsletter_view_title"); ?></th>
                                          <th class="text-center"></th>
                                          <th class="text-center" style="width: 250px;"><?php print _("newletter_time"); ?></th>
                                          <th class="text-center" style="width: 40px;"><i class="fa fa-cog"></i></th>
                                          <th class="text-center" style="width: 40px;"><i class="fa fa-cog"></i></th>
                                          <th class="text-center" style="width: 40px;"><i class="fa fa-cog"></i></th>
                                          <th class="text-center" style="width: 40px;"><i class="fa fa-cog"></i></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <?php
                                            if (empty($pg)){
                                                $pg=1;
                                            }

                                            $filter ? $wheretitle=" AND  emailSubject LIKE '$filter' ": '';
                                            $search ? $whereclause=" AND  emailTo LIKE '%$search%' ": '';

                                            $sql="SELECT * FROM newsletter_email_queue WHERE emailId IS NOT NULL $wheretitle $whereclause ORDER BY emailStatus ASC, emailId DESC";
                                            $query=query($sql);
                                            $numofdata=rows($query);

                                            $num=1;
                                            $numPerPage=20;
                                            $offset=($pg-1)*$numPerPage;
                                            $num=$num+(($pg-1)*$numPerPage);

                                            $sql.=' LIMIT '.$offset.','.$numPerPage;
                                            $query=query($sql);
                                            if ($numofdata<1){
                                        ?>
                                          <tr>
                                            <td colspan="9" class="asterik"><div class="text-center"><br />
                                            <?php print _("newsletter_queue_notavailable"); ?><br />
                                            <br /></div></td>
                                          </tr>
                                        <?php
                                            }else{
                                                while($data=fetch($query)){
                                                if ($data['emailStatus']=="s"){
                                                    $btnDisplay="<a href=\"javascript:if (window.confirm('"._('newsletter_show').": ".$data['emailTo']."')){window.location='newsletter_email_queue.php?form=submit&amp;action=queueshow&amp;id=" .$data['emailId'] ."'};\" title=\""._('newsletter_view_enable').": " .$data['emailTo'] ."\"  data-toggle='tooltip' data-placement='top'><i class='fa fa-minus-circle' style='color:#cc0000;' ></i></a>";
                                                }elseif($data['emailStatus']=="n"){
                                                    $btnDisplay="<a href=\"javascript:if (window.confirm('"._('newsletter_stop').": ".$data['emailTo']."')){window.location='newsletter_email_queue.php?form=submit&amp;action=queuehide&amp;id=" .$data['emailId'] ."'};\" title=\""._('newsletter_view_pending').": " .$data['emailTo'] ."\" data-toggle='tooltip' data-placement='top'><i class='fa fa-check'></i></a>";
                                                }
                                        ?>
                                                <tr>
                                                  <td class="text-center"><?php print $num; ?></td>
                                                  <td><?php print $data['emailTo']; ?></td>
                                                  <td>ini dia jarjit sing</td>
                                                  <td class="text-center"><?php if ($data['emailStatus']=="n"){ print "<b class='text-warning'>Pending</b>"; }elseif($data['emailStatus']=="s"){ print "<b class='text-info'>Stop</b>";}else{ print "<b class='text-success'>Sent</b>"; } ?></td>
                                                  <td class="text-center"><?php print dateformat($data['emailDate'],"full"); ?><?php //print date("d/m/Y H:i:s",$data['emailDate']); ?></td>
                                                  <td class="text-center">
                                                      <a href="email_readmore.php?action=readmore&amp;source=newsletter&amp;id=<?php print $data['emailId']; ?>" class="fancybox-frame" data-toggle="tooltip"  title="<?php print _("cms_detail"); ?>: <?php print $data['emailTo']; ?>"><i class="fa fa-table"></i></a>
                                                    </td>
                                                  <td class="text-center">
                                                      <?php if ($uac_edit and ($data['emailStatus'] == 'n' or $data['emailStatus'] == 's')){  print $btnDisplay; } ?>
                                                   </td>
                                                  <td class="text-center">
                                                      <?php if ($uac_edit and ($data['emailStatus'] == 'n' or $data['emailStatus'] == 's')){ ?><a href="newsletter_email_queue.php?action=edit&amp;id=<?php print $data['emailId']; ?>" title="<?php print _("cms_edit"); ?>: <?php print $data['emailTo']; ?>" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a><?php } ?>
                                                  </td>
                                                  <td class="text-center">
                                                      <?php if ($uac_delete){ ?><a class="delete" href="newsletter_email_queue.php?form=submit&amp;action=del&amp;id=<?php print $data['emailId']; ?>" title="<?php print $data['emailTo']; ?>"><i class="fa fa-trash" title="<?php print _('cms_delete'); ?>: <?php print $data['emailTo']; ?>" rel="tooltip"></i></a><?php } ?>
                                                    
                                                  </td>
                                                </tr>
                                                <?php
                                                    $num++;
                                                        }
                                                    }
                                                ?>
                                      </tbody>
                                    </table>
                                    <?php
                                        $options['total']=$numofdata;
                                        $options['filename']='newsletter_email_queue.php';
                                        $options['qualifier']='newsletter_email_queue.php';
                                        $options['pg']=$pg;
                                        $options['numPerPage']=$numPerPage;
                                        $options['style']=1;
                                        $options['addquery']=TRUE;
                                        paging($options);
                                    ?>
                                </div>
                                <?php
                                }elseif ($action=="edit" and $uac_edit){
                                    $sql="SELECT * FROM newsletter_email_queue WHERE emailId='".(int)$id."'";
                                    $query=query($sql);
                                    $data=fetch($query);
                                ?>
                                <a href="newsletter_email_queue.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
                                <h4><?php print _("newsletter_queue_titleedit"); ?></h4>
                                <hr/>
                                <form action="newsletter_email_queue.php?form=submit&amp;action=edit&amp;id=<?php print $data['emailId']; ?>" method="post" enctype="multipart/form-data" name="edit" id="edit" class="form-horizontal form-label-left">
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="email"><?php print _("newsletter_view_email"); ?>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top:8px;">
                                           <?php print $data['emailTo']; ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="emailsubject"><?php print _("newsletter_view_title"); ?><span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                           <input name="emailsubject" type="text" id="emailsubject" maxlength="255" value="<?php print $data['emailSubject']; ?>"  required class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="emailhead"><?php print _("newsletter_queue_emailhead"); ?>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea name="emailhead" id="emailhead"  class="form-control col-md-7 col-xs-12" cols="50" rows="5"><?php print $data['emailHead']; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="email"><?php print _("newsletter_queue_content"); ?><span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea name="emailmsg" id="emailmsg"  class="tinymce form-control col-md-7 col-xs-12" required><?php print $data['emailMsg']; ?></textarea>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="form-group">
                                        <div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <button name="Submit" type="submit" class="btn btn-primary btn-sm" value="Submit"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
                                            <button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='newsletter_email_queue.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
                                            <button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
                                        </div>
                                    </div>
                                </form>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
        	</div>
        </div>
        
        <?php include("com/com-footer.php"); ?>
    </div>
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>