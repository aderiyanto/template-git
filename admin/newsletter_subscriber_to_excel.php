<?php
//includes all files necessary to support operations
include("../modz/config-main.php");
include("../modz/config.php");
include("../modz/license.php");
include("../modz/errormsg.php");
include("../modz/mainmod.php");
include("../modz/mainmod-extend.php");
include("../modz/connic.php");
include("../modz/getall-admin.php");
include("authuser.php");
/**/
// call the function in the mainmod-extend.php file
exExcelLibs();

// buat objek new PHPExcel
$objPHPExcel = new PHPExcel();

// Set properties
$objPHPExcel->getProperties()->setCreator("Webby Digital");
$objPHPExcel->getProperties()->setLastModifiedBy("CMS Webby Digital");
$objPHPExcel->getProperties()->setTitle("Newsletter");
$objPHPExcel->getProperties()->setSubject("Newsletter Subscriber");

// Set lebar kolom
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(45);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(23);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);

// Mergecell, menyatukan beberapa kolom untuk judul
$objPHPExcel->getActiveSheet()->mergeCells('A1:D1');


// Buat Kolom judul tabel
$objPHPExcel->setActiveSheetIndex(0);
$SI = $objPHPExcel->getActiveSheet();
$SI->setCellValue('A1', 'Data Export Subscriber Terakhir Tanggal '.date('d-m-Y')); //Judul laporan

$SI->setCellValue('A3', 'No');
$SI->setCellValue('B3', 'Email');
$SI->setCellValue('C3', 'Type');
$SI->setCellValue('D3', 'Status');

//Mengeset Syle nya
$headerStyle = new PHPExcel_Style();
$bodyStyle   = new PHPExcel_Style();

$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setName('Candara');
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);

$alignment_H_center = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    )
);

$alignment_V_center = array(
    'alignment' => array(
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    )
);

$headerStyle->applyFromArray(
array('fill' 	=> array(
						'type'    => PHPExcel_Style_Fill::FILL_SOLID,
						'color'   => array('argb' => '4B4B4B')
	   ),
	  'font' 	=> array(
	  					'color' => array('rgb' => 'FFFFFF'),
	  					'bold'  => true,
	  					'size'  => 15
	  )
));

$bodyStyle->applyFromArray(
array('fill' 	=> array(
	  					'type'	=> PHPExcel_Style_Fill::FILL_SOLID,
	  					'color'	=> array('argb' => 'FFFFFFFF')
	  ),
	  'borders' => array(
	  					'bottom'	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
	  					'top'	    => array('style' => PHPExcel_Style_Border::BORDER_THIN),
	  					'color'		=> array('argb' => '888888'),
	  )
));

//Menggunakan HeaderStylenya
$objPHPExcel->getActiveSheet()->setSharedStyle($headerStyle, "A3:D3");

$sql = "SELECT * FROM newsletter_subscriber ORDER BY subId";
$query = query($sql);

$numofdata=rows($query);
if($numofdata > 0){	
	$nilai = array();
	$baris = 4;
	while ( $data = fetch($query) ) {
		$SI->setCellValue("A".$baris,$baris-3);
		$SI->setCellValue("B".$baris,$data['subEmail']);
		if($data['subIsMember']=='y'){
			$emailType = 'Customer';
		}elseif($data['subIsMember']=='n'){
			$emailType = 'Non-Customer';
		}else{
			$emailType = 'Volunteer';
		}
		$SI->setCellValue("C".$baris, $emailType);

		if($data['subActive']=='y'){ $statEmail = "Activated"; } else { $statEmail = "Deactivated"; }

		$SI->setCellValue("D".$baris,$statEmail);

		$nilai[] = $baris;
		$baris++;
	}

	$max_baris = max($nilai);

	$line = $max_baris;
	//Membuat garis di body tabel (isi data)
	$objPHPExcel->getActiveSheet()->setSharedStyle($bodyStyle, "A7:D".$line);
} else {
	$objPHPExcel->getActiveSheet()->mergeCells('A4:D4');
	$SI->setCellValue('A4', 'Belum Ada Data');
	$objPHPExcel->getActiveSheet()->setSharedStyle($bodyStyle, "A4:D4");
	$objPHPExcel->getActiveSheet()->getStyle("A4:D4")->applyFromArray($alignment_H_center);
}
/**/
//Memberi nama sheet
$objPHPExcel->getActiveSheet()->setTitle('Subscriber '.date('d-m-Y'));

$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Newsletter Subscriber '.date('d-m-Y').'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT+7'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT+7'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
?>