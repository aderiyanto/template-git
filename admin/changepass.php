<?php
	$page=4;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

	if ($form=="submit"){
		if (empty($oldpwd) or empty($nupwd) or empty($renupwd)){
			$error=errorlist(2);
		}

		//make sure old password is correct
		$oldpwd = sha1($oldpwd.getconfig('CMS_SALT_STRING'));
		$sql="SELECT COUNT(*) AS exist FROM admin WHERE adminUsername='$cook_username' AND adminId='$cook_id' AND adminPassword='$oldpwd'";
		$query=query($sql);
		$data=fetch($query);
		$exist=$data['exist'];

		if ($exist!=1){
			$error.=errorlist(5);
		}

		//check whether new password and re-enter new password is the same
		if ($nupwd!==$renupwd and $exist){
			$error.=errorlist(6);
		}
		elseif(strlen($nupwd)<6){
			$error.=errorlist(7);
		}
		
		//update the password
		if (!$error){
			$nupwd = sha1($nupwd.getconfig('CMS_SALT_STRING'));
			$sql="UPDATE admin SET adminPassword='$nupwd' WHERE adminId='$cook_id' AND adminUsername='$cook_username'";
			$query=query($sql);
		}
		//check whether query was successful
		if(!$query){
			$error.=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:infoupdate.php");
			}else{
				print "ok";
			}
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?>- CMS - <?php print _('pagecontent_pagetitle'); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>

<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>
<!-- jQuery Tags Input -->
<script type="text/javascript" src="/libs/jquery.tagsinput/src/jquery.tagsinput.js"></script>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('#result').hide();
	$('#keywordseo').tagsInput({
		width: 'auto'
	});

	validate('#result','#changepass','logout.php');
});
</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
    <div class="main_container">
        
        <div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->
        
        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h3><?php print _("changepass_pagetitle"); ?></h3>
                    <p><?php print _("changepass_pagedesc"); ?></p>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_content">
                                <form  name="changepass" id="changepass" method="post" action="changepass.php?form=submit" class="form-horizontal form-label-left">
                                  <div class="form-group">
                                      <label class="control-label col-md-2 col-sm-3 col-xs-12"><?php print _("changepass_nowpass"); ?> <span class="required">*</span></label>
                                      <div class="col-md-3 col-sm-5 col-xs-12">
                                        <input input name="oldpwd" type="password" id="oldpwd" class="form-control" required>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="control-label col-md-2 col-sm-3 col-xs-12"><?php print _("changepass_newpass"); ?> <span class="required">*</span></label>
                                      <div class="col-md-3 col-sm-5 col-xs-12">
                                        <input class="form-control" required type="password"  name="nupwd" id="nupwd">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="control-label col-md-2 col-sm-3 col-xs-12"><?php print _("changepass_retypenewpass"); ?><span class="required">*</span></label>
                                      <div class="col-md-3 col-sm-5 col-xs-12">
                                        <input class="form-control" name="renupwd" required type="password" id="renupwd">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                      <button type="submit" class="btn btn-primary" name="submit"><i class="fa fa-refresh"></i> <?php print _("changepass_change"); ?></button>
                                      <button type="button" class="btn btn-danger" onclick="window.location='index.php'" name="cancel" ><i class="fa fa-reply"></i> <?php print _("changepass_cancel"); ?></button>
                                      <button type="reset" class="btn btn-warning"><i class="fa fa-undo"></i> <?php print _("changepass_reset"); ?></button>
                                    </div>
                                  </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        	</div>
        </div>
        
        <?php include("com/com-footer.php"); ?>
    </div>
</div>
<!-- END .container .body -->
<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>