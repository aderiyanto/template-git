<?php
	#this file only allow to execute by using cron or cli command, but it still accessible using specific http get parameter
	if(isset($_SERVER['HTTP_USER_AGENT']) and $_GET['t'] != date('mY')){
		die('Forbidden Access');
	}

	//includes all files necessary to support operations
	$basepath = realpath(dirname(__file__).'/..');
	include($basepath."/modz/config-main.php");
	include($basepath."/modz/config.php");
	include($basepath."/modz/license.php");
	include($basepath."/modz/errormsg.php"); 
	include($basepath."/modz/mainmod.php"); 
	include($basepath."/modz/connic.php");
	include($basepath."/modz/getall-admin.php");
	
	$cmd = "php -q \"".$basepath."/admin/cron_sitemap_do.php\"";
	cli($cmd);
?>