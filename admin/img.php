<?php
session_start();
  $page='2';

  //includes all files necessary to support operations
  include("../modz/config-main.php");
  include("../modz/config-image.php");
  include("../modz/config.php");
  include("../modz/license.php");
  include("../modz/mainmod.php");
  include("../modz/mainmod-extend.php");
  include("../modz/errormsg.php");
  include("../modz/connic.php");
  include("../modz/getall-admin.php");
  include("authuser.php");

  //set the restrictions
  $allowedType = array("image/pjpeg","image/jpeg","image/gif","image/png");
  $realExt = array('jpeg','jpg','gif','png');

  if ($form=="submit"){
    if($action=="del" and $uac_delete){
      // find img filename
      $sql="SELECT * FROM imgbank WHERE imgId='" . $id . "'";
      $query=query($sql);
      $data=fetch($query);
      $file=$data['imgFile'];
      $dir=$data['imgDir'];
      $m=genthumb($file,"m");
      $s=genthumb($file,"s");

      // delete selected file and its thumbnail
      @unlink("../assets/imgbank/" . $dir . '/' . $file);
      @unlink("../assets/imgbank/" . $dir . '/' . $m);
      @unlink("../assets/imgbank/" . $dir . '/' . $s);

      // delete its entry from table
      $sql="DELETE FROM imgbank WHERE imgId='" . $id . "'";
      $query=query($sql);

      // try to remove $dir
      @rmdir("../assets/imgbank/" . $dir);
    }elseif($action=="dellall" and $uac_delete){
      
      foreach ($delid as $key => $value) {
        // find img filename
        $sql="SELECT * FROM imgbank WHERE imgId='" . $value . "'";
        $query=query($sql);
        $data=fetch($query);
        $file=$data['imgFile'];
        $dir=$data['imgDir'];
        $m=genthumb($file,"m");
        $s=genthumb($file,"s");

        // delete selected file and its thumbnail
        @unlink("../assets/imgbank/" . $dir . '/' . $file);
        @unlink("../assets/imgbank/" . $dir . '/' . $m);
        @unlink("../assets/imgbank/" . $dir . '/' . $s);

        // delete its entry from table
        $sql="DELETE FROM imgbank WHERE imgId='" . $value . "'";
        $query=query($sql);

        // try to remove $dir
        @rmdir("../assets/imgbank/" . $dir);
      }
      
    }

     //check whether query was successful
    if(!$query){
      $error.=errorlist(3);
    }

    if($error){
      print "<p>";
      print "<ul>";
      print nl2br($error);
      print "</ul>";
      print "</p>";
    }else{

      if(!$js && $source=="imgfly"){
        $sql="SELECT MAX(imgid) as maxid FROM imgbank";
        $query=query($sql);
        $data=fetch($query);
        $id=$data['maxid'];
        $imgurl="imgfly.php?s=y&id=$id";
        header("location:$imgurl");
      }elseif(!$js){
        header("location:img.php");
        exit;
      }else{
        print "ok";
      }
    }
    exit;
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _("img_pagetitle"); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">
<link href="/style/dropzone.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/javascript/dropzone.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($){
  $('.fancybox').fancybox({
    'arrows': false
  });
  $('.fancybox-frame').fancybox({
    'type'      : 'iframe',
    'width'     : '600'
  });
  $('#result').hide();
    validate('#result','#my-dropzone','img.php');

    $("#checkall1").click(function(){
    if ( (this).checked == true ){
       $('.checkradio1').prop('checked', true);

    } else {
       $('.checkradio1').prop('checked', false);

    }
  });
});
</script>
<script type="text/javascript">
  function copyToClipboard(element,imgno) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
    var $trigthis = $("#copyi"+imgno);
    $trigthis.tooltip({ title: "Copied!"});
    $trigthis.tooltip('show');
  }
  function copy(selector,imgno){
    var $temp = $("<span>");
    $("font").append($temp);
    $temp.attr("contenteditable", true)
         .html($(selector).html()).select()
         .on("focus", function() { document.execCommand('selectAll',false,null) })
         .focus();
    document.execCommand("copy");
    $temp.remove();
    var $trigthis = $("#copyu"+imgno);
    $trigthis.tooltip({ title: "Copied!"});
    $trigthis.tooltip('show');
  }
</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
  <div class="main_container">

    <div class="col-md-3 left_col menu_fixed">
      <div class="left_col scroll-view">
              <!-- Header Menu -->
               <font></font>
              <?php require("com/com-header-menu.php"); ?>
              <!-- /Header Menu -->
              <br />
              <!-- Main Menu -->
              <?php include("com/com-menu.php"); ?>
              <!-- /Main Menu -->
      </div>
      <!-- END .left_col .scroll-view -->
    </div>
    <!-- END .col-md-3 .left_col .menu_fixed -->

    <!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

          <div class="row">
        <div class="col-md-12">
                <div class="page-title">
                  <div class="title_left" style="width: 100%;">
                      <h3><?php print _('img_pagetitle'); ?></h3>
                      <p><?php print _('img_pagedesc'); ?></p>
                  </div>
                </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
            <?php if ($uac_add){ ?>
            <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                    <div class="pull-right">
                      <a href="img.php?action=add" class="btn btn-primary"><i class="fa fa-plus"></i> <?php print _('img_view_addbutton'); ?></a>
                    </div>
                  </div>
            </div>
            <?php } ?>
            <?php if (empty($action)){ ?>
              <?php if ($uac_delete){ ?>
              <form id="dellall" name="dellall" method="post" action="img.php?form=submit&amp;action=dellall" class="form-horizontal form-label-left">
              <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                      <div class="pull-left">
                        <label><input type="checkbox" id="checkall1"> Select All</label>
                        <a href="javascript:if (window.confirm('<?php print _('image_manager_delete_all'); ?> ?')){ document.getElementById('dellall').submit(); };" class="btn btn-danger"><i class="fa fa-trash"></i> <?php print _('image_manager_delete_all'); ?></a>
                      </div>
                    </div>
              </div>
              <?php } ?>
              <div class="box-manager">
            <?php
            if (empty($pg)){
              $pg=1;
            }

              $sql="SELECT * FROM imgbank ORDER BY imgId DESC";
            $query=query($sql);
            $numofdata=rows($query);

            $num=1;
            $numPerPage=25;
            $offset=($pg-1)*$numPerPage;
            $num=$num+(($pg-1)*$numPerPage);

            $sql.=' LIMIT '.$offset.','.$numPerPage;
            $query=query($sql);

            if ($numofdata<1){
          ?>
          <div class="text-center"><br /><?php print _("img_view_notavailable"); ?><br /><br /></div>
          <?php
            } else {

            while ($data=fetch($query)){
              $data=output($data);
              $picfile="../assets/imgbank/" . $data['imgDir'] . '/' . $data['imgFile'];
              $size=getimagesize($picfile);
              $widthpic=$size[0];
              $heightpic=$size[1];
          ?>
              <div class="col-md-15 box-drop">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <a href="img-data.php?action=data&amp;id=<?php print $data['imgId']; ?>" class="fancybox-frame"><img src="/assets/imgbank/<?php print $data['imgDir']; ?>/<?php print genthumb($data['imgFile'],"m"); ?>" alt="noname"/></a>
                  </div>
                  <?php if($uac_delete){ ?>
                  <div class="panel-body">
                    <div class="media">
                      <div class="media-left">
                        <label><input type="checkbox" name="delid[]" class="checkradio1" value="<?php print $data['imgId']; ?>"></label>
                      </div>
                      <div class="media-body">
                        <p id="url-i<?php print $num; ?>" style="display:none;">/assets/imgbank/<?php print $data["imgDir"]; ?>/<?php print $data['imgFile']; ?></p>
                        <button id="copyi<?php print $num; ?>" onclick="copyToClipboard('#url-i<?php print $num; ?>',<?php print $num; ?>)" class="btn btn-default img-copy" type="button"><i class="fa fa-clone"></i> URL</button>
                        <p id="demo<?php print $num; ?>" style="display:none;"><img src="/assets/imgbank/<?php print $data['imgDir']; ?>/<?php print $data['imgFile']; ?>" alt="noname"/></p>
                        <button id="copyu<?php print $num; ?>" onclick="copy('#demo<?php print $num; ?>',<?php print $num; ?>)" class="btn btn-primary img-copy" type="button"><i class="fa fa-clone"></i> Gambar</button>
                        <a href="javascript:if (window.confirm('<?php print _('cms_delete'); ?>: <?php print $data['imgFile']; ?> ?')){ window.location='img.php?form=submit&amp;action=del&amp;id=<?php print $data["imgId"]; ?>'; };" title="<?php print _('cms_delete'); ?>: <?php print $data['imgFile']; ?>" rel="tooltip" class="btn btn-danger" style="margin-right:0px; border-radius:0;"><i class="fa fa-trash"></i></a>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php
              $num++;
              }
            }
          ?>
          <div style="clear:both;"></div>
          </div><br/>
          </form>

          <?php
          $options['total']=$numofdata;
          $options['filename']='img.php';
          $options['qualifier']='img.php';
          $options['pg']=$pg;
          $options['numPerPage']=$numPerPage;
          $options['style']=1;
          $options['addquery']=TRUE;
          paging($options);
          ?>
              <?php } elseif ($action=="add" and $uac_add){ ?>
                <a href="img.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

              <h4><?php print _('galleryalbum_mainaddalbum') ?></h4>
          <hr/>

          <form action="img-upload.php?action=add&amp;form=submit" id="my-dropzone" class="dropzone form-horizontal form-label-left">

                      <div class="form-group">
                          <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _('img_view_imagefile'); ?> <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="drop-here"><div class="dz-message" data-dz-message><span>Drop files here</span></div></div>
                <div class="table table-striped drop-table" class="files" id="previews">

                  <div id="template" class="file-row">
                    <table class="table table-bordered">
                      <tr>
                        <td width="100"><span class="preview"><img data-dz-thumbnail /></span></td>
                        <td>
                          <div>
                              <p class="name" data-dz-name></p>
                              <strong class="error text-danger" data-dz-errormessage></strong>
                          </div>
                          <div>
                              <p class="size" data-dz-size></p>
                              <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                              </div>
                          </div>
                          <div>
                            <button data-dz-remove class="btn btn-warning cancel">
                                <i class="glyphicon glyphicon-ban-circle"></i>
                                <span>Cancel</span>
                            </button>
                          </div>
                        </td>
                      </tr>
                    </table>
                  </div>

                </div>
                            <span class="help-block"><?php print _("img_view_addpagedesc"); ?></span>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="checkbox">
                      <label>
                        <input type="checkbox" name="watermark" value="y" id="watermark" /> Watermark
                      </label>
                  </div>
                          </div>
                      </div>
                      
                      <hr/>

                      <div class="form-group">
                          <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <button name="Submit" id="submit-all" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
                            <button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='img.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
                            <button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
                          </div>
                      </div>
          </form>
            <?php } ?>
            </div>
          </div>


    </div>
        <!-- END THE CONTENT OF PAGE HERE -->


    <?php include("com/com-footer.php"); ?>

  </div>
  <!-- END .main_container -->
</div>
<!-- END .container .body -->
<?php if($action=="add"){ ?>
<script>
    var previewNode = document.querySelector("#template");
    previewNode.id = "";
    var previewTemplate = previewNode.parentNode.innerHTML;
    previewNode.parentNode.removeChild(previewNode);
    Dropzone.options.myDropzone = {

    // Prevents Dropzone from uploading dropped files immediately
    previewTemplate: previewTemplate,
    previewsContainer: '#previews',
    acceptedFiles: ".png,.jpg,.jpeg",
    uploadMultiple: true,
    autoProcessQueue: false,
    parallelUploads:100,

    init: function() {
    var submitButton = document.querySelector("#submit-all")
    myDropzone = this; // closure

    submitButton.addEventListener("click", function() {
    myDropzone.processQueue(); // Tell Dropzone to process all queued files.
    console.log(myDropzone);
    });

    // You might want to show the submit button only when 
    // files are dropped here:
    
    this.on("successmultiple", function(files, response) {
      // Gets triggered when the files have successfully been sent.
      // Redirect user or notify of success.
      window.location = "img.php";
    });
    this.on("success", function(file, responseText) {
    // Handle the responseText here. For example, add the text to the preview element:
    $(".removebutton").hide();
    });
    this.on("errormultiple", function(files, response) {
      // Gets triggered when there was an error sending the files.
      // Maybe show form again, and notify user of error
    
    if(response=="Server responded with 0 code."){
      window.location = "img.php";    
    }else{
      window.location = "img.php?action=add";    
    }

    });


    }
    };
</script>
<?php } ?>
<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>
