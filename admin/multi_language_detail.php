<?php
	$page=29;
	
	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	require('../modz/poedit/php-mo.php');
	include("authuser.php");
	
	$sql = "SELECT * FROM multi_languages_var WHERE varId='$id'";
	$query = query($sql);
	$data = fetch($query);
	
	if($data['varStatus'] == 'sync'){
		$pos = '0 0';
	} elseif($data['varStatus'] == 'new'){
		$pos = '-26px 0';
	} elseif($data['varStatus'] == 'notfound'){
		$pos = '-52px 0';
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- bootstrap-datepicker -->
<link rel="stylesheet" type="text/css" href="/libs/bootstrap-datepicker/css/datepicker3.css" />
<link rel="stylesheet" href="/libs/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">


<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce_simple.js"></script>
<script type="text/javascript" src="/libs/moment/min/moment-with-locales.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>
</head>

<body class="flyctn" style="background:#F9F9F9;">

<div class="container body">
	<div class="main_container">

		<div class="row">
			<div class="col-md-12" style="width:100%;">


				<div style="float:right;margin-right:40px;padding-top:3px;height:20px;width:20px;background:url(../assets/images/multilang_iconset.png)<?php print $pos;?>;background-repeat:no-repeat;font-weight:bold;text-indent:25px;font-size:10px;"><?php print strtoupper($data['varStatus']);?></div>

				<div><b><?php print _('multi_language_detail_admin_view_variabel'); ?></b> <?php print $data['varVar'];?></div>
				<?php
				if($data['varDesc']){
					?><div style="margin-top:6px;font-style:italic;color:#666;font-size:11px"><?php print $data['varDesc'];?></div><?php
				}
				
				print '<br/>';
				
				$sqlx = "SELECT * FROM multi_languages_value WHERE varId='$id'";
				$queryx = query($sqlx);
				while($datax = fetch($queryx)){
					?><div style="padding:8px;border:1px dashed #999;background-color:#f7f7f7;margin-top:10px;">
						<div><img src="../assets/images/flag-<?php print $datax['langCode'];?>-edit.png" align="absmiddle"/> <b><?php print localename($datax['langCode']);?></b></div>
						<div style="margin-top:5px;"><?php print $datax['valValue'] ? $datax['valValue']:'-';?></div>
						</div>
					<?php
				}
				
				$sqlx = "SELECT mark FROM multi_languages_mark WHERE varId='$id' ORDER BY mark ASC";
				$queryx = query($sqlx);
				$numofdata = rows($queryx);
				if($numofdata){
					?>
					
					<br/>
					<br/>
					<b><?php print _('multi_language_detail_admin_info1'); ?></b>
					<ol type="numeric" style="margin-left:-15px;margin-top:8px">
						<?php
						while($datax = fetch($queryx)){
							?><li style="margin-bottom:10px;border-bottom:1px solid #ccc;padding:4px;"><?php print $datax['mark'];?></li><?php
						}
						?>
					</ol>
					<?php
				} else {
					?><div style="margin-top:20px;font-style:italic;color:#ff0000"><?php print _('multi_language_detail_admin_info2'); ?></div><?php
				}
				?>


			</div>
		</div>
	</div>
</div>

</body>
</html>