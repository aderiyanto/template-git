<?php
	$page=29;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

	/*
	used to debug blank lang values;
	foreach($lc_lang as $key=> $value){
		$sql = "SELECT * FROM multi_languages_var";
		$query = query($sql);
		while($data = fetch($query)){
		$exist = checkDuplicateRecord("multi_languages_value","varId='{$data['varId']}' AND langCode='$value'");
			if($exist == 0){
				$nextid	=nextid("valId","multi_languages_value");
				$sqlx = "INSERT INTO multi_languages_value VALUES('$nextid', '{$data['varId']}', '$value', '')";
				$queryx= query($sqlx);
			}
		}
	} exit; */

	if ($form=="submit"){
		if ($action=="add" and $uac_add){
			if (empty($msgid)){
				$error=errorlist(2);
			}

			// check whether $target exist
			$exist = checkDuplicateRecord("multi_languages_var","varVar='".$msgid."'");
			if($exist>0){
				$error.=errorlist(90);
			}

			 if(!$error){
			 	$nextid	=nextid("varId","multi_languages_var");
				$sqlv	="INSERT INTO multi_languages_var VALUES ('$nextid','$msgid', '$desc', 'new')";
				$queryv	=query($sqlv);
				if($queryv){
					foreach($multilang_cms as $key=>$value){
						$nextv	=nextid("valId","multi_languages_value");
						 $sql="INSERT INTO multi_languages_value VALUES ('$nextv','$nextid','".$value."','".$langtext[$value]."')";
						$query=query($sql);
					}

					$basepath = realpath(dirname(__FILE__).'/..');
					cli("php $basepath/admin/multi_language_do.php cmd=generate");
				}
			}
		}elseif ($action=="edit" and $uac_edit){
			if (empty($msgid)){
				$error=errorlist(2);
			}

			/* preg_match_all('/[\*\(\)\-\+\/]/',$target,$match);
			if(count(array_filter($match))!=0){
				$error=errorlist(20);
			} */

			if(!$error){
				$sql = "UPDATE multi_languages_var SET varVar='$msgid', varDesc='$desc', varStatus='sync'  WHERE varId='".$id."'";
				$query = query($sql);

				foreach($multilang_cms as $key=>$value){
					$sql = "UPDATE multi_languages_value SET valValue='{$langtext[$value]}' WHERE varId='".$id."' AND langCode ='".$value."'";
					$query = query($sql);

					$check_data = countdata("multi_languages_value","varId='".$id."' AND langCode ='".$value."'");
					if($check_data < 1){
 						$nextv	=nextid("valId","multi_languages_value");
						$sql="INSERT INTO multi_languages_value VALUES ('$nextv','$id','".$value."','".$langtext[$value]."')";
						$query=query($sql);
					}
				}

				$basepath = realpath(dirname(__file__).'/..');
				cli("php $basepath/admin/multi_language_do.php cmd=generate",false);
			}
			print '<script>top.location="'.$redir.'";</script>';
			exit;
		} elseif($action=="del" and $uac_delete){
			$sql="DELETE FROM multi_languages_value WHERE varId='" . $id . "'";
			$query=query($sql);

			$sql="DELETE FROM multi_languages_var WHERE varId='" . $id . "'";
			$query=query($sql);

			$sql="DELETE FROM multi_languages_mark WHERE varId='" . $id . "'";
			$query=query($sql);

			$basepath = realpath(dirname(__file__).'/..');
			cli("php $basepath/admin/multi_language_do.php cmd=generate");
		} elseif($action == 'sync'){
			$scan = scandir_r(array('../web', '../wap'), 'php');
			$scan[] = '../admin/email_varguide.php';
			$scan[] = '../modz/errormsg.php';

			$scriptEncoded = false;
			foreach($scan as $key => $value){
				$lines = @file($value);
				foreach($lines as $line_num => $line){
					preg_match_all('/(\$\_\_oc\=strtolower\(substr\(php\_uname\(\)\,0\,3\)\)\;\$\_\_ln)/', $line, $encoded);
					if(count($encoded[0]) > 0){
						$scriptEncoded = true;
						break 2;
					}

					#finding gettext format: _('text') or _("text")
					preg_match_all('/\_\([\'|\"]([a-zA-Z0-9-_]+)[\'|\"]\)/', $line, $matches);
					if(count($matches[1]) > 0){
						$line_number = $line_num + 1;
						foreach($matches[1] as $k => $v){
							$variables[$v][] = str_replace('..', '', $value).":{$line_number}";
						}
					}
				}
			}

			if($scriptEncoded == false){
				#reset language table status
				$sql = "UPDATE multi_languages_var SET varStatus=null";
				$query = query($sql);
			}

			if(count($variables) > 0){
				foreach($variables as $key => $value){
					$exists = countdata("multi_languages_var", "varVar='$key'");
					if($exists == 0){
						#add new variable
						$nextid = nextid("varId", "multi_languages_var");
						$sql = "INSERT INTO multi_languages_var VALUES('$nextid', '$key', '', 'new')";
						$query = query($sql);

						if($query){
							#add blank values
							foreach($multilang_cms as $k => $v){
								$nextid2 = nextid("valId","multi_languages_value");
								$sqlx = "INSERT INTO multi_languages_value VALUES('$nextid2', '$nextid', '$v', '')";
								$queryx= query($sqlx);
							}

							#add mark
							foreach($value as $k => $v){
								$sqlx = "INSERT INTO multi_languages_mark VALUES('$v', '$nextid')";
								$queryx = query($sqlx);
							}
						}
					} else {
						$varId = getval("varId", "multi_languages_var", "varVar", $key);
						$nullExist = countdata("multi_languages_value", "valValue='' AND varId='$varId'");

						#update current variable
						$sql = "UPDATE multi_languages_var SET varStatus='".($nullExist == 0 ? 'sync':'new')."' WHERE varVar='$key'";
						$query = query($sql);

						if($query){
							#remove mark
							$sql = "DELETE FROM multi_languages_mark WHERE varId='$varId'";
							$query = query($sql);

							#add mark
							foreach($value as $k => $v){
								$sqlx = "INSERT INTO multi_languages_mark VALUES('$v', '$varId')";
								$queryx = query($sqlx);
							}
						}
					}
				}
			}

			if($scriptEncoded == false){
				#update missing variable status
				$sql = "SELECT * FROM multi_languages_var WHERE varStatus=''";
				$query = query($sql);
				while($data = fetch($query)){
					$sqlx = "DELETE FROM multi_languages_mark WHERE varId='{$data['varId']}'";
					$queryx = query($sqlx);
				}

				$sql = "UPDATE multi_languages_var SET varStatus='notfound' WHERE varStatus=''";
				$query = query($sql);
			}
		} elseif($action=="addgroup" and $uac_add){
			// check whether $target exist
			$exist = checkDuplicateRecord("multi_languages_group","groupPrefix='$prefix'");
			if($exist>0){
				$error.=errorlist(28);
			}

			if(!$error){
				$nextid = nextid("groupId","multi_languages_group");
				$sql = "INSERT INTO multi_languages_group VALUES('$nextid', '$prefix', '$name')";
				$query = query($sql);
			}
		} elseif($action=="editgroup" and $uac_edit){
			if(empty($prefix) or empty($name)){
				$error=errorlist(2);
			}
			if(!$error){
				$sql = "UPDATE multi_languages_group SET groupPrefix ='$prefix', groupName='$name' WHERE groupId='$id'";
				$query = query($sql);
			}
		} elseif($action=="delgroup" and $uac_delete){
			$sql = "DELETE FROM multi_languages_group WHERE groupId='$id'";
			$query = query($sql);
		}


		//check whether query was successful
		if(!$query){
			$error.=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:multi_language.php".($scriptEncoded === true ? '?encoded=y':''));
			}else{
				print "ok";
			}
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _("multi_language_pagetitle"); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('.fancybox').fancybox({
		'arrows': false
	});

	$('.fancybox-frame').fancybox({
		'type'		: 'iframe',
		'arrows'	: false
	});
	$('#result').hide();
	validate('#result','#add','multi_language.php');
	validate('#result','#edit','multi_language.php');
	validate('#result','#addgroup','multi_language.php?action=showgroup');
	validate('#result','#editgroup','multi_language.php?action=showgroup');
});
</script>
</head>

<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('multi_language_pagetitle'); ?></h3>
			                <p><?php print _('multi_language_pagedesc'); ?></p>
			            </div>
		            </div>
	        	</div>
        	</div>

        	
			<?php if($encoded == 'y'){ ?>
			<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
        			<div class="alert alert-info alert-dismissible fade in" role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
	                    <?php print _("multi_language_pageinfo"); ?>
	                </div>
        		</div>
        	</div>
        	<?php } ?>

        	<?php if ($uac_add and $cook_gid == 1){ ?>
        	<div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                	<div class="pull-right">
                	<?php if($action == 'showgroup'){ ?>                	
                	<a href="multi_language.php" class="btn btn-primary"><i class="fa fa-angle-double-left"></i> <?php print _('multi_language_view_back'); ?></a>          	
                	<a href="multi_language.php?action=addgroup" class="btn btn-primary"><i class="fa fa-plus"></i> <?php print _('multi_language_view_addbutton'); ?></a>
                	<?php
					} else {
					?>
					<a href="multi_language.php?action=add" class="btn btn-primary"><i class="fa fa-plus"></i> <?php print _('multi_language_view_addword'); ?></a>
                	<a href="multi_language.php?form=submit&action=sync" class="btn btn-primary"><i class="fa fa-refresh"></i> <?php print _('multi_language_view_scanvariable'); ?></a>
                	<a href="multi_language.php?action=showgroup" class="btn btn-primary"><i class="fa fa-cog"></i> <?php print _('multi_language_view_groupsetting'); ?></a>
					<?php } ?>
                	</div>
                </div>
        	</div>
        	<?php } ?>

        	<div class="row">
	        	<div class="col-md-12">
	        	<?php if (empty($action)){ ?>
	        		<?php
						if(empty($pg)){
							$pg=1;
						}

						$numPerPage=500;
						$offset=($pg-1)*$numPerPage;
						$num=1;
						$num=$num+(($pg-1)*$numPerPage);
						if($cat){
							if($cat == "_"){
								$whereClause[] = "SUBSTRING(multi_languages_var.varVar,1,1)='_'";
							} else {
								$whereClause[] = "multi_languages_var.varVar LIKE '$cat%'";
							}
						}

						if($keyword){
							$whereClause[] = "(text LIKE '%$keyword%' OR multi_languages_var.varVar LIKE '%$keyword%')";
						}

						if($langstatus){
							$whereClause[] = "multi_languages_var.varStatus='$langstatus'";
						}

						if(is_array($whereClause)){
							$whereClause =  ' HAVING '.implode(' AND ', $whereClause);
						}

						$sql = "SET SESSION group_concat_max_len = 100000";
						$query = query($sql);

						$sql="SELECT multi_languages_var.*, GROUP_CONCAT( multi_languages_value.langCode,'||',multi_languages_value.valValue ORDER BY multi_languages_value.langCode DESC SEPARATOR '<#>' ) AS text
							FROM multi_languages_var
							JOIN multi_languages_value ON multi_languages_var.varId=multi_languages_value.varId
							GROUP BY multi_languages_var.varId $whereClause ORDER BY multi_languages_var.varVar ASC";
						$query=query($sql);
						$numofdata=rows($query);
						$sql.=' LIMIT '.$offset.','.$numPerPage;
						$query=query($sql);
					?>
	        		<div class="row">	        			
						<div class="col-md-4">
							<strong>TOTAL:</strong> <?php print number_format($numofdata, 0, ',', '.');?> <?php print _("multi_language_tabel_variable"); ?>
						</div>
						<div class="col-md-8">
							<div class="pull-right">
								<form method="get" action="multi_language.php" class="form-inline">
									
									<div class="form-group">
										<select name="langstatus" class="form-control input-sm">
											<option value=""><?php print _("multi_language_view_selectstatus"); ?></option>
											<option value="new"<?php print $langstatus == 'new' ? ' selected="selected"':'';?>>New</option>
											<option value="sync"<?php print $langstatus == 'sync' ? ' selected="selected"':'';?>>Sync</option>
											<option value="notfound"<?php print $langstatus == 'notfound' ? ' selected="selected"':'';?>>Notfound</option>
										</select>
									</div>
									<div class="form-group">
										<select name="cat" class="form-control input-sm">
											<option value=""><?php print _("multi_language_view_selectgroup"); ?></option>
											<?php
											$sqls = "SELECT * FROM multi_languages_group ORDER BY groupName ASC";
											$querys = query($sqls);
											while($datas = fetch($querys)){
												?><option value="<?php print $datas['groupPrefix'];?>"<?php print ($datas['groupPrefix'] == $cat) ? ' selected="selected"':'';?>><?php print $datas['groupName'];?></option><?php
											}?>
										</select>
									</div>
									<div class="form-group">
										<input type="text" name="keyword" size="35" value="<?php print stripquote($keyword);?>" class="form-control input-sm">
									</div>
									<button type="submit" name="submit" value="<?php print _("newsletter_queue_search"); ?>" class="btn btn-sm btn-dark"><?php print _("newsletter_queue_search"); ?></button>
								</form>
							</div>
						</div>
					</div>
					<div class="table-responsive" style="margin-top:10px;">
            			<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
            				<thead>
            					<tr>
            						<th width="25"><?php print _("cms_no"); ?></th>
									<th width="280"><?php print _("multi_language_tabel_variable"); ?></th>
									<?php
									arsort($multilang_cms);
									foreach($multilang_cms as $key=>$value){?>
										<th><?php print _("multi_language_view_language"); ?> <?php print localename($value); ?></th>
										<?php
									}?>
									<th width="70"><?php print _("newsletter_view_status"); ?></th>
									<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
									<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
									<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
            					</tr>
            				</thead>
            				<tbody>
            					<?php
								if($numofdata<1){
									  ?>
								<tr>
									<td colspan="8" class="asterik"><div class="text-center"><br /><?php print _("multi_language_view_notavailable"); ?><br /><br /></div></td>
								</tr>
								  <?php
								} else {
								$i = 0;
								while ($data=fetch($query)){
									$data=output($data);
									$text = explode("<#>", $data['text']);
								  ?>
								<tr valign="top" id="itemLine<?php print $data['varId'];?>">
									<td class="text-center"><?php print $num; ?></td>
									<td>
										<strong><?php print $data['varVar']; ?></strong><br/>
										<small><?php print $data['varDesc'];?></small>
									</td>
									<?php
									foreach($text as $key => $value){
										list($langCode, $langText) = explode('||', $value);
										if(in_array($langCode, $multilang_cms)){
											?><td class="item"><?php print $langText;?></td><?php
										}
									}?>
									<td>
										<?php
										if($data['varStatus'] == 'sync'){
											$pos = '0 0';
										} elseif($data['varStatus'] == 'new'){
											$pos = '-26px 0';
										} elseif($data['varStatus'] == 'notfound'){
											$pos = '-52px 0';
										}
										?>
										<div style="padding-top:3px;height:20px;width:20px;background:url(../assets/images/multilang_iconset.png)<?php print $pos;?>;background-repeat:no-repeat;font-weight:bold;text-indent:25px;font-size:11px;"><?php print strtoupper($data['varStatus']);?></div>
									</td>
									<td class="text-center"><a title="detail" class="fancybox-frame" href="multi_language_detail.php?id=<?php print $data['varId'];?>" rel="tooltip"><i class="fa fa-list-alt"></i></a></td>
									<td class="text-center"><?php if ($uac_edit){ ?><a href="multi_language.php?action=edit&amp;id=<?php print $data['varId']; ?>" title="<?php print _("cms_edit"); ?>: <?php print $data['varVar']; ?>" rel="tooltip"><i class="fa fa-edit"></i></a><?php } ?></td>
									<td class="text-center"><?php if ($uac_delete and $cook_gid == 1){ ?><a class="delete" href="multi_language.php?form=submit&amp;action=del&amp;id=<?php print $data['varId']; ?>" data-original-title="<?php print $data['varVar']; ?>"><i title="<?php print _("cms_delete"); ?>: <?php print $data['varVar']; ?>" rel="tooltip" class="fa fa-trash"></i></a><?php } ?></td>
								</tr>
									  <?php
							  			$num++;
							  		}
								}
							  ?>
            				</tbody>
            			</table>
            		</div>
            		<?php
						$options['total']=$numofdata;
						$options['filename']='multi_language.php';
						$options['qualifier']='multi_language.php';
						$options['pg']=$pg;
						$options['numPerPage']=$numPerPage;
						$options['style']=1;
						$options['addquery']=TRUE;
						paging($options);
					?>
					<br/><br/><br/>
				<?php } elseif ($action=="add" and $uac_add){ ?>
					<a href="multi_language.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('multi_language_mainview_addword') ?></h4>
					<hr/>
					<form action="multi_language.php?action=add&amp;form=submit" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">

						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="msgid"><?php print _('multi_language_tabel_variable'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="msgid" type="text" id="msgid" size="40" required class="form-control">
	                        </div>
                    	</div>
                    	
                    	<?php foreach($multilang_cms as $key=>$value){ ?>
						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="langtext<?php print $value;?>"><?php print _("multi_language_view_language"); ?> <?php print localename($value);?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<textarea name="langtext[<?php print $value;?>]" id="langtext<?php print $value;?>" cols="60" rows="3" required class="form-control"></textarea>
	                        </div>
                    	</div>
						<?php } ?>

						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="desc"><?php print _('newsletter_view_information'); ?>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="desc" type="text" id="desc" size="60" class="form-control">
	                        </div>
                    	</div>

                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='multi_language.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>
                    	<br/><br/><br/>

					</form>
				<?php
					} elseif ($action=="edit" and $uac_edit){
					$sql	="SELECT * FROM multi_languages_var WHERE varId='$id'";
					$query	=query($sql);
					$data	=fetch($query);
					$data	=formoutput($data);
				?>
					<a href="multi_language.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('multi_language_view_updateword') ?></h4>
					<hr/>

					<form action="multi_language.php?action=edit&amp;form=submit&amp;id=<?php print $id ?>" method="post" enctype="multipart/form-data" name="edit" id="edit" class="form-horizontal form-label-left">
						<input type="hidden" name="redir" value="<?php print $_SERVER['HTTP_REFERER'];?>"/>

						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="msgid"><?php print _('multi_language_tabel_variable'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<?php
								if($cook_gid == 1){
									?><input name="msgid" type="text" id="msgid" size="40" required class="form-control" value="<?php print $data['varVar']; ?>"/><?php
								} else {
									?><input name="msgid" type="hidden" value="<?php print $data['varVar']; ?>"/><?php
									print '<p class="form-control-static">'.$data['varVar'].'</p>';
								} 
								?>
	                        </div>
                    	</div>

                    	<?php foreach($multilang_cms as $key=>$value){ ?>
						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="msgid"><?php print _("multi_language_view_language"); ?> <?php print localename($value);?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<textarea name="langtext[<?php print $value;?>]" cols="60" rows="3" class="form-control" required><?php print getval("valValue", "multi_languages_value", "varId='{$data['varId']}' AND langCode='$value'");?></textarea>
	                        </div>
                    	</div>
						<?php } ?>

						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="msgid"><?php print _('newsletter_view_information'); ?>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
								<?php
								if($cook_gid == 1){
									?><input name="desc" type="text" id="desc" size="60" class="form-control" value="<?php print $data['varDesc'];?>"/><?php
								} else {
									?><input name="desc" type="hidden" value="<?php print $data['varDesc'];?>"/><?php
									print '<p class="form-control-static">' . ($data['varDesc'] ? $data['varDesc']:'-') . '</p>';
								}
								?>
	                        </div>
                    	</div>

                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_updatebutton'); ?>"><?php print _('cms_updatebutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='multi_language.php'"><?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>
                    	<br/><br/><br/>

					</form>
				<?php } elseif($action == 'showgroup'){ ?>
					<div class="table-responsive">
            			<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
            				<thead>
            					<tr>
            						<th class="text-center" width="25"><?php print _("newsletter_view_number"); ?></th>
									<th width="250">Prefix</th>
									<th><?php print _("multi_language_view_groupname"); ?></th>
									<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
									<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
            					</tr>
            				</thead>
            				<tbody>
            					<?php
									$num = 1;
									$sql = "SELECT * FROM multi_languages_group ORDER BY groupPrefix ASC";
									$query = query($sql);
									while($data = fetch($query)){
									?>
									<tr>
										<td class="text-center"><?php print $num;?></td>
										<td><?php print $data['groupPrefix'];?></td>
										<td><?php print $data['groupName'];?></td>
										<td class="text-center"><?php if ($uac_edit){ ?><a href="multi_language.php?action=editgroup&amp;id=<?php print $data['groupId']; ?>" title="<?php print _("cms_edit"); ?>: <?php print $data['groupName'];?>" rel="tooltip"><i class="fa fa-edit"></i></a><?php } ?></td>
										<td align="center"><?php if ($uac_delete){ ?><a class="delete" href="multi_language.php?form=submit&amp;action=delgroup&amp;id=<?php print $data['groupId']; ?>" title="<?php print  stripquote($data['groupName']); ?>"><i rel="tooltip" title="<?php print _("cms_delete"); ?>: <?php print  stripquote($data['groupName']); ?>" class="fa fa-trash"></i></a><?php } ?></td>
									</tr>
								<?php
									++$num;
								}
								?>
            				</tbody>
            			</table>
            		</div>
            	<?php } elseif($action == 'addgroup'){ ?>
            		<a href="multi_language.php?action=showgroup" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('multi_language_view_mainaddbutton') ?></h4>
					<hr/>
					<form action="multi_language.php?action=addgroup&amp;form=submit" method="post" enctype="multipart/form-data" name="addgroup" id="addgroup" class="form-horizontal form-label-left">
						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prefix">Prefix <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="prefix" id="prefix" type="text" size="40" required class="form-control">
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _("multi_language_view_groupname"); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="name" id="name" type="text" size="40" required class="form-control">
	                        </div>
                    	</div>

                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><?php print _('cms_addbutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='multi_language.php?action=showgroup'"><?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>
                    	<br/><br/><br/>
					</form>
				<?php } elseif($action == 'editgroup'){
					$sql = "SELECT * FROM multi_languages_group WHERE groupId='$id'";
					$query = query($sql);
					$data = fetch($query);
				?>
					<a href="multi_language.php?action=showgroup" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('multi_language_view_mainaddbutton') ?></h4>
					<hr/>

					<form action="multi_language.php?action=editgroup&amp;form=submit&id=<?php print $id; ?>" method="post" enctype="multipart/form-data" name="editgroup" id="editgroup" class="form-horizontal form-label-left">
						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prefix">Prefix <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="prefix" id="prefix" type="text" size="40" required class="form-control" value="<?php print stripquote($data['groupPrefix']); ?>">
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _("multi_language_view_groupname"); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="name" id="name" type="text" size="40" required class="form-control" value="<?php print stripquote($data['groupName']); ?>">
	                        </div>
                    	</div>

                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_updatebutton'); ?>"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='multi_language.php?action=showgroup'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>
                    	<br/><br/><br/>
					</form>
	        	<?php } ?>
	        	</div>
	        </div>


		</div>
        <!-- END THE CONTENT OF PAGE HERE -->


		<?php include("com/com-footer.php"); ?>

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>