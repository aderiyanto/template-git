<?php
	$page=39;
	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/errormsg.php");
	include("../modz/license.php");
	include("../modz/mainmod.php");
	include("../modz/mainmod-extend.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");


    if ($form=="submit"){

        if ($action=="add"){
            if(empty($to) or empty($text) or empty($senderid)){
				$error=errorlist(2);
			}
            if($getbalance < 275 or empty($getbalance)){
                $error=errorlist(66);
            }
            if(!$error){
                $arrayto = explode(',', $to);
                foreach($arrayto as $t){
									$sendsms = send_sms($t,$text,"queue");
                }

            }
		}
		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:sms_log.php");
			}else{
				print "ok";
			}
		}
		exit;
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print _('sms_simple_page_title'); ?> - <?php print _('merchant_area'); ?> - <?php print SITE_NAME; ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="/libs/bootstrap-datepicker/css/datepicker3.css" />
<link rel="stylesheet" href="/libs/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" type="text/css" href="/style/bootstrap-clockpicker.min.css" />
<style>
	.tagsinput input{
		width:100% !important;
	}
</style>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">
<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>

<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="/libs/moment/min/moment-with-locales.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- jQuery Tags Input -->
<script type="text/javascript" src="/libs/jquery.tagsinput/src/jquery.tagsinput.js"></script>
<script type="text/javascript" src="/javascript/bootstrap-clockpicker.min.js"></script>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('#result').hide();
	validate('#result','#add','sms_log.php');
	$('#to').tagsInput({
		'width': 'auto',
		'defaultText':'<?php print _('merchant_smsviro_placeholder'); ?>',
	});
});
function countChar(val) {
    var len = val.value.length;
    var jumsms = Math.ceil(len/160);
    $('#charNum').text(len + ' Karakter');
    $('#countSMS').text(jumsms + ' SMS');

}
</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">
            <?php
            include "com/com-getbalance.php";
            ?>
			<div class="row">
				<div class="col-md-12">
					<h3><?php print _('sms_simple_page_title'); ?></h3>
                    <?php print "Balance: ".$balance['currency']." ".number_format($balance['balance']); ?>
					<hr>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
                    <?php
					if ($action==""){
                    ?>
                    <form action="sms_simple.php?form=submit&amp;action=add" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">
                        <input type="hidden" name="getbalance" id="getbalance" value="<?php print $balance['balance']; ?>">
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
	                        	<?php print _('sms_from'); ?><span class="required">*</span>
	                        </label>
	                        <div class="col-md-2 col-sm-2 col-xs-12">
	                        	<select class="form-control" name="senderid" required>

	                        	    <option value="<?php print getconfig('SMS_API_FROM'); ?>"><?php print getconfig('SMS_API_FROM'); ?></option>

	                        	</select>
	                        </div>
                        </div>
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
	                        	<?php print _('sms_to'); ?><span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-4 col-xs-12">
	                        	<input name="to" type="text" id="to" size="40" class="form-control" required>
                                <span class="help-block"><?php print _('sms_text_desc_separated'); ?></span>
	                        </div>
                        </div>
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="mCatPicLogo">
	                        	<?php print _('sms_use_template'); ?>
	                        </label>
	                        <div class="col-md-4 col-sm-4 col-xs-12">
								<div class="list-template-box">
									<ul class="list-template">
										<?php
										$sqltemp ="SELECT * FROM template_sms order by tempName ASC";
										$querytemp = query($sqltemp);
										while($datatemp =fetch($querytemp)){
											?>
											<li style="list-style:none;margin-left:-30px"><i class="fa fa-angle-right"></i>&nbsp;<a href="javascript:;" onclick="document.add.textd.value='<?php print stripquote(ereg_replace("\r\n"," ",$datatemp['tempTemplate'])); ?>'; document.add.textd.focus();"><?php print $datatemp['tempName']; ?></a></li>
											<?php
										}
										?>
									</ul>
								</div>
	                        </div>
                    	</div>
						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="textd">
	                        	<?php print _('sms_text'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-4 col-xs-12">
                                <textarea class="form-control" cols="20" rows="6" name="text" id="textd" onkeyup="countChar(this)" onfocus="countChar(this)" required></textarea>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 col-sm-6 col-lg-6">
                                        <div id="charNum" class="help-block" style="font-weight: bold;">0 Karakter</div>
                                    </div>
                                    <div class="col-md-6 col-xs-6 col-sm-6 col-lg-6">
                                        <div id="countSMS" class="help-block text-right" style="font-weight: bold;">0 SMS</div>
                                    </div>
                                </div>
	                        </div>
                    	</div>

                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> <?php print _('sms_add'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>
                    </form>
                    <?php
                    }
                    ?>
				</div>


			</div>
        </div>
        <!-- /page content -->

        <?php include("com/com-footer.php"); ?>
	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>
