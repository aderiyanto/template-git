<?php
	$page=8;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

	if($form=="submit"){
		if($action=='dorestore'){
			#get file data
			$d_fname = $_FILES['file'];
			$name=$d_fname['name'];
			$t_fname=$d_fname['tmp_name'];
			if(!in_array(strtolower(substr($name,-4,4)),array('.zip','.sql'))){
				$error=errorlist(13);
			}
			else{
				#make the backup file first, in case restore failed
				#generate a name for old data
				$fname = getconfig('DB_NAME').'-'.date("d",$now).date("m",$now).date("Y-His",$now).'-'.codegen(3);

				$cmd = "php -q \"".$_SERVER['DOCUMENT_ROOT']."/admin/backup_do.php\" \"{$_SERVER['DOCUMENT_ROOT']}\" \"".($_SERVER['MYSQL_HOME'] ? $_SERVER['MYSQL_HOME']:'unknown')."\" \"tmp-{$fname}\"";
				cli($cmd);

				#do database restoration process
				#check filetype
				if(strtolower(substr($d_fname['name'],-4,4))=='.zip'){
					#get the file
					$zip = zip_open($t_fname);
					if ($zip) {
						$zip_entry = zip_read($zip);
						$open_entry = zip_entry_open($zip, $zip_entry, "r");
						if ($open_entry) {
							$buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
							$handle = fopen("../assets/backup/{$fname}.sql", "w");
							fwrite($handle, $buf);
							fclose($handle);
							zip_entry_close($zip_entry);
						}
					}
					zip_close($zip);
				}
				elseif(strtolower(substr($d_fname['name'],-4,4))=='.sql'){
					move_uploaded_file($_FILES['file']['tmp_name'],"../assets/backup/{$fname}.sql");
				}

				$cmd = "php -q \"".$_SERVER['DOCUMENT_ROOT']."/admin/restore_do.php\" \"{$_SERVER['DOCUMENT_ROOT']}\" \"".($_SERVER['MYSQL_HOME'] ? $_SERVER['MYSQL_HOME']:'unknown')."\" \"$fname\"";
				cli($cmd);
				print $fname;
			}
		}
		elseif($action=='checkrestore'){
			if(file_exists("../assets/backup/{$fname}.wb")){
				if(file_get_contents("../assets/backup/{$fname}.wb")=='ok'){
					$restor_var = _("restore_success");
					$msg = $restor_var;
				}
				else{
					$restor_var = _("restore_failed");
					$msg = $restor_var;
				}

				$output = array(
					'msgid' => 'ok',
					'msg' => $msg
				);

				#remove temporary data
				if($fname){
					unlink("../assets/backup/{$fname}.wb");
					unlink("../assets/backup/{$fname}.sql");
					unlink("../assets/backup/tmp-{$fname}.wb");
					unlink("../assets/backup/tmp-{$fname}.zip");
					unlink("../assets/backup/tmp-{$fname}.sql");
				}
			}
			else{
				$output = array(
					'msgid' => 'wait',
					'msg' => '<img src="/assets/images/zoomloader.gif"/> Restoring data....'
				);
			}
			print json_encode($output);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _('restore_pagetitle'); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">


<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce_simple.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($){
	$('.fancybox').fancybox({
		'arrows': false
	});
	$('#result').hide();

	$('#restoredb').validate({
		submitHandler: function(form) {
			var options = {
				type: $(this).attr('method'),
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data){
					if(data){
						$('#result').html(data);
						if(data.substr(0,3)=='<p>'){
							$('#result').show();
						}
						else{
							checkrestore(data);
						}
					}
				}
			};
			jQuery(form).ajaxSubmit(options);
		}
	});
});

function checkrestore(fname){
	$.get("restore.php?form=submit&action=checkrestore&fname="+fname,function(data){
		data = JSON.parse(data);
		$('#checker').html('<div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;"><p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>'+data.msg+'</div>');
		if(data.msgid!='ok'){
			setTimeout("checkrestore('"+fname+"')",2000);
		}
	});
}
</script>
<style>
#checker{
	font-size:14px;
}
</style>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('restore_pagetitle'); ?></h3>
			            </div>
		            </div>
	        	</div>
        	</div>

        	<!-- THE CONTENT OF PAGE HERE -->
        	<div class="row">
	        	<div class="col-md-12">
	        		<div class="row">

	        			<?php
							if(function_exists('exec')){
								?>
								<div class="col-md-12">
									<strong><?php print _("restore_pagedesc"); ?></strong>
									<hr/>
								</div>
								<div class="col-md-12">
									<div id="checker" style="margin-bottom:10px;"></div>
								</div>
								<div class="col-md-8">
									<form name="restoredb" id="restoredb" method="post" action="restore.php?form=submit&action=dorestore" enctype="multipart/form-data" class="form-horizontal form-label-left">
										<div class="form-group">
											<label class="control-label col-md-2 col-sm-2 col-xs-12" for="file"><?php print _('restore_file'); ?>  <span class="required">*</span></label>
											<div class="col-md-4 col-sm-6 col-xs-12">
												<input type="file" required name="file" id="file" />
											</div>
										</div>
										<div class="form-group">
											<div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<button type="submit"  name="Submit" id="Submit" class="btn btn-sm btn-primary"><i class="fa fa-refresh"></i> <?php print _('restore_restorebutton'); ?></button>
												<button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
											</div>
										</div>
									</form>
								</div>
								<?php
							}else{
								?>
								<div class="col-md-7">
									<div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
										<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><?php print _("restore_descinfo"); ?></p>
									</div>
								</div>
								<?php
							}
						?>
	        		</div>
	        	</div>
	        </div>


		</div>
		<!-- END THE CONTENT OF PAGE HERE -->

		<?php include("com/com-footer.php"); ?>

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>