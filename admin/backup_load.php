<?php
	$page=7;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");
	include("../modz/mainmod-extend.php");

	
?>
<div class="table-responsive">
	<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
		<thead>
			<tr>
				<th width="35" class="text-center"><?php echo _('cms_no'); ?></th>
				<th width="180" class="text-center"><?php echo _('backup_activitydate'); ?></th>
				<th><?php echo _('backup_filebackup'); ?></th>
				<th width="130" class="text-center"></th>
			</tr>
		</thead>
		<tbody>
			<?php
			if(empty($pg)){
				$pg=1;
			}

			$numPerPage=20;
			$offset=($pg-1)*$numPerPage;
			$num=1;
			$num=$num+(($pg-1)*$numPerPage);

			$sql="SELECT * FROM backup_activity ORDER BY backactId DESC";
			$query=query($sql);
			$numofdata=rows($query);

			if ($numofdata<1){
			?>
			<tr>
				<td colspan="9"><div class="text-center"><br />
				<?php print _('backup_datablognotavailable'); ?><br />
				<br /></td>
			</tr>								
			<?php 
			} else {
				$sql.=' LIMIT '.$offset.','.$numPerPage;
				$query=query($sql);
				while ($data=fetch($query)){
			?>
				<tr>
					<td class="text-center"><?php echo $num; ?></td>
					<td class="text-center"><?php print dateformat($data['backactDate'],'time'); ?></td>
					<td>
						<a href="backup.php?form=submit&action=download_only&fname=<?php echo $data['backactFileName']; ?>"><?php echo $data['backactFileName'].".zip"; ?></a>
					</td>
					<td class="text-center">
					<?php 
						if($data['backactStatus']=='success'){
							echo '<a href="backup.php?form=submit&action=download_only&fname='.$data['backactFileName'].'" class="btn btn-primary btn-sm"><i class="fa fa-download"></i> Download</a>';
						} elseif($data['backactStatus']=='failed'){
							echo '<span class="label label-danger">Backup Failed</span>';
						}
					?>
					</td>
				</tr>
			<?php $num++; }
			}
			?>
		</tbody>
	</table>
</div>
<?php
	$options['total']=$numofdata;
	$options['filename']='backup.php';
	$options['qualifier']='backup.php';
	$options['pg']=$pg;
	$options['numPerPage']=$numPerPage;
	$options['style']=1;
	$options['addquery']=TRUE;
	paging($options);
?>