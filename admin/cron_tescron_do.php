<?php 
	#this file only allow to execute by using cron or cli command
	if(!defined('STDIN') ){
		die('Forbidden Access');
	}

	//includes all files necessary to support operations
	$basepath = realpath(dirname(__file__).'/..');
	include($basepath."/modz/config-main.php");
	include($basepath."/modz/config.php");
	include($basepath."/modz/license.php");
	include($basepath."/modz/errormsg.php");
	include($basepath."/modz/mainmod.php");
	include($basepath."/modz/connic.php");
	include($basepath."/modz/getall-admin.php");

	#Send mail
	$options = array(
		'mailopt' => '',
		'from' => SITE_EMAIL,
		'fromname' => SITE_NAME,
		'to' => 'afrioni@afrioni.web.id',
		'subject' => 'Tes Cron CMS Webby',
		'replyto' => SITE_EMAIL,
		'cc' => '',
		'bcc' => '',
		'message' => 'Tes Corn Jalan Apa Enggak',
		'messagetype' => 'html',
		'filename' => ''
	);
	$mail = sendMailComplete($options);
 
	//update activity statistic
	//$cron_option['cron_name'] = 'CRON_REMOVE_BACKUP_FILE'; 
	//cron_activity_log($cron_option);
?>