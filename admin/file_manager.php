<?php
	$page=16;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");
	
	#set allowed Ext
	$extList = array(
		'pdf' => '#DC0000',
		'doc' => '#0E41A7',
		'docx' => '#0E41A7',
		'xls' => '#467319',
		'xlsx' => '#467319',
		'ppt' => '#E54F27',
		'pptx' => '#E54F27',
		'txt' => '#44abff',
		'zip' => '#E2C076',
		'rar' => '#A04B8C'
	);
	
	#get max allowed size
	$maxAllowedSize = min(array( (int)ini_get('post_max_size'), (int)ini_get('upload_max_filesize')));
	
 	if ($form=="submit"){
		if($action=="add" and $uac_add){
			#Files
			$fileman=$_FILES['filename'];

			#Set error if field name not exist or file larger than upload_max_filesize
			if($fileman['error']==1 or !$fileman) {
				$error=errorlist(23);
			}elseif(empty($fileman['name'])){
				$error=errorlist(2);
			}else{
				$allowedExt = array_keys($extList);
				
				list($fileName, $ext) = explode(".", $fileman['name']);

				#check if it is really an uploaded file
				if(is_uploaded_file($fileman['tmp_name'])){
					$ext = strtolower(end(explode('.', $fileman['name'])));

					if (in_array($ext, $allowedExt) === false){
						$error .= errorlist(13);
					}
					
					if(($fileman['size']/1048576) > $maxAllowedSize){
						$error .= errorlist(23);
					}
				} else {
					$error .= errorlist(35);
				}

				if(!$error){
					$fname = $fileName.'.'.$ext;

					$dir = date("dmY");
					$targetdir = "../assets/fileman/".$dir."/";

					if(!@opendir($targetdir) ){
						@mkdir($targetdir);
					}

					if(@file_exists($targetdir.$fname)){
						$fname = $fileName.'_'.codegen(3).'.'.$ext;
					}
					
					$upload = @move_uploaded_file($fileman['tmp_name'], $targetdir.$fname);
	
					list($newFname, $newExt) = explode('.', $fname);
					if($zipit == 'y' and $ext != 'zip'){
						if(@file_exists($targetdir.$newFname.'.zip')){
							$fname = $newFname.'_'.codegen(3).'.zip';
						}
						list($newFname, $newExt) = explode('.', $fname);
						
						@chdir($targetdir);
						$zip     = new ZipArchive();
						$zipfile = $newFname.'.zip';

						$compress = $zip->open($zipfile, ZIPARCHIVE::CREATE);
						if($compress === true){
							$zip->addFile($fname);
							$zip->close();
							@unlink($fname);
						}

						$ext = 'ZIP';
						$fname = $newFname.'.zip';
					}
					
					if(empty($title)){
						$title = $fname;
					}
					
					#get the next id for fileman
					$nextid = nextid("fileId","file_manager");

					$sql = "INSERT INTO file_manager VALUES ('$nextid', '$title', '$dir', '$fname', '{$fileman['size']}', '".strtoupper($ext)."', '0', '$now')";
					$query = query($sql);
				}
			}
		}elseif($action=="del" and $uac_delete){
			if($emptyid){
				$error=$emptyid;
			}else{
			  // Find fileman filename
			 	$sql="SELECT * FROM file_manager WHERE fileId='". (int)$id ."'";
				$query=query($sql);
				$data=fetch($query);

				// Delete selected file
				@unlink("../assets/fileman/" . $data['fileDir']. '/' . $data['fileName']);
				@rmdir("../assets/fileman/" . $data['fileDir']);

				// Delete its entry from table
				$sql="DELETE FROM file_manager WHERE fileId='". (int)$id ."'";
				$query=query($sql);
			}
		}

		 //check whether query was successful
		if(!$query){
			$error=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:file_manager.php");
			}else{
				print "ok";
			}
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _("file_manager_pagetitle"); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>


<script type="text/javascript">
jQuery(document).ready(function($){
	$('#result').hide();
    validate('#result','#add','file_manager.php');
});
</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('file_manager_pagetitle'); ?></h3>
			                <p><?php print nl2br(_('file_manager_pagedesc')) ?> 
			                <?php
							foreach($extList as $key=>$value){
								$f[] = strtoupper($key);
							}
							
							print implode(', ', $f);
							?></p>
			            </div>
		            </div>
	        	</div>
        	</div>

        	<?php if ($uac_add){ ?>
        	<div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                	<div class="pull-right">
                	<a href="file_manager.php?action=add" class="btn btn-primary"><i class="fa fa-plus"></i> <?php print _('file_manager_view_addbutton'); ?></a>
                	</div>
                </div>
        	</div>
        	<?php } ?>

        	<div class="row">
	        	<div class="col-md-12">
        		<?php if (empty($action)){ ?>
        			<div class="row">
        				<div class="col-md-6 pull-right">
        					<div class="pull-right">
        						<form method="get" name="search" class="form-inline">
        							<div class="form-group">
										<select name="type" class="form-control input-sm">
											<option value=""><?php print _("cms_optionall"); ?></option>
											<?php
											foreach($extList as $key => $value){
												?><option value="<?php print strtoupper($key);?>"<?php print (strtoupper($key) == $type) ? ' selected="selected"':'';?>>File <?php print strtoupper($key); ?></option><?php
											}?>
										</select>
									</div>
									<div class="form-group">
										<input type="text" name="keyword" value="<?php print formoutput($keyword);?>" class="form-control input-sm">
									</div>
									<input type="submit" class="btn btn-sm btn-dark" name="submit" value="<?php print _("cms_search"); ?>" />
								</form>
        					</div>
        				</div>

        				<div class="col-md-12">
        					<div class="table-responsive" style="margin-top:12px;">
							<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
                				<thead>
                					<tr>
                						<th width="25"><?php print _("cms_no"); ?></th>
							            <th><?php print _("file_manager_view_filename"); ?></th>
							            <th><?php print _('file_manager_filefile'); ?></th>
							            <th width="80"><?php print _("file_manager_view_filetype"); ?></th>
							            <th width="100"><?php print _("file_manager_view_filesize"); ?></th>
							            <th width="80"><?php print _("cms_download"); ?></th>
							            <th width="300"><?php print _("file_manager_view_fileurl"); ?></th>
										<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
                					</tr>
                				</thead>
                				<tbody>
                					<?php
								  		if (empty($pg)){
											$pg=1;
										}
										
										if($keyword){
											$whereClause[] = "fileName LIKE '%".str_replace(" ", '%', $keyword)."%'";
										}
										
										if($type){
											$whereClause[] = "fileType='$type'";
										}
										
										if($whereClause){
											$whereClause = 'WHERE '.implode(' AND ', $whereClause);
										}
										
										$sql="SELECT * FROM file_manager $whereClause ORDER BY fileName ASC";
										$query=query($sql);
										$numofdata=rows($query);

										$num=1;
										$numPerPage=20;
										$offset=($pg-1)*$numPerPage;
										$num=$num+(($pg-1)*$numPerPage);

										$sql.=' LIMIT '.$offset.','.$numPerPage;
										$query=query($sql);

										if ($numofdata<1){
									?>
									<tr>
										<td colspan="8" class="asterik"><div class="text-center"><br /><?php print _("file_manager_view_notavailable"); ?><br /><br /></div></td>
									</tr>
									<?php
										}else{
										while($data=fetch($query)){
											$data=output($data);
											$filemanfile="../assets/fileman/" . $data['fileDir'] . '/' . $data['fileName'];
											$widthfileman=$size[0];
											$heightfileman=$size[1];

											 //File size
											$filesize=round($data['fileSize']/1024,2);
											if($filesize<=1023){
												$filesize=$filesize;
												$denomination=" KB";
											}else{
												$filesize=round($filesize/1024,2);
												$denomination=" MB";
										}
									?>
									<tr>
										<td class="text-center"><?php print $num; ?></td>
							            <td><?php print $data['fileNameShow']; ?></td>
										<td><?php print $data['fileName']; ?></td>
										<td class="text-center"><div style="width:30px; padding:3px 0;font-size:9px;border:1px solid #333;font-weight:bold;color:#fff;background-color:<?php print $extList[strtolower($data['fileType'])];?>"><?php print $data['fileType']; ?></div></td>
										<td class="text-center"><?php print  $filesize.$denomination; ?></td>
										<td class="text-center"><?php print  ($data['fileCounter']==0 ? "-":"".$data[fileCounter]." kali"); ?></td>
										<td><a href="http://<?php print getconfig('SITE_URL'); ?>/download/<?php print $data['fileId']; ?>/">http://<?php print getconfig('SITE_URL'); ?>/download/<?php print $data['fileId']; ?>/</a></td>
										<td class="text-center"><?php if ($uac_delete){ ?><a class="delete" href="file_manager.php?form=submit&amp;action=del&amp;id=<?php print $data['fileId']; ?>" title="<?php print stripquote($data['fileName']); ?>"><i rel="tooltip" title="<?php print _("file_manager_view_delete"); ?>: <?php print stripquote($data['fileName']); ?>" class="fa fa-trash-o"></i></a><?php } ?></td>
									</tr>
									<?php
												$num++;
											}
										}
									?>
                				</tbody>
                			</table>
                			</div>
                			<?php
								$options['total']=$numofdata;
								$options['filename']='file_manager.php';
								$options['qualifier']='file_manager.php';
								$options['pg']=$pg;
								$options['numPerPage']=$numPerPage;
								$options['style']=1;
								$options['addquery']=TRUE;
								paging($options);
							?>
        				</div>
        			</div>
        		<?php } elseif ($action=="add" and $uac_add){ ?>
        			<a href="file_manager.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('file_manager_pagetitle') ?></h4>
					<hr/>
					<form action="file_manager.php?form=submit&amp;action=add" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">
						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="title"><?php print _('file_manager_view_filename'); ?>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="title" type="text" id="title" size="40" class="form-control" />
	                        	<span class="help-block"><?php print _("file_manager_add_filedesc"); ?></span>
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="filename"><?php print _('file_manager_view_filename'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="filename" type="file" id="filename" size="30" required />
	                        	<span class="help-block"><?php print _("file_manager_add_fileinfo"); ?> <strong><?php print $maxAllowedSize;?>MB</strong></span>
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<div class="checkbox">
							        <label>
							        	<input name="zipit" type="checkbox" value="y" id="complressit"> <?php print _("file_manager_add_compress"); ?>
							        </label>
						    	</div>
	                        </div>
                    	</div>
                    	
                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='file_manager.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>
                    	<br/><br/><br/>
					</form>
        		<?php } ?>
	        	</div>
	        </div>


		</div>
        <!-- END THE CONTENT OF PAGE HERE -->


		<?php include("com/com-footer.php"); ?>

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>