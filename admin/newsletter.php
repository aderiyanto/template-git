<?php
	$page=32;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/mainmod-extend.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

	if ($form == "submit"){
		$files = $_FILES['files'];
		$pic = $_FILES['pic'];

		$admin_base_dir = dirname(__FILE__);

		#attachment config
		$attachment_maxFileSize = 7000000; #based on sendgrid limitation

		#image banner config
		$pic_allowedType = array("image/pjpeg","image/jpeg","image/png");
		$pic_realExt = array('jpeg','jpg','png');

		if ($action=="add" and $uac_add){
			if(empty($nsubject) || empty($nemailmsg) || empty($pic['name'])){
				$error = errorlist(2);
			}

			#validate attachment file if any
			if($files['name']){
				list($attach_filename, $ext) = explode(".", $files['name']);

				#check if it is really an uploaded file
				if(is_uploaded_file($files['tmp_name'])){
					$ext = strtolower(end(explode('.', $files['name'])));

					if (empty($ext) or $ext != 'pdf'){
						$error .= errorlist(13);
					}

					if($files['size'] > $attachment_maxFileSize){
						$error .= errorlist(23);
					}
				} else{
					$error .= errorlist(35);
				}
			}

			#validate image banner
			if(in_array($pic['type'], $pic_allowedType)){
				if(is_uploaded_file($pic['tmp_name'])){
					#make sure it is really really image
					$img = getimagesize($pic['tmp_name']);
					if(!(is_numeric($img[0]) or is_numeric($img[1]) or $img[0]>0 or $img[1]>0)){
						$error = errorlist(13);
					}
				}else{
					$error = errorlist(35);
				}
			} else{
				$error = errorlist(13);
			}

			if(!$error){
				$exist = countdata("newsletter","nEmailMsg='$nemailmsg'");
				if($exist){
					#Throw Msg if content exist before in another entry
					$error = errorlist(39);
				} else{
					#Insert new Newsletter
					$nextid = nextid("nId","newsletter");

					$sql = "INSERT INTO newsletter VALUES ('$nextid','$nsubject','','$nemailmsg','$now','n','$subscriber','')";
					$query = query($sql);

					#Insert subscriber
					if($query){
						#do upload attachment file if any
						if($files['name']){
							$fname = $attach_filename.'.pdf';

							$dir = date("dmY");
							$targetdir = "../assets/newsletter/".$dir."/";

							if(!@opendir($targetdir) ){
								@mkdir($targetdir);
							}

							if(@file_exists($targetdir.$fname)){
								$fname = $attach_filename.'_'.codegen(3).'.pdf';
							}

							$upload = @move_uploaded_file($files['tmp_name'], $targetdir.$fname);

							if($upload){
								$sqlx = "INSERT INTO newsletter_attach VALUES ('$nextid','$dir','$fname')";
								$queryx = query($sqlx);
							}
						}

						#do upload image banner
						$fileExt = array_keys($pic_allowedType, $pic['type']);
						$fileExt = $pic_realExt[$fileExt[0]];

						#get filename
						$fname = str_replace('.','',$pic['name']).'.'.$fileExt;

						$img = uploadit($fname,$pic['tmp_name'],"newsletter",$nextid,null,null,null,null,600,1000);
						$dir = $img['dir'];
						$imgfilename = $img['filename'];
						$full_img_path = $dir . '/' . $imgfilename;

						#update newsletter query
						$sqlx = "UPDATE newsletter SET nMainImg='$full_img_path' WHERE nId='$nextid'";
						$queryx = query($sqlx);

						#back to /admin
						chdir($admin_base_dir);

						#if the subscriber group selected, there are 'reg,noreg,all'
						if($subscriber=='reg'){
							$sqlmember=" AND subIsMember='y'";
						} elseif($subscriber=='noreg'){
							$sqlmember=" AND subIsMember='n'";
						}

						#remove old tmp
						$sqlm = "DELETE FROM newsletter_tmp WHERE nId='$id'";
						$querym = query($sqlm);

						#get subscriber
						$sqlm = "SELECT subId FROM newsletter_subscriber WHERE subActive='y' $sqlmember";
						$querym = query($sqlm);
						while ($datam=fetch($querym)){
							#Save newsletter to each member email to tmp before the 'Send' submited
							$sqln = "INSERT IGNORE INTO newsletter_tmp VALUES('{$datam['subId']}','$nextid')";
							$queryn = query($sqln);
						}

						#send the newsletter immediately
						if($sendnow=='y'){
							if($js=="on"){
								print 	"<script type=\"text/javascript\">
											top.location = \"newsletter.php?form=submit&action=sendemail&id=$nextid\";
										</script>";
								exit;
							}else{
								header("location:newsletter.php?form=submit&action=sendemail&id=$nextid");
								exit;
							}
 						}
					}
				}
			}
		}elseif($action=="edit" and $uac_edit){
			if(empty($nsubject) || empty($nemailmsg)){
				$error = errorlist(2);
			}

			#validate attachment file if any
			if($files['name']){
				list($attach_filename, $ext) = explode(".", $files['name']);

				#check if it is really an uploaded file
				if(is_uploaded_file($files['tmp_name'])){
					$ext = strtolower(end(explode('.', $files['name'])));

					if (empty($ext) or $ext != 'pdf'){
						$error .= errorlist(13);
					}

					if($files['size'] > $attachment_maxFileSize){
						$error .= errorlist(23);
					}
				} else{
					$error .= errorlist(35);
				}
			}

			#validate image banner
			if($files['pic']){
				if(in_array($pic['type'], $pic_allowedType)){
					if(is_uploaded_file($pic['tmp_name'])){
						#make sure it is really really image
						$img = getimagesize($pic['tmp_name']);
						if(!(is_numeric($img[0]) or is_numeric($img[1]) or $img[0]>0 or $img[1]>0)){
							$error = errorlist(13);
						}
					}else{
						$error = errorlist(35);
					}
				} else{
					$error = errorlist(13);
				}
			}

			if(!$error){
				$exist = countdata("newsletter","nId!='$id' AND nEmailMsg='$nemailmsg'");
				if($exist){
					$error=errorlist(39);
				}else{
					$sql = "UPDATE newsletter SET nSubject='$nsubject', nEmailMsg='$nemailmsg', nSubscriber='$subscriber', nRegDate='$now' WHERE nId='$id'";
					$query = query($sql);

					#Insert subscriber
					if($query){
						#do upload attachment file if any
						if($files['tmp_name']){
							$fname = $attach_filename.'.pdf';

							$dir = date("dmY");
							$targetdir = "../assets/newsletter/".$dir."/";

							if(!@opendir($targetdir) ){
								@mkdir($targetdir);
							}

							if(@file_exists($targetdir.$fname)){
								$fname = $attach_filename.'_'.codegen(3).'.pdf';
							}

							$upload = @move_uploaded_file($files['tmp_name'], $targetdir.$fname);

							if($upload){
								#remove old attachment file
								$attc = getval("attachDir,attachFile","newsletter_attach","nId='".$id."'");
								$thedir = $attc['attachDir'];
								$thefile = $attc['attachFile'];
								if($thedir and $thefile){
									@unlink("../assets/newsletter/" .$thedir. '/' . $thefile);
									@rmdir("../assets/newsletter/" .$thedir);
								}

								$sqlx = "UPDATE newsletter_attach SET attachDir='$dir', attachFile='$fname' WHERE nId='$id'";
								$queryx = query($sqlx);
							}
						}

						if($pic['tmp_name']){
							#get old image info
							$oldpic = getval("nMainImg","newsletter", "nId='$id'");

							#do upload image banner
							$fileExt = array_keys($pic_allowedType, $pic['type']);
							$fileExt = $pic_realExt[$fileExt[0]];

							#get filename
							$fname = str_replace('.','',$pic['name']).'.'.$fileExt;

							$img = uploadit($fname,$pic['tmp_name'],"newsletter",$id,null,null,null,null,600,1000);
							$dir = $img['dir'];
							$imgfilename = $img['filename'];
							$full_img_path = $dir . '/' . $imgfilename;

							#back to /admin
							chdir($admin_base_dir);

							#update newsletter query
							$sqlx = "UPDATE newsletter SET nMainImg='$full_img_path' WHERE nId='$id'";
							$queryx = query($sqlx);

							if($queryx){
								#remove old image
								list($dir, $files) = explode ("/", $oldpic);
								@unlink("../assets/newsletter/". $oldpic);
								@rmdir("../assets/newsletter/". $dir);
							}
						}

						#if the subscriber group selected, there are 'reg,noreg,all'
						if($subscriber=='reg'){
							$sqlmember=" AND subIsMember='y'";
						} elseif($subscriber=='noreg'){
							$sqlmember=" AND subIsMember='n'";
						}

						#get subscriber
						$sqlm = "SELECT subId FROM newsletter_subscriber WHERE subActive='y' $sqlmember";
						$querym = query($sqlm);
						while ($datam=fetch($querym)){
							#Save newsletter to each member email to tmp before the 'Send' submited
							$sqln = "INSERT IGNORE INTO newsletter_tmp VALUES('{$datam['subId']}','$id')";
							$queryn = query($sqln);
						}

						#send the newsletter immediately
						if($sendnow=='y'){
							if($js=="on"){
								print 	"<script type=\"text/javascript\">
											top.location = \"newsletter.php?form=submit&action=sendemail&id=$id\";
										</script>";
								exit;
							}else{
								header("location:newsletter.php?form=submit&action=sendemail&id=$id");
								exit;
							}
 						}
					}
				}
			}
		}elseif($action=="del" and $uac_delete){
			#elete newsletter content
			$attc = getval("attachDir,attachFile","newsletter_attach","nId='".$id."'");
			$thedir = $attc['attachDir'];
			$thefile = $attc['attachFile'];
			if($thedir and $thefile){
				@unlink("../assets/newsletter/" .$thedir. '/' . $thefile);
				@rmdir("../assets/newsletter/" .$thedir);

				$sql = "DELETE FROM newsletter_attach WHERE nId='$id'";
				$query = query($sql);
			}

			#get image banner image info
			$pic = getval("nMainImg","newsletter", "nId='$id'");
			if($pic){
				list($dir, $files) = explode ("/", $pic);
				@unlink( "../assets/newsletter/".$pic);
				@rmdir( "../assets/newsletter/".$dir);
			}

		 	$sql = "DELETE FROM newsletter WHERE nId='$id'";
			$query = query($sql);

			$sql = "DELETE FROM newsletter_tmp WHERE nId='$id'";
			$query = query($sql);

			$sql = "DELETE FROM newsletter_email_queue WHERE substring_index(tmpSign, '.' , -1) = '$id'";
			$query = query($sql);
		}elseif($action=="sendemail"){
			$cmd = "php -q \"".$_SERVER['DOCUMENT_ROOT']."/admin/newsletter_sendmail_do.php\" \"{$_SERVER['DOCUMENT_ROOT']}\" \"$id\"";
			cli($cmd);
		}elseif($action=="sending"){
			$sql	="UPDATE newsletter SET nSending='$sending' WHERE nId='".$id."'";
			$query	=query($sql);
		}

		//check whether query was successful
		if(!$query){
			$error=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if($action == 'sendemail'){
				$uriStr = "?s=y&fid=$id";
			}

			if(!$js){
				header("location:newsletter.php".$uriStr);
			}else{
				if($action=="del"){
					print "ok";
				}else{
					print 	"<script type=\"text/javascript\">
								top.location = \"newsletter.php$uriStr\";
							</script>";
					exit;
				}
 			}
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?>- CMS - <?php print _('pagecontent_pagetitle'); ?></title>
    
<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
    
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">
    

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    
<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>
<!-- jQuery Tags Input -->
<script type="text/javascript" src="/libs/jquery.tagsinput/src/jquery.tagsinput.js"></script>
  
<script type="text/javascript">
jQuery(document).ready(function($){
    $('.fancybox').fancybox({
		'arrows': false
	});
	$('.fancybox-frame').fancybox({
		'type'			: 'iframe'
	});

	$('#result').hide();
	$('#keywordseo').tagsInput({
		width: 'auto'
	});

	validate('#result','#add','newsletter.php');
	validate('#result','#edit','newsletter.php');
});
</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">
        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _("newsletter_pagetitle"); ?></h3>
			                <p><?php print _("newsletter_pagedesc"); ?></p>
			            </div>
		            </div>
	        	</div>
        	</div>
        	<?php if ($uac_add and lcauth('addpage')){ ?>
        	<div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 text-right" style="margin-bottom: 10px;">
                	<a href="newsletter.php?action=add" class="btn btn-primary"><i class="fa fa-plus"></i> <?php print _("newsletter_view_addnewsletter"); ?></a>
                </div>
        	</div>
        	<?php } ?>
        	<div class="row">
	        	<div class="col-md-12">
	                <div class="x_content">
	        		<?php if (empty($action)){ ?>
	        			<div class="table-responsive">
		        			<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
		        				<thead>
									<tr class="tablehead">
										<th style="width:25px;" class="text-center"><?php print _('cms_no'); ?></th>
										<th><?php print _("newsletter_view_title"); ?></th>
                                        <th style="width:250px;" class="text-center"><?php print _("newsletter_view_registerdate"); ?></th>
                                        <th style="width:150px;" class="text-center"><?php print _("newsletter_view_receiver"); ?></th>
                                        <th style="width:40px;">&nbsp;</th>
                                        <th style="width:40px;">&nbsp;</th>
                                        <th style="width:40px;">&nbsp;</th>
                                        <th style="width:40px;">&nbsp;</th>
                                        <th style="width:40px;">&nbsp;</th>
                                        <th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
                                        <th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
									</tr>
								</thead>
								<tbody>
								<?php
                                    if (empty($pg)){
                                        $pg=1;
                                    }

									$numPerPage=20;
                                    $offset=($pg-1)*$numPerPage;
                                    $num=1+(($pg-1)*$numPerPage);

                                    $sql="SELECT * FROM newsletter ORDER BY nRegDate DESC";
                                    $query=query($sql);
                                    $numofdata=rows($query);

                                    $sql.=' LIMIT '.$offset.','.$numPerPage;
                                    $query=query($sql);
									if ($numofdata<1){
								?>
									<tr>
										<td colspan="11"><div class="text-center"><br /><?php print _('newsletter_view_notavailable'); ?><br /><br /></div></td>
									</tr>
								<?php
									}else{
										while ($data=fetch($query)){
                                            $data=output($data);
								?>
									<tr valign="top">
										<td class="text-center"><?php print $num; ?></td>
										<td><?php print $data['nSubject']; ?></td>
										<td class="text-center"><?php print dateformat($data['nRegDate'],"full"); ?></td>
                                        <td class="text-center">
                                        <?php
                                            if($data['nSubscriber']=='reg'){
                                                print _("newsletter_view_customer");
                                            }elseif($data['nSubscriber']=='noreg'){
                                                print _("newsletter_view_noncustomer");
                                            }else{
                                                print _("newsletter_view_allreceiver");
                                            }
                                        ?>
                                        </td>	
                                        <td class="text-center">
                                        <?php if($data['nSending']=="y"){?>
                                            <a href="newsletter.php?form=submit&action=sending&sending=n&id=<?php print $data['nId'] ?>" onclick="return confirm('<?php print _("newsletter_view_askpause"); ?>')" >
                                            <i rel="tooltip" class="fa fa-spinner fa-pulse" alt="Newsletter was Sending" title="<?php print _("newsletter_view_sendingpause"); ?>"></i>
                                            </a><?php } ?>
                                        <?php if($data['nSending']=="n"){?>
                                            <a href="newsletter.php?form=submit&action=sending&sending=y&id=<?php print $data['nId'] ?>" onclick="return confirm('<?php print _("newsletter_view_askcontinue"); ?>')" title="<?php print _("newsletter_view_sendingcontinue"); ?>" data-toggle="tooltip" data-placement="top">
                                            <i class="fa fa-pause"></i></a><?php } ?>
                                        </td>
                                        <td class="text-center">
                                            <?php
                                                $exist	=checkDuplicateRecord("newsletter_attach","nId='".$data['nId']."'");
                                                if($exist>0){
                                                    $attc=getval("attachDir,attachFile","newsletter_attach","nId='".$data['nId']."'");
                                                    if($attc['attachDir'] and $attc['attachFile']){ ?>
                                                    <a href="../assets/newsletter/<?php print $attc['attachDir']."/".$attc['attachFile'] ?>" target="_blank">
                                                        <i class="fa fa-paperclip"></i>
                                                    </a>
                                            <?php
                                                }
                                                    }
                                            ?>
                                        </td>
                                        <td class="text-center">
                                            <?php
                                            $check_tmp = countdata("newsletter_tmp", "nId='{$data['nId']}'");

                                            if($check_tmp == 0){
                                                ?><img src="../assets/images/email-dim.png" title="<?php print _("newsletter_view_subnotavailable"); ?>"/><?php
                                            } else{
                                                if ($uac_edit){ ?><a href="javascript:if (window.confirm('<?php if($data['nStatus']=='n'){ ?><?php print _('newsletter_view_asksend'); ?> <?php print $data['nSubject']; }else{?><?php print _('newsletter_view_sendingnewsletter'); ?> <?php print $data['nSubject'];?>?<?php } ?>')){ window.location='newsletter.php?form=submit&action=sendemail&id=<?php print $data['nId']; ?>'; };" title="<?php print _("cms_send"); ?>: <?php print $data['nSubject']; ?>"  data-toggle="tooltip" data-placement="top"><i class="fa fa-envelope"></i></a><?php } ?>
                                                <?php
                                            }?>
                                        </td>
                                        <td class="text-center">
                                            <?php print ($data['nStatus']=='y' ? '<i class="fa fa-minus-circle" title="'._('Newsletter_view_sending').'" style="color:#cc0000;" data-toggle="tooltip" data-placement="top"></i>':'<i class="text-success fa fa-check" title="'._('Newsletter_view_notsending').'" data-toggle="tooltip" data-placement="top"></i>'); ?>
                                        </td>
                                        <td class="text-center">
                                            <a class="fancybox-frame" href="newsletter_readmore.php?action=readmore&amp;id=<?php print $data['nId']; ?>" title="<?php print _("cms_see"); ?>: <?php print $data['nSubject']; ?>"  data-toggle="tooltip" data-placement="top"><i class="fa fa-table fancybox-frame"></i></a>
                                        </td>
										<td class="text-center">
                                            <?php if ($uac_edit){ ?><a href="newsletter.php?action=edit&amp;id=<?php print $data['nId']; ?>" title="<?php print _("cms_edit"); ?>: <?php print $data['nSubject']; ?>" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a><?php } ?>
										</td>
										<td class="text-center">
                                          <?php if ($uac_delete){ ?><a class="delete" href="newsletter.php?form=submit&amp;action=del&amp;id=<?php print $data['nId']; ?>" title="<?php print stripquote($data['nSubject']);?>"><i class="fa fa-trash"  rel="tooltip" title="<?php print _('cms_delete'); ?>: <?php print stripquote($data['nSubject']);?>" ></i></a><?php } ?>
										</td>
									</tr>
									<?php
											$num++;
										}
									}
									?>
								<tbody>
							</table>
						</div>
					<?php }elseif ($action=="add" and $uac_add){ ?>
						<a href="newsletter.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
						<h4><?php print _('pagecontent_addpage') ?></h4>
						<hr/>
						<form  action="newsletter.php?form=submit&amp;action=add" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">

                            <div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="title"><?php print _("newsletter_view_header"); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<img src="../assets/images/newsletter_header.jpg" alt="<?php print _("newsletter_view_header"); ?>" title="<?php print _("newsletter_view_header"); ?>">
		                        </div>
	                    	</div>
                            <div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="pic"><?php print _("newsletter_view_mainimage"); ?><span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="pic" type="file" id="pic" required class="file" >
		                        	<i><small><?php print _("newsletter_view_mainimageinfo"); ?></small></i>
		                        </div>
	                    	</div>
                            <div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="nsubject"><?php print _("newsletter_view_title"); ?><span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="nsubject" type="text" id="nsubject" required class="form-control col-md-7 col-xs-12" >
		                        </div>
	                    	</div>
	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="nemailmsg"><?php print _("newsletter_view_information"); ?><span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="nemailmsg" id="nemailmsg" class="tinymce form-control col-md-7 col-xs-12" required></textarea>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="descseo"><?php print _("newsletter_view_fileattachment"); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="file" name="files" class="file">
                                    <i><small><?php print _("newsletter_view_fileattachmentinfo"); ?></small></i>
		                        </div>
	                    	</div>
                            <input type="hidden" name="subscriber" id="subscriber" value="all" />
                            <div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="descseo"><?php print _("newsletter_view_footer"); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
                                    <?php printf (_('newsletter_footer'),SITE_NAME); ?> <a  href="#">unsubscribe now</a>.
		                        </div>
	                    	</div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                    <div class="checkbox" style="margin-bottom: 10px;">
                                        <label for="sendnow"><input type="checkbox" name="sendnow" id="sendnow" value="y" > <?php print _("newsletter_view_sendnow"); ?></label>
                                    </div>
                                </div>
                            </div>
							<hr/>
							<div class="form-group">
		                        <div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="Submit"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
		                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='newsletter.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                        </div>
	                    	</div>
						</form>
					<?php } elseif ($action=="edit" and $uac_edit){
						$sql="SELECT * FROM newsletter WHERE nId='$id'";
                        $query=query($sql);
                        $data=fetch($query);

                        list($startid,$endid)=explode('-',$data['nLastSend']);
                        list($dir, $img_file) = explode("/",$data['nMainImg']);
					?>
						<a href="newsletter.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
						<h4><?php print _("newsletter_view_editnewsletter"); ?></h4>
						<hr/>
						<form action="newsletter.php?form=submit&amp;action=edit&amp;id=<?php print $id; ?>" method="post" name="edit" id="edit" class="form-horizontal form-label-left">
                            <div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="title"><?php print _("newsletter_view_header"); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<img src="../assets/images/newsletter_header.jpg" alt="<?php print _("newsletter_view_header"); ?>" title="<?php print _("newsletter_view_header"); ?>">
		                        </div>
	                    	</div>
                            <div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="pic"><?php print _("newsletter_view_mainimage"); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
                                    
                                    <a href="../assets/newsletter/<?php print $dir .'/'. $img_file ?>" class="fancybox"><img src="../assets/newsletter/<?php print $dir .'/'. $img_file;?>" alt="<?php print _("newsletter_view_mainimage"); ?>" title="<?php print _("newsletter_view_mainimage"); ?>"></a><br><br>
		                        	<input name="pic" type="file" id="pic" class="file" >
		                        	<i><small><?php print _("newsletter_view_mainimageinfo"); ?></small></i>
		                        </div>
	                    	</div>
                            <div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="nsubject"><?php print _("newsletter_view_title"); ?><span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="nsubject" type="text" id="nsubject" required class="form-control col-md-7 col-xs-12" value="<?php print $data['nSubject'];?>" >
		                        </div>
	                    	</div>
	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="nemailmsg"><?php print _("newsletter_view_information"); ?><span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="nemailmsg" id="nemailmsg" class="tinymce form-control col-md-7 col-xs-12" required><?php print $data['nEmailMsg'];?></textarea>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="descseo"><?php print _("newsletter_view_fileattachment"); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
                                    <?php  $attc=getval("attachDir,attachFile","newsletter_attach","nId='".$data['nId']."'");
                                    if($attc['attachDir'] and $attc['attachFile']){ ?>
                                        <table>
                                        <tr valign="bottom">
                                            <td>
                                                <a href="../assets/newsletter/<?php print $attc['attachDir']."/".$attc['attachFile'] ?>" target="_blank" title="<?php print $attc['attachFile'] ?>">
                                                    <img src="../assets/images/icon-pdf.png" alt="<?php print $attc['attachFile'] ?>" /><br />
                                                </a>
                                            </td>
                                            <td><a href="../assets/newsletter/<?php print $attc['attachDir']."/".$attc['attachFile'] ?>" target="_blank" title="<?php print $attc['attachFile'] ?>"><?php print $attc['attachFile'] ?></a></td>
                                        </tr>
                                        </table><br/>
                                    <?php
                                      }
                                    ?>
                                    
                                    <input type="file" name="files" class="file">
                                    <i><small><?php print _("newsletter_view_fileattachmentinfo"); ?></small></i>
		                        </div>
	                    	</div>
	                  
	                   
	                    	<hr/>
							<div class="form-group">
		                        <div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="Submit"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
		                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='newsletter.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                        </div>
	                    	</div>
						</form>
					<?php }  ?>
					</div>
	        	</div>
	        </div>
        </div>
        <!-- END THE CONTENT OF PAGE HERE -->

       <?php include("com/com-footer.php"); ?>
	</div>
	<!-- END .main_container -->
</div>

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>