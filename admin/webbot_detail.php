<?php
	$page=35;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

	$sql="SELECT * FROM webbot WHERE wbId='$id'";
	$query=query($sql);
	$numofdata=rows($query);
	$data=fetch($query);

	if($data['wbStatus'] == 'unverified'){
		$data['wbStatus'] = '<b style="color:orange">UNVERIFIED</b>';
	} elseif($data['wbStatus'] == 'allow'){
		$data['wbStatus'] = '<b style="color:green">DIIZINKAN</b>';
	} else {
		$data['wbStatus'] = '<b style="color:red">DICEKAL</b>';
	}
				
	if ($data['wbDesc']){
		$wbdesc     = json_decode($data['wbDesc'],true);
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _('webbot_pagetitle'); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>

</head>
<body class="flyctn" style="background:#F9F9F9;">

<div class="container body">
    <div class="main_container">

        <div class="row">
            <div class="col-md-12" style="width:100%;">

            <div class="table-responsive">
                <table width="650" border="0" cellspacing="0" cellpadding="5">
                    <tr>
                        <td width="150"><strong><?php print _('webbot_ipaddress'); ?></strong></td>
                        <td width="1">:</td>
                        <td><strong><?php print $data['wbIpAddress'];?></strong></td>
                    </tr>
                    <?php
                      	if ($data['wbDesc']){
                    		    foreach ($wbdesc as $key=>$value){
                    ?>
                    <tr>
                        <td><strong><?php print ucwords($key); ?></strong></td>
                        <td>:</td>
                        <td><?php print ucwords($value); ?></td>
                    </tr>
                    <?php
                            }
                	      }
                    ?>
                    <tr>
                        <td><strong><?php print _('webbot_hits'); ?></strong></td>
                        <td>:</td>
                        <td><?php print number_format($data['wbCount'], 0, ',', '.');?></td>
                    </tr>
                    <tr>
                        <td><strong><?php print _('webbot_lastvisit'); ?></strong></td>
                        <td>:</td>
                        <td><?php print $data['wbLastLog'] ? date('d/m/Y H:i:s', $data['wbLastLog']):'-';?></td>
                    </tr>  
                    <tr>
                        <td><strong><?php print _('webbot_status'); ?></strong></td>
                        <td>:</td>
                        <td><?php print $data['wbStatus'];?></td>
                    </tr>
                </table>
            </div>

            </div>
        </div>
    </div>
</div>

</body>
</html>
