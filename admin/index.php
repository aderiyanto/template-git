<?php
	$page="home";
	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/mainmod.php");	
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");
	include("../modz/mainmod-extend.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _('homepage_pagetitle'); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>

<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">


<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>

<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

</head>
<body class="nav-md">
<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">
        	

			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">

        			<div class="row tile_count">
        				<div class="col-md-2 col-sm-2 col-xs-12 tile_stats_count">
			                <span class="count_top"><i class="fa fa-bookmark"></i> <?php print _('homepage_licence_to'); ?></span>
			                <div class="flag">
			                	<?php print $lc_uri; ?>
			                	<br/>
			                </div>
			            </div>
			            <div class="col-md-2 col-sm-2 col-xs-12 tile_stats_count">
			                <span class="count_top"><i class="fa fa-bookmark"></i> <?php print _('homepage_licence_langsupport'); ?></span>
			                <div class="flag">
			                	<?php
									foreach($lc_lang as $key=>$value){
									?>
									<img width="19" src="/assets/images/flag-<?php print $value;?>-edit.png" align="absmiddle" /> <?php print localename($value); ?><br/> 
									<?php
									}
								?>
			                </div>
			            </div>
						<?php
							$sql="SELECT * FROM counter WHERE conId= :conId";
							$data=$dbh->prepare($sql);
							$conId=1;
							$data->bindParam(':conId',$conId);
							$data->execute();
							$data=$data->fetch(PDO::FETCH_ASSOC);
							$currHit=$data['conValue'];
							
							$sql="SELECT * FROM counter WHERE conId= :conId";
							$data=$dbh->prepare($sql);
							$conId=2;
							$data->bindParam(':conId',$conId);
							$data->execute();
							$data=$data->fetch(PDO::FETCH_ASSOC);
							$totalHit=$data['conValue'];

							$commentnews = countdata("newsinfo_comment","cStatus='y' AND cSpam='n'");
					    ?>
						<div class="col-md-2 col-sm-2 col-xs-12 tile_stats_count">
							<div class="count_top"><i class="fa fa-user"></i> Visitors</div>
							<div class="count"><?php print number_format($currHit,0,".",","); ?></div>
						</div>

						<div class="col-md-2 col-sm-2 col-xs-12 tile_stats_count">
							<div class="count_top"><i class="fa fa-eye"></i> Page Views</div>
							<div class="count"><?php print number_format($totalHit,0,".",","); ?></div>
						</div>

						<div class="col-md-2 col-sm-2 col-xs-12 tile_stats_count">
							<div class="count_top"><i class="fa fa-wifi"></i> Online Now</div>
							<div class="count">
							<?php
								$sql="SELECT COUNT(*) AS total FROM onlinenow";
								$data=$dbh->prepare($sql);
								$data->execute();
								$data=$data->fetch(PDO::FETCH_ASSOC);
								$total=$data['total'];			 
								print number_format($total,0,",",".");			
					        ?>
					        </div>
						</div>

					</div>

				</div>
			</div>

			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12">
	            	<div class="x_panel tile">

	            		<div class="x_title">
	            			<h2><?php print _('homepage_stat_title'); ?></h2>
	            			<ul class="nav navbar-right panel_toolbox">
			                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
			                </ul>
	            			<div class="clearfix"></div>
	            		</div>

	            		<div class="x_content">
	            			<div class="dashboard-widget-content">
	            				<div class="row">
                      				<div class="col-md-6 col-sm-6 col-xs-12">
                      					
                      					<div class="widget_summary">
				                          	<div class="w_left w_50">
					                            <a href="pagecontent.php"><span class="row col-xs-11 col-md-11">Page Content</span></a>
					                            <span class="col-xs-1 col-md-1">:</span>
					                         </div>
				                         	<div class="w_right w_50">
				                           		<span>
				                           			<?php
														$sql="SELECT COUNT(*) AS total FROM content";
														$data=$dbh->prepare($sql);
														$data->execute();
														$data=$data->fetch(PDO::FETCH_ASSOC);
														$total=$data['total'];
														print number_format($total,0,",",".");
													?>
				                           		</span>
				                          	</div>
				                          	<div class="clearfix"></div>
				                        </div>
				                        <div class="widget_summary">
				                          	<div class="w_left w_50">
					                            <a href="changeban.php"><span class="row col-xs-11 col-md-11">Main Banner</span></a>
					                            <span class="col-xs-1 col-md-1">:</span>
					                         </div>
				                         	<div class="w_right w_50">
				                           		<span>
				                           			<?php
														$sqlban="SELECT COUNT(*) AS total FROM mbanner";
														$data=$dbh->prepare($sqlban);
														$data->execute();
														$databan=$data->fetch(PDO::FETCH_ASSOC);
														$totalban=$databan['total'];
													    print number_format($totalban,0,",",".");
													?>
				                           		</span>
				                          	</div>
				                          	<div class="clearfix"></div>
				                        </div>
				                        <div class="widget_summary">
				                          	<div class="w_left w_50">
					                            <a href="infoupdate.php"><span class="row col-xs-11 col-md-11">Info/Update</span></a>
					                            <span class="col-xs-1 col-md-1">:</span>
					                         </div>
				                         	<div class="w_right w_50">
				                           		<span>
				                           			<?php
														$sql="SELECT COUNT(*) AS total FROM ticker";
														$data=$dbh->prepare($sql);
														$data->execute();
														$data=$data->fetch(PDO::FETCH_ASSOC);
														$total=$data['total'];
													    print number_format($total,0,",",".");
													?>
				                           		</span>
				                          	</div>
				                          	<div class="clearfix"></div>
				                        </div>
				                        <div class="widget_summary">
				                          	<div class="w_left w_50">
					                            <a href="gallery_album.php"><span class="row col-xs-11 col-md-11">Photo Gallery</span></a>
					                            <span class="col-xs-1 col-md-1">:</span>
					                         </div>
				                         	<div class="w_right w_50">
				                           		<span>
				                           			<?php
														$sql="SELECT COUNT(*) AS total FROM gallery_album";
														$data=$dbh->prepare($sql);
														$data->execute();
														$data=$data->fetch(PDO::FETCH_ASSOC);
														$total=$data['total'];
													    print number_format($total,0,",",".");
													?> Album, 
											        <?php
														$sql="SELECT COUNT(*) AS total FROM gallery_photo";
														$data=$dbh->prepare($sql);
														$data->execute();
														$data=$data->fetch(PDO::FETCH_ASSOC);
														$total=$data['total'];
													    print number_format($total,0,",",".");
													?> Photo
				                           		</span>
				                          	</div>
				                          	<div class="clearfix"></div>
				                        </div>
				                        <div class="widget_summary">
				                          	<div class="w_left w_50">
					                            <a href="newsinfo.php"><span class="row col-xs-11 col-md-11">News</span></a>
					                            <span class="col-xs-1 col-md-1">:</span>
					                         </div>
				                         	<div class="w_right w_50">
				                           		<span>
				                           			<?php
														$sql="SELECT COUNT(*) AS total FROM newsinfo";
														$data=$dbh->prepare($sql);
														$data->execute();
														$data=$data->fetch(PDO::FETCH_ASSOC);
														$total=$data['total'];
													    print number_format($total,0,",",".");
													?>
				                           		</span>
				                          	</div>
				                          	<div class="clearfix"></div>
				                        </div>
				                        <div class="widget_summary">
				                          	<div class="w_left w_50">
					                            <a href="newsinfocat.php"><span class="row col-xs-11 col-md-11">News: Category</span></a>
					                            <span class="col-xs-1 col-md-1">:</span>
					                         </div>
				                         	<div class="w_right w_50">
				                           		<span>
				                           			<?php
														$sql="SELECT COUNT(*) AS total FROM newsinfo_cat";
														$data=$dbh->prepare($sql);
														$data->execute();
														$data=$data->fetch(PDO::FETCH_ASSOC);
														$total=$data['total'];
													    print number_format($total,0,",",".");
													?>
				                           		</span>
				                          	</div>
				                          	<div class="clearfix"></div>
				                        </div>
				                        <div class="widget_summary">
				                          	<div class="w_left w_50">
					                            <a href="video.php"><span class="row col-xs-11 col-md-11">Video</span></a>
					                            <span class="col-xs-1 col-md-1">:</span>
					                         </div>
				                         	<div class="w_right w_50">
				                           		<span>
				                           			<?php
														$sql="SELECT COUNT(*) AS total FROM video";
														$data=$dbh->prepare($sql);
														$data->execute();
														$data=$data->fetch(PDO::FETCH_ASSOC);
														$total=$data['total'];
													    print number_format($total,0,",",".");
													?>
				                           		</span>
				                          	</div>
				                          	<div class="clearfix"></div>
				                        </div>
				                        <div class="widget_summary">
				                          	<div class="w_left w_50">
					                            <a href="videocat.php"><span class="row col-xs-11 col-md-11">Video: Category</span></a>
					                            <span class="col-xs-1 col-md-1">:</span>
					                         </div>
				                         	<div class="w_right w_50">
				                           		<span>
				                           			<?php
														$sql="SELECT COUNT(*) AS total FROM video_cat";
														$data=$dbh->prepare($sql);
														$data->execute();
														$data=$data->fetch(PDO::FETCH_ASSOC);
														$total=$data['total'];
													    print number_format($total,0,",",".");
													?>
				                           		</span>
				                          	</div>
				                          	<div class="clearfix"></div>
				                        </div>
                      				</div>

                      				<div class="col-md-6 col-sm-6 col-xs-12">
                      					<div class="widget_summary">
				                          	<div class="w_left w_50">
					                            <a href="img.php"><span class="row col-xs-11 col-md-11">Image Manager</span></a>
					                            <span class="col-xs-1 col-md-1">:</span>
					                         </div>
				                         	<div class="w_right w_50">
				                           		<span>
				                           			<?php
													    $sql="SELECT COUNT(*) AS total FROM imgbank";
													    $data=$dbh->prepare($sql);
														$data->execute();
													    $data=$data->fetch(PDO::FETCH_ASSOC);
													    $total=$data['total'];			 
											 		    print number_format($total,0,",",".");			
												    ?>
				                           		</span>
				                          	</div>
				                          	<div class="clearfix"></div>
				                        </div>
				                        <div class="widget_summary">
				                          	<div class="w_left w_50">
					                            <a href="file_manager.php"><span class="row col-xs-11 col-md-11">File Manager</span></a>
					                            <span class="col-xs-1 col-md-1">:</span>
					                         </div>
				                         	<div class="w_right w_50">
				                           		<span>
				                           			<?php
													    $sql="SELECT COUNT(*) AS total FROM file_manager";
													    $data=$dbh->prepare($sql);
														$data->execute();
													    $data=$data->fetch(PDO::FETCH_ASSOC);
													    $total=$data['total'];			 
											 		    print number_format($total,0,",",".");			
												    ?>
				                           		</span>
				                          	</div>
				                          	<div class="clearfix"></div>
				                        </div>
				                        <div class="widget_summary">
				                          	<div class="w_left w_50">
					                            <a href="glossary.php"><span class="row col-xs-11 col-md-11">Word List</span></a>
					                            <span class="col-xs-1 col-md-1">:</span>
					                         </div>
				                         	<div class="w_right w_50">
				                           		<span>
				                           			<?php
													    $sql="SELECT COUNT(*) AS total FROM glossary";
													    $data=$dbh->prepare($sql);
														$data->execute();
													    $data=$data->fetch(PDO::FETCH_ASSOC);
													    $total=$data['total'];			 
											 		    print number_format($total,0,",",".");			
												    ?>
				                           		</span>
				                          	</div>
				                          	<div class="clearfix"></div>
				                        </div>
				                        <div class="widget_summary">
				                          	<div class="w_left w_50">
					                            <a href="email_queue.php"><span class="row col-xs-11 col-md-11">Email Queue</span></a>
					                            <span class="col-xs-1 col-md-1">:</span>
					                         </div>
				                         	<div class="w_right w_50">
				                           		<span>
				                           			<?php
													    $sql="SELECT COUNT(*) AS total FROM email_queue";
													    $data=$dbh->prepare($sql);
														$data->execute();
													    $data=$data->fetch(PDO::FETCH_ASSOC);
													    $total=$data['total'];			 
											 		    print number_format($total,0,",",".");			
												    ?>
				                           		</span>
				                          	</div>
				                          	<div class="clearfix"></div>
				                        </div>
				                        <div class="widget_summary">
				                          	<div class="w_left w_50">
					                            <a href="email_blacklist.php"><span class="row col-xs-11 col-md-11">Email Blacklist</span></a>
					                            <span class="col-xs-1 col-md-1">:</span>
					                         </div>
				                         	<div class="w_right w_50">
				                           		<span>
				                           			<?php
													    $sql="SELECT COUNT(*) AS total FROM email_blacklist";
													    $data=$dbh->prepare($sql);
														$data->execute();
													    $data=$data->fetch(PDO::FETCH_ASSOC);
													    $total=$data['total'];			 
											 		    print number_format($total,0,",",".");		
												    ?>
				                           		</span>
				                          	</div>
				                          	<div class="clearfix"></div>
				                        </div>
                      				</div>
                      				<!-- /col-md-6 col-sm-6 col-xs-12 -->
                      			</div>
	            			</div>
	            		</div>
	            		<!-- /.x_content -->
	            	</div>
	            </div>

	            <div class="col-md-6 col-sm-12 col-xs-12">
	            	<div class="x_panel tile">
		                <div class="x_title">
	                		<h2><?php print _('infoupdate_pagetitle'); ?></h2>
		                	<ul class="nav navbar-right panel_toolbox">
			                    <li>
			                    	<a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
			                    </li>
		                	</ul>
		                	<div class="clearfix"></div>
		                </div>
		                <div class="x_content">
		                	<div class="dashboard-widget-content table-responsive">
			                    <table class="table table-hover">
			                    	<thead>
				                        <tr>
				                        	<th><?php print _('infoupdate_pagetitle'); ?></th>
				                        	<th width="127" class="text-center"><?php print _('infoupdate_date'); ?></th>
				                        </tr>
			                    	</thead>
			                    	<tbody>
			                    		<?php
											$sql="SELECT * FROM ticker ORDER BY tickDateTime DESC";
											$query=query($sql);
											$numofdata=rows($query);

											//$sql.=' LIMIT '.$offset.','.$numPerPage;
											$query=query($sql);

											if ($numofdata<1){
										?>
										<tr>
											<td colspan="5"><div class="text-center"><br /><?php print _('infoupdate_notavailable'); ?><br /><br /></div></td>
										</tr>
										<?php 
											} else {
												while ($data=fetch($query)){
										?>
				                        <tr>
				                        	<td><?php print output($data['tickNews']); ?></td>
				                        	<td class="text-center"><?php print dateformat($data['tickDateTime']); ?></td>
				                        </tr>
				                        <?php 
				                        	}
				                        } ?>
			                    	</tbody>
			                    </table>
		                	</div>
		                </div>
	            	</div>
	            </div>


        	</div>
        	<!-- /.row .tile_count -->
        </div>
        <!-- /page content -->

        <?php include("com/com-footer.php"); ?>
	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>