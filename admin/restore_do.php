<?php
	$page=8;

	//includes all files necessary to support operations
	include($argv[1]."/modz/config-main.php");
	include($argv[1]."/modz/config.php");
	include($argv[1]."/modz/license.php");
	include($argv[1]."/modz/errormsg.php");
	include($argv[1]."/modz/mainmod.php");
	include($argv[1]."/modz/connic.php");
	include($argv[1]."/modz/getall-admin.php");

	$fname = $argv[3];
	$cmd = ($argv[2]!='unknown' ? $argv[2].DIRECTORY_SEPARATOR:'') .'mysql -h '.getconfig('DB_HOST').' -u '.getconfig('DB_USER').' -p'.getconfig('DB_PASSWORD').' '.getconfig('DB_NAME').' < "'.$argv[1].'/assets/backup/'.$fname.'.sql"';
	$exced = cli($cmd,FALSE);

	if($exced[1]!=0){
		#mean that process failed, system will restore originl data backup
		$cmd = ($argv[2]!='unknown' ? $argv[2].DIRECTORY_SEPARATOR:'') .'mysql -h '.getconfig('DB_HOST').' -u '.getconfig('DB_USER').' -p'.getconfig('DB_PASSWORD').' '.getconfig('DB_NAME').' < "'.$argv[1].'/assets/backup/tmp-'.$fname.'.sql"';
		$exced2 = cli($cmd,FALSE);
	}

	$tmp = $argv[1]."/assets/backup/{$fname}.wb";

	$f = fopen($tmp,'w');
	fwrite($f,($exced[1]==0 ? 'ok':'error'));
	fclose($f);
?>
