<?php
	$page=24;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

	#check language license
	if($lang and !in_array($lang, $lc_lang) and ($action=='adden' or $action=='editen')){
		header('location:videocat.php');
		exit;
	}

	if ($form=="submit"){
		if ($action=="addcat" and $uac_add){
			if (empty($name)){
				$error=errorlist(2);
			}

			//get permalink
			$permalink=permalink($name);
			$exist=checkDuplicateRecord('video_cat',"catPermalink='$permalink'");
			if($exist){
				$error.=errorlist(28);
			}

			//insert data
			if (!$error){
				//get next category id
				$catid = nextid("catId","video_cat");
				//get the next sorting value
				$sortid= nextsort("video_cat","catParentId","$parentcategory","catSort");

				//insert to table video_cat
				$sql="INSERT INTO video_cat VALUES ($catid,'$name','$catdesc', '$permalink', $parentcategory, $sortid)";
				$query=query($sql);
			}
		}elseif ($action=="editcat" and $uac_edit){
			if (empty($name)){
				$error=errorlist(2);
			}

			//get permalink
			$permalink=permalink($name);
			$exist=checkDuplicateRecord('video_cat',"catPermalink='$permalink' AND catId!=". $catid ."");
			if($exist){
				$error.=errorlist(28);
			}

			##Category parent rule
			if($parentcategory!=0){
				$check=checkDuplicateRecord('video_cat',"catParentId=$catid");
				if($check){
					$error.=errorlist(46);//Error list nya ditambahkan menjadi $msg[xx] = "Kategori tidak dapat dipindahkan ke kategori lain, karena masih ada sub kategori di bawahnya. Hapus kategori di bawahnya terlebih dahulu";
				}
			}
			##End Category parent rule

			if (!$error){
				//update data
				$sortid= nextsort("video_cat","catParentId","$parentcategory","catSort");
				$sql="UPDATE video_cat SET catName='$name', catDesc='$catdesc', catPermalink='$permalink', catParentId=$parentcategory, catSort=$sortid WHERE catId=" . $catid;
				$query=query($sql);
			}
		}elseif($action=="delcat" and $uac_delete){
			//check wheter category have video info
			$sql="SELECT COUNT(*) AS total FROM video WHERE catId=". $catid;
			$query=query($sql);
			$data=fetch($query);
			$total=$data['total'];

			if ($total>0){
				$error.=errorlist(30);
			}
			$sql	="SELECT * FROM video_cat WHERE catParentId=" . $catid;
			$query	=query($sql);
			$rows	=rows($query);

			if($rows > 0){
				$error.=errorlist(31);
			}
			if(!$error){
				$sql="DELETE FROM video_cat WHERE catId='" . $catid."'";
				$query=query($sql);

				if($query){
					$sql="DELETE FROM video_cat_lang WHERE catId='" . $catid."'";
					$query=query($sql);
				}
			}
		}elseif ($action=="updatesort"){
			foreach ($s as $id=>$sortnum){
				$sql="UPDATE video_cat SET catSort=$sortnum WHERE catId='" . $id . "'";
				$query=query($sql);
			}
		}elseif($action=="adden" and $uac_add){
			if (empty($name)){
				$error=errorlist(2);
			}
			else{
				$exists = countdata('video_cat',"catId='$catid'");
				if ($exists){
					#add data
					$sql="INSERT INTO video_cat_lang VALUES ($catid,'$name','$catdesc','$lang')";
					$query=query($sql);
				}
			}
		}elseif($action=="editen" and $uac_edit){
			if (empty($name)){
				$error=errorlist(2);
			}else{
				//update data
				$sql="UPDATE video_cat_lang SET catName='$name', catDesc='$catdesc' WHERE catId='" . $catid."' AND lang='$lang'";
				$query=query($sql);
			}
		}

		//check whether query was successful
		if (!$query){
			$error=errorlist(3);
		}

		if ($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:videocat.php");
			}else{
				print "ok";
			}
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _('videocat_pagetitle'); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">


<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce_simple.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('#result').hide();
	validate('#result','#add','videocat.php');
	validate('#result','#edit','videocat.php');
});
</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('videocat_pagetitle'); ?></h3>
			                <p><?php print _('videocat_pagedesc'); ?></p>
			            </div>
		            </div>
	        	</div>
        	</div>

        	<?php if ($uac_add){ ?>
        	<div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                	<div class="pull-right">
                		<a href="videocat.php?action=addcat" class="btn btn-primary"><i class="fa fa-plus"></i> <?php print _('videocat_addvideocategory'); ?></a>
                	</div>
                </div>
        	</div>
        	<?php } ?>

        	<!-- THE CONTENT OF PAGE HERE -->
        	<div class="row">
	        	<div class="col-md-12">
	        		<?php if ($action==""){ ?>
        			<form action="videocat.php?form=submit&amp;action=updatesort" method="post" name="sort">
        				<?php if($uac_edit){ ?>
        				<div class="block-break">
                            <button type="submit" name="updatesort" class="btn btn-info"><i class="fa fa-refresh"></i> <?php print _('videocat_updatelist'); ?></button>
				        </div>
				        <?php } ?>

				        <div class="table-responsive">
	        				<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
	        					<thead>
	        						<tr>
										<th width="50"><?php print _('videocat_list'); ?></th>
										<th colspan="2"><?php print _('videocat_category'); ?></th>
										<th width="120"><?php print _('videocat_totalvideo'); ?></th>
										<?php if(count($lc_lang)>1){ ?><th style="width:40px;" class="text-center"><i rel="tooltip" title="<?php echo _('multi_language_view_language'); ?>" class="fa fa-language"></i></th><?php }?>
										<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
										<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
									</tr>
	        					</thead>
	        					<tbody>
	        						<?php
									if(empty($pg)){
										$pg=1;
									}

									$numPerPage=20;
									$offset=($pg-1)*$numPerPage;
									$num=1;
									$num=$num+(($pg-1)*$numPerPage);
									$sql="SELECT * FROM video_cat WHERE catParentId=0 ORDER BY catSort ASC";
									$query=query($sql);
									$numofdata=rows($query);
									$sql.=' LIMIT '.$offset.','.$numPerPage;
									$query=query($sql);
									if($numofdata<1){
									?>
										<tr>
											<td colspan="7" class="asterik"><div class="text-center"><br />
											<?php print _('videocat_categoryvideonotavailable'); ?><br /><br /></div></td>
										</tr>
									<?php
									}else{
										while ($data=fetch($query)){
											?>
											<tr>
												<td class="text-center">
													<?php if($uac_edit){ ?>
													<input type="text" size="3" name="s[<?php print $data['catId']; ?>]" value="<?php print $data['catSort']; ?>" class="form-control text-center" />
													<?php }else{ print $num;} ?>
												</td>
												<td colspan="2"><strong><a href="video.php?cid=<?php print $data['catId']; ?>"><?php print $data['catName']; ?></a></strong></td>
												<td class="text-center"><?php print countdata("video","catId='".$data['catId']."'");?></td>
												<?php
												if(count($lc_lang)>1){
													?>
													<td class="text-center">
														<?php
														foreach($lc_lang as $key=>$value){
															$exist=countdata("video_cat_lang","catId='".$data['catId']."' and lang='$value'");
															if($value==$lc_lang_default) continue;
															if($exist and $uac_edit){
																?><a href="videocat.php?action=editen&amp;lang=<?php print $value;?>&catid=<?php print $data['catId']; ?>" rel="tooltip" title="<?php print _('videocat_editvers'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-edit.png" alt="<?php print $value;?>" border="0" /></a><?php
															}
															elseif($uac_add){
																?><a href="videocat.php?action=adden&amp;lang=<?php print $value;?>&catid=<?php print $data['catId']; ?>" rel="tooltip" title="<?php print _('videocat_addvers'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-add.png" alt="<?php print $value;?>" border="0" /></a><?php
															}
														}?>
													</td>
													<?php
												}?>
												<td class="text-center"><?php if ($uac_edit){ ?><a href="videocat.php?action=editcat&amp;catid=<?php print $data['catId']; ?>"><i title="<?php print _('cms_edit'); ?>: <?php print stripquote($data['catName']); ?>" rel="tooltip" class="fa fa-edit"></i></a><?php } ?></td>
												<td class="text-center"><?php if ($uac_delete){ ?><a class="delete" href="videocat.php?form=submit&action=delcat&catid=<?php print $data['catId']; ?>" title="<?php print stripquote($data['catName']); ?>"><i rel="tooltip" title="<?php print _('cms_delete'); ?>: <?php print stripquote($data['catName']); ?>" class="fa fa-trash"></i></a><?php } ?></td>
											</tr>
										<?php
											$sql2="SELECT * FROM video_cat WHERE catParentId=". $data['catId'] ." ORDER BY catSort ASC";
											$query2=query($sql2);
											while($data2=fetch($query2)){
												?>
												<tr>
													<td>&nbsp;</td>
													<td width="50" class="text-center">
														<?php if($uac_edit){ ?>
														<input type="text" size="3" name="s[<?php print $data2['catId']; ?>]" value="<?php print $data2['catSort']; ?>" class="form-control text-center" />
														<?php }else{ print $data2['catSort'];} ?>
													</td>
													<td><strong><a href="video.php?cid=<?php print $data2['catId']; ?>"><?php print $data2['catName']; ?></a></strong></td>
													<td class="text-center"><?php print countdata("video","catId='".$data2['catId']."'");?></td>
													<?php
													if(count($lc_lang)>1){
														?>
														<td class="text-center">
															<?php
															foreach($lc_lang as $key=>$value){
																$exist=countdata("video_cat_lang","catId='".$data2['catId']."' and lang='$value'");
																if($value==$lc_lang_default) continue;
																if($exist and $uac_edit){
																	?><a href="videocat.php?action=editen&amp;lang=<?php print $value;?>&catid=<?php print $data2['catId']; ?>" rel="tooltip" title="<?php print _('videocat_editvers'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-edit.png" alt="<?php print $value;?>" border="0" /></a><?php
																}
																elseif($uac_add){
																	?><a href="videocat.php?action=adden&amp;lang=<?php print $value;?>&catid=<?php print $data2['catId']; ?>" rel="tooltip" title="<?php print _('videocat_addvers'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-add.png" alt="<?php print $value;?>" border="0" /></a><?php
																}
															}?>
														</td>
														<?php
													}?>
													<td class="text-center"><?php if ($uac_edit){ ?><a href="videocat.php?action=editcat&amp;catid=<?php print $data2['catId']; ?>"><i title="<?php print _('cms_edit'); ?>: <?php print stripquote($data2['catName']); ?>" rel="tooltip" class="fa fa-edit"></i></a><?php } ?></td>
													<td class="text-center"><?php if ($uac_delete){ ?><a class="delete" href="videocat.php?form=submit&action=delcat&catid=<?php print $data2['catId']; ?>" title="<?php print stripquote($data2['catName']); ?>"><i title="<?php print _('cms_delete'); ?>: <?php print stripquote($data2['catName']); ?>" rel="tooltip" class="fa fa-trash"></i></a><?php } ?></td>
												</tr>
												<?php
											}
											$num++;
										}
									}
									?>
	        					</tbody>
	        				</table>
	        			</div>
	        			<?php 
	        				$options['total']=$numofdata;
							$options['filename']='videocat.php';
							$options['qualifier']='videocat.php';
							$options['pg']=$pg;
							$options['numPerPage']=$numPerPage;
							$options['style']=1;
							$options['addquery']=TRUE;
							paging($options);
	        			?>
        			</form>
        			<?php } elseif ($action=="addcat" and $uac_add){ ?>
        				<a href="videocat.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
						<h4><?php print _('videocat_mainaddcategory'); ?></h4>
						<hr/>

						<form action="videocat.php?action=addcat&amp;form=submit" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">
							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="parentcategory"><?php print _('videocat_maincategory'); ?>
		                        </label>
		                        <div class="col-md-2 col-sm-6 col-xs-12">
		                        	<select name="parentcategory" id="parentcategory" class="form-control">
										<option value="0"><?php print _('videocat_selectmaincategory'); ?></option>
										<?php
											$sqlx="SELECT * FROM video_cat WHERE catParentId=0 ORDER BY catId";
											$queryx=query($sqlx);
											while ($datax=fetch($queryx)){
												$datax=formoutput($datax);
										?>
										<option value="<?php print $datax['catId']; ?>"><?php print $datax['catName']; ?></option>
										<?php
											}
										?>
									</select>
		                        </div>
		                	</div>
		                	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _('videocat_namecategory'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="name" type="text" id="name" size="50" required class="form-control" />
		                        </div>
		                	</div>
		                	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="catdesc"><?php print _('videocat_desccategory'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="catdesc" id="catdesc" cols="80" rows="5" class="form-control"></textarea>
		                        </div>
		                	</div>

		                	<hr/>

		                	<div class="form-group">
		                        <div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
		                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='videocat.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                        </div>
	                    	</div>
						</form>
					<?php } elseif ($action=="editcat" and $uac_edit){
							$sql="SELECT * FROM video_cat WHERE catId=" . $catid;
							$query=query($sql);
							$data=fetch($query);
							$data=formoutput($data);
					?>
						<a href="videocat.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
						<h4><?php print _('videocat_editcategory'); ?></h4>
						<hr/>

						<form action="videocat.php?action=editcat&amp;form=submit&amp;catid=<?php print $catid; ?>" method="post" enctype="multipart/form-data" name="edit" id="edit" class="form-horizontal form-label-left">
							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="parentcategory"><?php print _('videocat_maincategory'); ?>
		                        </label>
		                        <div class="col-md-2 col-sm-6 col-xs-12">
		                        	<select name="parentcategory" id="parentcategory" class="form-control">
										<option value="0"><?php print _('videocat_selectmaincategory'); ?></option>
										<?php
											$sqlx="SELECT * FROM video_cat WHERE catParentId='0' AND catId!='$catid' ORDER BY catId";
											$queryx=query($sqlx);
											while ($datax=fetch($queryx)){
												$datax=formoutput($datax);
												?>
												<option value="<?php print $datax['catId']; ?>"<?php print ($data['catParentId']==$datax['catId'])?" selected=\"selected\"":""; ?>><?php print $datax['catName']; ?></option>
												<?php
											}
										?>
									</select>
		                        </div>
		                	</div>
		                	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _('videocat_namecategory'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="name" type="text" id="name" size="50" value="<?php print $data['catName']; ?>" required class="form-control" />
		                        </div>
		                	</div>
		                	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="catdesc"><?php print _('videocat_desccategory'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="catdesc" id="catdesc" cols="80" rows="5" class="form-control"><?php print $data['catDesc']; ?></textarea>
		                        </div>
		                	</div>

		                	<hr/>

		                	<div class="form-group">
		                        <div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> <?php print _('cms_updatebuton'); ?></button>
		                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='videocat.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                        </div>
	                    	</div>

	                    	<br/><br/><br/>
						</form>
					<?php }elseif($action=="adden" and $uac_add){
						$sql="SELECT catName FROM video_cat WHERE catId='". $catid."'";
						$query=query($sql);
						$data=fetch($query);
						$data=formoutput($data);
						$catname=stripquote($data['catName']);
					?>
						<a href="video.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
						<h4><?php print _('videocat_addcategoryvers'); ?> <?php print localename($lang);?> &raquo; <?php print $catname; ?></h4>
						<hr/>
						<form action="videocat.php?action=adden&amp;form=submit&amp;lang=<?php print $lang;?>&amp;catid=<?php print $catid; ?>" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">
							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _('videocat_namecategory'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="name" type="text" id="name" size="50" required class="form-control" />
		                        </div>
		                	</div>
		                	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="catdesc"><?php print _('videocat_desccategory'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="catdesc" id="catdesc" cols="80" rows="5" class="form-control"></textarea>
		                        </div>
		                	</div>

		                	<hr/>

		                	<div class="form-group">
		                        <div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
		                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='videocat.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                        </div>
	                    	</div>
						</form>
					<?php }elseif($action=="editen" and $uac_edit){
						$sql="SELECT catName FROM video_cat WHERE catId='$catid'";
						$query=query($sql);
						$data=fetch($query);
						$data=formoutput($data);
						$catname=stripquote($data['catName']);

						$sql="SELECT * FROM video_cat_lang WHERE catId='$catid' AND lang='$lang'";
						$query=query($sql);
						$data=fetch($query);
						$data=formoutput($data);
					?>
						<a href="video.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
						<h4><?php print _('videocat_editcategoryvers'); ?> <?php print localename($lang);?> &raquo; <?php print $catname; ?></h4>
						<hr/>
						<form action="videocat.php?action=editen&amp;form=submit&amp;lang=<?php print $lang;?>&amp;catid=<?php print $catid; ?>" method="post" enctype="multipart/form-data" name="edit" id="edit" class="form-horizontal form-label-left">
							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _('videocat_namecategory'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input name="name" type="text" id="name" size="50" required class="form-control" value="<?php print formoutput($data['catName']); ?>" />
		                        </div>
		                	</div>
		                	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="catdesc"><?php print _('videocat_desccategory'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="catdesc" id="catdesc" cols="80" rows="5" class="form-control"><?php print formoutput($data['catDesc']); ?></textarea>
		                        </div>
		                	</div>

		                	<hr/>

		                	<div class="form-group">
		                        <div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_updatebuton'); ?>"><i class="fa fa-refresh"></i> <?php print _('cms_updatebuton'); ?></button>
		                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='videocat.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                        </div>
	                    	</div>
						</form>
	        		<?php } ?>
	        	</div>
	        </div>
	        <!-- END THE CONTENT OF PAGE HERE -->


		</div>
		<!-- END THE CONTENT OF PAGE HERE -->

		<?php include("com/com-footer.php"); ?>

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>