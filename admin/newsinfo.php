<?php
	$page=21;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config-image.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/mainmod-extend.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");




	#check language license
	if($lang and !in_array($lang, $lc_lang) and ($action=='adden' or $action=='editen')){
		header('location:newsinfo.php');
		exit;
	}

	//set the restrictions
	$allowedType=array("image/pjpeg","image/jpeg");
	$realExt = array('jpeg','jpg');

	if ($form=="submit"){
		//Populate images info
		$pic=$_FILES['pic'];

		if($action=="addnews" and $uac_add){
			if (empty($category) or empty($type) or empty($datetime) or empty($title) or empty($headline) or count($content)==0){
				$error=errorlist(2);
			}else{
				//convert date & time to mktime
				list($date,$time)=explode(" ",$datetime);
				list($day,$month,$year)=explode("/",$date);
				list($hour,$minut)=explode(":",$time);
				$datetime2=mktime($hour,$minut,0,$month,$day,$year);
				if($pic['name']){
					if(in_array($pic['type'], $allowedType)){
						$fileExt = array_keys($allowedType, $pic['type']);
						$fileExt = $realExt[$fileExt[0]];

						if(is_uploaded_file($pic['tmp_name'])){
							//make sure it is really really image
							$img=getimagesize($pic['tmp_name']);
							if(!in_array($pic['type'],$allowedType) or !(is_numeric($img[0]) or is_numeric($img[1]) or $img[0]>0 or $img[1]>0)){
								//make sure it is allowed
								$error.=errorlist(13);
							}
						} else{
							$error.=errorlist(27);
						}
					} else{
						$error.=errorlist(13);
					}

					//get filename
					$fname = str_replace('.','',$pic['name']).'.'.$fileExt;
				}

				if(!$error){
					//get next newsId
					$nextid=nextid("newsId","newsinfo");

					//create permalink
					$permalink=permalink($title,$nextid);

					//For big image
					if(!empty($pic['name'])){
						$img=uploadit($fname,$pic['tmp_name'],"news",$nextid,NEWSINFO_SMALL_WIDTH,NEWSINFO_SMALL_HEIGHT,NEWSINFO_MEDIUM_WIDTH,NEWSINFO_MEDIUM_HEIGHT,NEWSINFO_LARGE_WIDTH,NEWSINFO_LARGE_HEIGHT);
						$dirb=$img['dir'];
						$imgfilenameb=$img['filename'];
					}

					//insert data newsinfo
					$dateinsert=date("Y-m-d", $datetime2);

					//allow comment
					$allowcomment = ($allowcomment == 'n' ? 'n':'y');

					$sql="INSERT INTO newsinfo VALUES ($nextid, $category,'$title', '$permalink', '$subtitle', '$headline', '$author',
							 '$imgfilenameb', '$dirb', '$piccaption', '$keyword', '$source', '$datetime2', '$dateinsert', 0,  $type, '$allowcomment', $cook_id)";
					$query=query($sql);

					//looping for page
					$ikey=1;
					foreach($content as $key => $value){
						if(!empty($value)){
							$sql="INSERT INTO newsinfo_content VALUES($nextid,'$value','$ikey')";
							$query=query($sql);
							$ikey++;
						}
					}
				}
			}
		}elseif ($action=="editnews" and $uac_edit){
			if (empty($datetime) or empty($title) or empty($headline) or empty($page1)){
				$error=errorlist(2);
			}else{
				//Nextid
				$nextid=$newsid;

				//check permalink for duplicate
				$permalink=permalink($title,$nextid);

				//convert date & time to mktime
				list($date,$time)=explode(" " ,$datetime);
				list($day,$month,$year)=explode("/",$date);
				list($hour,$minut)=explode(":",$time);
				$datetime2=mktime($hour,$minut,0,$month,$day,$year);

				if(!empty($pic['name'])){
					if(in_array($pic['type'], $allowedType)){
						$fileExt = array_keys($allowedType, $pic['type']);
						$fileExt = $realExt[$fileExt[0]];

						if(is_uploaded_file($pic['tmp_name'])){
							//make sure it is really really image
							$img=getimagesize($pic['tmp_name']);
							if(!in_array($pic['type'],$allowedType) or !(is_numeric($img[0]) or is_numeric($img[1]) or $img[0]>0 or $img[1]>0)){
								//make sure it is allowed
								$error.=errorlist(13);
							}
						} else{
							$error.=errorlist(27);
						}
					} else{
						$error.=errorlist(13);
					}

					//get filename
					$fname = str_replace('.','',$pic['name']).'.'.$fileExt;
				}

				if (!$error){
					//get selected data from newsinfo
					$sql="SELECT * FROM newsinfo WHERE newsId='$newsid'";
					$query=query($sql);
					$data=fetch($query);
					$newsdir=$data['newsDir'];
					$newspic=$data['newsPic'];

					////////////////////////////////////////////UPLOAD THUMBS.///////////////////
					# Cek jika kedua field tidak kosong.
					if(!empty($pic['name'])){
							if($errorfileb){
								$error.=$errorfileb." (Gambar Utama)";
							}else{

								if ($newsdir and $newspic){
									//only when the existing photos exist
									@unlink("../assets/news/".$newsdir."/".$newspic);
									@unlink("../assets/news/".$newsdir."/".genthumb($newspic,"m"));
									@unlink("../assets/news/".$newsdir."/".genthumb($newspic,"s"));
								}

								//For big image
								$img=uploadit($fname,$pic['tmp_name'],"news/$newsdir",$nextid,NEWSINFO_SMALL_WIDTH,NEWSINFO_SMALL_HEIGHT,NEWSINFO_MEDIUM_WIDTH,NEWSINFO_MEDIUM_HEIGHT,NEWSINFO_LARGE_WIDTH,NEWSINFO_LARGE_HEIGHT);
								$dirb=$img['dir'];
								$imgfilenameb=$img['filename'];

								$oriimage=" ,newsDir='$dirb', newsPic='$imgfilenameb'";
							}
					}
				}////////////////////////////////////////////////////////////


				if(!$error){
					//allow comment
					$allowcomment = ($allowcomment == 'n' ? 'n':'y');

					$sql="UPDATE newsinfo SET catId=$category, newsTitle='$title', newsPermalink='$permalink',newsSubtitle='$subtitle',
								newsHeadline='$headline', newsAuthor='$author',newsPicCaption='$piccaption', newsKeyword='$keyword', newsSource='$source',
								newsAddedOn=$datetime2, newsType=$type, newsAllowComment='$allowcomment', adminId=$cook_id $oriimage WHERE newsId='$newsid'";
					$query=query($sql);

					//update newsinfo_content page1
					$sql="UPDATE newsinfo_content SET newsContent='$page1' WHERE newsPage=1 AND newsId=$newsid";
					$query=query($sql);

					//delete newsinfo_content exclude page 1
					$sql="DELETE FROM newsinfo_content WHERE newsPage!=1 AND newsId=$newsid";
					$query=query($sql);

					//insert back old + updated data from form
					foreach ($content as $key => $value) {
						if(!empty($value)){
							$sql="INSERT INTO newsinfo_content VALUES($newsid,'$value', $key+2)";
							$query=query($sql);
						}
					}
				}
			}
		}elseif ($action=="delnews" and $uac_delete){
			//find main and thumbnails image
			$sql="SELECT * FROM newsinfo WHERE newsId=$newsid";
			$query=query($sql);
			$data=fetch($query);
			$newspic=$data['newsPic'];
			$newsdir=$data['newsDir'];

			if ($newsdir and $newspic){
				//delete main and thumb image
				@unlink("../assets/news/".$newsdir."/".$newspic);
				@unlink("../assets/news/".$newsdir."/".genthumb($newspic,"m"));
				@unlink("../assets/news/".$newsdir."/".genthumb($newspic,"s"));
			}

			//find slide image
			$sql2="SELECT * FROM newsinfo_slide WHERE newsId=$newsid";
			$query2=query($sql2);
			$numofslider=rows($query2);
			if ($numofslider>0){
				while($data2=fetch($query2)){
					$slideid=$data2['slideId'];
					$slidedir=$data2['slideDir'];
					$slideName=$data2['slideFileName'];

					@unlink("../assets/news/".$slidedir."/".$slideName);
					@unlink("../assets/news/".$slidedir."/".genthumb($slideName,"m"));
					@unlink("../assets/news/".$slidedir."/".genthumb($slideName,"s"));

					$sql3="DELETE FROM newsinfo_slide WHERE slideId=$slideid";
					$query3=query($sql3);
				}
			}

			//delete newsinfo data on table newsinfo
			$sql="DELETE FROM newsinfo WHERE newsId='$newsid'";
			$query=query($sql);

			//delete newsinfo_content data on table newsinfo_content
			$sql="DELETE FROM newsinfo_content WHERE newsId='$newsid'";
			$query=query($sql);

			//delete newsinfo_comment
			$sql="DELETE FROM newsinfo_comment WHERE newsId='$newsid'";
			$query=query($sql);

			//for english version
			if($query){
				//delete newsinfo data on table newsinfo
				$sql="DELETE FROM newsinfo_lang WHERE newsId='$newsid'";
				$query=query($sql);

				//delete newsinfo_content data on table newsinfo_content
				$sql="DELETE FROM newsinfo_content_lang WHERE newsId='$newsid'";
				$query=query($sql);
			}

			// try to remove $dir
			if(!empty($newspic)){
				@rmdir("../assets/news/" . $newsdir);
			}
		}elseif ($action=="addslide" and $uac_add){
			//get newsid
			$newsid=$currnewsid;

			//chek if no file uploaded
			if(count($_FILES['slide'])==0){
				$error=errorlist(2);
			}

			//this code is used to check image slide
			for($i=0;$i<count($_FILES['slide']);$i++){
				if($_FILES['slide']['name'][$i]){
					$slideName[$i]=$_FILES['slide']['name'][$i];
					$slideType[$i]=$_FILES['slide']['type'][$i];
					$slideTmp[$i]=$_FILES['slide']['tmp_name'][$i];

					//make sure it is allowed
					if(in_array($slideType[$i], $allowedType)){
						$fileExt = array_keys($allowedType, $slideType[$i]);
						$fileExt = $realExt[$fileExt[0]];

						if(is_uploaded_file($slideTmp[$i])){
							//make sure it is really really image
							$img=getimagesize($slideTmp[$i]);
							if(!in_array($slideType[$i],$allowedType) or !(is_numeric($img[0]) or is_numeric($img[1]) or $img[0]>0 or $img[1]>0)){
								//make sure it is allowed
								$error.=errorlist(13);
							}
						} else{
							$error.=errorlist(27);
						}

						//get filename
						$slideName[$i] = str_replace('.','',$slideName[$i]).'.'.$fileExt;
					} else{
						$error.=errorlist(13);
					}
				}
			}

			if(!$error){
				for($i=0;$i<count($_FILES['slide']['name']);$i++){
					//skip the next rest process if current looping have noimage
					if(empty($slideName[$i])){
						continue;
					}

					$slideId=nextid("slideId","newsinfo_slide");
					$img=uploadit($slideName[$i],$slideTmp[$i],"news",$slideId,NEWSINFO_SMALL_WIDTH,NEWSINFO_SMALL_HEIGHT,NEWSINFO_MEDIUM_WIDTH,NEWSINFO_MEDIUM_HEIGHT,NEWSINFO_LARGE_WIDTH,NEWSINFO_LARGE_HEIGHT);
					$dir=$img['dir'];
					$imgfilename=$img['filename'];

					//save information to newsinfo_slide
					$sql="INSERT INTO newsinfo_slide VALUES('$slideId','$newsid','$dir','$imgfilename')";
					$query=query($sql);
				}
			}
		}elseif ($action=="delslide" and $uac_delete){
			//find slide image
			$sql2="SELECT * FROM newsinfo_slide WHERE slideId='$slideid'";
			$query2=query($sql2);
			$data2=fetch($query2);
			$slideid=$data2['slideId'];
			$newsid=$data2['newsId'];
			$slidedir=$data2['slideDir'];
			$slideName=$data2['slideFileName'];

			@unlink("../assets/news/".$slidedir."/".$slideName);
			@unlink("../assets/news/".$slidedir."/".genthumb($slideName,"m"));
			@unlink("../assets/news/".$slidedir."/".genthumb($slideName,"s"));
			@rmdir("../assets/news/".$slidedir);

			//delete on table newsinfo_slide
			$sql3="DELETE FROM newsinfo_slide WHERE slideId=$slideid";
			$query3=query($sql3);

			//redirect back to edit
			header("location:newsinfo.php?action=addslide&newsid=$newsid");
			exit;
		}elseif($action=="adden" and $uac_add){
			if (empty($title) or empty($headline) or count($content)==0){
				$error=errorlist(2);
			}else{
				if(!$error){
					$sql="INSERT INTO newsinfo_lang VALUES ($newsid, '$title','$subtitle', '$headline','$piccaption', '$keyword','$lang')";
					$query=query($sql);

					//looping for page
					$ikey=1;
					foreach($content as $key => $value){
						if(!empty($value)){
							$sql="INSERT INTO newsinfo_content_lang VALUES($newsid,'$value','$ikey','$lang')";
							$query=query($sql);
							$ikey++;
						}
					}
				}
			}
		}elseif($action=="editen" and $uac_edit){
			if (empty($title) or empty($headline) or empty($page1)){
				$error=errorlist(2);
			}else{
				if(!$error){
					$sql="UPDATE newsinfo_lang SET newsTitle='$title',newsSubtitle='$subtitle',
								newsHeadline='$headline',newsPicCaption='$piccaption', newsKeyword='$keyword'
								WHERE newsId='$newsid' AND lang='$lang'";
					$query=query($sql);

					//update newsinfo_content page1
					$sql="UPDATE newsinfo_content_lang SET newsContent='$page1' WHERE newsPage=1 AND newsId='$newsid' AND lang='$lang'";
					$query=query($sql);

					//delete newsinfo_content exclude page 1
					$sql="DELETE FROM newsinfo_content_lang WHERE newsPage!=1 AND newsId=$newsid AND lang='$lang'";
					$query=query($sql);

					//insert back old + updated data from form
					foreach ($content as $key => $value) {
						if(!empty($value)){
							$sql="INSERT INTO newsinfo_content_lang VALUES($newsid,'$value', $key+2,'$lang')";
							$query=query($sql);
						}
					}
				}
			}
		}

		//Will create new rss every this action
		if($action=="addnews"){
			chdir("../../");
			articlerss();
		}

		//check whether query was successful
		if(!$query){
			$error=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:newsinfo.php");
			}else{
				print "ok";
			}
		}
		exit;
	}

	if ($cid){
		$sql="SELECT * FROM newsinfo_cat WHERE catId='$cid'";
		$query=query($sql);
		$data=fetch($query);
		$catname=$data['catName'];
		$cpid=$data['catParentId'];

		$title=$catname;

		if ($cpid>0){
			$sql="SELECT * FROM newsinfo_cat WHERE catId='$cpid'";
			$query=query($sql);
			$data=fetch($query);
			$catparentname=$data['catName'];

			$title=$catparentname . " - " . $title;
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _('newsinfo_pagetitle'); ?> <?php print ($title)?" - " . $title:""; ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- bootstrap-datepicker -->
<link rel="stylesheet" type="text/css" href="/libs/bootstrap-datepicker/css/datepicker3.css" />
<link rel="stylesheet" href="/libs/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">


<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce_simple.js"></script>
<script type="text/javascript" src="/libs/moment/min/moment-with-locales.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('.fancybox').fancybox({
		'arrows': false
	});
	$('.fancybox-frame').fancybox({
		'type': 'iframe'
	});

	$('.fancybox-gallery').fancybox();

	$('#result').hide();
	validate('#result','#add','newsinfo.php');
	validate('#result','#edit','newsinfo.php');
	validate('#result','#addslide','newsinfo.php?action=addslide&newsid=<?php print $newsid; ?>');

	$(".datepickerdate").datepicker({
		startDate: '<?php echo date('d') ?>/<?php echo date('m') ?>/<?php echo date('Y') ?>',
		format: 'dd/mm/yyyy',
		todayHighlight: true,
		toggleActive: true,
		autoclose: true
	});
	$(".datetimepicker").datetimepicker({
		format: 'DD/MM/YYYY HH:mm',
		locale: 'id' 
	});
});
</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('newsinfo_pagetitle'); ?></h3>
			                <p><?php print _('newsinfo_pagedesc'); ?></p>
			            </div>
		            </div>
	        	</div>
        	</div>

        	<?php if ($uac_add){ ?>
        	<div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                	<div class="pull-right">
                		<a href="newsinfo.php?action=addnews" class="btn btn-primary"><i class="fa fa-plus"></i> <?php print _('cms_addbuttonpost'); ?></a>
                	</div>
                </div>
        	</div>
        	<?php } ?>

        	<!-- THE CONTENT OF PAGE HERE -->
        	<div class="row">
	        	<div class="col-md-12">
	        		<?php if ($action==""){ ?>
	        			<?php
							$sql="SELECT * FROM newsinfo_cat";
							$query=query($sql);
							while($data=fetch($query)){
								$selected=($data['catId']==$category ? "selected=\"selected\"":"");
								$categoryDraft.="<option value=\"" .$data['catId'] ."\" $selected>" .$data['catName'] ."</option>";
							}

						?>
						<div class="row">
							<?php if ($catname){ ?>
							<div class="col-md-6">
								<strong><a href="newsinfocat.php"><?php print _('newsinfo_category'); ?></a>:</strong> <?php print ($catparentname) ? "<a href=\"newsinfo.php?cid=$cpid\">" . $catparentname . "</a> - ":""; ?> <a href="newsinfo.php?cid=<?php print $cid; ?>"><?php print $catname; ?></a>
							</div>
							<?php } ?>
							<div class="col-md-6 pull-right">
								<div class="pull-right">
									
									<form name="search" method="get" class="form-inline">
										<input name="cid" type="hidden" value="<?php print $cid;?>" />

										<div class="form-group">
											<select name="category" class="form-control input-sm">
												<option value=""><?php print _('newsinfo_allcategory'); ?></option>
													<?php
														$sqlx="SELECT * FROM newsinfo_cat WHERE catParentId=0 ORDER BY catSort";
														$queryx=query($sqlx);
														while ($datax=fetch($queryx)){
															$datax=formoutput($datax);
													?>
													<option value="<?php print $datax['catId']; ?>"<?php print ($datax['catId']==$category)?" selected=\"selected\"":""; ?>><?php print $datax['catName']; ?></option>
													<?php
															$sqlxx="SELECT * FROM newsinfo_cat WHERE catParentId='" . $datax['catId'] . "' ORDER BY catSort";
															$queryxx=query($sqlxx);
															while ($dataxx=fetch($queryxx)){
																$dataxx=formoutput($dataxx);
													?>
													<option value="<?php print $dataxx['catId']; ?>"<?php print ($dataxx['catId']==$category)?" selected=\"selected\"":""; ?>>&nbsp; &nbsp; - <?php print $dataxx['catName']; ?></option>
													<?php
															}
														}
													?>
											</select>
										</div>
										<div class="form-group">
											<input name="searchtext" type="text" id="searchtext" size="40" value="<?php print $searchtext;?>"  class="form-control input-sm" />
										</div>

										<button type="submit" name="Submit" value="<?php print _('newsinfo_find'); ?>" class="btn btn-sm btn-dark"><?php print _('newsinfo_find'); ?></button>
									</form>

								</div>
							</div>
							
						</div>

						<div class="table-responsive" style="margin-top:12px;">
							<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
                				<thead>
                					<tr>
                						<th width="35"><?php print _('newsinfo_numb'); ?></th>
										<th><?php print _('newsinfo_title'); ?></th>
										<th width="80"><?php print _('newsinfo_read'); ?></th>
										<th width="80"><?php print _('newsinfo_comment'); ?></th>
										<th width="200"><?php print _('newsinfo_date'); ?></th>
										<?php if(count($lc_lang)>1){ ?><th style="width:40px;" class="text-center"><i rel="tooltip" title="<?php echo _('multi_language_view_language'); ?>" class="fa fa-language"></i></th><?php }?>
										<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
										<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
										<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
                					</tr>
                				</thead>
                				<tbody>
                				<?php
									if (empty($pg)){
										$pg=1;
									}
									$numPerPage=20;
									$offset=($pg-1)*$numPerPage;
									$num=1;
									$num=$num+(($pg-1)*$numPerPage);
									if($category){

										$whereClause[]="catId='$category'";
									}

									if($searchtext){
										//cahnge space between word to %
										$searchtext=str_replace(" ", "%", trim($searchtext));
										$whereClause[]="newsTitle like '%$searchtext%' or newsSubtitle like '%$searchtext%'";
									}

									if(is_array($whereClause)){
										$whereClause=implode('  and ',$whereClause);
									}
									if($category or $searchtext){
										$whereClause="WHERE $whereClause";
									}

									if ($cid){
										if (empty($whereClause)){
											$whereClause.=" WHERE catId='$cid' ";
										}else{
											$whereClause.=" and catId='$cid' ";
										}
									}

									$sql="SELECT * FROM newsinfo $whereClause ORDER BY newsAddedOn DESC";
									$query=query($sql);
									$numofdata=rows($query);

									$sql.=' LIMIT '.$offset.','.$numPerPage;
									$query=query($sql);

									if ($numofdata<1){
								?>
									<tr>
										<td colspan="9" class="asterik"><div class="text-center"><br />
										<?php print _('newsinfo_datablognotavailable'); ?><br />
										<br /></td>
									</tr>
								<?php
									} else {
									while ($data=fetch($query)){
										$catPermalink = getval("catPermalink", "newsinfo_cat", "catId", $data['catId']);
								?>
									<tr valign="top">
										<td class="text-center"><?php print $num; ?></td>
										<td>
											<?php
												//print subtitle if it have
												if(!empty($data['newsSubtitle'])){
													print $data['newsSubtitle']."<br />";
												}
											?>
											<strong><a target="_blank" href="/blog/<?php print $catPermalink.'/'.$data['newsPermalink'];?>/"><?php print $data['newsTitle'];?></a></strong>
										</td>
										<td class="text-center"><?php print number_format($data['newsViewed'],0,",","."); ?></td>
										<td class="text-center"><a href="newsinfo_comment.php?newsid=<?php print $data['newsId'];?>" target="_blank"><?php print number_format(countdata("newsinfo_comment", "newsId='".$data['newsId']."'"), 0, ',', '.'); ?></a></td>
										<td class="text-center"><?php print dateformat($data['newsAddedOn'],"time"); ?><?php //print date("d/m/Y H:i", $data['newsAddedOn']); ?></td>
										<?php
										if(count($lc_lang)>1){
										?>
											<td class="text-center">
												<?php
												foreach($lc_lang as $key=>$value){
													$exist=countdata("newsinfo_lang","newsId='".$data['newsId']."' and lang='$value'");
													if($value==$lc_lang_default) continue;
													if($exist and $uac_edit){
														?><a href="newsinfo.php?action=editen&amp;lang=<?php print $value;?>&newsid=<?php print $data['newsId']; ?>" rel="tooltip" title="<?php print _('newsinfo_editversi'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-edit.png" alt="<?php print $value;?>"  border="0" /></a><?php
													}
													elseif($uac_add){
														?><a href="newsinfo.php?action=adden&amp;lang=<?php print $value;?>&newsid=<?php print $data['newsId']; ?>" rel="tooltip" title="<?php print _('newsinfo_addversi'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-add.png" alt="<?php print $value;?>" border="0" /></a><?php
													}
												}
												?>
											</td>
										<?php
										} ?>
										<td class="text-center"><?php if ($uac_add){ ?><a href="newsinfo.php?action=addslide&amp;newsid=<?php print $data['newsId']; ?>" title="<?php print _('newsinfo_uploadimg'); ?>: <?php print stripquote($data['newsTitle']);?>" rel="tooltip"><i class="fa fa-image"></i></a><?php } ?></td>
										<td class="text-center"><?php if ($uac_edit){ ?><a href="newsinfo.php?action=editnews&amp;newsid=<?php print $data['newsId']; ?>" title="<?php print _('cms_edit'); ?>: <?php print stripquote($data['newsTitle']); ?>" rel="tooltip"><i class="fa fa-edit"></i></a><?php } ?></td>
										<td class="text-center"><?php if ($uac_delete){ ?><a href="newsinfo.php?action=delnews&amp;form=submit&amp;newsid=<?php print $data['newsId']; ?>" class="delete" title="<?php print stripquote($data['newsTitle']); ?>"><i rel="tooltip" title="<?php print _('cms_delete'); ?>: <?php print stripquote($data['newsTitle']); ?>" class="fa fa-trash-o"></i></a><?php } ?></td>
									 </tr>
								<?php $num++;
									}
								}
								?>
                				</tbody>
                			</table>
						</div>
						<?php
						$options['total']=$numofdata;
						$options['filename']='newsinfo.php';
						$options['qualifier']='newsinfo.php';
						$options['pg']=$pg;
						$options['numPerPage']=$numPerPage;
						$options['style']=1;
						$options['addquery']="searchtext=$searchtext&cid=$cid";
						paging($options);
						?>
					<?php } elseif ($action=="addnews" and $uac_add){?>
						<a href="newsinfo.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

		        		<h4><?php print _('cms_mainaddbuttonpost') ?></h4>
						<hr/>

						<form action="newsinfo.php?action=addnews&amp;form=submit" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">
							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="category"><?php print _('newsinfo_category'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-3 col-sm-6 col-xs-12">
		                        	<select name="category" id="category" required class="form-control">
										<?php
											$sqlx="SELECT * FROM newsinfo_cat WHERE catParentId=0 ORDER BY catSort";
											$queryx=query($sqlx);
											$rows = rows($queryx);

											if($rows > 1){
										?>
											<option value=""><?php print _('newsinfo_selectcategory'); ?></option>
										<?php
											}
											while ($datax=fetch($queryx)){
												$datax=formoutput($datax);
										?>
										<option value="<?php print $datax['catId']; ?>"><?php print $datax['catName']; ?></option>
										<?php
												$sqlxx="SELECT * FROM newsinfo_cat WHERE catParentId='" . $datax['catId'] . "' ORDER BY catSort";
												$queryxx=query($sqlxx);
												while ($dataxx=fetch($queryxx)){
													$dataxx=formoutput($dataxx);
										?>
										<option value="<?php print $dataxx['catId']; ?>">&nbsp; &nbsp; - <?php print $dataxx['catName']; ?></option>
										<?php
												}
											}
										?>
									</select>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="type"><?php print _('newsinfo_type'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-3 col-sm-6 col-xs-12">
		                        	<select name="type" id="type" required class="form-control">
										<option value="1">News</option>
										<option value="2">Advertorial</option>
										<option value="3">Video</option>
									</select>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="datetime"><?php print _('newsinfo_date'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-3 col-sm-6 col-xs-12">
		                        	<input name="datetime" id="datetime" required class="datetimepicker form-control has-feedback-left" type="text" size="20" value="<?php print date("d/m/Y H:i", $now); ?>"/>
		                        	<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="subtitle"><?php print _('newsinfo_smalltitle'); ?>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<input name="subtitle" type="text" id="subtitle" size="80" class="form-control" />
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="title"><?php print _('newsinfo_title'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<input name="title" type="text" id="title" size="80" required class="form-control" />
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="headline"><?php print _('newsinfo_headline'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<textarea name="headline" id="headline" class="tinymce" required></textarea>
		                        	<a class="btn btn-default btn-xs fancybox-frame" href="imgfly.php"><i class="fa fa-upload"></i> <?php print _('pagecontent_uploadimg'); ?></a><br/><br/>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="page1"><?php print _('newsinfo_page'); ?> 1 <span class="required">*</span>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<textarea name="content[]" cols="80" rows="5" id="page1" required class="tinymce"></textarea>
		                        	<a class="btn btn-default btn-xs fancybox-frame" href="imgfly.php"><i class="fa fa-upload"></i> <?php print _('pagecontent_uploadimg'); ?></a><br/><br/>
		                        </div>
	                    	</div>

	                    	<?php
							for($i=2;$i<=3;$i++){
							?>
							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="page<?php print $i; ?>"><?php print _('newsinfo_page'); ?> <?php print $i; ?>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<textarea name="content[]" cols="80" rows="5" id="page<?php print $i; ?>" class="tinymce"></textarea>
		                        	<a class="btn btn-default btn-xs fancybox-frame" href="imgfly.php"><i class="fa fa-upload"></i> <?php print _('pagecontent_uploadimg'); ?></a><br/><br/>
		                        </div>
	                    	</div>
							<?php
							}
							?>
							<input type="hidden" name="lastpage" value="<?php print $i-1; ?>" />

							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="headline"><?php print _('newsinfo_allowedcomment'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<div class="radio">
		                        		<label>
		                        			<input name="allowcomment" id="comment-y" value="y" type="radio" checked> <?php print _('newsinfo_yes'); ?>
		                        		</label>
			                        </div>

			                        <div class="radio">
		                        		<label>
		                        			<input name="allowcomment" id="comment-n" value="n" type="radio"> <?php print _('newsinfo_no'); ?>
		                        		</label>
			                        </div>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="author"><?php print _('newsinfo_author'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<input name="author" type="text" id="author" size="80" required class="form-control" />
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="author"><?php print _('newsinfo_sourcelink'); ?> 
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<input name="source" type="text" id="source" size="80" class="form-control"  />
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="keyword"><?php print _('newsinfo_keyword'); ?> 
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<input name="keyword" type="text" id="keyword" size="80" class="form-control"  />
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="keyword"><?php print _('newsinfo_mainimage'); ?> 
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<input name="pic" type="file" id="pic" size="40">
		                        	<span class="help-block"><?php print _('newsinfo_mainimage_info'); ?><?php print NEWSINFO_LARGE_WIDTH; ?> pixel.</span>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="keyword"><?php print _('newsinfo_descimage'); ?> 
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<input name="piccaption" type="text" id="piccaption" size="80" class="form-control" />
		                        </div>
	                    	</div>

	                    	<hr/>

	                    	<div class="form-group">
		                        <div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
		                        	<?php /*<button name="Submit" type="submit" class="btn btn-info btn-sm" value="Preview" onclick="window.location.reload(); document.add.action ='/web/template_post.php'; document.add.target ='blank'; document.add.method='post'; document.add.submit(); return false;"><i class="fa fa-list"></i> Preview</button>
		                        	*/?><button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='newsinfo.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                        </div>
	                    	</div>

	                    	<br/><br/><br/>

						</form>
					<?php } elseif($action=="editnews" and $uac_edit){
						//fetch newsinfo data
						$sql="SELECT * FROM newsinfo WHERE newsId=" . $newsid;
						$query=query($sql);
						$data=fetch($query);
						$data=formoutput($data);
					?>
						<a href="newsinfo.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
						<h4><?php print _('pagecontent_addpage') ?></h4>
						<hr/>

						<form action="newsinfo.php?action=editnews&amp;form=submit&amp;newsid=<?php print $newsid; ?>" method="post" enctype="multipart/form-data" name="edit" id="edit" class="form-horizontal form-label-left">

							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="category"><?php print _('newsinfo_category'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-3 col-sm-6 col-xs-12">
		                        	<select name="category" id="category" required class="form-control">
										<?php
											$sqlx="SELECT * FROM newsinfo_cat WHERE catParentId=0 ORDER BY catSort";
											$queryx=query($sqlx);
											$rows = rows($queryx);

											if($rows > 1){
										?>
											<option value=""><?php print _('newsinfo_selectcategory'); ?></option>
										<?php
											}
											while ($datax=fetch($queryx)){
												$datax=formoutput($datax);
										?>
										<option value="<?php print $datax['catId']; ?>"<?php print ($datax['catId']==$data['catId'])?" selected=\"selected\"":""; ?>><?php print $datax['catName']; ?></option>
										<?php
												$sqlxx="SELECT * FROM newsinfo_cat WHERE catParentId='" . $datax['catId'] . "' ORDER BY catSort";
												$queryxx=query($sqlxx);
												while ($dataxx=fetch($queryxx)){
													$dataxx=formoutput($dataxx);
										?>
										<option value="<?php print $dataxx['catId']; ?>"<?php print ($dataxx['catId']==$data['catId'])?" selected=\"selected\"":""; ?>>&nbsp; &nbsp; - <?php print $dataxx['catName']; ?></option>
										<?php
												}
											}
										?>
									</select>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="type"><?php print _('newsinfo_type'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-3 col-sm-6 col-xs-12">
		                        	<select name="type" id="type" required class="form-control">
										<option value="1"<?php print ($data['newsType']==1)?" selected=\"selected\"":""; ?>>News</option>
										<option value="2"<?php print ($data['newsType']==2)?" selected=\"selected\"":""; ?>>Advertorial</option>
										<option value="3"<?php print ($data['newsType']==3)?" selected=\"selected\"":""; ?>>Video</option>
									</select>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="datetime"><?php print _('newsinfo_date'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-3 col-sm-6 col-xs-12">
		                        	<input name="datetime" id="datetime" required class="datetimepicker form-control has-feedback-left" type="text" size="20" value="<?php print date("d/m/Y H:i", $data['newsAddedOn']); ?>" />
		                        	<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="subtitle"><?php print _('newsinfo_smalltitle'); ?>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<input name="subtitle" type="text" id="subtitle" size="80" class="form-control" value="<?php print $data['newsSubtitle']; ?>" />
		                        </div>
		                	</div>

		                	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="title"><?php print _('newsinfo_title'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<input name="title" type="text" id="title" size="80" required class="form-control" value="<?php print $data['newsTitle']; ?>" />
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="headline"><?php print _('newsinfo_headline'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<textarea name="headline" id="headline" class="tinymce" required><?php print $data['newsHeadline']; ?></textarea>
		                        	<a class="btn btn-default btn-xs fancybox-frame" href="imgfly.php"><i class="fa fa-upload"></i> <?php print _('pagecontent_uploadimg'); ?></a><br/><br/>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="page1"><?php print _('newsinfo_page'); ?> 1 <span class="required">*</span>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<?php
										//get data page1 form newsinfo_content
										$sql1="SELECT * FROM newsinfo_content WHERE newsPage=1 AND newsId=$newsid";
										$query1=query($sql1);
										$data1=fetch($query1);
									?>
		                        	<textarea name="page1" cols="80" rows="5" id="page1" required class="tinymce"><?php print $data1['newsContent']; ?></textarea>
		                        	<a class="btn btn-default btn-xs fancybox-frame" href="imgfly.php"><i class="fa fa-upload"></i> <?php print _('pagecontent_uploadimg'); ?></a><br/><br/>
		                        </div>
	                    	</div>

	                    	<?php
								//get data page!=1 form newsinfo_content
								$sql2="SELECT * FROM newsinfo_content WHERE newsPage!=1 AND newsId=$newsid";
								$query2=query($sql2);
								$numofrec2=rows($query2);
								$lastpage=$numofrec2+2;
								$beforelastpage=$numofrec2+1;
								$beforelastpageform="page".$beforelastpage;
								$lastpageform="page".$lastpage;
								if($numofrec2>0){
									//if newsinfo_content have more than 1 page
									$num1=2;
									while($data2=fetch($query2)){
							?>
								<div class="form-group">
			                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="page<?php print $num1; ?>"><?php print _('newsinfo_page'); ?> <?php print $num1; ?>
			                        </label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                        	<textarea name="content[]" cols="80" rows="5" id="page<?php print $num1; ?>" class="tinymce"><?php print $data2['newsContent']; ?></textarea>
			                        	<a class="btn btn-default btn-xs fancybox-frame" href="imgfly.php"><i class="fa fa-upload"></i> <?php print _('pagecontent_uploadimg'); ?></a><br/><br/>
			                        </div>
		                    	</div>
							<?php
									$num1++;
									}
								}
							?>
							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="page1"><?php print _('newsinfo_page'); ?> <?php print $lastpage; ?>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<textarea name="content[]" cols="80" rows="5" class="tinymce"></textarea>
		                        	<a class="btn btn-default btn-xs fancybox-frame" href="imgfly.php"><i class="fa fa-upload"></i> <?php print _('pagecontent_uploadimg'); ?></a><br/><br/>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="headline"><?php print _('newsinfo_allowedcomment'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<div class="radio">
		                        		<label>
		                        			<input type="radio" name="allowcomment" value="y" id="comment-y"<?php print $data['newsAllowComment'] == 'y' ? ' checked="checked"':''; ?> /> <?php print _('newsinfo_yes'); ?>
		                        		</label>
			                        </div>

			                        <div class="radio">
		                        		<label>
		                        			<input type="radio" name="allowcomment" value="n" id="comment-n"<?php print $data['newsAllowComment'] == 'n' ? ' checked="checked"':''; ?> /> <?php print _('newsinfo_no'); ?>
		                        		</label>
			                        </div>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="author"><?php print _('newsinfo_author'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<input name="author" type="text" id="author" size="80" required class="form-control" value="<?php print $data['newsAuthor']; ?>" />
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="author"><?php print _('newsinfo_sourcelink'); ?> 
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<input name="source" type="text" id="source" size="80" class="form-control" value="<?php print $data['newsSource']; ?>" />
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="keyword"><?php print _('newsinfo_keyword'); ?> 
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<input name="keyword" type="text" id="keyword" size="80" class="form-control" value="<?php print $data['newsKeyword']; ?>" />
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="keyword"><?php print _('newsinfo_mainimage'); ?> 
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<?php if (!empty($data['newsPic'])){ ?>
									<a href="/assets/news/<?php print $data['newsDir']; ?>/<?php print $data['newsPic']; ?>" class="fancybox"><img src="../assets/news/<?php print $data['newsDir']; ?>/<?php print genthumb($data['newsPic'],"m"); ?>" alt="<?php print stripquote($data['newsPicCaption']); ?>"/></a><br /><br /><?php } ?>

		                        	<input name="pic" type="file" id="pic" size="40">
		                        	<span class="help-block"><?php print _('newsinfo_mainimage_info'); ?><?php print NEWSINFO_LARGE_WIDTH; ?> pixel.</span>
		                        	<input type="hidden" name="direkpic" value="<?php print $data['newsDir'];?>">
									<input type="hidden" name="picture" value="<?php print $data['newsPic'];?>">
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="piccaption"><?php print _('newsinfo_descimage'); ?> 
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<input name="piccaption" type="text" id="piccaption" size="80" class="form-control" value="<?php print $data['newsPicCaption']; ?>" />
		                        </div>
	                    	</div>

	                    	<hr/>

	                    	<div class="form-group">
		                        <div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
		                        	<?php /*<button name="preview" type="button" class="btn btn-info btn-sm" value="Preview" onclick="window.location.reload(); document.edit.action ='../web/template_post.php'; document.edit.target ='blank'; document.edit.method='post'; document.edit.submit(); return false;"><i class="fa fa-list"></i> Preview</button>
		                        	*/?><button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='newsinfo.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                        </div>
	                    	</div>

	                    	<br/><br/><br/>

						</form>
					<?php } elseif($action=="addslide" and $uac_add){ ?>
						<a href="newsinfo.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
						<h4>Slide</h4>
						<hr/>

						<form action="newsinfo.php?action=addslide&amp;form=submit" method="post" enctype="multipart/form-data" name="addslide" id="addslide">
							<div class="form-group">
		                        <label class="control-label" for="keyword"><?php print _('newsinfo_image'); ?> Slide
		                        </label>
	                        	<input name="slide[]" type="file" size="40" multiple />
	                        	<span class="help-block"><?php print _('newsinfo_banneinfo'); ?><?php print NEWSINFO_SLIDE_LARGE_WIDTH . "x" . NEWSINFO_SLIDE_LARGE_HEIGHT; ?> pixel.</span>
	                    	</div>
	                    	<input type="hidden" name="currnewsid" value="<?php print $newsid; ?>" />

	                    	<hr/>

	                    	<div class="form-group">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('newsinfo_uploadbutton'); ?>"><i class="fa fa-upload"></i> <?php print _('newsinfo_uploadbutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" value="<?php print _('cms_cancelbutton'); ?>" onclick="window.location='newsinfo.php';"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                    	</div>

	                    	<br/><br/>
						</form>

						<h3><i class="fa fa-image"></i> <?php print _('newsinfo_slidealready'); ?></h3>
						<hr/>
						<?php
							$sql="SELECT * FROM newsinfo_slide WHERE newsId=" . $newsid;
							$query=query($sql);
							$numofslide=rows($query);

							if ($numofslide>0){
						?>
						<div class="row">
							<?php
							while ($data=fetch($query)) {
							?>
							<div class="col-md-55">
								<div class="thumbnail thumbnail-height-auto">
									<div class="image view view-first">
										<img src="/assets/news/<?php print $data['slideDir']; ?>/<?php print genthumb($data['slideFileName'],"m"); ?>" style="width: 100%; display: block;" alt="image" />
										<div class="mask">
											<div class="tools tools-bottom">
												<a class="fancybox-gallery" href="/assets/news/<?php print $data['slideDir']; ?>/<?php print $data['slideFileName']; ?>"><i class="fa fa-search-plus"></i></a>
												<a href="javascript:if (window.confirm('<?php print _('newsinfo_windowsconfirm'); ?>??')){ window.location='newsinfo.php?action=delslide&amp;form=submit&amp;slideid=<?php print $data['slideId']; ?>'; };"><i class="fa fa-times"></i></a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php
							}
							?>
						</div>
						<?php } else { ?>
							<p class="text-center"><?php echo _('newsinfo_picturenotavailable'); ?></p>
						<?php } ?>

					<?php } elseif($action=="editen" and $uac_edit){ 
						//fetch newsinfo data
						$sql="SELECT * FROM newsinfo WHERE newsId='$newsid'";
						$query=query($sql);
						$data=fetch($query);
						$data=formoutput($data);
						$postname=$data['newsTitle'];
						$dir=$data['newsDir'];
						$image=$data['newsPic'];

						$sql="SELECT * FROM newsinfo_lang WHERE newsId='$newsid' AND lang='$lang'";
						$query=query($sql);
						$data=fetch($query);
						$data=formoutput($data);


					?>
						<a href="newsinfo.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

		        		<h4><?php print _('newsinfo_editpostvers'); ?> <?php print localename($lang); ?> &raquo; <?php print $postname; ?></h4>
						<hr/>
						<form action="newsinfo.php?action=editen&amp;form=submit&amp;lang=<?php print $lang;?>&amp;newsid=<?php print $newsid; ?>" method="post" enctype="multipart/form-data" name="edit" id="edit" class="form-horizontal form-label-left">

							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="subtitle"><?php print _('newsinfo_smalltitle'); ?>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<input name="subtitle" type="text" id="subtitle" size="80" class="form-control" value="<?php print $data['newsSubtitle']; ?>" />
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="title"><?php print _('newsinfo_title'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<input name="title" type="text" id="title" size="80" value="<?php print $data['newsTitle']; ?>" required class="form-control" />
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="headline"><?php print _('newsinfo_headline'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<textarea name="headline" id="headline" class="tinymce" required><?php print $data['newsHeadline']; ?></textarea>
		                        	<a class="btn btn-default btn-xs fancybox-frame" href="imgfly.php"><i class="fa fa-upload"></i> <?php print _('pagecontent_uploadimg'); ?></a><br/><br/>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="page1"><?php print _('newsinfo_page'); ?> 1 <span class="required">*</span>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<?php
										//get data page1 form newsinfo_content
										$sql1="SELECT * FROM newsinfo_content_lang WHERE newsPage=1 AND newsId='$newsid' AND lang='$lang'";
										$query1=query($sql1);
										$data1=fetch($query1);
									?>
		                        	<textarea name="content[]" cols="80" rows="5" id="page1" required class="tinymce"><?php print $data1['newsContent']; ?></textarea>
		                        	<a class="btn btn-default btn-xs fancybox-frame" href="imgfly.php"><i class="fa fa-upload"></i> <?php print _('pagecontent_uploadimg'); ?></a><br/><br/>
		                        </div>
	                    	</div>

	                    	<?php
							//get data page!=1 form newsinfo_content
							$sql2="SELECT * FROM newsinfo_content_lang WHERE newsPage!=1 AND newsId='$newsid' AND lang='$lang'";
							$query2=query($sql2);
							$numofrec2=rows($query2);
							$lastpage=$numofrec2+2;
							$beforelastpage=$numofrec2+1;
							$beforelastpageform="page".$beforelastpage;
							$lastpageform="page".$lastpage;
							if($numofrec2>0){
								//if newsinfo_content have more than 1 page
								$num1=2;
								while($data2=fetch($query2)){
							?>
								<div class="form-group">
			                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="page<?php print $num1; ?>"><?php print _('newsinfo_page'); ?> <?php print $num1; ?>
			                        </label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                        	<textarea name="content[]" cols="80" rows="5" id="page<?php print $num1; ?>" class="tinymce"><?php print $data2['newsContent']; ?></textarea>
			                        	<a class="btn btn-default btn-xs fancybox-frame" href="imgfly.php"><i class="fa fa-upload"></i> <?php print _('pagecontent_uploadimg'); ?></a><br/><br/>
			                        </div>
		                    	</div>
							<?php
								$num1++;
								}
							}
							?>

							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="page1c"><?php print _('newsinfo_page'); ?> <?php print $lastpage; ?><span class="required">*</span>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<textarea name="content[]" cols="80" rows="5" id="page1c" required class="tinymce"></textarea>
		                        	<a class="btn btn-default btn-xs fancybox-frame" href="imgfly.php"><i class="fa fa-upload"></i> <?php print _('pagecontent_uploadimg'); ?></a><br/><br/>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="keyword"><?php print _('newsinfo_keyword'); ?> 
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<input name="keyword" type="text" id="keyword" size="80" class="form-control" value="<?php print $data['newsKeyword']; ?>" />
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="keyword"><?php print _('newsinfo_mainimage'); ?> 
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<?php if (!empty($image)){ ?>
									<a href="/assets/news/<?php print $dir; ?>/<?php print $image; ?>" class="fancybox"><img src="/assets/news/<?php print $dir; ?>/<?php print genthumb($image,"m"); ?>" alt="<?php print stripquote($data['newsPicCaption']); ?>"/></a><br /><br /><?php } ?>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="piccaption"><?php print _('newsinfo_descimage'); ?> 
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<input name="piccaption" type="text" id="piccaption" size="80" class="form-control" value="<?php print $data['newsPicCaption']; ?>" />
		                        </div>
	                    	</div>

	                    	<hr/>

	                    	<div class="form-group">
		                        <div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_updatebutton'); ?>"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
		                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='newsinfo.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                        </div>
	                    	</div>

	                    	<br/><br/><br/>

						</form>
					<?php }elseif($action=="adden" and $uac_add){
						//fetch newsinfo data
						$sql="SELECT * FROM newsinfo WHERE newsId='$newsid'";
						$query=query($sql);
						$data=fetch($query);
						$data=formoutput($data);
						$postname=$data['newsTitle'];
					?>
						<a href="newsinfo.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back');?></a><br/>
						<h4><?php print _('newsinfo_addpostvers'); ?><?php print localename($lang);?> &raquo; <?php print $postname; ?></h4>
						<hr/>

						<form action="newsinfo.php?action=adden&amp;form=submit&amp;lang=<?php print $lang;?>&amp;newsid=<?php print $newsid; ?>" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">
							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="subtitle"><?php print _('newsinfo_smalltitle'); ?>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<input name="subtitle" type="text" id="subtitle" size="80" class="form-control" />
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="title"><?php print _('newsinfo_title'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<input name="title" type="text" id="title" size="80" required class="form-control" />
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="headline"><?php print _('newsinfo_headline'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<textarea name="headline" id="headline" class="tinymce" required></textarea>
		                        	<a class="btn btn-default btn-xs fancybox-frame" href="imgfly.php"><i class="fa fa-upload"></i> <?php print _('pagecontent_uploadimg'); ?></a><br/><br/>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="page1"><?php print _('newsinfo_page'); ?> 1 <span class="required">*</span>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<textarea name="content[]" cols="80" rows="5" id="page1" required class="tinymce"></textarea>
		                        	<a class="btn btn-default btn-xs fancybox-frame" href="imgfly.php"><i class="fa fa-upload"></i> <?php print _('pagecontent_uploadimg'); ?></a><br/><br/>
		                        </div>
	                    	</div>

	                    	<?php
							for($i=2;$i<=3;$i++){
							?>
							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="page<?php print $i; ?>"><?php print _('newsinfo_page'); ?> <?php print $i; ?>
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<textarea name="content[]" cols="80" rows="5" id="page<?php print $i; ?>" class="tinymce"></textarea>
		                        	<a class="btn btn-default btn-xs fancybox-frame" href="imgfly.php"><i class="fa fa-upload"></i> <?php print _('pagecontent_uploadimg'); ?></a><br/><br/>
		                        </div>
	                    	</div>
							<?php
							}
							?>
							<input type="hidden" name="lastpage" value="<?php print $i-1; ?>" />

							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="keyword"><?php print _('newsinfo_keyword'); ?> 
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<input name="keyword" type="text" id="keyword" size="80" class="form-control" />
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="keyword"><?php print _('newsinfo_mainimage'); ?> 
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<?php if (!empty($data['newsPic'])){ ?>
									<a href="/assets/news/<?php print $data['newsDir']; ?>/<?php print $data['newsPic']; ?>" class="fancybox"><img src="../assets/news/<?php print $data['newsDir']; ?>/<?php print genthumb($data['newsPic'],"m"); ?>" alt="<?php print stripquote($data['newsPicCaption']); ?>"/></a><br /><br /><?php } ?>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="keyword"><?php print _('newsinfo_descimage'); ?> 
		                        </label>
		                        <div class="col-md-7 col-sm-6 col-xs-12">
		                        	<input name="piccaption" type="text" id="piccaption" size="80" class="form-control" />
		                        </div>
	                    	</div>

	                    	<hr/>

	                    	<div class="form-group">
		                        <div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
		                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" value="<?php print _('cms_cancelbutton'); ?>" onclick="window.location='newsinfo.php';;"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                        </div>
	                    	</div>

	                    	<br/><br/><br/>

						</form>
	        		<?php } ?>
	        	</div>
	        </div>
	        <!-- END THE CONTENT OF PAGE HERE -->

		</div>
		<!-- END 	`THE CONTENT OF PAGE HERE -->

		<?php include("com/com-footer.php"); ?>

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>