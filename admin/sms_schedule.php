<?php
	$page=42;
	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/mainmod.php");	
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("../modz/errormsg.php");
	include("authuser.php");
	include("../modz/mainmod-extend.php");
	
	if ($form=="submit"){
        if ($action=="delete"){
			foreach ($checkedid as $key => $value) {
				$sql = "DELETE FROM sms_schedule  WHERE schId='$value'";
				$query = query($sql);
			}
      
		}elseif($action=='edit' ){
			if (empty($message)){
				$error = errorlist(2);
			}
			
			if(!empty($scheduletype)){
				#sms schedule section
				if($scheduletype == 'e_date' and empty($s_mul)){
					displayerror(2);
				} elseif($scheduletype == 'e_day' and count(array_filter($s_day)) == 0){
					displayerror(2);
				} elseif($scheduletype == 'e_month' and count(array_filter($s_date)) == 0){
					displayerror(2);
				} elseif($scheduletype == 'e_year' and (empty($cdate) or empty($cmon))){
					displayerror(2);
				}
			}

			$exist = checkDuplicateRecord('sms_schedule', "schTo='$number' and md5(schText)='".md5($message)."' AND schId!='$id'");
				
			if($exist == 0){
				$startSendOn = ($startdatesend) ? strtotime("$startdatesend $starttime") : $now;
				$endSendOn = ($enddatesend) ? strtotime("$enddatesend $endtime") : 0;
				$baseTimeStamp = ($startdatesend) ? strtotime($startdatesend) : strtotime(date('Y-m-d'));
				list($h, $m) = explode(':', $schintime);
				$sendOn = ((int) $h * 3600) + ((int) $m * 60);
				
				if($scheduletype == 'e_date'){
					$schType = 'multiple';
					$schRules = $s_mul;
					$schNextSent = strtotime("+$s_mul days", $baseTimeStamp);
				} elseif($scheduletype == 'e_day'){
					$schType = 'day';
					ksort($s_day);
					$schRules = implode(',', array_filter($s_day));

					if(in_array(date('l', $baseTimeStamp), $s_day) and ($baseTimeStamp + $sendOn) > $now){
						$schNextSent = $baseTimeStamp;
					} else {
						foreach($s_day as $key => $value){
							$nextDay[] = strtotime('next '.$value, $baseTimeStamp);
						}
					
						$schNextSent = min($nextDay);
					}
				} elseif($scheduletype == 'e_month'){
					$schType = 'month';
					ksort($s_date);
					$schRules = implode(',', array_filter($s_date));
					
					if(in_array(date('j', $baseTimeStamp), $s_date) and ($baseTimeStamp + $sendOn) > $now){
						$schNextSent = $baseTimeStamp;
					} else {
						$result = false;
						foreach($s_date as $key => $value){
							if($value > date('j', $baseTimeStamp)){
								$nextDate = $value;
								$result = true;
								break;
							}
						}
						
						if($result == true){
							$schNextSent = strtotime("+".($nextDate - date('j', $baseTimeStamp))." days", $baseTimeStamp);
						} else {
							$nextDate = min($s_date);
							$schNextSent =  strtotime("$nextDate ".date('F Y', strtotime('+1 month', $baseTimeStamp)));
						}
					}
				} elseif($scheduletype == 'e_year'){
					$schType = 'year';
					$schRules = $cdate.' '.$cmon;
					if(strtotime("$cdate $cmon") < $baseTimeStamp){
						$year = date('Y', $baseTimeStamp) + 1;
					} else {
						$year = date('Y', $baseTimeStamp);
					}
					
					$schNextSent = strtotime("$cdate $cmon $year");
				}
				
				#add hour
				$schNextSent += $sendOn;
		
				$sql = "UPDATE sms_schedule SET schText='$message', schRules='$schRules', 
					schTime='$schintime', schStartDateOn='$startSendOn', schEndDateOn='$endSendOn', 
					schNextSend='$schNextSent'  WHERE schId='$id'"; 
				$query = query($sql);
			}
		}
        if(!$query){
			$error=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:sms_schedule.php");
			}else{
				print "ok";
			}
		}
		exit;
    }

	
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print _('sms_schedule_title'); ?> - <?php print _('area'); ?> - <?php print SITE_NAME; ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="/libs/bootstrap-datepicker/css/datepicker3.css" />
<link rel="stylesheet" href="/libs/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" type="text/css" href="/style/footable.bootstrap.css" />
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/style/bootstrap-clockpicker.min.css" />

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>

<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="/libs/moment/min/moment-with-locales.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="/javascript/footable.js"></script>
<script type="text/javascript" src="/javascript/bootstrap-clockpicker.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($){
	
	$(".datepickerdate").datepicker({
		startDate: '<?php echo date('d') ?>-<?php echo date('M') ?>-<?php echo date('Y') ?>',
		format: 'dd-mm-yyyy',
		todayHighlight: true,
		toggleActive: true,
		autoclose: true
	});
    

    
    $('input[type=radio][name=option]').change(function() {
        if (this.value == 'tanggal') {
            $('#showdate').show();
        }else if(this.value == 'now'){
             $('#showdate').hide();
        }
        
    });
	
	$('.clockpicker').clockpicker()
		.find('input').change(function(){
			console.log(this.value);
	});
    
    $('#scheduletype').change(function(){
            x=$(this).val();
            
            if(x=="e_date"){
                $('#e_date').show(); 
                $('#star_date').show();
                $('#s_now').hide();
                $('#e_year').hide(); 
            }else if(x=="e_day"){
                $('#e_day').show(); 
                $('#e_date').hide(); 
                $('#star_date').show();
                $('#e_year').hide(); 
                $('#s_now').hide();
                $('#e_month').hide(); 
            }else if(x=="e_month"){
                $('#e_day').hide(); 
                $('#e_date').hide(); 
                $('#e_month').show(); 
                $('#e_year').hide(); 
                $('#star_date').show();
                $('#s_now').hide();
            }else if(x=="e_year"){
                $('#e_day').hide(); 
                $('#e_date').hide(); 
                $('#e_month').hide(); 
                $('#e_year').show(); 
                $('#star_date').show();
                $('#s_now').hide();
            }else if(x==""){
                $('#e_date').hide(); 
                $('#e_day').hide(); 
                $('#e_year').hide(); 
                $('#star_date').hide();
                $('#e_month').hide(); 
                $('#s_now').show();
            }
			/*$(".star_date").show();
			$(".end_date").show();
			<!--$(".senderid").hide();-->
			$(".preview").hide();

			$('#btnSend').val("Save");
			if ($('#scheduletype').val() == "e_date" ){
				$(".e_date").show();$(".e_day").hide();$(".e_month").hide();$(".e_year").hide();
			}else if ($('#scheduletype').val() == "e_day"){
				$(".e_day").show();$(".e_date").hide();$(".e_month").hide();$(".e_year").hide();
			}else if ($('#scheduletype').val() == "e_month"){
				$(".e_month").show();$(".e_date").hide();$(".e_day").hide();$(".e_year").hide();
			}else if ($('#scheduletype').val() == "e_year"){
				$(".e_year").show();$(".e_date").hide();$(".e_day").hide();$(".e_month").hide();
			}else if ($('#scheduletype').val() == ""){
				$(".e_year").hide();$(".e_date").hide();$(".e_day").hide();$(".e_month").hide();$(".preview").show();<!--$(".senderid").hide();-->
				$(".s_now").show();
				$(".star_date").hide();
				$(".end_date").hide();
				$('#btnSend').val("Send");
			}*/
		});
	
	$(".datetimepicker").datetimepicker({
		format: 'DD-MM-YYYY',
		locale: 'id' 
	});
	
	/* check all id function */
	$(".checkedid_all").click(function(){
		if ( (this).checked == true ){

		   $('.checkbox-area').prop('checked', true);

		} else {

		   $('.checkbox-area').prop('checked', false);

		}
	});
	
	/*delete bulk function */
	$(document).on('click', '.delete_bulk', function(event) {
		con=$(this);
		if(con.attr('title')){
			var orititle=con.attr('title');
		}else{
			var orititle=con.attr('data-original-title');
		}
		event.preventDefault();

		if(confirm( orititle.replace('Delete: ', '') + '?')){
			form=$('#bulk_action');
			$.ajax({
				type: 'POST',
				url: form.attr('action')+'&js=on',
				data: form.serialize(),
				beforeSend: function(data){
					form.attr('disabled','disabled');
				},
				success: function(data) {
					if(data){
						data = data.trim();
						if (data=='ok') {
							 window.location = 'sms_schedule.php?pg=<?php print ($pg) ? $pg.'' : ''; ?>';
						}
						else if(data.slice(0,7)=='<script' || data.slice(-1,8)=='/script>'){
							$(result).html(data);
						}else{
							$('#result').show();
							$('#result').html(data);
							$(form).click(function(form) {
								$(result).fadeOut(500);
							});
						}
					}
					con.removeAttr('disabled');
				}
			});
		}
	});
});
function countChar(val) {
    var len = val.value.length;
    var jumsms = Math.ceil(len/160);
    $('#charNum').text(len + ' Karakter');
    $('#countSMS').text(jumsms + ' SMS');

}
</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">
			<div class="row">
				<div class="col-md-12">
					<h3><?php print _('sms_schedule_title'); ?></h3>
                    <p><?php print _('sms_schedule_title_desc'); ?></p>
					<hr>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
                    <?php 
					if ($action==""){
                    ?>
                   <div class="row">
	                    <div class="col-md-7 pull-right">
	                        <div class="pull-right">
	                            <form name="search" method="get" class="form-inline">
	                                <div class="form-group">
	                                    <input name="searchtext" type="text" class="form-control input-sm" id="keyword" size="40" value="<?php print $searchtext;?>" />
	                                </div>
	                                <button type="submit" name="Submit" value="<?php print _('webbot_find'); ?>" class="btn btn-sm btn-dark"><?php print _('search'); ?></button>
	                            </form>
	                        </div>
	                    </div>
	                </div>
	                <br/>
                    <div class="row">
						<form action="sms_schedule.php?action=delete&amp;form=submit" method="post" enctype="multipart/form-data" name="bulk_action" id="bulk_action">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
										<thead>
											<tr>
												<th style="width:20px;" class="text-center"><input name="checkedid_all" class="checkedid_all" type="checkbox" value="y" /></th>
												<th style="width:20px;" data-breakpoints="xs" class="text-center"><?php print _('cms_no'); ?></th>
												<th style="width:120px;" class="text-center"><?php print _('sms_schedule_to'); ?></th>
												<th style="width:450px;" data-breakpoints="xs" class="text-center"><?php print _('sms_schedule_text'); ?></th>
												<th style="width:230px;" data-breakpoints="xs" class="text-center"><?php print _('sms_schedule_next_send'); ?></th>
												<th style="width:325px;" data-breakpoints="xs" class="text-center"><?php print _('sms_schedule_schedule_type'); ?></th>
												<th style="width:100px;" class="text-center" class="text-center"><?php print _('sms_schedule_from'); ?></th>
												<th style="width:20px;" data-breakpoints="xs" class="text-center"></th>
												<th style="width:20px;" class="text-center"></th>
												
											</tr>
										</thead>
										<tbody data-empty="bcdcbd">
											<?php
											if(empty($pg)){
												$pg=1;
											}

											$numPerPage=20;
											$offset=($pg-1)*$numPerPage;
											$num=1;
											$num=$num+(($pg-1)*$numPerPage);
							
											if($searchtext){
												$whereClause[]="(schText like '%$searchtext%' OR schTo LIKE '%$searchtext%' OR smsSenderId='$searchtext')";
											}

											if(is_array($whereClause)){
												$whereClause=implode('  and ',$whereClause);
											}
											if($searchtext){
												$whereClause="AND $whereClause";
											}    
										
											$sql="SELECT * FROM sms_schedule WHERE schId!=0 $whereClause ORDER BY schAddedOn DESC";
											$query=query($sql);
											$numofdata=rows($query);
											$sql.=' LIMIT '.$offset.','.$numPerPage;
											$query=query($sql);
											if($numofdata>0){ 
												$arrayhistory = array();
												while($data = fetch($query)){
													$arrayhistory[] = $data;
													$sql2 = "SELECT * FROM sms_history WHERE historyMessageId='$data[messageId]' ORDER BY historyId DESC LIMIT 1";
													$query2 = query($sql2);
													$data2 = fetch($query2);
													$historyresponse = $data2['historyResponse'];
													$decodejson = json_decode($historyresponse, true);
													$statusgroupid = $decodejson['results'][0][status][groupId];
													$statusgroupname= $decodejson['results'][0][status][groupName];
													
													$contactname = getval("conFullName","contacts_sms","conHP='{$data['schTo']}'");
													
													if($data['schType'] == 'multiple'){
														$typeofschedule="Every {$data['schRules']} day".($data['schRules'] > 1 ? 's':'');
													}elseif($data['schType'] == 'day'){

														$typeofschedule="Every ".str_replace(',', ', ', $data['schRules']);
													}elseif($data['schType'] == 'month'){
														$typeofschedule="Every month on ".str_replace(',', ', ', $data['schRules']);
													}elseif($data['schType'] == 'year'){
														$typeofschedule="Every Year on ".$data['schRules'];
													}
											?>
											<tr>
												<td >
													<input name="checkedid[]" class="checkbox-area" type="checkbox" value="<?php print $data['schId']; ?>" />
												</td>
												<td class="text-center"><?php print $num; ?></td>
												<td>
													<strong><?php print output($contactname); ?></strong>
													<br/>
													&lsaquo;&lsaquo;<?php print output($data['schTo']); ?>&rsaquo;&rsaquo;
												</td>
												<td>
													<?php print substr(output($data['schText']),0,15); ?>...
													<a rel="tooltip" class="label label-warning" data-toggle="modal" data-target="#<?php print $data['schId']; ?>" style="cursor:pointer" title="<?php print _('sms_template_view_detail'); ?>">...</a>
													<!---modal area -->
													<div id="<?php print $data['schId']; ?>" class="modal fade" role="dialog">
														<div class="modal-dialog modal-lg">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal">&times;</button>
																	<h4 class="modal-title"><?php print _('sms_schedule_detail'); ?></h4>
																</div>
																<div class="modal-body get-margin-xx">
																	<div class="row">
																	  <div class="col-md-4 col-sm-4 col-xs-12"><strong><?php print _('sms_schedule_name'); ?></strong></div>
																	  <div class="col-md-1 col-sm-1 hidden-xs">:</div>
																	  <div class="col-md-7  col-sm-7 col-xs-12"><?php print output($contactname); ?></div>
																	</div>
																	<div class="row">
																	  <div class="col-md-4 col-sm-4 col-xs-12"><strong><?php print _('sms_schedule_number'); ?></strong></div>
																	  <div class="col-md-1 col-sm-1 hidden-xs">:</div>
																	  <div class="col-md-7  col-sm-7 col-xs-12"><?php print output($data['schTo']); ?></div>
																	</div>
																	<div class="row">
																	  <div class="col-md-4 col-sm-4 col-xs-12"><strong><?php print _('sms_schedule_text'); ?></strong></div>
																	  <div class="col-md-1 col-sm-1 hidden-xs">:</div>
																	  <div class="col-md-7  col-sm-7 col-xs-12"><?php print nl2br(output($data['schText'])); ?></div>
																	</div>
																	<div class="row">
																	  <div class="col-md-4 col-sm-4 col-xs-12"><strong><?php print _('sms_schedule_type'); ?></strong></div>
																	  <div class="col-md-1 col-sm-1 hidden-xs">:</div>
																	  <div class="col-md-7  col-sm-7 col-xs-12"><?php print $typeofschedule; ?></div>
																	</div>
																	<div class="row">
																	  <div class="col-md-4 col-sm-4 col-xs-12"><strong><?php print _('sms_schedule_create'); ?></strong></div>
																	  <div class="col-md-1 col-sm-1 hidden-xs">:</div>
																	  <div class="col-md-7  col-sm-7 col-xs-12"><?php print $data['schAddedOn']!=0 ? date('d F Y ',$data['schAddedOn']) : "-"; ?></div>
																	</div>
																	<div class="row">
																	  <div class="col-md-4 col-sm-4 col-xs-12"><strong><?php print _('sms_schedule_start'); ?></strong></div>
																	  <div class="col-md-1 col-sm-1 hidden-xs">:</div>
																	  <div class="col-md-7  col-sm-7 col-xs-12"><?php print $data['schStartDateOn']!=0 ? date('l, d F Y H:i',$data['schStartDateOn']).' WIB' : "-"; ?></div>
																	</div>
																	<div class="row">
																	  <div class="col-md-4 col-sm-4 col-xs-12"><strong><?php print _('sms_schedule_end'); ?></strong></div>
																	  <div class="col-md-1 col-sm-1 hidden-xs">:</div>
																	  <div class="col-md-7  col-sm-7 col-xs-12"><?php print $data['schEndDateOn']!=0 ? date('l, d F Y H:i',$data['schEndDateOn']).' WIB' : "-"; ?></div>
																	</div>
																	<div class="row">
																	  <div class="col-md-4 col-sm-4 col-xs-12"><strong><?php print _('sms_schedule_status'); ?></strong></div>
																	  <div class="col-md-1 col-sm-1 hidden-xs">:</div>
																	  <div class="col-md-7  col-sm-7 col-xs-12">
																		<i class="fa <?php echo ($data['schStatus']=='active' ? 'fa-check':'fa-times');?>"></i>
																		<?php print $data['schStatus']=='active' ? "Active": "NonActive"; ?>
																		</div>
																	</div>
																	<div class="row">
																	  <div class="col-md-4 col-sm-4 col-xs-12"><strong><?php print _('sms_schedule_last_sent'); ?></strong></div>
																	  <div class="col-md-1 col-sm-1 hidden-xs">:</div>
																	  <div class="col-md-7  col-sm-7 col-xs-12"><?php print $data['schLastSent']!=0 ? date('l, d F Y H:i',$data['schLastSent'])." WIB" : "-"; ?></div>
																	</div>
																	<div class="row">
																	  <div class="col-md-4 col-sm-4 col-xs-12"><strong><?php print _('sms_schedule_next_send'); ?></strong></div>
																	  <div class="col-md-1 col-sm-1 hidden-xs">:</div>
																	  <div class="col-md-7  col-sm-7 col-xs-12"><?php print date('l, d F Y H:i', $data['schNextSend']);?> WIB</div>
																	</div>
																	<div class="row">
																	  <div class="col-md-4 col-sm-4 col-xs-12"><strong><?php print _('sms_schedule_send_by'); ?></strong></div>
																	  <div class="col-md-1 col-sm-1 hidden-xs">:</div>
																	  <div class="col-md-7  col-sm-7 col-xs-12"><?php print $data['smsSenderId'];?></div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</td>
												<td class="text-center">
													<?php print $data['schNextSend'] ? date('d M Y H:i',$data['schNextSend']) : '';?> WIB
												</td>
												 <td><?php print $typeofschedule; ?></td>
												<td class="text-center">
													<?php print output($data['smsSenderId']); ?>
												</td>
												<td class="text-center">
													<a rel="tooltip" data-toggle="modal" data-target="#<?php print $data['schId']; ?>" style="cursor:pointer" title="<?php print _('sms_template_view_detail'); ?>"><i class="fa fa-list"></i></a>
												</td>
												<td class="text-center">
													<a href="?action=edit&id=<?php print $data['schId']; ?>/" rel="tooltip" title="<?php print _('sms_schedule_edit'); ?> <?php print substr(output($data['schText']),0,15); ?>"><i class="fa fa-edit"></i></a>
												</td>
												
											</tr>
											<?php
												$num++;
												}
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-md-12" style="margin-bottom:15px;">
								<div class="row">
									<div class="col-md-12 col-sm-9 col-xs-12">
										<button title="<?php print _('delete_question'); ?>" class="btn btn-danger delete_bulk"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp;<?php print _('button_delete'); ?></button>
									</div>
								</div>
							</div>
						</form>
                        <?php
                        $options['total']=$numofdata;
                        $options['filename']='sms_schedule.php';
                        $options['qualifier']='sms_schedule.php';
                        $options['pg']=$pg;
                        $options['numPerPage']=$numPerPage;
                        $options['style']=1;
                        $options['addquery']=true;
                        paging($options);
                        ?>
                    </div>
                    
                    <?php
                    }else if($action=="edit"){
                        $sql="SELECT * FROM sms_schedule WHERE schId ='$id'";
                        $query=query($sql);
                        $data=fetch($query);
                        $data=formoutput($data);
						
						$contactName=getval('conFullName','contacts_sms','conHP',$data['schTo']);
                     
						?>
						<a href="sms_schedule.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
						<h4><?php print _('sms_template_edit_title'); ?></h4>
						<hr/>
						<form action="sms_schedule.php?form=submit&amp;action=edit&id=<?php print $id; ?>" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">
                        <input type="hidden" name="getbalance" id="getbalance" value="<?php print $balance['balance']; ?>">
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
	                        	<?php print _('sms_from'); ?><span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-4 col-xs-12">
	                        	<select class="form-control" name="senderid" required>
	                        	    <option value="<?php print getconfig('SMS_API_FROM'); ?>"><?php print getconfig('SMS_API_FROM'); ?></option>
                                    
	                        	</select>
	                        </div>
                        </div>
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
	                        	<?php print _('sms_to'); ?>
	                        </label>
	                        <div class="col-md-4 col-sm-4 col-xs-12">
                               <?php print $data['schName'] .(($contactName ? ucfirst($contactName):'<i>(unknown)</i>') .' &laquo<b>'.$data['schTo']."</b>&raquo");?>
	                        </div>
                        </div>
                         <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="mCatPicLogo">
	                        	<?php print _('sms_use_template'); ?>  
	                        </label>
	                        <div class="col-md-4 col-sm-4 col-xs-12">
								<div class="list-template-box">
									<ul class="list-template">
										<?php 
										$sqltemp ="SELECT * FROM template_sms order by tempName ASC";
										$querytemp = query($sqltemp);
										while($datatemp =fetch($querytemp)){
											?>
											<li style="list-style:none;margin-left:-30px"><i class="fa fa-angle-right"></i>&nbsp;<a href="javascript:;" onclick="document.add.textd.value='<?php print stripquote(ereg_replace("\r\n"," ",$datatemp['tempTemplate'])); ?>'; document.add.textd.focus();"><?php print $datatemp['tempName']; ?></a></li>
											<?php 
										}
										?>
									</ul>
								</div>
	                        </div>
                    	</div>
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="mCatPicLogo">
	                        	<?php print _('sms_text'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-4 col-xs-12">
                                <textarea class="form-control" cols="10" rows="10" name="message" id="textd" onkeyup="countChar(this)" onfocus="countChar(this)" required><?php print $data['schText']; ?></textarea>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 col-sm-6 col-lg-6">
                                        <div id="charNum" class="help-block" style="font-weight: bold;">0 Karakter</div>
                                    </div>
                                    <div class="col-md-6 col-xs-6 col-sm-6 col-lg-6">
                                        <div id="countSMS" class="help-block text-right" style="font-weight: bold;">0 SMS</div>
                                    </div>
                                </div> 
                                <a href="#modal-smsvarguide" role="button" class="btn btn-success" data-toggle="modal"><i class="fa fa-list"></i> SMS Variable Guide</a>
	                        </div>
                    	</div>
                        
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
	                        	<?php print _('sms_schedule'); ?>
	                        </label>
	                        <div class="col-md-4 col-sm-4 col-xs-12 label-xx">
								<?php
								if($data['schType'] == 'multiple'){
									$scTypeStr = _('sms_schedule_selected_date');
									$scType = 'e_date';
								} elseif($data['schType'] == 'day'){
									$scTypeStr = _('sms_schedule_every_day');
									$scType = 'e_day';
								} elseif($data['schType'] == 'month'){
									$scTypeStr = _('sms_schedule_every_month');
									$scType = 'e_month';
								} else{
									$scTypeStr = _('sms_schedule_every_year');
									$scType = 'e_year';
								}

								print "<label class=\"label label-primary \">$scTypeStr</label>
								<br/>
								<br/>";
								?>
								<input type="hidden" name="number" value="<?php print $data['schTo'];?>" />
								<input type="hidden" name="scheduletype" value="<?php print $scType;?>" />
								<?php 
								if($data['schType'] == 'multiple' ){
									?>
									<div class="form-group"  id="e_date" style="width:150px;">
										<label><?php print _('sms_schedule_select_multiple'); ?></label>
										<select name="s_mul" class="form-control">
										   <option value="">- <?php print _('sms_schedule_select_multiple'); ?> -</option>
										   <?php for ($i=1; $i<=10; $i++) { ?>
										   <option value="<?php print $i; ?>" <?php print $data['schRules']==$i ?'selected="selected"' :'';  ?>><?php print $i; ?></option>
										 <?php }?>
										 </select>
									</div>
									<?php 
								}elseif($data['schType'] == 'day'){
									$dy = explode(',',$data['schRules']);
									?>
									<div class="form-group"  id="e_day">
										<label>Select Day</label><br>
										<label class="checkbox-inline"><input <?php print in_array("Sunday",$dy) ? 'checked="checked"' : ''; ?> type="checkbox" name="s_day[0]" value="Sunday" />Sunday</label>
										<label class="checkbox-inline"><input <?php print in_array("Monday",$dy) ? 'checked="checked"' : ''; ?> type="checkbox" name="s_day[1]" value="Monday" />Monday</label>
										<label class="checkbox-inline"><input <?php print in_array("Tuesday",$dy) ? 'checked="checked"' : ''; ?> type="checkbox" name="s_day[2]" value="Tuesday" />Tuesday</label>
										<label class="checkbox-inline"><input <?php print in_array("Wednesday",$dy) ? 'checked="checked"' : ''; ?> type="checkbox" name="s_day[3]" value="Wednesday" />Wednesday</label><br>
										<label class="checkbox-inline"><input <?php print in_array("Thursday",$dy) ? 'checked="checked"' : ''; ?> type="checkbox" name="s_day[4]" value="Thursday" />Thursday</label>
										<label class="checkbox-inline"><input <?php print in_array("Friday",$dy) ? 'checked="checked"' : ''; ?> type="checkbox" name="s_day[5]" value="Friday" />Friday</label>
										<label class="checkbox-inline"><input <?php print in_array("Saturday",$dy) ? 'checked="checked"' : ''; ?> type="checkbox" name="s_day[6]" value="Saturday" />Saturday</label>
									</div>
									<?php 
								}elseif($data['schType'] == 'month'){
									?>
									<div class="form-group"  id="e_month">
										<label>Select Date</label><br>
										 <?php 
										 $dt = explode(',',$data['schRules']); 
										 for ($i=1; $i<=28; $i++) { 
											if ($i<10) { $i="0$i"; }  
											?>
										<label class="checkbox-inline">
											
										<input type="checkbox" name="s_date[<?php print (int) $i;?>]" value="<?php echo (int) $i; ?>" style="margin-right:-2px" <?php print in_array((int) $i, $dt) ? 'checked="checked"' : ''; ?>/> <?php echo"$i"; ?></label>
										 <?php
										} ?>
									</div>
									<?php 
								}elseif($data['schType'] == 'year'){
									list($d, $m) = explode(' ', $data['schRules']);
									?>
									<div class="row" id="e_year"  >
										<div class="col-md-6">
											<div class="form-group" >
												<label>Select Date</label>
												<select name="cdate" class="form-control">
												   <option value="">Date</option>
												   <?php for ($i=1; $i<=28; $i++) { ?>
												   <option <?php print $d==$i ? 'selected="selected"' : ''; ?> value="<?php print $i; ?>"><?php print $i; ?></option>
												 <?php } ?>
												 </select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group" >
												<label>Month</label>
												<select name="cmon" class="form-control">
													 <option value="">Month</option>
													 <?php for ($i=1; $i<=12; $i++) { ?>
													 <option value="<?php convertidtomonthEng($i); ?>" <?php print $m==convertidtomonthEng($i) ? 'selected="selected"' : ''; ?>><?php print convertidtomonthEng($i); ?></option>
													 <?php } ?>
												</select>
											</div>
										</div>
									</div>
									<?php 
								}
								?>
                                <div id="star_date"  >
                                    <div class="form-group" style="width:150px;">
                                        <label>Time</label>
                                        <select name="schintime" id="schintime" class="form-control">
                                            <?php 
											list($sh,$sm)	= explode(':',$data['schTime']);
											for ($i=0;$i<=23;$i++){
											$h=($i<10 ?"0":"");
											$h.=$i;
                                            ?>
                                            <option value="<?php print ($i<10?"0":""); ?><?php print $i; ?>:00" <?php if($sh==$h and $sm=='00'){ print ' selected="selected"';}?>><?php print ($i<10)?"0":""; ?><?php print $i; ?>:00</option>
                                            <?php   
                                            }
											?>
                                          </select>
                                    </div>
                                    <div class="form-group" >
                                        <div class="row">
                                            <div class="col-md-6">
                                               <label>Start Date</label>
                                                <div class="form-group input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                                    <input name="startdatesend" value="<?php print date('d-m-Y',$data['schStartDateOn']); ?>" type="text" class="datepickerdate form-control" aria-describedby="basic-addon1">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Time</label>
                                                <select name="starttime" id="starttime" class="form-control">
                                                       <?php for ($i=0;$i<=23;$i++){
                                                        $h=($i<10 ?"0":"");
                                                        $h.=$i;
                                                        ?>
                                                       <option <?php if(date('H', $data['schStartDateOn'])==$h){ print ' selected="selected"';}?>  value="<?php print ($i<10?"0":""); ?><?php print $i; ?>:00"<?php if($sendingHour==$h and $sendingMinute=='00'){ print ' selected="selected"';}?>><?php print ($i<10)?"0":""; ?><?php print $i; ?>:00</option>
                                                       <?php
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" >
                                    <div class="row">
                                        <div class="col-md-6">
                                           <label>End Date</label>
                                            <div class="form-group input-group">
                                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                                <input name="enddatesend" value="<?php print date('d-m-Y',$data['schEndDateOn']); ?>"  type="text" class="datepickerdate form-control" aria-describedby="basic-addon1">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Time</label>
                                            <select name="endtime" id="endtime" class="form-control">
                                                   <?php for ($i=0;$i<=23;$i++){
                                                    $h=($i<10 ?"0":"");
                                                    $h.=$i;
                                                    ?>
                                                   <option <?php if(date('H', $data['schEndDateOn'])==$h and $data['schEndDateOn'] != 0){ print ' selected="selected"';}?> value="<?php print ($i<10?"0":""); ?><?php print $i; ?>:00"<?php if($sendingHour==$h and $sendingMinute=='00'){ print ' selected="selected"';}?>><?php print ($i<10)?"0":""; ?><?php print $i; ?>:00</option>
                                                   <?php
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                </div>
	                        </div>
                        </div>
                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"> <?php print _('sms_update'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='sms_schedule.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>
                    </form>
                    
                    <div class="modal fade" id="modal-smsvarguide" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        �
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">
                                        SMS Variable Guide
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table>
                                                <tr>
                                                    <td style="width:150px;"><b>{MOBILE}</b></td>
                                                    <td style="width:250px;">Replaced with contact's phone number</td>
                                                </tr>
                                                <tr>
                                                    <td style="width:150px;"><b>{FULLNAME}</b></td>
                                                    <td style="width:250px;">Replaced with name</td>
                                                </tr>
                                                <tr>
                                                    <td style="width:150px;"><b>{EMAIL}</b></td>
                                                    <td style="width:250px;">Replaced with email</td>
                                                </tr>
                                            </table>
                                            <br>
                                            <strong>Example:</strong><br>
                                            Dear Sir/Madam {FULLNAME}. We have received your message and we will reply as soon as possible to {MOBILE}.<br><br>
                                            <strong>Recipient will receive SMS as:</strong><br>
                                            Dear Sir/Madam John Doe. We have received your message and we will reply as soon as possible to +6281902188999.<br><br>
                                            <strong style="color:red;">WARNING:</strong><br>
                                            <span style="color:red;">If any contact do NOT have the data, it will appear as BLANK</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                        Close
                                    </button> 
                                </div>
                            </div>
                        </div>
                    </div>
						<?php
                    }
                    ?>
				</div>
			</div>
        </div>
        <!-- /page content -->

        <?php include("com/com-footer.php"); ?>
	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>