<?php
session_start();
	$page='2';

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config-image.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/mainmod.php");
	include("../modz/errormsg.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

	//set the restrictions
	$allowedType = array("image/pjpeg","image/jpeg","image/gif","image/png");
	$realExt = array('jpeg','jpg','gif','png');

	if ($form=="submit"){
		if (($action=="add" or $action=="upload") and $uac_add){
			
			$countimg=count($_FILES['file']['name']);
			$nowid=nextid("imgId","imgbank");
			for($i=0;$i<count($_FILES['file']['name']);$i++){
				if($_FILES['file']['name'][$i]){
					$imgName[$i]=$_FILES['file']['name'][$i];
					$imgType[$i]=$_FILES['file']['type'][$i];
					$imgTmp[$i]=$_FILES['file']['tmp_name'][$i];

					//make sure it is allowed
					if(in_array($imgType[$i], $allowedType)){
						$fileExt = array_keys($allowedType, $imgType[$i]);
						$fileExt = $realExt[$fileExt[0]];

						if(is_uploaded_file($imgTmp[$i])){
							//make sure it is really really image
							$img=getimagesize($imgTmp[$i]);
							if(!in_array($imgType[$i],$allowedType) or !(is_numeric($img[0]) or is_numeric($img[1]) or $img[0]>0 or $img[1]>0)){
								//make sure it is allowed
								$error.=errorlist(13);
							}
						} else{
							$error.=errorlist(27);
						}

						//get filename
						$imgName[$i] = str_replace('.','',$imgName[$i]).'.'.$fileExt;
					} else{
						$error.=errorlist(13);
					}
				}
				if(!$error){
					//skip the next rest process if current looping have noimage
					if(empty($imgName[$i])){
						continue;
					}					
					$imgid=nextid("imgId","imgbank");
					chdir($_SERVER['DOCUMENT_ROOT']."/admin");

					$img=uploadit($imgName[$i],$imgTmp[$i],"imgbank",$imgid,IMG_SMALL_WIDTH,IMG_SMALL_HEIGHT,IMG_MEDIUM_WIDTH,IMG_MEDIUM_HEIGHT,$img[0],$img[1]);
					$dir=$img['dir'];
					$imgfilename=$img['filename'];					

					//Is it will use watermark ?
					if($watermark=="y"){
						chdir($_SERVER['DOCUMENT_ROOT']."/admin");
						$imagesrc='../assets/imgbank/' . $dir . '/'.$imgfilename;
						watermark($imagesrc);
					}


					$sql="INSERT INTO imgbank(imgId,imgFile,imgDir) VALUES ('$imgid','$imgfilename','$dir')";
					$query=query($sql);
					

				}
			}

	
		}
	}
?>