<?php
	$page=20;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");	
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");
	
	if ($form=="submit"){		
		if ($action=="dellog" and $uac_delete){
			$sql="DELETE FROM systemlog WHERE logTime=" . $logtime;
			$query=query($sql);
		}elseif($action=="clearlog"){
			$sql="DELETE FROM systemlog";
			$query=query($sql);
			header("location:sitelog.php");
			exit;
		}
		
		//check whether query was successful
		if (!$query){			
			$error.=errorlist(3);
		}
		
		if ($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:sitelog.php");
			}else{
				print "ok";
			}
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _('sitelog_pagetitle'); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- datepicker -->
<link rel="stylesheet" type="text/css" href="/libs/bootstrap-datepicker/css/datepicker3.css" />
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript"> 
jQuery(document).ready(function($){
	$('#result').hide();

	$(".dob1").datepicker({
		/* startDate: '<?php echo date('d') ?>/<?php echo date('m') ?>/<?php echo date('Y') ?>', */
		format: 'dd/mm/yyyy',
		todayHighlight: true,
		toggleActive: true,
		autoclose: true
	});

	$(".dob2").datepicker({
		/* startDate: '<?php echo date('d') ?>/<?php echo date('m') ?>/<?php echo date('Y') ?>', */
		format: 'dd/mm/yyyy',
		todayHighlight: true,
		toggleActive: true,
		autoclose: true
	});
	/*
	$("#dob1").datepicker({ 
		dateFormat: "dd/mm/yy",
		closeAtTop: false, 
		yearRange: "<?php print $starty; ?>:<?php print $endy; ?>",
		showOn: "both", 
		buttonImage: "/assets/images/calendar.png", 
		buttonImageOnly: true 
	});
	
	$("#dob2").datepicker({ 
		dateFormat: "dd/mm/yy",
		closeAtTop: false, 
		yearRange: "<?php print $starty; ?>:<?php print $endy; ?>",
		showOn: "both", 
		buttonImage: "/assets/images/calendar.png", 
		buttonImageOnly: true 
	});
	*/
});
</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('sitelog_pagetitle'); ?></h3>
			                <p><?php print _('sitelog_pagedesc'); ?></p>
			            </div>
		            </div>
	        	</div>
        	</div>
        	<?php if($uac_delete){ ?>
        	<div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                	<div class="pull-right">
                	<a href="javascript: if (window.confirm('<?php print _('sitelog_windowsconfirm'); ?>?')){ window.location='sitelog.php?form=submit&action=clearlog';};" title="clear log" class="btn btn-primary"><?php print _('sitelog_emptysitelog'); ?></a>
                	</div>
                </div>
        	</div>
        	<?php } ?>
        	<div class="row">
	        	<div class="col-md-12">
	        		<?php if ($action==""){ ?>
	        			<div class="row">
	        				<div class="col-md-9 pull-right">
	        					<div class="pull-right">
	        						<form name="search" method="get" class="form-inline">
	        							<input name="cid" type="hidden" value="<?php print $cid;?>">

	        							<div class="form-group">
	        								<select name="category" class="form-control input-sm">
												<option value=""><?php print _('sitelog_all'); ?></option>
												<?php $selected=($category)?"selected":""; ?>
											    <option value="2" <?php print ($category==2)?$selected:"";?>><?php print _('sitelog_web'); ?></option>
											    <option value="1" <?php print ($category==1)?$selected:"";?>><?php print _('sitelog_admin'); ?></option>
												<?php
												$sql="SELECT adminUsername FROM admin ORDER BY adminUsername ASC";
												$query=query($sql);
												while ($data=fetch($query)){
													?>
													<option value="<?php print $data['adminUsername'];?>"<?php print ($category==$data['adminUsername'] ? ' selected="selected"':'');?>>&nbsp;&nbsp;- <?php print $data['adminUsername'];?></option>
													<?php
												}
												?>
											</select>
	        							</div>
	        							<?php
											//set value for datestart
											if($datestart){
												$datestart=$datestart;						
												list($date, $time)=explode(" " , $datestart);
												list($day,$month,$year)=explode("/", $date);				
												$datestart=mktime(0,0,0,$month,$day,$year);
											}else{
												$datestart=$now;
											}
											
											//set value for dateend
											if($dateend){
												$dateend=$dateend;
												list($date, $time)=explode(" " , $dateend);
												list($day,$month,$year)=explode("/", $date);							
												$dateend=mktime(23,59,59,$month,$day,$year);						
											}else{
												$dateend=$now;
											}
										?>
										<div class="form-group input-group">
											<input name="datestart" type="text" id="dob1" size="12" value="<?php print date("d/m/Y", $datestart); ?>" class="form-control input-sm dob1" />
											<span class="add-on input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
										<div class="form-group input-group">
											<input name="dateend" type="text" id="dob2" size="12" value="<?php print date("d/m/Y", $dateend); ?>"  class="form-control input-sm dob2" />
											<span class="add-on input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>

										<button type="submit" name="Submit" value="<?php print _('sitelog_find'); ?>" class="btn btn-sm btn-dark"><?php print _('sitelog_find'); ?></button>
	        						</form>
	        					</div>
	        				</div>
	        			</div>
	        			<div class="table-responsive" style="margin-top:12px;">
	        				<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
                				<thead>
                					<tr>
                						<th width="35"><?php print _('sitelog_numb'); ?></th>
										<th width="130"><?php print _('sitelog_time'); ?></th>
										<th width="100"><?php print _('sitelog_ip'); ?></th>
										<th><?php print _('sitelog_url'); ?></th>
										<th width="35"><?php print _('sitelog_counter'); ?></th>
										<th width="70"><?php print _('sitelog_admin'); ?></th>
										<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
                					</tr>
                				</thead>
                				<tbody>
                				<?php
							  	if(empty($pg)){
							  		$pg=1;
							  	}
							  	
								$numPerPage=20;
								$offset=($pg-1)*$numPerPage;
								$num=1;
								$num=$num+(($pg-1)*$numPerPage);
								
								if($category){
									if($category==1 or $category==2){
										$whereClause[]="logType='$category'";	
									}
									else{
										$whereClause[]="logAdmin='$category'";	
									}
								}
								
								if($datestart!=$dateend){
									$whereClause[]="logTime BETWEEN '$datestart' AND '$dateend'";
								}
								
								if($whereClause){
									$whereClause = implode(' AND ',$whereClause);
								}
							
								if($datestart!=$dateend or $category){
									$whereClause="WHERE $whereClause";
								}
								
								$sql="SELECT * FROM systemlog $whereClause ORDER BY logTime DESC";			
								$query=query($sql);
								$numofdata=rows($query);
								$sql.=' LIMIT '.$offset.','.$numPerPage;
								$query=query($sql);
								if($numofdata<1){
									?>
									<tr>
										<td colspan="7"><div class="text-center"><br /><?php print _('sitelog_sitelognotavailable'); ?><br /><br /></div></td>
									</tr>
									<?php
								}else{				
									while ($data=fetch($query)){
										?>
										<tr valign="top">
											<td class="text-center"><?php print $num; ?></td>						
											<td><?php print date("d/m/Y H:i:s", $data['logTime']) ; ?></td>
											<td><?php print $data['logIp']; ?></td>
											<td><?php print $data['logUrl']; ?></td>
											<td class="text-center"><?php print number_format($data['logCounter'],0,',','.'); ?></td>
											<td class="text-center"><?php print ($data['logAdmin'] ? $data['logAdmin']:'-');?></td>
											<td class="text-center"><?php if ($uac_delete){ ?><a class="delete" href="sitelog.php?form=submit&action=dellog&logtime=<?php print $data['logTime']; ?>" title="Hapus log berikut: <?php print $data['logUrl']; ?>"><img src="../assets/images/icondel.gif" width="15" height="13" alt="Hapus" /></a><?php } ?></td>
										  </tr> 
										<?php
										$num++;
									}				
								}
								?>
                				</tbody>
                			</table>
	        			</div>
	        			<?php 
	        			$options['total']=$numofdata;
						$options['filename']='sitelog.php';
						$options['qualifier']='sitelog.php';
						$options['pg']=$pg;
						$options['numPerPage']=$numPerPage;
						$options['style']=1;
						$options['addquery']=TRUE;
						paging($options);
						?>
	        		<?php } ?>
	        	</div>
	        </div>


		</div>
        <!-- END THE CONTENT OF PAGE HERE -->


		<?php include("com/com-footer.php"); ?>

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>