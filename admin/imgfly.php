<?php
session_start();
	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config-image.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Image Library</title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">
<link href="/style/dropzone.css" rel="stylesheet">
<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/javascript/dropzone.js"></script>

<script type="text/javascript"> 
jQuery(document).ready(function($){
$('#result').hide();
    validate('#result','#my-dropzone','imgfly.php');
});
</script>
<style type="text/css">
	body.flyctn{
		height: 800px !important;
	}
	.btn{
		padding: 2px !important;
	}
	.box-drop{
		margin-bottom: 5px;
	}
</style>
<script type="text/javascript">
	function copyToClipboard(element,imgno) {
	  var $temp = $("<input>");
	  $("body").append($temp);
	  $temp.val($(element).text()).select();
	  document.execCommand("copy");
	  $temp.remove();
	  var $trigthis = $("#copyi"+imgno);
	  $trigthis.tooltip({ title: "Copied!"});
	  $trigthis.tooltip('show');
	}
	function copy(selector,imgno){
	  var $temp = $("<span>");
	  $("font").append($temp);
	  $temp.attr("contenteditable", true)
	       .html($(selector).html()).select()
	       .on("focus", function() { document.execCommand('selectAll',false,null) })
	       .focus();
	  document.execCommand("copy");
	  $temp.remove();
	  var $trigthis = $("#copyu"+imgno);
	  $trigthis.tooltip({ title: "Copied!"});
	  $trigthis.tooltip('show');
	}
</script>
</head>
<body class="flyctn">
<div id="result"></div>

<div class="container flybody">

	<div class="row">
		<div class="col-md-12">
			<!-- content-->
	        <div role="tabpanel" data-example-id="togglable-tabs">
	        	<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
		            <li><a href="imgfly.php">Upload</a>
		            </li>
		            <li><a href="imgfly.php?tab=library">Media Library</a>
		            </li>
	        	</ul>
		        <div id="myTabContent" class="tab-content" style="width:100%;">
		        	<?php if($tab!="library"){ ?>
					<div>
						<?php if($upload=="y"){
							$idthis=$_SESSION['nowid'];
							if(!empty($idthis)){
							$sql="SELECT * FROM imgbank WHERE imgId>='$idthis'";
							$query=query($sql);
						?>
						<div class="alert alert-success alert-dismissible fade in" role="alert">
			                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
			                </button>
			                Images have been uploaded. Here is its URL:<br/>
			                <?php while ($data=fetch($query)) { ?>
			                /assets/imgbank/<?php print $data['imgDir']; ?>/<?php print $data['imgFile']; ?> <br/>
			                <?php } ?>
			            </div>
						<?php unset($_SESSION['nowid']); } } ?>
							
						<form action="imgfly-upload.php?action=upload&amp;form=submit&amp;source=imgfly" id="my-dropzone" class="dropzone form-horizontal form-label-left">
						<div class="form-group">
                          	<label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _('img_view_imagefile'); ?> <span class="required">*</span>
                          	</label>
                          	<div class="col-md-6 col-sm-6 col-xs-12">
                            	<div class="drop-here"><div class="dz-message" data-dz-message><span>Drop files here</span></div></div>
                				<div class="table table-striped drop-table" class="files" id="previews">
									<div id="template" class="file-row">
										<table class="table table-bordered">
										  <tr>
										    <td width="100"><span class="preview"><img data-dz-thumbnail /></span></td>
										    <td>
										      <div>
										          <p class="name" data-dz-name></p>
										          <strong class="error text-danger" data-dz-errormessage></strong>
										      </div>
										      <div>
										          <p class="size" data-dz-size></p>
										          <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
										            <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
										          </div>
										      </div>
										      <div>
										        <button data-dz-remove class="btn btn-warning cancel">
										            <i class="glyphicon glyphicon-ban-circle"></i>
										            <span>Cancel</span>
										        </button>
										      </div>
										    </td>
										  </tr>
										</table>
									</div>
                				</div>
                            	<span class="help-block"><?php print _("img_view_addpagedesc"); ?></span>
                          	</div>
                      	</div>
                      	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                            <div class="checkbox">
				                    <label>
				                       	<input type="checkbox" name="watermark" value="y" id="watermark" /> Watermark
				                    </label>
				                </div>
                         	</div>
                       	</div>
                      	<hr/>

	                    <div class="form-group">
	                        <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                            <button name="Submit" id="submit-all" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
	                            <button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='img.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                            <button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
	                    </div>
          				</form>
					</div>
					<?php }else{ ?>
					<font></font>
					<div style="width:100%;">
						<div class="box-manager">
				    	<?php
				    		if (empty($pg)){
								$pg=1;
							}
							$numPerPage=16;
							$offset=($pg-1)*$numPerPage;

							$sql="SELECT * FROM imgbank ORDER BY imgId DESC";
							$query=query($sql);
							$numofdata=rows($query);

							$sql.=' LIMIT '.$offset.','.$numPerPage;
							$query=query($sql);
							$rows=rows($query);
							if($rows<1){
						?>
						<p class="text-center">Image is empty</p>
						<?php } else { 
							$noi=1;
							while($data=fetch($query)){
						?>
						<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 box-drop">
			                <div class="panel panel-default">
			                  	<div class="panel-heading">
			                    	<img src="/assets/imgbank/<?php print $data['imgDir']; ?>/<?php print genthumb($data['imgFile'],"m"); ?>" alt="noname"/>
			                  	</div>
			                  	<table>
								<tr>
									<td width="50%">
										<p id="url-i<?php print $noi; ?>" style="display:none;">/assets/imgbank/<?php print $data["imgDir"]; ?>/<?php print $data['imgFile']; ?></p>
									<button id="copyi<?php print $noi; ?>" data-toggle="tooltip" class="btn btn-default" onclick="copyToClipboard('#url-i<?php print $noi; ?>',<?php print $noi; ?>)">Copy URL</button>
									</td>
									<td width="50%">
										<p id="demo<?php print $noi; ?>" style="display:none;"><img src="/assets/imgbank/<?php print $data['imgDir']; ?>/<?php print $data['imgFile']; ?>" alt="noname"/></p>
									<button id="copyu<?php print $noi; ?>" data-toggle="tooltip" class="btn btn-primary" onclick="copy('#demo<?php print $noi; ?>',<?php print $noi; ?>)">Copy Gambar</button>
									</td>
								</tr>
							</table>
			                </div>
			            </div>
						<?php $noi++; } ?> 
						<div style="clear:both;"></div>
						<div class="text-center">
						<?php
							$options['total']=$numofdata;
							$options['filename']='imgfly.php';
							$options['qualifier']='imgfly.php';
							$options['pg']=$pg;
							$options['numPerPage']=$numPerPage;
							$options['style']=2;
							$options['addquery']=TRUE;
							paging($options);
						?>
						</div>
						<?php } ?>
						</div>
					</div>
					<?php } ?>
				</div>
		    </div>
			<!-- end content -->
		</div>
	</div>

</div>


</div>
<?php if($tab!="library"){ ?>
<script>
    var previewNode = document.querySelector("#template");
    previewNode.id = "";
    var previewTemplate = previewNode.parentNode.innerHTML;
    previewNode.parentNode.removeChild(previewNode);
    Dropzone.options.myDropzone = {

    // Prevents Dropzone from uploading dropped files immediately
    previewTemplate: previewTemplate,
    previewsContainer: '#previews',
    acceptedFiles: ".png,.jpg,.jpeg",
    uploadMultiple: true,
    autoProcessQueue: false,
    parallelUploads:100,

    init: function() {
    var submitButton = document.querySelector("#submit-all")
    myDropzone = this; // closure

    submitButton.addEventListener("click", function() {
    myDropzone.processQueue(); // Tell Dropzone to process all queued files.
    console.log(myDropzone);
    });

    // You might want to show the submit button only when 
    // files are dropped here:
    this.on("sending", function(file, responseText) {
    // Handle the responseText here. For example, add the text to the preview element:
    	$("#submit-all").attr("disabled", true);
    });
    this.on("processingmultiple", function(file, responseText) {
    // Handle the responseText here. For example, add the text to the preview element:
    	$("#submit-all").attr("disabled", true);
    });
    this.on("processing", function(file, responseText) {
    // Handle the responseText here. For example, add the text to the preview element:
    	$("#submit-all").attr("disabled", true);
    });
    this.on("successmultiple", function(files, response) {
      // Gets triggered when the files have successfully been sent.
      // Redirect user or notify of success.
      console.log(response);
      console.log(files);
      window.location = "imgfly.php?upload=y";
    });
    this.on("success", function(file, responseText) {
    // Handle the responseText here. For example, add the text to the preview element:
    $("#submit-all").hide();
    });
    this.on("errormultiple", function(files, response) {
      // Gets triggered when there was an error sending the files.
      // Maybe show form again, and notify user of error
    
    if(response=="Server responded with 0 code."){
      window.location = "imgfly.php";    
    }else{
      window.location = "imgfly.php";    
    }

    });


    }
    };
</script>
<?php } ?>
</body>
</html>