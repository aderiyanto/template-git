<?php
	#this file only allow to execute by using cron or cli command
/* 	if(! defined('STDIN') ){
		die('Forbidden Access');
	} */

	//includes all files necessary to support operations
	$basepath = realpath(dirname(__file__).'/..');
 	include($basepath."/modz/config-main.php");
	include($basepath."/modz/config.php");
	include($basepath."/modz/license.php");
	include($basepath."/modz/errormsg.php");
	include($basepath."/modz/mainmod.php");
	include($basepath."/modz/connic.php");
	include($basepath."/modz/getall-admin.php");

	$webprotocol="http://";
	/*
		0.8-1.0: Homepage, subdomains, product info, major features.
		0.4-0.7: Articles and blog entries, category pages, FAQs.
		0.0-0.3: Outdated news, info that has become irrelevant.

		Define the value of the array if the url has been define before on .htaccess,
		thus the permalink will not retrive from the database anymore
	*/
	#1 Navigation
	$sitemapconfig=array();
	$sitemapconfig['sitemaptype']='navigation'; //articlelike or navigation
	$sitemapconfig['filename']='../sitemap/sitemap_navigation.xml';
	$sitemapconfig['url']='';
	$sitemapconfig['changefreq']='daily';
	$sitemapconfig['highestpriority']=0.8;

	#INI UNTUK ID YANG TIDAK DIMASUKKAN KE SITEMAP
	//this is just example, please refer to .htaccess and index in on table "content"

	$sql="SELECT * FROM content WHERE contentSitemap='y' ORDER BY contentId";
	$query=query($sql);
	while($data=fetch($query)){
		$content[$data['contentId']]=$data['contentPermalink'];
	}

	foreach($content as $sitemapcontent){
		$sitemapconfig['url'].= "
			<url>
				<loc>".$webprotocol.getconfig('SITE_URL')."/".$sitemapcontent."/</loc>
				<changefreq>".$sitemapconfig['changefreq']."</changefreq>
				<priority>".str_replace(",",".",$sitemapconfig['highestpriority'])."</priority>
			</url>";
	}

	########### IF ANY OTHER TABLE WILL BE SET TO SITEMAP
	$sql="SELECT * FROM newsinfo_cat WHERE catType='1' ORDER BY catId ASC";
	$query=query($sql);
	while($data=fetch($query)){
		$content[$data['catId']]=$data['catPermalink'];
	}
	foreach($content as $sitemapcontent){
		$sitemapconfig['url'].= "
			<url>
				<loc>".$webprotocol.getconfig('SITE_URL')."/berita/".$sitemapcontent."/</loc>
				<changefreq>".$sitemapconfig['changefreq']."</changefreq>
				<priority>".str_replace(",",".",$sitemapconfig['highestpriority'])."</priority>
			</url>";
	}


	//create the sitemap navigation
	createsitemap($sitemapconfig);
	unset( $sitemapconfig);

	#2. Example for article like
	$sitemapconfig=array();
	$sitemapconfig['sitemaptype']='articlelike';
	$sitemapconfig['filename']='../sitemap/sitemap_article.xml';
	$sitemapconfig['url']='';
	$sitemapconfig['changefreq']='daily';
	$sitemapconfig['highestpriority']=0.8;
	$sqllimit=7666;
	$offset=50;
	$sql="SELECT catId, articlePermalink FROM article WHERE articleAddedDate < $now ORDER BY articleAddedDate DESC LIMIT $sqllimit";
	$query=query($sql);
	$no=1;
	while ($data = fetch($query)){
		$category= getval("catPermalink", "article_cat", "catId", $data['catId']);
		$url=$webprotocol.getconfig('SITE_URL').'/artikel/'.$category.'/'.$data['articlePermalink'].'/';
		$data=output($data);
		$sitemapconfig['url'].="
		<url>
			<loc>".$url."</loc>
			<changefreq>".$sitemapconfig['changefreq']."</changefreq>
			<priority>".str_replace(",",".",$sitemapconfig['highestpriority'])."</priority>
		</url>";
		$no++;
		if($no==$offset){
			//decrease the priority 0.5/50 rows. STOP if the priority is 0.5 (lowest priority)
			if($sitemapconfig['highestpriority'] > 0.05){
				$sitemapconfig['highestpriority'] -= 0.05;
			}

			//reset $no
			$no=1;
		}
	}
	//create the sitemap
	createsitemap($sitemapconfig);
	unset( $sitemapconfig);
	
	#2. Example for article like
	$sitemapconfig=array();
	$sitemapconfig['sitemaptype']='articlelike';
	$sitemapconfig['filename']='../sitemap/sitemap_newsinfo.xml';
	$sitemapconfig['url']='';
	$sitemapconfig['changefreq']='daily';
	$sitemapconfig['highestpriority']=0.8;
	$sqllimit=7666;
	$offset=50;
	$sql="SELECT newsPermalink FROM newsinfo WHERE newsAddedDate < $now ORDER BY newsAddedDate DESC LIMIT $sqllimit";
	$query=query($sql);
	$no=1;
	while ($data = fetch($query)){
		$url=$webprotocol.getconfig('SITE_URL').'/berita/baca/'.$data['newsPermalink'].'/';
		$data=output($data);
		$sitemapconfig['url'].="
		<url>
			<loc>".$url."</loc>
			<changefreq>".$sitemapconfig['changefreq']."</changefreq>
			<priority>".str_replace(",",".",$sitemapconfig['highestpriority'])."</priority>
		</url>";
		$no++;
		if($no==$offset){
			//decrease the priority 0.5/50 rows. STOP if the priority is 0.5 (lowest priority)
			if($sitemapconfig['highestpriority'] > 0.05){
				$sitemapconfig['highestpriority'] -= 0.05;
			}

			//reset $no
			$no=1;
		}
	}
	//create the sitemap
	createsitemap($sitemapconfig);
	unset( $sitemapconfig);
	
	#3. Send parent sitemap to google
	//Collect the child sitemap here
	$sitemap_article="../sitemap/sitemap_article.xml";
	$sitemap_newsinfo="../sitemap/sitemap_newsinfo.xml";
	$sitemap_navigation="../sitemap/sitemap_navigation.xml";

	$sitemaptag="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	<sitemapindex xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">
	 <sitemap>
		<loc>".$webprotocol.getconfig('SITE_URL').substr($sitemap_navigation, 2)."</loc>
		<lastmod>".date ("Y-m-d", filemtime(realpath(dirname(__file__).'/..')).substr($sitemap_navigation, 2))."</lastmod>
	 </sitemap>
	 <sitemap>
		<loc>".$webprotocol.getconfig('SITE_URL').substr($sitemap_article, 2)."</loc>
		<lastmod>".date ("Y-m-d", filemtime(realpath(dirname(__file__).'/..')).substr($sitemap_article, 2))."</lastmod>
	 </sitemap>
	 <sitemap>
		<loc>".$webprotocol.getconfig('SITE_URL').substr($sitemap_newsinfo, 2)."</loc>
		<lastmod>".date ("Y-m-d", filemtime(realpath(dirname(__file__).'/..')).substr($sitemap_newsinfo, 2))."</lastmod>
	 </sitemap>
	</sitemapindex>";

	$file=realpath(dirname(__file__).'/..').'/sitemap/sitemapindex.xml';
	@chmod($file, 0777);
	$fp=@fopen($file,"w");
	@fwrite($fp,$sitemaptag);
	@fclose($fp);
	@chmod($file, 0644);

 	$url=urlencode($webprotocol.getconfig('SITE_URL')."/sitemap/sitemapindex.xml");
	$url="www.google.com/webmasters/tools/ping?sitemap=".$url;
	$session = curl_init();
	curl_setopt($session, CURLOPT_URL,$url);
	curl_setopt($session, CURLOPT_HEADER, false);
	curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($session);
	$result=curl_getinfo($session, CURLINFO_HTTP_CODE); //200 mean successfully transfered
	curl_close($session);

	#update activity statistic
	$cron_option['cron_name'] = 'CRON_SITEMAP'; 
	cron_activity_log($cron_option);
?>