<?php
	$page=19;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config-image.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

	#check language license
	if($lang and !in_array($lang, $lc_lang) and ($action=='adden' or $action=='editen')){
		header('location:gallery_album.php');
		exit;
	}

	if($form=="submit"){
		//Is id exist ?
		if($action=="edit" or $action=="del"){
			$exist=countdata("gallery_album","albId='". (int)$id ."'");
			if($exist<1){
				$emptyid=errorlist(29);
			}
		}

		//set the restrictions
		$allowedType = array("image/pjpeg","image/jpeg","image/png","image/gif");
		$realExt = array('jpeg','jpg','png','gif');

		if ($action=="add" and $uac_add){
			$pic=$_FILES['albumcov'];

			if(empty($albname) or empty($pic['name'])){
				$error=errorlist(2);
			}else{
				//check whether permalink exist
				$permalink=permalink($albname);
				$exist=countdata("gallery_album","albPermalink='$permalink'");
				if($exist>0){
					$error=errorlist(24);
				}else{
					//make sure it is allowed
					if(in_array($pic['type'], $allowedType)){
						$fileExt = array_keys($allowedType, $pic['type']);
						$fileExt = $realExt[$fileExt[0]];

						if(is_uploaded_file($pic['tmp_name'])){
							//make sure it is really really image
							$img=getimagesize($pic['tmp_name']);
							if(!in_array($pic['type'],$allowedType) or !(is_numeric($img[0]) or is_numeric($img[1]) or $img[0]>0 or $img[1]>0)){
								//make sure it is allowed
								$error.=errorlist(13);
							}
						}else{
							$error.=errorlist(27);
						}
					} else{
						$error.=errorlist(13);
					}

					//get filename
					$fname = str_replace('.','',$pic['name']).'.'.$fileExt;
					if($showdate){
						list($showD,$showM,$showY)=explode(" ",$showdate);
						$showMs		=convertidtomonth(strtolower(convertmonthtoid($showM)));
						$tshowdate	=mktime(0,0,0,$showMs,$showD,$showY);
					}
					if(!$error){
						//Next id
						$nextid=nextid("albId","gallery_album");

						$img=uploadit($fname,$pic['tmp_name'],"gallery",$nextid,GALLERY_ALBUM_SMALL_WIDTH,GALLERY_ALBUM_SMALL_HEIGHT,GALLERY_ALBUM_MEDIUM_WIDTH,GALLERY_ALBUM_MEDIUM_HEIGHT,GALLERY_ALBUM_LARGE_WIDTH,GALLERY_ALBUM_LARGE_HEIGHT,"album");
						$dir=$img['dir'];
						$imgfilename=$img['filename'];

						//Is it will use watermark ?
						if($watermark=="y"){
							chdir('../../');
							$imagesrc='../assets/gallery/' . $dir . '/'.$imgfilename;
							watermark($imagesrc);
						}

						$sql="INSERT INTO gallery_album VALUES ('$nextid','$albname','$permalink','$tshowdate','$tshowdate','$albdesc','$dir','$imgfilename','$nextid')";
						$query=query($sql);
					}
				}
			}
		}elseif($action=="edit" and $uac_edit){
			if($emptyid){
				$error=$emptyid;
			}else{
				if(empty($albname)){
					$error=errorlist(2);
				}else{
					$pic=$_FILES['albumcov'];

					if(!empty($pic['name'])){
						//make sure it is allowed
						if(in_array($pic['type'], $allowedType)){
							$fileExt = array_keys($allowedType, $pic['type']);
							$fileExt = $realExt[$fileExt[0]];

							if(is_uploaded_file($pic['tmp_name'])){
								//make sure it is really really image
								$img=getimagesize($pic['tmp_name']);
								if(!in_array($pic['type'],$allowedType) or !(is_numeric($img[0]) or is_numeric($img[1]) or $img[0]>0 or $img[1]>0)){
									//make sure it is allowed
									$error.=errorlist(13);
								}
							}else{
								$error.=errorlist(27);
							}
						} else{
							$error.=errorlist(13);
						}

						//get filename
						$fname = str_replace('.','',$pic['name']).'.'.$fileExt;
					}
					if($showdate){
						list($showD,$showM,$showY)=explode(" ",$showdate);
						$showMs		=convertidtomonth(strtolower(convertmonthtoid($showM)));
						$tshowdate	=mktime(0,0,0,$showMs,$showD,$showY);
					}

					if(!$error){
						// check whether permalink exist
						$permalink=permalink($albname);

						//Upload file if exist
						if(!empty($pic['name'])){
							$sql="SELECT * FROM gallery_album WHERE albId='". (int)$id ."'";
							$query=query($sql);
							$data=fetch($query);

							$img         = uploadit($fname,$pic['tmp_name'],"gallery",$id,GALLERY_ALBUM_SMALL_WIDTH,GALLERY_ALBUM_SMALL_HEIGHT,GALLERY_ALBUM_MEDIUM_WIDTH,GALLERY_ALBUM_MEDIUM_HEIGHT,GALLERY_ALBUM_LARGE_WIDTH,GALLERY_ALBUM_LARGE_HEIGHT,"album");
							$dir         = $img['dir'];
							$imgfilename = $img['filename'];
							$sqlfile     = ", albDir='$dir', albFilename='$imgfilename'";

							chdir("../../");

							//delete photo
							@unlink("../assets/gallery/".$data['albDir'].'/'.$data['albFilename']);
							@unlink("../assets/gallery/".$data['albDir'].'/'.genthumb($data['albFilename'],"m"));
							@unlink("../assets/gallery/".$data['albDir'].'/'.genthumb($data['albFilename'],"s"));
							@rmdir ("../assets/gallery/".$data['albDir']);

							//Is it will use watermark ?
							if($watermark=="y"){
								$imagesrc='../assets/gallery/' . $dir . '/'.$imgfilename;
								watermark($imagesrc);
							}
						}

						//update data
						$sql="UPDATE gallery_album SET albAddedDate='$tshowdate', albLastUpdateDate='$tshowdate', albName='$albname', albPermalink='$permalink',albDesc='$albdesc' $sqlfile WHERE albId='". (int)$id ."'";
						$query=query($sql);
					}
				}
			}
		}elseif ($action=="del" and $uac_delete){
			if($emptyid){
				$error=$emptyid;
			}else{
				//Stop delete if album still contain file
				$exist=countdata("gallery_photo","albId='". (int)$id ."'");
				if($exist>0){
					$error=errorlist(25);
				}else{
					$sql="SELECT * FROM gallery_album WHERE albId='". (int)$id ."'";
					$query=query($sql);
					$data=fetch($query);

					//Delete photo
					@unlink("../assets/gallery/".$data['albDir'].'/'.$data['albFilename']);
					@unlink("../assets/gallery/".$data['albDir'].'/'.genthumb($data['albFilename'],"m"));
					@unlink("../assets/gallery/".$data['albDir'].'/'.genthumb($data['albFilename'],"s"));
					@rmdir ("../assets/gallery/".$data['albDir']);

					$sql="DELETE FROM gallery_album WHERE albId='". (int)$id ."'";
					$query=query($sql);

					#remove multilingual features
					$sql="DELETE FROM gallery_album_lang WHERE albId='". (int)$id ."'";
					$query=query($sql);
				}
			}
		}elseif($action=="update"){
			foreach ($s as $id=>$sortnum){
			  $sql="UPDATE gallery_album SET albSort='$sortnum' WHERE albId='$id'";
				$query=query($sql);
			}
		}
		elseif($action=='adden' and $uac_add){
			if(empty($albname)){
				$error=errorlist(2);
			}
			else{
				$sql="INSERT INTO gallery_album_lang VALUES ('$id','$albname','$albdesc','$lang')";
				$query=query($sql);
			}
		}
		elseif($action=='editen' and $uac_edit){
			if(empty($albname)){
				$error=errorlist(2);
			}
			else{
				$sql="UPDATE gallery_album_lang SET albName='$albname', albDesc='$albdesc' WHERE albId='$id' AND lang='$lang'";
				$query=query($sql);
			}
		}

		//check whether query was successful
		if (!$query){
			$error.=errorlist(3);
		}

		if ($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:gallery_album.php");
			}else{
				print "ok";
			}
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS -<?php print _('galleryalbum_pagetitle'); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="/libs/bootstrap-datepicker/css/datepicker3.css" />

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>

<script type="text/javascript" src="/libs/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('#result').hide();

	$('.fancybox').fancybox({
		'arrows': false
	});

	$('.fancybox-frame').fancybox({
		'type'			: 'iframe'
	});

	validate('#result','#add','gallery_album.php');
	validate('#result','#edit','gallery_album.php');

	$(".datepickerdate").datepicker({
		format: 'dd MM yyyy',
		todayHighlight: true,
		toggleActive: true,
		autoclose: true
	});
});
</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('galleryalbum_title'); ?></h3>
			                <p><?php print nl2br(_('galleryalbum_pagedesc')) ?></p>
			            </div>
		            </div>
	        	</div>
        	</div>

        	<?php if ($uac_add){ ?>
        	<div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                	<div class="pull-right">
                	<a href="gallery_album.php?action=add" class="btn btn-primary"><i class="fa fa-plus"></i> <?php print _('cms_addbuttongallery'); ?></a>
                	</div>
                </div>
        	</div>
        	<?php } ?>

        	<div class="row">
	        	<div class="col-md-12">

                	<?php if ($action==""){ ?>
            		<form action="gallery_album.php?form=submit&amp;action=update" method="post" name="sort">
            			<?php if($uac_edit){ ?>
            			<div class="block-break">
            				<button type="submit" name="update" value="<?php print _('galleryalbum_updatelist'); ?>" class="btn btn-sm btn-info"><i class="fa fa-refresh"></i> <?php print _('galleryalbum_updatelist'); ?></button>
            			</div>
            			<?php } ?>
                		<div class="table-responsive">
                			<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
                				<thead>
                					<tr>
										<th width="35"><?php print _('galleryalbum_list'); ?></th>
										<th><?php print _('galleryalbum_albumname'); ?></th>
										<th width="100"><?php print _('galleryalbum_manyphoto'); ?></th>
										<?php if(count($lc_lang)>1){ ?><th style="width:40px;" class="text-center"><i rel="tooltip" title="<?php echo _('multi_language_view_language'); ?>" class="fa fa-language"></i></th><?php } ?>
										<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
										<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
										<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
										<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
									</tr>
                				</thead>
                				<tbody>
                					<?php
									if(empty($pg)){
										$pg=1;
									}

									$numPerPage=20;
									$offset=($pg-1)*$numPerPage;
									$num=1;
									$num=$num+(($pg-1)*$numPerPage);
									$sql="SELECT * FROM gallery_album ORDER BY albSort ASC";
									$query=query($sql);
									$numofdata=rows($query);
									$sql.=' LIMIT '.$offset.','.$numPerPage;
									$query=query($sql);
									if($numofdata<1){
									?>
									<tr>
										<td colspan="8" class="text-center"><div class="text-center"><br /><?php print _('galleryalbum_albumnotavailable'); ?><br /><br /></div></td>
									</tr>
									<?php
									} else {
										while ($data=fetch($query)){
										$imagesum=countdata("gallery_photo","albId='$data[albId]'");
										?>
										<tr>
											<td class="text-center"><?php if($uac_edit){ ?><input type="text" size="3" name="s[<?php print $data['albId']; ?>]" value="<?php print $data['albSort']; ?>" class="text-center form-control"><?php }else{ print $num;} ?></td>
											<td>
												<strong><?php print $data['albName']; ?></strong><br/>
											</td>
											<td class="text-center"><?php print $imagesum; ?></td>
											<?php
											if(count($lc_lang)>1){
												?>
												<td class="text-center">
													<?php
													foreach($lc_lang as $key=>$value){
														$exist=countdata("gallery_album_lang","albId='".$data['albId']."' and lang='$value'");
														if($value==$lc_lang_default) continue;
														if($exist and $uac_edit){
															?><a href="gallery_album.php?action=editen&amp;lang=<?php print $value;?>&amp;id=<?php print $data['albId'] ?>" rel="tooltip" title="<?php print _('galleryalbum_editvers'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-edit.png" alt="<?php print $value;?>"  border="0" /></a><?php
														}
														elseif($uac_add){
															?><a href="gallery_album.php?action=adden&amp;lang=<?php print $value;?>&amp;id=<?php print $data['albId']; ?>" rel="tooltip" title="<?php print _('galleryalbum_addvers'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-add.png" alt="<?php print $value;?>" border="0" /></a><?php
														}
													}?>
												</td>
												<?php
											} ?>
											<td class="text-center">
												<a href="/assets/gallery/<?php print $data['albDir']; ?>/<?php print $data['albFilename']; ?>" rel="tooltip" title="Cover: <?php print stripquote($data['albName']); ?>" class="fancybox"><i class="fa fa-image"></i></a>
											</td>
											<td class="text-center">
												<a href="gallery.php?albid=<?php print $data['albId'];?>" rel="tooltip" title="<?php print _('galleryalbum_seephoto'); ?>: <?php print stripquote($data['albName']); ?>"><i class="fa fa-folder-open-o"></i></a>
											</td>
											<td class="text-center">
												<?php if ($uac_edit){ ?><a href="gallery_album.php?action=edit&amp;id=<?php print $data['albId']; ?>" title="<?php print _('cms_edit'); ?>: <?php print stripquote($data['albName']); ?>" rel="tooltip"><i class="fa fa-edit"></i></a><?php } ?>
											</td>
											<td class="text-center">
												<?php if ($uac_delete){ ?><a class="delete" href="gallery_album.php?form=submit&action=del&id=<?php print $data['albId']; ?>" title="<?php print stripquote($data['albName']); ?>"><i title="<?php print _('cms_delete'); ?>: <?php print stripquote($data['albName']); ?>" rel="tooltip" class="fa fa-trash-o"></i></a><?php } ?>
											</td>
										</tr>
										<?php
										$num++;
										}
									}
									?>
                				</tbody>
                			</table>
                		</div>
                		<?php
							$options['total']=$numofdata;
							$options['filename']='gallery_album.php';
							$options['qualifier']='gallery_album.php';
							$options['pg']=$pg;
							$options['numPerPage']=$numPerPage;
							$options['style']=1;
							$options['addquery']=TRUE;
							paging($options);
						?>
            		</form>
            		<?php } elseif ($action=="add" and $uac_add){ ?>
            		<a href="gallery_album.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('galleryalbum_mainaddalbum') ?></h4>
					<hr/>

					<form action="gallery_album.php?form=submit&amp;action=add" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">
						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _('galleryalbum_albumname'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="albname" type="text" id="albname" size="40" required class="form-control col-md-7 col-xs-12">
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="showdate"><?php print _('article_date'); ?>
	                        </label>
	                        <div class="col-md-3 col-sm-6 col-xs-12">
	                        	<input name="showdate" id="showdate" type="text" class="datepickerdate form-control has-feedback-left dob showdate">
	                        	<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _('galleryalbum_file'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input type="file" size="40" name="albumcov" id="albumcov" required>
	                        	<span class="help-block"><?php print _('galleryalbum_file_info'); ?> <?php print GALLERY_ALBUM_LARGE_WIDTH . "x" . GALLERY_ALBUM_LARGE_HEIGHT; ?> pixel</span>
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _('galleryalbum_description'); ?>
	                        </label>
	                        <div class="col-md-9 col-sm-9 col-xs-12">
	                        	<textarea name="albdesc" type="text" id="albdesc" cols="60" rows="4" class="tinymce"></textarea>
	                        	<a class="btn btn-default btn-xs fancybox-frame" href="imgfly.php"><i class="fa fa-upload"></i> <?php print _('pagecontent_uploadimg'); ?></a><br/><br/>
	                        </div>
                    	</div>
                    	
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="watermark">Watermark 
	                        </label>
	                        <div class="checkbox col-md-6 col-sm-6 col-xs-12">
                        		<label>
                        			<input name="watermark" id="watermark" value="y" type="checkbox"> Ya
                        		</label>
	                        </div>
                    	</div>
						
                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='gallery_album.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>
					</form>
					<?php
					}elseif ($action=="edit" and $uac_edit){
						$sql="SELECT * FROM gallery_album WHERE albId='". (int)$id ."'";
						$query=query($sql);
						$data=formoutput($data=fetch($query));
						if($data['albAddedDate']){
								$starttime	=date('d F Y',$data['albAddedDate']);
						}
					?>
					<a href="gallery_album.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('galleryalbum_editalbum') ?></h4>
					<hr/>

					<form action="gallery_album.php?form=submit&amp;action=edit&amp;id=<?php print $id; ?>" method="post" enctype="multipart/form-data" name="edit" id="edit" class="form-horizontal form-label-left">

						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _('galleryalbum_albumname'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="albname" type="text" id="albname" size="40" value="<?php print $data['albName']; ?>" required class="form-control col-md-7 col-xs-12" />
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="hidedate"><?php print _('article_date'); ?>
	                        </label>
	                        <div class="col-md-3 col-sm-6 col-xs-12">
		                        	<input name="showdate" id="showdate" type="text" size="30" class="datepickerdate form-control has-feedback-left dob showdate" value="<?php print $starttime ?>">
		                        	<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
		                        	<span class="help-block"><?php print _('changeban_startdatedisplay_info'); ?></span>
		                    </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _('galleryalbum_file'); ?>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<img src="/assets/gallery/<?php print $data['albDir']; ?>/<?php print genthumb($data['albFilename'],"m"); ?>" alt="<?php print stripquote($data['albFilename'],"m"); ?>" title="<?php print stripquote($data['albName'],"m"); ?>"><br /><br />
								<input type="file" size="40" name="albumcov" id="albumcov" />
								<span class="help-block"><?php print _('galleryalbum_file_info'); ?> <?php print GALLERY_ALBUM_LARGE_WIDTH . "x" . GALLERY_ALBUM_LARGE_HEIGHT; ?> pixel</span>
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _('galleryalbum_description'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<textarea name="albdesc" type="text" id="albdesc" cols="60" rows="4" class="tinymce"><?php print $data['albDesc']; ?></textarea>
	                        </div>
                    	</div>
                    	
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="watermark">Watermark 
	                        </label>
	                        <div class="checkbox col-md-6 col-sm-6 col-xs-12">
                        		<label>
                        			<input name="watermark" id="watermark" value="y" type="checkbox"> Ya
                        		</label>
	                        </div>
                    	</div>
						
                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_updatebutton'); ?>"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='gallery_album.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>

                    </form>
                    <?php } elseif ($action=="adden" and $uac_add){ ?>
                    <a href="gallery_album.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('galleryalbum_addalbumvers'); ?><?php print localename($lang);?> &raquo; <?php print getval('albName','gallery_album','albId',$id); ?></h4>
					<hr/>

					<form action="gallery_album.php?form=submit&amp;action=adden&amp;lang=<?php print $lang;?>&amp;id=<?php print $id;?>" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">

						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="albname"><?php print _('galleryalbum_albumname'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="albname" type="text" id="albname" size="40" required class="form-control col-md-7 col-xs-12">
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="albdesc"><?php print _('galleryalbum_description'); ?>
	                        </label>
	                        <div class="col-md-9 col-sm-9 col-xs-12">
	                        	<textarea name="albdesc" type="text" id="albdesc" cols="60" rows="4" class="tinymce"></textarea>
	                        </div>
                    	</div>

                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='gallery_album.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>

					</form>
					<?php
						} elseif ($action=="editen" and $uac_edit){
							$sql="SELECT * FROM gallery_album_lang WHERE albId='". (int)$id ."' AND lang='$lang'";
							$query=query($sql);
							$data=formoutput($data=fetch($query));
					?>
					<a href="gallery_album.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('galleryalbum_editalbumvers'); ?> <?php print localename($lang);?> &raquo; <?php print getval('albName','gallery_album','albId',$id); ?></h4>
					<hr/>

					<form action="gallery_album.php?form=submit&amp;action=editen&amp;lang=<?php print $lang;?>&amp;id=<?php print $id; ?>" method="post" enctype="multipart/form-data" name="edit" id="edit" class="form-horizontal form-label-left">

						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="albname"><?php print _('galleryalbum_albumname'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="albname" type="text" id="albname" size="40" value="<?php print $data['albName']; ?>" required class="form-control col-md-7 col-xs-12">
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="albdesc"><?php print _('galleryalbum_description'); ?>
	                        </label>
	                        <div class="col-md-9 col-sm-9 col-xs-12">
	                        	<textarea name="albdesc" type="text" id="albdesc" cols="60" rows="4" class="tinymce"><?php print $data['albDesc']; ?></textarea>
	                        </div>
                    	</div>

                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_updatebutton'); ?>"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='gallery_album.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>

					</form>
                	<?php } ?>

		        </div>
		    </div>


		</div>
        <!-- END THE CONTENT OF PAGE HERE -->


		<?php include("com/com-footer.php"); ?>

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>