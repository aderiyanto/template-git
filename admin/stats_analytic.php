<?php
	$page=26;
	
	header("location:https://accounts.google.com/ServiceLogin?service=analytics&passive=true&nui=1&hl=id&continue=https%3A%2F%2Fwww.google.com%2Fanalytics%2Fweb%2F%3Fhl%3Did&followup=https%3A%2F%2Fwww.google.com%2Fanalytics%2Fweb%2F%3Fhl%3Did");
	//includes all files necessary to support operations
	/*include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");
	include("../modz/google/gapi.class.php");
	
	header("location:https://www.google.com/analytics/");*/
?>
<!--
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<title><?php print SITE_NAME; ?> - CMS - <?php print _('stats_analytic_pagetitle'); ?></title>
<link rel="stylesheet" type="text/css" href="../style/styleadmin.css" />
<link rel="stylesheet" type="text/css" href="../style/facebox.css" />
<link rel="stylesheet" type="text/css" href="../style/jquery-ui.css" />
<script language="javascript" type="text/javascript" src="../javascript/commonjs.js"></script>
<script language="javascript" type="text/javascript" src="../javascript/jquery.js"></script>
<script language="javascript" type="text/javascript" src="../javascript/jquery-ui.js"></script>
<script language="javascript" type="text/javascript" src="../javascript/jquery-timepicker.js"></script>
<script language="javascript" type="text/javascript" src="../javascript/jquery.validate.js"></script>
<script language="javascript" type="text/javascript" src="../javascript/jquery.form.js"></script>
<script language="javascript" type="text/javascript" src="../javascript/facebox.js"></script>
<script language="javascript" type="text/javascript" src="../javascript/validate.js"></script>
<script language="javascript" type="text/javascript" src="../javascript/ddaccordion.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript"> 
jQuery(document).ready(function($){
	$('a[rel*=facebox]').facebox();
	$('#result').hide();
	validate('#result','#add','stats_analytic.php');

	$(".datetimepicker").datetimepicker({
		changeYear: true,
		showOn: "both",
		buttonImage: "../assets/images/calendar.png",
		buttonImageOnly: true,
		dayNamesMin: ['M', 'S', 'S', 'R', 'K', 'J', 'S'],
		monthNames: ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'],
		dateFormat: 'dd/mm/yy',
	});	

	$("#dob1").datepicker({ 
		dateFormat: "dd/mm/yy",
		closeAtTop: false, 
		yearRange: "<?php print $starty; ?>:<?php print $endy; ?>",
		showOn: "both", 
		buttonImage: "/assets/images/calendar.png", 
		buttonImageOnly: true 
	});
	
	$("#dob2").datepicker({ 
		dateFormat: "dd/mm/yy",
		closeAtTop: false, 
		yearRange: "<?php print $starty; ?>:<?php print $endy; ?>",
		showOn: "both", 
		buttonImage: "/assets/images/calendar.png", 
		buttonImageOnly: true 
	});
});
<?php
	//   1, = Start Index
	//   500 = Max results
	if(empty($startdate)){
		$startdate=date("d/m/Y", strtotime(date('m').'/01/'.date('Y').' 00:00:00'));
	}else{
		$startdate=$startdate;
	}
  
	if(empty($enddate)){
		$enddate=date("d/m/Y", strtotime('-1 second',strtotime('+1 month',strtotime(date('m').'/01/'.date('Y').' 00:00:00'))));
	}else{
		$enddate=$enddate;
	}
?>  
 function loadData(id,startdate,enddate){
	id=id || '';
	id = typeof(id) !== 'undefined' ? id : 'city';
 
	startdate=startdate || '';
	startdate = typeof(startdate) !== 'undefined' ? startdate : '<?php print $startdate; ?>';
	
	enddate=enddate || '';
	enddate = typeof(enddate) !== 'undefined' ? enddate : '<?php print $enddate; ?>';
	
	if(startdate==''){
		var startdate='<?php print $startdate; ?>';
	}else{
		var startdate=startdate;
	}
	
	if(enddate==''){
		var enddate='<?php print $enddate; ?>';
	}else{
		var enddate=enddate;
	}
	
	if(id=='city'){
		var id=id || '';
	}else{
		var id=$('#queryoption').val();
	}
	
	
	$('#querytable').hide();
	$(".more").show();
	$(".more").html('<img src="../assets/images/loading.gif" />');
	
	$.ajax({
		type: "get",
		url: "stats_analytic_table.php",
		data: "action=showquery&id="+id+"&startdate="+startdate+"&enddate="+enddate,
		success: function(data){
			$('#querytable').show();
			$('#querycontainer').html(data);
			$(".more").hide();
		}
	});
 }
</script>

</head>
<body>
<div id="resultedit"></div>
<div id="result"></div>
<?php include("com/com-menu.php"); ?>
<div id="contentbox">
	<?php include("com/com-greet.php"); ?>
    <div id="content">
    	<span class="subheading"><?php print _('stats_analytic_pagetitle'); ?></span>
		<br />
    	<br />
			<?php print _('stats_analytic_pagedesc'); ?>.
		<br />
    	<br />
		<div style="text-align:right">
			<form name="search" action="stats_analytic.php" method="get">
				<input name="startdate" id="dob1" type="text" size="12" maxlength="12" value="<?php print $startdate;  ?>">
				<input name="enddate" id="dob2" type="text" size="12" maxlength="12"   value="<?php print $enddate; ?>">
				<input type="submit" value="<?php print _('stats_analytic_show'); ?>" name="showga" id="showga" class="button" >
			</form>
		</div>
		 <?php
			$startdateajax=$startdate;
			$enddateajax=$enddate;
			list($day,$month,$year)=explode("/", $startdate);		
			$startdate=$year.'-'.$month.'-'.$day;
			
			list($day,$month,$year)=explode("/", $enddate);	
			$enddate=$year.'-'.$month.'-'.$day;
				
			$ga = new gapi(getconfig('GA_EMAIL'),getconfig('GA_PASSWORD')); 
			$ga->requestReportData(getconfig('GA_PROFILE_ID'),array('browser','browserVersion','operatingSystem','country','city','referralPath','visitorType'),array('visitors','uniquePageviews','pageviews','visits','avgTimeOnPage','avgTimeOnSite','entranceBounceRate','newVisits','percentNewVisits'),'-visits','',$startdate,$enddate,1,500);
			$allvisitor=$ga->getVisits();
 
		?> 
	<div id="ga-top">
		<div class="secheading"><?php print _('stats_analytic_simplestatistic'); ?></div>
		<table width="400" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="200"><strong><?php print _('stats_analytic_totalvisit'); ?></strong></td>
				<td width="1">:</td>
				<td><?php print $allvisitor; ?> <?php print _('stats_analytic_visit'); ?></td>
			</tr>
			<tr>
				<td width="200"><strong><?php print _('stats_analytic_totalvisitor'); ?></strong></td>
				<td width="1">:</td>
				<td><?php print $ga->getVisitors(); ?> <?php print _('stats_analytic_visitor'); ?></td>
			</tr>
			<tr>
				<td><strong><?php print _('stats_analytic_unique_page_views'); ?></strong></td>
				<td>:</td>
				<td><?php print $ga->getUniquePageviews(); ?> <?php print _('stats_analytic_times'); ?></td>
			</tr>
			<tr>
				<td><strong><?php print _('stats_analytic_pageviews'); ?></strong></td>
				<td>:</td>
				<td><?php print $ga->getPageviews(); ?> <?php print _('stats_analytic_times'); ?></td>
			</tr>
			<tr>
				<td><strong><?php print _('stats_analytic_info'); ?></strong></td>
				<td>:</td>
				<td>
				<?php 
					if($allvisitor>0){
						$duration='';
						$hours = floor($ga->getAvgTimeOnSite() / 3600);
						$minutes = floor($ga->getAvgTimeOnSite() / 60);
						$seconds = $ga->getAvgTimeOnSite() - $minutes * 60;

						if($hours > 0) {
						$duration .= ' ' . $hours . ' jam';
						}
						
						if($minutes > 0) {
							$duration .= ' ' . $minutes . ' menit';
						}
						
						if($seconds > 0) {
							$duration .= ' ' . round($seconds,2) . ' detik';
						}
						print $duration;
					}else{
						print "-";
					}
				?></td>
			</tr>
			<tr>
				<td><strong><?php print _('stats_analytic_visitglance'); ?></strong></td>
				<td>:</td>
				<td><?php print round($ga->getEntranceBounceRate(),2); ?>%</td>
			</tr>
			<tr>
				<td><strong><?php print _('stats_analytic_newvisit'); ?></strong></td>
				<td>:</td>
				<td><?php print $ga->getNewVisits(); ?>&nbsp;kunjungan&nbsp;( <?php print round($ga->getPercentNewVisits(),2); ?>% )</td>
			</tr>
			<tr>
				<td><strong><?php print _('stats_analytic_returnvisit'); ?></strong></td> 
				<td>:</td>
				<td>
				<?php
					print $ga->getVisits()-$ga->getNewVisits();
				?> <?php print _('stats_analytic_visit'); ?>
				<?php	if($ga->getVisits()-$ga->getNewVisits()>0){?>
				( <?php print abs(round($ga->getPercentNewVisits()-100,2)); ?>% )</td>
				
				<?php
					}else{ print '( 0% )';}
				?>
			</tr>
		</table>
	</div>
	<div id="chart"></div>
	<div id="ga-bottom">
		<br /><br />
		<select name="queryoption" id="queryoption" >
			<option value="city"><?php print _('stats_analytic_city'); ?></option>
			<option value="country"><?php print _('stats_analytic_country'); ?></option>
			<option value="browser"><?php print _('stats_analytic_web_browser'); ?></option>
			<option value="os"><?php print _('stats_analytic_systemoperation'); ?></option>
 		</select>
		<input type="button" name="showoption" id="showoption" value="<?php print _('stats_analytic_show'); ?>" class="button" onClick="loadData()">
		<br /><br />
		<div  style="text-align:center" class="more"></div>
		<div id="querycontainer">
			<?php
				if(!$showquery){
			?>
				<script type="text/javascript">
					loadData('city','<?php print $startdateajax; ?>','<?php print $enddateajax; ?>'); 
				</script>
			<?php		
				}
			?>
		</div>
	</div>	
    </div>
</div>
</body>
</html>-->