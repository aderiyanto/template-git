<?php
	$page=18;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/mainmod-extend.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

	if($form=="submit"){
		if($action=="edit" and $uac_edit){
			#Check whether id exist
			if(empty($emailsubject) or empty($emailmsg)){
				$error=errorlist(2);
			} else {
				#check whether email exist
				$isexist = countdata("email_queue","emailMsg='$emailmsg' AND emailTo='$emailto' AND  emailId!='". (int)$id ."'");
				if($isexist>0){
					$error=errorlist(21);
				}else{
					$m=$_FILES['emailattach'];

					//upload file
					if (!empty ($m)){
						$sql = "SELECT emailAttachFile, emailAttachDir FROM email_queue WHERE emailId='".$id."'";
						$query = query($sql);
						$data = fetch($query);
						$xfile = $data['emailAttachFile'];
						$xdir = $data['emailAttachDir'];

						$fname = $m['name'];
						$ftemp = $m['tmp_name'];
						
						$ext = strtolower(end(explode('.', $fname)));
						$allowedExt = array('jpg', 'jpeg', 'png', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'zip', 'pdf');

						if(in_array($ext, $allowedExt)){
							$nowdir = date('dmY');
							$uploaddir = '../assets/emailqueue/'.$nowdir.'/';
							
							#est whether targetdir exists
							if(!opendir($uploaddir)){
								@mkdir($uploaddir);
							}
							
							#make it unique
							if(file_exists($uploaddir.'/'.$fname)){
								$fname = $fname.'_'.strtolower(codegen(4)).'.'.$ext;
							}
							
							#delete current file
							@unlink("../assets/emailqueue/".$xdir."/".$xfile);
							
							#copy new file
							$copy = copy($ftemp,$uploaddir.$fname);

							if (!$copy){
								$error.=errorlist(13);
							}

							$sqlban = ",emailAttachDir='$nowdir', emailAttachFile='$fname', emailAttachType='$ext'";
						}
					}
					
					#implode head format
					$emailhead = "$from#$fromname#$replyto#$cc#$bcc";

					$sql = "UPDATE email_queue SET emailSubject='$emailsubject',emailMsg='$emailmsg',emailHead='$emailhead' $sqlban  WHERE emailId='". (int)$id ."'";
					$query = query($sql);
				}
			}
		}elseif($action=="del" and $uac_delete){
			if($id == 'all'){
				$sql = "SELECT emailId, emailAttachDir, emailAttachFile FROM email_queue";
				$query = query($sql);
			} else {
				$sql = "SELECT emailId, emailAttachDir, emailAttachFile FROM email_queue WHERE emailId='" . $id . "'";
				$query = query($sql);
			}
			
			while($data = fetch($query)){
				if($data['emailAttachFile']){
					@unlink("../assets/emailqueue/".$data['emailAttachDir']."/".$data['emailAttachFile']);
					@rmdir("../assets/emailqueue/".$data['emailAttachDir']);
				}
				
				$sqlx = "DELETE FROM email_queue WHERE emailId='{$data['emailId']}'";
				$queryx = query($sqlx);
			}
		}elseif($action=="queueshow" and $uac_edit){
			$sql="UPDATE email_queue SET emailStatus='y' WHERE emailId='". (int)$id ."'";
			$query=query($sql);
		}elseif($action=="queuehide" and $uac_edit){
			$sql="UPDATE email_queue SET emailStatus='n' WHERE emailId='". (int)$id ."'";
			$query=query($sql);
		}

		//check whether query was successful
		if(!$query){
			$error=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:email_queue.php");
			}else{
				print "ok";
			}
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _("email_queue_pagetitle"); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">


<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce_simple.js"></script>
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('.fancybox').fancybox({
		'arrows': false
	});
	$('.fancybox-frame').fancybox({
		'type'			: 'iframe'
	});
	$('#result').hide();
	validate('#result','#edit','email_queue.php');
});
</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('email_queue_pagetitle'); ?></h3>
			                <p><?php print _('email_queue_pagedesc'); ?></p>
			            </div>
		            </div>
	        	</div>
        	</div>

        	<?php
			$countqueue=countdata("email_queue","emailId");
			$sent=countdata("email_queue","emailStatus='y'");
			$pending=countdata("email_queue","emailStatus='n'");
			if ($uac_delete and ($countqueue>0)){ ?>
        	<div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                	<div class="pull-right">
                	<a href="javascript:if (window.confirm('Apakah anda yakin ingin menghapus seluruh email?')){ window.location='email_queue.php?form=submit&amp;action=del&amp;id=all'; };" class="btn btn-primary"><i class="fa fa-close"></i> <?php print _('email_queue_hapussemuaemail'); ?></a>
                	</div>
                </div>
        	</div>
        	<?php } ?>

        	<div class="row">
	        	<div class="col-md-12">
	        	<?php
				if (empty($action)){
					print "<div class=\"block-break\">";
					print "<strong>" . _('email_queue_totalemail') . "</strong> " . number_format($countqueue,0,",",".") . " | ";
					print "<strong>" . _('email_queue_emailterkirim') . "</strong> " . number_format($sent,0,",",".") . " | ";
					print "<strong>" . _('email_queue_emailpending') . "</strong> " . number_format($pending,0,",",".");
					print "</div>";
				?>
					<div class="table-responsive" style="margin-top:12px;">
						<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
            				<thead>
            					<tr>
            						<th width="25"><?php print _("email_queue_no"); ?></th>
							        <th><?php print _("email_queue_email"); ?></th>
							        <th><?php print _("email_queue_title"); ?></th>
							        <th width="70"><?php print _("email_queue_status"); ?></th>
							        <th><?php print _("email_queue_sender"); ?></th>
							        <th width="220"><?php print _("email_queue_time"); ?></th>
									<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
									<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
									<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
									<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
									<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
            					</tr>
            				</thead>
            				<tbody>
            					<?php
									if (empty($pg)){
										$pg=1;
									}
									$sql="SELECT * FROM email_queue ORDER BY emailStatus DESC, emailId DESC";
									$query=query($sql);
								 	$numofdata=rows($query);

									$num=1;
									$numPerPage=20;
									$offset=($pg-1)*$numPerPage;
									$num=$num+(($pg-1)*$numPerPage);

								 	$sql.=' LIMIT '.$offset.','.$numPerPage;
									$query=query($sql);
									if ($numofdata<1){
								?>
								<tr>
							  		<td colspan="11" class="asterik" class="text-center"><div class="text-center"><br /><?php print _("email_queue_notavailable"); ?><br /><br /></div></td>
								</tr>
								<?php
									}else{
										while($data=fetch($query)){
										list($from, $fromname, $replyto, $cc, $bcc) = explode("#", $data['emailHead']);
										if ($data['emailStatus']=="n"){
											$btnDisplay="<a href=\"email_queue.php?form=submit&amp;action=queueshow&amp;id=" .$data['emailId'] ."\" rel=\"tooltip\" title=\"Tunda: " .stripquote($data['emailTo']) ."\"><i class=\"fa fa-check-circle\" style=\"color:#00cc00;\"></i></a>";
										}else{
											$btnDisplay="<a href=\"email_queue.php?form=submit&amp;action=queuehide&amp;id=" .$data['emailId'] ."\" rel=\"tooltip\" title=\"Aktifkan: " .stripquote($data['emailTo']) ."\"><i class=\"fa fa-minus-circle\" style=\"color:#cc0000;\"></i></a>";
										}
								?>
						    	<tr>
							        <td class="text-center"><?php print $num; ?></td>
							        <td><?php print $data['emailTo']; ?></td>
							        <td><?php print $data['emailSubject']; ?></td>
							        <td class="text-center"><?php if ($data['emailStatus']=="n"){ print "<strong style=\"color: orange\">Pending</strong>"; }else{ print "<strong style=\"color: green\">Sent</strong>"; } ?></td>
							        <td><?php print $from.($fromname ? ' &lt'.$fromname.'&gt':''); ?></td>
									<td class="text-center"><?php print dateformat($data['emailDate'],"full"); ?></td>
							        <td class="text-center"><?php if($data['emailAttachFile']){?><a href="../assets/emailqueue/<?php print $data['emailAttachDir'].'/'.$data['emailAttachFile'];?>" target="blank"><img src="../assets/images/icon-attachment.png" alt="<?php print $data['emailAttachType'];?>"/></a><?php }?></td>
									<td class="text-center"><a href="email_readmore.php?action=readmore&amp;id=<?php print $data['emailId']; ?>" class="fancybox-frame"><i class="fa fa-list-alt"></i></a></td>
							        <td class="text-center"><?php if($uac_edit){ print $btnDisplay; } ?></td>
							        <td class="text-center"><?php if ($uac_edit and $data['emailStatus']=="n"){ ?><a href="email_queue.php?action=edit&amp;id=<?php print $data['emailId']; ?>" title="<?php print _('cms_edit'); ?>: <?php print stripquote($data['emailTo']); ?>" rel="tooltip"><i class="fa fa-edit"></i></a><?php } ?></td>
							        <td class="text-center"><?php if ($uac_delete){ ?><a class="delete" href="email_queue.php?form=submit&amp;action=del&amp;id=<?php print $data['emailId']; ?>" title="<?php print stripquote($data['emailTo']); ?>"><i rel="tooltip" title="<?php print _("cms_delete"); ?>: <?php print stripquote($data['emailTo']); ?>" class="fa fa-trash-o"></i></a><?php } ?></td>
						    	</tr>
								<?php
							  			$num++;
							  		}
								}
								?>
            				</tbody>
            			</table>
            		</div>
            		<?php
						$options['total']=$numofdata;
						$options['filename']='email_queue.php';
						$options['qualifier']='email_queue.php';
						$options['pg']=$pg;
						$options['numPerPage']=$numPerPage;
						$options['style']=1;
						$options['addquery']=TRUE;
						paging($options);
					?>
				<?php
				} elseif ($action=="edit" and $uac_edit){
					$sql="SELECT * FROM email_queue WHERE emailId='".(int)$id."'";
					$query=query($sql);
					$data=formoutput(fetch($query));
					list($from, $fromname, $replyto, $cc, $bcc) = explode("#", $data['emailHead']);
				?>
					<a href="email_queue.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _("email_queue_editemail"); ?></h4>
					<hr/>
					<form action="email_queue.php?form=submit&amp;action=edit&amp;id=<?php print $data['emailId']; ?>" method="post" enctype="multipart/form-data" name="edit" id="edit" class="form-horizontal form-label-left">
						<input type="hidden" name="from" value="<?php print $from;?>" />
						<input type="hidden" name="fromname" value="<?php print $fromname;?>" />

						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="datetime"><?php print _('email_queue_receiver'); ?>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<p class="form-control-static"><?php print $data['emailTo']; ?></p>
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="datetime"><?php print _('email_queue_replayto'); ?>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="replyto" type="text" id="replyto" size="40" maxlength="255" value="<?php print $replyto; ?>" class="form-control" />
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="datetime"><?php print _('email_queue_cc'); ?>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="cc" type="text" id="cc" size="40" maxlength="255" value="<?php print $cc; ?>" class="form-control">
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="datetime"><?php print _('email_queue_bcc'); ?>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="bcc" type="text" id="bcc" size="40" maxlength="255" value="<?php print $bcc; ?>" class="form-control">
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="datetime"><?php print _('email_queue_judulsubjek'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="emailsubject" type="text" id="emailsubject" size="40" maxlength="255" required value="<?php print $data['emailSubject']; ?>" class="form-control">
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="datetime"><?php print _('email_queue_isi'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<textarea name="emailmsg" id="emailmsg" cols="50" rows="5" class="form-control<?php print $data['emailMsgType'] == 'html' ? ' tinymce':''; ?>" required><?php print $data['emailMsg']; ?></textarea>
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="datetime"><?php print _('email_queue_attachment'); ?>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<?php
									if (!empty($data['emailAttachFile'])){
								?>
									<a href="/assets/emailqueue/<?php print $data['emailAttachDir'];?>/<?php print $data['emailAttachFile'];?>" target="_blank"><i class="fa fa-paperclip"></i> <?php print $data['emailAttachFile'];?></a><br/><br/>
								<?php
									}
								?>
								<input name="emailattach" type="file" id="emailattach"  size="40"  />
	                        </div>
                    	</div>

                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_updatebutton'); ?>"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='email_queue.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>

                    	<br/><br/><br/>
					</form>
				<?php } elseif ($action=="add"){ ?>
					<a href="email_queue.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4>Kirim Email</h4>
					<hr/>
					<form action="email_queue.php?form=submit&amp;action=add" method="post" enctype="multipart/form-data" name="edit" id="edit" class="form-horizontal form-label-left">
						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="datetime">Email <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="emailto" type="text" id="emailto" size="40" maxlength="255" class="form-control" required>
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="datetime">Judul <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="emailsubject" type="text" id="emailsubject" size="40" maxlength="255" class="form-control" required>
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="datetime">Header <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<textarea name="emailhead" id="emailhead" cols="50" rows="5" class="form-control" required></textarea>
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="datetime">Isi <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<textarea name="emailmsghtml" id="emailmsghtml" cols="50" rows="5" class="required tinymce"><?php print $data['emailMsg']; ?></textarea>
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="datetime">Attachment
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="emailattach" type="file" id="emailattach" size="40" />
	                        </div>
                    	</div>

                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='email_queue.php'"><i class="fa fa-close"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>
                    	<br/><br/><br/>
					</form>
				<?php } ?>
	        	</div>
	        </div>


		</div>
        <!-- END THE CONTENT OF PAGE HERE -->


		<?php include("com/com-footer.php"); ?>

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>