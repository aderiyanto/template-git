<?php
	//includes all files necessary to support operations
	include($argv[1]."/modz/config-main.php");
	include($argv[1]."/modz/config.php");
	include($argv[1]."/modz/license.php");
	include($argv[1]."/modz/errormsg.php");
	include($argv[1]."/modz/mainmod.php");
	include($argv[1]."/modz/connic.php");
	include($argv[1]."/modz/getall-admin.php");
	
	$fid = $argv[2];
	
	$sqlstatus = "UPDATE newsletter SET nStatus='y',nSending='y' WHERE nId='$fid'";
	$statusquery = query($sqlstatus);
	
	#Prepare var use to count sent email
	$sql = "SELECT * FROM newsletter_tmp JOIN newsletter JOIN newsletter_subscriber ";
	$sql .= "ON newsletter_tmp.nId=newsletter.nId AND newsletter_tmp.subId=newsletter_subscriber.subId ";
	$sql .= "WHERE newsletter.nId='".$fid."' AND newsletter_subscriber.subActive='y'";
	$query = query($sql);
	$rows = rows($query);
	
	$countemailsend = 1;
	while($data=fetch($query)){
		$emailsubject	=$data['nSubject'];
		$nmainimg		='/assets/newsletter/'.$data['nMainImg'];
		$newsdate		=$data['nRegDate'];
		$emailcontent	=$data['nEmailMsg'];
		$emailto		=$data['subEmail'];
		$nid			=$data['nId'];
		$subid			=$data['subId'];
		$tmpsign		="{$subid}.{$nid}";
		$subscribe = sprintf(_('newsletter_footer'),SITE_NAME);
		
		$content='<table align="center" bgcolor="#f6f3e4" width="100%">
					<tr>
						<td>
							<table width="600"  bgcolor="#ffffff" cellpadding="10" align="center"> 
								<tr>
									<td align="center"><img src="http://'.getconfig('SITE_URL').'/assets/images/newsletter_header.jpg" width="600"></td>
								</tr>
								<tr>
									<td align="center"><img src="http://'.getconfig('SITE_URL').$nmainimg.'" width="600"></td>
								</tr>
								<tr>
									<td align="center"  style="font-family:Georgia, Times, serif;font-size:24px;font-weight:bolder;">'.$emailsubject.'</td>
								</tr>
								<tr>
									<td style="text-align:center;padding-bottom:5px"><img src="http://'.getconfig('SITE_URL').'/assets/images/divider.gif" align="center" width="600"></td>
								</tr>
								<tr>
									<td >'.$emailcontent.'</td>
								</tr>
								<tr>
									<td align="center"><img src="http://'.getconfig('SITE_URL').'/assets/images/hr-newsletter.gif" height="11" width="600"></td>
								</tr>
								<tr>
									<td style="	font-family: Georgia;font-size:10px;line-height:12px;text-align:left;color:#888;padding-left:13px">'.$subscribe.' <a  href="http://'.getconfig('SITE_URL').'/newsletter/berhenti/'.$data['subId'].'/'.$data['subCode'].'/">unsubscribe now</a>.</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>';
				
		#Insert into newsletter_email_queue
		$countemail = newsletter_sendmail($nid,$subid,$tmpsign,$emailto,$emailsubject,$content,$content);
		
		#Count email sent
		if($countemail == true){
			$countemailsend++;
		}
	}
?>