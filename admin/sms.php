<?php
	$page="sms";
	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/mainmod.php");	
	include("../modz/connic.php");
	include("../modz/getall-merchant.php");
	include("../modz/errormsg.php");
	include("authuser.php");
	include("../modz/mainmod-extend.php");
	
    if ($form=="submit"){
        
        if ($action=="add"){
            if(empty($to) or empty($text) or empty($senderid)){
				$error=errorlist(2);
			}
            if(!$error){
				$sql="INSERT INTO sms_merchant(merchantId, smsTo, smsText, smsSenderId, smsDay, smsMonth, smsYear, smsDate, smsTime,smsTimetime,smsStatus) VALUES ('$cook_merchant_id','$to','$text','$senderid','".date('d', $now)."','".date('m', $now)."','".date('Y', $now)."','".date('Y-m-d', $now)."','".date('H:i:s', $now)."','$now','new')";
				$query=query($sql);
            }
      
		}else if($action=="cekbalance"){

            if($getbalance < 235 or empty($getbalance)){
                $error=errorlist(65);
            }else{
                print "<script>window.location='/sms/add'</script>";
            }
        }
        if(!$query){
			$error=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:/sms/");
			}else{
				print "ok";
			}
		}
		exit;
    }
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print _('brosismerchant_cashback_history_transaction'); ?> - <?php print _('brosismerchant_merchant_area'); ?> - <?php print SITE_NAME; ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="/libs/bootstrap-datepicker/css/datepicker3.css" />
<link rel="stylesheet" href="/libs/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />

<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">


<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>

<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="/libs/moment/min/moment-with-locales.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($){
	$('#result').hide();
	validate('#result','#add','/sms');
	validate('#result','#edit','/sms');
	validate('#result','#balance','/sms');
	
	$('html').click(function(){
		$('.ui-widget').slideUp(500);
	});
	
	$(".datetimepicker").datetimepicker({
		format: 'DD-MM-YYYY',
		locale: 'id' 
	});
});

</script>
<script>
    function countChar(val) {
        var len = val.value.length;
        var jumsms = Math.ceil(len/160);
        $('#charNum').text(len + ' Karakter');
        $('#countSMS').text(jumsms + ' SMS');
        
    }
</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">
            <?php
            $checksms=countdata("merchant_smsviro","merchantId='$cook_merchant_id'");
            if($checksms > 0){
            ?>
			<div class="row">
				<div class="col-md-12">
					<h3><?php print _('brosismerchant_sms_page_title'); ?></h3>
                    <p><?php print _('brosismerchant_sms_page_title_desc'); ?></p>
                    <h3>Balance: 
                        <?php
                            $smsviro = getval("merchantSmsUsername,merchantSmsPassword","merchant_smsviro","merchantId=$cook_merchant_id");
                            $balance = GetBalance(endek('decrypt',$smsviro['merchantSmsUsername']),endek('decrypt',$smsviro['merchantSmsPassword'])); 
                            print $balance['currency'].number_format($balance['balance']);
                        ?>
                    </h3>
					<hr>
				</div>
			</div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                    <div class="pull-right">
                        <form action="/merchant/sms.php?form=submit&amp;action=cekbalance" method="post" enctype="multipart/form-data" name="balance" id="balance">
                            <input type="hidden" name="getbalance" value="<?php print $balance['balance']; ?>">
                            <button class="btn btn-primary"><?php print _('brosismerchant_sms_btnadd'); ?></button>
                        </form>
                    </div>
                </div>
            </div>
			<div class="row">
				<div class="col-md-12">
                    <?php 
					if ($action==""){
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dt-responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th style="width:20px;"><?php print _('cms_no'); ?></th>
                                            <th style="width:130px;"><?php print _('brosismerchant_sms_messageid'); ?></th>
                                            <th style="width:120px;"><?php print _('brosismerchant_sms_to'); ?></th>
                                            <th style="width:125px;"><?php print _('brosismerchant_sms_from'); ?></th>
                                            <th style="width:500px;"><?php print _('brosismerchant_sms_text'); ?></th>
                                            <th style="width:100px;" class="text-center"><?php print _('brosismerchant_sms_status'); ?></th>
                                            <th style="width:20px;"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if(empty($pg)){
                                            $pg=1;
                                        }

                                        $numPerPage=20;
                                        $offset=($pg-1)*$numPerPage;
                                        $num=1;
                                        $num=$num+(($pg-1)*$numPerPage);
                                        $sql="SELECT * FROM sms_merchant WHERE merchantId='$cook_merchant_id' ORDER BY smsId DESC";
                                        $query=query($sql);
                                        $numofdata=rows($query);
                                        $sql.=' LIMIT '.$offset.','.$numPerPage;
                                        $query=query($sql);
                                        if($numofdata<1){
                                        ?>
                                        <tr>
                                            <td colspan="7" class="text-center"><?php print _('brosismerchant_sms_data_empty');  ?></td>
                                        </tr>
                                        <?php
                                        }else{
                                            $arrayhistory = array();
                                            while($data = fetch($query)){
                                                $arrayhistory[] = $data;
                                                $sql2 = "SELECT * FROM sms_history_merchant WHERE historyMessageId='$data[messageId]' ORDER BY historyId DESC LIMIT 1";
                                                $query2 = query($sql2);
                                                $data2 = fetch($query2);
                                                $historyresponse = $data2['historyResponse'];
                                                $decodejson = json_decode($historyresponse, true);
                                                $statusgroupid = $decodejson['results'][0][status][groupId];
                                                $statusgroupname= $decodejson['results'][0][status][groupName];
                                        ?>
                                        <tr>
                                            <td><?php print $num; ?></td>
                                            <td><?php print output($data['messageId']); ?></td>
                                            <td><?php print output($data['smsTo']); ?></td>
                                            <td><?php print output($data['smsSenderId']); ?></td>
                                            <td><?php print output($data['smsText']); ?></td>
                                            <td class="text-center">
                                                <?php
                                                if(empty($data['messageId'])){
                                                ?>
                                                <span class="label label-info">New</span>
                                                <?php
                                                }else{
                                                    if($statusgroupid==1){
                                                        print "<span class='label label-warning'>$statusgroupname</span>";
                                                    }else if($statusgroupid==2){
                                                        print "<span class='label label-danger'>$statusgroupname</span>";
                                                    }else if($statusgroupid==3){
                                                        print "<span class='label label-success'>$statusgroupname</span>";
                                                    }
                                                ?>
                                                
                                                <?php
                                                }
                                                ?>
                                            </td>
                                            <td class="text-center">
                                                <?php
                                                if(!empty($data['messageId'])){
                                                ?>
                                                <a id="modal-history" href="#modal-history<?php print $data['smsId']; ?>"  data-toggle="modal"><i class="fa fa-list"></i></a>
                                                <?php
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                            $num++;
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?php
                        $options['total']=$numofdata;
                        $options['filename']='merchant_cat.php';
                        $options['qualifier']='merchant_cat.php';
                        $options['pg']=$pg;
                        $options['numPerPage']=$numPerPage;
                        $options['style']=1;
                        $options['addquery']=TRUE;
                        paging($options);
                        ?>
                    </div>
                    <?php
                    foreach($arrayhistory as $index=>$val){
                    ?>
                    <div class="modal fade" id="modal-history<?php print $val['smsId']; ?>" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">

                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        ×
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">
                                        History SMS
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <th style="width:20px;">No</th>
                                                <th style="width:10px;">Message ID</th>
                                                <th style="width:130px;">Sent AT</th>
                                                <th style="width:130px;">Done AT</th>
                                                <th style="width:10px;" class="text-center">SMS Count</th>
                                                <th style="width:120px;">Price</th>
                                                <th style="width:50px;">Status</th>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $sqlhistory = "SELECT * FROM sms_history_merchant WHERE historyMessageId='$val[messageId]'";
                                                $queryhistory = query($sqlhistory);
                                                $rowshistory = rows($queryhistory);
                                                if($rowshistory > 0){
                                                    $numb = 1;
                                                    while($datahistory = fetch($queryhistory)){
                                                        $history_response = $datahistory['historyResponse'];
                                                        $decode_json = json_decode($history_response, true);
                                                        //print "<pre>";
                                                        //print_r($decode_json);
                                                ?>
                                                <tr>
                                                    <td><?php print $numb; ?></td>
                                                    <td><?php print $decode_json['results'][0][messageId]; ?></td>
                                                    <td><?php print gmdate("Y-m-d H:i:s",strtotime($decode_json['results'][0][sentAt])); ?></td>
                                                    <td><?php print gmdate("Y-m-d H:i:s",strtotime($decode_json['results'][0][doneAt])); ?></td>
                                                    <td class="text-center"><?php print $decode_json['results'][0][smsCount]; ?></td>
                                                    <td><?php print "Per Message: ".$decode_json['results'][0][price][pricePerMessage]."<br>Currency: ".$decode_json['results'][0][price][currency]; ?></td>
                                                    <td><?php print "Group Name<br>"."<strong>".$decode_json['results'][0][status][groupName]."</strong><br>Name<br><strong>".$decode_json['results'][0][status][name]."</strong>"; ?></td>
                                                </tr>
                                                <?php
                                                        
                                                        $numb++;
                                                    }
                                                }else{
                                                ?>
                                                <tr>
                                                    <td colspan="9" class="text-center">Belum Ada History</td>
                                                </tr>
                                                <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="modal-footer">

                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                        Close
                                    </button>
                                </div>
                            </div>

                        </div>

                    </div>
                    <?php
                    }    
                    ?>
                    <?php
                    }else if($action=="add"){
                        if($balance['balance'] < 235){
                            print "<script>window.location='sms.php'</script>";
                        }
                        
                        $sqladd = "SELECT merchantSenderId FROM merchant_smsviro WHERE merchantId='$cook_merchant_id' LIMIT 1";
                        $queryadd = query($sqladd);
                        $datadd = fetch($queryadd);
                        $senderid = explode(',', $datadd['merchantSenderId']);
                    ?>
                    <a href="/sms" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
	        		<h4><?php print _('brosismerchant_sms_add'); ?></h4>
					<hr/>
                    <form action="/merchant/sms.php?form=submit&amp;action=add" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
	                        	<?php print _('brosismerchant_sms_from'); ?><span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-4 col-xs-12">
	                        	<select class="form-control" name="senderid" required>
                                    <?php
                                    foreach($senderid as $s){
                                    ?>
	                        	    <option value="<?php print $s; ?>"><?php print $s; ?></option>
                                    <?php
                                    }
                                    ?>
	                        	</select>
	                        </div>
                        </div>
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
	                        	<?php print _('brosismerchant_sms_to'); ?><span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-4 col-xs-12">
	                        	<input name="to" type="text" id="to" size="40" class="form-control" required>
                                <span class="help-block">ex: 08123xxxx, +62812xxx</span>
	                        </div>
                        </div>
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="mCatPicLogo">
	                        	<?php print _('brosismerchant_sms_text'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-4 col-xs-12">
                                <textarea class="form-control" cols="10" rows="10" name="text" id="text" onkeyup="countChar(this)" required></textarea>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 col-sm-6 col-lg-6">
                                        <div id="charNum" class="help-block" style="font-weight: bold;">0 Karakter</div>
                                    </div>
                                    <div class="col-md-6 col-xs-6 col-sm-6 col-lg-6">
                                        <div id="countSMS" class="help-block text-right" style="font-weight: bold;">1 SMS</div>
                                    </div>
                                </div> 
	                        </div>
                    	</div>
                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='sms.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>

                    </form>
                    <?php
                    }
                    ?>
				</div>


			</div>
            <?php
            }else{
                print getval("contentDesc","content","contentId='42'");
            }
            ?>
        </div>
        <!-- /page content -->

        <?php include("com/com-footer.php"); ?>
	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>