<?php
	$page=7;

	//includes all files necessary to support operations
	include($argv[1]."/modz/config-main.php");
	include($argv[1]."/modz/config.php");
	include($argv[1]."/modz/license.php");
	include($argv[1]."/modz/errormsg.php");
	include($argv[1]."/modz/mainmod.php");
	include($argv[1]."/modz/connic.php");
	include($argv[1]."/modz/getall-admin.php");

	$fname = $argv[3];
	$cmd = ($argv[2]!='unknown' ? $argv[2].DIRECTORY_SEPARATOR:'') .'mysqldump -h '.getconfig('DB_HOST').' -u '.getconfig('DB_USER').' -p'.getconfig('DB_PASSWORD').' --add-drop-table --skip-comments '.getconfig('DB_NAME').' > "'.$argv[1].'/assets/backup/'.$fname.'.sql"';
	cli($cmd,FALSE);
	
	#compress the file into zip format
	$tmp=$argv[1]."/assets/backup/".$fname;
	$zip = new ZipArchive();
	$status=$zip->open($tmp.'.zip', ZIPARCHIVE::CREATE) ;

	if ($status=== TRUE) {
		$zip->addFile($tmp.'.sql', $fname.'.sql');
		$zip->close();
	}
	
	$f = fopen("{$tmp}.wb",'w');
	fwrite($f,"ok");
	fclose($f);
?>
 