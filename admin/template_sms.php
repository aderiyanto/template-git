<?php
	$page=45;
	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config-image.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/mainmod-extend.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");
	
    if ($form=="submit"){
        
        if ($action=="add"){
            if(empty($name) OR empty($content) ){
				$error=errorlist(2);
			}
            
            if(!$error){
                $sql="INSERT INTO template_sms(tempName,tempTemplate) VALUES ('$name','$content')";
                $query=query($sql);
            }
      
		}elseif($action=="del"){
           if(empty($id)){
               $error=errorlist(2);
           }else{
               $sql = "DELETE FROM template_sms WHERE tempId='$id'";
               $query = query($sql);
           }
		}elseif($action=="edit"){
		    if(empty($id) or empty($name) or empty($content)){
				$error=errorlist(2);
			}
            
            if(!$error){
                $sql="UPDATE template_sms SET tempName='$name',tempTemplate='$content' WHERE tempId='$id'"; 
                $query=query($sql);
                
                
            }
            
		}
        
        
        if(!$query){
			$error=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:template_sms.php");
			}else{
				print "ok";
			}
		}
		exit;
    }
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print _('sms_template_page_title'); ?> - <?php print _('merchant_area'); ?> - <?php print SITE_NAME; ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="/libs/bootstrap-datepicker/css/datepicker3.css" />
<link rel="stylesheet" href="/libs/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />

<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">
<link href="/style/chosen.css" rel="stylesheet">
<!--<link rel="stylesheet" type="text/css" href="/style/footable.bootstrap.css" />-->
<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/javascript/chosen.jquery.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>

<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>

<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="/libs/moment/min/moment-with-locales.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!--<script type="text/javascript" src="/javascript/footable.js"></script>-->
<script type="text/javascript">
jQuery(document).ready(function($){
	$('#result').hide();
	validate('#result','#add','template_sms.php');
	validate('#result','#edit','template_sms.php');
	
	$('html').click(function(){
		$('.ui-widget').slideUp(500);
	});
//	$('.table').footable();
//	$('.tbody').footable({
//		"empty": "There are no rows"
//	});
});

</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">
            <?php
            ?>
			<div class="row">
				<div class="col-md-12">
					<h3><?php print _('sms_template_page_title'); ?></h3>
                    <p><?php print _('sms_template_page_title_desc'); ?></p>
					<hr>
				</div>
			</div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                    <div class="pull-right">
                           <a class="btn btn-primary" href="template_sms.php?action=add"><?php print _('sms_template_add'); ?></a>
                    </div>
                </div>
            </div>
			<div class="row">
				<div class="col-md-12">
                    <?php 
					if ($action==""){
						?>
					   
						<div class= "row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
										<thead>
											<tr>
												<th style="width:20px;"><?php print _('cms_no'); ?></th>
												<th class="text-center"><?php print _('sms_template_name'); ?></th>
												<th	class="text-center" data-breakpoints="xs"><?php print _('sms_template_content'); ?></th>
												<th style="width:20px;" data-breakpoints="xs"></th>
												<th style="width:20px;" data-breakpoints="xs"></th>
												<th style="width:20px;" data-breakpoints="xs"></th>
											</tr>
										</thead>
										<tbody data-empty="bcdcbd">
											<?php
											if (empty($pg)){
												$pg=1;
											}
											$numPerPage=10;
											$offset=($pg-1)*$numPerPage;
											$num=1;
											$num=$num+(($pg-1)*$numPerPage);
											
											$sql = "SELECT * FROM template_sms ORDER BY tempName ASC";
											$query=query($sql);
											$numofdata=rows($query);

											$sql.=' LIMIT '.$offset.','.$numPerPage;
											$query=query($sql);
											$num=1;
											if($numofdata>0){
												while ($data=fetch($query)){
													$data = output($data);
													?>
													<tr>
														<td><?php print $num; ?></td>
														<td><?php print $data['tempName']; ?></td>
														<td>
															<?php 
																print substr($data['tempTemplate'],0,10).'..' ; 
																
															?>
															<a rel="tooltip" class="label label-warning" data-toggle="modal" data-target="#<?php print $data['tempId']; ?>" style="cursor:pointer" title="<?php print _('sms_template_view_detail'); ?>">...</a>
															<!---modal area -->
															<div id="<?php print $data['tempId']; ?>" class="modal fade" role="dialog">
																<div class="modal-dialog modal-lg">
																	<div class="modal-content">
																		<div class="modal-header">
																			<button type="button" class="close" data-dismiss="modal">&times;</button>
																			<h4 class="modal-title"><?php print _('sms_template_content'); ?></h4>
																		</div>
																		<div class="modal-body">
																			<div class="row">
																				<div class="col-md-12">
																					<?php 
																						print $data['tempTemplate']  ; 
																						
																					?>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</td>
														
														<td class="text-center">
															<?php print strlen($data['tempTemplate']); ?>
														</td>
														<td class="text-center">
															<a href="template_sms.php?action=edit&amp;id=<?php print $data['tempId']; ?>" rel="tooltip" title="<?php print _('cms_edit'); ?>: <?php print ucwords($data['tempName']); ?>"><i class="fa fa-edit"></i> </a>
														</td>
														<td class="text-center">
															<a href="template_sms.php?form=submit&action=del&id=<?php print $data['tempId']; ?>" title="Apakah anda yakin akan menghapus : <?php print ucwords($data['tempName']); ?>" class="delete" ><i title="<?php print _('cms_delete'); ?>: <?php print ucwords($data['tempName']); ?>" rel="tooltip" class="fa fa-trash-o"></i></a>
														
														</td>    
													</tr>
													<?php
												$num++;
												
												}
											}
											?>
										</tbody>
									</table>
								</div>
								
							</div>
						  
						</div>
						
						<?php
						   
						$options['total'] = $numofdata;
						$options['filename'] = 'template_sms.php';
						$options['qualifier'] = '{pg}';
						$options['pg'] = $pg;
						$options['numPerPage'] = $numPerPage;
						$options['style'] = 2;
						$options['addquery'] = FALSE;
						paging($options);
                 
                    }else if($action=="add"){
						?>
						<a href="template_sms.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
						<h4><?php print _('sms_template_add_title'); ?></h4>
						<hr/>
						<form action="template_sms.php?form=submit&amp;action=add" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
									<?php print _('sms_template_name'); ?><span class="required">*</span>
								</label>
								<div class="col-md-4 col-sm-4 col-xs-12">
									<input name="name" type="text" id="name" size="40" class="form-control" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
									<?php print _('sms_template_content'); ?><span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<textarea class="form-control" cols="20" rows="6" name="content" id="text"  required></textarea>
								</div>
							</div>
							<hr/>

							<div class="form-group">
								<div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
									<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='template_sms.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
									<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
								</div>
							</div>

						</form>
						<?php
                    }else if($action=="edit"){
                        $sql="SELECT * FROM template_sms WHERE tempId='$id'";
                        $query=query($sql);
                        $data=fetch($query);
                        $data=formoutput($data);
                     
						?>
						<a href="template_sms.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
						<h4><?php print _('sms_template_edit_title'); ?></h4>
						<hr/>
						<form action="template_sms.php?form=submit&amp;action=edit&id=<?php print $id ?>" method="post" enctype="multipart/form-data" name="edit" id="edit" class="form-horizontal form-label-left">
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
									<?php print _('sms_template_name'); ?><span class="required">*</span>
								</label>
								<div class="col-md-4 col-sm-4 col-xs-12">
									<input name="name" type="text" id="name" size="40" class="form-control" required value="<?php print $data['tempName']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">
									<?php print _('sms_template_content'); ?><span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<textarea class="form-control" cols="20" rows="6" name="content" id="text"  required><?php print $data['tempTemplate']; ?></textarea>
								</div>
							</div>
							<hr/>

							<div class="form-group">
								<div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<button name="Submit" type="submit" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> <?php print _('cms_updatebutton'); ?></button>
									<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='template_sms.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
									<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
								</div>
							</div>

						</form>
						<?php
                    }
                    ?>
				</div>
			</div>
            <?php
            ?>
        </div>
        <!-- /page content -->

        <?php include("com/com-footer.php"); ?>
	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
<script>
	var config = {
  '.chosen-select'           : {},
  '.chosen-select-deselect'  : { allow_single_deselect: true },
  '.chosen-select-rtl'       : { rtl: true },
  '.chosen-select-width'     : { width: '95%' }
}
for (var selector in config) {
  $(selector).chosen(config[selector]);
}
</script>
</body>
</html>