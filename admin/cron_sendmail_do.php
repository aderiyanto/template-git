<?php
	#this file only allow to execute by using cron or cli command
	if(! defined('STDIN') ){
		die('Forbidden Access');
	}

	//includes all files necessary to support operations
	$basepath = realpath(dirname(__file__).'/..');
	include($basepath."/modz/config-main.php");
	include($basepath."/modz/config.php");
	include($basepath."/modz/license.php");
	include($basepath."/modz/errormsg.php");
	include($basepath."/modz/mainmod.php");
	include($basepath."/modz/connic.php");
	include($basepath."/modz/getall-admin.php");

	#This file will access data from database and send it as email according to cron schedule
	$sql = "SELECT * FROM email_queue WHERE emailStatus='n' ORDER BY emailId ASC LIMIT ".getconfig('EMAIL_QUOTA');
	$query = query($sql);
	$numofdata = rows($query);

	if ($numofdata>0){
		while($data = fetch($query)){
			if (!empty($data['emailHead'])){
				list($from, $fromname, $replyto, $cc, $bcc,$toname) = explode("#", $data['emailHead']);
			} else {
				$from = SITE_EMAIL;
				$fromname = SITE_NAME;
			}

			if (!empty($data['emailAttachFile'])){
				$attachment = $basepath.'/assets/emailqueue/'.$data['emailAttachDir'].'/'.$data['emailAttachFile'];
			} else {
			$attachment  = '';
			}
			
			#Send mail
			$options = array(
				'mailopt' => '',
				'from' => $from,
				'fromname' => $fromname,
				'to' => $data['emailTo'],
				'toname' => $toname,
				'subject' => $data['emailSubject'],
				'replyto' => $replyto,
				'cc' => $cc,
				'bcc' => $bcc,
				'message' => $data['emailMsg'],
				'messagetype' => $data['emailMsgType'],
				'filename' => $attachment
			);
			$mail = sendMailComplete($options);
			
			#Delete email if has been sent.
			if($mail){
				$sqlx = "UPDATE email_queue SET emailStatus='y', emailDateSent='$now' WHERE emailId='".$data['emailId']."'";
				$queryx = query($sqlx);
			}
		}
	}
 
	//update activity statistic
	$cron_option['cron_name'] = 'CRON_SENDMAIL'; 
	cron_activity_log($cron_option);
?>