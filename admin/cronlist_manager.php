<?php
	$page=36;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/mainmod-extend.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

	if($form == "submit"){
		if($action=="active" and $uac_edit){
			$sql="UPDATE activitylog SET actStatus='y' WHERE actId='". (int)$id ."'";
			$query=query($sql);
		}elseif($action == 'inactive' and $uac_edit){
			$sql="UPDATE activitylog SET actStatus='n' WHERE actId='". (int)$id ."'";
			$query=query($sql);
		}

		//check whether query was successful
		if(!$query){
			$error=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				$uri="cronlist_manager.php";
				header("location:".$uri);
			}else{
				print "ok";
			}
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _("cronlist_manager_pagetitle"); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- required for this page -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($){
	$('.fancybox').fancybox({
		'arrows': false
	});
});
</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('cronlist_manager_pagetitle'); ?></h3>
			                <p><?php print nl2br(_('cronlist_manager_pagedesc')); ?></p>
			            </div>
		            </div>
	        	</div>
        	</div>

        	<div class="row">
	        	<div class="col-md-12">
	        	<?php if ($action==""){ ?>
	        		<div class="table-responsive">
            			<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
            				<thead>
            					<tr>
            						<th width="25" class="text-center"><?php print _("cms_no"); ?></th>
									<th><?php print _("cronlist_manager_view_name"); ?></th>
									<th class="text-center" width="120"><?php print _("cronlist_status"); ?></th>
									<th class="text-center" width="100"><?php print _("cronlist_duration"); ?></th>
									<th class="text-center" width="210"><?php print _("cronlist_manager_view_lastactivity"); ?></th> 
									<th class="text-center" width="210"><?php print _("cronlist_nextactivity"); ?></th>
            					</tr>
            				</thead>
            				<tbody>
            					<?php 
									$num=1;
									$sql="SELECT * FROM activitylog ORDER BY actId ASC";
									$query=query($sql);
									$numofdata=rows($query);
									$query=query($sql);

									if ($numofdata<1){
								?>
								<tr>
								  	<td colspan="6" class="text-center"><br />
							  	    <?php print _("cronlist_manager_view_notavailable"); ?>
							  	    <br /><br />
									</td>
							    </tr>
								<?php
									} else {
										while ($data=fetch($query)){
								?>
								<tr style="vertical-align: top">
									<td class="text-center"><?php print $num; ?></td>
									<td>
										<strong><?php print $data['actName'] ?></strong> <a href="#detail<?php print $data['actId'] ?>" class="fancybox" rel="tooltip" title="Detail"><i class="fa fa-eye"></i></a><br />
										<?php if($uac_edit){ if($data['actUrl']){ ?>Path: <a href="<?php print $data['actUrl'];?>?t=<?php print date('mY', $now); ?>" target="_blank"><?php print $data['actUrl'];?> <i class="fa fa-external-link"></i></a><?php } else { print "-"; } } ?>

										<div style="display: none;" id="detail<?php print $data['actId'] ?>">
											<h4><?php echo _('cronlist_detailinfotitle'); ?></h4>
											<hr/>
											<strong>cPanel Code:</strong><br/>
											<code>
											<?php echo $data['actDesc']; ?>
											</code>
										</div>
									</td>									
									<td class="text-center">
										<?php print $data['actStatus'] == "y" ? "<a href=\"cronlist_manager.php?form=submit&amp;action=inactive&amp;id={$data['actId']}\" class=\"btn btn-success btn-xs\">" . _('cronlist_stat_y') . "</a>" : "<a href=\"cronlist_manager.php?form=submit&amp;action=active&amp;id={$data['actId']}\" class=\"btn btn-danger btn-xs\">" . _('cronlist_stat_n') . "</a>";
										?>
									</td>
									<td class="text-center">
										<?php print $data['actDuration'] . " " . _('cronlist_minutes'); ?>
									</td>
									<td class="text-center">
										<?php print $data['actLastActivity'] > 0 ? dateformat($data['actLastActivity'],'full'): '-'; ?>
									</td>
									<td class="text-center">
										<?php 
											$nextcronact = strtotime("+{$data['actDuration']} minutes", $data['actLastActivity']);
											print ($data['actDuration'] > 0 AND $data['actLastActivity'] > 0) ? dateformat($nextcronact,'full'): '-';
										?>
									</td>
								</tr>
								<?php
											$num++;
										}
									}
								?>
            				</tbody>
            			</table>
            		</div>
            		<br/><br/><br/>
	        	<?php } ?>
	        	</div>
	        </div>


		</div>
        <!-- END THE CONTENT OF PAGE HERE -->


		<?php include("com/com-footer.php"); ?>

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>