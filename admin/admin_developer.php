<?php
	$page=15;
	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config-image.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/mainmod-extend.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

	if ($form=="submit"){
	//if any form is submitted
		if ($action=="add" and $uac_add){
			//check for required fields
			if (empty($gid) or empty($name) or empty($username) or empty($password) or empty($confirmpwd)){
				$error=errorlist(2);
			}
			if(strlen($password)<6){
				$error.=errorlist(7);
			}
			if($password != $confirmpwd){
				$error.=errorlist(6);
			}
			if($password == $username){
				$error.=errorlist(11);
			}
			if (checkduplicate("admin","adminUsername",$username)) {
				$error.=errorlist(8);
			}

			// Get Next ID			
			$nextid=nextid("adminId","admin");

			$pic = $_FILES['profilepic'];

			$dirpic='';
			$imgfilename='';

			if(!empty($pic['name'])){				
				//set the restrictions
				$allowedType = array("image/pjpeg","image/jpeg","image/png","image/gif");
				$realExt = array('jpeg','jpg','png','gif');

				if(in_array($pic['type'], $allowedType)){
					$fileExt = array_keys($allowedType, $pic['type']);
					$fileExt = $realExt[$fileExt[0]];

					if(is_uploaded_file($pic['tmp_name'])){
						//make sure it is really really image
						$img=getimagesize($pic['tmp_name']);
						if(!in_array($pic['type'],$allowedType) or !(is_numeric($img[0]) or is_numeric($img[1]) or $img[0]>0 or $img[1]>0)){
							//make sure it is allowed
							$error.=errorlist(13);
						}
					} else{
						$error.=errorlist(27);
					}
				} else{
					$error.=errorlist(13);
				}

				//get filename
				$fname = str_replace('.','',$pic['name']).'.'.$fileExt;

				//For big image
				$imgupload=uploadit($fname,$pic['tmp_name'],"profile",$nextid,PROFILE_SMALL_WIDTH,PROFILE_SMALL_HEIGHT,PROFILE_MEDIUM_WIDTH,PROFILE_MEDIUM_HEIGHT,PROFILE_LARGE_WIDTH,PROFILE_LARGE_HEIGHT);
				$dirpic=$imgupload['dir'];
				$imgfilename=$imgupload['filename'];
			}

			$password = sha1($password.getconfig('CMS_SALT_STRING'));
			//insert data
			if (!$error){
				$sql="INSERT INTO admin VALUES($nextid,'$gid','$name','$username','$password',$now,'y','$dirpic','$imgfilename')";
				$query=query($sql);
			}
		}elseif ($action=="edit" and $uac_edit){
			//check for required fields
			if (empty($gid) or empty($name)){
				$error=errorlist(2);
			}

			if (!empty($newpwd)){
				if(strlen($newpwd)<6){
					$error.=errorlist(7);
				}
				if($newpwd == $username){
					$error.=errorlist(11);
				}

				//Warn if the current user want to reset password it self
				if($cook_id==$id){
					$error.=errorlist(33);
				}else{
					//super user wanna reset password
					$newpwd = sha1($newpwd.getconfig('CMS_SALT_STRING'));
					$updatepwd=", adminPassword='" . $newpwd . "'";
				}
			}

			// Get Next ID			
			$nextid=$id;

			$pic = $_FILES['profilepic'];

			$updatepic='';

			if(!empty($pic['name'])){				
				//set the restrictions
				$allowedType = array("image/pjpeg","image/jpeg","image/png","image/gif");
				$realExt = array('jpeg','jpg','png','gif');

				if(in_array($pic['type'], $allowedType)){
					$fileExt = array_keys($allowedType, $pic['type']);
					$fileExt = $realExt[$fileExt[0]];

					if(is_uploaded_file($pic['tmp_name'])){
						//make sure it is really really image
						$img=getimagesize($pic['tmp_name']);
						if(!in_array($pic['type'],$allowedType) or !(is_numeric($img[0]) or is_numeric($img[1]) or $img[0]>0 or $img[1]>0)){
							//make sure it is allowed
							$error.=errorlist(13);
						}
					} else{
						$error.=errorlist(27);
					}
				} else{
					$error.=errorlist(13);
				}

				//get filename
				$fname = str_replace('.','',$pic['name']).'.'.$fileExt;

				$imgprofile = getval("adminDirPic, adminPic","admin", "adminId='".$id."'");

				if ($imgprofile['adminDirPic'] AND $imgprofile['adminPic']){
					//only when the existing photos exist
					@unlink("../assets/profile/".$imgprofile['adminDirPic']."/".$imgprofile['adminPic']);
					@unlink("../assets/profile/".$imgprofile['adminDirPic']."/".genthumb($imgprofile['adminPic'],"m"));
					@unlink("../assets/profile/".$imgprofile['adminDirPic']."/".genthumb($imgprofile['adminPic'],"s"));
				}

				//For big image
				$imgupload=uploadit($fname,$pic['tmp_name'],"profile",$nextid,PROFILE_SMALL_WIDTH,PROFILE_SMALL_HEIGHT,PROFILE_MEDIUM_WIDTH,PROFILE_MEDIUM_HEIGHT,PROFILE_LARGE_WIDTH,PROFILE_LARGE_HEIGHT);
				$dirpic=$imgupload['dir'];
				$imgfilename=$imgupload['filename'];

				$updatepic = ", adminDirPic='{$dirpic}', adminPic='{$imgfilename}'";
			}

			//update table
			if (!$error){
				$sql="UPDATE admin SET groupId='{$gid}', adminName='{$name}'{$updatepwd}{$updatepic} WHERE adminId='" . $id . "'";
				$query=query($sql);
			}

		}elseif ($action=="del" and $uac_delete){
			if ($id==1){
				$error=errorlist(19);
			}

			if(!$error){
				$sql="SELECT * FROM admin WHERE adminUsername='$cook_username'";
				$query=query($sql);
				$data=fetch($query);

				if(	$data['adminId']==$id){
					$error=errorlist(18);
				}

				if(!$error){
					//cant delete default user
					$imgprofile = getval("adminDirPic, adminPic","admin", "adminId='".$id."'");

					if ($imgprofile['adminDirPic'] AND $imgprofile['adminPic']){
						//only when the existing photos exist
						@unlink("../assets/profile/".$imgprofile['adminDirPic']."/".$imgprofile['adminPic']);
						@unlink("../assets/profile/".$imgprofile['adminDirPic']."/".genthumb($imgprofile['adminPic'],"m"));
						@unlink("../assets/profile/".$imgprofile['adminDirPic']."/".genthumb($imgprofile['adminPic'],"s"));
					}
					//delete admin data
					$sql="DELETE FROM admin WHERE adminId='" . $id . "'";
					$query=query($sql);
				}
			}
		}elseif ($action=="activate"){
			$sql="UPDATE admin SET adminActive='y' WHERE adminId='" . $id . "' AND adminId!='1'";
			$query=query($sql);
		}elseif ($action=="deactivate"){
			$sql="UPDATE admin SET adminActive='n' WHERE adminId='" . $id . "' AND adminId!='1'";
			$query=query($sql);
		}

		//check whether query was successful
		if(!$query){
			$error=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:admin_developer.php");
			}else{
				print "ok";
			}
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _('admin_pagetitle'); ?></title>
<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce_simple.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('.fancybox').fancybox({
		'arrows': false
	});
	$('#result').hide();
	validate('#result','#addadmin','admin_developer.php');
	validate('#result','#editadmin','admin_developer.php');
});
</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main" style="min-height:500px !important">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('admin_pagetitle'); ?></h3>
			            </div>
		            </div>
	        	</div>
        	</div>

        	<?php if ($uac_add){ ?>
        	<div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                	<div class="pull-right">
                		<a href="admin_developer.php?action=add" class="btn btn-primary"><i class="fa fa-plus"></i> <?php print _('admin_addbutton'); ?></a>
                	</div>
                </div>
        	</div>
        	<?php } ?>

        	<!-- THE CONTENT OF PAGE HERE -->
        	<div class="row">
	        	<div class="col-md-12">
	        		<?php if ($action==""){ ?>
	        		<div class="table-responsive">
        				<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
        					<thead>
        						<tr>
									<th width="35"><?php print _('cms_no'); ?></th>
									<th><?php print _('admin_name'); ?></th>
									<th width="200" class="text-center"><?php print _('admin_group'); ?></th>
									<th width="250" class="text-center"><?php print _('admin_lastlogin'); ?></th>
									<th width="100" class="text-center"><?php print _('admin_stat'); ?></th>
									<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
									<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
									<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
								</tr>
        					</thead>
        					<tbody>
        						<?php
									$sql="SELECT * FROM admin a, admin_group g WHERE a.groupId=g.groupId ORDER BY a.groupId, a.adminName";
									$query=query($sql);
									$num=1;
									while ($data=fetch($query)){
										$data=output($data);
								?>
								<tr>
									<td class="text-center"><?php print $num; ?></td>
									<td><?php print $data['adminName']; ?> <?php print "[" . $data['adminUsername'] . "]"; ?></td>
									<td class="text-center"><?php print $data['groupName']; ?></td>
									<td class="text-center"><?php print dateformat($data['adminLastLogin'],"full"); ?></td>
									<td class="text-center"><?php print ($data['adminActive']=="y")?"<span class=\"label label-success\">Aktif</span>":"<span class=\"label label-danger\">Non-Aktif</span>"; ?></td>
									<td class="text-center">
							            <?php if($uac_edit and $data['adminId']!="1"){ ?>
											<?php if ($data['adminActive']=="y"){ ?>
								            <a href="javascript:if (window.confirm('<?php print _('admin_statusinfo1'); ?>.')){ window.location='admin_developer.php?form=submit&amp;action=deactivate&amp;id=<?php print $data['adminId']; ?>'; };" title="<?php print _('admin_altnonaktif'); ?>: <?php print stripquote($data['adminName']); ?>" rel="tooltip"><i style="color:#cc0000;" class="fa fa-user-times"></i></a>
								                <?php }else{ ?>
								            <a href="javascript:if (window.confirm('<?php print _('admin_statusinfo2'); ?>')){ window.location='admin_developer.php?form=submit&amp;action=activate&amp;id=<?php print $data['adminId']; ?>'; };" title="<?php print _('admin_aktif'); ?>: <?php print stripquote($data['adminName']); ?>" rel="tooltip"><i style="color:#00cc00;" class="fa fa-user"></i></a>
								            <?php } ?>
							        	<?php } ?>
									</td>
									<td class="text-center"><?php if ($uac_edit){ ?><a href="admin_developer.php?action=edit&amp;id=<?php print $data['adminId']; ?>"><i title="<?php print _('cms_edit'); ?>: <?php print stripquote($data['adminName']); ?>" rel="tooltip" class="fa fa-edit"></i></a><?php } ?></td>
									<td class="text-center"><?php if ($uac_delete and $data['adminId']!="1"){ ?><a class="delete" href="admin_developer.php?form=submit&amp;action=del&amp;id=<?php print $data['adminId']; ?>" title="<?php print stripquote($data['adminName']); ?>"><i title="<?php print _('admin_delete'); ?>: <?php print stripquote($data['adminName']); ?>" rel="tooltip" class="fa fa-trash"></i></a><?php } ?></td>
								  </tr>
								  <?php
										$num++;
									}
								?>
        					</tbody>
        				</table>
        			</div>
        			<?php }elseif ($action=="add" and $uac_add){ ?>
        			<a href="admin_developer.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _("admin_addbutton"); ?></h4>
					<hr/>
					<form name="addadmin" id="addadmin" method="post" action="admin_developer.php?action=add&amp;form=submit" class="form-horizontal form-label-left">
						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="gid">
	                        	<?php print _('admin_group'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-3 col-sm-6 col-xs-12">
	                        	<select name="gid" id="gid" class="form-control" required>
					            	<option value="" class="required"><?php print _('admin_selectgroup'); ?></option>
					        		<?php
									 $sqlx="SELECT * FROM admin_group";
										$queryx=query($sqlx);
										while ($datax=fetch($queryx)){
										$datax=formoutput($datax);
									?>
										<option value="<?php print $datax['groupId']; ?>"><?php print $datax['groupName']; ?></option>
										<?php
										}
									?>
				            	</select>
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">
	                        	<?php print _('admin_name'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<input name="name" type="text" id="name" size="24" class="form-control" required />
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="username">
	                        	<?php print _('admin_username'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<input name="username" type="text" id="username" size="24" class="form-control" required />
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="password">
	                        	<?php print _('admin_pass'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<input name="password" type="password" id="password" size="24" class="form-control" required />
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="confirmpwd">
	                        	<?php print _('admin_confirmpass'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<input name="confirmpwd" type="password" id="confirmpwd" size="24" class="form-control" required />
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="profilepic">
	                        	<?php print _('admin_uploadprofilephoto'); ?>
	                        </label>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<input type="file" size="40" name="profilepic" id="profilepic">
	                        	<span class="help-block"><?php echo _('admin_uploadprofilephoto_desc'); ?></span>
	                        </div>
                    	</div>

                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='admin_developer.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>
                    	<br/><br/><br/>
					</form>
					<?php
					} elseif ($action=="edit" and $uac_edit){
						$sql="SELECT * FROM admin WHERE adminId='" . $id . "'";
						$query=query($sql);
						$data=fetch($query);
						$data=formoutput($data);
					?>
					<a href="admin_developer.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('admin_editadmin'); ?> <?php print output($data['adminName']); ?></h4>
					<hr/>
					<form name="editadmin" id="editadmin" method="post" action="admin_developer.php?action=edit&amp;form=submit&amp;id=<?php print $id; ?>" class="form-horizontal form-label-left">
						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="gid">
	                        	<?php print _('admin_group'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-3 col-sm-6 col-xs-12">
	                        	<select name="gid" id="gid" class="form-control">
					            	<?php
										$sqlx="SELECT * FROM admin_group";
										$queryx=query($sqlx);
										while ($datax=fetch($queryx)){
												$datax=formoutput($datax);
										?>
											<option value="<?php print $datax['groupId']; ?>"<?php print ($datax['groupId']==$data['groupId'])?" selected=\"selected\"":""; ?>><?php print $datax['groupName']; ?></option>
										<?php
										}
										?>
				            	</select>
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">
	                        	<?php print _('admin_name'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<input name="name" type="text" id="name" size="24" value="<?php print formoutput($data['adminName']); ?>" class="form-control" required />
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="username">
	                        	<?php print _('admin_username'); ?>
	                        </label>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<input name="username" type="hidden" value="<?php print output($data['adminUsername']); ?>"/>
	                        	<input type="text" class="form-control" value="<?php print output($data['adminUsername']); ?>" disabled="disabled" />
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="newpwd">
	                        	<?php print _('admin_resetpass'); ?>
	                        </label>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<input name="newpwd" type="password" id="newpwd" size="24" class="form-control" />
	                        	<span class="help-block"><?php print _('admin_changepassinfo'); ?></span>
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="confirmpwd">
	                        	<?php print _('admin_lastlogin'); ?>
	                        </label>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<p class="form-control-static"><?php print date("d M Y g:i A",$data['adminLastLogin']); ?></p>
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="profilepic">
	                        	<?php print _('admin_uploadprofilephoto'); ?>
	                        </label>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<?php 
								// get image profile
								if($data['adminDirPic']!='' AND $data['adminPic']!=''){
									echo '<img src="/assets/profile/'.$data['adminDirPic'].'/'.genthumb($data['adminPic'],'m').'" alt="Profile: '.$data['adminName'].'">';
								} else {
									echo '<img src="/assets/images/img.jpg" alt="Profile">';
								}
								?>
								<br/><br/>
	                        	<input type="file" size="40" name="profilepic" id="profilepic">
	                        	<span class="help-block"><?php echo _('admin_uploadprofilephoto_desc'); ?></span>
	                        </div>
                    	</div>

                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_updatebutton'); ?>"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='admin_developer.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>
                    	<br/><br/><br/>
					</form>
	        		<?php } ?>
	        	</div>
	        </div>

		</div>
		<!-- END THE CONTENT OF PAGE HERE -->

		<?php include("com/com-footer.php"); ?>

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>