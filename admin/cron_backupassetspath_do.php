<?php
	#this file only allow to execute by using cron or cli command
	if(! defined('STDIN') ){
		die('Forbidden Access');
	}
	
	//includes all files necessary to support operations
	$basepath = realpath(dirname(__file__).'/..');
	include($basepath."/modz/config-main.php");
	include($basepath."/modz/config.php");
	include($basepath."/modz/license.php");
	include($basepath."/modz/errormsg.php");	
	include($basepath."/modz/mainmod.php");
	include($basepath."/modz/connic.php");
	include($basepath."/modz/getall-admin.php"); 

	if(ONLINE_BACKUP == 'off'){
		exit;
	} 

	#get root dir
	$dir = substr(dirname(__FILE__), 0, -6);
	$scandir = array();
	$get_all_path = array();
	$container_after_checking = array();
	$container_to_upload = array();
	
	#xml file containing list of file path in /xml-backup 
	$xmldir =  $dir.DIRECTORY_SEPARATOR."xml-backup".DIRECTORY_SEPARATOR;
	if(!file_exists($xmldir)){
		mkdir($xmldir);
		chmod($xmldir,755);
	}
	
	$xmlfile  = $dir.DIRECTORY_SEPARATOR."xml-backup".DIRECTORY_SEPARATOR."allfile.xml";
	$xmlfile_to_upload  = $dir.DIRECTORY_SEPARATOR."xml-backup".DIRECTORY_SEPARATOR."uploaded-file.xml";
	foreach(getconfig('ONLINE_BACKUP_PATH') as $back_key => $back_dir){
		$backup_path = $dir.DIRECTORY_SEPARATOR.$back_dir; //assets
		$get_all_path[] = find_all_files($backup_path);
	 
		if(count($get_all_path)){
			foreach($get_all_path as $key => $value){ 
				foreach($value as $k => $v){
					$scandir[] =  $v;
				}
			}
		}
	 
		#make sure the destination directory is exist
		if(is_dir($backup_path)){
			if(is_file($xmlfile)){
				$xmldata = xml_fetchall_data($xmlfile); 
				
				foreach ($scandir as $file_key => $file_name){
					$file_path = $scandir[$file_key];
					$date_modified = filemtime($file_path);

					if(xml_check_element($xmldata,$file_path)){
						$container_after_checking[$file_path] = $xmldata[$file_path];
					}else{
						$container_after_checking[$file_path] = $date_modified;
						$container_to_upload[$file_path] = $date_modified;
					}
				}

				#create complete data xml file
				xml_create_on_modify($container_after_checking,$xmlfile, getconfig('ONLINE_BACKUP_EXCLUDE_PATH'));
				
				#create xml data to upload if new of updated file
				xml_create_on_modify($container_to_upload,$xmlfile_to_upload, getconfig('ONLINE_BACKUP_EXCLUDE_PATH'));
			}else{	
				#create xml file
				xml_list_all_file($scandir,$xmlfile, getconfig('ONLINE_BACKUP_EXCLUDE_PATH'));

				#create xml for firts time upload
				xml_list_all_file($scandir,$xmlfile_to_upload, getconfig('ONLINE_BACKUP_EXCLUDE_PATH'));
			}
		}
	}
	#Do not touch, please :D
	#================================================================================
	function xml_list_all_file($scandir,$xmlfile, $exclude_path = null){
		$dom = new DOMDocument('1.0', 'UTF-8');
		$dom->preserveWhiteSpace = false; 
		$dom->formatOutput = true;
		$root = $dom->createElement("backup");
		$dom->appendChild($root);
		
		$file_exist = false;
		
		foreach ($scandir as $file_key => $file_name){
		 	$file_path = $scandir[$file_key];
			
			#exception for ex_list
			if(count($exclude_path)){
				foreach($exclude_path as $k => $d){
					if(strpos($file_path, $d)){ 
						$file_exist = true;
						if($file_exist == true){ break; }
					}
				}
				
				if($file_exist == true){ 
					$file_exist = false;
					continue; 
				}
			}
			
			$item = $dom->createElement("file");
			$root->appendChild($item);

			// create text node
			$modified_date = $dom->createTextNode(filemtime($file_path));
			$item->appendChild($modified_date);

			// create attribute node
			$filepath = $dom->createAttribute("file_path");
			$item->appendChild($filepath);

			// create attribute value node
			$pathval = $dom->createTextNode($file_path);
			$filepath->appendChild($pathval);
		}

		$saved = $dom->save($xmlfile);
		chmod($xmlfile,0755);
		unset($dom); 
	}

	function xml_create_on_modify($scandir,$xmlfile, $exclude_path = null){
		$dom = new DOMDocument('1.0', 'UTF-8');
		$dom->preserveWhiteSpace = false; 
		$dom->formatOutput = true;
		$root = $dom->createElement("backup");
		$dom->appendChild($root);
		
		$file_exist = false;
		
		foreach ($scandir as $file_path => $date){
			if(count($exclude_path)){
				#exception for ex_list
				foreach($exclude_path as $k => $d){
					if(strpos($file_path, $d)){ 
						$file_exist = true;
						if($file_exist == true){ break; }
					}				
				}
				
				if($file_exist == true){ 
					$file_exist = false;
					continue; 
				}
			}
		
			$item = $dom->createElement("file");
			$root->appendChild($item);

			// create text node
			$modified_date = $dom->createTextNode($date);
			$item->appendChild($modified_date);

			// create attribute node
			$filepath = $dom->createAttribute("file_path");
			$item->appendChild($filepath);

			// create attribute value node
			$pathval = $dom->createTextNode($file_path);
			$filepath->appendChild($pathval);
		}


		$saved = $dom->save($xmlfile);
		chmod($xmlfile,0755);
		unset($dom); 
	}

	function xml_check_element($xmldoc,$element){
		#return true if exist and the modified date is same, but false vice versa 
 		if($xmldoc[$element]){
			$date_modified = filemtime($element);
			if($xmldoc[$element] == $date_modified){
				return true;
			} 
		} 
		
		return false;
	}
	
	function xml_fetchall_data($path){
		#return data (array) if exist and false if not exist
		$data = array();
		$xmldoc = new DOMDocument();
		$xmldoc->load($path);
		$elements = $xmldoc->getElementsByTagName("file");
		
		$xpath = new DOMXPath($xmldoc);
		
		foreach($elements as $elements){
			$file_path = $elements->getAttribute('file_path');
			$date_modified = $elements->textContent;
			$data[$file_path] = $date_modified;
		}
		
		return $data;
		
		return false;
 	}

	function find_all_files($dir){
		$root = scandir($dir);
		foreach($root as $value){
			if($value === '.' || $value === '..'){
				continue;
			}
			
			if(is_file("$dir".DIRECTORY_SEPARATOR."$value")) {
				$result[]="$dir".DIRECTORY_SEPARATOR."$value";
				continue;
			}
			
			foreach(find_all_files("$dir".DIRECTORY_SEPARATOR."$value") as $value){
				$result[]=$value;
			}
		}
		return $result;
	}
	#================================================================================
	
	//update activity statistic
	$cron_option['cron_name'] = 'CRON_BACKUP_FILES';
	cron_activity_log($cron_option);
?>