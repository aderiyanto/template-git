<?php 
	#this file only allow to execute by using cron or cli command
	if(! defined('STDIN') ){
		die('Forbidden Access');
	}

	//includes all files necessary to support operations
	include($argv[1]."/modz/config-main.php");
	include($argv[1]."/modz/config.php");
	include($argv[1]."/modz/license.php");
	include($argv[1]."/modz/errormsg.php");
	include($argv[1]."/modz/mainmod.php");
	include($argv[1]."/modz/connic.php");
	include($argv[1]."/modz/getall-admin.php");
	
	/* 
		This script don't check the available RAM or disk,
		thus, You need to monitor them on the cpanel 
		Max file size can be handled is about 230mb of .sql file
	*/
	
	$fname = $argv[3];
	$cmd = ($argv[2]!='unknown' ? $argv[2].DIRECTORY_SEPARATOR:'') .'mysqldump -u '.getconfig('DB_USER').' -p'.getconfig('DB_PASSWORD').' --add-drop-table --skip-comments '.getconfig('DB_NAME').' > "'.$argv[1].'/assets/backup/'.$fname.'.sql"';
	 
	#this following table list will not lookup, separate by comma
	$ignored_tables = "'counter','onlinenow','backup_log','systemlog'";
	
	#get all changed table
	$sql = "SELECT * FROM backup_log WHERE backupLastUpdate > backupLastUpload AND backupTable NOT IN(".$ignored_tables.") ORDER BY backupTable ASC";
	$query = query($sql);
	$rows = rows($query);
	
	if($rows > 0){
		cli($cmd,FALSE); //dump sql file
	
		#compress the file into zip format
		$tmp=$argv[1]."/assets/backup/".$fname;
		$sql_filename = $fname.'.sql';
		$log_filename = $fname.'.log';
		
		$changes  = false; //table list
		$log_openfile = fopen($argv[1]."/assets/backup/".$log_filename,'w'); // open file firts 
		$title = "#Table Name             Last Updated \n\n";
		fwrite($log_openfile,$title);
		
		while($data = fetch($query)){ 
			$changes .= $data['backupTable'] . '	'.date("d-m-Y H:i:s",$data['backupLastUpdate'])." wib\n"; //table list
		}
		$log_write = fwrite($log_openfile,$changes); // write into the file
		fclose($log_openfile); //write ended
		
		$sqlupdate = "UPDATE backup_log SET backupLastUpload='$now'";
		$queryupdate = query($sqlupdate);
		
		$zip = new ZipArchive();
		$status=$zip->open($tmp.'.zip', ZIPARCHIVE::CREATE) ;

		if ($status=== TRUE) {
			$zip->addFile($tmp.'.sql', $sql_filename);
			$zip->addFile($tmp.'.log', $log_filename); //add log file

			$zip->close();
		}
		
		$f = fopen("{$tmp}.wb",'w');
		fwrite($f,"ok");
		fclose($f); 
	}
	
	//update activity statistic
	$cron_option['cron_name'] = 'CRON_BACKUP_DATABASE';
	cron_activity_log($cron_option);	
?>