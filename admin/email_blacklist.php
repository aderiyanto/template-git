<?php
	$page=17;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

	if ($form=="submit"){
		//Is id exist ?
		if($action=="del"){
			$exist=countdata("email_blacklist","blackId='". (int)$id ."'");
			if($exist<1){
				$emptyid=errorlist(21);
			}
		}

		//get the next id for fileman
		$nextid=nextid("blackId","email_blacklist");

		if ($action=="add" and $uac_add){
			if (empty($blackemail)){
				$error=errorlist(2);
			}else{
				//Validate email firts
				if(!filter_var($blackemail, FILTER_VALIDATE_EMAIL)){
					$error=errorlist(16);
				}else{
					// check whether email exist
					$isexist=countdata("email_blacklist","blackEmail='$blackemail'");
					if($isexist>0){
						$error=errorlist(17);
					}else{
						$sql="INSERT INTO email_blacklist VALUES ('$nextid','$blackemail')";
						$query=query($sql);
					}
				}
			}
		}elseif($action=="del" and $uac_delete){
			$sql="DELETE FROM email_blacklist WHERE blackId='" . $id . "'";
			$query=query($sql);
		}

		//check whether query was successful
		if(!$query){
			$error=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:email_blacklist.php");
			}else{
				print "ok";
			}
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _("email_blacklist_pagetitle"); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('.fancybox').fancybox({
		'arrows': false
	});

	$('.fancybox-frame').fancybox({
		'type'			: 'iframe'
	});
	$('#result').hide();
	validate('#result','#add','email_blacklist.php');
});
</script>
</head>

<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('email_blacklist_pagetitle'); ?></h3>
			                <p><?php print _('email_blacklist_pagedesc'); ?></p>
			            </div>
		            </div>
	        	</div>
        	</div>

        	<?php if ($uac_add){ ?>
        	<div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                	<div class="pull-right">
                	<a href="email_blacklist.php?action=add" class="btn btn-primary"><i class="fa fa-plus"></i> <?php print _('email_blacklist_view_addbtton'); ?></a>
                	</div>
                </div>
        	</div>
        	<?php } ?>

        	<div class="row">
	        	<div class="col-md-12">
	        	<?php if (empty($action)){ ?>
	        		<div class="table-responsive">
            			<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
            				<thead>
            					<tr>
            						<th width="25"><?php print _("cms_no"); ?></th>
							        <th><?php print _("newsletter_view_email"); ?></th>
									<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
            					</tr>
            				</thead>
            				<tbody>
            					<?php
									if (empty($pg)){
										$pg=1;
									}
									$sql="SELECT * FROM email_blacklist ORDER BY blackEmail ASC";
									$query=query($sql);
									$numofdata=rows($query);

									$num=1;
									$numPerPage=20;
									$offset=($pg-1)*$numPerPage;
									$num=$num+(($pg-1)*$numPerPage);

									$sql.=' LIMIT '.$offset.','.$numPerPage;
									$query=query($sql);
										if ($numofdata<1){
								?>
								<tr>
								  	<td colspan="5" class="asterik"><div class="text-center"><br /><?php print _("email_blacklist_view_notavailable"); ?><br /><br /></div></td>
								</tr>
								<?php
									}else{
										while ($data=fetch($query)){
								?>
								<tr>
									<td class="text-center"><?php print $num; ?></td>
									<td><?php print $data['blackEmail']; ?></td>
									<td class="text-center"><?php if ($uac_delete){ ?><a class="delete" href=" email_blacklist.php?form=submit&amp;action=del&amp;id=<?php print $data['blackId']; ?>" title="<?php print stripquote($data['blackEmail']); ?>"><i title="<?php print _('cms_delete'); ?>: <?php print stripquote($data['blackEmail']); ?>" rel="tooltip" class="fa fa-trash-o"></i></a><?php } ?></td>
								</tr>
								<?php
								  			$num++;
								  		}
									}
								?>
            				</tbody>
            			</table>
            		</div>
            		<?php
						$options['total']=$numofdata;
						$options['filename']='email_blacklist.php';
						$options['qualifier']='email_blacklist.php';
						$options['pg']=$pg;
						$options['numPerPage']=$numPerPage;
						$options['style']=1;
						$options['addquery']=TRUE;
						paging($options);
					?>
				<?php } elseif ($action=="add" and $uac_add){ ?>
					<a href="email_blacklist.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('email_blacklist_mainview_addbtton') ?></h4>
					<hr/>
					<form action="email_blacklist.php?action=add&amp;form=submit" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">
						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _('newsletter_view_email'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="blackemail" type="text" id="blackemail" size="40" maxlength="255" required class="form-control" />
	                        </div>
                    	</div>
                    	
                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='email_blacklist.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>
                    	<br/><br/><br/>
					</form>
	        	<?php } ?>
	        	</div>
	        </div>


		</div>
        <!-- END THE CONTENT OF PAGE HERE -->


		<?php include("com/com-footer.php"); ?>

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>