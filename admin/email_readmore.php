<?php
	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");	
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS</title>
<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
</head>

<body class="flyctn" style="background:#F9F9F9;">

<div class="container body">
	<div class="main_container">

		<div class="row">
			<div class="col-md-12">
			<?php
			if($action=="readmore"){
				if($source=='newsletter'){
					$sql="SELECT * FROM newsletter_email_queue WHERE emailId='". $id ."'";
					$query=query($sql);
					$data=fetch($query);
			 
					print "<div class='commenthead'>";
					print "Email : "; 
					print "<strong>".$data['emailTo']."</strong>"; 
					print "<br/>";
				 
					print "Judul : "; 
					print "<strong>".$data['emailSubject']."</strong><br />";
					print "</div>";
					print"<strong>Isi : </strong>";
					print "<div class='commentbody'>";
					print $data['emailMsg'];
					print "</div>";	
				} else {
					$sql = "SELECT * FROM email_queue WHERE emailId='". $id ."'";
					$query = query($sql);
					$data = fetch($query);
					
					list($from, $fromname, $replyto, $cc, $bcc) = explode("#", $data['emailHead']);
					?>
					<div style="background-color:#132537;color:#fff;padding:11px"><h3 style="padding:0;margin:0;">Detail Email</h3></div>
					<div class="clearfix"></div>
					
					<div style="margin-right:10px;margin-top:10px;text-decoration:underline;color:#888" class="pull-right"><b>Tanggal Terkirim: </b><i><?php print $data['emailDateSent'] > 0 ? formatdate('d F Y H:i', $data['emailDateSent']).' WIB':'-';?></i></div>
					
					<div style="padding-top:10px">
						<table cellpadding="5">
						<tr>
							<td><b>Pengirim</b></td>
							<td>: <?php print $from.($fromname ? ' &lt'.$fromname.'&gt':'');?></td>
						</tr>
						<tr>
							<td><b>Penerima</b></td>
							<td>: <?php print $data['emailTo'];?></td>
						</tr>
						<tr>
							<td><b>ReplyTo</b></td>
							<td>: <?php print $replyyo ? $replyyo:'-';?></td>
						</tr>
						<tr>
							<td><b>CC</b></td>
							<td>: <?php print $cc ? $cc:'-';?></td>
						</tr>
						<tr>
							<td><b>BCC</b></td>
							<td>: <?php print $bcc ? $bcc:'-';?></td>
						</tr>
						<tr>
							<td><b>Judul</b></td>
							<td>: <?php print $data['emailSubject'];?></td>
						</tr>
						</table>
						
						<div style="margin-top: 15px;border:1px dashed #ccc;background-color:#fcfcfc;padding:10px 5px;">
							<?php print $data['emailMsg'];?>
						</div>
						
						<?php
						if($data['emailAttachFile']){
							?>
							<div style="float:right;margin-top:10px;">
								<a href="../assets/emailqueue/<?php print $data['emailAttachDir'].'/'.$data['emailAttachFile'];?>" target="blank"><sup><small><?php print $data['emailAttachType'];?></small></sup><br/><img align="absmiddle" src="../assets/images/icon-attachment.png" alt="<?php print $data['emailAttachType'];?>"/> <?php print $data['emailAttachFile'];?></a>
							</div>
							<?php
						}?>
					</div>
					<?php
				}
			}
			?>


			</div>
		</div>
	</div>
</div>

</body>
</html>