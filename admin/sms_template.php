<?php
  $page=49;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

	#check language license
	if($lang and !in_array($lang, $lc_lang) and ($action=='adden' or $action=='editen')){
		header('location:sms_template.php');
		exit;
	}

	if ($form=="submit"){
        if($action=="edit"){
			if(empty($tsms)){
				$error=errorlist(2);
			}else{
				$sql="UPDATE sms_template SET tSMS='$tsms' WHERE tId='".$id."'";
				$query=query($sql);
			}
        }elseif($action=="reset"){
            $sql="SELECT tSMSbak FROM sms_template WHERE tId='".$id."'";
			$query=query($sql);
			$data=fetch($query);
			if(!empty($data['tSMSbak'])){
				$sql="UPDATE sms_template SET tSMS='".$data['tSMSbak']."' WHERE tId='".$id."'";
				$query=query($sql);
			}

			#loop all other lang
			$sqlx="SELECT tSMSbak FROM sms_template_lang WHERE tId='".$id."'";
			$queryx=query($sqlx);
        }
		
		//check whether query was successful
		if(!$query){
			$error=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:sms_template.php");
			}else{
				print "ok";
			}
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _('sms_template_pagetittle'); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce_simple.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
	$('.fancybox').fancybox({
		'arrows': false
	});

	$('.fancybox-frame').fancybox({
		'type'		: 'iframe',
		'arrows'	: false
	});
	$('#result').hide();
	validate('#result','#edit','sms_template.php');
});
</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('template_page_title'); ?></h3>
			                <p><?php print _('template_page_title_desc'); ?></p>
			            </div>
		            </div>
	        	</div>
        	</div>

        	<!-- THE CONTENT OF PAGE HERE -->
        	<div class="row">
	        	<div class="col-md-12">
	        		<?php
						if (empty($action)){
					?>
	        		<div class="table-responsive">
        				<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
        					<thead>
        						<tr>
        							<th width="30"><?php print _('emailtemplate_view_no'); ?></th>
							        <th><?php print _('emailtemplate_view_templatename'); ?></th>
							        <?php if(count($lc_lang)>1){ ?><th style="width:40px;" class="text-center"><i rel="tooltip" title="<?php echo _('multi_language_view_language'); ?>" class="fa fa-language"></i></th><?php } ?>
									<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
									<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
        						</tr>
        					</thead>
        					<tbody>
        						<?php
								  	$sql="SELECT * FROM sms_template ORDER BY tName ASC";
									$query=query($sql);
									$num=1;
									while ($data=fetch($query)){
								  ?>
							      <tr valign="top">
							        <td align="center"><?php print $num; ?></td>
							        <td><?php print $data['tName']; ?></td>
									<?php
										if(count($lc_lang)>1){
											?>
											<td align="center">
												<?php
												foreach($lc_lang as $key=>$value){
													$exist=countdata("sms_template_lang","tId='".$data['tId']."' and lang='$value'");
													if($value==$lc_lang_default) continue;
													if($exist and $uac_edit){
														?><a href="sms_template.php?action=editen&amp;lang=<?php print $value;?>&id=<?php print $data['tId']; ?>" rel="tooltip" title="<?php print _('sms_template_view_title1'); ?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-edit.png" alt="<?php print $value;?>"  border="0" /></a><?php
													}
													elseif($uac_add){
														?><a href="sms_template.php?action=adden&amp;lang=<?php print $value;?>&id=<?php print $data['tId']; ?>" rel="tooltip" title="<?php print _('sms_template_view_title2'); ?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-add.png" alt="<?php print $value;?>" border="0" /></a><?php
													}
												}?>
											</td>
											<?php
										}
									?>
							        <td align="center">
										<a href="javascript:if (window.confirm('<?php print _('emailtemplate_view_statusinfo'); ?>')){ window.location='sms_template.php?form=submit&amp;action=reset&amp;id=<?php print $data['tId']; ?>'; };" rel="tooltip" title="<?php print _('emailtemplate_view_title3'); ?>"><i class="fa fa-refresh"></i></a>
									</td>
									<td align="center"><a href="sms_template.php?action=edit&amp;id=<?php print $data['tId']; ?>" title="<?php print stripquote($data['tName']); ?>"><i rel="tooltip" title="<?php print _('cms_edit'); ?>: <?php print stripquote($data['tName']); ?>" class="fa fa-edit"></i></a></td>
							      </tr>
								<?php
										$num++;
									}
								?>
        					</tbody>
        				</table>
        			</div>
        			<?php
						}elseif ($action=="edit"){
							$sql="SELECT * FROM sms_template WHERE tId='". $id ."'";
							$query=query($sql);
							$data=fetch($query);
							$data=formoutput($data);
					?>
					<a href="sms_template.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _("sms_template_edit_edittemplateversion"); ?></h4>
					<hr/>
					<form method="post" action="sms_template.php?form=submit&amp;action=edit&amp;id=<?php print $id; ?>" name="edit" id="edit" enctype="multipart/form-data" class="form-horizontal form-label-left">
						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">
	                        	<?php print _('emailtemplate_edit_templatename'); ?>
	                        </label>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<p class="form-control-static"><?php print $data['tName']; ?></p>
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">
	                        	<?php print _('sms_template'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-5 col-sm-6 col-xs-12">
	                        	<textarea name="tsms" class="form-control" required><?php print $data['tSMS']; ?></textarea>
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12">
	                        </label>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<a href="email_varguidemsg.php?template=<?php print $id; ?>" class="btn btn-xs btn-info fancybox-frame"><?php print _('sms_template_add_variabelguide'); ?></a>
	                        </div>
                    	</div>
                    	<hr/>
                    	<div class="form-group">
	                        <div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_updatebutton'); ?>"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='sms_template.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>
                    	<br/><br/><br/>
					</form>
        			<?php } ?>
	        	</div>
	        </div>
	        <!-- END THE CONTENT OF PAGE HERE -->


		</div>
		<!-- END THE CONTENT OF PAGE HERE -->

		<?php include("com/com-footer.php"); ?>

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>