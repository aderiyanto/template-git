<?php
	$page=14;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

 	//set the restrictions
	$allowedType=array("image/png");

	if($form=="submit"){
		if (getconfig(MOD_MEMCACHED)==TRUE){
		$memcache->flush(1); 
		}
		//Set name for watermark
		$watermarksrc=$_FILES['sett'];
		foreach($sett as $key=>$value){
			if(empty($value) and $key!=11 and $key!=12 and $key!=13 and $key!=14 and $key!=15 and $key!=16){
				$error=errorlist(2);
			}
		}

		if(!empty($watermarksrc['name']['13'])){
		 //get filename, filetype
		  $fname=$watermarksrc['name']['13'];
			$ftype=$watermarksrc['type']['13'];

			//check if it is really an uploaded file
			if(!is_uploaded_file($watermarksrc['tmp_name']['13'])){
				$errorfile=errorlist(13);
			}

			//make sure it is allowed
			if(!in_array($ftype,$allowedType)){
				$errorfile=errorlist(13);
			}

			//make sure it is really really image
			$img=getimagesize($watermarksrc['tmp_name']['13']);
			if (!(is_numeric($img[0]) or is_numeric($img[1]) or $img[0]>0 or $img[1]>0)){
				$errorfile=errorlist(13);
			}

			list($filename,$ext)=explode(".",$fname);
			if (empty($ext)){
				$errorfile=errorlist(13);
			}
		}

		if($errorfile){
			$error.=$errorfile;
		}

		//Watermark name will always set with static name and even no image
		$sett[13]="watermarkimage.png";
		if(!$error){
			//Is watermark exist ?
			if(!empty($watermarksrc['name']['13'])){
				$targetdir="../assets/images/";
				//Test whether targetdir exists
				if(!opendir($targetdir)){
					mkdir($targetdir);
				}
				chdir($targetdir);
				copy($watermarksrc['tmp_name']['13'],$sett[13]);
				chdir("../");
			}

			foreach($sett as $key=>$value){
				if($key == 25){
					$value = "{$sett[$key]['pos']['x']} {$sett[$key]['pos']['y']},{$sett[$key]['repeat']}";
				}

				$sql="UPDATE systemsetting SET setValue='$value' WHERE setId=" .$key;
				$query=query($sql);
			}

			if($query){
				$sql="SELECT setVar, setValue FROM systemsetting";
				$query=query($sql);
				$written.="\n";
				while ($data=fetch($query)){
					$data=output($data);
					$written.="define(\"" . $data['setVar'] . "\",\"" . $data['setValue'] . "\");\n";
				}

				$writtens.="<?php \nerror_reporting(0);\n";
				$writtens.="date_default_timezone_set(\"Asia/Jakarta\"); \n";
				$writtens.='$now=time();';
				$writtens.="\n";
				$writtens.=stripslashes($written) . "?>";

				//open config.php
				$file="../modz/config.php";
				chmod($file, 0777);
				$fp=fopen($file,"w");
				fwrite($fp,$writtens);
				fclose($fp);
				chmod($file, 0644);
			}
		}

		//check whether query was successful
		if(!$query){
			$error=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:systemsettings.php?s=y");
			}else{
				print "ok";
			}
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _('systemsetting_pagetitle'); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Bootstrap Colorpicker -->
<link href="/libs/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce_simple.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>
<!-- Bootstrap Colorpicker -->
<script src="/libs/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($){
	$('.fancybox').fancybox({
		'arrows': false
	});
	$('.fancybox-frame').fancybox({
		'type': 'iframe'
	});
	
	$('.color').colorpicker();
	$('#result').hide();
	validate('#result','#settings','systemsettings.php?s=y');
});
</script>
</head>

<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('systemsetting_pagetitle'); ?></h3>
			                <p><?php print _('systemsetting_pagedesc'); ?> <?php print SITE_NAME; ?></p>
			            </div>
		            </div>
		            <br/>
		            <?php if ($s=="y"){ ?>
						<div class="alert alert-success alert-dismissible fade in" role="alert">
	                    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                    	</button>
							<i class="fa fa-check-circle"></i> <?php print _('systemsetting_settingsave'); ?>
		                </div>
				    <?php } ?>
	        	</div>
        	</div>

        	<?php if ($uac_add){ ?>
        	<div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                	<div class="pull-right">
                	<a href="file_manager.php?action=add" class="btn btn-primary"><i class="fa fa-plus"></i> <?php print _('file_manager_view_addbutton'); ?></a>
                	</div>
                </div>
        	</div>
        	<?php } ?>

        	<div class="row">
	        	<div class="col-md-12">
	        		<form name="settings" id="settings" method="post" <?php if($uac_edit){ ?> action="systemsettings.php?form=submit" <?php } ?> enctype="multipart/form-data" class="form-horizontal form-label-left">
	        			<?php
						$sql="SELECT * FROM systemsetting WHERE NOT FIND_IN_SET(setId,'7') AND setId<>'9' AND setStatus='on' ORDER BY setSort ASC";
						$query=query($sql);
						while($data=fetch($query)){
						$data=formoutput($data);
							
							#comment moderation
							if($data['setId'] == 20 and lcauth(34) === false){
								continue;
							}
							?>
							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-12">
		                        	<?php print _($data['setName']) . ($data['setId']==11 || $data['setId']==12 || $data['setId']==13 || $data['setId']==14 || $data['setId']==15 || $data['setId']==16 || $data['setId']==17 || $data['setId']==18 || $data['setId']==21? '':' <span class="required">*</span>'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
	                    	<?php

							if($data['setId']==1 or $data['setId']==2 or $data['setId']==8) {
								print "<input type=\"text\" name=\"sett[" .$data['setId'] ."]\" value=\"" . $data['setValue'] ."\" size=\"55\" maxlength=\"255\" required class=\"form-control".($data['setId']==9 ? ' email':'')."\"/>";
							}

							if($data['setId']==3 or $data['setId']==4 or $data['setId']==10) {
								print "<textarea name=\"sett[" .$data['setId'] ."]\" cols=\"55\" rows=\"3\" class=\"form-control\" " . ($data['setId']==6 or $data['setId']==12 ? 'required ':'') . "maxlength=\"255\">".$data['setValue']."</textarea>";
							}

							if($data['setId']==15 or $data['setId']==16) {
								print "<textarea name=\"sett[" .$data['setId'] ."]\" cols=\"55\" rows=\"3\" class=\"form-control\">".$data['setValue']."</textarea>";
							}

							if($data['setId']==6 /* || $data['setId']==7 */) {
								print "<select style=\"padding:2px;\" class=\"form-control\" required name=\"sett[" .$data['setId'] ."]\"><option".($data['setValue']=='on'?' selected="selected"':'')." value='on'>On</option><option".($data['setValue']=='off'?' selected="selected"':'')." value='off'>Off</option>
								</select>";
							}

							if($data['setId']==11 or $data['setId'] == "21" or $data['setId'] == "22") {
								print "<input type=\"text\" name=\"sett[" .$data['setId'] ."]\" value=\"" .$data['setValue'] ."\" size=\"55\" maxlength=\"255\" class=\"form-control\" ".($data['setId'] != 11 ? "required":"")." />
								<a class=\"btn btn-default btn-xs fancybox-frame\" href=\"imgfly.php\"><i class=\"fa fa-upload\"></i> ". _('pagecontent_uploadimg') . "</a><br/><br/>";
							}

							if($data['setId']==12) {
								print "<input type=\"text\" name=\"sett[" .$data['setId'] ."]\" id=\"triggerSet\" size=\"6\" maxlength=\"255\" value=\"" .$data['setValue'] ."\" style=\"height:23px;\" class=\"form-control\">";
							}

							if($data['setId']==13) {
									print "<img src=\"/assets/images/watermarkimage.png\" alt=\"watermarkimage\" title=\"Watermark\" style=\"background-color: gray; margin-bottom:8px;\">
									<br />
									<input type=\"file\" name=\"sett[" .$data['setId'] ."]\" size=\"55\" maxlength=\"255\">
									<span class=\"help-block\">" . _("systemsetting_watermarkimagedesc") . "</span>";
							}

							if($data['setId']==17 or $data['setId']==18  or $data['setId']==20 ) {
								print "<select style=\"padding:2px;\" required class=\"form-control\" name=\"sett[" .$data['setId'] ."]\"><option".($data['setValue']=='on'?' selected="selected"':'')." value=\"on\">On</option><option".($data['setValue']=='off'?' selected="selected"':'')." value=\"off\">Off</option>
								</select>";
							}

							if($data['setId']==19){
								$img_quality = array(
									'Low' => 70,
									'Medium' => 75,
									'High' => 80,
									'Very High' => 85,
									'Maximum' => 90
								);

								?><select name="sett[<?php print $data['setId']; ?>]" class="form-control"><?php
								foreach($img_quality as $key=>$value){
									print '<option value="'.$value.'"'.($value==$data['setValue'] ? ' selected="selected"':'').'>'.$key.'</option>';
								}
								?></select><?php
							}

							if($data['setId']==25) {
								@list($bgpos, $bgrepeat) = @explode(',', $data['setValue']);
								@list($bgpos_x, $bgpos_y) = @explode(' ', $bgpos);

								print "<div style=\"margin-right:10px;overflow:hidden;display:inline-block;text-align:center;font-weight:bold;background-color:#f3f3f3;border:1px solid #333;\">
									<div style=\"background-color:#006C80;color:#fff;padding:4px;\">Horizontal Position</div>
									<div style=\"padding:8px\">
										<select name=\"sett[".$data['setId']."][pos][x]\" class=\"form-control\">
											<option value=\"left\"".($bgpos_x == 'left' ? ' selected="selected"':'').">Left</option>
											<option value=\"center\"".($bgpos_x == 'center' ? ' selected="selected"':'').">Center</option>
											<option value=\"right\"".($bgpos_x == 'right' ? ' selected="selected"':'').">Right</option>
										</select>
									</div>
									</div>";

								print "<div style=\"margin-right:10px;overflow:hidden;display:inline-block;text-align:center;font-weight:bold;background-color:#f3f3f3;border:1px solid #333;\">
									<div style=\"background-color:#006C80;color:#fff;padding:4px;\">Vertical Position</div>
									<div style=\"padding:8px\">
										<select name=\"sett[" .$data['setId'] ."][pos][y]\" class=\"form-control\">
											<option value=\"top\"".($bgpos_y == 'top' ? ' selected="selected"':'').">Top</option>
											<option value=\"center\"".($bgpos_y == 'center' ? ' selected="selected"':'').">Center</option>
											<option value=\"bottom\"".($bgpos_y == 'bottom' ? ' selected="selected"':'').">Bottom</option>
										</select>
									</div>
									</div>";

								print "<div style=\"margin-right:10px;overflow:hidden;display:inline-block;text-align:center;font-weight:bold;background-color:#f3f3f3;border:1px solid #333;\">
									<div style=\"background-color:#006C80;color:#fff;padding:4px;\">Background Repeat</div>
									<div style=\"padding:8px\">
										<select name=\"sett[" .$data['setId'] ."][repeat]\" class=\"form-control\">
											<option value=\"repeat\"".($bgrepeat == 'repeat' ? ' selected="selected"':'').">Repeat</option>
											<option value=\"no-repeat\"".($bgrepeat == 'no-repeat' ? ' selected="selected"':'').">No Repeat</option>
											<option value=\"repeat-y\"".($bgrepeat == 'repeat-y' ? ' selected="selected"':'').">Repeat Vertical</option>
											<option value=\"repeat-x\"".($bgrepeat == 'repeat-x' ? ' selected="selected"':'').">Repeat Horizontal</option>
										</select>
									</div>
									</div>";
							}

							if($data['setId']==14) {
								print "<select style=\"padding:2px;\" name=\"sett[" .$data['setId'] ."]\" class=\"form-control\"><option".($data['setValue']=='1'?' selected="selected"':'')." value='1'>"._('top_left')."</option>
									<option".($data['setValue']=='2'?' selected="selected"':'')." value='2'>"._('top_middle')."</option>
									<option".($data['setValue']=='3'?' selected="selected"':'')." value='3'>"._('top_right')."</option>
									<option".($data['setValue']=='4'?' selected="selected"':'')." value='4'>"._('middle')."</option>
									<option".($data['setValue']=='5'?' selected="selected"':'')." value='5'>"._('bottom_left')."</option>
									<option".($data['setValue']=='6'?' selected="selected"':'')." value='6'>"._('bottom_middle')."</option>
									<option".($data['setValue']=='7'?' selected="selected"':'')." value='7'>"._('bottom_right')."</option>
									</select>";
							}

							if($data['setRemark']) {
								print "<span class=\"help-block\">". _($data['setRemark'])."</span>";
							}
							?>
							</div>
	                    </div>
							<?php
						}
						?>
						<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_updatebutton'); ?>" <?php if(!$uac_edit){ print "disabled";} ?>><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>
                    	<br/><br/><br/>
	        		</form>
	        	</div>
	        </div>



		</div>
        <!-- END THE CONTENT OF PAGE HERE -->


		<?php include("com/com-footer.php"); ?>

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>