<?php
	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");

	#form guard
	$fguard = guardian('init', 'login');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php print SITE_NAME; ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- iCheck -->
<link href="/libs/iCheck/skins/flat/green.css" rel="stylesheet">
<!-- bootstrap-progressbar -->
<link href="/libs/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
<!-- JQVMap -->
<link href="/libs/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
<!-- bootstrap-daterangepicker -->
<link href="/libs/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>

<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($){
	$('#resultnopadding').hide();
	validate('#resultnopadding', '#login', 'index.php', '<?php print $fguard;?>');
});
</script>
</head>
<body class="login" onload="document.login.username.focus()">
<noscript class="noscript">Required javascript.</noscript>
<div id="resultnopadding"></div>
<div>
  <div class="login_wrapper">
    <div class="animate form login_form">
      <section class="login_content">
        <form action="loginp.php" method="post" name="login" id="login">
          
          <h1>CMS Website <?php print SITE_NAME; ?></h1>
          <?php
            if (!empty($error)){
          ?>
            <div class="alert alert-danger alert-dismissible fade in" role="alert" style="font-weight:normal; color:#fff; text-shadow:none;">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              <i class="fa fa-exclamation-triangle"></i> <?php print $error; ?>
            </div>
          <?php
            }
          ?>
          <div>
            <input type="text" class="form-control" placeholder="Username" name="username" id="username" size="20" required="" />
          </div>
          <div>
            <input type="password" class="form-control" placeholder="Password" name="password" id="password" size="20" autocomplete="off" required="" />
          </div>
          <div>
            <button class="btn btn-default submit" value="Login" name="Submit" type="submit">Log in</button>
            <button class="btn btn-default" type="reset" id="Reset" value="Reset" name="Reset">Reset</button>
          </div>

          <div class="clearfix"></div>

          <div class="separator">
            <div>
              <p>©<?php echo date("Y"); ?> All Rights Reserved.</p>
            </div>
          </div>
        </form>
      </section>
    </div>
  </div>
</div>

</body>
</html>