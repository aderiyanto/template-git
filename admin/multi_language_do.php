<?php
	$page=47;
	
	//includes all files necessary to support operations
	$basepath = realpath(dirname(__file__).'/..');
	include($basepath."/modz/config-main.php");
	include($basepath."/modz/config.php");
	include($basepath."/modz/license.php");
	include($basepath."/modz/errormsg.php");
	include($basepath."/modz/mainmod.php");
	include($basepath."/modz/connic.php");
	include($basepath."/modz/getall-admin.php");
	include($basepath."/modz/msgfmt/msgfmt-functions.php");
	
	#parse string into variable
	parse_str(str_replace('#', '&', $argv[1]));
	
	if($cmd == 'generate'){
		foreach($lc_lang as $key=>$value){
			$str = '';
			$str_dummy = '';
			$str_original = '';
			$varId = array();
			$filepath = $basepath."/lang/$value/LC_MESSAGES";
			
			$po = fopen("$filepath/messages.po", 'w');
			$po_dummy = fopen("$filepath/messages_dummy.po", 'w');

			$str .= 'msgid ""'."\r\n";
			$str .= 'msgstr ""'."\r\n";
			$str .= '"Project-Id-Version: WebbyEmedia\n"'."\r\n";
			$str .= '"POT-Creation-Date: 2013-03-07 16:04+0700\n"'."\r\n";
			$str .= '"PO-Revision-Date: '.date('Y-m-d H:i').'+0700\n"'."\r\n";
			$str .= '"Last-Translator: '.$cook_username.' <webmaster@webbyemedia.net>\n"'."\r\n";
			$str .= '"Language-Team: WebbyEmedia Developer Team\n"'."\r\n";
			$str .= '"MIME-Version: 1.0\n"'."\r\n";
			$str .= '"Content-Type: text/plain; charset=UTF-8\n"'."\r\n";
			$str .= '"X-Generator: Poedit 1.5.5\n"'."\r\n";
			$str .= '"X-Poedit-KeywordsList: _;gettext;gettext_noop\n"'."\r\n";
			$str .= '"Language: '.$value.'\n"'."\r\n";
			$str .= '"X-Poedit-SourceCharset: UTF-8\n"'."\r\n\r\n";
			
			$sql = "SELECT * FROM multi_languages_group ORDER BY groupPrefix ASC";
			$query = query($sql);
			while($data = fetch($query)){
				if($data['groupPrefix'] == "_"){
					$whereClause = "WHERE SUBSTRING(multi_languages_var.varVar,1,1)='_'";
				} else {
					$whereClause = "WHERE multi_languages_var.varVar LIKE '{$data['groupPrefix']}%'";
				}
				
				$str_original .= "# {$data['groupName']}\r\n";
				$str_original .= "# ===============================\r\n";
				
				$str_dummy .= "# {$data['groupName']}\r\n";
				$str_dummy .= "# ===============================\r\n";
				
				$sqlx = "SELECT * FROM multi_languages_var INNER JOIN multi_languages_value ON multi_languages_var.varId=multi_languages_value.varId AND multi_languages_value.langCode='$value' $whereClause ORDER BY varVar ASC";
				$queryx = query($sqlx);
				while($datax = fetch($queryx)){
					if(in_array($datax['varId'], $varId) or empty($datax['valValue'])){
						continue;
					}
					
					$varId[] = $datax['varId'];
					
					if($datax['varDesc']){
						$str_original .= "# {$datax['varDesc']}\r\n";
					}
					
					$str_original .= 'msgid "'.$datax['varVar'].'"'."\r\n";
					$str_original .= 'msgstr "'.addslashes($datax['valValue']).'"'."\r\n\r\n";
					
					$str_dummy .= "# \r\n";
					$str_dummy .= 'msgid "'.$datax['varVar'].'"'."\r\n";
					$str_dummy .= 'msgstr "'.addslashes($datax['valValue']).'"'."\r\n\r\n";
				}
				
				$str_original .= "# End Of {$data['groupName']}\r\n";
				$str_original .= "# ===============================\r\n\r\n";
				
				$str_dummy .= "# End Of {$data['groupName']}\r\n";
				$str_dummy .= "# ===============================\r\n\r\n";
			}
			
			$sql = "SELECT * FROM multi_languages_var INNER JOIN multi_languages_value ON multi_languages_var.varId=multi_languages_value.varId AND multi_languages_value.langCode='$value' WHERE multi_languages_var.varId NOT IN('".implode(',', array_unique($varId))."') ORDER BY varVar ASC";
			$query= query($sql);
			while($data = fetch($query)){
				if(empty($data['valValue'])){
					continue;
				}
					
				if($data['varDesc']){
					$str_original .= "# {$data['varDesc']}\r\n";
				}
				
				$str_original .= 'msgid "'.$data['varVar'].'"'."\r\n";
				$str_original .= 'msgstr "'.addslashes($data['valValue']).'"'."\r\n\r\n";
				
				$str_dummy .= "# \r\n";
				$str_dummy .= 'msgid "'.$data['varVar'].'"'."\r\n";
				$str_dummy .= 'msgstr "'.addslashes($data['valValue']).'"'."\r\n\r\n";
			}
			
			fwrite($po_dummy, $str.$str_dummy);
			fclose($po_dummy);
			
			fwrite($po, $str.$str_original);
			fclose($po);
			
			#COMPILE MESSAGE.PO TO MESSAGE.MO
			$po = "$filepath/messages_dummy.po";
			$mo = "$filepath/messages.mo";
			
			$hash = parse_po_file($po);
			if ($hash === FALSE) {
				print("Error reading '{$po}', aborted.\n");
			}
			else {
				write_mo_file($hash, $mo);
				unlink($po);
			}
		}
	}