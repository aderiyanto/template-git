<?php
	$page=1;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

	#check language license
	if($lang and !in_array($lang, $lc_lang) and ($action=='adden' or $action=='editen')){
		header('location:pagecontent.php');
		exit;
	}

	if($action=='add' and !lcauth('addpage')){
		header('location:pagecontent.php');
		exit;
	}
	
	if ($form=="submit"){
		$nextid=nextid("contentId","content");
		if ($action=="add" and $uac_add){
			$sqladmin="SELECT COUNT( adminId ) AS adminExist
						FROM admin_group a, admin b
						WHERE b.groupId =1
						AND b.adminId =$cook_id AND b.groupId=a.groupId";
			$queryadmin=query($sqladmin);
			$dataadmin=fetch($queryadmin);
			
			if($dataadmin['adminExist']==1){
				if($status=='y'){
					$status="default";
				}else{
					$status="added";
				}
			}else{
				$status="added";
			}	
			 
			if(empty($title)){
				$error=errorlist(2);
			}else{
				$permalink=permalink($title);
				$exist=checkDuplicateRecord('content',"contentPermalink='$permalink'");
				
				if($exist){
					$error.=errorlist(51);
				}
			 
				if(!$error){
					$sql="INSERT INTO content VALUES ($nextid,'$title','$label','$permalink','$desc','$descseo','$keywordseo','$status','$display','$sitemap')";
					$query=query($sql);
				}
			}
		}elseif($action=="edit" and $uac_edit){
			if (empty($title) or empty($permalink)){
				$error=errorlist(2);
			}else{
				$exist=checkDuplicateRecord('content',"contentPermalink='$permalink' and contentId!=$pid");
				if($exist){
					$error.=errorlist(51);
				}
				
				if ($display){
					$wheredisplay=" ,contentDisplay='$display'";
				}
				
				$sqladmin="SELECT COUNT( adminId ) AS adminExist
						FROM admin_group a, admin b
						WHERE b.groupId =1
						AND b.adminId =$cook_id AND b.groupId=a.groupId";
				$queryadmin=query($sqladmin);
				$dataadmin=fetch($queryadmin);
				
				if($dataadmin['adminExist']==1){
					if($status=='y'){
						$status="default";
					}else{
						$status="added";
					}
					$contentstatus ="contentStatus='$status',";
				}
				
				if (!$error){
					$sql="UPDATE content SET $contentstatus contentTitle='$title', contentLabel='$label', contentPermalink='$permalink',contentDesc='$desc',contentDescSeo='$descseo',contentKeywordSeo='$keywordseo', contentSitemap='$sitemap' $wheredisplay WHERE contentId='$pid'";
					$query=query($sql);
				}
			}
		}elseif($action=="del" and $uac_delete){
			$sql="SELECT contentStatus FROM content WHERE contentId=$pid";
			$query=query($sql);
			$status=fetch($query);
			$status=$status['contentStatus'];

			if($status!="added"){
				$error=errorlist(10);
			}

			$sql="DELETE FROM content WHERE contentId='$pid'";
			$query=query($sql);
			
			if($query){
				$sql="DELETE FROM content_lang WHERE contentId='$pid'";
				$query=query($sql);
			}
			
		}elseif($action=="adden" and $uac_add){
			if(empty($title)){
				$error=errorlist(2);
			}else{
				$sql="INSERT INTO content_lang VALUES ($pid,'$title','$label','$desc','$descseo','$keywordseo','$lang')";
				$query=query($sql);
			}
		}elseif($action=="editen" and $uac_add){
			if(empty($title)){
				$error=errorlist(2);
			}else{
				$sql="UPDATE content_lang SET contentTitle='$title','$label', contentDesc='$desc', contentDescSeo='$descseo', contentKeywordSeo='$keywordseo' WHERE contentId='$pid' AND lang='$lang'";
				$query=query($sql);
			}
		}

		//check whether query was successful
		if(!$query){
			$error=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}
		else{
			if(!$js){
				header("location:pagecontent.php");
			}else{
				print "ok";
			}
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?>- CMS - <?php print _('pagecontent_pagetitle'); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>

<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>
<!-- jQuery Tags Input -->
<script type="text/javascript" src="/libs/jquery.tagsinput/src/jquery.tagsinput.js"></script>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('.fancybox-frame').fancybox({
		'type'			: 'iframe'
	});

	$('#result').hide();
	$('#keywordseo').tagsInput({
		width: 'auto'
	});

	validate('#result','#add','pagecontent.php');
	validate('#result','#edit','pagecontent.php');
});
</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">
        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('pagecontent_pagetitle'); ?></h3>
			                <p><?php print _('pagecontent_pagedesc'); ?></p>
			            </div>
		            </div>
	        	</div>
        	</div>
        	<?php 
        	if ($uac_add and lcauth('addpage')){ ?>
        	<div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 text-right" style="margin-bottom: 10px;">
                	<a href="pagecontent.php?action=add" class="btn btn-primary"><i class="fa fa-plus"></i> <?php print _('pagecontent_addpage'); ?></a>
                </div>
        	</div>
        	<?php } ?>
        	<div class="row">
	        	<div class="col-md-12">
	                <div class="x_content">
	        		<?php if (empty($action)){ ?>
	        			<div class="table-responsive">
		        			<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
		        				<thead>
									<tr class="tablehead">
										<th style="width:25px;" class="text-center"><?php print _('cms_no'); ?></th>
										<th><?php print _('pagecontent_titlepage') ?></th>
										<th><?php print _('pagecontent_labelpage') ?></th>
										<th style="width:100px;" class="text-center"><?php print _('pagecontent_view'); ?></th>
										<?php if(count($lc_lang)>1){ ?><th style="width:40px;" class="text-center"><i rel="tooltip" title="<?php echo _('multi_language_view_language'); ?>" class="fa fa-language"></i></th><?php } ?>
										<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
										<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
									</tr>
								</thead>
								<tbody>
								<?php
									$num=1;
									$sql="SELECT * FROM content ORDER BY contentTitle ASC";
									$query=query($sql);
									$numofdata=rows($query);
									if ($numofdata<1){
								?>
									<tr>
										<td colspan="6"><div class="text-center"><br /><?php print _('pagecontent_pagenotavailable'); ?><br /><br /></div></td>
									</tr>
								<?php
									}else{
										while ($data=fetch($query)){
											$title=$data['contentTitle'];
											$title=output($title);
											$pid=$data['contentId'];
								?>
									<tr valign="top">
										<td class="text-center"><?php print $num; ?></td>
										<td>
											<a href="pagecontent.php?pid=<?php print $pid; ?>&amp;action=preview" title="Preview: <?php print stripquote($title); ?>"><strong><?php print $title; ?></strong></a>
											<?php if ($data['contentStatus']=="added"){ ?><br /><a style="font-weight:normal;color:#333;" target="_blank" href="/page/<?php print $data['contentPermalink'];?>/">http://<?php print getconfig('SITE_URL'); ?>/page/<?php print $data['contentPermalink'];?>/</a><?php } ?>
										</td>
										<td>
											<?php print $data['contentLabel'];?>
										</td>
										<td class="text-center"><?php print ($data['contentDisplay']=="y")?"<i class=\"text-success fa fa-check\"></i>":"<i class=\"text-danger fa fa-times\"></i>"; ?></td>
										<?php
										if(count($lc_lang)>1){
											?>
											<td class="text-center">
												<?php
												foreach($lc_lang as $key=>$value){
													$exist=countdata("content_lang","contentId='$pid' and lang='$value'");
													if($value==$lc_lang_default) continue;
													if($exist and $uac_edit){
														?><a href="pagecontent.php?action=editen&amp;lang=<?php print $value;?>&pid=<?php print $pid; ?>" rel="tooltip" title="<?php print _('pagecontent_editvers'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-edit.png" alt="<?php print $value;?>"  border="0" /></a><?php 
													}
													elseif($uac_add){ 
														?><a href="pagecontent.php?action=adden&amp;lang=<?php print $value;?>&pid=<?php print $pid; ?>" rel="tooltip" title="<?php print _('pagecontent_addvers'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-add.png" alt="<?php print $value;?>" border="0" /></a><?php 
													}
												}?>
											</td>
											<?php
										}?>
										<td class="text-center">
											<?php if ($uac_edit){ ?><a href="pagecontent.php?pid=<?php print $pid; ?>&amp;action=edit" title="<?php print _('cms_edit'); ?>: <?php print stripquote($title); ?>" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a><?php } ?>
										</td>
										<td class="text-center">
											<?php if($data['contentStatus']=="added" AND $uac_delete){ ?><a href="pagecontent.php?form=submit&amp;action=del&amp;pid=<?php print $data['contentId']; ?>" title="<?php print stripquote($title); ?>" class="delete"><i rel="tooltip" title="<?php print _('cms_delete'); ?>: <?php print stripquote($title); ?>" class="fa fa-trash-o"></i></a><?php } ?>
										</td>
									</tr>
									<?php
											$num++;
										}
									}
									?>
								<tbody>
							</table>
						</div>
					<?php } if($action=="preview"){
						$sql="SELECT * FROM content WHERE contentId='$pid'";
						$query=query($sql);
						$data=fetch($query);
						$data=output($data);
					?>
						<a href="pagecontent.php" class="btn btn-default btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

						<h4 class="green"><?php print _('pagecontent_idnvers'); ?>: <?php print $data['contentTitle']; ?></h4>
						<div style="background-color: #efefef; border: 1px dashed #c4c4c4; padding: 10px; overflow: auto">
							<?php print $data['contentDesc']; ?>
						</div>
						<div style="background-color: #efefef; border: 1px dashed #c4c4c4; padding: 10px; overflow: auto;margin-top:10px;">
							<strong><?php print _('pagecontent_descpageseo'); ?>:</strong><br/><?php print $data['contentDescSeo']; ?><br /><br/>
							<strong><?php print _('pagecontent_keywordseo'); ?>:</strong><br/><?php print $data['contentKeywordSeo']; ?>
						</div>
						<br/>
						<hr/>
						<?php
							$sql="SELECT * FROM content_lang WHERE contentId='$pid'";
							$query=query($sql);
							$exist=rows($query);
							if($exist){
								$data=fetch($query);
								$data=output($data);
						?>	
							<h4 class="green"><?php print _('pagecontent_envers'); ?>: <?php print $data['contentTitle']; ?></h4>
							<div style="background-color: #efefef; border: 1px dashed #c4c4c4; padding: 10px; overflow: auto">
								<?php print $data['contentDesc']; ?> 
							</div>
							<div style="background-color: #efefef; border: 1px dashed #c4c4c4; padding: 10px; overflow: auto;margin-top:10px;">
								<strong><?php print _('pagecontent_descpageseo'); ?>:</strong><br/> <?php print $data['contentDescSeo']; ?><br/><br/>
								<strong><?php print _('pagecontent_keywordseo'); ?>:</strong><br/> <?php print $data['contentKeywordSeo']; ?>
							</div>
						<?php }else{ ?>
							<h4 class="green"><?php print _('pagecontent_envers'); ?>: </h4><span><?php print _('pagecontent_versennotavailable'); ?></span>
						<?php } ?>
						<br/><br/><a href="pagecontent.php" class="btn btn-default btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a>

					<?php } elseif ($action=="add" and $uac_add){ ?>
						<a href="pagecontent.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
						<h4><?php print _('pagecontent_addpage') ?></h4>
						<hr/>
						<form action="pagecontent.php?action=add&form=submit" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">

							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="title"><?php print _('pagecontent_titlepage'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input id="title" required class="form-control col-md-7 col-xs-12" name="title" type="text">
		                        	<span class="help-block"><?php print _('pagecontent_displayasmenu'); ?></span>
		                        </div>
	                    	</div>
	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="title"><?php print _('pagecontent_labelpage'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input id="label" required class="form-control col-md-7 col-xs-12" name="label" type="text">
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="desc"><?php print _('pagecontent_contentspage'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="desc" id="desc" class="tinymce form-control col-md-7 col-xs-12"></textarea>
		                        	<a class="btn btn-default btn-xs fancybox-frame" href="imgfly.php"><i class="fa fa-upload"></i> <?php print _('pagecontent_uploadimg'); ?></a><br/><br/>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="descseo"><?php print _('pagecontent_descpageseo'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="descseo" id="descseo" maxlength="255" class="form-control col-md-7 col-xs-12"></textarea>
		                        	<span class="help-block"><?php print _('pagecontent_descpageseo_info'); ?></span>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="keywordseo"><?php print _('pagecontent_keywordseo'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="keywordseo" id="keywordseo" maxlength="255" class="form-control col-md-7 col-xs-12"></textarea>
		                        	<span class="help-block"><?php print _('pagecontent_keywordseo_info'); ?></span>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12"><?php print _('pagecontent_displayasmenu'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<label class="checkbox-inline"><input name="display" type="radio" class="checkradio" id="radio" value="y" checked="checked" /> <?php print _('pagecontent_yes'); ?></label>
									<label class="checkbox-inline"><input name="display" type="radio" class="checkradio" id="radio2" value="n" /> <?php print _('pagecontent_no'); ?></label>
		                        </div>
	                    	</div>
	                    	<?php   
								$sqladmin="SELECT COUNT( adminId ) AS adminExist
													FROM admin_group a, admin b
													WHERE b.groupId =1
													AND b.adminId =$cook_id AND b.groupId=a.groupId";
								$queryadmin=query($sqladmin);
								$dataadmin=fetch($queryadmin);
								
								if($dataadmin['adminExist']==1){
							?>
								<div class="form-group">
			                        <label class="control-label col-md-2 col-sm-3 col-xs-12"><?php print _('pagecontent_statpage'); ?>
			                        </label>
			                        <div class="col-md-6 col-sm-6 col-xs-12">
			                        	<label class="checkbox-inline"><input name="status" type="radio" class="checkradio" id="status1" value="y" checked="checked" /> <?php print _('pagecontent_default'); ?></label>
										<label class="checkbox-inline"><input name="status" type="radio" class="checkradio" id="status2" value="n" /> <?php print _('pagecontent_added'); ?></label>
			                        </div>
		                    	</div>
							<?php } ?>
							<?php if($dataadmin['adminExist']==1){ ?>
								<div class="form-group">
			                        <label class="control-label col-md-2 col-sm-3 col-xs-12"><?php print _('pagecontent_displayassitemap'); ?>
			                        </label>
			                        <div class="col-md-6 col-sm-6 col-xs-12">
			                        	<label class="checkbox-inline"><input name="sitemap" type="radio" class="checkradio" id="radio" value="y" /> <?php print _('pagecontent_yes'); ?></label>
										<label class="checkbox-inline"><input name="sitemap" type="radio" class="checkradio" id="radio2" value="n" checked="checked" /> <?php print _('pagecontent_no'); ?></label>
			                        </div>
		                    	</div>
							<?php
								}
							?>
							<hr/>
							<div class="form-group">
		                        <div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="Submit"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
		                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='pagecontent.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                        </div>
	                    	</div>
	                    	<br/><br/><br/>
						</form>
					<?php } elseif ($action=="edit" and $uac_edit){
						$sql="SELECT * FROM content WHERE contentId='$pid'";
						$query=query($sql);
						$data=fetch($query);
						$data=formoutput($data);
					?>
						<a href="pagecontent.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
						<h4><?php print _('pagecontent_editpage') ?></h4>
						<hr/>
						<form action="pagecontent.php?action=edit&amp;form=submit&amp;pid=<?php print $pid; ?>" method="post" enctype="multipart/form-data" name="edit" id="edit" class="form-horizontal form-label-left">

							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="title"><?php print _('pagecontent_titlepage'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input id="title" required class="form-control col-md-7 col-xs-12" name="title" type="text" value="<?php print $data['contentTitle'] ?>">
		                        	<span class="help-block"><?php print _('pagecontent_displayasmenu'); ?></span>
		                        </div>
	                    	</div>
	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="title"><?php print _('pagecontent_labelpage'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input id="title" required class="form-control col-md-7 col-xs-12" name="label" type="text" value="<?php print $data['contentLabel'] ?>">
		                        </div>
	                    	</div>


	                    	<?php
							$sqladmin="SELECT COUNT( adminId ) AS adminExist
													FROM admin_group a, admin b
													WHERE b.groupId=1
													AND b.adminId='$cook_id' AND b.groupId=a.groupId";
							$queryadmin=query($sqladmin);
							$dataadmin=fetch($queryadmin);								
							if($dataadmin['adminExist']==1){
							?>
							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="permalink"><?php print _('pagecontent_permalink'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input required class="form-control col-md-7 col-xs-12" name="permalink" id="permalink" type="text"  value="<?php print $data['contentPermalink'] ?>">
		                        </div>
	                    	</div>
                            <?php	
							}else{
								?>
                                <input name="permalink" type="hidden" value="<?php print $data['contentPermalink'] ?>" />
                                <?php	
							}
							?>

							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="desc"><?php print _('pagecontent_contentspage'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="desc" id="desc" class="tinymce form-control col-md-7 col-xs-12"><?php print $data['contentDesc'] ?></textarea>
		                        	<a class="btn btn-default btn-xs fancybox-frame" href="imgfly.php"><i class="fa fa-upload"></i> <?php print _('pagecontent_uploadimg'); ?></a><br/><br/>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="descseo"><?php print _('pagecontent_descpageseo'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="descseo" id="descseo" maxlength="255" class="form-control col-md-7 col-xs-12"><?php print $data['contentDescSeo'] ?></textarea>
		                        	<span class="help-block"><?php print _('pagecontent_descpageseo_info'); ?></span>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="keywordseo"><?php print _('pagecontent_keywordseo'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="keywordseo" id="keywordseo" maxlength="255" class="form-control col-md-7 col-xs-12"><?php print $data['contentKeywordSeo'] ?></textarea>
		                        	<span class="help-block"><?php print _('pagecontent_keywordseo_info'); ?></span>
		                        </div>
	                    	</div>

	                    	<?php if ($data['contentStatus']=="added"){ ?>
	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12"><?php print _('pagecontent_displayasmenu'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<label class="checkbox-inline"><input name="display" type="radio" class="checkradio" id="radio" value="y"<?php print ($data['contentDisplay']=="y")?"  checked=\"checked\"":""; ?> /> <?php print _('pagecontent_yes'); ?></label>
									<label class="checkbox-inline"><input name="display" type="radio" class="checkradio" id="radio2" value="n"<?php print ($data['contentDisplay']=="n")?"  checked=\"checked\"":""; ?> /> <?php print _('pagecontent_no'); ?></label>
		                        </div>
	                    	</div>
	                    	<?php } ?>
	                    	<?php if($dataadmin['adminExist']==1){ ?>
	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12"><?php print _('pagecontent_statpage'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<label class="checkbox-inline"><input name="status" type="radio" class="checkradio" id="status1" value="y"<?php print $data['contentStatus']=="default" ? " checked=\"checked\"":"" ?> /> <?php print _('pagecontent_default'); ?></label>
									<label class="checkbox-inline"><input name="status" type="radio" class="checkradio" id="status2" value="n"<?php print $data['contentStatus']=="added" ? " checked=\"checked\"":"" ?> /> <?php print _('pagecontent_added'); ?></label>
		                        </div>
	                    	</div>
	                    	<?php } ?>
	                    	<?php if($dataadmin['adminExist']==1){ ?>
	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12"><?php print _('pagecontent_displayassitemap'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<label class="checkbox-inline"><input name="sitemap" type="radio" class="checkradio" id="radio" value="y"<?php print $data['contentSitemap']=="y" ? " checked=\"checked\"":""; ?> /> <?php print _('pagecontent_yes'); ?></label>
									<label class="checkbox-inline"><input name="sitemap" type="radio" class="checkradio" id="radio2" value="n" <?php print $data['contentSitemap']=="n" ? " checked=\"checked\"":""; ?> /> <?php print _('pagecontent_no'); ?></label>
		                        </div>
	                    	</div>
	                    	<?php } ?>
	                    	<hr/>
							<div class="form-group">
		                        <div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="Submit"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
		                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='pagecontent.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                        </div>
	                    	</div>
	                    	<br/><br/><br/>
						</form>
					<?php } elseif ($action=="adden" and $uac_add){
						$sql="SELECT * FROM content WHERE contentId='$pid'";
						$query=query($sql);
						$data=fetch($query);
						$data=formoutput($data);
					?>
						<a href="pagecontent.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

						<h4><?php print _('pagecontent_addpagevers'); ?> <?php print localename($lang);?> &raquo;  <?php print $data['contentTitle'];?></h4>
						<hr/>

						<form action="pagecontent.php?action=adden&amp;form=submit&amp;lang=<?php print $lang;?>&pid=<?php print $pid; ?>" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">
							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="title"><?php print _('pagecontent_titlepage'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input id="title" required class="form-control col-md-7 col-xs-12" name="title" type="text">
		                        	<span class="help-block"><?php print _('pagecontent_displayasmenu'); ?></span>
		                        </div>
	                    	</div>
	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="title"><?php print _('pagecontent_labelpage'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input id="label" required class="form-control col-md-7 col-xs-12" name="label" type="text">
		              
		                        </div>
	                    	</div>
	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="desc"><?php print _('pagecontent_contentspage'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="desc" id="desc" class="tinymce form-control col-md-7 col-xs-12"></textarea>
		                        	<a class="btn btn-default btn-xs" href="javascript:MM_openBrWindow('imgfly.php','imgbank','scrollbars=yes,resizable=no,width=500,height=500')"><i class="fa fa-upload"></i> <?php print _('pagecontent_uploadimg'); ?></a><br/><br/>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="descseo"><?php print _('pagecontent_descpageseo'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="descseo" id="descseo" maxlength="255" class="form-control col-md-7 col-xs-12"></textarea>
		                        	<span class="help-block"><?php print _('pagecontent_descpageseo_info'); ?></span>
		                        </div>
	                    	</div>
	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="keywordseo"><?php print _('pagecontent_keywordseo'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="keywordseo" id="keywordseo" maxlength="255" class="form-control col-md-7 col-xs-12"></textarea>
		                        	<span class="help-block"><?php print _('pagecontent_keywordseo_info'); ?></span>
		                        </div>
	                    	</div>
	                    	<hr/>
							<div class="form-group">
		                        <div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="Submit"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
		                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='pagecontent.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                        </div>
	                    	</div>
						</form>

					<?php } elseif ($action=="editen" and $uac_edit){
						$sql="SELECT contentTitle FROM content WHERE contentId=$pid";
						$query=query($sql);
						$data=fetch($query);
						$data=formoutput($data);
						$contenttitle=$data['contentTitle'];
						
						$sql="SELECT * FROM content_lang WHERE contentId=$pid AND lang='$lang'";
						$query=query($sql);
						$data=fetch($query);
						$data=formoutput($data);
					?>
						<a href="pagecontent.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

						<h4><?php print _('pagecontent_editpagevers'); ?> <?php print localename($lang);?> &raquo; <?php print $contenttitle; ?></h4>
						<hr/>
						<form action="pagecontent.php?action=editen&amp;form=submit&amp;pid=<?php print $pid; ?>&amp;lang=<?php print $lang ?>" method="post" enctype="multipart/form-data" name="edit" id="edit" class="form-horizontal form-label-left">

							<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="title"><?php print _('pagecontent_titlepage'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input id="title" required class="form-control col-md-7 col-xs-12" name="title" type="text" value="<?php print $data['contentTitle'] ?>">
		                        	<span class="help-block"><?php print _('pagecontent_displayasmenu'); ?></span>
		                        </div>
	                    	</div>
	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="title"><?php print _('pagecontent_labelpage'); ?> <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input id="title" required class="form-control col-md-7 col-xs-12" name="label" type="text" value="<?php print $data['contentTitle'] ?>">
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="desc"><?php print _('pagecontent_contentspage'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="desc" id="desc" class="tinymce form-control col-md-7 col-xs-12"><?php print $data['contentDesc'] ?></textarea>
		                        	<a class="btn btn-default btn-xs" href="javascript:MM_openBrWindow('imgfly.php','imgbank','scrollbars=yes,resizable=no,width=500,height=500')"><i class="fa fa-upload"></i> <?php print _('pagecontent_uploadimg'); ?></a><br/><br/>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="descseo"><?php print _('pagecontent_descpageseo'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="descseo" id="descseo" maxlength="255" class="form-control col-md-7 col-xs-12"><?php print $data['contentDescSeo'] ?></textarea>
		                        	<span class="help-block"><?php print _('pagecontent_descpageseo_info'); ?></span>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="keywordseo"><?php print _('pagecontent_keywordseo'); ?>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<textarea name="keywordseo" id="keywordseo" maxlength="255" class="form-control col-md-7 col-xs-12"><?php print $data['contentKeywordSeo'] ?></textarea>
		                        	<span class="help-block"><?php print _('pagecontent_keywordseo_info'); ?></span>
		                        </div>
	                    	</div>

	                    	<div class="form-group">
		                        <div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="Submit"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
		                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='pagecontent.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                        </div>
	                    	</div>

						</form>
					<?php } ?>
					</div>
	        	</div>
	        </div>
        </div>
        <!-- END THE CONTENT OF PAGE HERE -->

        <?php include("com/com-footer.php"); ?>
	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>