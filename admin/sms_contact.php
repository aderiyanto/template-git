<?php
	$page=43;
	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/mainmod.php");	
	include("../modz/mainmod-extend.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("../modz/errormsg.php");
	include("authuser.php");
    include("../modz/excel_reader.php"); // using class phpExcelReader
    session_start();
    
    if ($form=="submit"){

        if ($action=="add"){
            if(empty($name) or empty($hp) ){
				$error=errorlist(2);
			}
            
            $hp=displayhp($hp);
            $name=trim($name);
            
            if(strlen($hp) < 12 || strlen($hp) > 15){
                $error=errorlist(72);
            }
            
            $exist=countdata("contacts_sms","conHP='$hp'");
            if ($exist==1){
				$error=errorlist(67);
			}
            $regmo=date("n",$now);
            $regyear=date("Y",$now);
           
            if(!$error){
                $conid=nextid("conId","contacts_sms");		
                $sql="INSERT INTO contacts_sms(conId,conFullName,conHP,conEmail,conStatus,conRegDateTime,conRegMonth,conRegYear) VALUES ($conid,'$name','$hp','$email','y','$now','$regmo','$regyear')";
                $query=query($sql);
                if ($query){
                    foreach ($gr as $grid){
                        $sql="INSERT INTO groups_contacts_sms(groupId,conId) VALUES ($grid,$conid)";
                        $query = query($sql);
                    }

                }
            }
      
		}else if($action=="edit"){
          
            if(empty($id) or empty($name) or empty($hp) ){
				$error=errorlist(2);
            }
            
            $hp=displayhp($hp);
            $name=trim($name);
            $exist=countdata("contacts_sms","conHP='$hp'  AND conId!='$id'");
            if ($exist==1){
				$error=errorlist(67);
			}
            
            if(strlen($hp) < 12 || strlen($hp) > 15){
                $error=errorlist(72);
            }
            if(!$error){
                $sql="UPDATE contacts_sms SET conFullName='$name', conHP='$hp', conEmail='$email' WHERE conId='$id'";
                $query=query($sql);
                if ($query){
                    $sql2="DELETE FROM groups_contacts_sms WHERE conId='".$id."'"; 
				    $query2=query($sql2);
                    
                    foreach ($gr as $grid){
                        $sql="INSERT INTO groups_contacts_sms(groupId,conId) VALUES ($grid,$id)";
                        $query = query($sql);
                    }

                }
            }
            
        }else if($action=="nonactive"){
            $sql = "UPDATE contacts_sms SET conStatus='n' WHERE conId='$id'";
            $query = query($sql);
        }else if($action=="active"){
            $sql = "UPDATE contacts_sms SET conStatus='y' WHERE conId='$id'";
            $query = query($sql);
        }elseif($action=="del"){
           if(empty($id)){
               $error=errorlist(2);
           }else{
               $sql = "DELETE FROM  contacts_sms WHERE conId='$id' ";
               $query = query($sql);
               if($query){
                   $sql2 = "DELETE FROM groups_contacts_sms WHERE conId='$id'";
                   $query2 = query($sql2);
               }
           }
		}else if($action=="sendsms"){
            if(empty($id) or empty($text) or empty($senderid)){
				$error=errorlist(2);
			}
            
            if($getbalance < 275 or empty($getbalance)){
                $error=errorlist(66);
            }
            
            if(!$error){
                $sqlcontact = "SELECT * FROM contacts_sms WHERE conId='$id'";
                $querycontact = query($sqlcontact);
                while($data = fetch($querycontact)){
                    $sql="INSERT INTO sms( smsTo, smsText,smsDay, smsMonth, smsYear, smsDate, smsTime,smsTimetime,smsStatus) VALUES ('$data[conHP]','$text','".date('d', $now)."','".date('m', $now)."','".date('Y', $now)."','".date('Y-m-d', $now)."','".date('H:i:s', $now)."','$now','new')";
				    $query=query($sql);
                }

            }
            
		}else if($action=="import"){
            
            $rand = rand(1,99);
			if (empty($_FILES['userfile'])){
				displayerror(2);
			}elseif(!strpos($_FILES['userfile']['name'],'.xlsx')){
				displayerror(6);
			}
			//print_r($_FILES['userfile']);exit;
            $name_new_file = 'excel-'.$cook_id.'-'.$rand.'-data.xlsx';
            
            
            if(is_file('../docs/tmp/'.$name_new_file)){
                unlink('../docs/tmp/'.$name_new_file); 
            }
            $tipe_file = $_FILES['userfile']['type'];
			$tmp_file = $_FILES['userfile']['tmp_name'];
            
            if($tipe_file == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){
				
                require_once '../modz/PHPExcel/PHPExcel.php';
                move_uploaded_file($tmp_file, $_SERVER['DOCUMENT_ROOT'].'/docs/tmp/'.$name_new_file);
                
                $excelreader = new PHPExcel_Reader_Excel2007();
                $loadexcel = $excelreader->load('../docs/tmp/'.$name_new_file);
                ini_set('memory_limit', '20000M');
                ini_set('max_execution_time', 1800); //600 seconds = 10 minutes
                $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
                //$last=count($gr);
				//$gr[$last]=1;
                $numrow = 1;
				
				$count_all_rows = 1;
				$count_success_data = 0;
				$count_failed_data = 0;
				$failed_data = array();
				$sqlquery = array();
				$phones = array();
                
                if(count($sheet) > 2500){
                    $sql = "INSERT INTO sms_contact_upload_temp(tempFile) VALUES('$name_new_file')";
                    $query = query($sql);
                }else{
                    foreach($sheet as $row){
                        $name = cleanup($row['A']); 
                        $hp = cleanup($row['B']);
                        $email = cleanup($row['C']); 
                        $group = cleanup($row['D']); 


                        if($numrow > 1){
                            if($name and $hp){
                                $count_all_rows++;
                                $hp=displayhp($hp);
                                $exist=countdata("contacts_sms","conHP='$hp'  ");
                                $fullname=$name;
                                if(in_array($hp,$phones)){
                                    $count_failed_data++;
                                    $failed_data[] = array(
                                        'name' => $name,
                                        'phone' => $hp,
                                        'reason' => 'Nomor Ganda/Sudah tersimpan sebelumnya'
                                    );
                                    continue;
                                }
//                                var_dump($failed_data);
                                //make array of phone number to avoid same number insert twice time		
                                $phones[]=$hp;
                                $arrname=splitname($name);
                                $fname=$arrname[0];
                                $mname=$arrname[1];
                                $lname=$arrname[2];

                                $fullname=$name;
                                $nickname=$fname;
                                if($exist){
                                    $conid=getval('conId','contacts_sms','conHP='.$hp);
                                    $sql="UPDATE contacts_sms SET conFullName='$fullname',conHP='$hp',conEmail='$email' WHERE conId='$conid'";
                                    $query=query($sql);
                                    if($query){
                                        $count_success_data++;
                                    }
                                }else{

                                    $conid=nextid("conId","contacts_sms");				

                                    $regmo=date("n",$now);
                                    $regyear=date("Y",$now);

                                    //Full texts 	conId 	provId 	merchantId 	conFirstName 	conMidName 	conLastName 	conFullName 	conNickName 	conBirthday format dd/mm	conHP 	conHPType 	conEmail 	conCity 	conStatus 	conRegDateTime 	conRegMonth 	conRegYear 	conLastSent 	conDesc
                                    $sql="INSERT INTO contacts_sms(conId,provId,conFirstName,conMidName,conLastName,conFullName,conNickName,conBirthday,conHP,conHPType,conEmail,conCity,conStatus,conRegDateTime,conRegMonth,conRegYear,conLastSent,conDesc) VALUES ($conid,'$provid','$fname','$mname','$lname','$fullname','$nickname','$birthday','$hp','0','$email','$city','y','$now','$regmo','$regyear',0,'$desc')";
                                    $query=query($sql);
                                    if($query){
                                        $count_success_data++;
                                    }

                                }

                                if($query){
                                    if($exist){
                                        $sql="DELETE FROM groups_contacts_sms WHERE conId='$conid'";
                                        $query=query($sql);
                                    }
                                    unset($grxls);
                                    if($group){
                                        $groups=explode(',',$group);
                                        foreach($groups as $groupName){
                                            $groupName=cleanup($groupName);
                                            $groupId=getval('groupId','groups_sms','groupName',$groupName);
                                            if($groupId){
                                                $grxls[]=$groupId;							
                                            }
                                            else{
                                                $nextGroupId=nextid('groupId','groups_sms');
                                                $sql="INSERT INTO groups_sms VALUES('$nextGroupId','$groupName')";
                                                $query=query($sql);
                                                if($query){
                                                    $sql="INSERT INTO groups_contacts_sms VALUES ('$nextGroupId','$conid')";

                                                    $query=query($sql);
                                                }
                                            }
                                        }	

                                    }

                                    unset($grx);
                                    $sqlquery=array();
                                    if($gr){
                                        $grx=($grxls ? array_merge($grxls,$gr):$gr);
                                    } else{
                                        $grx= $grxls;
                                    }

                                    $grx=array_unique($grx);

                                    foreach ($grx as $grid){
                                        $sqlquery[]="('$grid','$conid')";
                                    }
                                    if(count($sqlquery)>0){
                                        asort($sqlquery);
                                        $sql="INSERT INTO groups_contacts_sms VALUES ".implode(',',$sqlquery);
                                        $query=query($sql);
                                    }

                                }	


                            }
                        }


                        $numrow++;
                    }
                    unlink('../docs/tmp/'.$name_new_file); 
                    $_SESSION['import']['count_rows'] = $count_all_rows-1;
                    $_SESSION['import']['count_rows_success'] = $count_success_data;
                    $_SESSION['import']['count_rows_failed'] = $count_failed_data;
                    $_SESSION['import']['failed_content'] = $failed_data;
                }
               
            }else{
                $error=errorlist(73);
            }
            
            
           
		}else if($action=="testing"){
            ini_set('memory_limit', '20000M');
            ini_set('max_execution_time', 1800); //600 seconds = 10 minutes
            $tmp_file = $_FILES['file1']['tmp_name'];
            $name_new_file = $_FILES['file1']['name'];
            require_once '../modz/PHPExcel/PHPExcel.php';
            move_uploaded_file($tmp_file, $_SERVER['DOCUMENT_ROOT'].'/docs/tmp/'.$name_new_file);

            $excelreader = new PHPExcel_Reader_Excel2007();
            $loadexcel = $excelreader->load('../docs/tmp/'.$name_new_file);
            $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
            
            if($sheet){
                print count($sheet);
            }
            
            exit;   
		}
        
        
        if(!$query){
			$error=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:sms_contact.php");
			}else{
				print "ok";
			}
		}
		exit;
    }
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print _('sms_contact_page_title'); ?> - <?php print _('area'); ?> - <?php print SITE_NAME; ?></title>

<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- bootstrap-datepicker -->
<link rel="stylesheet" type="text/css" href="/libs/bootstrap-datepicker/css/datepicker3.css" />
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce_business.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($){
	$('#result').hide();
    $('.fancybox').fancybox({
		'arrows': false
	});
	$('.fancybox-frame').fancybox({
		'type': 'iframe'
	});

	validate('#result','#add','sms_contact.php');
	validate('#result','#edit','sms_contact.php');
	validate('#result','#import','sms_contact.php?');
    <?php
    $sql="SELECT conId FROM contacts_sms";
    $query = query($sql);
    while($data = fetch($query)){
    ?>
	validate('#result','#sendsms<?php print $data['conId']; ?>','sms_log.php');
    <?php
    }
    ?>
	
	$('html').click(function(){
		$('.ui-widget').slideUp(500);
	});
    
    
    $("#formupload").submit(function(evt){	 
      evt.preventDefault();
      var formData = new FormData($(this)[0]);
       $.ajax({
           url: '/admin/sms_contact.php?form=submit&action=testing',
           type: 'POST',
           data: formData,
           async: false,
           cache: false,
           timeout: 600000,
           contentType: false,
           enctype: 'multipart/form-data',
           processData: false,
           success: function (response) {
             alert(response);
           }
       });
       return false;
 });
    
}); 

</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">
			<div class="row">
				<div class="col-md-12">
					<h3><?php print _('sms_contact_page_title'); ?></h3>
                    <p><?php print _('sms_contact_page_title_desc'); ?></p>
					<hr>
				</div>
			</div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                    <div class="pull-right">
                           <a class="btn btn-primary" href="?action=add"><?php print _('sms_contact_add'); ?></a>
                           <a class="btn btn-primary" href="?action=import"><?php print _('sms_contact_import'); ?></a>
                    </div>
                </div>
            </div>
			<div class="row">
				<div class="col-md-12">
                    <?php 
					if ($action==""){
                        include "com/com-getbalance.php"; 
                    ?>
                    <div class="row">
                        <div class="col-md-7 pull-right">
                            <div class="pull-right">
                                <form name="search" method="get" class="form-inline">
                                    <div class="form-group">
                                        <input name="searchtext" type="text" class="form-control input-sm" id="keyword" size="40" value="<?php print $searchtext;?>" />
                                    </div>
                                    <button type="submit" name="Submit" value="<?php print _('webbot_find'); ?>" class="btn btn-sm btn-dark"><?php print _('search'); ?></button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                             if($_SESSION['import']['count_rows']){
								?>
								<div class="alert alert-success alert-dismissable">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
										×
									</button>
									<i class="fa fa-check"></i> <?php print _('sms_contact_import_success'); ?><br><br>
									Jumlah Seluruh Contact : <?php print $_SESSION['import']['count_rows']; ?><br>
									Jumlah Contact Success : <?php print $_SESSION['import']['count_rows_success']; ?><br>
									<?php 
									if(count($_SESSION['import']['failed_content'])>0){
										?>
										<a id="log_error" href="#log_error"  data-toggle="modal">Jumlah Contact Failed: <?php print $_SESSION['import']['count_rows_failed']; ?></a>
										<div class="modal fade" id="log_error" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg">
												<div class="modal-content">
													<div class="modal-header">

														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
															×
														</button>
														<h4 class="modal-title" id="myModalLabel">
															History SMS
														</h4>
													</div>
													<div class="modal-body">
														<div class="table-responsive">
															<table class="table table-bordered">
																<thead>
																	<th style="width:20px;">No</th>
																	<th style="width:10px;">Message ID</th>
																	<th style="width:130px;">Sent AT</th>
																	<th style="width:130px;">Done AT</th>
																	<th style="width:10px;" class="text-center">SMS Count</th>
																	<th style="width:120px;">Price</th>
																	<th style="width:50px;">Status</th>
																</thead>
																<tbody>
																	<?php
																	$sqlhistory = "SELECT * FROM sms_history WHERE historyMessageId='$val[messageId]'";
																	$queryhistory = query($sqlhistory);
																	$rowshistory = rows($queryhistory);
																	if($rowshistory > 0){
																		$numb = 1;
																		while($datahistory = fetch($queryhistory)){
																			$history_response = $datahistory['historyResponse'];
																			$decode_json = json_decode($history_response, true);
																			//print "<pre>";
																			//print_r($decode_json);
																	?>
																	<tr>
																		<td><?php print $numb; ?></td>
																		<td><?php print $decode_json['results'][0][messageId]; ?></td>
																		<td><?php print gmdate("Y-m-d H:i:s",strtotime($decode_json['results'][0][sentAt])); ?></td>
																		<td><?php print gmdate("Y-m-d H:i:s",strtotime($decode_json['results'][0][doneAt])); ?></td>
																		<td class="text-center"><?php print $decode_json['results'][0][smsCount]; ?></td>
																		<td><?php print "Per Message: ".$decode_json['results'][0][price][pricePerMessage]."<br>Currency: ".$decode_json['results'][0][price][currency]; ?></td>
																		<td><?php print "Group Name<br>"."<strong>".$decode_json['results'][0][status][groupName]."</strong><br>Name<br><strong>".$decode_json['results'][0][status][name]."</strong>"; ?></td>
																	</tr>
																	<?php
																			
																			$numb++;
																		}
																	}else{
																	?>
																	<tr>
																		<td colspan="9" class="text-center">Belum Ada History</td>
																	</tr>
																	<?php
																	}
																	?>
																</tbody>
															</table>
														</div>
													</div>
													<div class="modal-footer">

														<button type="button" class="btn btn-default" data-dismiss="modal">
															Close
														</button>
													</div>
												</div>

											</div>

										</div>
										<?php 
									}else{
										?>
										Jumlah Contact Failed: <?php print $_SESSION['import']['count_rows_failed']; ?>
										<?php 
									}
									?>									
								</div>
								<?php
								unset($_SESSION['import']);
                            }
                            ?>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dt-responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th style="width:20px;"><?php print _('cms_no'); ?></th>
                                            <th style="width:130px;"><?php print _('sms_contact_name'); ?></th>
                                            <th style="width:120px;"><?php print _('sms_contact_hp'); ?></th>
                                            <th style="width:300px;"><?php print _('sms_contact_email'); ?></th>
                                            <th style="width:20px;"></th>
                                            <th style="width:20px;"></th>
                                            <th style="width:20px;"></th>
                                            <th style="width:20px;"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if(empty($pg)){
                                            $pg=1;
                                        }

                                        $numPerPage=20;
                                        $offset=($pg-1)*$numPerPage;
                                        $num=1;
                                        $num=$num+(($pg-1)*$numPerPage);
                        
                                        if($searchtext){
                                            $whereClause[]="(conFullName like '%$searchtext%' OR conHP LIKE '%$searchtext%' OR conEmail='$searchtext')";
                                        }

                                        if(is_array($whereClause)){
                                            $whereClause=implode('  and ',$whereClause);
                                        }
                                        if($searchtext){
                                            $whereClause="AND $whereClause";
                                        }  
                        
                                        $sql="SELECT * FROM contacts_sms WHERE conId!=0 $whereClause ORDER BY conFullname ASC";
                                        $query=query($sql);
                                        $numofdata=rows($query);
                                        $sql.=' LIMIT '.$offset.','.$numPerPage;
                                        $query=query($sql);
                                        if($numofdata<1){
                                        ?>
                                        <tr>
                                            <td colspan="9" class="text-center"><?php print _('sms_data_empty');  ?></td>
                                        </tr>
                                        <?php
                                        }else{
                                            while($data = fetch($query)){
                                        ?>
                                        <tr>
                                            <td><?php print $num; ?></td>
                                            <td><?php print  ucwords(output($data['conFullName'])); ?></td>
                                            <td><?php print output($data['conHP']); ?></td>
                                            <td><?php print output($data['conEmail']); ?></td>
                                            <td class="text-center"><?php if($data['conStatus']=='y') print "<a href='sms_contact.php?form=submit&action=nonactive&id=$data[conId]' onclick=\" return confirm('Apakah anda yakin ingin menonaktifkan : $data[conFullName]') \"><i class='fa fa-check' style='color:#00cc00;' title='". _("sms_contact_nonaktif")." : ".$data['conFullName']."' rel='tooltip'></i></a>"; else print "<a href='sms_contact.php?form=submit&action=active&id=$data[conId]'><i class='fa fa-minus-circle' title='"._('sms_contact_aktif')." : $data[conFullName]' style='color:#cc0000;' rel='tooltip' onclick=\" return confirm('Apakah anda yakin ingin mengaktifkan : $data[conFullName]') \"></i></a>"; ?></td>
                                            <td class="text-center">
                                               <a id="modal-history" href="#modal-sendsms<?php print $data['conId']; ?>"  data-toggle="modal"  rel="tooltip" title="Send SMS to: <?php print $data['conFullName']; ?>"><i class="fa fa-envelope"></i></a>
                                            </td>
                                            <td class="text-center">
                                                <a href="sms_contact.php?action=edit&id=<?php print $data['conId']; ?>" rel="tooltip" title="<?php print _('cms_edit'); ?>: <?php print ucwords($data['conFullName']); ?>"><i class="fa fa-edit"></i> </a>
                                            </td>
                                            <td class="text-center">
												<a href="sms_contact.php?form=submit&action=del&id=<?php print $data['conId']; ?>" title="Apakah anda yakin akan menghapus : <?php print ucwords($data['conFullName']); ?>" class="delete" ><i title="<?php print _('cms_delete'); ?>: <?php print ucwords($data['conFullName']); ?>" rel="tooltip" class="fa fa-trash-o"></i></a>
											</td>
                                        </tr>
                                          <script>
                                            function countChar<?php print $data['conId']; ?>(val) {
                                                var len = val.value.length;
                                                var jumsms = Math.ceil(len/160);
                                                $('#charNum<?php print $data['conId']; ?>').text(len + ' Karakter');
                                                $('#countSMS<?php print $data['conId']; ?>').text(jumsms + ' SMS');
                                            }
                                        </script>
                                        <div class="modal fade" id="modal-sendsms<?php print $data['conId']; ?>" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                            ×
                                                        </button>
                                                        <h4 class="modal-title" id="myModalLabel">
                                                            Send SMS
                                                        </h4>
                                                    </div>
                                                    <form  action="sms_contact.php?form=submit&amp;action=sendsms&id=<?php print $data['conId']; ?>" method="post" enctype="multipart/form-data" name="sendsms<?php print $data['conId']; ?>" id="sendsms<?php print $data['conId']; ?>">
                                                    <input type="hidden" name="getbalance" id="getbalance" value="<?php print $balance['balance']; ?>">
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="prodname">
                                                                        <?php print _('sms_from'); ?><span class="required">*</span>
                                                                    </label>
                                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                                        <select class="form-control" name="senderid" required style="width:200px;">
                                                                            
                                                                            <option value="<?php print getconfig('SMS_API_FROM'); ?>"><?php print getconfig('SMS_API_FROM');; ?></option>
                                                                           
                                                                        </select>
                                                                    </div>
                                                                </div>&nbsp;&nbsp;
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="prodname">
                                                                        <?php print _('sms_to'); ?>
                                                                    </label>
                                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                                        <?php print $data['conFullName']." <". $data['conHP'].">" ?>
                                                                    </div>
                                                                </div>&nbsp;&nbsp;
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mCatPicLogo">
                                                                        <?php print _('sms_text'); ?> <span class="required">*</span>
                                                                    </label>
                                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                                        <textarea class="form-control" cols="10" rows="10" name="text" id="text"  onkeyup="countChar<?php print $data['conId']; ?>(this)" required></textarea>
                                                                        <div class="row">
                                                                            <div class="col-md-6 col-xs-6 col-sm-6 col-lg-6">
                                                                                 <div id="charNum<?php print $data['conId']; ?>" class="help-block" style="font-weight: bold;">0 Karakter</div>
                                                                            </div>
                                                                            <div class="col-md-6 col-xs-6 col-sm-6 col-lg-6">
                                                                                 <div id="countSMS<?php print $data['conId']; ?>" class="help-block text-right" style="font-weight: bold;">0 SMS</div>
                                                                            </div>
                                                                        </div> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-primary">
                                                            Send    
                                                        </button>
                                                        <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-top:1px;">
                                                            Close
                                                        </button>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                            $num++;
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?php
                        $options['total']=$numofdata;
                        $options['filename']='sms_contact.php';
                        $options['qualifier']='sms_contact.php';
                        $options['pg']=$pg;
                        $options['numPerPage']=$numPerPage;
                        $options['style']=1;
                        $options['addquery']=TRUE;
                        paging($options);
                        ?>
                    </div>
                    <?php
                    }else if($action=="add"){
                    ?>
                   
                    <a href="sms_contact.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
	        		<h4><?php print _('sms_contact_add_title'); ?></h4>
					<hr/>
                    <form action="sms_contact.php?form=submit&amp;action=add" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
	                        	<?php print _('sms_contact_name'); ?><span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-4 col-xs-12">
	                        	<input name="name" type="text" id="nama" size="40" class="form-control" required>
	                        </div>
                        </div>
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
	                        	<?php print _('sms_contact_email'); ?>
	                        </label>
	                        <div class="col-md-4 col-sm-4 col-xs-12">
	                        	<input name="email" type="email" id="email" size="40" class="form-control" >
	                        </div>
                        </div>
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
	                        	<?php print _('sms_contact_hp'); ?><span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-4 col-xs-12">
	                        	<input name="hp" type="text" id="hp" size="40" class="form-control" required>
                                <span class="help-block">Ex: 08123xxxx</span>
	                        </div>
                        </div>
                        <?php
                        $sqlx="SELECT * FROM groups_sms ORDER BY groupName";
                        $queryx=query($sqlx);
                        $rowsx = rows($queryx);
                        if($rowsx > 0){
                        ?>
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
	                        	<?php print _('sms_contact_add_to_group'); ?><span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-4 col-xs-12">
								<label class="checkbox-inline">
                                    <input type="checkbox" disabled="disabled" checked="checked"  name="default" /> All Contact
                                </label>
								<br>
                                <?php
                                while ($datax=fetch($queryx)){
                                    $datax=output($datax);
                                ?>
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="gr[]" value="<?php print $datax['groupId']; ?>" id="group_<?php print $datax['groupId']; ?>" /> <?php print $datax['groupName']; ?>
                                </label><br>
                                <?php
                                }
                                ?>
	                        </div>
                        </div>
                        <?php
                        }
                        ?>
                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='sms_contact.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>

                    </form>
                    <?php
                    }else if($action=="edit"){
                        $sql="SELECT * FROM contacts_sms WHERE conId='$id'";
                        $query=query($sql);
                        $data=fetch($query);
                        $data=formoutput($data);
                    ?>
                    <a href="sms_contact.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
	        		<h4><?php print _('sms_contact_edit_title'); ?></h4>
					<hr/>
                    <form action="sms_contact.php?action=edit&amp;form=submit&amp;id=<?php print $id; ?>" method="post" enctype="multipart/form-data" name="edit" id="edit" class="form-horizontal form-label-left">
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
	                        	<?php print _('sms_contact_name'); ?><span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-4 col-xs-12">
	                        	<input name="name" type="text" id="nama" size="40" class="form-control" required value="<?php print $data['conFullName']; ?>">
	                        </div>
                        </div>
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
	                        	<?php print _('sms_contact_email'); ?>
	                        </label>
	                        <div class="col-md-4 col-sm-4 col-xs-12">
	                        	<input name="email" type="email" id="email" size="40" class="form-control"  value="<?php print $data['conEmail']; ?>">
	                        </div>
                        </div>
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
	                        	<?php print _('sms_contact_hp'); ?><span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-4 col-xs-12">
	                        	<input name="hp" type="text" id="hp" size="40" class="form-control" required  value="<?php print $data['conHP']; ?>">
                                <span class="help-block">Ex: 08123xxxx</span>
	                        </div>
                        </div>
                        <?php
                        $sqlx="SELECT * FROM groups_sms ORDER BY groupName";
                        $queryx=query($sqlx);
                        $rowsx = rows($queryx);
                        if($rowsx > 0){
                        ?>
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
	                        	<?php print _('sms_contact_add_to_group'); ?><span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-4 col-xs-12">
								<label class="checkbox-inline">
                                    <input type="checkbox" disabled="disabled" checked="checked" name="default" id="group_<?php print $datax['groupId']; ?>" /> All Contact
                                </label>
								<br>
                                <?php
                                    $grid=array();
                                    $sqlf="SELECT groupId FROM groups_contacts_sms WHERE conId=" . $id;
                                    $queryf=query($sqlf);
                                    while ($dataf=fetch($queryf)){
                                        $dataf=output($dataf);
                                        $grid[]=$dataf['groupId'];
                                    }

                                   
                                    while ($datax=fetch($queryx)){
                                        $datax=output($datax);
                                ?>
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="gr[]" value="<?php print $datax['groupId']; ?>" id="group_<?php print $datax['groupId']; ?>" <?php print (in_array($datax['groupId'],$grid))?" checked=\"checked\"":""; ?>    /> <?php print $datax['groupName']; ?>
                                </label><br>
                                <?php
                                }
                                ?>
	                        </div>
                        </div>
                        <?php
                        }
                        ?>
                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" ><i class="fa fa-plus"></i> <?php print _('cms_updatebutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='sms_contact.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>

                    </form>
                    <?php
                    }else if($action=="import"){
                    ?>
                     <a href="sms_contact.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
	        		<h4><?php print _('sms_contact_import_title'); ?></h4>
                     <a class="fancybox" href="/assets/images/sscontact.jpg"><img src="/assets/images/sscontact.jpg"></a><br><br>
                    
                    <?php print _('sms_contact_import_anounce'); ?><br><br>
                    <a href="/docs/contacts.xlsx"><strong>Download Template File</strong></a>
					<hr/>
                    <form action="sms_contact.php?form=submit&amp;action=import" method="post" enctype="multipart/form-data" name="import" id="import" class="form-horizontal form-label-left">
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
	                        	<?php print _('sms_contact_file'); ?><span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-4 col-xs-12">
	                        	<input  name="userfile" type="file" size="40" class="form-control" required>
	                        </div>
                        </div>
                        <?php
                        $sqlx="SELECT * FROM groups_sms ORDER BY groupName";
                        $queryx=query($sqlx);
                        $rowsx = rows($queryx);
                    
                        ?>
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
	                        	<?php print _('sms_contact_add_to_group'); ?><span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-4 col-xs-12">
								<label class="checkbox-inline">
                                    <input type="checkbox" disabled="disabled" checked="checked"  id="group_<?php print $datax['groupId']; ?>" /> All Contact
                                </label>
								<br>
                                <?php
                                while ($datax=fetch($queryx)){
                                    $datax=output($datax);
                                ?>
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="gr[]" value="<?php print $datax['groupId']; ?>" id="group_<?php print $datax['groupId']; ?>" /> <?php print $datax['groupName']; ?>
                                </label><br>
                                <?php
                                }
                                ?>
	                        </div>
                        </div>
                        
                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='sms_contact.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>

                    </form>
                    <?php
                    }else if($action=="testing"){
                    ?>
                    <form id="formupload" enctype="multipart/form-data" method="post">
                        <input type="file" name="file1" id="file1"><br>
                        <input type="submit" value="Upload File">
                    </form>
                    <?php
                    }
                    ?>
				</div>
			</div>
        </div>
        <!-- /page content -->

        <?php include("com/com-footer.php"); ?>
	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>