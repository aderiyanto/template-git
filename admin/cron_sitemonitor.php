<?php
	#this file only allow to execute by using cron or cli command, but it still accessible using specific http get parameter
	if(isset($_SERVER['HTTP_USER_AGENT']) and $_GET['t'] != date('mY')){
		die('Forbidden Access');
	}
	
	//includes all files necessary to support operations
	$basepath = realpath(dirname(__file__).'/..');
	include($basepath."/modz/config-main.php");
	include($basepath."/modz/config.php");
	include($basepath."/modz/license.php");
	include($basepath."/modz/errormsg.php");
	include($basepath."/modz/mainmod.php");
	include($basepath."/modz/connic.php");
	include($basepath."/modz/getall-admin.php");
	include($basepath."/modz/XMLFunction.php");

	//this is all the cron job should be written
	//cron is set to run once in an hour

	//1. remove xml data
	XMLremoveOldNode('sms',1);
	XMLremoveOldNode('contact',1);

	//2. remove from email queue. more than 6 hours
	$ago=$now-(6*3600);
	$sql="DELETE FROM email_queue WHERE emailStatus='y' AND emailDate<=$ago";
	$query=query($sql);
	
	//3. get IP information for all empty web bot desc info
	$sql="SELECT wbId, wbIpAddress FROM webbot WHERE wbDesc='' ORDER BY wbId LIMIT 20";
	$query=query($sql);
	$numofrec=rows($query);
	if ($numofrec>0){
		while ($data=fetch($query)){
			$wbid=$data['wbId'];
			$ip=$data['wbIpAddress'];
			
			//get information
			$ipinfo=cleanup(geoip_info($ip));
			//update to webbot table
			$sqlx="UPDATE webbot SET wbDesc='$ipinfo' WHERE wbId='$wbid' and wbIpAddress='$ip'";
			$queryx=query($sqlx);
		}
	}
	
	#update activity statistic
 	$cron_option['cron_name'] = 'CRON_SITEMONITOR'; 
	cron_activity_log($cron_option);
?>