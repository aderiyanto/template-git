<?php

//includes all files necessary to support operations
include("../modz/config-main.php");
include("../modz/config.php");
include("../modz/errormsg.php");
include("../modz/license.php");
include("../modz/mainmod.php");
include("../modz/mainmod-extend.php");
include("../modz/connic.php");
include("../modz/getall-admin.php");
include("authuser.php");

/*//reset smsviro
$sms = query("TRUNCATE contacts_sms");
$sms = query("TRUNCATE sms");
$histroy = query("TRUNCATE sms_history");
$histo = query("TRUNCATE sms_history_merchant");
$schedule = query("TRUNCATE sms_schedule");
$lang_sms = query("TRUNCATE sms_template_lang");
$grup = query("TRUNCATE groups_sms");
$grup_sms = query("TRUNCATE groups_contacts_sms");
$t_sms = query("TRUNCATE template_sms");*/


//reset image manajer
$imgbank = query("TRUNCATE imgbank");
//reset glosary/wordlist
$glosary = query("TRUNCATE glossary");
//reset file manager
$file = query("TRUNCATE file_manager");

//reset data email queue
$email = query("TRUNCATE email_queue");

//reset photo gallery
$gallery = query("TRUNCATE gallery_album");
$gallery_lang = query("TRUNCATE gallery_album_lang");
$photo = query("TRUNCATE gallery_photo");
$photo_lang = query("TRUNCATE gallery_photo_lang");

//reset news info
$berita = query("TRUNCATE newsinfo");
$cat_berita = query("TRUNCATE newsinfo_cat");
$cat_lang = query("TRUNCATE newsinfo_cat_lang");
$komen = query("TRUNCATE newsinfo_comment");
$content = query("TRUNCATE newsinfo_content");
$content_lang = query("TRUNCATE newsinfo_content_lang");
$lang = query("TRUNCATE newsinfo_lang");
$slide = query("TRUNCATE newsinfo_slide");

//reset counter/visit
$online  = query("TRUNCATE onlinenow");
$counter = query("UPDATE counter SET conValue = '0'");

/*//reset faq
$faq = query("TRUNCATE support");
$faqq = query("TRUNCATE supportcat");
$faqq_lang = query("TRUNCATE supportcat_lang");
$faq_lang = query("TRUNCATE support_lang");
*/
//reset newsinfo
$tick = query("TRUNCATE ticker");
$tick_lang = query("TRUNCATE ticker_lang");

//reset video
$video = query("TRUNCATE video");
$video_cat = query("TRUNCATE video_cat");
$video_cat_lang = query("TRUNCATE video_cat_lang");
$vvideo_lang = query("TRUNCATE video_lang");

#folder
$g[]="../assets/gallery";
$g[]="../assets/imgbank";
$g[]="../assets/news";

foreach ($g as $key => $value) {
  delete_directory($value);
}

print "<script>alert('Data Berhasil DI reset!'); window.location='index.php';</script>";




?>
