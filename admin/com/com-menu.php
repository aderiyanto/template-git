<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
	<div class="menu_section">
		<?php
		if(count($multilang_cms) > 1){
		?>
		<div class="bendera">				 
			<span class="bendera-title"><?php print _('commenu_view_language'); ?>: </span>
			<?php
			foreach($multilang_cms as $key=>$value){
				?>
				<a href="/set/lang-admin/<?php print $value;?>/"><img align="absmiddle" width="19" height="19" src="../assets/images/flag-<?php print ($_COOKIE['lang_admin']!=$value)?$value."-add.png":$value."-edit.png";?>"/></a>
			<?php
			}
			?>				 
		</div>
		<?php } ?>

		<ul class="nav side-menu">
			<li<?php print($page=="home")?" class=\"current-page\"":""; ?>><a href="index.php"><i class="fa fa-home"></i> Home</a></li>

			<?php
				$lc = unserialize(strdecode($license_0195));
				$newsletter_features = false;
				if(in_array(25, $lc['features'])){
					$newsletter_features = true;
				}
				
				$features=implode(',', $lc['features']);
				unset($lc);
				$sqln="SELECT * FROM admin_menu_cat c, admin_group_menu_cat g WHERE g.catId=c.catId AND g.groupId=$cook_gid ORDER BY c.catSort ASC";
				$queryn=query($sqln);
				while ($datan=fetch($queryn)){
					$sqlm="SELECT * FROM admin_menu m, admin_group_menu g WHERE m.catId='" . $datan['catId'] . "' and m.menuId=g.menuId and g.groupId='$cook_gid'".($datan['catId']!=3 ? " AND FIND_IN_SET(m.menuId,'$features')":'')." ORDER BY m.menuSort ASC";	
					$querym=query($sqlm);
					if(rows($querym)){
						?>
						<li><a><?php
							if(!empty($datan['catMenuIcon'])){
								echo "<i class=\"{$datan['catMenuIcon']}\"></i> ";
							}else{
								echo "<i class=\"fa fa-cog\"></i> ";
							}

							print $datan['catName']; ?> 
							<span class="fa fa-chevron-down"></span>
							</a>
							<ul class="nav child_menu"> 
							<?php
							while ($datam=fetch($querym)){
								$datam=output($datam);
								if($datam['menuId']==33 or $datam['menuId']==34){
									if(!$newsletter_features){
										continue;
									}
								}
								?><li<?php print($page==$datam['menuId']) ? " class=\"current-page\"":""; ?>><a href="<?php print $datam['menuFile']; ?>"<?php print ($datam['menuId']==25 or $datam['menuId']==26) ? " target=\"_blank\"":""; if ($datam['menuId']=="9") { print " target=\"_blank\""; } ?>><?php print $datam['menuName']; ?></a></li>
								<?php				
							}
							?>
							</ul>
						</li>
						<?php
					}
				}
			?>

			<li><a href="logout.php"><i class="fa fa-sign-out"></i> <?php print _('commenu_view_logout'); ?></a></li>
		</ul>
	</div>
</div>
<!-- END #sidebar-menu -->
