<?php if(!defined('WEBBY_EXEC')) exit; ?>

<div class="navbar nav_title" style="border: 0;">
    <a href="index.php" class="site_title">
        <img src="/assets/images/webby-logo-menu.png">
        <span>Webby Digital</span>
    </a>
</div>

<div class="clearfix"></div>

<div class="profile clearfix">
	<div class="profile_pic">
		<?php 
		// get image profile
		$countimgprofile = countdata("admin", "adminId='".$cook_id."' AND adminDirPic!='' AND adminPic!=''");
		if($countimgprofile > 0){
			$imgprofile = getval("adminName, adminDirPic, adminPic","admin", "adminId='".$cook_id."' AND adminDirPic!='' AND adminPic!=''");
			echo '<img src="/assets/profile/'.$imgprofile['adminDirPic'].'/'.genthumb($imgprofile['adminPic'],'m').'" alt="Profile: '.$imgprofile['adminName'].'" class="img-circle profile_img">';
		} else {
			echo '<img src="/assets/images/img.jpg" alt="Profile" class="img-circle profile_img">';
		}
		?>
	</div>
	<div class="profile_info">
		<span><?php print _('comgreet_view_welcom'); ?></span>
		<h2><?php print output($cook_username); ?></h2>
	</div>
</div>
