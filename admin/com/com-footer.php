<?php if(!defined('WEBBY_EXEC')) exit; ?>

<!-- footer content -->
<footer>
	<div class="pull-right">
		Powered By <a href="http://webby.digital/">Webby Digital</a>
	</div>
	<div class="clearfix"></div>
</footer>
<!-- /footer content -->