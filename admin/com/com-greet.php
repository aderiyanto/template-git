<?php if(!defined('WEBBY_EXEC')) exit; ?>

<!-- top navigation -->
<div class="top_nav">
	<div class="nav_menu">
		<nav>
			<div class="nav toggle">
				<a id="menu_toggle"><i class="fa fa-bars"></i></a>
			</div>

			<ul class="nav navbar-nav navbar-right">
				<li class="">
					<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<?php 
						// get image profile
						$countimgprofile = countdata("admin", "adminId='".$cook_id."' AND adminDirPic!='' AND adminPic!=''");
						if($countimgprofile > 0){
							$imgprofile = getval("adminName, adminDirPic, adminPic","admin", "adminId='".$cook_id."' AND adminDirPic!='' AND adminPic!=''");
							echo '<img src="/assets/profile/'.$imgprofile['adminDirPic'].'/'.genthumb($imgprofile['adminPic'],'s').'" alt="Profile: '.$imgprofile['adminName'].'">';
						} else {
							echo '<img src="/assets/images/img.jpg" alt="Profile">';
						}
						?><?php print output($cook_username); ?>
						<span class=" fa fa-angle-down"></span>
					</a>
					<ul class="dropdown-menu dropdown-usermenu pull-right">
						<li><a href="systemsettings.php"><i class="fa fa-cog pull-right"></i>Settings</a></li>
						<li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
					</ul>
				</li>
				<li class="">
					<a href="http://<?php echo getconfig('SITE_URL'); ?>" target="_blank">
						<i class="fa fa-globe"></i> <?php print _('comgreet_view_website'); ?>
					</a>
				</li>

				<?php
				/*
				*
				*
				* THIS IS FOR NOTIFICATION STRUCTUR
				*
				*
				*/
				/*

				<li role="presentation" class="dropdown">
					<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
						<i class="fa fa-bell-o"></i>
						<span class="badge bg-green">6</span>
					</a>
					<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
						<li>
							<a>
								<span class="image"><img src="/assets/images/img.jpg" alt="Profile Image" /></span>
								<span>
									<span>John Smith</span>
									<span class="time">3 mins ago</span>
								</span>
								<span class="message">
									Film festivals used to be do-or-die moments for movie makers. They were where...
								</span>
							</a>
						</li>
						<li>
						  <a>
						    <span class="image"><img src="/assets/images/img.jpg" alt="Profile Image" /></span>
						    <span>
						    	<span>John Smith</span>
						    	<span class="time">3 mins ago</span>
						    </span>
						    <span class="message">
						    	Film festivals used to be do-or-die moments for movie makers. They were where...
						    </span>
						  </a>
						</li>
						<li>
						  <a>
						    <span class="image"><img src="/assets/images/img.jpg" alt="Profile Image" /></span>
						    <span>
						    	<span>John Smith</span>
						    	<span class="time">3 mins ago</span>
						    </span>
						    <span class="message">
						    	Film festivals used to be do-or-die moments for movie makers. They were where...
						    </span>
						  </a>
						</li>
						<li>
						  <a>
						    <span class="image"><img src="/assets/images/img.jpg" alt="Profile Image" /></span>
						    <span>
						    	<span>John Smith</span>
						    	<span class="time">3 mins ago</span>
						    </span>
						    <span class="message">
						    	Film festivals used to be do-or-die moments for movie makers. They were where...
						    </span>
						  </a>
						</li>
						<li>
						  <div class="text-center">
						    <a>
						    	<strong>See All Alerts</strong>
						    	<i class="fa fa-angle-right"></i>
						    </a>
						  </div>
						</li>
					</ul>
				</li>
				*/ ?>
			</ul>
		</nav>
	</div>
</div><div class="clearfix"></div>

<?php /*
<!-- /top navigation -->
<div id="greet" style="position:relative">
	<?php print _('comgreet_view_welcom'); ?> <strong><?php print output($cook_username); ?></strong> | 
	<?php print _('comgreet_view_yourlastlogin'); ?> <strong><?php print convertdaytolang(date("l",$cook_lastlog)) . ", " . date("d ",$cook_lastlog) . convertmonthtolang(date("F",$cook_lastlog)) . date(" Y H:i:s",$cook_lastlog); ?> wib</strong> | <a href="logout.php">Logout</a>
	
</div>
*/?>