<?php
	$page=38;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config-image.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

	if (!isset($_SESSION)) { session_start(); }

	// Max Depth Menu
	$menuDepthLimit = 3;

	if($form=="submit"){

		if($action=="add" and $uac_add){
			if(empty($nama_menu)){
				$error=errorlist(2);
			}

			if($menu_akses=='pagecontent_link' AND empty($pagecontent)){
				$error=errorlist(2);
			}

			if($menu_akses=='newscategory_link' AND empty($newscategory)){
				$error=errorlist(2);
			}

			if($menu_akses=='out_link' AND empty($outlink)){
				$error=errorlist(2);
			}

			if($menu_akses=='product_link' AND empty($product)){
				$error=errorlist(2);
			}

			if($menu_akses=='productcategory_link' AND empty($productcategory)){
				$error=errorlist(2);
			}

			$exp_induk = explode("-",$induk);
			$levelberikut=$exp_induk[0]+1;

			if($levelberikut > $menuDepthLimit){
				$error=errorlist(98);
			}

			//insert data
			if(!$error){
				$menuName 	= $nama_menu;
		        $menuAccess	= $menu_akses;

		        $relID = 0;

		        if($menuAccess=='pagecontent_link'){
		        	$valcontent = getval("contentPermalink,contentId","content","contentId='{$pagecontent}'");

		    		$dataAccess = "/page/" . $valcontent['contentPermalink'];
		    		$relID = $pagecontent;
		        } elseif($menuAccess=='newscategory_link'){
		        	$valcontent = getval("catPermalink,catId","newsinfo_cat","catId='{$newscategory}'");

		    		$dataAccess = "/category/" . $valcontent['catPermalink'];
		    		$relID = $newscategory;
		        } elseif($menuAccess=='out_link'){
		    		$dataAccess = $outlink;
		    		$relID = 0;
		        } elseif($menuAccess=='in_link'){
		    		$dataAccess = "/".$inlink;
		    		$relID = 0;
		    	}elseif($menuAccess=='no_link'){
		            $dataAccess = "";
		            $relID = 0;
		        } elseif($menuAccess=='product_link'){
		        	$valcontent = getval("prodId,prodPermalink","product","prodId='{$product}'");
		            $dataAccess = "/produk/detail/".$valcontent['prodPermalink']."/";
		            $relID = $product;
		        } elseif($menuAccess=='productcategory_link'){
		        	$catpermalink = '';

		        	$valcontent1 = getval("catId,catPermalink,catLevel,subCat","productcat","catId='{$productcategory}'");
		        	if($valcontent1['subCat']==0  AND $valcontent1['catLevel']=1){
		        		$catpermalink = $valcontent1['catPermalink'];
		        	} else {

			        	$catcount2 = countdata("productcat","catId='{$productcategory}' AND catLevel=2");
			        	if($catcount2>0){
			        		$vallvl2content2 = getval("catId,catPermalink,catLevel,subCat","productcat","catId='{$productcategory}' AND subCat!=0 AND catLevel=2");

			        		$vallvl1content2 = getval("catId,catPermalink,catLevel,subCat","productcat","catId='{$vallvl2content2['subCat']}'");
			        		$catpermalink = $vallvl1content2['catPermalink']."/".$vallvl2content2['catPermalink'];
			        	}

			        	$catcount3 = countdata("productcat","catId='{$productcategory}' AND catLevel=3");
			        	if($catcount3>0){
			        		$vallvl3content3 = getval("catId,catPermalink,catLevel,subCat","productcat","catId='{$productcategory}' AND subCat!=0 AND catLevel=3");

			        		$vallvl2content3 = getval("catId,catPermalink,catLevel,subCat","productcat","catId='{$vallvl3content3['subCat']}'");

			        		$vallvl1content3 = getval("catId,catPermalink,catLevel,subCat","productcat","catId='{$vallvl2content3['subCat']}'");
			        		$catpermalink = $vallvl1content3['catPermalink']."/".$vallvl2content3['catPermalink']."/".$vallvl3content3['catPermalink'];
			        	}
			    	}

		            $dataAccess = "/produk/".$catpermalink."/";
		            $relID = $productcategory;
		        }

		        $addDate = $now;

		        $nextsort = nextsort("menu_website","menuParentId",$exp_induk[1],"menuSort");

		        $nextID = nextid('menuId','menu_website');
		        $sqlmenu = "INSERT INTO menu_website VALUES('{$nextID}','{$exp_induk[1]}','{$relID}','{$menuName}','{$menuAccess}','{$dataAccess}','{$addDate}','{$nextsort}','y')";
		        $queryins = query($sqlmenu);

		        if($queryins){
		        	$_SESSION['add_sukses'] = 'ok';
		        }
			}
		}elseif($action=="edit" and $uac_edit){
			if(empty($nama_menu)){
				$error=errorlist(2);
			}

			if($menu_akses=='pagecontent_link' AND empty($pagecontent)){
				$error=errorlist(2);
			}

			if($menu_akses=='newscategory_link' AND empty($newscategory)){
				$error=errorlist(2);
			}

			if($menu_akses=='out_link' AND empty($outlink)){
				$error=errorlist(2);
			}

			if($menu_akses=='product_link' AND empty($product)){
				$error=errorlist(2);
			}

			if($menu_akses=='productcategory_link' AND empty($productcategory)){
				$error=errorlist(2);
			}

			$exp_induk = explode("-",$induk);
			$levelberikut=$exp_induk[0]+1;

			if($levelberikut > $menuDepthLimit){
				$error=errorlist(98);
			}

			//update data
			if(!$error){
				$menuName 	= $nama_menu;
		        $menuAccess	= $menu_akses;

		        $relID = 0;

		        if($menuAccess=='pagecontent_link'){
		        	$valcontent = getval("contentPermalink,contentId","content","contentId='{$pagecontent}'");

		    		$dataAccess = "/page/" . $valcontent['contentPermalink'];
		    		$relID = $pagecontent;
		        } elseif($menuAccess=='newscategory_link'){
		        	$valcontent = getval("catPermalink,catId","newsinfo_cat","catId='{$newscategory}'");

		    		$dataAccess = "/category/" . $valcontent['catPermalink'];
		    		$relID = $newscategory;
		        } elseif($menuAccess=='out_link'){
		    		$dataAccess = $outlink;
		    		$relID = 0;
		        } elseif($menuAccess=='in_link'){
		    		$dataAccess = "/".$inlink;
		    		$relID = 0;
		    	}elseif($menuAccess=='no_link'){
		            $dataAccess = "";
		            $relID = 0;
		        } elseif($menuAccess=='product_link'){
		        	$valcontent = getval("prodId,prodPermalink","product","prodId='{$product}'");
		            $dataAccess = "/produk/detail/".$valcontent['prodPermalink']."/";
		            $relID = $product;
		        } elseif($menuAccess=='productcategory_link'){
		        	$catpermalink = '';

		        	$valcontent1 = getval("catId,catPermalink,catLevel,subCat","productcat","catId='{$productcategory}'");
		        	if($valcontent1['subCat']==0  AND $valcontent1['catLevel']=1){
		        		$catpermalink = $valcontent1['catPermalink'];
		        	} else {

			        	$catcount2 = countdata("productcat","catId='{$productcategory}' AND catLevel=2");
			        	if($catcount2>0){
			        		$vallvl2content2 = getval("catId,catPermalink,catLevel,subCat","productcat","catId='{$productcategory}' AND subCat!=0 AND catLevel=2");

			        		$vallvl1content2 = getval("catId,catPermalink,catLevel,subCat","productcat","catId='{$vallvl2content2['subCat']}'");
			        		$catpermalink = $vallvl1content2['catPermalink']."/".$vallvl2content2['catPermalink'];
			        	}

			        	$catcount3 = countdata("productcat","catId='{$productcategory}' AND catLevel=3");
			        	if($catcount3>0){
			        		$vallvl3content3 = getval("catId,catPermalink,catLevel,subCat","productcat","catId='{$productcategory}' AND subCat!=0 AND catLevel=3");

			        		$vallvl2content3 = getval("catId,catPermalink,catLevel,subCat","productcat","catId='{$vallvl3content3['subCat']}'");

			        		$vallvl1content3 = getval("catId,catPermalink,catLevel,subCat","productcat","catId='{$vallvl2content3['subCat']}'");
			        		$catpermalink = $vallvl1content3['catPermalink']."/".$vallvl2content3['catPermalink']."/".$vallvl3content3['catPermalink'];
			        	}
			    	}

		            $dataAccess = "/produk/".$catpermalink."/";
		            $relID = $productcategory;
		        }

		        if(isset($status)=='y'){
		        	$stat = 'y';
		        } else {
		        	$stat = 'n';
		        }

		        $nextsort = nextsort("menu_website","menuParentId",$exp_induk[1],"menuSort");

		        $sqlmenu = "UPDATE menu_website SET menuParentId='{$exp_induk[1]}',menuRelationshipId='{$relID}',menuName='{$menuName}',menuAccessType='{$menuAccess}',menuUrlAccess='{$dataAccess}',menuActive='{$stat}',menuSort='{$nextsort}' WHERE menuId='{$id}'";
        		$queryins = query($sqlmenu);
			}
		}elseif($action=="del" and $uac_delete){
			$countmenu = countdata("menu_website","menuParentId ='{$id}'");
			if($countmenu > 0){
				$error=true;
			}
			/*
			if($countmenu > 0){
				$query = query("SELECT * FROM menu_website WHERE menuParentId ='$id'");

				while ( $dt = fetch($query)) {
					// get sort
					$nextsort = nextsort("menu_website","menuParentId","0","menuSort");

					// update menu child first
					$update = query("UPDATE menu_website SET menuParentId='0',menuSort='{$nextsort}' WHERE menuId ='{$dt['menuId']}'");
				}
			}
			*/
			if(!$error){
				$hapus  = query("DELETE FROM menu_website WHERE menuId ='$id'");

	    		if($hapus){
	    			$_SESSION['del_sukses'] = 'ok';
	    		}
    		} 
    		if($error) {
    			$_SESSION['del_nok'] = 'ok';
    		}

	    	header("location:menu_web.php");
		}elseif($action=="updatesortmenu" and $uac_edit){
			$menuData = json_decode(output($menu_data), true);

		    $sort1 = 1;
		    foreach ($menuData as $key1 => $value1) {
		        $sql1 = "UPDATE menu_website SET menuParentId = '0', menuSort='{$sort1}' WHERE menuId='{$value1['id']}'";
		        $query1 = query($sql1);
		        
		        if(!empty($value1['children'])){
		            $sort2 = 1;
		            foreach ($value1['children'] as $key2 => $value2) {
		                $sql2 = "UPDATE menu_website SET menuParentId = '{$value1['id']}', menuSort='{$sort2}' WHERE menuId='{$value2['id']}'";
		                $query2 = query($sql2);

		                if(!empty($value2['children'])){
		                    $sort3 = 1;
		                    foreach ($value2['children'] as $key3 => $value3) { 
		                        $sql3 = "UPDATE menu_website SET menuParentId = '{$value2['id']}', menuSort='{$sort3}' WHERE menuId='{$value3['id']}'";
		                        $query3 = query($sql3);

		                        /*
		                        if(!empty($value3['children'])){
		                            $sort4 = 1;
		                            foreach ($value3['children'] as $key4 => $value4) {
		                                $sql4 = "UPDATE menu_website SET menuParentId = '{$value3['id']}', menuSort='{$sort4}' WHERE menuId='{$value4['id']}'";
		                                $query4 = query($sql4);
		                                $sort4++;
		                            }
		                        }
		                        */
		                        $sort3++;
		                    }
		                }
		                $sort2++;
		            }
		        }
		        $sort1++;
		    }

		    $_SESSION['ubah_sukses'] = 'ok';
		}elseif($action=="adden" and $uac_add){
			if(empty($name)){
				$error=errorlist(2);
			}else{
				$sql="INSERT INTO menu_website_lang (menuId,menuName,lang) VALUES ($id,'$name','$lang')";
				$query=query($sql);
			}
		}elseif($action=="editen" and $uac_edit){
			if(empty($name)){
				$error=errorlist(2);
			}else{
				$sql="UPDATE menu_website_lang SET menuName='$name' WHERE menuId='$id' AND lang='$lang'";
				$query=query($sql);
			}
		}

		//check whether query was successful
		if(!$query){
			$error .=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}
		else{
			if(!$js){
				header("location:menu_web.php");
			}else{
				print "ok";
			}
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _('menuweb_pagetitle'); ?></title>
<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- Nestable -->
<link href="/libs/nestable-master/nestable.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- required for this page -->
<!-- Nestable -->
<script type="text/javascript" src="/libs/nestable-master/jquery.nestable.js"></script>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('#result').hide();
	validate('#result','#add','menu_web.php');
	validate('#result','#edit','menu_web.php');

	<?php if($action!="edit" AND $action!="adden" AND $action!="editen"){ ?>
	//nestable
    var updateOutput = function(e){
        var list   = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };
    <?php if ($uac_edit){?>
    // activate Nestable for list 1
    $('#nestable').nestable({
        maxDepth:<?php echo $menuDepthLimit; ?>
    })
    .on('change', updateOutput);

    // output initial serialised data
    updateOutput($('#nestable').data('output', $('#nestable-output')));

    $('#nestable-menu').on('click', function(e){
        var target = $(e.target),
            action = target.data('action');
        if (action === 'expand-all') {
            $('.dd').nestable('expandAll');
        }
        if (action === 'collapse-all') {
            $('.dd').nestable('collapseAll');
        }
    });
    <?php } ?>
    <?php } ?>
});

function linktype(menu_akses){
    switch (this.menu_akses.value) {
        case '':
        case 'pagecontent_link':
            document.getElementById("pagecontentlink").style.display = 'block';
            document.getElementById("pagecontent").setAttribute("required","required");
            document.getElementById("pagecontent").removeAttribute("disabled");

            document.getElementById("newscategorylink").style.display = 'none';
            document.getElementById("newscategory").removeAttribute("required"); 
            document.getElementById("newscategory").setAttribute("disabled","disabled");

            document.getElementById("urllink").style.display = 'none';
            document.getElementById("outlink").removeAttribute("required"); 
            document.getElementById("outlink").setAttribute("disabled","disabled");

            document.getElementById("productlink").style.display = 'none';
            document.getElementById("product").removeAttribute("required"); 
            document.getElementById("product").setAttribute("disabled","disabled");

            document.getElementById("productcategorylink").style.display = 'none';
            document.getElementById("productcategory").removeAttribute("required"); 
            document.getElementById("productcategory").setAttribute("disabled","disabled");

            document.getElementById("urllinkin").style.display = 'none';
            document.getElementById("inlink").style.display = 'none';
            document.getElementById("inlink").removeAttribute("required");
            document.getElementById("inlink").setAttribute("disabled","disabled");
        break;
        case 'newscategory_link':
            document.getElementById("pagecontentlink").style.display = 'none';
            document.getElementById("pagecontent").removeAttribute("required");
            document.getElementById("pagecontent").setAttribute("disabled","disabled");

            document.getElementById("newscategorylink").style.display = 'block';
            document.getElementById("newscategory").setAttribute("required","required");
            document.getElementById("newscategory").removeAttribute("disabled");

            document.getElementById("urllink").style.display = 'none';
            document.getElementById("outlink").removeAttribute("required");
            document.getElementById("outlink").setAttribute("disabled","disabled");

            document.getElementById("productlink").style.display = 'none';
            document.getElementById("product").removeAttribute("required"); 
            document.getElementById("product").setAttribute("disabled","disabled");

            document.getElementById("productcategorylink").style.display = 'none';
            document.getElementById("productcategory").removeAttribute("required"); 
            document.getElementById("productcategory").setAttribute("disabled","disabled");

            document.getElementById("urllinkin").style.display = 'none';
            document.getElementById("inlink").style.display = 'none';
            document.getElementById("inlink").removeAttribute("required");
            document.getElementById("inlink").setAttribute("disabled","disabled");
        break;
        case 'out_link':
            document.getElementById("pagecontentlink").style.display = 'none';
            document.getElementById("pagecontent").removeAttribute("required");
            document.getElementById("pagecontent").setAttribute("disabled","disabled");

            document.getElementById("newscategorylink").style.display = 'none';
            document.getElementById("newscategory").removeAttribute("required"); 
            document.getElementById("newscategory").setAttribute("disabled","disabled");

            document.getElementById("urllink").style.display = 'block';
            document.getElementById("outlink").setAttribute("required","required");
            document.getElementById("outlink").removeAttribute("disabled");

            document.getElementById("productlink").style.display = 'none';
            document.getElementById("product").removeAttribute("required"); 
            document.getElementById("product").setAttribute("disabled","disabled");

            document.getElementById("productcategorylink").style.display = 'none';
            document.getElementById("productcategory").removeAttribute("required"); 
            document.getElementById("productcategory").setAttribute("disabled","disabled");

            document.getElementById("urllinkin").style.display = 'none';
            document.getElementById("inlink").style.display = 'none';
            document.getElementById("inlink").removeAttribute("required");
            document.getElementById("inlink").setAttribute("disabled","disabled");
        break;
         case 'in_link':
            document.getElementById("pagecontentlink").style.display = 'none';
            document.getElementById("pagecontent").removeAttribute("required");
            document.getElementById("pagecontent").setAttribute("disabled","disabled");

            document.getElementById("newscategorylink").style.display = 'none';
            document.getElementById("newscategory").removeAttribute("required"); 
            document.getElementById("newscategory").setAttribute("disabled","disabled");
            
            document.getElementById("urllink").style.display = 'none';
            document.getElementById("outlink").style.display = 'none';
            document.getElementById("outlink").removeAttribute("required");
            document.getElementById("outlink").setAttribute("disabled","disabled");

            document.getElementById("productlink").style.display = 'none';
            document.getElementById("product").removeAttribute("required"); 
            document.getElementById("product").setAttribute("disabled","disabled");

            document.getElementById("productcategorylink").style.display = 'none';
            document.getElementById("productcategory").removeAttribute("required"); 
            document.getElementById("productcategory").setAttribute("disabled","disabled");

            document.getElementById("urllinkin").style.display = 'block';
            document.getElementById("inlink").style.display = 'block';
            document.getElementById("inlink").setAttribute("required","required");
            document.getElementById("inlink").removeAttribute("disabled");
        break;
        case 'no_link':
            document.getElementById("pagecontentlink").style.display = 'none';
            document.getElementById("pagecontent").removeAttribute("required");
            document.getElementById("pagecontent").setAttribute("disabled","disabled");

            document.getElementById("newscategorylink").style.display = 'none';
            document.getElementById("newscategory").removeAttribute("required"); 
            document.getElementById("newscategory").setAttribute("disabled","disabled");

            document.getElementById("urllink").style.display = 'none';
            document.getElementById("outlink").removeAttribute("required"); 
            document.getElementById("outlink").setAttribute("disabled","disabled");

            document.getElementById("productlink").style.display = 'none';
            document.getElementById("product").removeAttribute("required"); 
            document.getElementById("product").setAttribute("disabled","disabled");

            document.getElementById("productcategorylink").style.display = 'none';
            document.getElementById("productcategory").removeAttribute("required"); 
            document.getElementById("productcategory").setAttribute("disabled","disabled");

            document.getElementById("urllinkin").style.display = 'none';
            document.getElementById("inlink").style.display = 'none';
            document.getElementById("inlink").removeAttribute("required");
            document.getElementById("inlink").setAttribute("disabled","disabled");
        break;
        case 'product_link':
            document.getElementById("pagecontentlink").style.display = 'none';
            document.getElementById("pagecontent").removeAttribute("required");
            document.getElementById("pagecontent").setAttribute("disabled","disabled");

            document.getElementById("newscategorylink").style.display = 'none';
            document.getElementById("newscategory").removeAttribute("required"); 
            document.getElementById("newscategory").setAttribute("disabled","disabled");

            document.getElementById("urllink").style.display = 'none';
            document.getElementById("outlink").removeAttribute("required"); 
            document.getElementById("outlink").setAttribute("disabled","disabled");

            document.getElementById("productlink").style.display = 'block';
            document.getElementById("product").setAttribute("required","required");
            document.getElementById("product").removeAttribute("disabled");

            document.getElementById("productcategorylink").style.display = 'none';
            document.getElementById("productcategory").removeAttribute("required"); 
            document.getElementById("productcategory").setAttribute("disabled","disabled");

            document.getElementById("urllinkin").style.display = 'none';
            document.getElementById("inlink").style.display = 'none';
            document.getElementById("inlink").removeAttribute("required");
            document.getElementById("inlink").setAttribute("disabled","disabled");
        break; 
        case 'productcategory_link':
            document.getElementById("pagecontentlink").style.display = 'none';
            document.getElementById("pagecontent").removeAttribute("required");
            document.getElementById("pagecontent").setAttribute("disabled","disabled");

            document.getElementById("newscategorylink").style.display = 'none';
            document.getElementById("newscategory").removeAttribute("required"); 
            document.getElementById("newscategory").setAttribute("disabled","disabled");

            document.getElementById("urllink").style.display = 'none';
            document.getElementById("outlink").removeAttribute("required"); 
            document.getElementById("outlink").setAttribute("disabled","disabled");

            document.getElementById("productlink").style.display = 'none';
            document.getElementById("product").removeAttribute("required"); 
            document.getElementById("product").setAttribute("disabled","disabled");

            document.getElementById("productcategorylink").style.display = 'block';
            document.getElementById("productcategory").setAttribute("required","required");
            document.getElementById("productcategory").removeAttribute("disabled");

            document.getElementById("urllinkin").style.display = 'none';
            document.getElementById("inlink").style.display = 'none';
            document.getElementById("inlink").removeAttribute("required");
            document.getElementById("inlink").setAttribute("disabled","disabled");
        break;       
    }
}
</script>
<style type="text/css">
    #urllink { display:none; }
    #newscategorylink { display:none; }
    #productlink { display:none; }
    #productcategorylink { display:none; }
    #pagecontentlink { display:block; }
</style>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('menuweb_pagetitle'); ?></h3>
			                <p><?php print _('menuweb_pagedesc'); ?></p>
			            </div>
		            </div>
	        	</div>
        	</div>
        	
        	<div class="row">
        		<?php if(empty($action)) { ?>
        		<?php if ($uac_add){ ?>
	        	<div class="col-md-4">
					<div class="x_panel tile">
						<div class="x_title">
	            			<h2><?php echo _('menuweb_addmenu'); ?></h2>
	            			<ul class="nav navbar-right panel_toolbox">
			                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
			                </ul>
	            			<div class="clearfix"></div>
	            		</div>

	            		<div class="x_content">
			                <form action="menu_web.php?action=add&form=submit" method="post" enctype="multipart/form-data" name="add" id="add">
			                    <section>
			                        <div class="form-group">
			                            <label for="induk"><?php echo _('menuweb_parentform'); ?></label>
			                            <select class="form-control" id="induk" name="induk">
			                                <option value="0-0"><?php echo _('menuweb_noparent'); ?></option>
			                                <?php 
			                                    $queryinduk = query("SELECT * FROM menu_website WHERE menuParentId = '0' ORDER BY menuSort ASC");
			                                    $xx1 = 1;
			                                    while ($pm1 = fetch($queryinduk)) {
			                                        echo "<option value=\"1-{$pm1['menuId']}\">{$xx1}. {$pm1['menuName']}</option>";

			                                        $sql2 = "SELECT * FROM menu_website WHERE menuParentId = '{$pm1['menuId']}' AND menuParentId != '0'";
			                                        $querynumMenu2 = query($sql2);
				                                    $numnumMenu2 = rows($querynumMenu2);

				                                    if($numnumMenu2>0){
				                                        $sqlsortMenu2 = $sql2." ORDER BY menuSort ASC";
				                                        $queryMenu2 = query($sqlsortMenu2);
				                                        $xx2 = 1;
				                                        while ( $pm2 = fetch($queryMenu2)) {
				                                        	echo "<option value=\"2-{$pm2['menuId']}\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$xx1}.{$xx2}. {$pm2['menuName']}</option>";

				                                        	$sql3 = "SELECT * FROM menu_website WHERE menuParentId = '{$pm2['menuId']}' AND menuParentId != '0'";
				                                            $querynumMenu3 = query($sql3);
				                                            $numnumMenu3 = rows($querynumMenu3);
				                                            if($numnumMenu3>0){
				                                                $sqlsortMenu3 = $sql3." ORDER BY menuSort ASC";
				                                                $queryMenu3 = query($sqlsortMenu3);
				                                                $xx3 = 1;
				                                                while ( $pm3 = fetch($queryMenu3)) {
				                                                	echo "<option value=\"3-{$pm3['menuId']}\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$xx1}.{$xx2}.{$xx3} {$pm3['menuName']}</option>";
				                                                	$xx3++;
				                                                }
				                                            }
				                                            $xx2++;
				                                        }
				                                    }
				                                    $xx1++;
			                                    }
			                                ?>
			                            </select>
			                        </div>

			                        <div class="form-group">
			                            <label for="nama_menu"><?php echo _('menuweb_menunameform'); ?> <span class="required">*</span></label>
			                            <input type="text" class="form-control" id="nama_menu" name="nama_menu" required />
			                        </div>

			                        <div class="form-group">
			                            <label for="menu_akses"><?php echo _('menuweb_accessmethodform'); ?></label>

			                            <select class="form-control" id="menu_akses" name="menu_akses" onchange="linktype(this.value);">
			                                <option value="pagecontent_link" selected="selected">Page Content</option>
			                                <option value="newscategory_link">News: Category</option>
			                                <option value="out_link"><?php echo _('menuweb_optionexternallink'); ?></option>
			                                <option value="in_link"><?php echo _('menuweb_optioninterternallink'); ?></option>
			                                <option value="product_link"><?php echo _('menuweb_optionproduct'); ?></option>
			                                <option value="productcategory_link"><?php echo _('menuweb_optionproductcat'); ?></option>
			                                <option value="no_link"><?php echo _('menuweb_optionnoaccess'); ?></option>
			                            </select>
			                        </div>

			                        <div class="form-group">
			                            <div id="pagecontentlink">
			                                <label for="pagecontent">Page Content <span class="required">*</span></label>
			                                <select class="form-control" id="pagecontent" name="pagecontent" required>
			                                    <option value="">-- <?php echo _('menuweb_choosepgctn'); ?> Page Content --</option>
			                                    <?php 
			                                        $queryinduk = query("SELECT * FROM content WHERE contentDisplay='y' ORDER BY contentTitle ASC");
			                                        while ($mod = fetch($queryinduk)) {
			                                            echo "<option value=\"{$mod['contentId']}\">{$mod['contentTitle']}</option>";
			                                        }
			                                    ?>
			                                </select>
			                            </div>

			                            <div id="newscategorylink">
			                                <label for="pagecontent">News: Category <span class="required">*</span></label>
			                                <select class="form-control" id="newscategory" name="newscategory" disabled="disabled" required>
			                                    <option value="">- <?php echo _('menuweb_choosepgctn'); ?> Category --</option>
			                                    <?php 
			                                        $querycat = query("SELECT * FROM newsinfo_cat ORDER BY catName ASC");
			                                        while ($newscat = fetch($querycat)) {
			                                            echo "<option value=\"{$newscat['catId']}\">{$newscat['catName']}</option>";
			                                        }
			                                    ?>
			                                </select>
			                            </div>

			                            <div id="urllink">
			                                <label for="outlink">URL <span class="required">*</span></label>
			                                <input type="url" class="form-control" id="outlink" disabled="disabled" name="outlink" required />
			                                <span class="help-block"><?php echo _('menuweb_example'); ?>: http://myurl.com/</span>
			                            </div>
			                            <div id="urllinkin" style="display:none">
			                            	<div class="row">
				                                <div class="col-xs-12 col-md-12">
				                                <label for="inlink">URL <span class="required">*</span></label>
				                                </div>
				                                <div class="col-xs-12 col-md-7">
				                                <span class="help-block">http://<?php echo getconfig(SITE_URL); ?>/</span> 
				                                </div>
				                                <div class="col-xs-12 col-md-5">
				                                <input type="text" class="form-control" id="inlink" disabled="disabled" name="inlink" required />
				                           		</div>
			                           		</div>
			                            </div>

			                            <div id="productlink">
			                                <label for="product"><?php echo _('menuweb_productname'); ?> <span class="required">*</span></label>
			                                <select class="form-control" id="product" name="product" disabled="disabled" required>
			                                    <option value="">-- <?php echo _('menuweb_choosepgctn')." ". _('menuweb_productname'); ?> --</option>
			                                    <?php 
			                                        $queryprod = query("SELECT prodId,prodName,prodCode FROM product WHERE prodDisplay='y' ORDER BY prodName ASC");
			                                        while ($p = fetch($queryprod)) {
			                                            echo "<option value=\"{$p['prodId']}\">{$p['prodName']} ({$p['prodCode']})</option>";
			                                        }
			                                    ?>
			                                </select>
			                            </div>

			                            <div id="productcategorylink">
			                                <label for="productcategory"><?php echo _('menuweb_productcatname'); ?> <span class="required">*</span></label>
			                                <select class="form-control" id="productcategory" name="productcategory" disabled="disabled" required>
			                                    <option value="">-- <?php echo _('menuweb_choosepgctn')." ". _('menuweb_productcatname'); ?> --</option>
												<?php
													$no=1;
													$sql="SELECT * FROM productcat WHERE catLevel=1 AND catId!=1 ORDER BY catName ASC";
													$query=query($sql);
													while ($data=fetch($query)){
													$data=output($data);?>
													<option value="<?php print $data['catId']; ?>"><?php  print $no;?>. &nbsp; <?php print $data['catName'] ?></option>
												<?php
													$nosub=1;
													$sql1="SELECT * FROM productcat WHERE catLevel=2 and subCat='" . $data['catId']. "'  ORDER BY catSort ASC";
													$query1=query($sql1);
													while ($data1=fetch($query1)){ ?>
													<option value="<?php print $data1['catId']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php  print $no . "." . $nosub;?>. &nbsp; <?php print $data1['catName'] ?></option>
												<?php
													$nosub2=1;
													$sql2="SELECT * FROM productcat WHERE catLevel=3 and subCat='" . $data1['catId']. "'  ORDER BY catName ASC";
													$query2=query($sql2);
													while ($data2=fetch($query2)){ ?>
													<option value="<?php print $data2['catId']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php  print $no . "." . $nosub . "." . $nosub2;?>. &nbsp; <?php print $data2['catName'] ?></option>
												<?php
													$nosub2++;
													}
													$nosub++;
													}
													$no++;
													}
												?>
			                                </select>
			                            </div>
			                        </div>

			                        <hr/>
			                        <div class="form-group">
			                            <button class="btn btn-primary" type="submit"><?php echo _('cms_addbutton'); ?></button>
			                        </div>
			                    </section>
			                </form>
		                </div>
		            </div>
				</div>
            	<?php } ?>
	            
            	<div class="col-md-8">
                	<div class="x_panel tile">
						<div class="x_title">
	            			<h2><?php echo _('menuweb_menusetting'); ?></h2>
	            			<ul class="nav navbar-right panel_toolbox">
			                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
			                </ul>
	            			<div class="clearfix"></div>
	            		</div>

	            		<div class="x_content">
	            		<div class="row">

		                    <div class="col-md-12">
		                    <?php
		                        if(isset($_SESSION['ubah_sukses'])=='ok'){
		                            echo "<div class=\"alert alert-success\">
		                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>
		                                <i class=\"fa fa-check\"></i> "._('menuweb_successedit')."
		                                </div>";
		                            unset($_SESSION['ubah_sukses']);
		                        }
		                        if(isset($_SESSION['add_sukses'])=='ok'){
		                            echo "<div class=\"alert alert-success\">
		                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>
		                                <i class=\"fa fa-check\"></i> "._('menuweb_successadd')."
		                                </div>";
		                            unset($_SESSION['add_sukses']);
		                        }
		                        if(isset($_SESSION['del_sukses'])=='ok'){
		                            echo "<div class=\"alert alert-success\">
		                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>
		                                <i class=\"fa fa-check\"></i> "._('menuweb_successdelete')."
		                                </div>";
		                            unset($_SESSION['del_sukses']);
		                        }
		                        if(isset($_SESSION['del_nok'])=='ok'){
		                        	 echo "<div class=\"alert alert-danger\">
		                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>
		                                <i class=\"fa fa-exclamation-triangle\"></i> "._('error_97')."
		                                </div>";
		                            unset($_SESSION['del_nok']);
		                        }
		                    ?>
		                    </div>

		                    <form <?php if($uac_edit){ ?>action="menu_web.php?action=updatesortmenu&form=submit"<?php } ?> method="post" enctype="multipart/form-data" name="edit" id="edit">

		                    <div class="col-md-12">
		                        <div id="nestable-menu">
		                        	<div class="pull-left">
		                            	<button class="btn btn-primary" type="submit" <?php if(!$uac_edit){ ?>disabled<?php } ?>><i class="fa fa-refresh"></i> <?php echo _('cms_updatebutton'); ?></button>
		                            </div>
		                            <div class="pull-right">
		                                <button type="button" class="btn btn-warning btn-xs" data-action="expand-all"><span class="arrow_expand"></span> Expand All</button>
		                                <button type="button" class="btn btn-danger btn-xs" data-action="collapse-all"><span class="arrow_condense"></span> Collapse All</button>
		                            </div>
		                            
		                            <div class="clearfix"></div>
		                        </div>
		                        <hr/>
		                    </div>

		                    <div class="col-sm-12">
		                        <section>
		                        <div class="dd" id="nestable" style="clear:both;">
		                            <ol class="dd-list">
		                                <?php
		                                $sqlMenu1 = "SELECT * FROM menu_website WHERE menuParentId = '0' ORDER BY menuSort ASC";
		                                $queryMenu1 = query($sqlMenu1);
		                                while ( $dm1 = fetch($queryMenu1)) {
		                                	if($dm1['menuAccessType']=='pagecontent_link'){ $menuType1 = "Page Content"; }
		                                	if($dm1['menuAccessType']=='newscategory_link'){ $menuType1 = "News: Category"; }
		                                	if($dm1['menuAccessType']=='out_link'){ $menuType1 = _('menuweb_optionexternallink'); }
		                                	if($dm1['menuAccessType']=='in_link'){ $menuType1 = _('menuweb_optioninterternallink'); }
		                                	if($dm1['menuAccessType']=='no_link'){ $menuType1 = _('menuweb_optionnoaccess'); }
		                                	if($dm1['menuAccessType']=='product_link'){ $menuType1 = _('menuweb_optionproduct'); }
		                                	if($dm1['menuAccessType']=='productcategory_link'){ $menuType1 = _('menuweb_optionproductcat'); }

		                                    echo "<li class=\"dd-item\" data-id=\"{$dm1['menuId']}\">
		                                    	<div class=\"dd-handle\">{$dm1['menuName']} <span style=\"font-weight:normal;\">({$menuType1})</span></div>
		                                    	<div class=\"nestable-ctn\">";
		                                    	if(count($lc_lang)>1){
		                                 ?>
																			<?php
																			foreach($lc_lang as $key=>$value){
																				$exist=countdata("menu_website_lang","menuId='{$dm1['menuId']}' and lang='$value'");
																				if($value==$lc_lang_default) continue;
																				if($exist and $uac_edit){
																			?>
																				<a href="menu_web.php?action=editen&amp;lang=<?php print $value;?>&id=<?php print $dm1['menuId']; ?>" rel="tooltip" title="<?php print _('pagecontent_editvers'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-edit.png" alt="<?php print $value;?>"  border="0" /></a> |
																			<?php } elseif($uac_add){ ?>
																				<a href="menu_web.php?action=adden&amp;lang=<?php print $value;?>&id=<?php print $dm1['menuId']; ?>" rel="tooltip" title="<?php print _('pagecontent_addvers'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-add.png" alt="<?php print $value;?>" border="0" /></a> |
																			<?php } }?>
		                                 <?php } ?>
		                                 <?php 
		                                    	echo "
                                            		" . (($uac_edit) ? "<a href=\"menu_web.php?id={$dm1['menuId']}&amp;action=edit\">"._('cms_edit')."</a> | ":"") ."
                                            		" . (($uac_delete) ? "<a href=\"javascript: if (window.confirm('" . _('cms_delete_word') . " ".$dm1['menuName']."')){ window.location='menu_web.php?form=submit&amp;action=del&amp;id={$dm1['menuId']}'; };\">"._('cms_delete')."</a>":"") . "
		                                        </div>";

		                                    $sqlMenu2 = "SELECT * FROM menu_website WHERE menuParentId = '{$dm1['menuId']}' AND menuParentId != '0'";
		                                    $querynumMenu2 = query($sqlMenu2);
		                                    $numnumMenu2 = rows($querynumMenu2);

		                                    if($numnumMenu2>0){
		                                        $sqlsortMenu2 = $sqlMenu2." ORDER BY menuSort ASC";
		                                        $queryMenu2 = query($sqlsortMenu2);

		                                        echo "<ol class=\"dd-list\">";
		                                        while ( $dm2 = fetch($queryMenu2)) {
		                                        	if($dm2['menuAccessType']=='pagecontent_link'){ $menuType2 = "Page Content"; }
				                                	if($dm2['menuAccessType']=='newscategory_link'){ $menuType2 = "News: Category"; }
				                                	if($dm2['menuAccessType']=='out_link'){ $menuType2 = _('menuweb_optionexternallink'); }
				                                	if($dm2['menuAccessType']=='in_link'){ $menuType2 = _('menuweb_optioninterternallink'); }
				                                	if($dm2['menuAccessType']=='no_link'){ $menuType2 = _('menuweb_optionnoaccess'); }
		                                			if($dm2['menuAccessType']=='product_link'){ $menuType2 = _('menuweb_optionproduct'); }
		                                			if($dm2['menuAccessType']=='productcategory_link'){ $menuType2 = _('menuweb_optionproductcat'); }

		                                            echo "<li class=\"dd-item\" data-id=\"{$dm2['menuId']}\">
		                                            	<div class=\"dd-handle\">{$dm2['menuName']} <span style=\"font-weight:normal;\">({$menuType2})</span></div>
		                                            	<div class=\"nestable-ctn\">";
		                                            	if(count($lc_lang)>1){
		                                 ?>
																			<?php
																			foreach($lc_lang as $key=>$value){
																				$exist=countdata("menu_website_lang","menuId='{$dm2['menuId']}' and lang='$value'");
																				if($value==$lc_lang_default) continue;
																				if($exist and $uac_edit){
																			?>
																				<a href="menu_web.php?action=editen&amp;lang=<?php print $value;?>&id=<?php print $dm2['menuId']; ?>" rel="tooltip" title="<?php print _('pagecontent_editvers'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-edit.png" alt="<?php print $value;?>"  border="0" /></a> |
																			<?php } elseif($uac_add){ ?>
																				<a href="menu_web.php?action=adden&amp;lang=<?php print $value;?>&id=<?php print $dm2['menuId']; ?>" rel="tooltip" title="<?php print _('pagecontent_addvers'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-add.png" alt="<?php print $value;?>" border="0" /></a> |
																			<?php } }?>
		                                 <?php } ?>
		                                 <?php 
		                                    	echo "
		                                            		" . (($uac_edit) ? "<a href=\"menu_web.php?id={$dm2['menuId']}&amp;action=edit\">"._('cms_edit')."</a> | ":"") ."
		                                            		" . (($uac_delete) ? "<a href=\"javascript: if (window.confirm('" . _('cms_delete_word') . " ".$dm2['menuName']."')){ window.location='menu_web.php?form=submit&amp;action=del&amp;id={$dm2['menuId']}'; };\">"._('cms_delete')."</a>":"") . "
		                                            	</div>";

		                                            $sqlMenu3 = "SELECT * FROM menu_website WHERE menuParentId = '{$dm2['menuId']}' AND menuParentId != '0'";
		                                            $querynumMenu3 = query($sqlMenu3);
		                                            $numnumMenu3 = rows($querynumMenu3);
		                                            if($numnumMenu3>0){
		                                                $sqlsortMenu3 = $sqlMenu3." ORDER BY menuSort ASC";
		                                                $queryMenu3 = query($sqlsortMenu3);
		                                                echo "<ol class=\"dd-list\">";
		                                                while( $dm3 = fetch($queryMenu3)){
		                                                	if($dm3['menuAccessType']=='pagecontent_link'){ $menuType3 = "Page Content"; }
						                                	if($dm3['menuAccessType']=='newscategory_link'){ $menuType3 = "News: Category"; }
						                                	if($dm3['menuAccessType']=='out_link'){ $menuType3 = _('menuweb_optionexternallink'); }
						                                	if($dm3['menuAccessType']=='in_link'){ $menuType3 = _('menuweb_optioninterternallink'); }
						                                	if($dm3['menuAccessType']=='no_link'){ $menuType3 = _('menuweb_optionnoaccess'); }
						                                	if($dm3['menuAccessType']=='product_link'){ $menuType3 = _('menuweb_optionproduct'); }
		                                					if($dm3['menuAccessType']=='productcategory_link'){ $menuType3 = _('menuweb_optionproductcat'); }

		                                                    echo "<li class=\"dd-item\" data-id=\"{$dm3['menuId']}\"><div class=\"dd-handle\">{$dm3['menuName']} <span style=\"font-weight:normal;\">({$menuType3})</span></div>
				                                            	<div class=\"nestable-ctn\">
				                                            	";
		                                            	if(count($lc_lang)>1){
		                                 ?>
																			<?php
																			foreach($lc_lang as $key=>$value){
																				$exist=countdata("menu_website_lang","menuId='{$dm3['menuId']}' and lang='$value'");
																				if($value==$lc_lang_default) continue;
																				if($exist and $uac_edit){
																			?>
																				<a href="menu_web.php?action=editen&amp;lang=<?php print $value;?>&id=<?php print $dm3['menuId']; ?>" rel="tooltip" title="<?php print _('pagecontent_editvers'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-edit.png" alt="<?php print $value;?>"  border="0" /></a> |
																			<?php } elseif($uac_add){ ?>
																				<a href="menu_web.php?action=adden&amp;lang=<?php print $value;?>&id=<?php print $dm3['menuId']; ?>" rel="tooltip" title="<?php print _('pagecontent_addvers'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-add.png" alt="<?php print $value;?>" border="0" /></a> |
																			<?php } }?>
		                                 <?php } ?>
		                                 <?php 
		                                    	echo "
				                                            		" . (($uac_edit) ? "<a href=\"menu_web.php?id={$dm3['menuId']}&amp;action=edit\">"._('cms_edit')."</a> | ":"") ."
				                                            		" . (($uac_delete) ? "<a href=\"javascript: if (window.confirm('" . _('cms_delete_word') . " ".$dm3['menuName']."')){ window.location='menu_web.php?form=submit&amp;action=del&amp;id={$dm3['menuId']}'; };\">"._('cms_delete')."</a>":"") . "
				                                            	</div>";
				                                            
		                                                    echo "</li>";
		                                                }
		                                                echo "</ol>";
		                                            }
		                                            echo "</li>";
		                                        }
		                                        echo "</ol>";
		                                    }
		                                    echo "</li>";
		                                }
		                                ?>
		                            </ol>
		                        </div>
		                        </section>
		                                                
		                    </div>

		                    <div class="col-md-12">
		                        <input type="hidden" id="nestable-output" name="menu_data" />
		                        <hr/>
		                        <div class="form-group">
		                            <button class="btn btn-primary" name="Submit" value="<?php echo _('cms_updatebutton'); ?>" type="submit" <?php if(!$uac_edit){ ?>disabled<?php } ?>><i class="fa fa-refresh"></i> <?php echo _('cms_updatebutton'); ?></button>
		                        </div>
		                    </div>

		                    </form>

		                </div>
		                </div>
		                <!-- END .x_content -->
		            </div>
		            <!-- END .x_panel -->
	            </div>
	            
	            <?php } elseif($action=="edit" AND $uac_edit) { 
	            	$sql="SELECT * FROM menu_website WHERE menuId='" . $id . "'";
					$query=query($sql);
					$data=fetch($query);
					$data=formoutput($data);
	            ?>
	            <div class="col-md-12">
	            	<a href="menu_web.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('menuweb_editdesc'); ?></h4>
					<hr/>
					<form id="edit" name="edit" method="post" action="menu_web.php?form=submit&amp;action=edit&amp;id=<?php print $id; ?>" class="form-horizontal form-label-left">
						<div class="form-group">
                            <label for="induk" class="control-label col-md-2 col-sm-2 col-xs-12"><?php echo _('menuweb_parentform'); ?></label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
	                            <select class="form-control" id="induk" name="induk">
	                                <option value="0-0"><?php echo _('menuweb_noparent'); ?></option>
	                                <?php 
	                                    $queryinduk = query("SELECT * FROM menu_website WHERE menuParentId = '0' ORDER BY menuSort ASC");
	                                    $xx1 = 1;
	                                    while ($pm1 = fetch($queryinduk)) {
	                                        echo "<option value=\"1-{$pm1['menuId']}\"";
	                                        if($pm1['menuId'] == $data['menuParentId']){
	                                        	echo " selected=\"selected\"";
	                                        }
	                                        if($pm1['menuId'] == $data['menuId']){
	                                        	echo " disabled=\"disabled\"";
	                                        }
	                                        echo ">{$xx1}. {$pm1['menuName']}</option>";

	                                        $sql2 = "SELECT * FROM menu_website WHERE menuParentId = '{$pm1['menuId']}' AND menuParentId != '0'";
	                                        $querynumMenu2 = query($sql2);
		                                    $numnumMenu2 = rows($querynumMenu2);

		                                    if($numnumMenu2>0){
		                                        $sqlsortMenu2 = $sql2." ORDER BY menuSort ASC";
		                                        $queryMenu2 = query($sqlsortMenu2);
		                                        $xx2 = 1;
		                                        while ( $pm2 = fetch($queryMenu2)) {
		                                        	echo "<option value=\"2-{$pm2['menuId']}\"";
			                                        if($pm2['menuId'] == $data['menuParentId']){
			                                        	echo " selected=\"selected\"";
			                                        }
			                                        if($pm2['menuId'] == $data['menuId']){
			                                        	echo " disabled=\"disabled\"";
			                                        }
			                                        echo ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$xx1}.{$xx2}. {$pm2['menuName']}</option>";

		                                        	$sql3 = "SELECT * FROM menu_website WHERE menuParentId = '{$pm2['menuId']}' AND menuParentId != '0'";
		                                            $querynumMenu3 = query($sql3);
		                                            $numnumMenu3 = rows($querynumMenu3);
		                                            if($numnumMenu3>0){
		                                                $sqlsortMenu3 = $sql3." ORDER BY menuSort ASC";
		                                                $queryMenu3 = query($sqlsortMenu3);
		                                                $xx3 = 1;
		                                                while ( $pm3 = fetch($queryMenu3)) {
		                                                	echo "<option value=\"3-{$pm3['menuId']}\"";
					                                        if($pm3['menuId'] == $data['menuParentId']){
					                                        	echo " selected=\"selected\"";
					                                        }
					                                        if($pm3['menuId'] == $data['menuId']){
					                                        	echo " disabled=\"disabled\"";
					                                        }
					                                        echo ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$xx1}.{$xx2}.{$xx3} {$pm3['menuName']}</option>";
		                                                	$xx3++;
		                                                }
		                                            }
		                                            $xx2++;
		                                        }
		                                    }
		                                    $xx1++;
	                                    }
	                                ?>
	                            </select>
	                        </div>
                        </div>

                        <div class="form-group">
                            <label for="nama_menu" class="control-label col-md-2 col-sm-2 col-xs-12"><?php echo _('menuweb_menunameform'); ?> <span class="required">*</span></label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                            	<input type="text" class="form-control" id="nama_menu" name="nama_menu" required value="<?php echo $data['menuName']; ?>" />
                        	</div>
                        </div>

                        <div class="form-group">
                            <label for="menu_akses" class="control-label col-md-2 col-sm-2 col-xs-12"><?php echo _('menuweb_accessmethodform'); ?></label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
	                            <select class="form-control" id="menu_akses" name="menu_akses" onchange="linktype(this.value);">
	                                <option value="pagecontent_link"<?php if($data['menuAccessType']=='pagecontent_link'){ echo ' selected="selected"'; } ?>>Page Content</option>
	                                <option value="newscategory_link"<?php if($data['menuAccessType']=='newscategory_link'){ echo ' selected="selected"'; } ?>>News: Category</option>
	                                <option value="out_link"<?php if($data['menuAccessType']=='out_link'){ echo ' selected="selected"'; } ?>><?php echo _('menuweb_optionexternallink'); ?></option>
	                                <option value="in_link"<?php if($data['menuAccessType']=='in_link'){ echo ' selected="selected"'; } ?>><?php echo _('menuweb_optioninterternallink'); ?></option>
	                                <option value="product_link"<?php if($data['menuAccessType']=='product_link'){ echo ' selected="selected"'; } ?>><?php echo _('menuweb_optionproduct'); ?></option>
	                                <option value="productcategory_link"<?php if($data['menuAccessType']=='productcategory_link'){ echo ' selected="selected"'; } ?>><?php echo _('menuweb_optionproductcat'); ?></option>
	                                <option value="no_link"<?php if($data['menuAccessType']=='no_link'){ echo ' selected="selected"'; } ?>><?php echo _('menuweb_optionnoaccess'); ?></option>
	                            </select>
	                        </div>
                        </div>

                        <div class="form-group">
                            <div id="pagecontentlink"<?php if($data['menuAccessType']=='pagecontent_link'){ echo ' style="display:block;"'; } else { echo ' style="display:none;"'; } ?>>
                                <label for="pagecontent" class="control-label col-md-2 col-sm-2 col-xs-12">Page Content <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
	                                <select class="form-control" id="pagecontent" name="pagecontent" required>
	                                    <option value="">-- <?php echo _('menuweb_choosepgctn'); ?> Page Content --</option>
	                                    <?php 
	                                        $queryinduk = query("SELECT * FROM content WHERE contentDisplay='y' ORDER BY contentTitle ASC");
	                                        while ($mod = fetch($queryinduk)) {
	                                            echo "<option value=\"{$mod['contentId']}\"";
	                                            if($mod['contentId']==$data['menuRelationshipId']){
	                                            	echo " selected=\"selected\"";
	                                            }
	                                            echo ">{$mod['contentTitle']}</option>";
	                                        }
	                                    ?>
	                                </select>
                                </div>
                            </div>

                            <div id="newscategorylink"<?php if($data['menuAccessType']=='newscategory_link'){ echo ' style="display:block;"'; } else { echo ' style="display:none;"'; } ?>>
                                <label for="pagecontent"  class="control-label col-md-2 col-sm-2 col-xs-12">News: Category <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
	                                <select class="form-control" id="newscategory" name="newscategory" required>
	                                    <option value="">- <?php echo _('menuweb_choosepgctn'); ?> Category --</option>
	                                    <?php 
	                                        $querycat = query("SELECT * FROM newsinfo_cat ORDER BY catName ASC");
	                                        while ($newscat = fetch($querycat)) {
	                                            echo "<option value=\"{$newscat['catId']}\"";
	                                            if($newscat['catId']==$data['menuRelationshipId']){
	                                            	echo " selected=\"selected\"";
	                                            }
	                                            echo ">{$newscat['catName']}</option>";
	                                        }
	                                    ?>
	                                </select>
                                </div>
                            </div>

                            <div id="urllink"<?php if($data['menuAccessType']=='out_link'){ echo ' style="display:block;"'; } else { echo ' style="display:none;"'; } ?>>
                                <label for="outlink" class="control-label col-md-2 col-sm-2 col-xs-12">URL <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
	                                <input type="url" class="form-control" id="outlink" <?php if($data['menuAccessType']=='out_link'){ echo ' required value="'.$data['menuUrlAccess'].'"'; } else { echo ' disabled="disabled"'; } ?> name="outlink"  />
	                                <span class="help-block"><?php echo _('menuweb_example'); ?>: http://myurl.com/</span>
	                            </div>
                            </div>
                            <div id="urllinkin"<?php if($data['menuAccessType']=='in_link'){ echo ' style="display:block;"'; } else { echo ' style="display:none;"'; } ?>>
                                <label for="inlink" class="control-label col-md-2 col-sm-2 col-xs-12">URL <span class="required">*</span></label>
                                <div class="col-md-2 col-sm-6 col-xs-12">
	           
	                                <span class="help-block"><?php echo getconfig(SITE_URL); ?>/</span>
	                            </div>
                                <div class="col-md-2 col-sm-6 col-xs-12">
	                                <input type="text" class="form-control" id="inlink" <?php if($data['menuAccessType']=='in_link'){ echo ' required value="'.substr($data['menuUrlAccess'],1).'"'; } else { echo ' disabled="disabled"'; } ?> name="inlink"  />
	                            </div>
                            </div>

                            <div id="productlink"<?php if($data['menuAccessType']=='product_link'){ echo ' style="display:block;"'; } else { echo ' style="display:none;"'; } ?>>
                                <label for="product" class="control-label col-md-2 col-sm-2 col-xs-12"><?php echo _('menuweb_productname'); ?> <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
	                                <select class="form-control" id="product" name="product" required>
	                                    <option value="">-- <?php echo _('menuweb_choosepgctn')." ". _('menuweb_productname'); ?> --</option>
	                                    <?php 
	                                        $queryprod = query("SELECT prodId,prodName,prodCode FROM product WHERE prodDisplay='y' ORDER BY prodName ASC");
	                                        while ($p = fetch($queryprod)) {
	                                            echo "<option value=\"{$p['prodId']}\"";
	                                            if($p['prodId']==$data['menuRelationshipId']){
	                                            	echo " selected=\"selected\"";
	                                            }
		                                        echo ">{$p['prodName']} ({$p['prodCode']})</option>";
	                                        }
	                                    ?>
	                                </select>
                                </div>
                            </div>

                            <div id="productcategorylink"<?php if($data['menuAccessType']=='productcategory_link'){ echo ' style="display:block;"'; } else { echo ' style="display:none;"'; } ?>>
                                <label for="productcategory" class="control-label col-md-2 col-sm-2 col-xs-12"><?php echo _('menuweb_productcatname'); ?> <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
	                                <select class="form-control" id="productcategory" name="productcategory" required>
	                                    <option value="">-- <?php echo _('menuweb_choosepgctn')." ". _('menuweb_productcatname'); ?> --</option>
										<?php
											$noc=1;
											$sqlc="SELECT * FROM productcat WHERE catLevel=1 AND catId!=1 ORDER BY catName ASC";
											$queryc=query($sqlc);
											while ($datac=fetch($queryc)){
											$datac=output($datac);?>
											<option value="<?php print $datac['catId']; ?>"<?php 
												if($datac['catId']==$data['menuRelationshipId']){
	                                            	echo " selected=\"selected\"";
	                                            }
	                                        ?>><?php  print $noc;?>. &nbsp; <?php print $datac['catName'] ?></option>
										<?php
											$nosub=1;
											$sqlc1="SELECT * FROM productcat WHERE catLevel=2 and subCat='" . $datac['catId']. "'  ORDER BY catName ASC";
											$queryc1=query($sqlc1);
											while ($datac1=fetch($queryc1)){ ?>
											<option value="<?php print $datac1['catId']; ?>"<?php 
												if($datac1['catId']==$data['menuRelationshipId']){
	                                            	echo " selected=\"selected\"";
	                                            }
	                                        ?>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php  print $noc . "." . $nosub;?>. &nbsp; <?php print $datac1['catName'] ?></option>
										<?php
											$nosub2=1;
											$sqlc2="SELECT * FROM productcat WHERE catLevel=3 and subCat='" . $datac1['catId']. "'  ORDER BY catName ASC";
											$queryc2=query($sqlc2);
											while ($datac2=fetch($queryc2)){ ?>
											<option value="<?php print $datac2['catId']; ?>"<?php 
												if($datac2['catId']==$data['menuRelationshipId']){
	                                            	echo " selected=\"selected\"";
	                                            }
	                                        ?>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php  print $noc . "." . $nosub . "." . $nosub2;?>. &nbsp; <?php print $datac2['catName'] ?></option>
										<?php
											$nosub2++;
											}
											$nosub++;
											}
											$noc++;
											}
										?>
	                                </select>
	                            </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="active" class="control-label col-md-2 col-sm-2 col-xs-12"><?php echo _('menuweb_menuactiveform'); ?></label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
	                            <div class="checkbox">
									<label>
										<input type="checkbox" name="status" id="status" value="y"<?php if($data['menuActive']=='y'){ echo ' checked="checked"'; } ?>> <?php echo _('menuweb_activevalue'); ?>
									</label>
								</div>
	                        </div>
                        </div>

                        <hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_updatebutton'); ?>"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='menu_web.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>
					</form>
				</div>
	            <?php } elseif ($action=="adden" and $uac_add){  ?>
	            <div class="col-md-12">
	            	<a href="menu_web.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        			<h4><?php print _('menuweb_lang_add'); ?></h4>
								<hr/>
								<form id="add" name="add" method="post" action="menu_web.php?form=submit&amp;action=adden&amp;id=<?php print $id; ?>&amp;lang=<?php print $lang ?>" class="form-horizontal form-label-left">
									<div class="form-group">
		                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">
		                	<?php print _('menuweb_menunameform'); ?> <span class="required">*</span>
		                </label>
		                <div class="col-md-6 col-sm-6 col-xs-12">
		                	<input name="name" type="text" id="name" class="form-control" required>
			              </div>
		              </div>
		              <div class="form-group">
		                <div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
		                <div class="col-md-6 col-sm-6 col-xs-12">
		                	<button name="Submit" type="submit" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
		                	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='menu_web.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                </div>
		            	</div>
								</form>
								<br/><br/><br/>
								</div>
								
	            <?php } elseif ($action=="editen" and $uac_edit){ 
	            $sql="SELECT * FROM menu_website_lang WHERE menuId='$id' AND lang= '$lang'";
							$query=query($sql);
							$data=fetch($query);
							$data=formoutput($data);
	             ?>
	            	<div class="col-md-12">
	            	<a href="menu_web.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        			<h4><?php print _('menuweb_lang_add'); ?></h4>
								<hr/>
								<form id="edit" name="edit" method="post" action="menu_web.php?form=submit&amp;action=editen&amp;id=<?php print $id; ?>&amp;lang=<?php print $lang ?>" class="form-horizontal form-label-left">
									<div class="form-group">
		                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">
		                	<?php print _('menuweb_menunameform'); ?> <span class="required">*</span>
		                </label>
		                <div class="col-md-6 col-sm-6 col-xs-12">
		                	<input name="name" type="text" id="name" class="form-control" value="<?php print $data['menuName']; ?>" required>
			              </div>
		              </div>
		              <div class="form-group">
		                <div class="control-label col-md-2 col-sm-3 col-xs-12"></div>
		                <div class="col-md-6 col-sm-6 col-xs-12">
		                	<button name="Submit" type="submit" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
		                	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='menu_web.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
		                	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
		                </div>
		            	</div>
								</form>
								</div>
								<br/><br/><br/>
	      	<?php } ?>
			</div>

        </div>
        <!-- END THE CONTENT OF PAGE HERE -->

        <?php include("com/com-footer.php"); ?>

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>
