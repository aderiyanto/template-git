<?php
	$page=10;
	
	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");
	
	if ($form=="submit"){
	//if any form is submitted
		if ($action=="addcat" and $uac_add){
			if (empty($name)){
				$error=errorlist(2);
			}
			if (!$error){
				$nextid=nextid("catId","admin_menu_cat");
				$sql="INSERT INTO admin_menu_cat VALUES ($nextid,'$name','$nextid','$iconmenu')";
				$query=query($sql);
				 		
				//auto addd cat for web developer
				$sql="INSERT INTO admin_group_menu_cat VALUE('1','$nextid')";
				$query=query($sql);
			}
			
		}elseif ($action=="editcat" and $uac_edit){
			if (empty($name)){
				$error=errorlist(2);
			}
			if (!$error){
				$sql="UPDATE admin_menu_cat SET catName='$name',catMenuIcon='$iconmenu' WHERE catId='" . $id . "'";
				$query=query($sql);
			}
		}elseif ($action=="delcat" and $uac_delete){
			if (empty($id)){
				$error=errorlist(2);
			}
			
			$sql="SELECT COUNT(*) AS total FROM admin_menu WHERE catId=". $id;
			$query=query($sql);
			$data=fetch($query);
			$total=$data['total'];
			
			if ($total>0){
				$error=errorlist(14);
			}else{
				$sql="DELETE FROM admin_menu_cat WHERE catId='" . $id . "'";
				$query=query($sql);
			}
		}elseif ($action=="add" and $uac_add){
			if (empty($name) or empty($catid) or empty($file) or empty($view)){
				$error=errorlist(2);
			}
			
			$nextid=nextid("menuId","admin_menu");
			$nextsort=nextsort("admin_menu","catId","$catid","menuSort");
			
			if (empty($add)){
				$add='n';
			}
			if (empty($edit)){
				$edit='n';
			}
			if (empty($delete)){
				$delete='n';
			}
			if (!$error){	
				$sql="INSERT INTO admin_menu VALUES ($nextid,$catid,'$name','$file',$nextsort,'$view','$add','$edit','$delete')";
				$query=query($sql);
				
				if($query){
					//auto addd menu for web developer
					$sql="INSERT INTO admin_group_menu VALUES('1','$nextid','$view','$add','$edit','$delete')";
					$query=query($sql);
				}
			}			
		}elseif ($action=="edit" and $uac_edit){
			if (empty($name) or empty($catid) or empty($file) or empty($view) or empty($id)){
				$error=errorlist(2);
			}
			
			if (empty($add)){
				$add='n';
			}
			if (empty($edit)){
				$edit='n';
			}
			if (empty($delete)){
				$delete='n';
			}
			if (!$error){
				$sql="UPDATE admin_menu SET catId=$catid, menuName='$name', menuFile='$file', menuView='$view', menuAdd='$add', menuEdit='$edit', menuDelete='$delete' WHERE menuId='$id'";
				$query=query($sql);
			}
		}elseif ($action=="del" and $uac_delete){
			if (empty($id)){
				$error=errorlist(2);
			}else{
				$sql="DELETE FROM admin_group_menu WHERE menuId='" . $id . "'";
				$query=query($sql);
				
				$sql="DELETE FROM admin_menu WHERE menuId='" . $id . "'";
				$query=query($sql);
			}
		}elseif ($action=="update"){
			foreach ($s as $id=>$sortnum){
				$sql="UPDATE admin_menu_cat SET catSort=$sortnum WHERE catId='" . $id . "'";
				$query=query($sql);
			}
			foreach ($sm as $idm=>$msortnum){
				$sql="UPDATE admin_menu SET menuSort=$msortnum WHERE menuId=" . $idm;
				$query=query($sql);
			}
		}
 
		//check whether query was successful
		if(!$query){
			$error .=errorlist(3);
		}
		
		if($error){		
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}
		else{
			if(!$js){
				header("location:admin_menu.php");
			}else{
				print "ok";
			}
		}		
		exit;	
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _('admin_menu_pagetitle'); ?></title>
<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>

<script type="text/javascript"> 
jQuery(document).ready(function($){
	$('#result').hide();
	validate('#result','#add','admin_menu.php');
	validate('#result','#edit','admin_menu.php');	
});
</script>

</head>
<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('admin_menu_pagetitle'); ?></h3>
			            </div>
		            </div>
	        	</div>
        	</div>

        	<?php if ($uac_add){ ?>
        	<div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                	<div class="pull-right">
                		<a href="admin_menu.php?action=add" class="btn btn-primary"><i class="fa fa-plus"></i> <?php print _('admin_menu_addmenu'); ?></a>
                		<a href="admin_menu.php?action=addcat" class="btn btn-primary"><i class="fa fa-plus"></i> <?php print _('admin_menu_addcategory'); ?></a>
                	</div>
                </div>
        	</div>
        	<?php } ?>

        	<div class="row">
	        	<div class="col-md-12">
	        		<?php if ($action==""){ ?>
	        		<form action="admin_menu.php?form=submit&amp;action=update" method="post" name="sort">
	        			<div class="block-break">
            				<button type="submit" name="update" value="<?php print _('admin_menu_updateshort'); ?>" class="btn btn-sm btn-info"><i class="fa fa-refresh"></i> <?php print _('admin_menu_updateshort'); ?></button>
            			</div>
            			<div class="table-responsive">
                			<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
                				<thead>
                					<tr>
                						<th><?php print _('admin_menu_short'); ?></th>
									    <th colspan="3"><?php print _('admin_menu_menuandcategory'); ?></th>
									    <th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
										<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
                					</tr>
                				</thead>
                				<tbody>
                					<?php
									  	$sql="SELECT * FROM admin_menu_cat ORDER BY catSort ASC";
										$query=query($sql);
										while ($data=fetch($query)){
											$data=output($data);
									?>
									<tr>
									    <td width="60" class="text-center">
									    	<input type="text" size="3" name="s[<?php print $data['catId']; ?>]" value="<?php print $data['catSort']; ?>" class="text-center form-control" />
									    </td>
									    <td colspan="3"><strong><?php print empty($data['catMenuIcon']) ? "<i class=\"fa fa-cog\"></i> ":"<i class=\"" . $data['catMenuIcon'] . "\"></i> "; ?><?php print $data['catName']; ?></strong></td>
									    <td class="text-center"><?php if ($uac_edit){ ?><a href="admin_menu.php?action=editcat&amp;id=<?php print $data['catId']; ?>" title="<?php print _('cms_edit'); ?>: <?php print stripquote($data['catName']); ?>"><i class="fa fa-edit"></i></a><?php } ?></td>
									    <td class="text-center"><?php if ($uac_delete){ ?><a class="delete" href="admin_menu.php?form=submit&action=delcat&id=<?php print $data['catId']; ?>" title="<?php print stripquote($data['catName']); ?>"><i title="<?php print _('cms_delete'); ?>: <?php print stripquote($data['catName']); ?>" rel="tooltip" class="fa fa-trash-o"></i></a><?php } ?></td>
								    </tr>
							        <?php
								  		$sqlx="SELECT * FROM admin_menu WHERE catId=" . $data['catId'] . " ORDER BY menuSort ASC";
										$queryx=query($sqlx);
										while ($datax=fetch($queryx)){
											$datax=output($datax);
									?>
							        <tr>
									    <td width="60">&nbsp;</td>
							            <td width="70" class="text-center">
							            	<input type="text" size="3" name="sm[<?php print $datax['menuId']; ?>]" value="<?php print $datax['menuSort']; ?>" class="text-center form-control" />
							            </td>
									    <td><?php print $datax['menuName']; ?></td>
							            <td width="150"><?php print $datax['menuFile']; ?></td>
									    <td class="text-center"><?php if ($uac_edit){ ?><a href="admin_menu.php?action=edit&amp;id=<?php print $datax['menuId']; ?>" title="<?php print _('cms_edit'); ?>: <?php print stripquote($datax['menuName']); ?>"><i class="fa fa-edit"></i></a><?php } ?></td>
									    <td class="text-center"><?php if ($uac_delete){ ?><a class="delete" href="admin_menu.php?form=submit&action=del&id=<?php print $datax['menuId']; ?>" title="<?php print stripquote($datax['menuName']); ?>"><i title="<?php print _('cms_delete'); ?>: <?php print stripquote($datax['menuName']); ?>" rel="tooltip" class="fa fa-trash-o"></i></a><?php } ?></td>
								    </tr>
							        <?php
											}
										}
							        ?>
                				</tbody>
                			</table>
                		</div>
	        		</form>
	        		<br/><br/><br/>
	        		<?php }elseif ($action=="addcat" and $uac_add){ ?>
	        		<a href="admin_menu.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('admin_menu_addcategory') ?></h4>
					<hr/>
					<form id="add" name="add" method="post" action="admin_menu.php?form=submit&amp;action=addcat" class="form-horizontal form-label-left">
						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _('admin_menu_menunamecategory'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<input name="name" type="text" id="name" size="50" required class="form-control col-md-7 col-xs-12" />
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="iconmenu"><?php print _('admin_menu_menuiconcategory'); ?>
	                        </label>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<input name="iconmenu" type="text" id="iconmenu" size="50" class="form-control col-md-7 col-xs-12" />
	                        	<span class="help-block"><?php print _('admin_menu_menuicondesccategory'); ?></span>
	                        </div>
                    	</div>

                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='admin_menu.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>
					</form>
					<?php 
		            } elseif ($action=="editcat" and $uac_edit){
		                $sql="SELECT * FROM admin_menu_cat WHERE catId='" . $id . "'";
		                $query=query($sql);
		                $data=fetch($query);
						$data=formoutput($data);
		            ?>
		            <a href="admin_menu.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('admin_menu_editcategory'); ?> - <?php print output($data['catName']); ?></h4>
					<hr/>
					<form id="edit" name="edit" method="post" action="admin_menu.php?form=submit&amp;action=editcat&amp;id=<?php print $id; ?>" class="form-horizontal form-label-left">
						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _('admin_menu_menunamecategory'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<input name="name" type="text" id="name" size="50" value="<?php print $data['catName']; ?>" required class="form-control col-md-7 col-xs-12" />
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="iconmenu"><?php print _('admin_menu_menuiconcategory'); ?>
	                        </label>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<input name="iconmenu" type="text" id="iconmenu" size="50" value="<?php print $data['catMenuIcon']; ?>" required class="form-control col-md-7 col-xs-12" />
	                        	<span class="help-block"><?php print _('admin_menu_menuicondesccategory'); ?></span>
	                        </div>
                    	</div>

                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_updatebutton'); ?>"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='admin_menu.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>
					</form>
					<?php }elseif ($action=="add" and $uac_add){ ?>
					<a href="admin_menu.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('admin_menu_addmenu'); ?></h4>
					<hr/>

					<form id="add" name="add" method="post" action="admin_menu.php?form=submit&amp;action=add" class="form-horizontal form-label-left">
						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="catid"><?php print _('admin_menu_category'); ?>
	                        </label>
	                        <div class="col-md-3 col-sm-6 col-xs-12">
	                        	<select name="catid" id="catid" class="form-control">
					              <?php
								  	$sql="SELECT * FROM admin_menu_cat ORDER BY catSort ASC";
									$query=query($sql);
									while ($data=fetch($query)){
										$data=output($data);
								  ?>
					              <option value="<?php print $data['catId']; ?>"><?php print $data['catName']; ?></option>
					              <?php
									}
								  ?>
					            </select>
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _('admin_menu_menuname'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<input name="name" type="text" id="name" size="50" required class="form-control col-md-7 col-xs-12" />
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="file"><?php print _('admin_menu_file_path'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<input name="file" type="text" id="file" size="30" required class="form-control col-md-7 col-xs-12" />
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12"><?php print _('admin_menu_function'); ?>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<label class="checkbox-inline">
									<input name="view" type="checkbox" class="checkradio" id="view" value="y" checked="checked" required /> <?php print _('admin_menu_view'); ?>
								</label>
								<label class="checkbox-inline">
									<input name="add" type="checkbox" class="checkradio" id="add" value="y" /> <?php print _('admin_menu_add'); ?>
								</label>
								<label class="checkbox-inline">
									<input name="edit" type="checkbox" class="checkradio" id="edit" value="y" /> <?php print _('admin_menu_edit'); ?>
								</label>
								<label class="checkbox-inline">
									<input name="delete" type="checkbox" class="checkradio" id="delete" value="y" /> <?php print _('admin_menu_delete'); ?>
								</label>
	                        </div>
                    	</div>
                    	
                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='admin_menu.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>

                    	<br/><br/><br/>
					</form>
					<?php 
					} elseif ($action=="edit" and $uac_edit){ 
						$sql="SELECT * FROM admin_menu WHERE menuId='" . $id . "'";
						$query=query($sql);
						$data=fetch($query);
						$data=formoutput($data);
					?>
					<a href="admin_menu.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('admin_menu_addmenu'); ?></h4>
					<hr/>
					<form id="edit" name="edit" method="post" action="admin_menu.php?form=submit&amp;action=edit&amp;id=<?php print $id; ?>" class="form-horizontal form-label-left">
						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="catid"><?php print _('admin_menu_category'); ?>
	                        </label>
	                        <div class="col-md-3 col-sm-6 col-xs-12">
	                        	<select name="catid" id="catid" class="form-control">
					            	<?php
								  	$sqlx="SELECT * FROM admin_menu_cat ORDER BY catSort ASC";
									$queryx=query($sqlx);
									while ($datax=fetch($queryx)){
										$datax=output($datax);
									?>
					            	<option value="<?php print $datax['catId']; ?>"<?php print ($datax['catId']==$data['catId'])?" selected=\"selected\"":""; ?>><?php print $datax['catName']; ?></option>
					            	<?php
									}
									?>
				            	</select>
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _('admin_menu_menuname'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<input name="name" type="text" id="name" size="50" value="<?php print $data['menuName']; ?>" required class="form-control col-md-7 col-xs-12" />
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="file"><?php print _('admin_menu_file_path'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<input name="file" type="text" id="file" size="30" value="<?php print $data['menuFile']; ?>" required class="form-control col-md-7 col-xs-12" />
	                        </div>
                    	</div>
                    	<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12"><?php print _('admin_menu_function'); ?>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<label class="checkbox-inline">
									<input name="view" type="checkbox" class="checkradio" id="view" value="y"<?php print ($data['menuView']=="y")?" checked=\"checked\"":""; ?> /> <?php print _('admin_menu_view'); ?>
								</label>
								<label class="checkbox-inline">
									<input name="add" type="checkbox" class="checkradio" id="add" value="y"<?php print ($data['menuAdd']=="y")?" checked=\"checked\"":""; ?> /> <?php print _('admin_menu_add'); ?>
								</label>
								<label class="checkbox-inline">
									<input name="edit" type="checkbox" class="checkradio" id="edit" value="y"<?php print ($data['menuEdit']=="y")?" checked=\"checked\"":""; ?> /> <?php print _('admin_menu_edit'); ?>
								</label>
								<label class="checkbox-inline">
									<input name="delete" type="checkbox" class="checkradio" id="delete" value="y"<?php print ($data['menuDelete']=="y")?" checked=\"checked\"":""; ?> /> <?php print _('admin_menu_delete'); ?>
								</label>
	                        </div>
                    	</div>

                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_updatebutton'); ?>"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='admin_menu.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>
					</form>
	        		<?php } ?>
	        	</div>
	        </div>


		</div>
        <!-- END THE CONTENT OF PAGE HERE -->


		<?php include("com/com-footer.php"); ?>

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>