<?php
	$page=44;
	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/mainmod.php");	
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("../modz/errormsg.php");
	include("authuser.php");
	include("../modz/mainmod-extend.php");
	
    $sql="SELECT * FROM  groups_sms ORDER BY groupName ASC";
    $query=query($sql);
    $numofdata=rows($query);
    $arraygroup = array();
    while($data = fetch($query)){
        $arraygroup[] = $data;
    }

    if ($form=="submit"){
        
        if ($action=="add"){
            if(empty($name) ){
				$error=errorlist(2);
			}
            $exist=countdata("groups_sms","groupName='$name'");
            if ($exist==1){
				$error=errorlist(68);
			}
            
           
            if(!$error){
                $gid=nextid("groupId","groups_sms");
                $sql="INSERT INTO groups_sms(groupId,groupName) VALUES ($gid,'$name')";
                $query=query($sql);

                if ($query and count($con)>0){
                    foreach ($con as $cid){
                        $sql="INSERT INTO groups_contacts_sms(groupId,conId) VALUES ($gid,$cid)";
                        $query=query($sql);
                    }
                }
            }
      
		}elseif($action=="del"){
           if(empty($id)){
               $error=errorlist(2);
           }else{
               $sql = "DELETE FROM  groups_sms WHERE groupId='$id'";
               $query = query($sql);
               if($query){
                   $sql2 = "DELETE FROM groups_contacts_sms WHERE groupId='$id'";
                   $query2 = query($sql2);
               }
           }
		}else if($action=="edit"){
		    if(empty($id) or empty($name)){
				$error=errorlist(2);
			}
            
             
            if(!$error){
                $sql="UPDATE groups_sms SET groupName='$name' WHERE groupId='$id'";
                $query=query($sql);
        
                if ($query){
                    $sql="DELETE FROM  groups_contacts_sms WHERE groupId=" . $id;
                    $query=query($sql);

                    if (count($con)>0){
                        //insert data
                        foreach ($con as $cid){
                             $sql="INSERT INTO groups_contacts_sms(groupId,conId) VALUES ($id,$cid)";
                            $query=query($sql);
                        }
                    }
                }
                
                
            }
            
		}else if($action=="sendsms"){
        
            if(empty($gid) or empty($text) or empty($senderid)){
				$error=errorlist(2);
			}
            
            if($getbalance < 275 or empty($getbalance)){
                $error=errorlist(66);
            }
            if(!$error){
                $sqlcontact = "SELECT * FROM groups_sms,groups_contacts_sms,contacts_sms WHERE  groups_sms.groupId=groups_contacts_sms.groupId AND groups_sms.groupId='$gid' AND groups_contacts_sms.conId=contacts_sms.conId";
                $querycontact = query($sqlcontact);
                while($data = fetch($querycontact)){
                    $sql="INSERT INTO sms(smsTo, smsText, smsDay, smsMonth, smsYear, smsDate, smsTime,smsTimetime,smsStatus) VALUES ('$data[conHP]','$text','".date('d', $now)."','".date('m', $now)."','".date('Y', $now)."','".date('Y-m-d', $now)."','".date('H:i:s', $now)."','$now','new')";
				    $query=query($sql);
                }

            }
            
		}
        
        
        if(!$query){
			$error=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:sms_group.php");
			}else{
				print "ok";
			}
		}
		exit;
    }
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print _('sms_group_page_title'); ?> - <?php print _('area'); ?> - <?php print SITE_NAME; ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="/libs/bootstrap-datepicker/css/datepicker3.css" />
<link rel="stylesheet" href="/libs/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />

<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">
<link href="/style/chosen.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/javascript/chosen.jquery.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>

<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>

<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="/libs/moment/min/moment-with-locales.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($){
	$('#result').hide();
	validate('#result','#add','sms_group.php');
	validate('#result','#edit','sms_group.php');
    <?php
    foreach($arraygroup as $data){
    ?>
	validate('#result','#sendsms<?php print $data['groupId']; ?>','sms_group.php');
    <?php
    }
    ?>
	
	$('html').click(function(){
		$('.ui-widget').slideUp(500);
	});
	
	$(".datetimepicker").datetimepicker({
		format: 'DD-MM-YYYY',
		locale: 'id' 
	});
});

</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">
			<div class="row">
				<div class="col-md-12">
					<h3><?php print _('sms_group_page_title'); ?></h3>
                    <p><?php print _('sms_group_page_title_desc'); ?></p>
					<hr>
				</div>
			</div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                    <div class="pull-right">
                           <a class="btn btn-primary" href="?action=add"><?php print _('sms_group_add'); ?></a>
                    </div>
                </div>
            </div>
			<div class="row">
				<div class="col-md-12">
                    <?php 
					if ($action==""){
                    ?>
                   
                    <div class= "row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dt-responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th style="width:20px;"><?php print _('cms_no'); ?></th>
                                            <th style="width:530px;"><?php print _('sms_group_name'); ?></th>
                                            <th style="width:30px;" class="text-center"><?php print _('sms_group_contact'); ?></th>
                                            <th style="width:20px;"></th>
                                            <th style="width:20px;"></th>
                                            <th style="width:20px;"></th>
                                            <th style="width:20px;"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                       
                                        $num=1;
                                        if($numofdata<1){
                                        ?>
                                        <tr>
                                            <td colspan="9" class="text-center"><?php print _('sms_data_empty');  ?></td>
                                        </tr>
                                        <?php
                                        }else{
                                            foreach($arraygroup as $data){
                                        ?>
                                        <tr>
                                            <td><?php print $num; ?></td>
                                            <td><?php print $data['groupName']; ?></td>
                                            <td class="text-center">
                                            <?php
                                                $sqlx="SELECT COUNT(*) AS total FROM groups_contacts_sms WHERE groupId='$data[groupId]'";
                                                $queryx=query($sqlx);
                                                $datax=output(fetch($queryx));
                                                print number_format($datax['total'],0,",",".");
                                            ?>
                                            </td> 
                                            <td class="text-center">
                                                <?php
                                                if($datax['total'] > 0){
                                                ?>
                                                <a id="modal-sendsms" href="#modal-sendsms<?php print $data['groupId']; ?>"  data-toggle="modal"  rel="tooltip" title="Send SMS to: <?php print $data['groupName']; ?>"><i class="fa fa-envelope"></i></a>
                                                <?php
                                                }
                                                ?>
                                            </td>
                                            <td class="text-center">
                                                <?php
                                                if($datax['total'] > 0){
                                                ?>
                                                <a id="modal-detail" href="#modal-detail<?php print $data['groupId']; ?>"  data-toggle="modal"  rel="tooltip" title="Detail: <?php print $data['groupName']; ?>"><i class="fa fa-list"></i></a>
                                                <?php
                                                }
                                                ?>
                                            </td>
                                           
                                            <td class="text-center">
                                                <a href="?action=edit&id=<?php print $data['groupId']; ?>" rel="tooltip" title="<?php print _('cms_edit'); ?>: <?php print ucwords($data['groupName']); ?>"><i class="fa fa-edit"></i> </a>
                                               
                                            </td>
                                            <td class="text-center">
                                                <?php
                                                if($data['groupName']!="Member Deal" && $data['groupName']!="Member Cashback"){
                                                ?>
                                                <a href="sms_group.php?form=submit&action=del&id=<?php print $data['groupId']; ?>" title="Apakah anda yakin akan menghapus : <?php print ucwords($data['groupName']); ?>" class="delete" ><i title="<?php print _('cms_delete'); ?>: <?php print ucwords($data['groupName']); ?>" rel="tooltip" class="fa fa-trash-o"></i></a>
                                                <?php
                                                }
                                                ?>
                                                
											</td>    
                                        </tr>
                                        <?php
                                            $num++;
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                      
                    </div>
                    
                    <?php
                    include "com/com-getbalance.php";    
                        
                    foreach($arraygroup as $g){
                        $sqlx="SELECT COUNT(*) AS total FROM groups_contacts_sms WHERE groupId='$g[groupId]'";
                        $queryx=query($sqlx);
                        $datax=output(fetch($queryx));
                    ?>
                    <script>
                        function countChar<?php print $g['groupId']; ?>(val) {
                            var len = val.value.length;
                            var jumsms = Math.ceil(len/160);
                            $('#charNum<?php print $g['groupId']; ?>').text(len + ' Karakter');
                            $('#countSMS<?php print $g['groupId']; ?>').text(jumsms + ' SMS');
                        }
                    </script>
                    <div class="modal fade" id="modal-sendsms<?php print $g['groupId']; ?>" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        ×
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">
                                        Send SMS - <?php print ucwords($g['groupName']); ?><br>
                                    </h4>
                                    <?php print "Balance: ".$balance['currency'].number_format($balance['balance']); ?>
                                </div>
                                <form  action="sms_group.php?form=submit&amp;action=sendsms&gid=<?php print $g['groupId']; ?>" method="post" enctype="multipart/form-data" name="sendsms<?php print $g['groupId']; ?>" id="sendsms<?php print $g['groupId']; ?>">
                                <input type="hidden" name="getbalance" id="getbalance" value="<?php print $balance['balance']; ?>">
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="prodname">
                                                        <?php print _('sms_from'); ?><span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                                        <select class="form-control" name="senderid" required>
                                                            <option value="<?php print getconfig('SMS_API_FROM'); ?>"><?php print getconfig('SMS_API_FROM'); ?></option>
                                                        </select>
                                                    </div>
                                                </div>&nbsp;&nbsp;
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-3" for="prodname">
                                                        <?php print _('sms_to'); ?>
                                                    </label>
                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <?php print ucwords($g['groupName']); ?> (<?php print $datax['total']; ?> Contacts)
                                                    </div>
                                                </div>&nbsp;&nbsp;
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mCatPicLogo">
                                                        <?php print _('sms_text'); ?> <span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <textarea class="form-control" cols="10" rows="10" name="text" id="text" onkeyup="countChar<?php print $g['groupId']; ?>(this)" required></textarea>
                                                        <div class="row">
                                                            <div class="col-md-6 col-xs-6 col-sm-6 col-lg-6">
                                                                <div id="charNum<?php print $g['groupId']; ?>" class="help-block" style="font-weight: bold;">0 Karakter</div>
                                                            </div>
                                                            <div class="col-md-6 col-xs-6 col-sm-6 col-lg-6">
                                                                <div id="countSMS<?php print $g['groupId']; ?>" class="help-block text-right" style="font-weight: bold;">0 SMS</div>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">
                                            Send    
                                        </button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-top:1px;">
                                            Close
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modal-detail<?php print $g['groupId']; ?>" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        ×
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">
                                        Phone Book - Group - <?php print ucwords($g['groupName']); ?>
                                    </h4>
                                </div>
                                <div class="modal-body">
                                   <div class="row">
                                       <div class="col-md-12">
                                           <div style="width:100%; height:400px; overflow:auto;">
                                               <table>
                                                    <?php
                                                    $sqlgroupcontact = "SELECT * FROM groups_contacts_sms,contacts_sms WHERE groups_contacts_sms.conId=contacts_sms.conId AND groups_contacts_sms.groupId='$g[groupId]'";
                                                    $querygroup = query($sqlgroupcontact);
                                                    while($data = fetch($querygroup)){
                                                    ?>
                                                       <tr>
                                                           <td style="width:200px;"><?php print "<b>".ucwords($data['conFullName'])."</b>"; ?></td>
                                                           <td><?php print $data['conHP']; ?></td>
                                                       </tr>
                                                   <?php
                                                    }
                                                    ?>
                                               </table>
                                            </div>
                                       </div>
                                   </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"  data-dismiss="modal">
                                        Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    }    
                 
                    }else if($action=="add"){
                    ?>
                    <a href="sms_group.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
	        		<h4><?php print _('sms_group_add_title'); ?></h4>
					<hr/>
                    <form action="sms_group.php?form=submit&amp;action=add" method="post" enctype="multipart/form-data" name="add" id="add" class="form-horizontal form-label-left">
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
	                        	<?php print _('sms_group_name'); ?><span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-4 col-xs-12">
	                        	<input name="name" type="text" id="name" size="40" class="form-control" required>
	                        </div>
                        </div>
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
	                        	<?php print _('sms_group_contact'); ?>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
                                <select data-placeholder="Choose a contact" multiple class="chosen-select" style="width:100%;" name="con[]">
                                     <?php
                                        $sqly="SELECT * FROM contacts_sms WHERE conStatus='y' ORDER BY conFullName ASC";
                                        $queryy=query($sqly);
                                        while ($datay=fetch($queryy)){
                                            $datay=output($datay);
                                      ?>
                                    <option value="<?php print $datay['conId']; ?>"><?php print $datay['conFullName']." | ".$datay['conHP']; ?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
	                        </div>
                        </div>
                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='sms_group.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>

                    </form>
                    <?php
                    }else if($action=="edit"){
                        $sql="SELECT * FROM groups_sms WHERE groupId='$id'";
                        $query=query($sql);
                        $data=fetch($query);
                        $data=formoutput($data);
                        
                        $sqlx="SELECT conId FROM groups_contacts_sms WHERE groupId=" . $id;
                        $queryx=query($sqlx);
                        while ($datax=fetch($queryx)){
                            $datax=output($datax);
                            $coid[]=$datax['conId'];
                        }
                     
                    ?>
                    <a href="sms_group.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>
	        		<h4><?php print _('sms_group_edit_title'); ?></h4>
					<hr/>
                    <form action="sms_group.php?form=submit&amp;action=edit&id=<?php print $id ?>" method="post" enctype="multipart/form-data" name="edit" id="edit" class="form-horizontal form-label-left">
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
	                        	<?php print _('sms_group_name'); ?><span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-4 col-xs-12">
	                        	<input name="name" type="text" id="name" size="40" class="form-control" required value="<?php print $data['groupName']; ?>"  <?php  if($data['groupName']=="Member Deal" || $data['groupName']=="Member Cashback") print "readonly"; ?>  >
	                        </div>
                        </div>
                        <div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="prodname">
	                        	<?php print _('sms_group_contact'); ?>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
                                <select data-placeholder="Choose a contact" multiple class="chosen-select" style="width:100%;" name="con[]">
                                     <?php
                                        $sqly="SELECT * FROM contacts_sms WHERE conStatus='y' ORDER BY conFullName ASC";
                                        $queryy=query($sqly);
                                        while ($datay=fetch($queryy)){
                                            $datay=output($datay);
                                            
                                      ?>
                                    <option value="<?php print $datay['conId']; ?>" <?php if(in_array($datay['conId'],$coid)) print "selected"; ?>  ><?php print $datay['conFullName']." | ".$datay['conHP']; ?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
	                        </div>
                        </div>
                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> <?php print _('cms_updatebutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='sms_group.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>

                    </form>
                    <?php
                    }
                    ?>
				</div>
			</div>
        </div>
        <!-- /page content -->

        <?php include("com/com-footer.php"); ?>
	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
<script>
	var config = {
  '.chosen-select'           : {},
  '.chosen-select-deselect'  : { allow_single_deselect: true },
  '.chosen-select-rtl'       : { rtl: true },
  '.chosen-select-width'     : { width: '95%' }
}
for (var selector in config) {
  $(selector).chosen(config[selector]);
}
</script>
</body>
</html>