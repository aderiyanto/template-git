<?php
	$page=35;
	
	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");	
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- bootstrap-datepicker -->
<link rel="stylesheet" type="text/css" href="/libs/bootstrap-datepicker/css/datepicker3.css" />
<link rel="stylesheet" href="/libs/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">


<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce_simple.js"></script>
<script type="text/javascript" src="/libs/moment/min/moment-with-locales.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>
</head>

<body class="flyctn" style="background:#F9F9F9;">

<div class="container body">
	<div class="main_container">

		<div class="row">
			<div class="col-md-12" style="width:100%;">
			<?php
			if($action=="readmore"){
				$sql=$sql="SELECT * FROM newsinfo_comment WHERE cId='$id'";
				$query=query($sql);
				$data=fetch($query);

				?>
				<div style="overflow:hidden;">
					<div style="background-color:#132537;color:#fff;padding:11px;margin-bottom:12px;">
						<h3 style="padding:0;margin:0;"><?php print _('newsinfocomment_readmore_detailcomment'); ?></h3>
					</div>
					<div class="commenthead">
						<table style="margin-bottom:10px" cellpadding="3" width="100%">
						<tr>
							<td width="80"><strong><?php print _('newsinfocomment_readmore_from'); ?></strong></td>
							<td><b>:</b> <?php print ucfirst($data['cName']).' ('.$data['cEmail'].')';?></td>
						</tr>
						<tr>
							<td><strong><?php print _('newsinfocomment_readmore_ipaddress'); ?></strong></td>
							<td><b>:</b> <?php print $data['cIpAddress'];?></td>
						</tr>
						</table>
						<hr/>
						<small><?php print convertdaytoid(date("l",$data['cDateAdded'])) . ", " . date("d ",$data['cDateAdded']) . convertmonthtoid(date("F",$data['cDateAdded'])) . date(" Y H:i:s",$data['cDateAdded']);?> </small>
						<div class="stat">Status: <img align="absmiddle" src="../assets/images/<?php print $data['cStatus'] == 'y' ? 'ok.png':'no.png';?>" width="16"/> <em>(<?php print $data['cStatus'] == 'y' ? 'ditampilkan':'tidak ditampilkan';?>)</em></div>
					</div>
					<div class="commentbody">
						<div class="chead"><?php print _('newsinfocomment_readmore_comment'); ?> </em></div><br/>
						<?php print output(nl2br(strip_tags($data['cContent'])));?>
						
						<?php
						$comSensored = output(strip_tags($data['cContent']));
						if($comSensored != $data['cContent']){
							?>
							<br/><br/>
							<div class="chead"><?php print _('newsinfocomment_readmore_comment'); ?> <em><?php print _('newsinfocomment_readmore_aftercensored'); ?></em></div><br/>
							<?php 
							print $comSensored;
						}?>
					</div>
					<hr/>
					<div class="note">
						<?php
						if($data['cSpam'] == 'y'){
							?>
								<img src="../assets/images/message-spam.png" align="absmiddle" /> &nbsp;<?php print _('newsinfocomment_readmore_spam1'); ?></em>
							<?php
						}else{
							?>
								<img src="../assets/images/message-ok.png" align="absmiddle"/> &nbsp;<?php print _('newsinfocomment_readmore_spam2'); ?>
							<?php
						}
						?>
					</div>
					<br/>
				</div>
				
			<?php
			}
			?>
			</div>
		</div>
	</div>
</div>

</body>
</html>