<?php
	$page=6;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");

	if ($form=="submit"){
	//if any form is submitted
		if ($action=="add" and $uac_add){
			if (empty($name) or count($view)==0){
				$error=errorlist(2);
			}

			if (!$error){
				$gid=nextid("groupId","admin_group");
				$num=1;
				foreach ($view as $key=>$value){
					if ($value=="y"){
						$addv    = ($add[$key])?$add[$key]:"n";
						$editv   = ($edit[$key])?$edit[$key]:"n";
						$deletev = ($delete[$key])?$delete[$key]:"n";

						list($mid,$cid)=explode("-",$key);

						//make sure admin does not try to add group to access "WEB DEVELOPER" menu
						if ($cid!==3){
							//insert data into admin_group_menu
							$sql="INSERT INTO admin_group_menu VALUES ($gid,$mid,'$value','$addv','$editv','$deletev')";
							$query=query($sql);

							$mcat[$num]=$cid;
							$num++;
						}
					}
				}

				$mcat=array_unique($mcat);

				foreach ($mcat as $mkey=>$mvalue){
					if ($mvalue!==3){
						//insert data into admin_group_menu
						$sql="INSERT INTO admin_group_menu_cat VALUES ($gid,$mvalue)";
						$query=query($sql);
					}
				}

				//insert data into group
				$sql="INSERT INTO admin_group VALUES ($gid,'$name','y')";
				$query=query($sql);
			}
		}elseif ($action=="edit" and $uac_edit){
			if (empty($name) or count($view)==0){
				$error=errorlist(2);
			}

			if (!$error){
				$sql="DELETE FROM admin_group_menu_cat WHERE groupId='" . $id . "'";
				$query=query($sql);

				$sql="DELETE FROM admin_group_menu WHERE groupId='" . $id . "'";
				$query=query($sql);

				$num=1;
				foreach ($view as $key=>$value){

					if ($value=="y"){
						$addv    = ($add[$key])?$add[$key]:"n";
						$editv   = ($edit[$key])?$edit[$key]:"n";
						$deletev = ($delete[$key])?$delete[$key]:"n";

						list($mid,$cid)=explode("-",$key);
						if ($cid!==3){
							//insert data into admin_group_menu
							$sql="INSERT INTO admin_group_menu VALUES ($id,$mid,'$value','$addv','$editv','$deletev')";
							$query=query($sql);

							$mcat[$num]=$cid;
							$num++;
						}
					}
				}

				$mcat=array_unique($mcat);

				foreach ($mcat as $mkey=>$mvalue){
					if ($mvalue!==3){
						//insert data into admin_group_menu
						$sql="INSERT INTO admin_group_menu_cat VALUES ($id,$mvalue)";
						$query=query($sql);
					}
				}

				$sql="UPDATE admin_group SET groupName='$name' WHERE groupId='" . $id . "'";
				$query=query($sql);
			}
		}elseif ($action=="del" and $uac_delete){
			if ($id>2){
				$sqlx="SELECT COUNT(*) AS total FROM admin WHERE groupId='" . $id . "'";
				$queryx=query($sqlx);
				$datax=fetch($queryx);
				$total=$datax['total'];

				if ($total>0){
					$error.=errorlist(15);
				}
				else{
					$sql="DELETE FROM admin_group_menu_cat WHERE groupId='" . $id . "'";
					$query=query($sql);

					$sql="DELETE FROM admin_group_menu WHERE groupId='" . $id . "'";
					$query=query($sql);

					$sql="DELETE FROM admin_group WHERE groupId='" . $id . "'";
					$query=query($sql);
				}
			}
		}elseif ($action=="activate"){
			if ($id>2){
				$sql="UPDATE admin_group SET groupActive='y' WHERE groupId='" . $id . "'";
				$query=query($sql);

				$sql="UPDATE admin SET adminActive='y' WHERE groupId='" . $id . "'";
				$query=query($sql);
			}
		}elseif ($action=="deactivate"){
			if ($id>2){
				$sql="UPDATE admin_group SET groupActive='n' WHERE groupId='" . $id . "'";
				$query=query($sql);

				$sql="UPDATE admin SET adminActive='n' WHERE groupId='" . $id . "'";
				$query=query($sql);
			}
		}

		//check whether query was successful
		if(!$query){
			$error.=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:admin_group.php");
			}else{
				print "ok";
			}
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS - <?php print _('admingroup_pagetitle'); ?></title>
<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce_simple.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('.fancybox').fancybox({
		'arrows': false
	});
	$('#result').hide();
	validate('#result','#add','admin_group.php');
	validate('#result','#edit','admin_group.php');
});
</script>
<script type="text/javascript">
$(function(){ //Sama jika menggunakan $(document).ready(function(){

	$("#check-all1").click(function(){
		if ( (this).checked == true ){

		   $('.checkradio1').prop('checked', true);

		} else {

		   $('.checkradio1').prop('checked', false);

		}
	});
	$("#check-all2").click(function(){
		if ( (this).checked == true ){

		   $('.checkradio2').prop('checked', true);

		} else {

		   $('.checkradio2').prop('checked', false);

		}
	});
	$("#check-all3").click(function(){
		if ( (this).checked == true ){

		   $('.checkradio3').prop('checked', true);

		} else {

		   $('.checkradio3').prop('checked', false);

		}
	});
	$("#check-all4").click(function(){
		if ( (this).checked == true ){

		   $('.checkradio4').prop('checked', true);

		} else {

		   $('.checkradio4').prop('checked', false);

		}
	});

});
</script>
</head>

<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('admingroup_pagetitle'); ?></h3>
			            </div>
		            </div>
	        	</div>
        	</div>

        	<?php if ($uac_add){ ?>
        	<div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                	<div class="pull-right">
                		<a href="admin_group.php?action=add" class="btn btn-primary"><i class="fa fa-plus"></i> <?php print _('admingroup_add'); ?></a>
                	</div>
                </div>
        	</div>
        	<?php } ?>

        	<!-- THE CONTENT OF PAGE HERE -->
        	<div class="row">
	        	<div class="col-md-12">
	        	<?php if ($action==""){ ?>
	        		<div class="table-responsive">
        				<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
        					<thead>
        						<tr>
        							<th width="35" class="text-center"><?php print _('admingroup_numb'); ?></th>
								    <th><?php print _('admingroup_groupname'); ?></th>
								    <th width="150"><?php print _('admingroup_admin'); ?></th>
								    <th width="150"><?php print _('admingroup_status'); ?></th>
									<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
									<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
									<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
        						</tr>
        					</thead>
        					<tbody>
        						<?php
									$sql="SELECT * FROM admin_group WHERE groupId>2";
									$query=query($sql);
									$num=1;

									$numofdata=rows($query);
									if ($numofdata<1){
									?>
									<tr>
										<td colspan="7" class="asterik"><div class="text-center"><br /><?php print _('admingroup_groupnotavailable'); ?><br /><br /></div></td>
									</tr>
									<?php
									}else{
										while ($data=fetch($query)){
											$data=output($data);
								?>
								<tr>
								    <td class="text-center"><?php print $num; ?></td>
								    <td><strong><?php print $data['groupName']; ?></strong></td>
								    <td class="text-center">
						    		<?php
										$sqlx="SELECT COUNT(*) AS total FROM admin WHERE groupId=" . $data['groupId'];
										$queryx=query($sqlx);
										$datax=fetch($queryx);
										print number_format($datax['total'],0,",",".");
									?>
						        	</td>
								    <td class="text-center"><strong><?php print ($data['groupActive']=="y")?"<span class=\"label label-success\">Aktif</span>":"<span class=\"label label-danger\">Non-Aktif</span>"; ?></strong></td>
								    <td class="text-center">
						            	<?php if ($data['groupActive']=="y"){ ?>
						                <a href="javascript:if (window.confirm('<?php print _('admingroup_windowsconfirmendisable'); ?>')){ window.location='admin_group.php?form=submit&amp;action=deactivate&amp;id=<?php print $data['groupId']; ?>'; };" title="<?php print _('admingroup_disable'); ?> <?php print stripquote($data['groupName']); ?>" rel="tooltip"><i class="fa fa-user-times" style="color:#cc0000;"></i></a>
						                <?php }else{ ?>
						                <a href="javascript:if (window.confirm('<?php print _('admingroup_windowsconfirmenable'); ?>')){ window.location='admin_group.php?form=submit&amp;action=activate&amp;id=<?php print $data['groupId']; ?>'; };" title="<?php print _('admingroup_enable'); ?> <?php print stripquote($data['groupName']); ?>"><i class="fa fa-user" style="color:#00cc00;"></i></a>
						                <?php } ?>
						            </td>
								    <td class="text-center"><?php if ($uac_edit){ ?><a href="admin_group.php?action=edit&amp;id=<?php print $data['groupId']; ?>" title="<?php print _('cms_edit'); ?>: <?php print stripquote($data['groupName']); ?>" rel="tooltip"><i class="fa fa-edit"></i></a><?php } ?></td>
								    <td class="text-center"><?php if ($uac_delete){ ?><a class="delete" href="admin_group.php?form=submit&action=del&id=<?php print $data['groupId']; ?>" title="<?php print stripquote($data['groupName']); ?>"><i rel="tooltip" title="<?php print _('admin_delete'); ?>: <?php print stripquote($data['groupName']); ?>" class="fa fa-trash"></i></a><?php } ?></td>
						    	</tr>
					    	<?php
								  		$num++;
									}
								}
							?>
        					</tbody>
        				</table>
        			</div>
        		<?php } elseif ($action=="add" and $uac_add){ ?>
        			<a href="admin_group.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _("admingroup_add_tambah"); ?></h4>
					<hr/>

					<form id="add" name="add" method="post" action="admin_group.php?form=submit&amp;action=add" class="form-horizontal form-label-left">
						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">
	                        	<?php print _('admingroup_groupname'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<input name="name" type="text" id="name" size="40" class="form-control" required />
	                        </div>
                    	</div>

                    	<div class="clearfix"></div>
                    	<div class="row"><div class="col-md-6 col-sm-6 col-xs-12">
                    	<div class="table-responsive">
	        				<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
	        					<thead>
	        						<tr>
	        							<th width="30">&nbsp;</th>
							        	<th width="250">&nbsp;</th>
							        	<th width="50" class="text-center"><strong><?php print _('admingroup_view'); ?></strong></th>
							        	<th width="50" class="text-center"><strong><?php print _('admingroup_add_add'); ?></strong></th>
							        	<th width="50" class="text-center"><strong><?php print _('admingroup_edit'); ?></strong></th>
							        	<th width="50" class="text-center"><strong><?php print _('admingroup_delete'); ?></strong></th>
	        						</tr>
	        					</thead>
	        					<tbody>
	        						<tr>
										<td style="border-left:0px" colspan="2"><strong></strong><?php print _('admingroupsuper_Choose_all'); ?></td>
										<td class="text-center"><input type="checkbox" id="check-all1"></td>
										<td class="text-center"><input type="checkbox" id="check-all2"></td>
										<td class="text-center"><input type="checkbox" id="check-all3"></td>
										<td class="text-center"><input type="checkbox" id="check-all4"></td>
									</tr>
									<?php
										$sql="SELECT * FROM admin_menu_cat c, admin_group_menu_cat g WHERE g.catId=c.catId AND g.groupId=$cook_gid ORDER BY c.catSort ASC";
										$query=query($sql);
										while ($data=fetch($query)){
											$features = implode(',', $lc_features);
											$sqlx="SELECT * FROM admin_menu m, admin_group_menu g WHERE m.catId='" . $data['catId'] . "' and m.menuId=g.menuId and g.groupId='$cook_gid'".($data['catId']!=3 ? " AND FIND_IN_SET(m.menuId,'$features')":'')." ORDER BY m.menuSort ASC";
											$queryx=query($sqlx);
											if(rows($queryx)){
												?>
												<tr>
													<td colspan="6"><strong><?php print $data['catName']; ?></strong></td>
												</tr>
												<?php
												while ($datax=fetch($queryx)){
													?>
													<tr>
														<td>&nbsp;</td>
														<td><?php print $datax['menuName']; ?></td>
														<td align="center"><?php if ($datax['gmView']=="y"){ ?><input name="view[<?php print $datax['menuId'] . "-" . $data['catId']; ?>]" type="checkbox" class="checkradio1" value="y" checked="checked" /><?php } ?></td>
														<td align="center"><?php if ($datax['gmAdd']=="y"){ ?><input name="add[<?php print $datax['menuId'] . "-" . $data['catId']; ?>]" type="checkbox" class="checkradio2" value="y" checked="checked" /><?php } ?></td>
														<td align="center"><?php if ($datax['gmEdit']=="y"){ ?><input name="edit[<?php print $datax['menuId'] . "-" . $data['catId']; ?>]" type="checkbox" class="checkradio3" value="y" checked="checked" /><?php } ?></td>
														<td align="center"><?php if ($datax['gmDelete']=="y"){ ?><input name="delete[<?php print $datax['menuId'] . "-" . $data['catId']; ?>]" type="checkbox" class="checkradio4" value="y" checked="checked" /><?php } ?></td>
													</tr>
													<?php
												}
											}
										}
									?>
	        					</tbody>
	        				</table>
	        			</div>
	        			</div></div>
	        			<div class="clearfix"></div>
	        			<div class="form-group">
	                        <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('admingroup_add_button'); ?>"><i class="fa fa-plus"></i> <?php print _('admingroup_add_button'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='admin_group.php'"><i class="fa fa-reply"></i> <?php print _('admingroup_cancel'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>
                    	<br/><br/><br/>
					</form>
				<?php
					} elseif ($action=="edit" and $uac_edit){
						$sqlz="SELECT * FROM admin_group WHERE groupId='" . $id . "'";
						$queryz=query($sqlz);
						$dataz=fetch($queryz);
				?>
					<a href="admin_group.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('admingroup_editgroup'); ?> - <?php print output($dataz['groupName']); ?></h4>
					<hr/>
					<form id="edit" name="edit" method="post" action="admin_group.php?form=submit&amp;action=edit&amp;id=<?php print $dataz['groupId']; ?>" class="form-horizontal form-label-left">
						<div class="form-group">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name"><?php print _('admingroup_groupname'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-4 col-sm-6 col-xs-12">
	                        	<input name="name" type="text" id="name" size="40" class="form-control" required value="<?php print $dataz['groupName']; ?>">
	                        </div>
                    	</div>
                    	
                    	<div class="clearfix"></div>
                    	<div class="row"><div class="col-md-6 col-sm-6 col-xs-12">
                    	<div class="table-responsive">
                    		<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
	        					<thead>
	        						<tr>
	        							<th width="30">&nbsp;</th>
					                    <th width="250">&nbsp;</th>
					                    <th width="50" class="text-center"><strong><?php print _('admingroup_view'); ?></strong></th>
					                    <th width="50" class="text-center"><strong><?php print _('admingroup_add_add'); ?></strong></th>
					                    <th width="50" class="text-center"><strong><?php print _('admingroup_edit'); ?></strong></th>
					                    <th width="50" class="text-center"><strong><?php print _('admingroup_delete'); ?></strong></th>
	        						</tr>
	        					</thead>
	        					<tbody>
	        						<tr>
										<td style="border-left:0px" colspan="2"><strong></strong><?php print _('admingroupsuper_Choose_all'); ?></td>
										<td class="text-center"><input type="checkbox" id="check-all1"></td>
										<td class="text-center"><input type="checkbox" id="check-all2"></td>
										<td class="text-center"><input type="checkbox" id="check-all3"></td>
										<td class="text-center"><input type="checkbox" id="check-all4"></td>
									</tr>
	        						<?php
				                        $sql="SELECT * FROM admin_menu_cat WHERE catId<>3 ORDER BY catSort ASC";
				                        $query=query($sql);

										$features = implode(',', $lc_features);
				                        while ($data=fetch($query)){
											$sqlx="SELECT * FROM admin_menu WHERE catId=" . $data['catId'] . " AND FIND_IN_SET(menuId, '$features') ORDER BY menuSort ASC";
											$queryx=query($sqlx);
											if(rows($queryx)){
												?>
												<tr>
												  <td colspan="6"><strong><?php print $data['catName']; ?></strong></td>
												</tr>
												<?php
												while ($datax=fetch($queryx)){
													$sqlv="SELECT * FROM admin_group_menu WHERE groupId=" . $id . " AND menuId=" . $datax['menuId'];
													$queryv=query($sqlv);
													$datav=fetch($queryv);
													?>
													<tr>
													  <td>&nbsp;</td>
													  <td><?php print $datax['menuName']; ?></td>
													  <td align="center"><?php if ($datax['menuView']=="y"){ ?><input name="view[<?php print $datax['menuId'] . "-" . $data['catId']; ?>]" type="checkbox" class="checkradio1" value="y"<?php if ($datax['menuView']==$datav['gmView']){ ?> checked="checked"<?php } ?> /><?php } ?></td>
													  <td align="center"><?php if ($datax['menuAdd']=="y"){ ?><input name="add[<?php print $datax['menuId'] . "-" . $data['catId']; ?>]" type="checkbox" class="checkradio2" value="y"<?php if ($datax['menuAdd']==$datav['gmAdd']){ ?> checked="checked"<?php } ?> /><?php } ?></td>
													  <td align="center"><?php if ($datax['menuEdit']=="y"){ ?><input name="edit[<?php print $datax['menuId'] . "-" . $data['catId']; ?>]" type="checkbox" class="checkradio3" value="y"<?php if ($datax['menuEdit']==$datav['gmEdit']){ ?> checked="checked"<?php } ?> /><?php } ?></td>
													  <td align="center"><?php if ($datax['menuDelete']=="y"){ ?><input name="delete[<?php print $datax['menuId'] . "-" . $data['catId']; ?>]" type="checkbox" class="checkradio4" value="y"<?php if ($datax['menuDelete']==$datav['gmDelete']){ ?> checked="checked"<?php } ?> /><?php } ?></td>
													</tr>
													<?php
													}
											}
				                        }
				                    ?>
	        						
	        					</tbody>
	        				</table>
	        			</div>
	        			</div></div>
	        			<div class="clearfix"></div>
                    	<div class="form-group">
	                        <div class="control-label col-md-2 col-sm-2 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_updatebutton'); ?>"><?php print _('cms_updatebutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='admin_group.php'"><?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>
                    	<br/><br/><br/>

					</form>
	        	<?php } ?>
	        	</div>
	        </div>



		</div>
		<!-- END THE CONTENT OF PAGE HERE -->

		<?php include("com/com-footer.php"); ?>

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>