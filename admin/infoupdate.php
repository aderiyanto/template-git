<?php
	$page=13;

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");
	include("../modz/mainmod-extend.php");




	#check language license
	if($lang and !in_array($lang, $lc_lang) and ($action=='adden' or $action=='editen')){
		header('location:infoupdate.php');
		exit;
	}

	$curry  = date("Y");
	$starty = $curry;
	$endy   = $curry+1;

	if ($form=="submit"){
	//if any form is submitted
		if ($action=="add" and $uac_add){
			if(empty($news) or empty($date)){
				$error=errorlist(2);
			}

			list($day,$month,$year)=explode("/",$date);
			if(!checkdate($month,$day,$year) and $date){
				$error.=errorlist(22);
			}

			if(!$error){
				$nextid=nextid("tickId","ticker");
				$datetime=createtimestamp($day,$month,$year);
				$sql="INSERT INTO ticker VALUES ($nextid,'$news','$datetime')";
				$query=query($sql);
			}
		}elseif($action=="edit" and $uac_edit){
			if(empty($news) or empty($date)){
				$error=errorlist(2);
			}

			list($day,$month,$year)=explode("/",$date);
			if(!checkdate($month,$day,$year) and $date){
				$error.=errorlist(22);
			}

			//update data
			if (!$error){
				$datetime=createtimestamp($day,$month,$year);
				$sql="UPDATE ticker SET tickNews='$news', tickDateTime='$datetime' WHERE tickId='$id'";
				$query=query($sql);
			}
		}elseif($action=="del" and $uac_delete){
			//delete data
			$sql="DELETE FROM ticker WHERE tickId='$id'";
			$query=query($sql);

			if($query){
				#delete english version
				$sql="DELETE FROM ticker_lang WHERE tickId='$id'";
				$query=query($sql);
			}
		}elseif($action=="adden" and $uac_add){
			//need only tickNews
			if(empty($news)){
				$error=errorlist(2);
			}else{
				$data=getval("tickNews,tickDateTime","ticker","tickId",$id);
				$datetime=$data['tickDateTime'];

				if(!$datetime){
					$error=errorlist(29);
				}

				//update data
				$sql="INSERT INTO ticker_lang VALUES ($id,'$news','$lang')";
				$query=query($sql);
			}
		}elseif($action=="editen" and $uac_edit){
			//need only tickNews
			if(empty($news)){
				$error=errorlist(2);
			}else{
				$exist=countdata('ticker',"tickId='$id'");
 				//check whether ID version exist
				if(!$exist){
					$error=errorlist(29);
				}

				//update data
				if (!$error){
 					$sql="UPDATE ticker_lang SET tickNews='$news' WHERE tickId='$id' AND lang='$lang'";
					$query=query($sql);
				}
			}
		}

		//check whether query was successful
		if(!$query){
			$error=errorlist(3);
		}

		if($error){
			print "<p>";
			print "<ul>";
			print nl2br($error);
			print "</ul>";
			print "</p>";
		}else{
			if(!$js){
				header("location:infoupdate.php");
			}else{
				print "ok";
			}
		}
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php print SITE_NAME; ?> - CMS -<?php print _('infoupdate_pagetitle'); ?></title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- bootstrap-datepicker -->
<link rel="stylesheet" type="text/css" href="/libs/bootstrap-datepicker/css/datepicker3.css" />
<!-- fancybox -->
<link href="/style/jquery.fancybox.css" rel="stylesheet"/>
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS Required -->
<script type="text/javascript" src="/javascript/commonjs.js"></script>
<script type="text/javascript" src="/javascript/jquery.validate.js"></script>
<script type="text/javascript" src="/javascript/jquery.form.js"></script>
<script type="text/javascript" src="/javascript/validate.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/libs/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script type="text/javascript" src="/libs/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/libs/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- required for this page -->
<script type="text/javascript" src="/libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce.js"></script>
<script type="text/javascript" src="/libs/tinymce/tinymce_simple.js"></script>
<script type="text/javascript" src="/libs/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- jQuery custom content scroller -->
<script type="text/javascript" src="/javascript/jquery.fancybox.pack.js"></script>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('.fancybox').fancybox({
		'arrows': false
	});

	$('#result').hide();
	validate('#result','#add','infoupdate.php');
	validate('#result','#edit','infoupdate.php');

	$(".datepickerdate").datepicker({
		startDate: '<?php echo date('d') ?>/<?php echo date('m') ?>/<?php echo date('Y') ?>',
		format: 'dd/mm/yyyy',
		todayHighlight: true,
		toggleActive: true,
		autoclose: true
	});

	$(".datepickerdate-update").datepicker({
		format: 'dd/mm/yyyy',
		todayHighlight: true,
		toggleActive: true,
		autoclose: true
	});
});
</script>
</head>
<body class="nav-md">
<div id="result"></div>
<div class="delwordcms"><?php echo _('cms_delete_word'); ?></div>

<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col menu_fixed">
			<div class="left_col scroll-view">
	            <!-- Header Menu -->
	            <?php require("com/com-header-menu.php"); ?>
	            <!-- /Header Menu -->
	            <br />
	            <!-- Main Menu -->
	            <?php include("com/com-menu.php"); ?>
	            <!-- /Main Menu -->
			</div>
			<!-- END .left_col .scroll-view -->
		</div>
		<!-- END .col-md-3 .left_col .menu_fixed -->

		<!-- Main Header Bar -->
        <?php include("com/com-greet.php"); ?>
        <!-- /Main Header Bar -->

        <!-- THE CONTENT OF PAGE HERE -->
        <div class="right_col" role="main">

        	<div class="row">
				<div class="col-md-12">
		            <div class="page-title">
			            <div class="title_left" style="width: 100%;">
			                <h3><?php print _('infoupdate_pagetitle'); ?></h3>
			                <p><?php print _('infoupdate_pagedesc'); ?><?php print SITE_NAME; ?></p>
			            </div>
		            </div>
	        	</div>
        	</div>

        	<?php if ($uac_add){ ?>
        	<div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                	<div class="pull-right">
                		<a href="infoupdate.php?action=add" class="btn btn-primary"><?php print _('infoupdate_addinfoupdate'); ?></a>
                	</div>
                </div>
        	</div>
        	<?php } ?>

        	<div class="row">
	        	<div class="col-md-12">
                	<?php if ($action==""){ ?>
                		<div class="table-responsive">
                		<?php
							if (empty($pg)){
								$pg=1;
							}
							$numPerPage=20;
							$offset=($pg-1)*$numPerPage;
							$num=1+(($pg-1)*$numPerPage);
						?>
                		<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
							<thead>
								<tr class="tablehead">
									<th class="text-center" width="150"><?php print _('infoupdate_date'); ?></th>
									<th><?php print _('infoupdate_pagetitle'); ?></th>
									<?php if(count($lc_lang)>1){ ?><th style="width:40px;" class="text-center"><i rel="tooltip" title="<?php echo _('multi_language_view_language'); ?>" class="fa fa-language"></i></th><?php }?>
									<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
									<th style="width:40px;" class="text-center"><i class="fa fa-cog"></i></th>
								</tr>
							</thead>
							<tbody>
								<?php
									$sql="SELECT * FROM ticker ORDER BY tickDateTime DESC";
									$query=query($sql);
									$numofdata=rows($query);

									$sql.=' LIMIT '.$offset.','.$numPerPage;
									$query=query($sql);

									if ($numofdata<1){
								?>
								<tr>
									<td colspan="5"><div class="text-center"><br /><?php print _('infoupdate_notavailable'); ?><br /><br /></div></td>
								</tr>
								<?php 
									} else {
										while ($data=fetch($query)){
								?>
								<tr style="vertical-align: top">
									<td class="text-center"><?php print dateformat($data['tickDateTime']); ?></td>
									<td><?php print output($data['tickNews']); ?></td>
									<?php
										if(count($lc_lang)>1){
											?>
											<td class="text-center">
												<?php
												foreach($lc_lang as $key=>$value){
													$exist=countdata("ticker_lang","tickId='".$data['tickId']."' and lang='$value'");
													if($value==$lc_lang_default) continue;
													if($exist and $uac_edit){
														?><a href="infoupdate.php?action=editen&amp;lang=<?php print $value;?>&id=<?php print $data['tickId']; ?>" rel="tooltip" title="<?php print _('infoupdate_editvers'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-edit.png" alt="<?php print $value;?>"  border="0" /></a><?php
													}
													elseif($uac_add){
														?><a href="infoupdate.php?action=adden&amp;lang=<?php print $value;?>&id=<?php print $data['tickId']; ?>" rel="tooltip" title="<?php print _('infoupdate_addvers'); ?> <?php print localename($value);?>"><img class="icon-lang" src="../assets/images/flag-<?php print $value;?>-add.png" alt="<?php print $value;?>" border="0" /></a><?php
													}
												} ?>
											</td>
										<?php
									} ?>
									<td class="text-center">
										<?php if ($uac_edit){ ?><a href="infoupdate.php?action=edit&amp;id=<?php print $data['tickId']; ?>" rel="tooltip" title="<?php print _('cms_edit'); ?>: info/update"><i class="fa fa-edit"></i></a><?php } ?>
									</td>
									<td class="text-center">
										<?php if ($uac_delete){ ?><a class="delete" href="infoupdate.php?action=del&amp;form=submit&amp;id=<?php print $data['tickId']; ?>" title="info/update"><i rel="tooltip" title="<?php print _('cms_delete'); ?>: info/update" class="fa fa-trash-o"></i></a><?php } ?>
									</td>
								</tr>
								<?php 	$num++;
										}
									}
								?>
							</tbody>
						</table>
                		</div>
                		<?php
							$options['total']=$numofdata;
							$options['filename']='infoupdate.php';
							$options['qualifier']='infoupdate.php';
							$options['pg']=$pg;
							$options['numPerPage']=$numPerPage;
							$options['style']=1;
							$options['addquery']=TRUE;
							paging($options);
						?>
					<?php } elseif ($action=="add" and $uac_add){ ?>
					<a href="infoupdate.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('infoupdate_mainaddinfoupdate') ?></h4>
					<hr/>
					<form name="add" id="add" method="post" action="infoupdate.php?action=add&amp;form=submit" class="form-horizontal form-label-left">
						<div class="form-group">
	                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="date"><?php print _('infoupdate_date'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="date" type="text" id="date" size="12" value="<?php print date("d/m/Y",$now); ?>" required class="form-control col-md-7 col-xs-12 datepickerdate has-feedback-left">
	                        	<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="news"><?php print _('infoupdate_info'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<textarea name="news" cols="80" rows="5" id="news" class="tinymce-simple" required></textarea>
	                        </div>
                    	</div>

                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-1 col-sm-1 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='infoupdate.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>
					</form>
					<?php } elseif ($action=="edit" and $uac_edit){
						$sql="SELECT * FROM ticker WHERE tickId='$id'";
						$query=query($sql);
						$data=fetch($query);
						$data=formoutput($data);
					?>
					<a href="infoupdate.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('infoupdate_editinfoupdate') ?></h4>
					<hr/>
					<form name="edit" id="edit" method="post" action="infoupdate.php?action=edit&amp;form=submit&amp;id=<?php print $id; ?>" class="form-horizontal form-label-left">
						<div class="form-group">
	                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="date"><?php print _('infoupdate_date'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<input name="date" type="text" id="date" size="12" value="<?php print date("d/m/Y",$data['tickDateTime']); ?>" required class="form-control col-md-7 col-xs-12 datepickerdate-update has-feedback-left">
	                        	<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
	                        </div>
                    	</div>

                    	<div class="form-group">
	                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="news"><?php print _('infoupdate_info'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<textarea name="news" cols="80" rows="5" id="news" class="tinymce-simple" required><?php print $data['tickNews']; ?></textarea>
	                        </div>
                    	</div>

                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-1 col-sm-1 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_updatebutton'); ?>"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='infoupdate.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>

					</form>
					<?php } elseif ($action=="adden" and $uac_add){
						$sql = "SELECT tickNews FROM ticker WHERE tickId='$id'";
						$query = query($sql);
						$data = fetch($query);
					?>
					<a href="infoupdate.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('infoupdate_addinfoupdatevers'); ?> <?php print localename($lang);?></h4>
					<hr/>
					<div style="background-color:#ededed;font-size:10px;border:1px dashed #777;margin-bottom:10px;padding:11px;"><?php print $data['tickNews'];?></div><br/>

					<form name="add" id="add" method="post" action="infoupdate.php?action=adden&amp;form=submit&amp;lang=<?php print $lang;?>&amp;id=<?php print $id; ?>" class="form-horizontal form-label-left">

						<div class="form-group">
	                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="news"><?php print _('infoupdate_info'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<textarea name="news" cols="80" rows="5" id="news" class="tinymce-simple" required></textarea>
	                        </div>
                    	</div>

                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-1 col-sm-1 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_addbutton'); ?>"><i class="fa fa-plus"></i> <?php print _('cms_addbutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='infoupdate.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>
					</form>
					<?php }elseif ($action=="editen" and $uac_edit){
						$sql="SELECT * FROM ticker_lang WHERE tickId='$id' AND lang='$lang'";
						$query=query($sql);
						$data=fetch($query);
						$data=formoutput($data);
					?>
					<a href="infoupdate.php" class="btn btn-warning btn-sm"><i class="fa fa-angle-double-left"></i> <?php print _('pagecontent_back'); ?></a><br/>

	        		<h4><?php print _('infoupdate_editinfoupdatevers'); ?> <?php print localename($lang);?></h4>
					<hr/>
					<div style="background-color:#ededed;font-size:10px;border:1px dashed #777;margin-bottom:10px;padding:11px;"><?php print getval('tickNews','ticker','tickId',$id); ?></div><br/>

					<form name="edit" id="edit" method="post" action="infoupdate.php?action=editen&amp;form=submit&amp;lang=<?php print $lang;?>&amp;id=<?php print $id; ?>" class="form-horizontal form-label-left">

						<div class="form-group">
	                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="news"><?php print _('infoupdate_info'); ?> <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<textarea name="news" cols="80" rows="5" id="news" class="tinymce-simple" required><?php print $data['tickNews']; ?></textarea>
	                        </div>
                    	</div>

                    	<hr/>

                    	<div class="form-group">
	                        <div class="control-label col-md-1 col-sm-1 col-xs-12"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                        	<button name="Submit" type="submit" class="btn btn-primary btn-sm" value="<?php print _('cms_updatebutton'); ?>"><i class="fa fa-refresh"></i> <?php print _('cms_updatebutton'); ?></button>
	                        	<button name="cancel" type="button" class="btn btn-danger btn-sm" onclick="window.location='infoupdate.php'"><i class="fa fa-reply"></i> <?php print _('cms_cancelbutton'); ?></button>
	                        	<button name="reset" type="reset" class="btn btn-warning btn-sm" id="reset"><i class="fa fa-undo"></i> <?php print _('cms_resetbutton'); ?></button>
	                        </div>
                    	</div>
					</form>
                	<?php } ?>

		        </div>
		    </div>

        </div>
        <!-- END THE CONTENT OF PAGE HERE -->


		<?php include("com/com-footer.php"); ?>

	</div>
	<!-- END .main_container -->
</div>
<!-- END .container .body -->

<script type="text/javascript" src="/javascript/main-admin.js"></script>
</body>
</html>