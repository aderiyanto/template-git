<?php

	//includes all files necessary to support operations
	include("../modz/config-main.php");
	include("../modz/config-image.php");
	include("../modz/config.php");
	include("../modz/license.php");
	include("../modz/errormsg.php");
	include("../modz/mainmod.php");
	include("../modz/connic.php");
	include("../modz/getall-admin.php");
	include("authuser.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="/assets/interface/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Image Library</title>

<!-- Bootstrap -->
<link href="/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/libs/nprogress/nprogress.css" rel="stylesheet">
<!-- Custom Theme Style -->
<link href="/style/style-admin.css" rel="stylesheet">

<!-- Main JS -->
<!-- jQuery -->
<script type="text/javascript" src="/javascript/jquery.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>

<script type="text/javascript"> 
jQuery(document).ready(function($){
$('#resultnopadding').hide();
validate('#resultnopadding','#upload','img.php?form=submit&source=imgfly');	
});
</script>
<style type="text/css">
	body.flybody{
		height: 500px !important;
	}
</style>
</head>
<body class="flyctn">
    <div class="container flybody">
        <?php
        if($action=="readmore"){
			//Colect data from newsletter
			$sqlnews="SELECT * FROM newsletter WHERE nId='$id'";
			$querynews=query($sqlnews);
			$datanews=fetch($querynews);
			
			//Prepare variable
			$to=$memberemail;
			$subject=$datanews['nSubject'];
			$nmainimg='/assets/newsletter/'.$datanews['nMainImg'];
			$newsdate=$datanews['nRegDate'];
			$emailcontent=$datanews['nEmailMsg'];
			$emailfooter=$datanews['nFooter'];
			$nads=$datanews['nAds'];
			$subscribe = sprintf(_('newsletter_footer'),SITE_NAME);
			$content='<style>
							.newsletter{
								width:700px;
								margin-left: auto;
								margin-right: auto;
								background-color:#f6f3e4;
							}

							.mainnewsleter_content{
								width:600px;
								margin-left: auto;
								margin-right: auto;
								valign:top;
								background-color:#ffffff;	
								padding-left:13px; 
								padding-right:13px;
							}
							 
							.newsfooter{
								font-family: Georgia; 
								font-size:10px;
								line-height:12px;
								text-align:center; 
								color:#888; 
							}

							#newstitle{
								text-align:center;
								font-family:Georgia, Times, serif; 
								font-size:24px;
								font-weight:bolder;
								padding:10px;
							}
							</style>';
			$content.=	'<table align="center" bgcolor="#f6f3e4" width="100%">
							<tr>
								<td>
									<table width="600"  bgcolor="#ffffff" cellpadding="10" align="center"> 
										<tr>
											<td align="center"><img src="http://'.getconfig('SITE_URL').'/assets/images/newsletter_header.jpg" width="600"></td>
										</tr>
										<tr>
											<td align="center"><img src="http://'.getconfig('SITE_URL').$nmainimg.'"></td>
										</tr>
										<tr>
											<td align="center"  style="font-family:Georgia, Times, serif;font-size:24px;font-weight:bolder;">'.$subject.'</td>
										</tr>
										<tr>
											<td align="center"><img src="http://'.getconfig('SITE_URL').'/assets/images/divider.gif" align="center" width="600"></td>
										</tr>
										<tr>
											<td >'.$emailcontent.'</td>
										</tr>
										<tr>
											<td align="center"><img src="http://'.getconfig('SITE_URL').'/assets/images/hr-newsletter.gif" height="11" width="600"></td>
										</tr>
										<tr>
											<td style="	font-family: Georgia;font-size:10px;line-height:12px;text-align:left;color:#888;padding-left:13px">'.$subscribe.' <a  href="#">unsubscribe now</a>.</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>';
		print $content;
	}
        ?>
    </div>
</body>
</html>