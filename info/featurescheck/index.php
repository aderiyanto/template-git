<?php
$k = 'pdo_driver';
$arr[$k]['name'] = 'PHP PDO EXTENSION';
if(extension_loaded('PDO')){
	if(extension_loaded('pdo_mysql') and in_array('mysql', PDO::getAvailableDrivers()) == true){
		$arr[$k]['result'] = array(
			'type' => 'ok'
		);
	} else {
		$arr[$k]['result'] = array(
			'type' => 'error',
			'desc' => 'PDO extension loaded, but MySQL driver not found.'
		);
	}
} else {
	$arr[$k]['result'] = array(
		'type' => 'error',
		'desc' => 'Missing PHP PDO'
	);
}

$k = 'exec';
$arr[$k]['name'] = 'PHP Exec';
if(function_exists('exec')){
	if(exec('echo EXEC') == 'EXEC'){
		$arr[$k]['result'] = array(
			'type' => 'ok'
		);
	} else {
		$arr[$k]['result'] = array(
			'type' => 'error',
			'desc' => 'Exec loaded, but not working correctly.'
		);
	}
} else {
	$arr[$k]['result'] = array(
		'type' => 'error',
		'desc' => 'PHP Exec not loaded.'
	);
}

$k = 'ioncube';
$arr[$k]['name'] = 'Ioncube Loader';
if(extension_loaded ('ionCube Loader')){
	$arr[$k]['result'] = array(
		'type' => 'ok'
	);
} else {
	$arr[$k]['result'] = array(
		'type' => 'error',
		'desc' => 'Ioncube loader not installed!'
	);
}

$k = 'base64';
$arr[$k]['name'] = 'BASE64';
if(function_exists('base64_encode') and function_exists('base64_decode')){
	$arr[$k]['result'] = array(
		'type' => 'ok'
	);
} else {
	$arr[$k]['result'] = array(
		'type' => 'error',
		'desc' => 'Missing base64 encoder'
	);
}


$k = 'gettext';
$arr[$k]['name'] = 'GETTEXT';
if(function_exists('gettext') and function_exists('bindtextdomain') and function_exists('textdomain') and function_exists('setlocale')){
	$arr[$k]['result'] = array(
		'type' => 'ok'
	);
} else {
	$arr[$k]['result'] = array(
		'type' => 'error',
		'desc' => 'Missing gettext function'
	);
}

$k = 'mcrypt';
$arr[$k]['name'] = 'Mcrypt';
if(function_exists('mcrypt_encrypt') and function_exists('mcrypt_decrypt')){
	$randKey = rand(16);
	$crypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, 'cdscvsdghcvdghsv', 'hello', MCRYPT_MODE_ECB);
	if(trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, 'cdscvsdghcvdghsv', $crypt, MCRYPT_MODE_ECB)) == 'hello'){
		$arr[$k]['result'] = array(
			'type' => 'ok'
		);
	} else {
			$arr[$k]['result'] = array(
			'type' => 'error',
			'desc' => 'Mcrypt loaded, but not working properly.'
		);
	}
} else {
	$arr[$k]['result'] = array(
		'type' => 'error',
		'desc' => 'Missing gettext extension'
	);
}

$k = 'DOMDocument';
$arr[$k]['name'] = 'DOMDocument';
if(class_exists('DOMDocument')){
	$arr[$k]['result'] = array(
		'type' => 'ok'
	);
} else {
	$arr[$k]['result'] = array(
		'type' => 'error',
		'desc' => 'Missing DOMDocument Class.'
	);
}

$k = 'ob';
$arr[$k]['name'] = 'Output Buffering';
if(function_exists('ob_start') and function_exists('ob_end_flush')){
	$arr[$k]['result'] = array(
		'type' => 'ok'
	);
} else {
	$arr[$k]['result'] = array(
		'type' => 'error',
		'desc' => 'missing Output Buffering.'
	);
}

$k = 'mod_rewrite';
$arr[$k]['name'] = 'Apache Mod Rewrite';
if(array_key_exists('HTTP_MOD_REWRITE', $_SERVER)){
	$arr[$k]['result'] = array(
		'type' => 'ok'
	);
} else {
	$arr[$k]['result'] = array(
		'type' => 'error',
		'desc' => 'Missing Apache Modules.'
	);
}

$k = 'mod_headers';
$arr[$k]['name'] = 'Apache Mod Headers';
if(array_key_exists('HTTP_MOD_HEADERS', $_SERVER)){
	$arr[$k]['result'] = array(
		'type' => 'ok'
	);
} else {
	$arr[$k]['result'] = array(
		'type' => 'error',
		'desc' => 'Missing Apache Modules.'
	);
}

$k = 'mod_expires';
$arr[$k]['name'] = 'Apache Mod Expires';
if(array_key_exists('HTTP_MOD_EXPIRES', $_SERVER)){
	$arr[$k]['result'] = array(
		'type' => 'ok'
	);
} else {
	$arr[$k]['result'] = array(
		'type' => 'error',
		'desc' => 'Missing Apache Modules.'
	);
}
?>
<html>
<head>
	<title>Requirement Website Features</title>
</head>
<style>
body{
	background-color:#333;
	color:#fff;
	font-size:14px;
	font-family: arial, verdana;
}

.error, .ok{
	padding:3px 5px;
	color: #fff;
}

.error{
	background-color:#dd0000;
}

.ok{
	background-color:#68ad2d;
}

.desc{
	font-size:12px;
	font-style:italic;
	padding-top:5px;
	color: #888;
}

.warning-box{
	border:1px solid #fff;
	padding:8px;
	background-color:#dd0000;
	width:500px;
	margin-bottom:30px;
}
</style>
<body>
<h1>Requirement Website Features</h1><br/>
<?php
if(array_key_exists('HTACCESS_LOADED', $_SERVER) == false){
	?><div class="warning-box"><b>.HTACCESS</b> file not loaded, Some task can't be performed.</div><?php
}?>

<table cellpadding="7" border="0">
<?php
foreach($arr as $key => $val){
	?><tr valign="top">
		<td width="300"><b><?php print $val['name'];?></b></td>
		<td>
			<span class="<?php print $val['result']['type'];?>"><?php print $val['result']['type'];?></span>
			<?php
			if(isset($val['result']['desc'])){
				?><div class="desc"><?php print $val['result']['desc'];?></div><?php
			}?>
		</td>
	</tr>
	<?php
}
?>
</table>
</body>
</html>